var $angularScope = {};
var app = angular.module('myContestApp', []);
app.directive('imageExists', function($http) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            attrs.$observe('ngSrc', function(ngSrc) {
                $http.get(ngSrc).then(function(){
                }, function(){
                    element.attr('src', '/images/user.png'); // set default image
                });
            });
        }
    };
});
app.controller('imageDetailsController', function($scope, $interval, $http) {
    var vm = $scope;
    vm.comments = [];
    vm.contestEntryId = null;
    vm.voteTypeName = '';
    vm.voteAmount = 0;
    vm.votePoint = 0;
    vm.userId = abp.session.userId;
    vm.nickname = '';
    vm.profileImage = '';
    vm.comment = '';
    vm.processing = false;
    vm.errorLogin = null;
    vm.errorMessage = null;
    vm.page = 1;
    vm.pageSize = 20;
    vm.totalCount = 0;
    vm.totalVotes = 0;
    vm.endOfItems = false;
    vm.isLoginRequired = false;
    vm.isLoginRequiredToComment = false;
    vm.isFirstLoading = true;
    vm.votingProcessing = false;
    $interval(function () {
        vm.vote = function (contestEntryId, contestId) {
            if (vm.voteTypeName !== 'Open') {
                vm.isLoginRequired = abp.session.userId === null;
                if (abp.session.userId === null) {
                    abp.message.error('Please login to vote for this user.');
                    return;
                }
            }
            if (contestId === undefined || contestEntryId === undefined){
                abp.message.error('Your vote could not be completed. Requirements not met.');
                return;
            }
            if (vm.voteTypeName === 'Paid') {
                abp.message.confirm('This vote requires you to pay NGN' + vm.voteAmount + ' which will be deducted from your wallet. Do you want to continue?', function (result) { 
                    if (!result){
                        return;
                    }
                    if (!vm.votingProcessing){
                    vm.votingProcessing = true;
                    $.get('/ContestEntry/Vote?contestEntryId=' + contestEntryId + '&contestId=' + contestId+ '&userId=' + abp.session.userId,
                        function (data) {
                            if (data.result.success){
                                vm.totalVotes++;
                                abp.notify.success(data.result.message);
                            }else{
                                abp.message.error(data.result.message);
                            }
                            vm.votingProcessing = false;
                        });
                }else{
                    abp.notify.error('Please wait for voting to complete.');
                }
                });
            }else if (vm.voteTypeName === 'VotePoint'){
                abp.message.confirm('This contest requires you to have ' + vm.votePoint + ' vote points to complete your vote. Do you want to continue?', function (result) {
                    if (!result) {
                        return;
                    }
                    if (!vm.votingProcessing){
                    vm.votingProcessing = true;
                    $.get('/ContestEntry/Vote?contestEntryId=' + contestEntryId + '&contestId=' + contestId + '&userId=' + abp.session.userId,
                        function (data) {
                            if (data.result.success) {
                                vm.totalVotes++;
                                abp.notify.success(data.result.message);
                            } else {
                                abp.message.error(data.result.message);
                            }
                            vm.votingProcessing = false;
                        });
                }else{
                        abp.notify.error('Please wait for voting to complete.');
                    }
                });
            } else{
                var query = '';
                if (abp.session.userId === null){
                     query = 'contestEntryId=' + contestEntryId + '&contestId=' + contestId;
                    
                } else{
                    query = 'contestEntryId=' + contestEntryId + '&contestId=' + contestId
                            + '&userId=' + abp.session.userId;
                }
                if (!vm.votingProcessing){
                    vm.votingProcessing = true;
                    $.get('/ContestEntry/Vote?' + query,
                        function (data) {
                            if (data.result.success){
                                vm.totalVotes++;
                                abp.notify.success(data.result.message);
                            }else{
                                abp.message.error(data.result.message);
                            }
                            vm.votingProcessing = false;
                        }); 
                }else{
                    abp.notify.error('Please wait for voting to complete.');
                } 
            }
        };
        vm.getComments = function () {
            if (vm.processing === false && vm.endOfItems === false) {
                vm.processing = true;
                //abp.ui.setBusy('#comment-wrp');
                $.get('/ContestEntry/Comments?contestEntryId=' + vm.contestEntryId + '&page=' + vm.page + '&pageSize=' + vm.pageSize,
                    function (data) {
                        if (data.result.success){
                            if (data.result.data.length > 0){
                                
                                if (vm.isFirstLoading){
                                    vm.comments = [];
                                } 
                                data.result.data.forEach(function (item) {
                                    vm.comments.push(item)
                                });
                                vm.processing = false;
                                vm.isFirstLoading = false;
                                vm.totalCount = data.result.totalCount;
                                vm.page++;

                            }else{
                                vm.endOfItems = true;
                                vm.processing = false;
                            }
                             console.log('Request finished.');
                             abp.ui.clearBusy('.comment-wrp');
                        }else{
                            abp.message.error(data.result.message);
                        }
                    });
            }

        };
        vm.postComments = function () {
            if (vm.userId === 0 || vm.userId === undefined){
                vm.isLoginRequiredToComment = true;
                return;
            } else if (vm.comment === ''){
                abp.message.warn('Please enter your comment.');
                return;
            }
            $.get('/ContestEntry/CreateComment',
                {ContestEntryId: vm.contestEntryId, Comment : vm.comment, UserId: vm.userId},function (data) {
                    if (data.result.success){
                        abp.notify.success('Comment posted successfully.');
                        vm.comment = '';
                        vm.processing = false;
                        vm.endOfItems = false;
                        vm.isFirstLoading = true;
                        vm.page = 1;
                        vm.getComments();
                    }else{
                        abp.message.error(data.result.message);
                    }

                });

        };
        vm.hideLoginAlert = function () {
            vm.isLoginRequired = false;
        };
        $angularScope = vm;
    }, 500);


});
jQuery(function ($) {
    setTimeout(function(){
        console.log($angularScope);
        abp.ui.setBusy('.comment-wrp');
        $angularScope.getComments();
        },1000);
   
});
