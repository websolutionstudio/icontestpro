﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using IContestPro.Configuration;

namespace IContestPro.Web.Host.Startup
{
    [DependsOn(
       typeof(IContestProWebCoreModule))]
    public class IContestProWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public IContestProWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(IContestProWebHostModule).GetAssembly());
        }
    }
}
