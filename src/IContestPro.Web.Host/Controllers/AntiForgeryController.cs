using Microsoft.AspNetCore.Antiforgery;
using IContestPro.Controllers;

namespace IContestPro.Web.Host.Controllers
{
    public class AntiForgeryController : IContestProControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
