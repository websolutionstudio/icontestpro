﻿using Abp.MultiTenancy;
using IContestPro.Authorization.Users;

namespace IContestPro.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
