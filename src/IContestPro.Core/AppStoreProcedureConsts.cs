﻿namespace IContestPro
{
    public class AppStoreProcedureConsts
    {
     public const string SpGetRandomFeaturedContests = "SpGetRandomFeaturedContests";
     public const string SpGetContestEntriesToPromote = "SpGetContestEntriesToPromote";
     public const string SpGetRandomFeaturedContestEntries = "SpGetRandomFeaturedContestEntries";
     public const string SpGetTopContestEntriesWithHighestVote = "SpGetTopContestEntriesWithHighestVote";
     public const string SpGetTopContestsWithHighestVotes = "SpGetTopContestsWithHighestVotes";
     public const string SpGetTopUsersWithHighestVotes = "SpGetTopUsersWithHighestVotes";
     public const string SpGetTopUsersWithHighestVotesInAContest = "SpGetTopUsersWithHighestVotesInAContest";
     public const string SpGetCurrentUserFollowers = "SpGetCurrentUserFollowers";
     public const string SpGetUsersInContest = "SpGetUsersInContest";
     public const string SpGetMessagesForUserInbox = "SpGetMessagesForUserInbox";
     public const string SpGetMessagesForUserSent = "SpGetMessagesForUserSent";
     public const string SpGetMessageDetails = "SpGetMessageDetails";
     public const string SpGetMessageResonses = "SpGetMessageResonses";
     public const string SpCheckIfCodeExists = "SpCheckIfCodeExists";
     public const string SpCheckIfBatchNoExists = "SpCheckIfBatchNoExists";
     public const string SpGetCodeDetailsByCode = "SpGetCodeDetailsByCode";
     public const string SpGetContests = "SpGetContests";
     public const string SpSearchForHomePages = "SpSearchForHomePages";
     public const string SpGetBlogsByStatus = "SpGetBlogsByStatus";
     public const string SpGetBlogTickers = "SpGetBlogTickers";
     public const string SpSearchBlogsByTag = "SpSearchBlogsByTag";
     public const string SpGetPastWinners = "SpGetPastWinners";
     public const string SpGetContestViews = "SpGetContestViews";
     public const string GetSponsorFollowers = "GetSponsorFollowers";
     public const string SpSearchContestsByTag = "SpSearchContestsByTag";
    }
   
}
