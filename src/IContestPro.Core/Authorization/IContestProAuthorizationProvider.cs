﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace IContestPro.Authorization
{
    public class IContestProAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            context.CreatePermission(PermissionNames.Pages_Admin, L("Admin"));
            context.CreatePermission(PermissionNames.Pages_Sponsor, L("Sponsor"));
            
            context.CreatePermission(PermissionNames.Pages_Manage, L("Manage"));
            context.CreatePermission(PermissionNames.Pages_Manage_Create, L("ManageCreate"));
            context.CreatePermission(PermissionNames.Pages_Manage_Update, L("ManageUpdate"));
            context.CreatePermission(PermissionNames.Pages_Manage_Delete, L("ManageDelete"));
            
            context.CreatePermission(PermissionNames.Pages_Wallet, L("Wallet"));
            context.CreatePermission(PermissionNames.Pages_Wallet_Deposit, L("WalletDeposit"));
            context.CreatePermission(PermissionNames.Pages_Wallet_Withdraw, L("WalletWithdraw"));
            context.CreatePermission(PermissionNames.Pages_Wallet_Transfer, L("WalletTransfer"));
            
            context.CreatePermission(PermissionNames.Pages_WalletManagement, L("WalletManagement"));
            context.CreatePermission(PermissionNames.Pages_WalletManagement_ProcessDeposit, L("WalletManagementProcessDeposit"));
            context.CreatePermission(PermissionNames.Pages_WalletManagement_ProcessWithdraw, L("WalletManagementProcessWithdraw"));
            context.CreatePermission(PermissionNames.Pages_WalletManagement_ProcessTransfer, L("WalletManagementProcessTransfer"));

            context.CreatePermission(PermissionNames.Pages_Referral, L("Referral"));
            context.CreatePermission(PermissionNames.Pages_Referral_Update, L("ReferralUpdate"));
   
            context.CreatePermission(PermissionNames.Pages_Contest, L("Contest"));
            context.CreatePermission(PermissionNames.Pages_Contest_Start, L("ContestStart"));
            context.CreatePermission(PermissionNames.Pages_Contest_Create, L("ContestCreate"));
            context.CreatePermission(PermissionNames.Pages_Contest_Update, L("ContestUpdate"));
            context.CreatePermission(PermissionNames.Pages_Contest_Delete, L("ContestDelete"));
            context.CreatePermission(PermissionNames.Pages_Contest_Choose_Winner, L("ContestChooseWinner"));
            context.CreatePermission(PermissionNames.Pages_Contest_Close_Winner, L("ContestCloseWinner"));

            context.CreatePermission(PermissionNames.Pages_Code_System, L("CodeSystem"));
            context.CreatePermission(PermissionNames.Pages_Code_System_Create, L("CodeSystemCreate"));
            context.CreatePermission(PermissionNames.Pages_Code_System_Update, L("CodeSystemUpdate"));
            context.CreatePermission(PermissionNames.Pages_Code_System_Delete, L("CodeSystemDelete"));
            context.CreatePermission(PermissionNames.Pages_Code_System_Validate, L("CodeSystemValidate"));
            
            context.CreatePermission(PermissionNames.Pages_Advert, L("Advert"));
            context.CreatePermission(PermissionNames.Pages_Advert_Create, L("AdvertCreate"));
            context.CreatePermission(PermissionNames.Pages_Advert_Update, L("AdvertUpdate"));
            context.CreatePermission(PermissionNames.Pages_Advert_Delete, L("AdvertDelete"));
            context.CreatePermission(PermissionNames.Pages_Advert_Process, L("AdvertProcess"));
            context.CreatePermission(PermissionNames.Pages_Advert_Report, L("AdvertReport"));
            
            context.CreatePermission(PermissionNames.Pages_Vote_Record, L("VoteRecord"));
            context.CreatePermission(PermissionNames.Pages_Vote_Record_Create, L("VoteRecordCreate"));
            context.CreatePermission(PermissionNames.Pages_Vote_Record_Update, L("VoteRecordUpdate"));
            context.CreatePermission(PermissionNames.Pages_Vote_Record_Delete, L("VoteRecordDelete"));

        context.CreatePermission(PermissionNames.Pages_Leader_Board, L("LeaderBoard"));
        context.CreatePermission(PermissionNames.Pages_Over_Leader_Board, L("OverLeaderBoard"));
        context.CreatePermission(PermissionNames.Pages_Contest_Leader_Board, L("ContestLeaderBoard"));
        context.CreatePermission(PermissionNames.Pages_Voter_Leader_Board, L("VoterLeaderBoard"));

        context.CreatePermission(PermissionNames.Pages_Report_UserList, L("ReportUserList"));
        context.CreatePermission(PermissionNames.Pages_Report_Transactions, L("ReportTransactions"));
        context.CreatePermission(PermissionNames.Pages_Report_Transaction_Fee, L("ReportTransactionFee"));
        context.CreatePermission(PermissionNames.Pages_Report_Contest_Statistic, L("ReportContestStatistic"));
        context.CreatePermission(PermissionNames.Pages_Report_Vote_Statistic, L("ReportVoteStatistic"));
        context.CreatePermission(PermissionNames.Pages_Report_Account_Position, L("ReportAccountPosition"));

        context.CreatePermission(PermissionNames.Pages_SupportTicket, L("SupportTicket"));
        context.CreatePermission(PermissionNames.Pages_SupportTicket_Create, L("SupportTicketCreate"));
        context.CreatePermission(PermissionNames.Pages_SupportTicket_Update, L("SupportTicketUpdate"));
        context.CreatePermission(PermissionNames.Pages_SupportTicket_Delete, L("SupportTicketDelete"));

        context.CreatePermission(PermissionNames.Pages_Dispute, L("Dispute"));
        context.CreatePermission(PermissionNames.Pages_Dispute_Create, L("DisputeCreate"));
        context.CreatePermission(PermissionNames.Pages_Dispute_Update, L("DisputeUpdate"));
        context.CreatePermission(PermissionNames.Pages_Dispute_Delete, L("DisputeDelete"));

        context.CreatePermission(PermissionNames.Pages_App_Settings, L("AppSettings"));
        context.CreatePermission(PermissionNames.Pages_App_Settings_Create, L("AppSettingsCreate"));
        context.CreatePermission(PermissionNames.Pages_App_Settings_Update, L("AppSettingsUpdate"));
        context.CreatePermission(PermissionNames.Pages_App_Settings_Delete, L("AppSettingsDelete"));

        
            
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, IContestProConsts.LocalizationSourceName);
        }
    }
}
