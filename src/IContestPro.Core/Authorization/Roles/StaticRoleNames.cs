namespace IContestPro.Authorization.Roles
{
    public static class StaticRoleNames
    {
        public static class Host
        {
            public const string Admin = "Admin";
        }

        public static class Tenants
        {
            public const string Admin = "Admin";
            public const string AdminLevel2 = "AdminLevel2";
            public const string Sponsors = "Sponsors";
            public const string Users = "Users";
            
        }
    }
}
