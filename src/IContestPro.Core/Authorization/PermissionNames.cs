﻿namespace IContestPro.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";

        public const string Pages_Roles = "Pages.Roles";
        public const string Pages_Admin = "Pages.Admin";
        public const string Pages_Sponsor = "Pages.Sponsor";
        
        public const string Pages_Manage = "Pages.Manage";
        public const string Pages_Manage_Create = "Pages.Manage.Create";
        public const string Pages_Manage_Update = "Pages.Manage.Update";
        public const string Pages_Manage_Delete = "Pages.Manage.Delete";
        
        public const string Pages_Wallet = "Pages.Wallet";
        public const string Pages_Wallet_Deposit = "Pages.Wallet.Deposit";
        public const string Pages_Wallet_Withdraw = "Pages.Wallet.Withdraw";
        public const string Pages_Wallet_Transfer = "Pages.Wallet.Transfer";

        public const string Pages_WalletManagement = "Pages.WalletManagement";
        public const string Pages_WalletManagement_ProcessDeposit = "Pages.WalletManagement.ProcessDeposit";
        public const string Pages_WalletManagement_ProcessWithdraw = "Pages.Wallet.ProcessWithdraw";
        public const string Pages_WalletManagement_ProcessTransfer = "Pages.Wallet.ProcessTransfer";
       
        public const string Pages_Referral = "Pages.Referral";
        public const string Pages_Referral_Update= "Pages.Referral.Update";
        
        public const string Pages_Contest = "Pages.Contest";
        public const string Pages_Contest_Start = "Pages.Contest.StartDate";
        public const string Pages_Contest_Create = "Pages.Contest.Create";
        public const string Pages_Contest_Update = "Pages.Contest.Update";
        public const string Pages_Contest_Delete = "Pages.Contest.Delete";
        public const string Pages_Contest_Choose_Winner = "Pages.Contest.Choosewinner";
        public const string Pages_Contest_Close_Winner = "Pages.Contest.Closewinner";
        
        public const string Pages_Code_System = "Pages.Codesystem";
        public const string Pages_Code_System_Create = "Pages.Codesystem.Create";
        public const string Pages_Code_System_Update = "Pages.Codesystem.Update";
        public const string Pages_Code_System_Delete = "Pages.Codesystem.Delete";
        public const string Pages_Code_System_Validate = "Pages.Codesystem.Validate";
        
        public const string Pages_Advert = "Pages.Advert";
        public const string Pages_Advert_Create = "Pages.Advert.Create";
        public const string Pages_Advert_Update = "Pages.Advert.Update";
        public const string Pages_Advert_Delete = "Pages.Advert.Delete";
        public const string Pages_Advert_Process = "Pages.Advert.Process";
        public const string Pages_Advert_Report = "Pages.Advert.Report";
        
        public const string Pages_Vote_Record = "Pages.Voterecord";
        public const string Pages_Vote_Record_Create = "Pages.Voterecord.Create";
        public const string Pages_Vote_Record_Update = "Pages.Voterecord.Update";
        public const string Pages_Vote_Record_Delete = "Pages.Voterecord.Delete";
        
        public const string Pages_Leader_Board = "Pages.LeaderBoard";
        public const string Pages_Over_Leader_Board = "Pages.OverLeaderBoard";
        public const string Pages_Contest_Leader_Board = "Pages.ContestLeaderBoard";
        public const string Pages_Voter_Leader_Board = "Pages.Voterecord.VoterLeaderBoard";
    
        public const string Pages_Report_UserList = "Pages.Report.UserList";
        public const string Pages_Report_Transactions = "Pages.Report.Transaction";
        public const string Pages_Report_Transaction_Fee = "Pages.Report.TransactionFee";
        public const string Pages_Report_Contest_Statistic = "Pages.Report.ContestStatistic";
        public const string Pages_Report_Vote_Statistic = "Pages.Report.VoteStatistic";
        public const string Pages_Report_Account_Position = "Pages.Report.AccountPosition";
        
        public const string Pages_SupportTicket = "Pages.Support";
        public const string Pages_SupportTicket_Create = "Pages.Support.Create";
        public const string Pages_SupportTicket_Update = "Pages.Support.Update";
        public const string Pages_SupportTicket_Delete = "Pages.Support.Delete";
        
        public const string Pages_Dispute = "Pages.Dispute";
        public const string Pages_Dispute_Create = "Pages.Dispute.Create";
        public const string Pages_Dispute_Update = "Pages.Dispute.Update";
        public const string Pages_Dispute_Delete = "Pages.Dispute.Delete";
        
        public const string Pages_App_Settings = "Pages.Appsettings";
        public const string Pages_App_Settings_Create = "Pages.Appsettings.Create";
        public const string Pages_App_Settings_Update = "Pages.Appsettings.Update";
        public const string Pages_App_Settings_Delete = "Pages.Appsettings.Delete";
    }
}
