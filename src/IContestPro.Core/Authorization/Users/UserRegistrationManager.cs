﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization.Users;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.IdentityFramework;
using Abp.Localization;
using Abp.Notifications;
using Abp.Runtime.Session;
using Abp.UI;
using IContestPro.Authorization.Roles;
using IContestPro.BackgroundJobs;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Helpers;
using IContestPro.IContestProEntities;
using IContestPro.MultiTenancy;
using IContestPro.Notifications;

namespace IContestPro.Authorization.Users
{
    public class UserRegistrationManager : DomainService
    {
       
        public IAbpSession AbpSession { get; set; }

        private readonly TenantManager _tenantManager;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IRepository<Referral> _referralRepository;
        private readonly IRepository<Setting, long> _settingsRepository;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;

        public UserRegistrationManager(
            TenantManager tenantManager,
            UserManager userManager,
            RoleManager roleManager,
            IPasswordHasher<User> passwordHasher, IRepository<Referral> referralRepository, 
            INotificationSubscriptionManager notificationSubscriptionManager, IAppNotifier appNotifier, IRepository<Setting, long> settingsRepository)
        {
            _tenantManager = tenantManager;
            _userManager = userManager;
            _roleManager = roleManager;
            _passwordHasher = passwordHasher;
            _referralRepository = referralRepository;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;
            _settingsRepository = settingsRepository;
            AbpSession = NullAbpSession.Instance;
            LocalizationSourceName = IContestProConsts.LocalizationSourceName;

        }

        public async Task<User> RegisterAsync(string nickName, string phoneNumber, string emailAddress,
                        string plainPassword, bool isEmailConfirmed, int? stateId, 
            string countryId, long? referredByUserId = null)
        {
            CheckForTenant();
            if (_userManager.Users.FirstOrDefault(usr => usr.PhoneNumber == phoneNumber) != null)
            {
                throw new UserFriendlyException("Phone number you entered is taken.");
            }
            var tenant = await GetActiveTenantAsync();

            var user = new User
            {
                Name = "",
                Surname = "",
                TenantId = tenant.Id,
                NickName = nickName,
                PhoneNumber = phoneNumber,
                EmailAddress = emailAddress,
                IsActive = true,
                UserName = emailAddress,
                StateId = stateId,
                CountryId = countryId,
                IsEmailConfirmed = false,
                IsPhoneNumberConfirmed = false,
                Roles = new List<UserRole>(),
                Permalink = nickName.GeneratePermalink()
            };

            user.SetNormalizedNames();

            user.Password = _passwordHasher.HashPassword(user, plainPassword);

            foreach (var defaultRole in await _roleManager.Roles.Where(r => r.IsDefault).ToListAsync())
            {
                user.Roles.Add(new UserRole(tenant.Id, user.Id, defaultRole.Id));
            }
            CheckErrors(await _userManager.CreateAsync(user));
            await _userManager.AddToRoleAsync(user, StaticRoleNames.Tenants.Users);
            //PermissionNames.Pages_Users
            if (referredByUserId != null)
            {
                await _referralRepository.InsertAsync(new Referral
                {
                    UserId = user.Id,
                    UnderUserId = referredByUserId.Value
                });
            }
            await CurrentUnitOfWork.SaveChangesAsync();
            var isEmailConfirmationRequiredForLogin = await GetSettingValueAsync<bool>(AppConsts.SettingsConfirmEmailAfterAccountCreation);
            if (!isEmailConfirmationRequiredForLogin)
            {
                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var confirmation = await _userManager.ConfirmEmailAsync(user, code);
            }

            //Notifications
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.ToUserIdentifier());
            await _appNotifier.SendMessageAsync(new BgJobNotifyUserDto
             {
                 UserId = user.Id,
                 TenantId = AbpSession.TenantId,
                 Message = L("NewUserRegisteredNotificationMessage"),
             });
           
            return user;
        }

        private void CheckForTenant()
        {
            if (!AbpSession.TenantId.HasValue)
            {
                throw new InvalidOperationException("Can not register host users!");
            }
        }

        private async Task<Tenant> GetActiveTenantAsync()
        {
            if (!AbpSession.TenantId.HasValue)
            {
                return null;
            }

            return await GetActiveTenantAsync(AbpSession.TenantId.Value);
        }

        private async Task<Tenant> GetActiveTenantAsync(int tenantId)
        {
            var tenant = await _tenantManager.FindByIdAsync(tenantId);
            if (tenant == null)
            {
                throw new UserFriendlyException(L("UnknownTenantId{0}", tenantId));
            }

            if (!tenant.IsActive)
            {
                throw new UserFriendlyException(L("TenantIdIsNotActive{0}", tenantId));
            }

            return tenant;
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<bool> IsPhoneNumberExists(string modelPhoneNumber)
        {
           return  await _userManager.Users.FirstOrDefaultAsync(user => user.PhoneNumber == modelPhoneNumber) != null;
        }
        
        public async Task<T> GetSettingValueAsync<T>(string key)
        {
            var query = await  _settingsRepository.GetAll().FirstOrDefaultAsync(itm => itm.Name == key);
            if (query == null)
            {
                return default(T);
            }
            var value = query.Value.ConvertValue<T>();
            return value;
        }
    }
}
