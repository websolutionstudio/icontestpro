﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Authorization.Users;
using Abp.Extensions;
using IContestPro.Helpers.DatabaseFieldLengths;
using IContestPro.IContestProEntities;
namespace IContestPro.Authorization.Users
{
    public class User : AbpUser<User>
    {
        [ForeignKey("Country")]
        public string CountryId { get; set; }
        [ForeignKey("State")]
        public int? StateId { get; set; }
        [StringLength(UserFieldsLengths.MaxProfessionLength)]
        public string Profession { get; set; }
        [StringLength(UserFieldsLengths.MaxNickNameLength)]
        public string NickName { get; set; }

        public Country Country { get; set; }
        public State State { get; set; }
        [StringLength(StandardFieldLength.MaxImageLength)]
        public string Image { get; set; }
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string FaceBook { get; set; }
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string LinkedIn { get; set; }
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string Twitter { get; set; }
        [StringLength(StandardFieldLength.MaxPermalinkLength)]
         public string Permalink { get; set; }
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string Instagram { get; set; }

        public const string DefaultPassword = "123qwe";

        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public static User CreateTenantAdminUser(int tenantId, string emailAddress)
        {
            var user = new User
            {
                TenantId = tenantId,
                EmailAddress = emailAddress,
                UserName = "admintenant",
                Name = AdminUserName,
                Surname = AdminUserName,
                NickName = "Admin",
                Profession = "Admin",
                Permalink = "admin-1357-24680"
            };

            user.SetNormalizedNames();

            return user;
        }
    }
}
