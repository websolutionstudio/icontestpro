﻿using Abp.Authorization;
using IContestPro.Authorization.Roles;
using IContestPro.Authorization.Users;

namespace IContestPro.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
