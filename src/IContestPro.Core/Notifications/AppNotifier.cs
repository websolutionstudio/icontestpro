using System.Threading.Tasks;
using Abp;
using Abp.Notifications;
using Hangfire;
using IContestPro.Authorization.Users;
using IContestPro.BackgroundJobs;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.MultiTenancy;

namespace IContestPro.Notifications
{
    public class AppNotifier : DomainServiceBase, IAppNotifier
    {
        private readonly INotificationPublisher _notificationPublisher;
        public AppNotifier(INotificationPublisher notificationPublisher)
        {
            _notificationPublisher = notificationPublisher;
            LocalizationSourceName = IContestProConsts.LocalizationSourceName;
        }

        public async Task WelcomeToTheApplicationAsync(User user)
        {
            /*await _notificationPublisher.PublishAsync(
                AppNotificationNames.NewUserRegistered,
                new MessageNotificationData(L("WelcomeToTheApplicationNotificationMessage")),
                severity: NotificationSeverity.Success,
                userIds: new[] { user.ToUserIdentifier() }
                );*/
        }


        public async Task NewTenantRegisteredAsync(Tenant tenant)
        {
            /*var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "NewTenantRegisteredNotificationMessage",
                    AppNotificationProvider.LocalizationSourceName
                    )
                );

            notificationData["tenancyName"] = tenant.TenancyName;
            await _notificationPublisher.PublishAsync(AppNotificationNames.NewTenantRegistered, notificationData);*/
        }

        //This is for test purposes
        //public async Task SendMessageAsync(UserIdentifier user, string message, NotificationSeverity severity = NotificationSeverity.Info)
        public async Task SendMessageAsync(BgJobNotifyUserDto input)
        {
            var userIdentifier = new UserIdentifier(input.TenantId, input.UserId);
           await _notificationPublisher.PublishAsync(
                "App.SimpleMessage",
                new MessageNotificationData(input.Message),
                severity: input.Severity,
                userIds: new[] { userIdentifier }
            );
          //  BackgroundJob.Enqueue<BgJobNotifyUser>(x => x.Notify(input));

        }
    }
}