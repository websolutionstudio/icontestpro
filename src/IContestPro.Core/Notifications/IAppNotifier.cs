﻿using System.Threading.Tasks;
using Abp;
using Abp.Notifications;
using IContestPro.Authorization.Users;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.MultiTenancy;

namespace IContestPro.Notifications
{
    public interface IAppNotifier
    {
        Task SendMessageAsync(BgJobNotifyUserDto input);
    }
}
