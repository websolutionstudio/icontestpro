namespace IContestPro.Notifications
{
    /// <summary>
    /// Constants for notification names used in this application.
    /// </summary>
    public static class AppNotificationNames
    {
        public const string NewUserRegistered = "App.NewUserRegistered";
        public const string UserUpgradesToSponsor = "App.UserUpgradesToSponsor";
    }
}