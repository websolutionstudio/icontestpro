﻿using Abp.Authorization;
using Abp.Localization;
using Abp.Notifications;
using IContestPro.Authorization;

namespace IContestPro.Notifications
{
    public class AppNotificationProvider : NotificationProvider
    {
       // public const string LocalizationSourceName = "IContestPro";

        public override void SetNotifications(INotificationDefinitionContext context)
        {
            /*context.Manager.Add(
                new NotificationDefinition(
                    AppNotificationNames.NewUserRegistered,
                    displayName: L("NewUserRegisteredNotificationMessage"),
                    permissionDependency: new SimplePermissionDependency(PermissionNames.Pages_Users)
                )
            );
            context.Manager.Add(
                new NotificationDefinition(
                    AppNotificationNames.UserUpgradesToSponsor,
                    displayName: L("UserUpgradesToSponsorNotificationMessage"),
                    permissionDependency: new SimplePermissionDependency(PermissionNames.Pages_Sponsor)
                    )
                );*/
            
        }

        public static ILocalizableString L(string name)
        {
            return new LocalizableString(name,  IContestProConsts.LocalizationSourceName);
        }
    }
}
