﻿namespace IContestPro
{
    public class AppConsts
    {
     public const string IContestProEmail = "no-reply@icontestpro.com";
     public const string IContestProDisplayName = "iContestPRO";
     public const string DomainName = "http://www.icontestpro.com";
     public const string ViewTypeGrid = "grid";
     public const string ViewTypeList = "list";

     /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public const string DefaultPassPhrase = "gsKxGZ012HLL3MI5";
        
        public const string NairaSymbol = "₦";
     
     //Todo: File <ref src="IContestPro.EntityFrameworkCore.Seed.Tenants." />
        public const string DefaultTenantAdminEmail = "admin@defaulttenant.com";
        public const string DefaultTenantAdminUserName = "admintenant";

        /// <summary>
        ///  Pending, Approve-Live, Pause, Disapprove, Expired, Failed, Pending-Closure, Closed
        /// </summary>
        public const string StatusPending = "Pending";
        public const string StatusLive = "Live";
        public const string StatusPaused = "Paused";
        public const string StatusDisapproved = "Disapproved";
        public const string StatusFailed = "Failed";
        public const string StatusSuccess = "Success";
        public const string StatusPendingClosure = "PendingClosure";
        public const string StatusPaymentPending = "PaymentPending";
        public const string StatusPendingPaymentConfirmation = "PendingPaymentConfirmation";
        public const string StatusPaymentConfirmed = "PaymentConfirmed";
        public const string StatusPaymentFailed = "PaymentFailed";
        public const string StatusActive = "Active";
        public const string StatusSuspended = "Suspended";
        public const string StatusPendingPaymentRenewal = "PendingPaymentRenewal";
        public const string StatusPaid = "Paid";
        public const string StatusProcessing = "Processing";
        public const string StatusCancelled = "Cancelled";
        public const string StatusClosed = "Closed";
        public const string StatusContestEntryClosed = "ContestEntryClosed";
        public const string StatusExpired = "ContestEntryClosed";
        public const string StatusNew = "New";
        public const string StatusRead = "Read";
        public const string StatusUnProcessed = "UnProcessed";
        public const string StatusAllocated = "Allocated";
        public const string StatusNotAllocated = "NotAllocated";
        public const string StatusUsed = "Used";
        public const string StatusNotUsed = "NotUsed";
        
        
       /// <summary>
       /// Contest types
       /// </summary>
        public const string ContestTypePaid = "Paid";
        public const string ContestTypeCoded = "Coded";
        public const string ContestTypeFree = "Free";
        
        
         /// <summary>
         /// Contest entry type
        /// </summary>
        public const string ContestEntryTypeImage = "Image";
        public const string ContestEntryTypeVideo = "Video";
        public const string ContestEntryTypeArticle = "Article";
      
       /// <summary>
       /// Vote types
       /// </summary>
        public const string VoteTypePaid = "Paid";
        public const string VoteTypeVotePoint = "VotePoint";
        public const string VoteTypeUnique = "Unique";
        public const string VoteTypeDaily = "Daily";
        public const string VoteTypeHourly = "Hourly";
        public const string VoteTypeOpen = "Open";
        
     /// <summary>
       /// Contest payment periods
       /// </summary>
        public const string PeriodicDaily = "Daily";
        public const string PeriodicWeekly = "Weekly";
        public const string PeriodicTwoWeeks = "TwoWeeks";
        public const string PeriodicMonthly = "Monthly";
        public const string PeriodicTwoMonths = "TwoMonths";
        public const string PeriodicThreeMonths = "ThreeMonths";
        public const string PeriodicSixMonths = "SixMonths";
        public const string PeriodicYearly = "Yearly";


     public const string CookieKeyOpenVoteUserHash = "OpenVoteTokenHash";
     public const string CookieKeyHideSponsorNotification = "CookieKeyHideSponsorNotification";
     public const string IsMobile = "IsMobile";
     public const string OpenVoteTokenHash = "OpenVoteTokenHash";
     public const string PaystackApiBase = "https://api.paystack.co/";
     public const string PaystackApiSecretKey = "sk_test_23e72ae67da0ddcd2fccea1b4fb17b8fa3bf7439";
     public const string PaystackApiPublicKey = "pk_test_e564c540e109248289a7aab6e4209291d2e630fe";
     
     /// <summary>
     /// Deposit types
     /// </summary>
     public const string DepositTypeCardPayment = "CardPayment";
     public const string DepositTypeBankDeposit = "BankDeposit";
     
     public const string NigeriaUniformBankAccountNumberApiKey = "4ea8b8c15128fda4a2e177bc944fce79";
     public const string NigeriaUniformBankAccountNumberUrl = "https://api.bank.codes/ng-nuban/json/" + NigeriaUniformBankAccountNumberApiKey + "/";

     public const string WithdrawalPayInUSD = "USD";
     public const string WithdrawalPayInNAIRA = "NAIRA";
     public const string WithdrawalPayInXPEP = "XPEP";
     
     public const string WithdrawalPayToBank = "Bank";
     public const string WithdrawalPayToPayPal = "PayPal";
     public const string WithdrawalPayToXPEP = "XPEP";


     public const string AppExceptionLogFolder = "/App_Data/AppExceptionLog";
    
     public const string SettingsWithdrawalRequestFeePercentage = "SettingsWithdrawalRequestFeePercentage";
     public const string SettingsWithdrawalMinimumAmount = "SettingsWithdrawalMinimumAmount";
     public const string SettingsTransferFeePercentage = "SettingsTransferFeePercentage";
     public const string SettingsReferralVotePointAward = "SettingsReferralVotePoint";
     public const string SettingsReferrerPercentageIfReferredUserBecomesSponsor = "SettingsReferrerPercentageIfReferredUserBecomesSponsor";
     public const string SettingsShouldPercentageBePaidIfReferredSponsorCreatesFirstContest = "SettingsShouldPercentageBePaidIfReferredSponsorCreatesFirstContest";
     public const string SettingsCodeSize = "SettingsCodeSize";
     public const string SettingsBatchNoSize = "SettingsBatchNoSize";
     public const string SettingsMaximumBatchSize = "SettingsMaximumBatchSize";
     public const string SettingsMaxCodesAssignablePerTime = "SettingsMaxCodesAssignablePerTime";
     public const string SettingsConfirmEmailAfterAccountCreation = "SettingsConfirmEmailAfterAccountCreation";
     public const string SettingsPhoneNumberAfterAccountCreation = "SettingsPhoneNumberAfterAccountCreation";
    
     public const string NotificationMessageDataTypeLocalizable = "Abp.Notifications.LocalizableMessageNotificationData";
     public const string NotificationMessageDataTypeSimple = "Abp.Notifications.MessageNotificationData";
     public const string UserUpgradesToSponsorNotificationMessage = "Thank you for upgrading your account to sponsor. You now have the privileges to create and sponsor contests. iContestPRO will help you creates the visibility for your brand by promoting your contests and reaching out to as many that may want to join.";
     public const string NewUserRegisteredNotificationMessage = "Welcome to iContestPRO. Your account was created successfully.";
     public const int UserIdentityLength = 5;
     
     public const string NotifyExpiringContestsJobId = "e55ca321-fea6-4f25-9130-7157785041e8";
     public const string DisableExpiredContestsJobId = "c92b833c-4c2a-4387-b5af-b09063bccaf1";
     public const string DisableExpiredHomePageBannerJobId = "283bf290-17da-423e-86f8-641a6318886e";
     public const string NotifyExpiringHomePageBannerJobId = "c96c38e2-6c8e-4acc-8f6d-a28c55a690c2";
     public const string DisableExpiredTopBannerJobId = "e755ea24-7385-47fa-a7a4-8004ba21e1f5";
     public const string NotifyExpiringTopBannerJobId = "e2489ddd-0fef-4990-8b14-b441f622f247";
     public const string NotifyExpiringFeaturedContestJobId = "e0a8a368-da81-4e52-b026-f65f79d4bd69";
     public const string DisableExpiredFeaturedContestJobId = "bc62c54b-9a86-4fc1-b103-136806a6368b";
     public const string NotifyExpiringFeaturedContestEntryJobId = "f56a2a41-4bec-450e-b7a5-1ab5629aa4a5";
     public const string DisableExpiredFeaturedContestEntryJobId = "9620e05c-a03f-4a28-9c51-bee2da5545c0";
     public const string NotifyExpiringAdvertBox1JobId = "7e15c4f1-5089-4ad3-b12e-f6e35bc6e241";
     public const string DisableExpiredAdvertBox1JobId = "91c7a2d6-86bb-4abc-a93f-37b9a2337b11";
     public const string NotifyExpiringAdvertBox2JobId = "6ed3c107-5c5c-4013-817f-0316d56b9d43";
     public const string DisableExpiredAdvertBox2JobId = "bcca9596-cfa2-4e3a-941f-05e2d0737cef";

     
     public const string ImageUploadFolder = "images/uploads";
     public const string CookieKeyCurrentUser = "CurrentUserToken";
     public const string CookieKeyCurrentLoginInformations = "CurrentLoginInformations";
     
     /// <summary>
     /// Leader board constants
     /// </summary>
     public const string LeaderBoardDisplayTypeLifetime = "Lifetime";
     public const string LeaderBoardDisplayTypeMonthly = "Monthly";
     public const string LeaderBoardDisplayTypeWeekly = "Weekly";
     
   /// <summary>
     /// Email Templates constants
     /// </summary>
     public const string EmailTemplateNewUserCreation = "NewUserCreation";
     public const string EmailTemplateDefaultEmailTemplate = "DefaultEmailTemplate";
     public const string EmailTemplateResetPasswordEmailTemplate = "ResetPasswordEmailTemplate";
   
     /// <summary>
     /// Home page searches
     /// </summary>
     public const string SearchTypeContests = "Contests";
     public const string SearchTypeEntries = "Entries";
     public const string SearchTypeSponsors = "Sponsors";
     public const string SearchTypeUsers = "Users";

     

    }
 public class MessageType
 {
  public const int Message = 0;
  public const int SupportTicket = 1;
  public const int Dispute = 2;
  public const int All = -1;
 }

 public static class MessageTypeExtension
 {
  public static string GetMessageTypeName(this int type)
  {
   switch (type)
   {
    case MessageType.Message:
     return "Message";
    case MessageType.SupportTicket:
     return "Support Ticket";
    case MessageType.All:
     return "All";
    default:
     return "";
   }
  }
 }
 
 public class MessageFetchType
 {
  public const string Inbox = "inbox";
  public const string Sent = "sent";
 }
   
}
