﻿using Abp.Hangfire;
using Abp.Hangfire.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Timing;
using Abp.Zero;
using Abp.Zero.Configuration;
using Hangfire;
using IContestPro.Authorization.Roles;
using IContestPro.Authorization.Users;
using IContestPro.Configuration;
using IContestPro.Localization;
using IContestPro.MultiTenancy;
using IContestPro.Notifications;
using IContestPro.Timing;

namespace IContestPro
{
    [DependsOn(typeof(AbpZeroCoreModule), typeof (AbpHangfireAspNetCoreModule))]
    public class IContestProCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            // Declare entity types
            Configuration.Modules.Zero().EntityTypes.Tenant = typeof(Tenant);
            Configuration.Modules.Zero().EntityTypes.Role = typeof(Role);
            Configuration.Modules.Zero().EntityTypes.User = typeof(User);

            IContestProLocalizationConfigurer.Configure(Configuration.Localization);

            // Enable this line to create a multi-tenant application.
            Configuration.MultiTenancy.IsEnabled = IContestProConsts.MultiTenancyDisabled;

            // Configure roles
            AppRoleConfig.Configure(Configuration.Modules.Zero().RoleManagement);

            Configuration.Settings.Providers.Add<AppSettingProvider>();
            
            //Adding notification providers
            Configuration.Notifications.Providers.Add<AppNotificationProvider>();
           // Configuration.BackgroundJobs.IsJobExecutionEnabled = true;
            /*Configuration.BackgroundJobs.UseHangfire(configuration =>
            {
                configuration.GlobalConfiguration.UseSqlServerStorage("Default");
            });*/
            Configuration.BackgroundJobs.UseHangfire();      
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(IContestProCoreModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            IocManager.Resolve<AppTimes>().StartupTime = Clock.Now;
        }
    }
}
