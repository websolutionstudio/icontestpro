﻿using System;
using Abp.Domain.Services;

namespace IContestPro
{
  
    public abstract class DomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected DomainServiceBase()
        {
            LocalizationSourceName = IContestProConsts.LocalizationSourceName;
        }
        
        
    }
}