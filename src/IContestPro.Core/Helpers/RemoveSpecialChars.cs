﻿using System;
using System.Text;

namespace IContestPro.Helpers
{
    public static class RemoveSpecialChars
    {
        public static string RemoveSpecialCharacters(this string str)
        {
            var sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        } 
        public static string RemoveSpecialCharactersPreserveSpaces(this string str)
        {
            var sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_'  || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        } 
        public static string GeneratePermalink(this string str)
        {
            str = str.RemoveSpecialCharactersPreserveSpaces();
            str = str.ToLower().Replace(" ", "-");
            //return str + "-" + string.Format("{0:d9}", (DateTime.Now.Ticks / 10) % 1000000000);
            var guid = Guid.NewGuid().ToString("N").Substring(0,20);
            return str + "-" + guid;
        } 
        public static string GeneratePermalinkWithId(this string str, int id)
        {
            str = str.RemoveSpecialCharactersPreserveSpaces();
            str = str.ToLower().Replace(" ", "-");
            return str + "-" + id.ToString().PadLeft(5, '0');
        } 
    }
}