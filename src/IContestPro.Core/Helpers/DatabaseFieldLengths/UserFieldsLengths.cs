﻿namespace IContestPro.Helpers.DatabaseFieldLengths
{
    /// <summary>
    /// This class is used to define the length of fields in the database
    /// </summary>
    public class UserFieldsLengths
    {
        public const int MaxProfessionLength = 255;
        public const int MaxNickNameLength = 50;
        
    }
}