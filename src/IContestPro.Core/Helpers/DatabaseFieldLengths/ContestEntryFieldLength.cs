﻿namespace IContestPro.Helpers.DatabaseFieldLengths
{
    public class ContestEntryFieldLength
    {
        public const int MaxImageLength = 255;
        public const int MaxVideoLength = 255;
        public const int MaxArticleLength = 4000;
        public const int MaxArticleImageLength = 50;
        public const int MaxPosterLength = 50;

    }
}