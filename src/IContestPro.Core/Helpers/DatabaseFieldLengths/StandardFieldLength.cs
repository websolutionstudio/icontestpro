﻿namespace IContestPro.Helpers.DatabaseFieldLengths
{
    public class StandardFieldLength
    {
        public const int MaxImageLength = 50;
        public const int MaxPermalinkLength = 255;
        public const int MaxSocialMediaLength = 255;

    }
}