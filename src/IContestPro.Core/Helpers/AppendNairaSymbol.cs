﻿namespace IContestPro.Helpers
{
    public static class AppendNairaSymbol
    {
        public static string ToNairaString(this string str)
        {
            return "₦" + str;
        } 
        public static string ToNairaString(this decimal str)
        {
            return "₦" + str.ToString("N");
        } 
        
        public static string ToNairaString(this int str)
        {
            return "₦" + str.ToString("N");
        }
        public static string ToNairaString(this double str)
        {
            return "₦" + str.ToString("N");
        } 
        
    }
}