﻿using System;

namespace IContestPro.Helpers
{
    public static class ClassExtensions
    {
        public static string ToAppString(this Guid guid)
        {
                return  guid.ToString("N");
        }
       
        public static object ConvertValue(this object value, Type t)
        {
            var underlyingType = Nullable.GetUnderlyingType(t);

            if (underlyingType != null && value == null)
            {
                return null;
            }
            var basetype = underlyingType == null ? t : underlyingType;
            return Convert.ChangeType(value, basetype);
        }

        public static T ConvertValue<T>(this object value)
        {
            return (T)value.ConvertValue(typeof(T));
        }
        
        public static string ConvertToNearestNumberString(this int numberToConvert)
        {
            if (numberToConvert < 1000){
                return numberToConvert.ToString();
            }
            if (numberToConvert >= 1000 && numberToConvert < 1000000){
                var number = (double)numberToConvert / (double)1000;
                return Math.Round(number, 1) + "K";
            }
            if (numberToConvert >= 1000000 && numberToConvert < 1000000000){
                var number1 = (double)numberToConvert / (double)1000000;
                return Math.Round(number1, 1) + "M";
            }
            if (numberToConvert >= 1000000000){
                var number2 = (double)numberToConvert / (double)1000000000;
                return Math.Round(number2, 1) + "B";
            }
            return "";
        }
        
    }
}