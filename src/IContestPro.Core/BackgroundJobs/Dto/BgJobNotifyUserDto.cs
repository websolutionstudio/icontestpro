﻿using System.ComponentModel.DataAnnotations;
using Abp.Notifications;

namespace IContestPro.BackgroundJobs.Dto
{
    public class BgJobNotifyUserDto
    {
        public BgJobNotifyUserDto()
        {
            Severity = NotificationSeverity.Info;
            TenantId = 1;
        }
        public int? TenantId { get; set; }
        [Required]
        public long UserId { get; set; }
        [Required]
        public string Message { get; set; }
        public NotificationSeverity Severity  { get; set; }
        
    }
}