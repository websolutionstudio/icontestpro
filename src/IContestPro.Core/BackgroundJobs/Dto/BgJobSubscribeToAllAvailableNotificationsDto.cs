namespace IContestPro.BackgroundJobs.Dto
{
    public class BgJobSubscribeToAllAvailableNotificationsDto
    {
        public int TenantId { get; set; }
        public long UserId { get; set; }
    }
}