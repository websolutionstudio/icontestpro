﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SendGrid.Helpers.Mail;

namespace IContestPro.BackgroundJobs.Dto
{
    public class SendEmailDto 
    {
       
        public EmailAddress From { get; set; }
        public bool ShouldUseDefaultEmailTemplate { get; set; }
        [Required]
        public List<EmailAddress> Tos { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Body { get; set; }
    }
}