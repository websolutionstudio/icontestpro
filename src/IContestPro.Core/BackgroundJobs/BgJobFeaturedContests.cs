﻿using Abp.Notifications;
using Castle.Components.DictionaryAdapter;
using Hangfire;
using IContestPro.Authorization.Users;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.IContestProEntities;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace IContestPro.BackgroundJobs
{
    public class BgJobFeaturedContests : BgJobBase
    {
        private readonly IConfiguration _configuration;
        public BgJobFeaturedContests(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        
        public void NotifyUserOfExpiring(User user, FeaturedContest item)
        {
            var apiKey = GetSendGridApiKey(_configuration);
            var client = new SendGridClient(apiKey);
            var input = new SendEmailDto
            {
                Body = $"Your featured contest will be expiring on '{item.EndDate.ToLongDateString()}'. Note: This advert will not be visible after it expires.",
                From = new EmailAddress(AppConsts.IContestProEmail, AppConsts.IContestProDisplayName),
                Subject = "Your featured contest is expiring",
                Tos = new EditableList<EmailAddress>
                {
                    new EmailAddress(user.EmailAddress, user.NickName),
                }
            };
            const bool displayRecipients = false; // set this to true if you want recipients to see each others mail id 
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(input.From, input.Tos, input.Subject, "",
                input.Body, displayRecipients);
            var response =  client.SendEmailAsync(msg);
            //Todo: Notify in the system
            BackgroundJob.Enqueue<BgJobNotifyUser>(x => x.Notify(new BgJobNotifyUserDto
            {
                Message = input.Body,
                TenantId = user.TenantId,
                UserId = user.Id,
                Severity = NotificationSeverity.Error

            }));
        }
        public void NotifyUserOfExpired(User user, FeaturedContest item)
        {
            var apiKey = GetSendGridApiKey(_configuration);
                        var client = new SendGridClient(apiKey);
                        var input = new SendEmailDto
                        {
                            Body = "Your featured contest has expired. Note: It will no longer be visible.",
                            From = new EmailAddress(AppConsts.IContestProEmail, AppConsts.IContestProDisplayName),
                            Subject = "Your featured contest has expired",
                            Tos = new EditableList<EmailAddress>
                            {
                                new EmailAddress(user.EmailAddress, user.NickName),
                            }
                        };
                        const bool displayRecipients = false; // set this to true if you want recipients to see each others mail id 
                        var msg = MailHelper.CreateSingleEmailToMultipleRecipients(input.From, input.Tos, input.Subject, "",
                            input.Body, displayRecipients);
                        var response =  client.SendEmailAsync(msg);
                        //Todo: Notify in the system
                        BackgroundJob.Enqueue<BgJobNotifyUser>(x => x.Notify(new BgJobNotifyUserDto
                        {
                            Message = input.Body,
                            TenantId = user.TenantId,
                            UserId = user.Id,
                            Severity = NotificationSeverity.Error
                        }));
        }
    }
}