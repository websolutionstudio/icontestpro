using System;
using Abp;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Notifications;
using Abp.UI;
using Castle.Components.DictionaryAdapter;
using Hangfire;
using IContestPro.Authorization.Users;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.IContestProEntities;
using IContestPro.Notifications;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace IContestPro.BackgroundJobs
{
    /// <summary>
    /// Backgroud job to notify user when upgrade to sponsor completes successfully.
    /// The int argument is the tenant id of the user
    /// </summary>
    public class BgJobEmailSender : BgJobBase
    {

        private readonly IConfiguration _configuration;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        public BgJobEmailSender(IConfiguration configuration, IRepository<EmailTemplate> emailTemplateRepository)
        {
            _configuration = configuration;
            _emailTemplateRepository = emailTemplateRepository;
        }

        public void Send(SendEmailDto input)
        {
            if (input.From == null)
            {
                input.From = new EmailAddress(AppConsts.IContestProEmail, AppConsts.IContestProDisplayName);
            }

            if (input.ShouldUseDefaultEmailTemplate)
            {
                var template = _emailTemplateRepository.FirstOrDefault(itm => itm.Name == AppConsts.EmailTemplateDefaultEmailTemplate);
                if (template != null)
                {
                    input.Body = template.Template.Replace("[placeholder]", input.Body);
                }
            }
            var apiKey = GetSendGridApiKey(_configuration);
            var client = new SendGridClient(apiKey);
            const bool
                displayRecipients = false; // set this to true if you want recipients to see each others mail id 
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(input.From, input.Tos, input.Subject, "",
                input.Body, displayRecipients);
            var response =  client.SendEmailAsync(msg);
        }

        
    }
}