﻿using Abp.Notifications;
using Castle.Components.DictionaryAdapter;
using Hangfire;
using IContestPro.Authorization.Users;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.IContestProEntities;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace IContestPro.BackgroundJobs
{
    public class BgJobContestProcesses : BgJobBase
    {
        private readonly IConfiguration _configuration;
        public BgJobContestProcesses(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        public void NotifyUserOfExpiringContest(User user, Contest contest)
        {
            var apiKey = GetSendGridApiKey(_configuration);
            var client = new SendGridClient(apiKey);
            var input = new SendEmailDto
            {
                Body = $"Your contest '{contest.Title}' will be expiring on '{contest.EndDate.ToLongDateString()}'. Note: This contest will not be accessible after it expires.",
                From = new EmailAddress(AppConsts.IContestProEmail, AppConsts.IContestProDisplayName),
                Subject = $"Your contest '{contest.Title}' is expiring",
                Tos = new EditableList<EmailAddress>
                {
                    new EmailAddress(user.EmailAddress, user.NickName),
                }
            };
            const bool displayRecipients = false; // set this to true if you want recipients to see each others mail id 
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(input.From, input.Tos, input.Subject, "",
                input.Body, displayRecipients);
            var response =  client.SendEmailAsync(msg);
            //Todo: Notify in the system
            BackgroundJob.Enqueue<BgJobNotifyUser>(x => x.Notify(new BgJobNotifyUserDto
            {
                Message = input.Body,
                TenantId = user.TenantId,
                UserId = user.Id,
                Severity = NotificationSeverity.Error

            }));
        }
        public void NotifyUserOfExpiredContest(User user, Contest contest)
        {
            var apiKey = GetSendGridApiKey(_configuration);
            var client = new SendGridClient(apiKey);
            var input = new SendEmailDto
            {
                Body = $"Your contest '{contest.Title}' has expired on '{contest.EndDate.ToLongDateString()}'. Note: This contest will no longer be accessible.",
                From = new EmailAddress(AppConsts.IContestProEmail, AppConsts.IContestProDisplayName),
                Subject = $"Your contest '{contest.Title}' has expired",
                Tos = new EditableList<EmailAddress>
                {
                    new EmailAddress(user.EmailAddress, user.NickName),
                }
            };
            const bool displayRecipients = false; // set this to true if you want recipients to see each others mail id 
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(input.From, input.Tos, input.Subject, "",
                input.Body, displayRecipients);
            var response =  client.SendEmailAsync(msg);
            //Todo: Notify in the system
            BackgroundJob.Enqueue<BgJobNotifyUser>(x => x.Notify(new BgJobNotifyUserDto
            {
                Message = input.Body,
                TenantId = user.TenantId,
                UserId = user.Id,
                Severity = NotificationSeverity.Error
            }));
        }


        public void NotifyUserOfExpiredTopBanner(User user, AdvertTopBanner item)
        {
            var apiKey = GetSendGridApiKey(_configuration);
                        var client = new SendGridClient(apiKey);
                        var input = new SendEmailDto
                        {
                            Body = $"Your home page banner has expired. Note: It will no longer be visible.",
                            From = new EmailAddress(AppConsts.IContestProEmail, AppConsts.IContestProDisplayName),
                            Subject = $"Your home page banner has expired",
                            Tos = new EditableList<EmailAddress>
                            {
                                new EmailAddress(user.EmailAddress, user.NickName),
                            }
                        };
                        const bool displayRecipients = false; // set this to true if you want recipients to see each others mail id 
                        var msg = MailHelper.CreateSingleEmailToMultipleRecipients(input.From, input.Tos, input.Subject, "",
                            input.Body, displayRecipients);
                        var response =  client.SendEmailAsync(msg);
                        //Todo: Notify in the system
                        BackgroundJob.Enqueue<BgJobNotifyUser>(x => x.Notify(new BgJobNotifyUserDto
                        {
                            Message = input.Body,
                            TenantId = user.TenantId,
                            UserId = user.Id,
                            Severity = NotificationSeverity.Error
                        }));
        }
    }
}