﻿using Abp.Dependency;
using Microsoft.Extensions.Configuration;

namespace IContestPro.BackgroundJobs
{
    public class BgJobBase : ITransientDependency
    {
        
        protected string GetSendGridApiKey(IConfiguration configuration)
        {
            return configuration.GetSection("SENDGRID_API_KEY").Value;
        }
       
       
    }
}