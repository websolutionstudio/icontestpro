﻿using Abp.Notifications;
using Castle.Components.DictionaryAdapter;
using Hangfire;
using IContestPro.Authorization.Users;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.IContestProEntities;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace IContestPro.BackgroundJobs
{
    public class BgJobHomePageBanner : BgJobBase
    {
        private readonly IConfiguration _configuration;
        public BgJobHomePageBanner(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        
        public void NotifyUserOfExpiringHomePageBanner(User user, AdvertHomePageBanner banner)
        {
            var apiKey = GetSendGridApiKey(_configuration);
            var client = new SendGridClient(apiKey);
            var input = new SendEmailDto
            {
                Body = $"Your home page banner advert will be expiring on '{banner.EndDate.ToLongDateString()}'. Note: This advert will not be visible after it expires.",
                From = new EmailAddress(AppConsts.IContestProEmail, AppConsts.IContestProDisplayName),
                Subject = "Your home page banner is expiring",
                Tos = new EditableList<EmailAddress>
                {
                    new EmailAddress(user.EmailAddress, user.NickName),
                }
            };
            const bool displayRecipients = false; // set this to true if you want recipients to see each others mail id 
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(input.From, input.Tos, input.Subject, "",
                input.Body, displayRecipients);
            var response =  client.SendEmailAsync(msg);
            //Todo: Notify in the system
            BackgroundJob.Enqueue<BgJobNotifyUser>(x => x.Notify(new BgJobNotifyUserDto
            {
                Message = input.Body,
                TenantId = user.TenantId,
                UserId = user.Id,
                Severity = NotificationSeverity.Error

            }));
        }
        public void NotifyUserOfExpiredHomePageBanner(User user, AdvertHomePageBanner item)
        {
            var apiKey = GetSendGridApiKey(_configuration);
                        var client = new SendGridClient(apiKey);
                        var input = new SendEmailDto
                        {
                            Body = "Your home page banner has expired. Note: It will no longer be visible.",
                            From = new EmailAddress(AppConsts.IContestProEmail, AppConsts.IContestProDisplayName),
                            Subject = "Your home page banner has expired",
                            Tos = new EditableList<EmailAddress>
                            {
                                new EmailAddress(user.EmailAddress, user.NickName),
                            }
                        };
                        const bool displayRecipients = false; // set this to true if you want recipients to see each others mail id 
                        var msg = MailHelper.CreateSingleEmailToMultipleRecipients(input.From, input.Tos, input.Subject, "",
                            input.Body, displayRecipients);
                        var response =  client.SendEmailAsync(msg);
                        //Todo: Notify in the system
                        BackgroundJob.Enqueue<BgJobNotifyUser>(x => x.Notify(new BgJobNotifyUserDto
                        {
                            Message = input.Body,
                            TenantId = user.TenantId,
                            UserId = user.Id,
                            Severity = NotificationSeverity.Error
                        }));
        }
    }
}