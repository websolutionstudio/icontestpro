using Abp;
using Abp.Dependency;
using Abp.Notifications;
using IContestPro.BackgroundJobs.Dto;
using Microsoft.Extensions.Configuration;

namespace IContestPro.BackgroundJobs
{
    /// <summary>
    /// Backgroud job to notify user when upgrade to sponsor completes successfully.
    /// The int argument is the tenant id of the user
    /// </summary>
    public class BgJobNotifyUser : BgJobBase
    {
        private readonly INotificationPublisher _notificationPublisher;

        public BgJobNotifyUser(INotificationPublisher notificationPublisher)
        {
            _notificationPublisher = notificationPublisher;
        }

        public void Notify(BgJobNotifyUserDto args)
        {
            var userIdentifier = new UserIdentifier(args.TenantId, args.UserId);
             _notificationPublisher.PublishAsync(
                "App.SimpleMessage",
                new MessageNotificationData(args.Message),
                severity: args.Severity,
                userIds: new[] { userIdentifier }
            );
        }
    }
}