﻿using Abp;
using Abp.Dependency;
using Abp.Notifications;
using IContestPro.BackgroundJobs.Dto;

namespace IContestPro.BackgroundJobs
{
    public class BgJobSubscribeToAllAvailableNotifications : BgJobBase
    {
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;

        public BgJobSubscribeToAllAvailableNotifications(INotificationSubscriptionManager notificationSubscriptionManager)
        {
            _notificationSubscriptionManager = notificationSubscriptionManager;
        }

        public void Subscribe(BgJobSubscribeToAllAvailableNotificationsDto args)
        {
            var userIdentifier = new UserIdentifier(args.TenantId, args.UserId);
             _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(userIdentifier);

        }
    }
}