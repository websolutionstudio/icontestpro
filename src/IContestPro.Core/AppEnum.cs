﻿namespace IContestPro
{
    public enum WalletOperation
    {
        Credit,
        Debit,
        NoEffect
    }
    public enum AdvertType
    {
        HomePageBanner = 0,
        TopBanner = 1,
        Contest = 2,
        ContestEntry = 3,
        AdvertBoxOne = 4,
        AdvertBoxTwo = 5,
    }
   public enum GrayColorType
    {
        Red,
        Green,
        Blue
    }
    
    
    public enum MessageTo
    {
        Follower,
        ContestParticipants
    }
   
}