﻿namespace IContestPro
{
    public class AppRoutingConsts
    {
        /// <summary>
        /// This route is used for contest details. Template Contest/{id}
        /// </summary>
        public static string ContestDetails(string permalink) => $"/Contest/{permalink}";
        /// <summary>
        /// This route is used for active entries of all contests. Template Contest/Entries/Active
        /// </summary>
        public static string ContestActiveEntries => "/Contest/Entries/Active";
        /// <summary>
        /// This route is used for entries of a contest. Template Contest/Entries/{id}
        /// </summary>
        public static string ContestEntries(string permalink) => $"/Contest/Entries/{permalink}";
         /// <summary>
        /// This route is used for entry votes of a contest entry. Template Contest/Entry/{id}/Votes
        /// </summary>
        public static string ContestEntryVotes(string permalink) => $"/Contest/Entry/{permalink}/Votes";
         /// <summary>
        /// This route is used to join a contest. Template Contest/Entries/Join/{id}
        /// permalink is the contest permalink
        /// </summary>
        public static string ContestEntriesJoin(string contestPermalink) => $"/Contest/Entries/Join/{contestPermalink}";
        /// <summary>
        /// This route is used to view the details of a contest entry. Template Contest/Entry/{id}
        /// </summary>
        public static string ContestEntryDetails(string permalink) => $"/Contest/Entry/{permalink}";
        /// <summary>
        /// This route is used to view the details of a sponsor. Template Sponsor/{id}
        /// </summary>
        public static string SponsorDetails(string permalink) => $"/Sponsor/{permalink}";
         /// <summary>
        /// This route is used to view the user profile. Template User/{id}
        /// </summary>
        public static string UserProfile(string permalink) => $"/User/{permalink}";


    }
}