﻿namespace IContestPro
{
    public class AppPagedRequestResultDto<T>
    {
        public T Result { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalItemsCount { get; set; }
    }
}