﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;
using IContestPro.Helpers.DatabaseFieldLengths;

namespace IContestPro.IContestProEntities
{
    public class AdvertPrice : FullAuditedEntity, IMayHaveTenant
    {
        public int? TenantId { get; set; }
        public string Name { get; set; }
        public int Days { get; set; }
        public decimal Amount { get; set; }
        public AdvertType Type { get; set; }
    }
}