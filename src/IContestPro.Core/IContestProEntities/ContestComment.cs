﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    /// <summary>
    /// Paid Contest (participants require to pay certain amount to participate) – limited to N500.
    /// Coded Contest (participants shall require to enter certain code to participate).
    /// Free Contest (entrance to participation shall be free)
    /// </summary>
    public class ContestComment : FullAuditedEntity, IMayHaveTenant
    {
        [ForeignKey("User")]
        public long UserId { get; set; }
        [ForeignKey("Contest")]
        public int ContestId { get; set; }
        [StringLength(255)]
        public string Comment { get; set; } 
        public int? TenantId { get; set; }
        
        public User User { get; set; }
        public Contest Contest { get; set; }

    }
}