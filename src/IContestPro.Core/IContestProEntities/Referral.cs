﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;
using IContestPro.Helpers.DatabaseFieldLengths;

namespace IContestPro.IContestProEntities
{
    public class Referral : FullAuditedEntity, IMayHaveTenant
    {
       
        [ForeignKey("User")]
        public long UserId { get; set; }
        [ForeignKey("UnderUser")]
        public long UnderUserId { get; set; }
        public int? TenantId { get; set; }
        public int? ReferrerAwardedVotePoint { get; set; }
        public DateTime? ReferrerAwardedDate { get; set; }
        public double? ReferrerSponsorPercentage { get; set; }
        public decimal? ReferrerSponsorAmount { get; set; }
        public DateTime? ReferrerSponsorDate { get; set; }
        
        public User User { get; set; }
        public User UnderUser { get; set; }

    }
}