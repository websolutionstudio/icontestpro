﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    public class UserVotePoint : FullAuditedEntity, IMayHaveTenant
    {
        
        public int Points { get; set; }
        [ForeignKey("User")]
        public long UserId { get; set; }
        public int? TenantId { get; set; }

        public User User { get; set; }

    }
}