﻿using Abp.Authorization.Users;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    public class BlogResponse : FullAuditedEntity, IMayHaveTenant
    {
        public int BlogId { get; set; }
        public long UserId { get; set; }
        public int StatusId { get; set; }
        public string Message { get; set; }
        public int? TenantId { get; set; }
        
        public Status Status { get; set; }
        public User User { get; set; }
    }
}
