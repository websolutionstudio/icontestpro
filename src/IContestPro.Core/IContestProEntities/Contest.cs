﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    /// <summary>
    /// This module manages setting up Contest Setup/Submission and Approval
    /// Contest Parameters: Title, Picture, Description, Objective, StartDate & End Date,
    /// Qualification Criteria (Free, Fee or Code),
    /// Entrance Format (Picture, Video and Article),
    /// Placeholder Image; mpTarget (minimum number of Participants target – parameter only useful for paid contest);
    /// EPrize (an optional everyone is a winner’s prize;
    /// Vote Option, Contest Category, Contest Tag,
    /// Contest Winning Prize Detail and TotalWorthPrize (in xpep)
    /// </summary>
    public class Contest : FullAuditedEntity, IMayHaveTenant
    {
         [StringLength(255)]
         public string Title { get; set; } 
         [StringLength(50)]
         public string Picture { get; set; } 
         [StringLength(2000)]
         public string Description { get; set; } 
         [StringLength(500)]
         public string Objective { get; set; } 
         public int MinimumParticipants { get; set; } 
         public DateTime StartDate { get; set; } 
         public DateTime EndDate { get; set; } 
         public DateTime EntryStartDate { get; set; } 
         public DateTime EntryEndDate { get; set; } 
         public int? TenantId { get; set; }
         [ForeignKey("Status")]
         public int StatusId { get; set; }
        [StringLength(255)]
        public string EveryoneIsAWinnerUrl { get; set; }
        [StringLength(100)]
        public string Tags { get; set; }
        [StringLength(255)]
        public string Permalink { get; set; } 
        
         public decimal? EntryAmount { get; set; }
        
         [ForeignKey("ContestType")]
         public int ContestTypeId { get; set; }
         [ForeignKey("ContestEntryType")]
         public int ContestEntryTypeId { get; set; }
         [ForeignKey("ContestCategory")]
         public int ContestCategoryId { get; set; }
         [ForeignKey("User")]
         public long UserId { get; set; }
    
         [ForeignKey("VoteType")]
         public int VoteTypeId { get; set; }
        
        [ForeignKey("ContestFee")]
         public int ContestFeeId { get; set; }
         [ForeignKey("Sponsor")]
         public int SponsorId { get; set; }
         public bool IsFeatured { get; set; }
        
         //Interval for payments
         [ForeignKey("Periodic")]
         public int? PeriodicId { get; set; }
         public DateTime? NextPaymentDate { get; set; }
        
        //Amount to pay to cast vote
        public decimal? VoteAmount { get; set; }
        
        public ContestType ContestType { get; set; }
        public ContestEntryType ContestEntryType { get; set; }
        public ContestCategory ContestCategory { get; set; }
        public User User { get; set; }
        public Status Status { get; set; }
        public VoteType VoteType { get; set; }
        public Periodic Periodic { get; set; }
        public ContestFee ContestFee { get; set; }
        public Sponsor Sponsor { get; set; }

    }
}