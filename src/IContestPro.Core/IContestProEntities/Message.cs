﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    public class Message : FullAuditedEntity<int>, IMayHaveTenant
    {
        [ForeignKey("User")]
        public long UserId { get; set; }
        [ForeignKey("ToUser")]
        public long ToUserId { get; set; }
        [StringLength(200)]
        public string Title { get; set; }
        [StringLength(1000)]
        public string MessageText { get; set; }
        public string Attachments { get; set; }
        [ForeignKey("Status")]
        public int StatusId { get; set; }
         public int? TenantId { get; set; }
         public int MessageType { get; set; }
        
        public User User { get; set; }
        public User ToUser { get; set; }
        public Status Status { get; set; }
        public ICollection<MessageResponse> MessageResponses { get; set; }
    }
}