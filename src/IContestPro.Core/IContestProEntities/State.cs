﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace IContestPro.IContestProEntities
{
    public class State : FullAuditedEntity, IMayHaveTenant
    {
        [StringLength(50)]
        public string Name { get; set; }
        public int? TenantId { get; set; }
        
        [ForeignKey("Country")]
        public string CountryId { get; set; }
      
        
        public Country Country { get; set; }

    }
}