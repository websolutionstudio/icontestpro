﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    public class Blog : FullAuditedEntity, IMayHaveTenant
    {
        public string Title { get; set; }
        public long UserId { get; set; }
        public string Content { get; set; }
        public string Tags { get; set; }
        public string Image { get; set; }
        public string Permalink { get; set; }
        public int StatusId { get; set; }
        public int? TenantId { get; set; }
        
        public Status Status { get; set; }
        public User User { get; set; }
    }
}