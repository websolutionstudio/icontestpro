﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;
using IContestPro.Helpers.DatabaseFieldLengths;

namespace IContestPro.IContestProEntities
{
    /// <summary>
    /// Paid Contest (participants require to pay certain amount to participate) – limited to N500.
    /// Coded Contest (participants shall require to enter certain code to participate).
    /// Free Contest (entrance to participation shall be free)
    /// </summary>
    public class ContestEntry : FullAuditedEntity, IMayHaveTenant
    {
        [StringLength(ContestEntryFieldLength.MaxImageLength)]
        public string Image { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxVideoLength)]
        public string Video { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxPosterLength)]
        public string Poster { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxArticleLength)]
        public string Article { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxArticleImageLength)]
        public string ArticleImage { get; set; } 
        [StringLength(StandardFieldLength.MaxPermalinkLength)]
        public string Permalink { get; set; } 
        public int? TenantId { get; set; }
        public bool IsFeatured { get; set; }

        [ForeignKey("User")]
         public long UserId { get; set; }
        [ForeignKey("Status")]
        public int StatusId { get; set; }
        [ForeignKey("Contest")]
        public int ContestId { get; set; }
        
        public User User { get; set; }
        public Contest Contest { get; set; }
        public Status Status { get; set; }

    }
}