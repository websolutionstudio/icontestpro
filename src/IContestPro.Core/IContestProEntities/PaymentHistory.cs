﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    
    public class PaymentHistory : FullAuditedEntity, IMayHaveTenant
    {
      
        [Required]
        [StringLength(500)]
         public string Title { get; set; }
         [StringLength(2000)]
         [Required]
         public string Description { get; set; }
         public string Reference { get; set; }
         public string Response { get; set; }
         public int? TenantId { get; set; }
        [Required]
         [ForeignKey("User")]
         public long UserId { get; set; }
        [Required]
        [ForeignKey("Status")]
        public int StatusId { get; set; }
        [Required]
         public decimal? Amount { get; set; }
         [Required]
         public WalletOperation WalletOperation { get; set; }
            
        public User User { get; set; }
        public Status Status { get; set; }

    }
}