﻿using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace IContestPro.IContestProEntities
{
    /// <summary>
    /// Paid Contest (participants require to pay certain amount to participate) – limited to N500.
    /// Coded Contest (participants shall require to enter certain code to participate).
    /// Free Contest (entrance to participation shall be free)
    /// </summary>
    public class ContestEntryShare : FullAuditedEntity<long>, IMayHaveTenant
    {
        public long? UserId { get; set; }
        public int ContestEntryId { get; set; }
        public int? TenantId { get; set; }
    }
}