﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    public class Wallet : FullAuditedEntity, IMayHaveTenant, ITransactionHistoryBase
    {
        [ForeignKey("User")]
        public long UserId { get; set; }
        public decimal Balance { get; set; }
        public decimal? LastCreditedAmount { get; set; }
        public decimal? LastDebitedAmount { get; set; }
        public int? TenantId { get; set; }

        public User User { get; set; }
    }
}