﻿using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace IContestPro.IContestProEntities
{
    public class PaymentType : FullAuditedEntity, IMayHaveTenant
    {
        public const int NameMaxLength = 50;
        public const int DescriptionMaxLength = 500;
        [Required]
        [StringLength(NameMaxLength)]
        public string Name { get; set; }
        [Required]
        [StringLength(DescriptionMaxLength)]
        public string Description { get; set; }
        public int? TenantId { get; set; }

    }
}