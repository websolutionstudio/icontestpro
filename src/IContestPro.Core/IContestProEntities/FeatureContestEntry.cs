﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    public class FeaturedContestEntry : FullAuditedEntity, IMayHaveTenant
    {
        public int? TenantId { get; set; }
        [ForeignKey("User")]
        [Required]
        public long UserId { get; set; }
        [ForeignKey("ContestEntry")]
        [Required]
        public int ContestEntryId { get; set; }
        [ForeignKey("Status")]
        [Required]
        public int StatusId { get; set; }
        [Required]
        public DateTime StartDate { get; set; } 
        [Required]
        public DateTime EndDate { get; set; } 
        [Required]
        public int AdvertPriceId { get; set; }
        [Required]
        public decimal AmountPaid { get; set; }
        
        
        public User User { get; set; }
        public ContestEntry ContestEntry { get; set; }
        public Status Status { get; set; }

    }
}