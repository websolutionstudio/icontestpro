﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;
using IContestPro.Helpers.DatabaseFieldLengths;

namespace IContestPro.IContestProEntities
{
    public class Sponsor : FullAuditedEntity, IMayHaveTenant
    {
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(60)]
        public string Logo { get; set; }
        [StringLength(60)]
        public string Banner { get; set; }
        [StringLength(255)]
        public string Permalink { get; set; }
        [StringLength(1500)]
        public string Description { get; set; }
        public int? TenantId { get; set; }
        [ForeignKey("User")]
        public long UserId { get; set; }
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string FaceBook { get; set; }
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string LinkedIn { get; set; }
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string Twitter { get; set; }
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string Instagram { get; set; }
        
        
        public User User { get; set; }

    }
}