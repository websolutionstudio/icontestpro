﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    public class MessageResponse : FullAuditedEntity<int>, IMayHaveTenant
    {
        [ForeignKey("User")]
        public long UserId { get; set; }
        public int MessageId { get; set; }
        public string Message { get; set; }
        public string Attachments { get; set; }
        [ForeignKey("Status")]
        public int StatusId { get; set; }
        public int? TenantId { get; set; }
        
        public User User { get; set; }
        public Status Status { get; set; }
    }
}