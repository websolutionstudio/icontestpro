﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    public class BankDetails : FullAuditedEntity, IMayHaveTenant
    {
        [ForeignKey("User")]
        public long UserId { get; set; }
        [ForeignKey("Bank")]
        public int BankId { get; set; }
        [StringLength(100)]
        public string AccountName { get; set; }
        [StringLength(10)]
        public string AccountNumber { get; set; }
        public int? TenantId { get; set; }
        public bool IsDefault { get; set; }

        public User User { get; set; }
        public Bank Bank { get; set; }
    }
}