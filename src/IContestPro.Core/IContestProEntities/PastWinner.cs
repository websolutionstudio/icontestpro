﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;
using IContestPro.Helpers.DatabaseFieldLengths;

namespace IContestPro.IContestProEntities
{
     public class PastWinner : FullAuditedEntity, IMayHaveTenant
    {
        
        [ForeignKey("User")]
        public long UserId { get; set; }
        [ForeignKey("Contest")]
        public int ContestId { get; set; }

        public User User { get; set; }
        public Contest Contest { get; set; }
        public int? TenantId { get; set; }
    }
}