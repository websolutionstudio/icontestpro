﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    /// <summary>
    /// Paid Contest (participants require to pay certain amount to participate) – limited to N500.
    /// Coded Contest (participants shall require to enter certain code to participate).
    /// Free Contest (entrance to participation shall be free)
    /// </summary>
    public class Vote : FullAuditedEntity<long>, IMayHaveTenant
    {
        [ForeignKey("User")]
        public long? UserId { get; set; }
        [ForeignKey("Contest")]
        public int ContestId { get; set; }
        [ForeignKey("ContestEntry")]
        public int ContestEntryId { get; set; }
        public int? TenantId { get; set; }
        [StringLength(50)]
        public string Hash { get; set; }

        public User User { get; set; }
        public Contest Contest { get; set; }
        public ContestEntry ContestEntry { get; set; }
    }
}