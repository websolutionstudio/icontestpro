﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    /// <summary>
    /// Paid Contest (participants require to pay certain amount to participate) – limited to N500.
    /// Coded Contest (participants shall require to enter certain code to participate).
    /// Free Contest (entrance to participation shall be free)
    /// </summary>
    public class Payment : FullAuditedEntity, IMayHaveTenant
    {
        [ForeignKey("Contest")]
        public int ContestId { get; set; }
        [ForeignKey("User")]
        public long UserId { get; set; }
        public decimal Amount { get; set; }
        public int? TenantId { get; set; }
        
        public User User { get; set; }
        public Contest Contest { get; set; }
    }
}