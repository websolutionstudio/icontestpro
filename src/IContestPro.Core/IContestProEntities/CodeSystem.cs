﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;
using IContestPro.Helpers.DatabaseFieldLengths;

namespace IContestPro.IContestProEntities
{
    public class CodeSystem : FullAuditedEntity, IMayHaveTenant
    {
        [Required]
        [StringLength(20)]
        public string Code { get; set; }
        [Required]
        [ForeignKey("Status")]
        public int StatusId { get; set; }
        [Required]
        [StringLength(10)]
        public string BatchNo { get; set; }
        [ForeignKey("AllocatedToUser")]
        public long? AllocatedToUserId { get; set; }
        [ForeignKey("UsedByUser")]
        public long? UsedByUserId { get; set; }
        public DateTime? UsableFrom { get; set; }
        public DateTime? ExpiryDate { get; set; }
        [ForeignKey("Contest")]
        public int? ContestId { get; set; }
        public int? TenantId { get; set; }

        public Status Status { get; set; }
        public User AllocatedToUser { get; set; }
        public User UsedByUser { get; set; }
        public Contest Contest { get; set; }

    }
}