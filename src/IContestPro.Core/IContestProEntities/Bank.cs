﻿using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace IContestPro.IContestProEntities
{
    public class Bank : FullAuditedEntity, IMayHaveTenant
    {
        [StringLength(50)]
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public int? TenantId { get; set; }

    }
}