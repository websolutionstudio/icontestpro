﻿using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace IContestPro.IContestProEntities
{
    /// <summary>
    /// Entry submission can be in format of IMAGES, VIDEO or ARTICLES+IMAGE;
    /// </summary>
    public class ContestEntryType : FullAuditedEntity, IMayHaveTenant
    {
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        public int? TenantId { get; set; }
    }
}