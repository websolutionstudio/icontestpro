﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    public class Withdraw : FullAuditedEntity, IMayHaveTenant, ITransactionHistoryBase
    {
        [ForeignKey("User")]
        public long UserId { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public int? TenantId { get; set; }
        [Required]
        public string PayIn { get; set; }
        [Required]
        public string PayTo { get; set; }
        [ForeignKey("BankDetails")]
        [Required]
        public int BankDetailsId { get; set; }
        
        [ForeignKey("Status")]
        [Required]
        public int StatusId { get; set; }
        [Required]
        public int FeePercentage { get; set; }
        [Required]
        public decimal FeeAmount { get; set; }
        public User User { get; set; }
        public Status Status { get; set; }
        public BankDetails BankDetails { get; set; }
    }
   
    
}