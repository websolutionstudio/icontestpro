﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace IContestPro.IContestProEntities
{
    public class EmailTemplate : FullAuditedEntity, IMayHaveTenant
    {
        public string Name { get; set; }
        public bool IsMainTemplate { get; set; }
        public string Template { get; set; }
        public int? TenantId { get; set; }
    }
}