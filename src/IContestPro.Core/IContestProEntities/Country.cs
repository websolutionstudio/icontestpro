﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace IContestPro.IContestProEntities
{
    public class Country : FullAuditedEntity<string>, IMayHaveTenant
    {
        [StringLength(50)]
        public string Name { get; set; }
        public string Iso { get; set; }
        public int? TenantId { get; set; }

        public ICollection<State> States { get; set; }
    }
}