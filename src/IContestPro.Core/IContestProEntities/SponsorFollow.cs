﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;

namespace IContestPro.IContestProEntities
{
    /// <summary>
    /// Paid Contest (participants require to pay certain amount to participate) – limited to N500.
    /// Coded Contest (participants shall require to enter certain code to participate).
    /// Free Contest (entrance to participation shall be free)
    /// </summary>
    public class SponsorFollow : FullAuditedEntity<long>, IMayHaveTenant
    {
        [ForeignKey("Sponsor")]
        public int SponsorId { get; set; }
        [ForeignKey("FollowerUser")]
        public long FollowerUserId { get; set; }
        public int? TenantId { get; set; }

        public User FollowerUser { get; set; }
        public Sponsor Sponsor { get; set; }
    }
}