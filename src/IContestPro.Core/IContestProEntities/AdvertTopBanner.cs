﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.Authorization.Users;
using IContestPro.Helpers.DatabaseFieldLengths;

namespace IContestPro.IContestProEntities
{
    public class AdvertTopBanner : FullAuditedEntity, IMayHaveTenant
    {
        public int? TenantId { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        [StringLength(100)]
        public string Image { get; set; }
        [Required]
        public int StatusId { get; set; }
        public long UserId { get; set; }
        [Required]
        public decimal AmountPaid { get; set; }
        [Required]
        public int AdvertPriceId { get; set; }
        [Required]
        public string ExternalUrl { get; set; }

        [ForeignKey("StatusId")]
        public Status Status { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
        [ForeignKey("AdvertPriceId")]
        public AdvertPrice AdvertPrice { get; set; }

        
       
        
    }
}