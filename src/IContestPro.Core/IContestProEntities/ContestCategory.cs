﻿using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace IContestPro.IContestProEntities
{
    /// <summary>
    /// Paid Contest (participants require to pay certain amount to participate) – limited to N500.
    /// Coded Contest (participants shall require to enter certain code to participate).
    /// Free Contest (entrance to participation shall be free)
    /// </summary>
    public class ContestCategory : FullAuditedEntity, IMayHaveTenant
    {
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        public int? TenantId { get; set; }
    }
}