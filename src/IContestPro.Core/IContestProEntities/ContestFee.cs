﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace IContestPro.IContestProEntities
{
    public class ContestFee : FullAuditedEntity, IMayHaveTenant
    {
        [ForeignKey("ContestType")]
        public int ContestTypeId { get; set; }
        public int CodeBatch { get; set; }
        public decimal Amount { get; set; }
        
        //The pertcentage of the original amount to be paid every month
        public int PercentagePerMonth { get; set; }
       
        public int? TenantId { get; set; }
        
        public ContestType ContestType { get; set; }

    }
}