﻿using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace IContestPro.IContestProEntities
{
    public class ContestView : FullAuditedEntity<long>, IMayHaveTenant
    {
        public long? UserId { get; set; }
        public int ContestId { get; set; }
        public int? TenantId { get; set; }
    }
}