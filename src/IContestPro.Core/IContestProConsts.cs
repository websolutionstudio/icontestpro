﻿namespace IContestPro
{
    public class IContestProConsts
    {
        public const string LocalizationSourceName = "IContestPro";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
        public const bool MultiTenancyDisabled = false;
    }
}
