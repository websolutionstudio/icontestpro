﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using Abp.Application.Services;
using IContestPro.HttpRequest.Dto;
using Newtonsoft.Json;

namespace IContestPro.HttpRequest
{
     public class HttpRequestAppService : ApplicationService, IHttpRequestAppService
    {

        public static HttpResponseDto<T> Get<T>(string url, Dictionary<string, string> headers = null)
        {
            var responseDto = new HttpResponseDto<T>();
            try
            {
                using (var client = new HttpClient())
                {
                    var request = new HttpRequestMessage
                    {
                        RequestUri = new Uri(url)
                    };
                    if (headers != null)
                    {
                        foreach (var header in headers)
                        {
                            request.Headers.Add(header.Key, header.Value);
                        }
                    }
                
                    var response = client.SendAsync(request).Result;
                    var responseBody = response.Content.ReadAsStringAsync().Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var textReader = new JsonTextReader(new StringReader(responseBody));
                        var result = new JsonSerializer().Deserialize<T>(textReader);
                        responseDto.Result = result;
                        return responseDto;
                    }
                
                    responseDto.Success = false;
                    responseDto.Message = responseBody;
                    return responseDto;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                responseDto.Success = false;
                responseDto.Message = e.Message;
                return responseDto;
            }
           
  
              
           

        }
        public static HttpResponseDto<T> Post<T>(string url, string contentType, string jsonData,  Dictionary<string, string> headers = null)
        {
            var responseDto = new HttpResponseDto<T>();
            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(url)
                };
                if (headers != null)
                {
                    foreach (var header in headers)
                    {
                        request.Headers.Add(header.Key, header.Value);
                    }
                }

                var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                var response = client.PostAsync(url, content).Result;
                var responseBody = response.Content.ReadAsStringAsync().Result;
                if (response.IsSuccessStatusCode)
                {
                    var textReader = new JsonTextReader(new StringReader(responseBody));
                    var result = new JsonSerializer().Deserialize<T>(textReader);
                    responseDto.Result = result;
                    return responseDto;
                }
                
                responseDto.Success = false;
                responseDto.Message = responseBody;
                return responseDto;
                
  
            }

        }
        
        /*
         public static HttpWebResponse SendPostRequest(string data, string url) 
{

    //Data parameter Example
    //string data = "name=" + value

    HttpWebRequest httpRequest = HttpWebRequest.Create(url);
    httpRequest.Method = "POST";
    httpRequest.ContentType = "application/x-www-form-urlencoded";
    httpRequest.ContentLength = data.Length;

    var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
    streamWriter.Write(data);
    streamWriter.Close();

    return httpRequest.GetResponse();
}

public static HttpWebResponse SendGetRequest(string url) 
{

    HttpWebRequest httpRequest = HttpWebRequest.Create(url);
    httpRequest.Method = "GET";

    return httpRequest.GetResponse();
} 
         */
    }
}