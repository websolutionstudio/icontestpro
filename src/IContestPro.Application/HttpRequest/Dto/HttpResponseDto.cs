﻿namespace IContestPro.HttpRequest.Dto
{
    public class HttpResponseDto<T>
    {
        public HttpResponseDto()
        {
            Success = true;
        }
        public bool Success { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }
}