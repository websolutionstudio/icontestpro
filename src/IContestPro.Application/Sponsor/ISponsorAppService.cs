﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.AssetUpload.Dto;
using IContestPro.Base;
using IContestPro.Sponsor.Dto;
using Microsoft.AspNetCore.Http;

namespace IContestPro.Sponsor
{
    public interface ISponsorAppService : IBaseAppService<SponsorDto, int, PagedResultRequestDto, CreateSponsorDto, SponsorDto>
    {
        Task<SponsorDto> GetByUserId(long userId);
        Task<SponsorDetailsDto> GetByPermalink(string permalink);
        Task<ImageUploadDto> UploadProfilePictureAsync(IFormFile file);
        Task<IEnumerable<SponsorComponentDto>> GetRandomSponsors(int totalTake = 18);
    }
}