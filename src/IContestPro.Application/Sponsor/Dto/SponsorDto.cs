﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Helpers.DatabaseFieldLengths;
using IContestPro.Users.Dto;

namespace IContestPro.Sponsor.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Sponsor))]
    public class SponsorDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public const int NameLength = 100;
        public const int LogoLength = 100;
        public const int BannerLength = 100;
        public const int DescriptionLength = 1500;
      
        public const int PermalinkLength = 255;
        [Required]
        [StringLength(NameLength)]
        public string Name { get; set; }
        
        [StringLength(LogoLength)]
        [Required]
        public string Logo { get; set; }
        
        [StringLength(BannerLength)]
        [Required]
        public string Banner { get; set; }
        
        [StringLength(PermalinkLength)]
        public string Permalink { get; set; }
        
        [StringLength(DescriptionLength)]
        public string Description { get; set; }
        
        public int? TenantId { get; set; }
        
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string FaceBook { get; set; }
        
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string LinkedIn { get; set; }
        
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string Twitter { get; set; }
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string Instagram { get; set; }
        
        [Required]
        public long UserId { get; set; }
        
        public UserDto User { get; set; }
        
        
        
    }
}