﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace IContestPro.Sponsor.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Sponsor))]
    public class SponsorDetailsDto : EntityDto
    {
        public string Name { get; set; }
        public string Logo { get; set; }
        public string Banner { get; set; }
        public string Permalink { get; set; }
        public string Description { get; set; }
        public string FaceBook { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public DateTime CreationTime { get; set; }
        public long UserId { get; set; }
        public int TotalFollowers { get; set; }
        
        
        
    }
}