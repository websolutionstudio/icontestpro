﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using IContestPro.Helpers.DatabaseFieldLengths;

namespace IContestPro.Sponsor.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Sponsor))]
    public class CreateSponsorDto
    {
        [Required]
        [StringLength(SponsorDto.NameLength)]
        public string Name { get; set; }
        
        [StringLength(SponsorDto.LogoLength)]
        [Required]
        public string Logo { get; set; }
        
        [StringLength(SponsorDto.BannerLength)]
        [Required]
        public string Banner { get; set; }
        
        [StringLength(SponsorDto.PermalinkLength)]
        public string Permalink { get; set; }
        
        [StringLength(SponsorDto.DescriptionLength)]
        public string Description { get; set; }
        
        public int? TenantId { get; set; }
        
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string FaceBook { get; set; }
        
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string LinkedIn { get; set; }
        
        [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string Twitter { get; set; }
       [StringLength(StandardFieldLength.MaxSocialMediaLength)]
        public string Instagram { get; set; }
        
        [Required]
        public long UserId { get; set; }
    }
}