﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Helpers.DatabaseFieldLengths;
using IContestPro.Users.Dto;

namespace IContestPro.Sponsor.Dto
{
    public class SponsorComponentDto
    {
        public int Id { get; set; }
        public string Logo { get; set; }
        public string Name { get; set; }
        public string Permalink { get; set; }
    }
}