﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using IContestPro.AdvertHomePageBanner.Dto;
using IContestPro.AssetUpload;
using IContestPro.AssetUpload.Dto;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Base;
using IContestPro.EntityFrameworkCore;
using IContestPro.Helpers;
using IContestPro.Sponsor.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.Sponsor
{
    public class SponsorAppService : BaseAppService<IContestProEntities.Sponsor, SponsorDto, int, PagedResultRequestDto, CreateSponsorDto, SponsorDto>, ISponsorAppService
    {
        private readonly IRepository<IContestProEntities.Sponsor> _repository;
        private readonly IAssetUploadAppService _assetUploadAppService;
        private readonly IContestProDbContextFactory _contestProDbContextFactory;

        public SponsorAppService(IRepository<IContestProEntities.Sponsor> repository ,
            IAssetUploadAppService assetUploadAppService,
            IIocResolver  iocResolver, IContestProDbContextFactory contestProDbContextFactory) : base(repository, iocResolver)
        {
            _repository = repository;
            _assetUploadAppService = assetUploadAppService;
            _contestProDbContextFactory = contestProDbContextFactory;
        }
        
        [AbpAuthorize]
        public override async Task<SponsorDto> Create(CreateSponsorDto input)
        {
            var item = ObjectMapper.Map<IContestProEntities.Sponsor>(input);
            item.TenantId = AbpSession.TenantId;
            item.Permalink = input.Name.GeneratePermalink();
            item = await _repository.InsertAsync(item);
            CurrentUnitOfWork.SaveChanges();
             await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
             {
                 UserId = input.UserId,
                 TenantId = AbpSession.TenantId,
                 Message = L("UserUpgradesToSponsorNotificationMessage"),
             });
           
            return MapToEntityDto(item);
        }
        [AbpAuthorize]
        public override async Task<SponsorDto> Update(SponsorDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.Sponsor>());
            UnitOfWorkManager.Current.SaveChanges();
            return item.MapTo<SponsorDto>();
        }
       
        public async Task<SponsorDto> GetByUserId(long userId)
        {
            var company = await _repository.FirstOrDefaultAsync(comp => comp.UserId == userId);
            return ObjectMapper.Map<SponsorDto>(company);
        }

        
        /*
         *
          public string Name { get; set; }
        public string Logo { get; set; }
        public string Banner { get; set; }
        public string Permalink { get; set; }
        public string Description { get; set; }
        public string FaceBook { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
        public DateTime CreationTime { get; set; }
        public long UserId { get; set; }
        public int TotalFollowers { get; set; }
         * 
         */
        
        public async Task<SponsorDetailsDto> GetByPermalink(string permalink)
        {
            using (var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var sponsor = await (
                        from s in context.Sponsors
                        join sf in context.SponsorFollow 
                        on s.Id equals sf.SponsorId
                        where s.Permalink == permalink
                        group sf by new
                        {
                            s.Id,
                            s.Name,
                            s.Logo,
                            s.Banner,
                            s.Permalink,
                            s.Description,
                            s.FaceBook,
                            s.LinkedIn,
                            s.Twitter,
                            s.Instagram,
                            s.CreationTime,
                            s.UserId
                        }
                        into g
                        orderby g.Count() descending
                        select new SponsorDetailsDto
                        {
                            Id = g.Key.Id,
                            Name = g.Key.Name,
                            Logo = g.Key.Logo,
                            Banner = g.Key.Banner,
                            Permalink = g.Key.Permalink,
                            Description = g.Key.Description,
                            FaceBook = g.Key.FaceBook,
                            LinkedIn = g.Key.LinkedIn,
                            Twitter = g.Key.Twitter,
                            Instagram = g.Key.Instagram,
                            CreationTime = g.Key.CreationTime,
                            UserId = g.Key.UserId,
                            TotalFollowers = g.Count()
                        })
                    .FirstOrDefaultAsync();
                /*var company = await _repository.FirstOrDefaultAsync(comp => comp.Permalink == permalink);
                return ObjectMapper.Map<SponsorDto>(company);*/
                return sponsor;
            }
        }
        
        [AbpAuthorize]
        public async Task<ImageUploadDto> UploadProfilePictureAsync(IFormFile file)
        {
            var uploaded = await _assetUploadAppService.UploadImage(new UploadDto{File = file, Path = "images/uploads"});
            return uploaded;
        }
        
        public async Task<IEnumerable<SponsorComponentDto>> GetRandomSponsors(int totalTake = 18)
        {
            var query =  _repository.GetAll().Select(item => new SponsorComponentDto
            {
                Id = item.Id, 
                Logo = item.Logo,
                Permalink = item.Permalink,
                Name = item.Name
            });
            query = query.OrderBy(item => Guid.NewGuid()).Take(totalTake);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return entities;
        } 
    }
}