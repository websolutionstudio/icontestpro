﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.AdvertPrice.Dto;
using IContestPro.Base;

namespace IContestPro.AdvertPrice
{
    public interface IAdvertPriceAppService : IBaseAppService<AdvertPriceDto, int, PagedResultRequestDto, CreateAdvertPriceDto, AdvertPriceDto>
    {
        Task<AdvertPriceDto> GetByName(string name);

        Task<PagedResultDto<AdvertPriceDto>> GetAllByType(AdvertType type, PagedResultRequestDto input);
    }
}