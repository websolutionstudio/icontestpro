﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.AdvertPrice.Dto
{
    [AutoMapTo(typeof(IContestProEntities.AdvertPrice))]
    public class CreateAdvertPriceDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int Days { get; set; }
        public decimal Amount { get; set; }
        [Required]
        public AdvertType Type { get; set; }


    }
}