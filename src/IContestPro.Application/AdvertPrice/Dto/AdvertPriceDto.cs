﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;

namespace IContestPro.AdvertPrice.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.AdvertPrice))]
    public class AdvertPriceDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public string Name { get; set; }
        public int Days { get; set; }
        public decimal Amount { get; set; }
        public int? TenantId { get; set; }
        public AdvertType Type { get; set; }

    }
}