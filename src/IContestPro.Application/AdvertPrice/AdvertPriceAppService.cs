﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.AdvertPrice.Dto;
using IContestPro.Base;

namespace IContestPro.AdvertPrice
{
    [AbpAuthorize]
     public class AdvertPriceAppService : BaseAppService<IContestProEntities.AdvertPrice, AdvertPriceDto, int,
                                PagedResultRequestDto, CreateAdvertPriceDto, AdvertPriceDto>, IAdvertPriceAppService
    {
        private readonly IRepository<IContestProEntities.AdvertPrice> _repository;

        public AdvertPriceAppService(IRepository<IContestProEntities.AdvertPrice> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }

        public async Task<AdvertPriceDto> GetByName(string name)
        {
            return  (await _repository.FirstOrDefaultAsync(itm => itm.Name == name))?.MapTo<AdvertPriceDto>();
        }

        public override async Task<PagedResultDto<AdvertPriceDto>> GetAll(PagedResultRequestDto input)
        {
           
            var query =  _repository.GetAll();
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.Type).ThenBy(item => item.Days);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            
            return new PagedResultDto<AdvertPriceDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        } public async Task<PagedResultDto<AdvertPriceDto>> GetAllByType(AdvertType type, PagedResultRequestDto input)
        {
           
            var query =  _repository.GetAll().Where(item => item.Type == type);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.Name);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            
            return new PagedResultDto<AdvertPriceDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
    }
}