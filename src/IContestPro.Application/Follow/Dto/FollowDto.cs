﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Users.Dto;

namespace IContestPro.Follow.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Follow))]
    public class FollowDto : FullAuditedEntityDto<long>, IMayHaveTenant
    {
        [Required]
        public long FollowerUserId { get; set; }
        [Required]
        public long FollowingUserId { get; set; }

        public int? TenantId { get; set; }

        public UserDto FollowerUser { get; set; }
        public UserDto FolloweringUser { get; set; }
    }
}