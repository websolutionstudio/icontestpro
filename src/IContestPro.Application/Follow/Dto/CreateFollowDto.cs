﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Follow.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Follow))]
    public class CreateFollowDto
    {
        /// <summary>
        /// FollowerUserId is the id of the user following me
        /// </summary>
        [Required]
        
        public long FollowerUserId { get; set; }
        
        /// <summary>
        /// FollowingUserId is the id of the user to follow
        /// </summary>
        [Required]
        public long FollowingUserId { get; set; }
    }
}