﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using IContestPro.Authorization;
using IContestPro.Base;
using IContestPro.EntityFrameworkCore;
using IContestPro.EntityFrameworkCore.StoreProcedureManager;
using IContestPro.Follow.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Follow
{
    public class FollowAppService : BaseAppService<IContestProEntities.Follow, FollowDto, long, PagedResultRequestDto, CreateFollowDto, FollowDto>, IFollowAppService
    {
        private readonly IRepository<IContestProEntities.Follow, long> _repository;
        private readonly IContestProDbContextFactory _contestProDbContextFactory;

        public FollowAppService(IRepository<IContestProEntities.Follow, long> repository, IIocResolver  iocResolver, IContestProDbContextFactory contestProDbContextFactory) : base(repository, iocResolver)
        {
            _repository = repository;
            _contestProDbContextFactory = contestProDbContextFactory;
        }

        [AbpAuthorize]
        public override async Task<FollowDto> Create(CreateFollowDto input)
        {
            if (input.FollowerUserId == 0 || input.FollowingUserId == 0)
            {
                throw new UserFriendlyException("Follower user and Followering user are required.");
            } 
            if (input.FollowerUserId == input.FollowingUserId)
            {
                throw new UserFriendlyException("You cannot follow yourself.");
            }
            if (await IsUserFollowingUser(input.FollowerUserId, input.FollowingUserId))
            {
                throw new UserFriendlyException("You are already following this user.");
            }
             

            return await base.Create(input);
        }

        public async Task<int> CountFollowersByUserId(long userId)
        {
            return await _repository.CountAsync(item => item.FollowingUserId == userId);
        }

        public async Task<int> CountFollowingByUserId(long userId)
        {
            return await _repository.CountAsync(item => item.FollowerUserId == userId);
        }
        
        public async Task<bool> IsUserFollowingUser(long isUserId, long followingUserId )
        {
            return await _repository.CountAsync(item => item.FollowerUserId == isUserId && item.FollowingUserId == followingUserId) > 0;
        }
        [AbpAuthorize]
        public async Task<IEnumerable<CurrentUserFollowersOrFollowingDto>> GetCurrentUserFollowers(
            string nickNameSuggestion = "", int pageSize = 20)
        {
            var userId = AbpSession.GetUserId();
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var isAdmin = await IsGrantedAsync(PermissionNames.Pages_Admin);
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetCurrentUserFollowers)
                    .WithSqlParam("UserId", userId)
                    .WithSqlParam("NickNameSuggestion", nickNameSuggestion)
                    .WithSqlParam("PageSize", pageSize)
                    .WithSqlParam("IsAdmin", isAdmin);
                var items = await cmd.ExecuteStoredProcedureListAsync<CurrentUserFollowersOrFollowingDto>();
                return items;
            }
        }
    }
}