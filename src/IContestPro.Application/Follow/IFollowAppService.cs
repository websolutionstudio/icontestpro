﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Follow.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Follow
{
    public interface IFollowAppService : IBaseAppService<FollowDto, long, PagedResultRequestDto, CreateFollowDto, FollowDto>
    {
        Task<int> CountFollowersByUserId(long userId);
        Task<int> CountFollowingByUserId(long userId);
        Task<bool> IsUserFollowingUser(long isUserId, long followingUserId);
        Task<IEnumerable<CurrentUserFollowersOrFollowingDto>> GetCurrentUserFollowers(
            string nickNameSuggestion = "",int pageSize = 20);

    }
}