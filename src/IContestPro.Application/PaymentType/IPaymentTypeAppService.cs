﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.PaymentType.Dto;

namespace IContestPro.PaymentType
{
    public interface IPaymentTypeAppService : IBaseAppService<PaymentTypeDto, int, PagedResultRequestDto, CreatePaymentTypeDto, PaymentTypeDto>
    {
        Task<PaymentTypeDto> GetByName(string name);
    }
}