﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Authorization;
using IContestPro.Base;
using IContestPro.PaymentType.Dto;

namespace IContestPro.PaymentType
{
    public class PaymentTypeAppService : BaseAppService<IContestProEntities.PaymentType, PaymentTypeDto, int,
                                PagedResultRequestDto, CreatePaymentTypeDto, PaymentTypeDto>, IPaymentTypeAppService
    {
        private readonly IRepository<IContestProEntities.PaymentType> _repository;

        public PaymentTypeAppService(IRepository<IContestProEntities.PaymentType> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }

        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<PaymentTypeDto> Create(CreatePaymentTypeDto input)
        {
           var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.PaymentType>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<PaymentTypeDto> Update(PaymentTypeDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.PaymentType>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }

       
        public async Task<PaymentTypeDto> GetByName(string name)
        {
            return  (await _repository.FirstOrDefaultAsync(itm => itm.Name == name)).MapTo<PaymentTypeDto>();
        }
    }
       
    
}