﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;

namespace IContestPro.PaymentType.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.PaymentType))]
    public class PaymentTypeDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public int? TenantId { get; set; }
    }
}