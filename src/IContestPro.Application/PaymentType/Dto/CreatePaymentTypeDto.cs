﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.PaymentType.Dto
{
    [AutoMapTo(typeof(IContestProEntities.PaymentType))]
    public class CreatePaymentTypeDto
    {
        public const int NameMaxLength = IContestProEntities.PaymentType.NameMaxLength;
        public const int DescriptionMaxLength = IContestProEntities.PaymentType.DescriptionMaxLength;
        [StringLength(NameMaxLength)]
        [Required]
        public string Name { get; set; }
        [StringLength(DescriptionMaxLength)]
        [Required]
        public string Description { get; set; }
    }
}