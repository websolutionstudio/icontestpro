﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.EmailTemplate.Dto
{
    [AutoMapTo(typeof(IContestProEntities.EmailTemplate))]
    public class CreateEmailTemplateDto
    {
        [Required]
        public string Name { get; set; }
        public bool IsMainTemplate { get; set; }
        [Required]
        public string Template { get; set; }
    }
}