﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;

namespace IContestPro.EmailTemplate.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.EmailTemplate))]
    public class EmailTemplateDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public string Name { get; set; }
        public bool IsMainTemplate { get; set; }
        public string Template { get; set; }
        public int? TenantId { get; set; }
    }
}