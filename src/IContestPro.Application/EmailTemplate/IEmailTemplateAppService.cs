﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.EmailTemplate.Dto;

namespace IContestPro.EmailTemplate
{
    public interface IEmailTemplateAppService : IBaseAppService<EmailTemplateDto, int, PagedResultRequestDto, CreateEmailTemplateDto, EmailTemplateDto>
    {
        Task<EmailTemplateDto> GetEmailTemplateByName(string name);
    }
}