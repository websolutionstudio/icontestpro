﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Base;
using IContestPro.EmailTemplate.Dto;

namespace IContestPro.EmailTemplate
{
    public class EmailTemplateAppService : BaseAppService<IContestProEntities.EmailTemplate, EmailTemplateDto, int, PagedResultRequestDto, CreateEmailTemplateDto, EmailTemplateDto>, IEmailTemplateAppService
    {
        private readonly IRepository<IContestProEntities.EmailTemplate> _repository;

        public EmailTemplateAppService(IRepository<IContestProEntities.EmailTemplate> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }

        [AbpAuthorize]
        public override async Task<EmailTemplateDto> Create(CreateEmailTemplateDto input)
        {
            return await base.Create(input);
        }

        public async Task<EmailTemplateDto> GetEmailTemplateByName(string name)
        {
            var item = await _repository.FirstOrDefaultAsync(itm => itm.Name == name);
            return item != null ? MapToEntityDto(item) : null;
        }
    }
}