﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Periodic.Dto;

namespace IContestPro.Periodic
{
    public interface IPeriodicAppService : IBaseAppService<PeriodicDto, int, PagedResultRequestDto, CreatePeriodicDto, PeriodicDto>
    {
        Task<PeriodicDto> GetByName(string name);
    }
}