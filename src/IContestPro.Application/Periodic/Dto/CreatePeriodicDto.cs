﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Periodic.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Periodic))]
    public class CreatePeriodicDto
    {
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
    }
}