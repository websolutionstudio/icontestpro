﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Authorization;
using IContestPro.Base;
using IContestPro.Periodic.Dto;

namespace IContestPro.Periodic
{
   
    public class PeriodicAppService : BaseAppService<IContestProEntities.Periodic, PeriodicDto, int,
                                PagedResultRequestDto, CreatePeriodicDto, PeriodicDto>, IPeriodicAppService
    {
        private readonly IRepository<IContestProEntities.Periodic> _repository;

        public PeriodicAppService(IRepository<IContestProEntities.Periodic> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }
        
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<PeriodicDto> Create(CreatePeriodicDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.Periodic>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<PeriodicDto> Update(PeriodicDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.Periodic>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }

        public async Task<PeriodicDto> GetByName(string name)
        {
            return  (await _repository.FirstOrDefaultAsync(itm => itm.Name == name)).MapTo<PeriodicDto>();
        }
    }
}