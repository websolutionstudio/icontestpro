﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.BankDetails.Dto
{
    [AutoMapTo(typeof(IContestProEntities.BankDetails))]
    public class CreateBankDetailsDto
    {
        [Required]
        public long UserId { get; set; }
        [Required]
        public int BankId { get; set; }
        [Required]
        [StringLength(100)]
        public string AccountName { get; set; }
        [Required]
        [StringLength(10)]
        public string AccountNumber { get; set; }
        public bool IsDefault { get; set; }

    }
}