﻿namespace IContestPro.BankDetails.Dto
{
    public class BankAccountValidateResponseDto
    {
        public string nuban { get; set; }
        public string bank { get; set; }
        public string bank_code { get; set; }
        public int checksum { get; set; }
        
        public bool valid { get; set; }
        public int error { get; set; }
        public string message { get; set; }
    }
}