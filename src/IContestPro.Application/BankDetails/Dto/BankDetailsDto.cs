﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Bank.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.BankDetails.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.BankDetails))]
    public class BankDetailsDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public long UserId { get; set; }
        public int BankId { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public bool IsDefault { get; set; }
        public int? TenantId { get; set; }

        public UserDto User { get; set; }
        public BankDto Bank { get; set; }
    }
}