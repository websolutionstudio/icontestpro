﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using IContestPro.Base;
using IContestPro.BankDetails.Dto;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.BankDetails
{
    [AbpAuthorize]
     public class BankDetailsAppService : BaseAppService<IContestProEntities.BankDetails, BankDetailsDto, int,
                                PagedResultRequestDto, CreateBankDetailsDto, BankDetailsDto>, IBankDetailsAppService
    {
        private readonly IRepository<IContestProEntities.BankDetails> _repository;

        public BankDetailsAppService(IRepository<IContestProEntities.BankDetails> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }

        public override async Task<BankDetailsDto> Create(CreateBankDetailsDto input)
        {
            if (await _repository.CountAsync(itm => itm.AccountNumber == input.AccountNumber) > 0)
            {
                throw new UserFriendlyException("Account number already exists.");
            }
            input.UserId = AbpSession.GetUserId();
            var defaultAccount =  await _repository.GetAll().Where(itm => itm.UserId == input.UserId && itm.IsDefault).ToListAsync();
            if (defaultAccount == null || defaultAccount.Count == 0)
            {
                input.IsDefault = true;
            }
            else
            {
                if (input.IsDefault)
                {
                    defaultAccount.ForEach(account =>
                    {
                        account.IsDefault = false;
                    });
                   await CurrentUnitOfWork.SaveChangesAsync();
                }
            }
            var item =  await base.Create(input);
            return item;

        }
        //Task<TEntityDto> Update(TUpdateInput input);
        public override async Task<BankDetailsDto> Update(BankDetailsDto input)
        {
            input.UserId = AbpSession.GetUserId();
            input.TenantId = AbpSession.GetTenantId();
            var defaultAccounts =  await _repository.GetAll().Where(itm => itm.UserId == input.UserId && itm.IsDefault).ToListAsync();
            if (defaultAccounts == null || defaultAccounts.Count == 0)
            {
                input.IsDefault = true;
            }
            else
            {
                if (input.IsDefault)
                {
                    defaultAccounts.ForEach(account =>
                    {
                        account.IsDefault = false;
                    });
                }
            }

            IContestProEntities.BankDetails item = null;
            if (defaultAccounts != null)
            {
                 item = defaultAccounts.FirstOrDefault(itm => itm.Id == input.Id);
                if (item != null)
                {
                    item.UserId = input.UserId;
                    item.BankId = input.BankId;
                    item.AccountName = input.AccountName;
                    item.AccountNumber = input.AccountNumber;
                    item.IsDefault = input.IsDefault;
                    item.TenantId = input.TenantId;
                }
            }
            //
            if (item != null)
            {
                item = await _repository.UpdateAsync(item);
            }
            else
            {
                item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.BankDetails>());
            }
            await CurrentUnitOfWork.SaveChangesAsync();

            return item.MapTo<BankDetailsDto>();

        }
        
        public override async Task<BankDetailsDto> Get(EntityDto<int> input)
        {
            var query = await _repository.GetAllIncluding(item => item.Bank).FirstOrDefaultAsync(item => item.Id == input.Id);
            return query != null ? query.MapTo<BankDetailsDto>() : null;
        }

        public override async Task<PagedResultDto<BankDetailsDto>> GetAll(PagedResultRequestDto input)
        {
            var query = _repository.GetAllIncluding(item => item.Bank);
            query = query.OrderByDescending(item => item.CreationTime);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<BankDetailsDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        
        public async Task<PagedResultDto<BankDetailsDto>> GetByUserId(long userId, PagedResultRequestDto input)
        {
            var query = _repository.GetAllIncluding(item => item.Bank).Where(itm => itm.UserId == userId);
            query = query.OrderByDescending(item => item.CreationTime);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<BankDetailsDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        public async Task<BankDetailsDto> GetUserDefaultBankAccount(long userId)
        {
            return  (await _repository.FirstOrDefaultAsync(itm => itm.UserId == userId && itm.IsDefault))?.MapTo<BankDetailsDto>();
        }

        public async Task<bool> AcountExists(string accountNumber)
        {
            return  await _repository.CountAsync(itm => itm.AccountNumber == accountNumber) > 0;

        }
    }
}