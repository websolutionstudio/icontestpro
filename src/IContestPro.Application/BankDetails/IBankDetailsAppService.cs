﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.BankDetails.Dto;

namespace IContestPro.BankDetails
{
    public interface IBankDetailsAppService : IBaseAppService<BankDetailsDto, int, PagedResultRequestDto, CreateBankDetailsDto, BankDetailsDto>
    {
        Task<PagedResultDto<BankDetailsDto>> GetByUserId(long userId, PagedResultRequestDto input);
        Task<BankDetailsDto> GetUserDefaultBankAccount(long userId);
        Task<bool> AcountExists(string accountNumber);
    }
}