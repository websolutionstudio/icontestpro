﻿using System.Collections.Generic;

namespace IContestPro.Logging.Dto
{
    public class SlackDebugDto
    {
        public string username { get; set; }
        public string icon_emoji { get; set; }
        public List<Dictionary<string,string>> attachments { get; set; }
        
        /*
        $data = ['username' => 'La Verita Bot', 'icon_emoji' => ':rat:',
        'attachments' => [
            [
                'fallback' => "$tag",
                'color' => '#205081',
                'author_name' => 'La Veritas - ' . $environment_name,
                'title' => "$tag",
                'text' => $text
            ]
        ]];
        */
    }
}