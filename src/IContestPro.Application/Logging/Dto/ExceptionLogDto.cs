﻿using System;

namespace IContestPro.Logging.Dto
{
    public class ExceptionLogDto
    {
        public DateTime Date { get; set; }
        public string UserName { get; set; }
        public string Ip { get; set; }
        public string HostName { get; set; }
        public string RequestUrl { get; set; }
        public string ErrorType { get; set; }
        public string InnerException { get; set; }
        public string ExceptionType { get; set; }
        public string ExceptionMessage { get; set; }
        public string SourceFunctionName { get; set; }
        public string SystemSource { get; set; }
        public string StackTrace { get; set; }
    }
}