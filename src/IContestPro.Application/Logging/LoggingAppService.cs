﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using IContestPro.Logging.Dto;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using PagedList;

namespace IContestPro.Logging
{
    public class LoggingAppService : ILoggingAppService
    {
        private const string LogFileNameOnly = @"LogFile";
        private const string LogFileExtension = @".txt";

        private const string DateTimeFormat = @"dd/MM/yyyy HH:mm:ss";
        private static readonly object LogLock = new object();
        private static string _logFileFolder;
        private static int _maxLogSize = 1000000;
        private static string _logFileName;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// Constructor
        /// </summary>
        public LoggingAppService(IHostingEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            _hostingEnvironment = hostingEnvironment;
            _httpContextAccessor = httpContextAccessor;
            // If we have no http context current then assume testing mode i.e. log file in run folder
            //_logFileFolder = HttpContext.Current != null ? HttpContext.Current.Server.MapPath(LogFileDirectory) : @".";
            _logFileFolder = _hostingEnvironment.ContentRootPath + AppConsts.AppExceptionLogFolder;
            _logFileName = MakeLogFileName(false);

        }

        #region Private static methods

        /// <summary>
        /// Generate a full log file name
        /// </summary>
        /// <param name="isArchive">If this an archive file, make the usual file name but append a timestamp</param>
        /// <returns></returns>
        private static string MakeLogFileName(bool isArchive)
        {
            return !isArchive
                ? string.Format("{0}/{1}{2}", _logFileFolder, LogFileNameOnly, LogFileExtension)
                : string.Format("{0}/{1}_{2}{3}", _logFileFolder, LogFileNameOnly,
                    DateTime.UtcNow.ToString("ddMMyyyy_hhmmss"), LogFileExtension);
        }

        public void LogException(Exception ex)
        {
            try
            {
                var httpException = ex as HttpException ?? new HttpException(500, "Internal Server Error", ex);
                var sw = new StringBuilder();
                /*EXCEPTION LOG FORMAT
           Date | User | IP | Host Name | Url | Error Type | Inner Exception | Exception Type | 
            Exception Message | Source Function Name | System Source | Stack trace
            */

                //Current User
                var currentUser = 
                sw.Append(_httpContextAccessor.HttpContext.User?.Identity?.Name);
                sw.Append("|");
                //User IP
                try
                {
                    sw.Append(_httpContextAccessor.HttpContext?.Connection?.RemoteIpAddress);
                }
                catch (Exception)
                {
                    //Console.WriteLine(EX_NAME);
                }
                sw.Append("|");
                //User Hostname
                try
                {
                    sw.Append(_httpContextAccessor.HttpContext?.Connection?.RemoteIpAddress);
                }
                catch (Exception)
                {
                    // ignored
                }
                sw.Append("|");
                //Request Url
                sw.Append(_httpContextAccessor.HttpContext.Request.Path);
                sw.Append("|");
                //Error Type
                sw.Append(httpException.GetHttpCode.ToString());
                sw.Append("|");
                var innerExceptionLoopCounter = 0;
                var inner = ex.InnerException;
                while (inner != null && innerExceptionLoopCounter++ <= 5)
                {
                    sw.Append("Inner Exception: " + inner.Message + "<br />");
                    inner = inner.InnerException;
                }
                sw.Append("|");
                //Exception Type
                sw.Append(ex.GetType());
                sw.Append("|");
                //Exception Message
                sw.Append(ex.Message);
                sw.Append("|");
                //Source Function Name
                sw.Append(ex.TargetSite.Name);
                sw.Append("|");
                //System Source
                sw.Append(ex.Source);
                sw.Append("|");
                // //Get a StackTrace object for the exception
                //    StackTrace st = new StackTrace(ex, true);
                //// Get the first stack frame
                //StackFrame frame = st.GetFrame(0);
                ////Get the file name
                //var fileName = frame.GetFileName(); //returns null
                //sw.Append("FileName: "+ fileName);
                //   //Get the method name
                //   var methodName = frame.GetMethod().Name; //returns PermissionDemand
                //sw.Append("MethodName: " + methodName);
                ////Get the line number from the stack frame
                //var line = frame.GetFileLineNumber(); //returns 0
                //sw.Append("Line Number: " + line);
                ////Get the column number
                //var column = frame.GetFileColumnNumber(); //returns 0  
                //sw.Append("Column: " + column);
                var stack = ex.StackTrace.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None)[0].Trim();
                sw.Append(stack);
                //Strip any new line added in the process
                sw = sw.Replace("\r\n", "").Replace("\n", "");
                WriteErrorToFile(sw.ToString());

                /************************************************************************
               ** POST MESSAGE TO SLACK
               ***********************************************************/

                var attachmentList = new List<Dictionary<string, string>>();
                var attachment = new Dictionary<string, string>();
                attachment.Add("fallback",HttpUtility.UrlEncode(ex.GetType().ToString()));
                attachment.Add("color", HttpUtility.UrlEncode("#e01212"));
                attachment.Add("title","User:\n"+ _httpContextAccessor.HttpContext.User?.Identity?.Name+"\nException Type:\n"+ ex.GetType());
                attachment.Add("text",new StringBuilder("Exception Message:\n " + ex.Message + ",\nInner Exception:\n" + 
                                                        ex.InnerException?.Message +"\nUrl:\n" + _httpContextAccessor.HttpContext.Request.Path + 
                                                        "\nStack:\n"+ stack).ToString());
                attachmentList.Add(attachment);
                var slackModel = new SlackDebugDto
                {
                    attachments = attachmentList
                };
                
                var jsonString = JsonConvert.SerializeObject(slackModel);
                  var response=   MakeSlackPostAsync(jsonString);
                // var URI = "https://hooks.slack.com/services/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";
                //using (var wc = new WebClient())
                //{
                //    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                //    var htmlResult = wc.UploadString(URI, jsonString);
                //}

            }
            catch (Exception)
            {
                //Ignore
            }
            //return new Task(() => {});
        }
        private async Task<string> MakeSlackPostAsync(string jsonString)
        {
            var httpClient = new HttpClient();
            var URI = "https://hooks.slack.com/services/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";
           HttpResponseMessage response;
            string responseContent=String.Empty;
            try
            {
                  response = await httpClient.PostAsync(URI, new StringContent(jsonString)).ConfigureAwait(false);
                var status = (int)response.StatusCode;
                 responseContent = await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException hre)
            {
               // Android.Util.Log.Error("MyApplication", hre.Message);
            }
            return responseContent;
        }

        /// <summary>
        /// Perform the write. Thread-safe.
        /// </summary>
        /// <param name="message"></param>
        private static void WriteErrorToFile(string message)
        {
            if (message != "File does not exist.")
            {
                try //LogFileDirectory ;
                {
                    if (!Directory.Exists(_logFileFolder))
                    {
                        Directory.CreateDirectory(_logFileFolder);
                    }
                    if (!File.Exists(_logFileName))
                    {
                        using (File.Create(_logFileName))
                        {
                        }
                    }
                    // Note there is a lock here. This class is only suitable for error logging,
                    // not ANY form of trace logging...
                    lock (LogLock)
                    {
                        var fs = File.OpenRead(_logFileName);
                        if (fs.Length >= _maxLogSize)
                        {
                            fs.Close();
                            ArchiveLog();
                        }
                        fs.Close();
                        using (var tw = TextWriter.Synchronized(File.AppendText(_logFileName)))
                        {
                            //Msg format
                            //Date, Message, Dll, Action, File name, Line number
                            tw.WriteLine("{0} | {1}", DateTime.UtcNow.ToString(DateTimeFormat), message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    var testing = ex;
                    // Not much to do if logging failed...
                }

            }
        }




        /// <summary>
        /// Move file to archive
        /// </summary>
        private static void ArchiveLog()
        {
            //Move file
            File.Copy(_logFileName, MakeLogFileName(true));
            //Create the file with empty content
            using (File.Create(_logFileName))
            {
            }
        }

        /// <summary>
        /// Returns a list of log entries from the log file
        /// </summary>
        /// <returns></returns>
        
        public IPagedList<ExceptionLogDto> ReadLogFile(int page = 1, int pageSize = 20, string logFileName = null)
        {
            if (!string.IsNullOrEmpty(logFileName))
                logFileName = _logFileFolder + logFileName + LogFileExtension;
            _logFileName = logFileName ?? _logFileName;
            if (!File.Exists(_logFileName))
                return new PagedList<ExceptionLogDto>(new List<ExceptionLogDto>(), page, pageSize);
            // create empty log list
            var logs = new List<ExceptionLogDto>();

            // Read the file and display it line by line.
            using (var file = new StreamReader(_logFileName, Encoding.UTF8, true))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    var lineSplit = line.Split('|');
                    ExceptionLogDto exceptionLogDto;
                    try
                    {
                        exceptionLogDto = new ExceptionLogDto
                        {
                            Date =
                                DateTime.ParseExact(lineSplit[0].Trim(), DateTimeFormat, CultureInfo.InvariantCulture),
                            UserName = lineSplit[1].Trim(),
                            Ip = lineSplit[2].Trim(),
                            HostName = lineSplit[3].Trim(),
                            RequestUrl = lineSplit[4].Trim(),
                            ErrorType = lineSplit[5].Trim(),
                            InnerException = lineSplit[6].Trim(),
                            ExceptionType = lineSplit[7].Trim(),
                            ExceptionMessage = lineSplit[8].Trim(),
                            SourceFunctionName = lineSplit[9].Trim(),
                            SystemSource = lineSplit[10].Trim(),
                            StackTrace = lineSplit[11].Trim()


                            /*
                            /#2#* Date | User | IP | Host Name | Url | Error Type | Inner Exception | Exception Type | 
            Exception Message | Source Function Name | System Source | Stack trace
          #2#
*/

                        };
                    }
                    catch (Exception)
                    {
                        exceptionLogDto = null;
                    }
                    if (exceptionLogDto != null)
                        logs.Add(exceptionLogDto);
                }
            }

            // Order and take a max of 1000 entries
            return logs.OrderByDescending(x => x.Date).ToPagedList(page, pageSize);
        }


        #endregion

        public void ClearLog()
        {
            ArchiveLog();
        }

        public bool DeleteException(int lineIndex, string fileName = null)
        {
            if (!string.IsNullOrEmpty(fileName))
                fileName = _logFileFolder + fileName + LogFileExtension;
            _logFileName = fileName ?? _logFileName;
            var linesList = File.ReadAllLines(_logFileName).ToList();
            linesList.RemoveAt(lineIndex);
            File.WriteAllLines(_logFileName, linesList.ToArray());
            return true;
        }

        public bool DeleteLogFile(string fileName)
        {
            var fileFullPath = _logFileFolder + fileName + LogFileExtension;
            if (string.IsNullOrEmpty(fileName)) return false;
            if (!File.Exists(fileFullPath)) return false;
            File.Delete(fileFullPath);
            return true;
        }
    }
    }
