﻿using System;
using Abp.Application.Services;
using IContestPro.Logging.Dto;
using PagedList;

namespace IContestPro.Logging
{
    public interface ILoggingAppService : IApplicationService
    {
        void LogException(Exception ex);
        IPagedList<ExceptionLogDto> ReadLogFile(int page = 1, int pageSize = 20, string logFileName = null);
        void ClearLog();
        bool DeleteException(int lineIndex, string fileName=null);
       bool DeleteLogFile(string fileName);
    }
}