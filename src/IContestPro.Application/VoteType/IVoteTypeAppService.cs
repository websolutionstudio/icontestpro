﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.VoteType.Dto;

namespace IContestPro.VoteType
{
    public interface IVoteTypeAppService : IBaseAppService<VoteTypeDto, int, PagedResultRequestDto, CreateVoteTypeDto, VoteTypeDto>
    {
        Task<VoteTypeDto> GetByName(string name);
    }
}