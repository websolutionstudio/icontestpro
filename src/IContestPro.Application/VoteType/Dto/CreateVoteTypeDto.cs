﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.VoteType.Dto
{
    [AutoMapTo(typeof(IContestProEntities.VoteType))]
    public class CreateVoteTypeDto
    {
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        public int VotePoint { get; set; }
        public int RewardPoint { get; set; }
        public decimal Amount { get; set; }
        public decimal MinAmount { get; set; }
        public decimal MaxAmount { get; set; }

    }
}