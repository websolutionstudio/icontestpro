﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;

namespace IContestPro.VoteType.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.VoteType))]
    public class VoteTypeDto : FullAuditedEntityDto, IMayHaveTenant
    {
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        public int VotePoint { get; set; }
        public int RewardPoint { get; set; }
        public decimal Amount { get; set; }
        public decimal MinAmount { get; set; }
        public decimal MaxAmount { get; set; }
        
        public int? TenantId { get; set; }
    }
}