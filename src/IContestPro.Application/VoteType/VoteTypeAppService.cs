﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Authorization;
using IContestPro.Base;
using IContestPro.VoteType.Dto;

namespace IContestPro.VoteType
{
     public class VoteTypeAppService : BaseAppService<IContestProEntities.VoteType, VoteTypeDto, int,
                                PagedResultRequestDto, CreateVoteTypeDto, VoteTypeDto>, IVoteTypeAppService
    {
        private readonly IRepository<IContestProEntities.VoteType, int> _repository;

        public VoteTypeAppService(IRepository<IContestProEntities.VoteType, int> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }
        
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<VoteTypeDto> Create(CreateVoteTypeDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.VoteType>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<VoteTypeDto> Update(VoteTypeDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.VoteType>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }

        public async Task<VoteTypeDto> GetByName(string name)
        {
            return  (await _repository.FirstOrDefaultAsync(itm => itm.Name == name)).MapTo<VoteTypeDto>();
        }
    }
}