﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using IContestPro.MultiTenancy.Dto;

namespace IContestPro.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
