﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Base;
using IContestPro.UserVotePoint.Dto;

namespace IContestPro.UserVotePoint
{
    [AbpAuthorize]
     public class UserVotePointAppService : BaseAppService<IContestProEntities.UserVotePoint, UserVotePointDto, int,
                                PagedResultRequestDto, CreateUserVotePointDto, UserVotePointDto>, IUserVotePointAppService
    {
        private readonly IRepository<IContestProEntities.UserVotePoint, int> _repository;

        public UserVotePointAppService(IRepository<IContestProEntities.UserVotePoint, int> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }
        
        public override async Task<PagedResultDto<UserVotePointDto>> GetAll(PagedResultRequestDto input)
        {
            var query = _repository.GetAllIncluding(item => item.User);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<UserVotePointDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        public override async Task<UserVotePointDto> Create(CreateUserVotePointDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.UserVotePoint>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        public override async Task<UserVotePointDto> Update(UserVotePointDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.UserVotePoint>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }

        public async Task<UserVotePointDto> GetByUserId(long userId)
        {
            return  (await _repository.FirstOrDefaultAsync(itm => itm.UserId == userId)).MapTo<UserVotePointDto>();
        }

        public async Task<UserVotePointDto> UpdateByUserId(long userId, int points)
        {
            var votePoint = await _repository.FirstOrDefaultAsync(item => item.UserId == userId);
            votePoint.Points += points;
            CurrentUnitOfWork.SaveChanges();
            return  votePoint.MapTo<UserVotePointDto>();
        }
    }    
}