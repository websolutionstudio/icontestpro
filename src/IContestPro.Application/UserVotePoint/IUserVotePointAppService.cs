﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.UserVotePoint.Dto;

namespace IContestPro.UserVotePoint
{
    public interface IUserVotePointAppService : IBaseAppService<UserVotePointDto, int, PagedResultRequestDto, CreateUserVotePointDto, UserVotePointDto>
    {
        Task<UserVotePointDto> GetByUserId(long userId);
        Task<UserVotePointDto> UpdateByUserId(long userId, int voteRewardPoint);
    }
}    