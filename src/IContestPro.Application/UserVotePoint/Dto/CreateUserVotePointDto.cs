﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.UserVotePoint.Dto
{
    [AutoMapTo(typeof(IContestProEntities.UserVotePoint))]
    public class CreateUserVotePointDto
    {
        [Required]
        public int Points { get; set; }
        [Required]
        public long UserId { get; set; }

    }
}