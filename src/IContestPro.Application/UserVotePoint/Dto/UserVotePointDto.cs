﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Users.Dto;

namespace IContestPro.UserVotePoint.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.UserVotePoint))]
    public class UserVotePointDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public int Points { get; set; }
        public long UserId { get; set; }
        public int? TenantId { get; set; }

        public UserDto User { get; set; }
    }
}