﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using IContestPro.Authorization;
using IContestPro.Base;
using IContestPro.CodeSystem.Dto;
using IContestPro.EntityFrameworkCore;
using IContestPro.EntityFrameworkCore.Seed;
using IContestPro.EntityFrameworkCore.StoreProcedureManager;

namespace IContestPro.CodeSystem
{
    [AbpAuthorize(PermissionNames.Pages_Admin, PermissionNames.Pages_Sponsor)]

     public class CodeSystemAppService : BaseAppService<IContestProEntities.CodeSystem, CodeSystemDto, int,
                                PagedResultRequestDto, CreateCodeSystemDto, CodeSystemDto>, ICodeSystemAppService
    {
        private readonly IRepository<IContestProEntities.CodeSystem> _repository;
        private readonly IContestProDbContextFactory _contestProDbContextFactory;

        public CodeSystemAppService(IRepository<IContestProEntities.CodeSystem> repository, IIocResolver  iocResolver, IContestProDbContextFactory contestProDbContextFactory) : base(repository, iocResolver)
        {
            _repository = repository;
            _contestProDbContextFactory = contestProDbContextFactory;
        }
        public async Task<PagedResultDto<CodeSystemDto>> GetAllByStatus(PagedResultRequestDto input, string statusName = "")
        {
            var query = CreateFilteredQuery(input);
            if (!string.IsNullOrEmpty(statusName))
            {
                var status = StatusSeed.GetItems().First(item => item.Value == statusName);
                query = query.Where(item => item.StatusId == status.Key);
            }
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<CodeSystemDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
       public async Task<int> CountByStatus(string statusName, string batchNo = "")
        {
            var query = _repository.GetAll();
            if (!string.IsNullOrEmpty(batchNo))
            {
                query = query.Where(item => item.BatchNo == batchNo);
            }
            
                if (statusName.Equals(AppConsts.StatusNotUsed))
                {
                    query = query.Where(item => item.UsedByUserId == null);
                }
                else if (statusName.Equals(AppConsts.StatusUsed))
                {
                    query = query.Where(item => item.UsedByUserId != null);
                }
                else if (statusName.Equals(AppConsts.StatusExpired))
                {
                    query = query.Where(item => item.ExpiryDate != null && item.ExpiryDate <= DateTime.Now);
                }else if (statusName.Equals(AppConsts.StatusAllocated))
                {
                    query = query.Where(item => item.AllocatedToUserId != null);
                }else if (statusName.Equals(AppConsts.StatusNotAllocated))
                {
                    query = query.Where(item => item.AllocatedToUserId == null);
                }
                else
                {
                    var status = StatusSeed.GetItems().First(item => item.Value == statusName);
                    query = query.Where(item => item.StatusId == status.Key);

                }
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            return totalCount;
        }
        
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public async Task<AllocatedCodesOutputDto> AllocateToUsers(AllocateCodeDto input)
        {
            if (string.IsNullOrEmpty(input.Recipients))
            {
                throw new UserFriendlyException("Please select user to allocate code to."); 
            }
            if (input.TotalCodesToAllocate <= 0)
            {
                throw new UserFriendlyException("Total codes to allocate must be greater than 0."); 
            }
            var users = input.Recipients.Split(",");

            var totalTake = users.Length * input.TotalCodesToAllocate;
            var maxCodeAssignable =
               await SettingManager.GetSettingValueAsync<int>(AppConsts.SettingsMaxCodesAssignablePerTime);
            if (totalTake > maxCodeAssignable)
            {
                throw new UserFriendlyException($"You are allowed to allocate {maxCodeAssignable} codes at a time. Please split the users or reduce the code to assign to each. You can change this from the settings page."); 
            }
            var freeCodes = _repository.GetAll()
                .Where(itm => itm.AllocatedToUserId == null)
                .Take(totalTake).ToList();
            
            foreach (var recipient in users)
            {
                var counter = 1;
                while (counter <= input.TotalCodesToAllocate)
                {
                    counter++;
                    var item = freeCodes.FirstOrDefault(itm => itm.AllocatedToUserId == null);
                    if (item == null)
                    {
                        continue;
                    }
                    var status = StatusSeed.GetItems().First(itm => itm.Value == AppConsts.StatusAllocated);
                    item.AllocatedToUserId = long.Parse(recipient);
                    item.StatusId = status.Key;
                   await _repository.UpdateAsync(item);
                }
            }

            await CurrentUnitOfWork.SaveChangesAsync();
            return new AllocatedCodesOutputDto{TotalCodesGenerated = totalTake};
        }

        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public async Task<CreateCodesOutputDto> CreateCodes(CreateCodeSystemWithCountDto input)
        {
            var settingsMaximumBatchSize = await SettingManager.GetSettingValueAsync<int>(AppConsts.SettingsMaximumBatchSize);
            if (input.TotalToGenerate > settingsMaximumBatchSize)
            {
                throw new UserFriendlyException($"Total codes to generate must be less than or equal {settingsMaximumBatchSize}. This can be changed from the settings page."); 
            }
            var counter = 1;
            if (!string.IsNullOrEmpty(input.BatchNo))
            {
                var itemFound = await CheckIfBatchNoExists(input.BatchNo);
                if (itemFound.TotalFound > 0)
                {
                    throw new UserFriendlyException("Batch no already exists."); 
                }
            }
            else
            {
                input.BatchNo = await GenerateUniqueBatchNo();  
            }
            var status = StatusSeed.GetItems().First(itm => itm.Value == AppConsts.StatusNotAllocated);
            IContestProEntities.CodeSystem item = null;
            while (counter <= input.TotalToGenerate)
            {
            var code = await GenerateUniqueCode();
            var model = new IContestProEntities.CodeSystem
            {
                Code = code,
                BatchNo = input.BatchNo,
                StatusId = status.Key,
                UsableFrom = input.UsableFrom,
                ExpiryDate = input.ExpiryDate
            };
            item = await _repository.InsertAsync(model);
             counter++;
            }

            await CurrentUnitOfWork.SaveChangesAsync();
            return new CreateCodesOutputDto
            {
                LastItem = MapToEntityDto(item),
                TotalCodesGenerated = input.TotalToGenerate
            };
        }
        
        public async Task<CodeSystemDto> GetByCode(string code)
        {
            return  (await _repository.FirstOrDefaultAsync(itm => itm.Code == code)).MapTo<CodeSystemDto>();
        }
        
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public async Task<string> GenerateUniqueCode()
        {
                //Todo: The code size can be changed from the settings page. Default 6
                var codeSize = await SettingManager.GetSettingValueAsync<int>(AppConsts.SettingsCodeSize);
                string code = null;
                var countBeforeExit = 2000; //Todo: If no unique code is gotten after 2000 loop stop
                while (countBeforeExit > 0)
                {
                     code = Guid.NewGuid().ToString("n").Substring(0, codeSize).ToUpper();
                    var item = await CheckIfCodeExists(code);
                    if (item.TotalFound == 0)
                    {
                        break;
                    }
                    countBeforeExit--;
                    if (countBeforeExit <= 0)
                    {
                        throw new UserFriendlyException("No unique code found after 2000 checks. Please increase the size of the code and try again. Code exited.");
                    }
                }
                return code;
 
        } 
        public async Task<CheckIfItemExistsDto> CheckIfCodeExists(string code)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                    var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpCheckIfCodeExists)
                        .WithSqlParam("@Code", code);
                    return await cmd.ExecuteStoredProcedureFirstOrDefaultAsync<CheckIfItemExistsDto>();
            }
        } 
        
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public async Task<string> GenerateUniqueBatchNo()
        {
               //Todo: The batch no size can be changed from the settings page. Default 6
                var batchNoSize = await SettingManager.GetSettingValueAsync<int>(AppConsts.SettingsBatchNoSize);
                string batchNo = null;
                var countBeforeExit = 2000; //Todo: If no unique code is gotten after 2000 loop stop
                while (countBeforeExit > 0)
                {
                     batchNo = Guid.NewGuid().ToString("n").Substring(0, batchNoSize).ToUpper();
                    var item = await CheckIfBatchNoExists(batchNo);
                    if (item.TotalFound == 0)
                    {
                        break;
                    }
                    countBeforeExit--;
                    if (countBeforeExit <= 0)
                    {
                        throw new UserFriendlyException("No unique batch no found after 2000 checks. Please increase the size of the code and try again. Code exited.");
                    }
                }
                return batchNo;
 
        } 
        public async Task<CheckIfItemExistsDto> CheckIfBatchNoExists(string batchNo)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                    var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpCheckIfBatchNoExists)
                        .WithSqlParam("@BatchNo", batchNo);
                    return await cmd.ExecuteStoredProcedureFirstOrDefaultAsync<CheckIfItemExistsDto>();
                   
 
            }
        }

        public async Task<PagedResultDto<CodeSystemDto>> GetAllWithFilter(PagedResultRequestDto input, string batchNo = "", string statusName = "")
        {
            var query = _repository.GetAllIncluding(itm => itm.Status);
            if (!string.IsNullOrEmpty(batchNo))
            {
                query = query.Where(item => item.BatchNo == batchNo);
            }
            if (!string.IsNullOrEmpty(statusName))
            {
                if (statusName.Equals(AppConsts.StatusNotUsed))
                {
                    query = query.Where(item => item.UsedByUserId == null);
                }
                else if (statusName.Equals(AppConsts.StatusUsed))
                {
                    query = query.Where(item => item.UsedByUserId != null);
                }
                else if (statusName.Equals(AppConsts.StatusExpired))
                {
                    query = query.Where(item => item.ExpiryDate != null && item.ExpiryDate <= DateTime.Now);
                }else if (statusName.Equals(AppConsts.StatusAllocated))
                {
                    query = query.Where(item => item.AllocatedToUserId != null);
                }else if (statusName.Equals(AppConsts.StatusNotAllocated))
                {
                    query = query.Where(item => item.AllocatedToUserId == null);
                }
                else
                {
                    var status = StatusSeed.GetItems().First(item => item.Value == statusName);
                    query = query.Where(item => item.StatusId == status.Key);

                }
            }

            if (!await IsGrantedAsync(PermissionNames.Pages_Admin))
            {
                query = query.Where(item => item.AllocatedToUserId == AbpSession.GetUserId());
            }
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderByDescending(itm => itm.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<CodeSystemDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
          
        }
        
        public async Task<CodeDetailsDto> GetCodeDetailsByCode(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
             throw new UserFriendlyException("Code is required."); 
            }
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetCodeDetailsByCode)
                    .WithSqlParam("@Code", code);
                return await cmd.ExecuteStoredProcedureFirstOrDefaultAsync<CodeDetailsDto>();
            }
           
        }

        public async Task<IReadOnlyList<string>> GenerateUniqueCodes(int totalToGenerate = 10)
        {
            var codes =  new List<string>();
            await Task.Run(async () =>
            {
                while (totalToGenerate > 0)
                {
                    var item = await GenerateUniqueCode();
                    codes.Add(item);
                    totalToGenerate--;
                } 
            });
            return codes;
        }
    }
}