﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.CodeSystem.Dto;

namespace IContestPro.CodeSystem
{
    public interface ICodeSystemAppService : IBaseAppService<CodeSystemDto, int, PagedResultRequestDto, CreateCodeSystemDto, CodeSystemDto>
    {
        Task<CodeSystemDto> GetByCode(string code);
        Task<PagedResultDto<CodeSystemDto>> GetAllByStatus(PagedResultRequestDto input, string statusName = "");
        Task<CreateCodesOutputDto> CreateCodes(CreateCodeSystemWithCountDto input);
        Task<string> GenerateUniqueCode();
        Task<IReadOnlyList<string>> GenerateUniqueCodes(int totalToGenerate = 10);
        Task<string> GenerateUniqueBatchNo();
        Task<CheckIfItemExistsDto> CheckIfCodeExists(string code);
        Task<CheckIfItemExistsDto> CheckIfBatchNoExists(string batchNo);
        Task<PagedResultDto<CodeSystemDto>> GetAllWithFilter(PagedResultRequestDto pagedResultRequestDto,
            string batchNo = "", string status = "" );

        Task<int> CountByStatus(string statusName, string batchNo = "");
        Task<AllocatedCodesOutputDto> AllocateToUsers(AllocateCodeDto model);
        Task<CodeDetailsDto> GetCodeDetailsByCode(string code);
    }
}