﻿using System.ComponentModel.DataAnnotations;

namespace IContestPro.CodeSystem.Dto
{
    public class AllocateCodeDto
    {
        [Required]
        public int TotalCodesToAllocate { get; set; }
        [Required]
        public string Recipients { get; set; }
    }
}