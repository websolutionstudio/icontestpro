﻿namespace IContestPro.CodeSystem.Dto
{
    public class CreateCodesOutputDto
    {
        public CodeSystemDto LastItem { get; set; }
        public int TotalCodesGenerated { get; set; }
    }
}