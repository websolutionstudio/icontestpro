﻿using System;

namespace IContestPro.CodeSystem.Dto
{
    public class CodeDetailsDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string BatchNo { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public long? AllocatedToUserId { get; set; }
        public string AllocatedToUserNickName { get; set; }
        public string AllocatedToUserPermalink { get; set; }
        public long? UsedByUserId { get; set; }
        public string UsedByUserNickName { get; set; }
        public string UsedByUserPermalink { get; set; }
        public DateTime? UsableFrom { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int? ContestId { get; set; }
        public string ContestTitle { get; set; }
        public DateTime CreationTime { get; set; }
    }
}