﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.CodeSystem.Dto
{
    [AutoMapTo(typeof(IContestProEntities.CodeSystem))]
    public class CreateCodeSystemWithCountDto
    {
        
        [StringLength(10)]
        public string BatchNo { get; set; }
        public int StatusId { get; set; }
        public DateTime? UsableFrom { get; set; }
        public DateTime? ExpiryDate { get; set; }
        [Required]
        public int TotalToGenerate { get; set; }
    }
}