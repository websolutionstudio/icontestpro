﻿namespace IContestPro.CodeSystem.Dto
{
    public class AllocatedCodesOutputDto
    {
        public CodeSystemDto LastItem { get; set; }
        public int TotalCodesGenerated { get; set; }
    }
}