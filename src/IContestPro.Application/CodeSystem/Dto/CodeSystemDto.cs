﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using IContestPro.Contest.Dto;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.CodeSystem.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.CodeSystem))]
    public class CodeSystemDto : FullAuditedEntityDto
    {
        public int? TenantId { get; set; }
        public string Code { get; set; }
        public string BatchNo { get; set; }
        public int StatusId { get; set; }
        public long? AllocatedToUserId { get; set; }
        public long? UsedByUserId { get; set; }
        public DateTime? UsableFrom { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int? ContestId { get; set; }

        public UserDto AllocatedToUser { get; set; }
        public UserDto UsedByUser { get; set; }
        public StatusDto Status { get; set; }
        public ContestDto Contest { get; set; }
        
        
        
       
    }
}