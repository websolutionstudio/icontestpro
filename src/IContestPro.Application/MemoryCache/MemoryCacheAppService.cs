﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace IContestPro.MemoryCache
{
    public class MemoryCacheAppService :  IMemoryCacheAppService
    {
        private readonly IMemoryCache _memoryCache;
        public MemoryCacheAppService(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

       
        public async Task<TOut> Get<TOut>(string key)
        {
                var item = _memoryCache.Get<TOut>(key);
                return item;
        }

        public async Task<T> Set<T>(string key, T value,  MemoryCacheEntryOptions cacheEntryOptions = null)
        {
            // Save data in cache.
            if (cacheEntryOptions == null)
            {
                cacheEntryOptions = new MemoryCacheEntryOptions();
                cacheEntryOptions.SetSlidingExpiration(TimeSpan.FromHours(1));
            }
            if (cacheEntryOptions.SlidingExpiration == null)
            {
                cacheEntryOptions.SetSlidingExpiration(TimeSpan.FromHours(1));
            }
            var item = _memoryCache.Set(key, value, cacheEntryOptions);
            return item;
        } 
        public async Task Remove( string key)
        {
            await Task.Run(() => _memoryCache.Remove(key));
        }
    }
}