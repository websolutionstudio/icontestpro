﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Microsoft.Extensions.Caching.Memory;

namespace IContestPro.MemoryCache
{
    public interface IMemoryCacheAppService : IApplicationService
    {
        Task<TOut> Get<TOut>(string key);
        Task<T> Set<T>(string key, T value,  MemoryCacheEntryOptions cacheEntryOptions = null);
        Task Remove(string key);
    }
}