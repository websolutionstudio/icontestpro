﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Withdraw.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Withdraw))]
    public class CreateWithdrawDto
    {
        public long UserId { get; set; }
        public int StatusId { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public string PayIn { get; set; }
        [Required]
        public string PayTo { get; set; }
        [Required]
        public int BankDetailsId { get; set; }
        public double FeePercentage { get; set; }
        public decimal FeeAmount { get; set; }

    }
}