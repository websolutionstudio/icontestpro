﻿namespace IContestPro.Withdraw.Dto
{
    public class WithdrawResultDto
    {   
        public long UserId { get; set; }
        public decimal Amount { get; set; }
        public int? TenantId { get; set; }
        public string Type { get; set; }
        public int BankDetailsId { get; set; }
        public int BankId { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string FullName { get; set; }
    }
}