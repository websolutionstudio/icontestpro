﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.BankDetails.Dto;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Withdraw.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Withdraw))]
    public class WithdrawDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public long UserId { get; set; }
        public decimal Amount { get; set; }
        public int? TenantId { get; set; }
        public string PayIn { get; set; }
        public string PayTo { get; set; }
        public string BankName { get; set; }
        public int BankDetailsId { get; set; }
        public int FeePercentage { get; set; }
        public decimal FeeAmount { get; set; }
        
        public int StatusId { get; set; }

        public StatusDto Status { get; set; }
        public BankDetailsDto BankDetails { get; set; }

        public UserDto User { get; set; }
    }
}