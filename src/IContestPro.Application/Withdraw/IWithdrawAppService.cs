﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.Dto;
using IContestPro.Withdraw.Dto;

namespace IContestPro.Withdraw
{
    public interface IWithdrawAppService : IBaseAppService<WithdrawDto, int, PagedResultRequestDto, CreateWithdrawDto, WithdrawDto>
    {
       Task<PagedResultDto<WithdrawDto>> GetByUserId(long userId, PagedResultRequestDto input);

         Task<WithdrawDto> UpdateStatusAsync(UpdateStatusDto model);
         Task<WithdrawDto> CancelWithdrawalRequestAsync(EntityDto model);

        Task<PagedResultDto<WithdrawDto>> GetAllBetweenDates(PagedResultRequestDto input, DateTime startDate, DateTime endDate);
        Task<PagedResultDto<WithdrawDto>> GetAllByUserBetweenDates(long userId, PagedResultRequestDto input, DateTime startDate, DateTime endDate);
    }
}