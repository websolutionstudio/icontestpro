﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using IContestPro.AppFees;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Base;
using IContestPro.Contest.Dto;
using IContestPro.Helpers;
using IContestPro.Logging;
using IContestPro.Status;
using IContestPro.Wallet;
using IContestPro.Wallet.Dto;
using IContestPro.Withdraw.Dto;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.Withdraw
{
    [AbpAuthorize]
     public class WithdrawAppService : BaseAppService<IContestProEntities.Withdraw, WithdrawDto, int,
                                PagedResultRequestDto, CreateWithdrawDto, WithdrawDto>, IWithdrawAppService
    {
        private readonly IRepository<IContestProEntities.Withdraw> _repository;
        private readonly IRepository<IContestProEntities.Bank> _bankRepository;
        private readonly IWalletAppService _walletAppService;
        private readonly ILoggingAppService _loggingAppService;
        private readonly IStatusAppService _statusAppService;
        private readonly IAppFeesAppService _appFeesAppService;


        public WithdrawAppService(IRepository<IContestProEntities.Withdraw> repository, IWalletAppService walletAppService,
            ILoggingAppService loggingAppService, IStatusAppService statusAppService, 
            IAppFeesAppService appFeesAppService,
            IRepository<IContestProEntities.Bank> bankRepository,
            IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
            _walletAppService = walletAppService;
            _loggingAppService = loggingAppService;
            _statusAppService = statusAppService;
            _appFeesAppService = appFeesAppService;
            _bankRepository = bankRepository;
        }

        public override async Task<PagedResultDto<WithdrawDto>> GetAll(PagedResultRequestDto input)
        {
            var query = _repository.GetAllIncluding(item => item.BankDetails, item => item.Status, item => item.User );
            query = query.OrderByDescending(item => item.CreationTime);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            foreach (var entity in entityDtos)
            {
                var bank = await _bankRepository.GetAll().Select(itm => new{itm.Name, itm.Id}).FirstOrDefaultAsync(itm =>itm.Id == entity.BankDetails.BankId);
              
                entity.BankName = bank != null ? bank.Name : "";
            }
            return new PagedResultDto<WithdrawDto>(
                totalCount,
                entityDtos
            );
        
        }
        public async Task<PagedResultDto<WithdrawDto>> GetAllBetweenDates(PagedResultRequestDto input, DateTime startDate, DateTime endDate)
        {
            var query = _repository.GetAllIncluding(item => item.BankDetails, item => item.Status, item => item.User );
            query = query.OrderByDescending(item => item.CreationTime);
            query = query.Where(itm => itm.CreationTime >= startDate && itm.CreationTime <= endDate);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            foreach (var entity in entityDtos)
            {
                var bank = await _bankRepository.GetAll().Select(itm => new{itm.Name, itm.Id}).FirstOrDefaultAsync(itm =>itm.Id == entity.BankDetails.BankId);
                entity.BankName =  bank != null ? bank.Name : "";
            }
            return new PagedResultDto<WithdrawDto>(
                totalCount,
                entityDtos
            );
        }

        public async Task<PagedResultDto<WithdrawDto>> GetByUserId(long userId, PagedResultRequestDto input)
        {
            var query = _repository.GetAllIncluding(item => item.BankDetails, item => item.Status, item => item.User );
            query = query.Where(itm => itm.UserId == userId);
            query = query.OrderByDescending(item => item.CreationTime);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            foreach (var entity in entityDtos)
            {
                var bank = await _bankRepository.GetAll().Select(itm => new{itm.Name, itm.Id}).FirstOrDefaultAsync(itm =>itm.Id == entity.BankDetails.BankId);
                entity.BankName =  bank != null ? bank.Name : "";
            }
            return new PagedResultDto<WithdrawDto>(
                totalCount,
                entityDtos
            );
        }

        public async Task<PagedResultDto<WithdrawDto>> GetAllByUserBetweenDates(long userId, PagedResultRequestDto input, DateTime startDate, DateTime endDate)
        {
            // 
            var query = _repository.GetAllIncluding(item => item.BankDetails, item => item.Status, item => item.User );
            query = query.Where(itm => itm.UserId == userId);
            query = query.Where(itm => itm.CreationTime >= startDate && itm.CreationTime <= endDate);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            foreach (var entity in entityDtos)
            {
                var bank = await _bankRepository.GetAll().Select(itm => new{itm.Name, itm.Id}).FirstOrDefaultAsync(itm =>itm.Id == entity.BankDetails.BankId);
                entity.BankName =  bank != null ? bank.Name : "";
            }
            return new PagedResultDto<WithdrawDto>(
                totalCount,
                entityDtos
            );
        }
        public override async Task<WithdrawDto> Get(EntityDto<int> input)
        {
            var query = await _repository.GetAllIncluding(item => item.BankDetails, item => item.Status, item => item.User )
                .FirstOrDefaultAsync(itm => itm.Id == input.Id);

            return query != null ? query.MapTo<WithdrawDto>() : null;
        
        }
        public override async Task<WithdrawDto> Create(CreateWithdrawDto input)
        {
            var accountBalance = await _walletAppService.GetBalance(AbpSession.GetUserId());
            var withdrawalRequestFeePercentage = await _appFeesAppService.GetWithdrawalRequestFeePercentage();
            var percentageChargeAmount = input.Amount * ((decimal)withdrawalRequestFeePercentage / 100);
            var minimumWithdrawAmount = await SettingManager.GetSettingValueAsync<decimal>(AppConsts.SettingsWithdrawalMinimumAmount);
            var adminWallet = await _walletAppService.GetAdminWallet();
            WithdrawDto item = null;
            try
            {
                if (input.Amount < minimumWithdrawAmount)
                {
                    throw new UserFriendlyException($"Withdrawal request amount must be equal or above {minimumWithdrawAmount}.");
                }
               
                var totalAmount = input.Amount + percentageChargeAmount;
                if (accountBalance < totalAmount)
                {
                    throw new UserFriendlyException($"Insuficient account balance. Your account balance must be above {totalAmount} to complete your request.");
                }
                var debitResult = await _walletAppService.Debit(AbpSession.GetUserId(), input.Amount, false);
                if (debitResult == null)
                {
                    throw new UserFriendlyException("Error occurred while creating withdrawal.");
                }
                //Todo: Log the debit in a new thread
                await _walletAppService.LogWalletOperation(new WalletInfoDto
                {
                    Amount = input.Amount, 
                    Title = "Withdrawal request debit",
                    Description =  "A withdrawal request debit occurred in your account.",
                    WalletInfoResult = WalletInfoResult.Success,
                    WalletOperation = WalletOperation.Debit,
                    UserId = AbpSession.GetUserId(),
                    
                });
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = AbpSession.GetUserId(),
                    TenantId = AbpSession.TenantId,
                    Message = $"A withdrawal request debit of {input.Amount.ToNairaString()} occurred in your account.",
                });
                var creditAdmin = await _walletAppService.Credit(adminWallet.UserId, input.Amount, false);
                if (creditAdmin == null)
                {
                    throw new UserFriendlyException("Error occurred while creating withdrawal.");
                }
                //Todo: Log the debit in a new thread
                await _walletAppService.LogWalletOperation(new WalletInfoDto
                {
                    Amount = input.Amount, 
                    Title = "Withdrawal request credit",
                    Description =  "A withdrawal request credit occurred in the admin account.",
                    WalletInfoResult = WalletInfoResult.Success,
                    WalletOperation = WalletOperation.Credit,
                    UserId = adminWallet.UserId
                });
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = adminWallet.UserId,
                    TenantId = AbpSession.TenantId,
                    Message = $"A withdrawal request credit of {input.Amount.ToNairaString()} occurred in your account.",
                });
                var feeResult = await _walletAppService.Debit(AbpSession.GetUserId(), percentageChargeAmount, false);
                if (feeResult == null)
                {
                    throw new UserFriendlyException("Error occurred while creating withdrawal.");
                }
                //Todo: Log the debit in a new thread
                await _walletAppService.LogWalletOperation(new WalletInfoDto
                {
                    Amount = percentageChargeAmount, 
                    Title = "Withdrawal request fee debit",
                    Description =  $"{withdrawalRequestFeePercentage}% fee was charged on {input.Amount.ToNairaString()} withdrawal request.",
                    WalletInfoResult = WalletInfoResult.Success,
                    WalletOperation = WalletOperation.Debit,
                    UserId = AbpSession.GetUserId(),
                });
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = AbpSession.GetUserId(),
                    TenantId = AbpSession.TenantId,
                    Message = $"{withdrawalRequestFeePercentage}% fee was charged on {input.Amount.ToNairaString()} withdrawal request."
                });
                var feeResultAdminCredit= await _walletAppService.Credit(adminWallet.UserId, percentageChargeAmount, false);
                if (feeResultAdminCredit == null)
                {
                    throw new UserFriendlyException("Error occurred while creating withdrawal.");
                }
                //Todo: Log the debit in a new thread
                await _walletAppService.LogWalletOperation(new WalletInfoDto
                {
                    Amount = percentageChargeAmount, 
                    Title = $"Withdrawal request {withdrawalRequestFeePercentage}% charge  credit",
                    Description =  $"A withdrawal request charge of {withdrawalRequestFeePercentage}% occurred in the admin account.",
                    WalletInfoResult = WalletInfoResult.Success,
                    WalletOperation = WalletOperation.Credit,
                    UserId = adminWallet.UserId
                });
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = adminWallet.UserId,
                    TenantId = AbpSession.TenantId,
                    Message =  $"A withdrawal request charge of {withdrawalRequestFeePercentage}% occurred in the admin account."
                });
                var status = await _statusAppService.GetByNameAsync(AppConsts.StatusPending);
                input.FeePercentage = withdrawalRequestFeePercentage;
                input.FeeAmount = percentageChargeAmount;
                input.StatusId = status.Id;
                input.UserId = AbpSession.GetUserId();
                 item = await base.Create(input);
                return item;
            }
            catch (Exception e)
            {
               await Task.Run(async () =>
                {
                    // Some specific work to do...
                    _loggingAppService.LogException(e);
                    var balanceAfter = await _walletAppService.GetBalance(AbpSession.GetUserId());
                    if (accountBalance > balanceAfter)
                    {
                        await _walletAppService.Credit(AbpSession.GetUserId(), input.Amount, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Amount = input.Amount, 
                            Title = "Withdrawal request reversal occurred",
                            Description =  "Your withdrawal request was reversed as the request was not successful.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Credit,
                            UserId = AbpSession.GetUserId(),
                        }); 
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = AbpSession.GetUserId(),
                            TenantId = AbpSession.TenantId,
                            Message =  "Your withdrawal request was reversed as the request was not successful."
                        });
                        await _walletAppService.Debit(adminWallet.UserId, input.Amount, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Amount = input.Amount, 
                            Title = "Withdrawal request reversal debit",
                            Description =  "A withdrawal request was reversed as the request was not successful.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Debit,
                            UserId = adminWallet.UserId,
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = adminWallet.UserId,
                            TenantId = AbpSession.TenantId,
                            Message =  $"A withdrawal request {input.Amount.ToNairaString()} was reversed as the request was not successful.",
                        });
                        await _walletAppService.Credit(AbpSession.GetUserId(), percentageChargeAmount, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Amount = percentageChargeAmount, 
                            Title = "Withdrawal request fee reversal credit",
                            Description =  $"{withdrawalRequestFeePercentage}% fee was charged on {input.Amount.ToNairaString()} withdrawal request was reversed.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Credit,
                            UserId = AbpSession.GetUserId(),
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = AbpSession.GetUserId(),
                            TenantId = AbpSession.TenantId,
                            Message =  $"{withdrawalRequestFeePercentage}% fee was charged on {input.Amount.ToNairaString()} withdrawal request was reversed.",
                        });
                        await _walletAppService.Debit(adminWallet.UserId, percentageChargeAmount, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Amount = percentageChargeAmount, 
                            Title = "Withdrawal request fee reversal debit",
                            Description =  $"{withdrawalRequestFeePercentage}% fee charged on {input.Amount.ToNairaString()} withdrawal request was reversed.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Debit,
                            UserId = adminWallet.UserId,
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = adminWallet.UserId,
                            TenantId = AbpSession.TenantId,
                            Message =  $"{withdrawalRequestFeePercentage}% fee charged on {input.Amount.ToNairaString()} withdrawal request was reversed.",
                        });
                    }

                    if (item != null)
                    {
                      await Delete(new EntityDto{Id = item.Id});
                    }
                   // throw e;

                });
            }

            return null;
        }
        public override async Task<WithdrawDto> Update(WithdrawDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.Withdraw>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        
      
        public async Task<WithdrawDto> UpdateStatusAsync(UpdateStatusDto model)
        {
            var entry = await _repository.FirstOrDefaultAsync(itm => itm.Id == model.EntityId);
            var status =  await _statusAppService.GetByNameAsync(model.StatusName);
            if (entry == null || status == null)
            {
                return null;
            }
            entry.StatusId = status.Id;
            UnitOfWorkManager.Current.SaveChanges();
            return entry.MapTo<WithdrawDto>();
        }

        public async Task<WithdrawDto> CancelWithdrawalRequestAsync(EntityDto model)
        {
            var request = await _repository.FirstOrDefaultAsync(itm => itm.Id == model.Id);
            if (request == null)
            {
                throw new UserFriendlyException("Withdrawal requerst not found.");
            }
            var adminWallet = await _walletAppService.GetAdminWallet();

                        await _walletAppService.Credit(request.UserId, request.Amount, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Amount = request.Amount, 
                            Title = "Withdrawal request cancellation credit",
                            Description =  "Your withdrawal request cancellation reversal credit occurred in your account.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Credit,
                            UserId = request.UserId,
                        }); 
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = request.UserId,
                            TenantId = AbpSession.TenantId,
                            Message = $"Your withdrawal request cancellation reversal credit of {request.Amount.ToNairaString()} occurred in your account.",
                        });
                        await _walletAppService.Debit(adminWallet.UserId, request.Amount, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Amount = request.Amount, 
                            Title = "Withdrawal request cancellation debit",
                            Description =  "A withdrawal request cancellation debit occurred in your account.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Debit,
                            UserId = adminWallet.UserId,
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = adminWallet.UserId,
                            TenantId = AbpSession.TenantId,
                            Message = $"A withdrawal request cancellation debit of {request.Amount.ToNairaString()} occurred in your account.",
                        });
                        await _walletAppService.Credit(request.UserId, request.FeeAmount, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Amount = request.FeeAmount, 
                            Title = "Withdrawal request cancellation fee reversal credit",
                            Description =  $"{request.FeePercentage}% withdrawal fee charged on {request.Amount.ToNairaString()} request was reversed as a result of cancellation.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Credit,
                            UserId = request.UserId,
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = request.UserId,
                            TenantId = AbpSession.TenantId,
                            Message = $"{request.FeePercentage}% withdrawal fee charged on {request.Amount.ToNairaString()} request was reversed as a result of cancellation.",
                        });
                        await _walletAppService.Debit(adminWallet.UserId, request.FeeAmount, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Amount = request.FeeAmount, 
                            Title = "Withdrawal request cancellation fee reversal debit",
                            Description =  $"{request.FeePercentage}% fee charged on {request.Amount} withdrawal request was reversed as a result of cancellation.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Debit,
                            UserId = adminWallet.UserId,
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = adminWallet.UserId,
                            TenantId = AbpSession.TenantId,
                            Message =  $"{request.FeePercentage}% fee charged on {request.Amount.ToNairaString()} withdrawal request was reversed as a result of cancellation.",
                        });
                        var status = await _statusAppService.GetByNameAsync(AppConsts.StatusCancelled);
                        request.StatusId = status.Id;
                        await UnitOfWorkManager.Current.SaveChangesAsync();
                        return request.MapTo<WithdrawDto>();
        }

       
    }
}