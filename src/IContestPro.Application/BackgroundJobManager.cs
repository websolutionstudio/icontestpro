﻿using IContestPro.AdvertBox1;
using IContestPro.AdvertBox2;
using IContestPro.AdvertHomePageBanner;
using IContestPro.AdvertTopBanner;
using IContestPro.BackgroundJobs;
using IContestPro.Contest;
using IContestPro.FeaturedContest;
using IContestPro.FeaturedContestEntry;

namespace IContestPro
{
    public class BackgroundJobManager : BgJobBase
    {
        private readonly IContestAppService _contestAppService;
        private readonly IAdvertHomePageBannerAppService _advertHomePageBannerAppService;
        private readonly IAdvertTopBannerAppService _advertTopBannerAppService;
        private readonly IFeaturedContestAppService _featuredContestAppService;
        private readonly IFeaturedContestEntryAppService _featuredContestEntryAppService;
        private readonly IAdvertBox1AppService _advertBox1AppService;
        private readonly IAdvertBox2AppService _advertBox2AppService;

        public BackgroundJobManager(IContestAppService contestAppService, IAdvertHomePageBannerAppService advertHomePageBannerAppService, 
            IAdvertTopBannerAppService advertTopBannerAppService, IFeaturedContestAppService featuredContestAppService,
            IFeaturedContestEntryAppService featuredContestEntryAppService, IAdvertBox1AppService advertBox1AppService, IAdvertBox2AppService advertBox2AppService)
        {
            _contestAppService = contestAppService;
            _advertHomePageBannerAppService = advertHomePageBannerAppService;
            _advertTopBannerAppService = advertTopBannerAppService;
            _featuredContestAppService = featuredContestAppService;
            _featuredContestEntryAppService = featuredContestEntryAppService;
            _advertBox1AppService = advertBox1AppService;
            _advertBox2AppService = advertBox2AppService;
        }

        public void NotifyExpiringContests()
        {
         _contestAppService.NotifyExpiringContests();
        }
        public void DisableExpiredContests()
        {
         _contestAppService.DisableExpiredContests();
        }

        public void NotifyExpiringHomePageBanner()
        {
            _advertHomePageBannerAppService.NotifyExpiring();
        }
        public void DisableExpiredHomePageBanner()
        {
            _advertHomePageBannerAppService.DisableExpired();

        }

        public void NotifyExpiringTopBanner()
        {
            _advertTopBannerAppService.NotifyExpiring();
        }
        public void DisableExpiredTopBanner()
        {
            _advertTopBannerAppService.DisableExpired();

        }

        public void NotifyExpiringFeaturedContests()
        {
            _featuredContestAppService.NotifyExpiring();
        }
        public void DisableExpiredFeaturedContests()
        {
            _featuredContestAppService.DisableExpired();
        }

        public void NotifyExpiringFeaturedContestEntries()
        {
            _featuredContestEntryAppService.NotifyExpiring();
        }
        public void DisableExpiredFeaturedContestEntries()
        {
            _featuredContestEntryAppService.DisableExpired();
        }

        public void NotifyExpiringAdvertBox1()
        {
            _advertBox1AppService.NotifyExpiring();
        }
        public void DisableExpiredAdvertBox1()
        {
            _advertBox1AppService.DisableExpired();

        }
        
        public void NotifyExpiringAdvertBox2()
        {
            _advertBox2AppService.NotifyExpiring();
        }
        public void DisableExpiredAdvertBox2()
        {
            _advertBox2AppService.DisableExpired();

        }
       
    }
}