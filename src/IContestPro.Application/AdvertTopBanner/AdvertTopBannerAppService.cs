﻿using System;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Notifications;
using Abp.UI;
using Hangfire;
using IContestPro.Base;
using IContestPro.AdvertTopBanner.Dto;
using IContestPro.AdvertPrice;
using IContestPro.AssetUpload;
using IContestPro.AssetUpload.Dto;
using IContestPro.BackgroundJobs;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Contest.Dto;
using IContestPro.Helpers;
using IContestPro.Status;
using IContestPro.Users;
using IContestPro.Wallet;
using IContestPro.Wallet.Dto;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.AdvertTopBanner
{
     public class AdvertTopBannerAppService : BaseAppService<IContestProEntities.AdvertTopBanner, AdvertTopBannerDto, int,
                                PagedResultRequestDto, CreateAdvertTopBannerDto, AdvertTopBannerDto>, IAdvertTopBannerAppService
    {
        private readonly IRepository<IContestProEntities.AdvertTopBanner> _repository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAdvertPriceAppService _advertPriceAppService;
        private readonly IAssetUploadAppService _assetUploadAppService;
        private readonly IWalletAppService _walletAppService;
        private readonly IUserAppService _userAppService;
       
        public AdvertTopBannerAppService(IRepository<IContestProEntities.AdvertTopBanner> repository, IStatusAppService statusAppService, IAdvertPriceAppService advertPriceAppService, IAssetUploadAppService assetUploadAppService, IWalletAppService walletAppService, IUserAppService userAppService, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
            _statusAppService = statusAppService;
            _advertPriceAppService = advertPriceAppService;
            _assetUploadAppService = assetUploadAppService;
            _walletAppService = walletAppService;
            _userAppService = userAppService;
        }
        public async Task<AdvertTopBannerDto> GetRandomLive(int totalTake = 1)
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var query =  _repository.GetAll().OrderBy(item => Guid.NewGuid())
                .FirstOrDefault(item => item.StatusId == status.Id && item.StartDate.AddHours(-1) <= DateTime.Now && item.EndDate >= DateTime.Now);
            return MapToEntityDto(query);
        } 
        [AbpAuthorize]
        public override async Task<PagedResultDto<AdvertTopBannerDto>> GetAll(PagedResultRequestDto input)
        {
            
            var query =  _repository.GetAllIncluding(item => item.Status);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            
            return new PagedResultDto<AdvertTopBannerDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        [AbpAuthorize]
        public async Task<PagedResultDto<AdvertTopBannerDto>> GetAllByStatus(string statusName, PagedResultRequestDto input)
        {
            var status = await _statusAppService.GetByNameAsync(statusName);
            if (status == null)
            {
                return null;
            }
            var query =  _repository.GetAllIncluding(item => item.Status).Where(item => item.StatusId == status.Id);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            
            return new PagedResultDto<AdvertTopBannerDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        [AbpAuthorize]
        public async Task<PagedResultDto<AdvertTopBannerDto>> GetAllByUser(long userId, PagedResultRequestDto input)
        {
            var query =  _repository.GetAllIncluding(item => item.Status).Where(item => item.UserId == userId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<AdvertTopBannerDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        
        [AbpAuthorize]
        public override Task<AdvertTopBannerDto> Create(CreateAdvertTopBannerDto input)
        {
            throw new UserFriendlyException("You are not allowed to use this method to create top banner. Please use CreateAndUploadImage method instead.");
        }
        [AbpAuthorize]
        public override Task<AdvertTopBannerDto> Update(AdvertTopBannerDto input)
        {
            throw new UserFriendlyException("You are not allowed to use this method to update top banner. Please use UpdateAndUploadImage method instead.");
        }
        [AbpAuthorize]
         public async Task<AdvertTopBannerDto> CreateAndUploadImage(CreateEditTopBannerDto input)
        {
            if (input.StartDate == null)
            {
                throw new UserFriendlyException("StartDate date must be at least one day ahead.");
            }
            if (input.ExternalUrl == null)
            {
                throw new UserFriendlyException("External url is required.");
            }
            if (input.StartDate > input.EndDate || input.StartDate == input.EndDate)
            {
                throw new UserFriendlyException("StartDate date must be less than end date.");
            }
            if (input.UserId == null)
            {
                throw new UserFriendlyException("User id is required.");
            }
            if (input.AdvertPriceId == null)
            {
                throw new UserFriendlyException("Duration is required.");
            }
            
            var wallet = await _walletAppService.GetByUserId(input.UserId.Value);
            if (wallet == null)
            {
                throw new UserFriendlyException("User wallet information not found.");
            }
            var advertPrice = await _advertPriceAppService.Get(new EntityDto{Id = input.AdvertPriceId.Value});
            if (wallet.Balance < advertPrice.Amount)
            {
                throw new UserFriendlyException("Insufficieent balance. Please add fund to your wallet to complete your request.");
            }
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusPending);
            input.StatusId = status.Id;
            var dbModel = input.MapTo<CreateAdvertTopBannerDto>();
            dbModel.AmountPaid = advertPrice.Amount;
            await _walletAppService.Debit(input.UserId.Value, dbModel.AmountPaid, false);
           await _walletAppService.LogWalletOperation(new WalletInfoDto
            {
                Reference = Guid.NewGuid().ToString(),
                Amount = dbModel.AmountPaid, 
                Title = "Top banner creation debit occurred",
                Description =  "A debit occurred in your wallet to complete your top banner creation.",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Debit,
                UserId = input.UserId.Value
            });
            
            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                Message = $"A debit of {dbModel.AmountPaid.ToNairaString()} occurred in your wallet to complete your top banner creation.",
                Severity = NotificationSeverity.Success,
                UserId = input.UserId.Value
            });
            
            var walletAdmin = await _walletAppService.GetAdminWallet();
            await _walletAppService.Credit(walletAdmin.UserId, dbModel.AmountPaid, false);
            var user = await _userAppService.Get(new EntityDto<long>{Id = input.UserId.Value});
            await _walletAppService.LogWalletOperation(new WalletInfoDto
            {
                Reference = Guid.NewGuid().ToString(),
                Amount = dbModel.AmountPaid, 
                Title = "Top banner creation credit occurred",
                Description =  $"A credit occurred in your wallet from '{user.NickName}' to complete the creation of top banner. Please <a href='/TopBanners/Index'>click here</a> to check and approve to the banner.",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Credit,
                UserId = walletAdmin.UserId
            });
            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                Message = $"A credit of {dbModel.AmountPaid.ToNairaString()} occurred in your wallet from '{user.NickName}' to complete the creation of top banner. Please <a href='/TopBanners/Index'>click here</a> to check and approve to the banner.",
                Severity = NotificationSeverity.Success,
                UserId = walletAdmin.UserId
            });
            
            //Todo: Make sure image dimension is 1920x80
            var image = await _assetUploadAppService.UploadImage(new UploadDto{File = input.Image, Path = AppConsts.ImageUploadFolder});
            dbModel.Image = image.FileName;
            dbModel.StatusId = status.Id;
            dbModel.EndDate =  dbModel.StartDate.AddDays(advertPrice.Days);
            return await base.Create(dbModel);
        }
        [AbpAuthorize]
        public async Task<AdvertTopBannerDto> UpdateAndUploadImage(CreateEditTopBannerDto input)
        {
            if (input.StartDate == null)
            {
                throw new UserFriendlyException("StartDate date must be at least one day ahead.");
            }
            if (input.ExternalUrl == null)
            {
                throw new UserFriendlyException("External url is required.");
            }
            if (input.StartDate > input.EndDate || input.StartDate == input.EndDate)
            {
                throw new UserFriendlyException("StartDate date must be less than end date.");
            }
            
            if (input.UserId == null)
            {
                throw new UserFriendlyException("User id is required.");
            }
            if (input.AdvertPriceId == null)
            {
                throw new UserFriendlyException("Duration is required.");
            }
            if (input.Id == null)
                        {
                            throw new UserFriendlyException("ID is required.");
                        }

            var itemToUpdate = await _repository.GetAsync(input.Id.Value);
            if (itemToUpdate == null)
            {
                throw new UserFriendlyException("Item not found.");
            }
            var wallet = await _walletAppService.GetByUserId(input.UserId.Value);
            if (wallet == null)
            {
                throw new UserFriendlyException("User wallet information not found.");
            }
            
            var advertPrice = await _advertPriceAppService.Get(new EntityDto{Id = input.AdvertPriceId.Value});
            
            if (itemToUpdate.AdvertPriceId != advertPrice.Id)
            {
                var advertPriceOld = await _advertPriceAppService.Get(new EntityDto{Id = itemToUpdate.AdvertPriceId});
                var priceDifference = advertPrice.Amount - itemToUpdate.AmountPaid;
                
                //Todo: Credit the admin if he advert owner upgraded the advert
                if (priceDifference > 0)
              {
                  if (wallet.Balance < priceDifference)
                  {
                      throw new UserFriendlyException("Insufficieent balance. Please add fund to your wallet to complete your request.");
                  }
                await _walletAppService.Debit(input.UserId.Value, priceDifference, false);
                await _walletAppService.LogWalletOperation(new WalletInfoDto
            {
                Reference = Guid.NewGuid().ToString(),
                Amount = priceDifference, 
                Title = "Top banner edit debit occurred",
                Description =  $"A top edit debit occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Debit,
                UserId = input.UserId.Value
            });
            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                Message = $"A top banner edit debit of {priceDifference.ToNairaString()} occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                Severity = NotificationSeverity.Success,
                UserId = input.UserId.Value
            });
            
            var walletAdmin = await _walletAppService.GetAdminWallet();
            await _walletAppService.Credit(walletAdmin.UserId, priceDifference, false);
            var user = await _userAppService.Get(new EntityDto<long>{Id = input.UserId.Value});
            await _walletAppService.LogWalletOperation(new WalletInfoDto
            {
                Reference = Guid.NewGuid().ToString(),
                Amount = priceDifference, 
                Title = "Top banner edit credit occurred",
                Description =  $"A top banner edit credit occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from '{user.NickName}' to complete the amount difference for the edit he initiated.",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Credit,
                UserId = walletAdmin.UserId
            });
            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                Message = $"A top banner edit credit of {priceDifference.ToNairaString()} occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from '{user.NickName}' to complete the amount difference for the edit he initiated.",
                Severity = NotificationSeverity.Success,
                UserId = walletAdmin.UserId
            });
                }
                else
                {
                    priceDifference = Math.Abs(priceDifference);
                    await _walletAppService.Credit(input.UserId.Value, priceDifference, false);
                    await _walletAppService.LogWalletOperation(new WalletInfoDto
                    {
                        Reference = Guid.NewGuid().ToString(),
                        Amount = priceDifference, 
                        Title = "Top banner edit credit occurred",
                        Description = $"A top banner edit credit occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                        WalletInfoResult = WalletInfoResult.Success,
                        WalletOperation = WalletOperation.Credit,
                        UserId = input.UserId.Value
                    });
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        Message = $"A top banner edit credit of {priceDifference.ToNairaString()} occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                        Severity = NotificationSeverity.Success,
                        UserId = input.UserId.Value
                    });
           
            
            var walletAdmin = await _walletAppService.GetAdminWallet();
            await _walletAppService.Debit(walletAdmin.UserId, priceDifference, false);
            var user = await _userAppService.Get(new EntityDto<long>{Id = input.UserId.Value});
           
                    await _walletAppService.LogWalletOperation(new WalletInfoDto
                    {
                        Reference = Guid.NewGuid().ToString(),
                        Amount = priceDifference, 
                        Title = "Top banner edit debit occurred",
                        Description =  $"A top banner edit debit occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from {user.NickName} to complete amount difference for the edit he initiated.",
                        WalletInfoResult = WalletInfoResult.Success,
                        WalletOperation = WalletOperation.Debit,
                        UserId = walletAdmin.UserId
                    });
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        Message = $"A top banner edit debit occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from {user.NickName} to complete amount difference for the edit he initiated.",
                        Severity = NotificationSeverity.Success,
                        UserId = walletAdmin.UserId
                    });
                    
                }
            }
            
            //Todo: Make sure image dimension is 1920x1200
            itemToUpdate.StartDate = input.StartDate.Value;
            itemToUpdate.AdvertPriceId = input.AdvertPriceId.Value;
            itemToUpdate.ExternalUrl = input.ExternalUrl;
            ImageUploadDto imageDto;
            if(input.Image != null)
            {
                imageDto = await _assetUploadAppService.UploadImage(new UploadDto{File = input.Image, Path = AppConsts.ImageUploadFolder});
                _assetUploadAppService.Delete(new DeleteAssetDto{Path = AppConsts.ImageUploadFolder + "/" + itemToUpdate.Image});
                itemToUpdate.Image = imageDto.FileName;
            }
            itemToUpdate.EndDate =  itemToUpdate.StartDate.AddDays(advertPrice.Days);
            itemToUpdate.AmountPaid =  advertPrice.Amount;
            await CurrentUnitOfWork.SaveChangesAsync();
            return MapToEntityDto(itemToUpdate);
        }

        [AbpAuthorize]
        public async Task<AdvertTopBannerDto> UpdateStatusAsync(UpdateStatusDto model)
        {
            var entry = await _repository.FirstOrDefaultAsync(itm => itm.Id == model.EntityId);
            var status =  await _statusAppService.GetByNameAsync(model.StatusName);
            var pendingStatus =  await _statusAppService.GetByNameAsync(AppConsts.StatusPending);
            if (entry == null || status == null)
            {
                return null;
            }

            if (status.Name.Equals(AppConsts.StatusLive) && pendingStatus.Id == entry.StatusId)
            {
                entry.StartDate = DateTime.Now;
            } 
            if (status.Name.Equals(AppConsts.StatusCancelled))
            {
                var daysAdvertRan = (DateTime.Now - entry.StartDate).Days;
                var totaldaysForAdvert = await _advertPriceAppService.Get(new EntityDto {Id = entry.AdvertPriceId});
                var amountUsed = entry.AmountPaid - (((decimal)daysAdvertRan * entry.AmountPaid) / (decimal)totaldaysForAdvert.Days);
                var adminWallet = await _walletAppService.GetAdminWallet();
                var currentUser = await _userAppService.GetCurrentUser();
                if (amountUsed > 0)
                {
                        await _walletAppService.Credit(entry.UserId, amountUsed, false);
                        await _walletAppService.LogWalletOperation(
                            new WalletInfoDto
                        {
                            Amount = amountUsed, 
                            Title = "Top banner cancellation credit",
                            Description =  $"Your top banner cancellation reversal credit occurred in your account. {amountUsed.ToNairaString()} was credited from {entry.AmountPaid.ToNairaString()} you paid because the advert has ran for {daysAdvertRan} days.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Credit,
                            UserId = entry.UserId,
                        }); 
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = entry.UserId,
                            TenantId = AbpSession.TenantId,
                            Message =  $"Your top banner cancellation reversal credit occurred in your account. {amountUsed.ToNairaString()} was credited from {entry.AmountPaid.ToNairaString()} you paid because the advert has ran for {daysAdvertRan} days.",
                        });
                        await _walletAppService.Debit(adminWallet.UserId, amountUsed, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Amount = amountUsed, 
                            Title = "Top banner cancellation debit",
                            Description =  $"Your top banner cancellation debit occurred in your account from '{currentUser.NickName}'. {amountUsed.ToNairaString()} was debited from {entry.AmountPaid.ToNairaString()} paid for the banner because the advert has ran for {daysAdvertRan} days.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Debit,
                            UserId = adminWallet.UserId,
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = adminWallet.UserId,
                            TenantId = AbpSession.TenantId,
                            Message = $"Your top banner cancellation debit occurred in your account from '{currentUser.NickName}'. {amountUsed.ToNairaString()} was debited from {entry.AmountPaid.ToNairaString()} paid for the banner because the advert has ran for {daysAdvertRan} days.",
                        });  
                    entry.StatusId = status.Id;
                    UnitOfWorkManager.Current.SaveChanges();
                    return entry.MapTo<AdvertTopBannerDto>();
                }
                else
                {
                    entry.StatusId = status.Id;
                    UnitOfWorkManager.Current.SaveChanges();
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        UserId = adminWallet.UserId,
                        Message = $"Top banner cancellation request from '{currentUser.NickName}' was completed successfully.",
                    });
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        UserId = entry.UserId,
                        Message = $"Your top banner cancellation request from '{currentUser.NickName}' was completed successfully.",
                    });
                    return entry.MapTo<AdvertTopBannerDto>();
                }
                    
            }
            entry.StatusId = status.Id;
            UnitOfWorkManager.Current.SaveChanges();
            return entry.MapTo<AdvertTopBannerDto>();
        }

        public async Task NotifyExpiring()
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var items = await _repository.GetAllIncluding(itm => itm.User).Where(x => x.EndDate <= DateTime.Now.AddDays(7) && x.StatusId == status.Id).ToListAsync();
           
            if (items != null && items.Count > 0)
            {
                items.ForEach(item =>
                {
                    BackgroundJob.Enqueue<BgJobTopBanner>(x => x.NotifyUserOfExpiringTopBanner(item.User, item));
                });
            }
        }

        public async Task DisableExpired()
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var items = await _repository.GetAllIncluding(itm => itm.User).Where(x => x.EndDate <= DateTime.Now && x.StatusId == status.Id).ToListAsync();
            if (items != null && items.Count > 0)
            {
                var closedStatus = await _statusAppService.GetByNameAsync(AppConsts.StatusExpired);
                items.ForEach(item =>
                {
                    item.StatusId = closedStatus.Id;
                    BackgroundJob.Enqueue<BgJobTopBanner>(x => x.NotifyUserOfExpiredTopBanner(item.User, item));

                });
                await CurrentUnitOfWork.SaveChangesAsync();
            }
        }
    }
}