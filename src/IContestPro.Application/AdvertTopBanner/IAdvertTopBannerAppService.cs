﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.AdvertTopBanner.Dto;
using IContestPro.Contest.Dto;

namespace IContestPro.AdvertTopBanner
{
    public interface IAdvertTopBannerAppService : IBaseAppService<AdvertTopBannerDto, int, PagedResultRequestDto, CreateAdvertTopBannerDto, AdvertTopBannerDto>
    {
        Task<AdvertTopBannerDto> GetRandomLive(int totalTake = 1);
        Task<PagedResultDto<AdvertTopBannerDto>> GetAllByUser(long userId, PagedResultRequestDto input);
        Task<PagedResultDto<AdvertTopBannerDto>> GetAllByStatus(string statusName, PagedResultRequestDto input);
        Task<AdvertTopBannerDto> CreateAndUploadImage(CreateEditTopBannerDto input);
        Task<AdvertTopBannerDto> UpdateAndUploadImage(CreateEditTopBannerDto input);
        Task<AdvertTopBannerDto> UpdateStatusAsync(UpdateStatusDto model);
        Task NotifyExpiring();
        Task DisableExpired();
    }
}