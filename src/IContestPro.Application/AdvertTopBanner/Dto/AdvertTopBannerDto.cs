﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.AdvertPrice.Dto;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.AdvertTopBanner.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.AdvertTopBanner))]
    public class AdvertTopBannerDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Image { get; set; }
        public int StatusId { get; set; }
        public int UserId { get; set; }
        public decimal AmountPaid { get; set; }
        public int AdvertPriceId { get; set; }
        public string ExternalUrl { get; set; }
        
        public StatusDto Status { get; set; }
        public UserDto User { get; set; }
        public AdvertPriceDto AdvertPrice { get; set; }
        public int? TenantId { get; set; }
    }
}