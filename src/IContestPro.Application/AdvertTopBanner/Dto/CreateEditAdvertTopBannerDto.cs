﻿using System;
using Abp.AutoMapper;
using Microsoft.AspNetCore.Http;

namespace IContestPro.AdvertTopBanner.Dto
{
    [AutoMapTo(typeof(CreateAdvertTopBannerDto))]
    [AutoMapFrom(typeof(AdvertTopBannerDto))]
    public class CreateEditTopBannerDto
    {
        
        public int? Id { get; set; }
        public int? TenantId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public IFormFile Image { get; set; }
        public int? StatusId { get; set; }
        public long? UserId { get; set; }
        public int? AdvertPriceId { get; set; }
        public string ExternalUrl { get; set; }

    }
}