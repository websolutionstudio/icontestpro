﻿namespace IContestPro.FeaturedContestEntry.Dto
{
    public class ContestEntriesToPromoteDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}