﻿using System;
using Abp.AutoMapper;

namespace IContestPro.FeaturedContestEntry.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.FeaturedContest))]
    public class RandomFeaturedContestEntriesDto
    {
        public int Id { get; set; }
        public int ContestEntryId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool ContestEntryIsFeatured { get; set; }
        public string ContestEntryImage { get; set; }
        public string ContestEntryVideo { get; set; }
        public string ContestEntryPoster { get; set; }
        public string ContestEntryType { get; set; }
        public string ContestEntryPermalink { get; set; }
        public string UserNickName { get; set; }
        public string ContestTitle { get; set; }
    }
}