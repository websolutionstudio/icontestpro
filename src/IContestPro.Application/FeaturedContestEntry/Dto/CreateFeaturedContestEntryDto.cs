﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.FeaturedContestEntry.Dto
{
    [AutoMapTo(typeof(IContestProEntities.FeaturedContestEntry))]
    public class CreateFeaturedContestEntryDto
    {
        [Required]
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
         public int StatusId { get; set; }
        [Required]
        public int AdvertPriceId { get; set; }
        [Required]
        public decimal AmountPaid { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int ContestEntryId { get; set; }
    }
}