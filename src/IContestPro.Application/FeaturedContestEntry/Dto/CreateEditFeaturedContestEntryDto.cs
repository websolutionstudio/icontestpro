﻿using System;
using Abp.AutoMapper;

namespace IContestPro.FeaturedContestEntry.Dto
{
    [AutoMapTo(typeof(CreateFeaturedContestEntryDto))]
    [AutoMapFrom(typeof(FeaturedContestEntryDto))]
    public class CreateEditFeaturedContestEntryDto
    {
        
        public int? Id { get; set; }
        public int? TenantId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? StatusId { get; set; }
        public long? UserId { get; set; }
        public int? AdvertPriceId { get; set; }
        public int? ContestEntryId { get; set; }
        
    }
}