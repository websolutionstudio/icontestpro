﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.FeaturedContestEntry.Dto;
using IContestPro.Contest.Dto;

namespace IContestPro.FeaturedContestEntry
{
    public interface IFeaturedContestEntryAppService : IBaseAppService<FeaturedContestEntryDto, int, PagedResultRequestDto, CreateFeaturedContestEntryDto, FeaturedContestEntryDto>
    {
        Task<IEnumerable<RandomFeaturedContestEntriesDto>> GetRandomLive(int pageSize = 5);
        Task<PagedResultDto<FeaturedContestEntryDto>> GetAllByUser(long userId, PagedResultRequestDto input);
        Task<PagedResultDto<FeaturedContestEntryDto>> GetAllByStatus(string statusName, PagedResultRequestDto input);
        Task<FeaturedContestEntryDto> UpdateStatusAsync(UpdateStatusDto model);

        Task NotifyExpiring();
        Task DisableExpired();
        Task<IEnumerable<ContestEntriesToPromoteDto>> GetContestEntriesToPromote(long userId);
    }
}