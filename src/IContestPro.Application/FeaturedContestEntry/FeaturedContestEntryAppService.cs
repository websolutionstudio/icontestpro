﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Notifications;
using Abp.UI;
using Hangfire;
using IContestPro.Base;
using IContestPro.FeaturedContestEntry.Dto;
using IContestPro.AdvertPrice;
using IContestPro.BackgroundJobs;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Contest.Dto;
using IContestPro.EntityFrameworkCore;
using IContestPro.EntityFrameworkCore.StoreProcedureManager;
using IContestPro.Helpers;
using IContestPro.IContestProEntities;
using IContestPro.Status;
using IContestPro.Users;
using IContestPro.Wallet;
using IContestPro.Wallet.Dto;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.FeaturedContestEntry
{
     public class FeaturedContestEntryAppService : BaseAppService<IContestProEntities.FeaturedContestEntry, FeaturedContestEntryDto, int,
                                PagedResultRequestDto, CreateFeaturedContestEntryDto, FeaturedContestEntryDto>, IFeaturedContestEntryAppService
    {
        private readonly IRepository<IContestProEntities.FeaturedContestEntry> _repository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAdvertPriceAppService _advertPriceAppService;
        private readonly IWalletAppService _walletAppService;
        private readonly IUserAppService _userAppService;
        private readonly IContestProDbContextFactory _contestProDbContextFactory;
        private readonly IRepository<ContestEntry> _contestEntryRepository;
        public FeaturedContestEntryAppService(IRepository<IContestProEntities.FeaturedContestEntry> repository,
            IStatusAppService statusAppService, IAdvertPriceAppService advertPriceAppService, 
            IWalletAppService walletAppService, IUserAppService userAppService, IIocResolver  iocResolver,
            IContestProDbContextFactory contestProDbContextFactory,
            IRepository<ContestEntry> contestEntryRepository) : base(repository, iocResolver)
        {
            _repository = repository;
            _statusAppService = statusAppService;
            _advertPriceAppService = advertPriceAppService;
            _walletAppService = walletAppService;
            _userAppService = userAppService;
            _contestProDbContextFactory = contestProDbContextFactory;
            _contestEntryRepository = contestEntryRepository;

        }
        
        public async Task<IEnumerable<RandomFeaturedContestEntriesDto>> GetRandomLive(int pageSize = 5)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetRandomFeaturedContestEntries)
                    .WithSqlParam("PageSize", pageSize);
                var items = await cmd.ExecuteStoredProcedureListAsync<RandomFeaturedContestEntriesDto>();
                return items;
            }
        } 
        public async Task<IEnumerable<ContestEntriesToPromoteDto>> GetContestEntriesToPromote(long userId)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetContestEntriesToPromote)
                    .WithSqlParam("userId", userId);
                var items = await cmd.ExecuteStoredProcedureListAsync<ContestEntriesToPromoteDto>();
                return items;
            }
        } 
        [AbpAuthorize]
        public override async Task<PagedResultDto<FeaturedContestEntryDto>> GetAll(PagedResultRequestDto input)
        {
            
            var query =  _repository.GetAllIncluding(item => item.Status, item => item.ContestEntry);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            
            return new PagedResultDto<FeaturedContestEntryDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        [AbpAuthorize]
        public async Task<PagedResultDto<FeaturedContestEntryDto>> GetAllByStatus(string statusName, PagedResultRequestDto input)
        {
            var status = await _statusAppService.GetByNameAsync(statusName);
            if (status == null)
            {
                return null;
            }
            var query =  _repository.GetAllIncluding(item => item.Status, item => item.ContestEntry).Where(item => item.StatusId == status.Id);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            
            return new PagedResultDto<FeaturedContestEntryDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        [AbpAuthorize]
        public async Task<PagedResultDto<FeaturedContestEntryDto>> GetAllByUser(long userId, PagedResultRequestDto input)
        {
            var query =  _repository.GetAllIncluding(item => item.Status, item => item.ContestEntry).Where(item => item.UserId == userId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<FeaturedContestEntryDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        
       
        [AbpAuthorize]
         public override async Task<FeaturedContestEntryDto> Create(CreateFeaturedContestEntryDto input)
        {
            if (input.StartDate == null)
            {
                throw new UserFriendlyException("StartDate date must be at least one day ahead.");
            }
            
            
            var wallet = await _walletAppService.GetByUserId(input.UserId);
            if (wallet == null)
            {
                throw new UserFriendlyException("User wallet information not found.");
            }
            var advertPrice = await _advertPriceAppService.Get(new EntityDto{Id = input.AdvertPriceId});
            if (wallet.Balance < advertPrice.Amount)
            {
                throw new UserFriendlyException("Insufficieent balance. Please add fund to your wallet to complete your request.");
            }
            var contestEntry = await _contestEntryRepository.FirstOrDefaultAsync(item => item.Id == input.ContestEntryId);
            if (contestEntry == null)
            {
                throw new UserFriendlyException("Contest entry not found.");
            }
            
            if (contestEntry.IsFeatured)
            {
                throw new UserFriendlyException("This entry is already featured. Please select a diffrent entry to promote.");
            }
            var balanceBefore = await _walletAppService.GetBalance(input.UserId);
            var walletAdmin = await _walletAppService.GetAdminWallet();
            var user = await _userAppService.Get(new EntityDto<long> {Id = input.UserId});
           
                    try
                    {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusPending);
            input.StatusId = status.Id;
            var dbModel = input.MapTo<CreateFeaturedContestEntryDto>();
            dbModel.AmountPaid = advertPrice.Amount;
            await _walletAppService.Debit(input.UserId, dbModel.AmountPaid, false);
           await _walletAppService.LogWalletOperation(new WalletInfoDto
            {
                Reference = Guid.NewGuid().ToString(),
                Amount = dbModel.AmountPaid, 
                Title = "Featured contest entry creation debit occurred",
                Description =  "A debit occurred in your wallet to complete your featured contest entry creation.",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Debit,
                UserId = input.UserId
            });
            
            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                Message = $"A debit of {dbModel.AmountPaid.ToNairaString()} occurred in your wallet to complete your featured contest entry creation.",
                Severity = NotificationSeverity.Success,
                UserId = input.UserId
            });
            
            await _walletAppService.Credit(walletAdmin.UserId, dbModel.AmountPaid, false);
            await _walletAppService.LogWalletOperation(new WalletInfoDto
            {
                Reference = Guid.NewGuid().ToString(),
                Amount = dbModel.AmountPaid, 
                Title = "Featured contest entry creation credit occurred",
                Description =  $"A credit occurred in your wallet from '{user.NickName}' to complete the creation of featured contest entry.",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Credit,
                UserId = walletAdmin.UserId
            });
            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                Message = $"A credit of {dbModel.AmountPaid.ToNairaString()} occurred in your wallet from '{user.NickName}' to complete the creation of featured contest entry.",
                Severity = NotificationSeverity.Success,
                UserId = walletAdmin.UserId
            });
            
            dbModel.StatusId = status.Id;
            dbModel.EndDate =  dbModel.StartDate.AddDays(advertPrice.Days);
            var result = await base.Create(dbModel);
            contestEntry.IsFeatured = true;
            await CurrentUnitOfWork.SaveChangesAsync();  //return res;
            return result;
                    }
                    catch (Exception)
                    {
                      var balanceNow = await _walletAppService.GetBalance(input.UserId);
                        if (balanceBefore != balanceNow)
                        {
                        await _walletAppService.Credit(input.UserId, advertPrice.Amount, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Reference = Guid.NewGuid().ToString(),
                            Amount = advertPrice.Amount,
                            Title = "Featured contest entry creation reversal credit occurred",
                            Description = "A reversal credit occurred in your wallet for the featured contest entry creation you initiated which was not successful.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Credit,
                            UserId = input.UserId
                        });

                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            Message = $"A reversal credit of NGN{advertPrice.Amount.ToString("N")} occurred in your wallet for the featured contest entry creation you initiated which was not successful.",
                            Severity = NotificationSeverity.Error,
                            UserId = input.UserId
                        });

                        await _walletAppService.Debit(walletAdmin.UserId, advertPrice.Amount, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Reference = Guid.NewGuid().ToString(),
                            Amount = advertPrice.Amount,
                            Title = "Featured contest entry creation reversal debit occurred",
                            Description =
                                $"A reversal debit of NGN{advertPrice.Amount.ToString("N")} occurred in your wallet for the featured contest entry creation which '{user.NickName}' initiated which was not successful.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Debit,
                            UserId = walletAdmin.UserId
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            Message =
                                $"A reversal debit of NGN{advertPrice.Amount.ToString("N")} occurred in your wallet for the featured contest entry creation which '{user.NickName}' initiated which was not successful.",
                            Severity = NotificationSeverity.Error,
                            UserId = walletAdmin.UserId
                        });

                        }
                        throw;
                    }
           
               
        }
        [AbpAuthorize]
        public override async Task<FeaturedContestEntryDto> Update(FeaturedContestEntryDto input)
        {
            if (input.StartDate == null)
            {
                throw new UserFriendlyException("StartDate date must be at least one day ahead.");
            }
           
            var itemToUpdate = await _repository.GetAsync(input.Id);
            if (itemToUpdate == null)
            {
                throw new UserFriendlyException("Item not found.");
            }
            var wallet = await _walletAppService.GetByUserId(input.UserId);
            if (wallet == null)
            {
                throw new UserFriendlyException("User wallet information not found.");
            }
            
            var advertPrice = await _advertPriceAppService.Get(new EntityDto{Id = input.AdvertPriceId});
            using (var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
            if (itemToUpdate.AdvertPriceId != advertPrice.Id)
            {
                var advertPriceOld = await _advertPriceAppService.Get(new EntityDto{Id = itemToUpdate.AdvertPriceId});
                var priceDifference = advertPrice.Amount - itemToUpdate.AmountPaid;
                
                //Todo: Credit the admin if he advert owner upgraded the advert
                if (priceDifference > 0)
              {
                  if (wallet.Balance < priceDifference)
                  {
                      throw new UserFriendlyException("Insufficieent balance. Please add fund to your wallet to complete your request.");
                  }

                 
                              await _walletAppService.Debit(input.UserId, priceDifference, false);
                              await _walletAppService.LogWalletOperation(new WalletInfoDto
                              {
                                  Reference = Guid.NewGuid().ToString(),
                                  Amount = priceDifference,
                                  Title = "Featured contest entry edit debit occurred",
                                  Description =
                                      $"A featured contest entry edit debit occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                                  WalletInfoResult = WalletInfoResult.Success,
                                  WalletOperation = WalletOperation.Debit,
                                  UserId = input.UserId
                              });
                              await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                              {
                                  Message =
                                      $"A featured contest entry edit debit of {priceDifference.ToNairaString()} occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                                  Severity = NotificationSeverity.Success,
                                  UserId = input.UserId
                              });

                              var walletAdmin = await _walletAppService.GetAdminWallet();
                              await _walletAppService.Credit(walletAdmin.UserId, priceDifference, false);
                              var user = await _userAppService.Get(new EntityDto<long> {Id = input.UserId});
                              await _walletAppService.LogWalletOperation(new WalletInfoDto
                              {
                                  Reference = Guid.NewGuid().ToString(),
                                  Amount = priceDifference,
                                  Title = "Featured contest entry edit credit occurred",
                                  Description =
                                      $"A featured contest entry edit credit occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from '{user.NickName}' to complete the amount difference for the edit he initiated.",
                                  WalletInfoResult = WalletInfoResult.Success,
                                  WalletOperation = WalletOperation.Credit,
                                  UserId = walletAdmin.UserId
                              });
                              await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                              {
                                  Message =
                                      $"A featured contest entry edit credit of {priceDifference.ToNairaString()} occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from '{user.NickName}' to complete the amount difference for the edit he initiated.",
                                  Severity = NotificationSeverity.Success,
                                  UserId = walletAdmin.UserId
                              });
                             
              }
                else
                {
                    priceDifference = Math.Abs(priceDifference);
                    await _walletAppService.Credit(input.UserId, priceDifference, false);
                    await _walletAppService.LogWalletOperation(new WalletInfoDto
                    {
                        Reference = Guid.NewGuid().ToString(),
                        Amount = priceDifference, 
                        Title = "Featured contest entry edit credit occurred",
                        Description = $"A featured contest entry edit credit occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                        WalletInfoResult = WalletInfoResult.Success,
                        WalletOperation = WalletOperation.Credit,
                        UserId = input.UserId
                    });
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        Message = $"A featured contest entry edit credit of {priceDifference.ToNairaString()} occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                        Severity = NotificationSeverity.Success,
                        UserId = input.UserId
                    });
           
            
            var walletAdmin = await _walletAppService.GetAdminWallet();
            await _walletAppService.Debit(walletAdmin.UserId, priceDifference, false);
            var user = await _userAppService.Get(new EntityDto<long>{Id = input.UserId});
           
                    await _walletAppService.LogWalletOperation(new WalletInfoDto
                    {
                        Reference = Guid.NewGuid().ToString(),
                        Amount = priceDifference, 
                        Title = "Featured contest entry edit debit occurred",
                        Description =  $"A top edit debit occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from {user.NickName} to complete amount difference for the edit he initiated.",
                        WalletInfoResult = WalletInfoResult.Success,
                        WalletOperation = WalletOperation.Debit,
                        UserId = walletAdmin.UserId
                    });
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        Message = $"A featured contest entry edit debit occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from {user.NickName} to complete amount difference for the edit he initiated.",
                        Severity = NotificationSeverity.Success,
                        UserId = walletAdmin.UserId
                    });
                    
                }
            }
            
            itemToUpdate.StartDate = input.StartDate;
            itemToUpdate.AdvertPriceId = input.AdvertPriceId;
            itemToUpdate.EndDate =  itemToUpdate.StartDate.AddDays(advertPrice.Days);
            itemToUpdate.AmountPaid =  advertPrice.Amount;
            await CurrentUnitOfWork.SaveChangesAsync();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }

                }
            }
            return MapToEntityDto(itemToUpdate);
        }

        [AbpAuthorize]
        public async Task<FeaturedContestEntryDto> UpdateStatusAsync(UpdateStatusDto model)
        {
            var entry = await _repository.FirstOrDefaultAsync(itm => itm.Id == model.EntityId);
            var status =  await _statusAppService.GetByNameAsync(model.StatusName);
            var pendingStatus =  await _statusAppService.GetByNameAsync(AppConsts.StatusPending);
            if (entry == null || status == null)
            {
                return null;
            }
            using (var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
            if (status.Name.Equals(AppConsts.StatusLive) && pendingStatus.Id == entry.StatusId)
            {
                entry.StartDate = DateTime.Now;
            } 
            if (status.Name.Equals(AppConsts.StatusCancelled))
            {
                var daysAdvertRan = (DateTime.Now - entry.StartDate).Days;
                var totaldaysForAdvert = await _advertPriceAppService.Get(new EntityDto {Id = entry.AdvertPriceId});
                var amountUsed = entry.AmountPaid - (((decimal)daysAdvertRan * entry.AmountPaid) / (decimal)totaldaysForAdvert.Days);
                var adminWallet = await _walletAppService.GetAdminWallet();
                var currentUser = await _userAppService.GetCurrentUser();
                if (amountUsed > 0)
                {
                        await _walletAppService.Credit(entry.UserId, amountUsed, false);
                        await _walletAppService.LogWalletOperation(
                            new WalletInfoDto
                        {
                            Amount = amountUsed, 
                            Title = "Featured contest entry cancellation credit",
                            Description =  $"Your featured contest entry cancellation reversal credit occurred in your account. {amountUsed.ToNairaString()} was credited from {entry.AmountPaid.ToNairaString()} you paid because the advert has ran for {daysAdvertRan} days.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Credit,
                            UserId = entry.UserId,
                        }); 
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = entry.UserId,
                            TenantId = AbpSession.TenantId,
                            Message =  $"Your featured contest entry cancellation reversal credit occurred in your account. {amountUsed.ToNairaString()} was credited from {entry.AmountPaid.ToNairaString()} you paid because the advert has ran for {daysAdvertRan} days.",
                        });
                        await _walletAppService.Debit(adminWallet.UserId, amountUsed, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Amount = amountUsed, 
                            Title = "Featured contest entry cancellation debit",
                            Description =  $"Your featured contest entry cancellation debit occurred in your account from '{currentUser.NickName}'. {amountUsed.ToNairaString()} was debited from {entry.AmountPaid.ToNairaString()} paid for the banner because the advert has ran for {daysAdvertRan} days.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Debit,
                            UserId = adminWallet.UserId,
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = adminWallet.UserId,
                            TenantId = AbpSession.TenantId,
                            Message = $"Your featured contest entry cancellation debit occurred in your account from '{currentUser.NickName}'. {amountUsed.ToNairaString()} was debited from {entry.AmountPaid.ToNairaString()} paid for the banner because the advert has ran for {daysAdvertRan} days.",
                        });  
                    entry.StatusId = status.Id;
                    UnitOfWorkManager.Current.SaveChanges();
                    return entry.MapTo<FeaturedContestEntryDto>();
                }
                else
                {
                    entry.StatusId = status.Id;
                    UnitOfWorkManager.Current.SaveChanges();
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        UserId = adminWallet.UserId,
                        Message = $"Featured contest entry cancellation request from '{currentUser.NickName}' was completed successfully.",
                    });
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        UserId = entry.UserId,
                        Message = $"Your featured contest entry cancellation request from '{currentUser.NickName}' was completed successfully.",
                    });
                    return entry.MapTo<FeaturedContestEntryDto>();
                }
                    
            }
            entry.StatusId = status.Id;
            UnitOfWorkManager.Current.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }

                }
            }
            return entry.MapTo<FeaturedContestEntryDto>();
        }
        
        public async Task NotifyExpiring()
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var items = await _repository.GetAllIncluding(itm => itm.User).Where(x => x.EndDate <= DateTime.Now.AddDays(7) && x.StatusId == status.Id).ToListAsync();
           
            if (items != null && items.Count > 0)
            {
                items.ForEach(item =>
                {
                    BackgroundJob.Enqueue<BgJobFeaturedContestEntries>(x => x.NotifyUserOfExpiring(item.User, item));
                });
            }
        }

        public async Task DisableExpired()
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var items = await _repository.GetAllIncluding(itm => itm.User).Where(x => x.EndDate <= DateTime.Now && x.StatusId == status.Id).ToListAsync();
            if (items != null && items.Count > 0)
            {
                var closedStatus = await _statusAppService.GetByNameAsync(AppConsts.StatusExpired);
                items.ForEach(item =>
                {
                    item.StatusId = closedStatus.Id;
                    var entry = _contestEntryRepository.Get(item.ContestEntryId);
                    entry.IsFeatured = false;
                    BackgroundJob.Enqueue<BgJobFeaturedContestEntries>(x => x.NotifyUserOfExpired(item.User, item));

                });
               
                await CurrentUnitOfWork.SaveChangesAsync();
            }
        }
    }
}