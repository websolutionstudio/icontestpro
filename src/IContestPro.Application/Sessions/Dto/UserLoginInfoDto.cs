﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using IContestPro.Authorization.Users;

namespace IContestPro.Sessions.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserLoginInfoDto : EntityDto<long>
    {
        public string Name { get; set; }
        public string NickName { get; set; }
        public string PhoneNumber { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }
        
        public string CountryId { get; set; }
        public int? StateId { get; set; }
        public string Profession { get; set; }

        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string Image { get; set; }
        public string FaceBook { get; set; }
        public string LinkedIn { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
        public string Permalink { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
         public bool IsPhoneNumberConfirmed { get; set; }
          public bool IsActive { get; set; }
          public string FullName { get; set; }
           public DateTime? LastLoginTime { get; set; }
           public string UserAppIdentity { get; set; }

    }
}
