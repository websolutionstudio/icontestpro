﻿using IContestPro.UserVotePoint.Dto;
using IContestPro.Wallet.Dto;

namespace IContestPro.Sessions.Dto
{
    public class GetCurrentLoginInformationsOutput
    {
        public ApplicationInfoDto Application { get; set; }

        public UserLoginInfoDto User { get; set; }
        public WalletDto Wallet { get; set; }

        public TenantLoginInfoDto Tenant { get; set; }
        public UserVotePointDto VotePoint { get; set; }
        public int Followers { get; set; }
        public int Following { get; set; }
    }
}
