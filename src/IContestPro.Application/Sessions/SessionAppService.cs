﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Runtime.Session;
using Hangfire;
using IContestPro.BackgroundJobs;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Cookie;
using IContestPro.CountryState;
using IContestPro.Follow;
using IContestPro.Sessions.Dto;
using IContestPro.UserVotePoint;
using IContestPro.UserVotePoint.Dto;
using IContestPro.Wallet;
using IContestPro.Wallet.Dto;

namespace IContestPro.Sessions
{
    public class SessionAppService : IContestProAppServiceBase, ISessionAppService
    {
        private readonly ICountryStateAppService _countryStateAppService;
        private readonly IWalletAppService _walletAppService;
        private readonly IUserVotePointAppService _userVotePointAppService;
        private readonly IFollowAppService _followAppService;
        private readonly ICookieAppService _cookieAppService;

        public SessionAppService(ICountryStateAppService countryStateAppService,
                 IWalletAppService walletAppService, 
                    IUserVotePointAppService userVotePointAppService, IFollowAppService followAppService, ICookieAppService cookieAppService)
        {
            _countryStateAppService = countryStateAppService;
            _walletAppService = walletAppService;
            _userVotePointAppService = userVotePointAppService;
            _followAppService = followAppService;
            _cookieAppService = cookieAppService;
        }

        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations(bool forceRefresh = false)
        {
            
            
            var output = GetCurrentLoginInformationsFromCookie() ?? new GetCurrentLoginInformationsOutput
            {
                Application = new ApplicationInfoDto
                {
                    Version = AppVersionHelper.Version,
                    ReleaseDate = AppVersionHelper.ReleaseDate,
                    Features = new Dictionary<string, bool>()
                }
            };

            if (!forceRefresh)
            {
                if (output.Tenant == null)
                {
                    if (AbpSession.TenantId.HasValue)
                    {
                        output.Tenant = ObjectMapper.Map<TenantLoginInfoDto>(await GetCurrentTenantAsync());
                    } 
                }  
            }
            else
            {
                if (AbpSession.TenantId.HasValue)
                {
                    output.Tenant = ObjectMapper.Map<TenantLoginInfoDto>(await GetCurrentTenantAsync());
                }  
            }
            
           
            if (AbpSession.UserId.HasValue)
            {
                if (!forceRefresh)
                {
                    if (output.User == null)
                    {
                        output.User = ObjectMapper.Map<UserLoginInfoDto>(await GetCurrentUserAsync());
                        output.User.UserAppIdentity = GetCurrentUserAppIdentity();
                        if (output.User.CountryId != null)
                        {
                            output.User.CountryName =
                                (await _countryStateAppService.Get(new EntityDto<string> {Id = output.User.CountryId}))
                                ?.Name;
                        }

                        if (output.User.StateId != null)
                        {
                            output.User.StateName =
                                (await _countryStateAppService.GetStateByIdAsync(output.User.StateId.Value))?.Name;
                        }
                    }
                }
                else
                {
                    output.User = ObjectMapper.Map<UserLoginInfoDto>(await GetCurrentUserAsync());
                    output.User.UserAppIdentity = GetCurrentUserAppIdentity();
                    if (output.User.CountryId != null)
                    {
                        output.User.CountryName =
                            (await _countryStateAppService.Get(new EntityDto<string> {Id = output.User.CountryId}))
                            ?.Name;
                    }

                    if (output.User.StateId != null)
                    {
                        output.User.StateName =
                            (await _countryStateAppService.GetStateByIdAsync(output.User.StateId.Value))?.Name;
                    } 
                }



                var wallet = await _walletAppService.GetByUserId(AbpSession.UserId.Value);
                if (wallet == null)
                {
                     wallet = await _walletAppService.Create(new CreateWalletDto
                    {
                        UserId = AbpSession.UserId.Value,
                        Balance = 0,
                        LastCreditedAmount = 0,
                        LastDebitedAmount = 0
                    });
                }
                 output.Wallet = wallet;
                 output.Wallet.User = null;
                var votePoint = await _userVotePointAppService.GetByUserId(AbpSession.UserId.Value);
                if (votePoint == null)
                {
                    votePoint = await _userVotePointAppService.Create(new CreateUserVotePointDto
                    {
                        UserId = AbpSession.UserId.Value,
                       Points = 0
                    });
                }
                 output.VotePoint = votePoint;
                 output.VotePoint.User = null;
                 output.Followers =  await _followAppService.CountFollowersByUserId(AbpSession.UserId.Value);
                 output.Following = await _followAppService.CountFollowingByUserId(AbpSession.UserId.Value);
                BackgroundJob.Enqueue<BgJobSubscribeToAllAvailableNotifications>(x => x.Subscribe(
                    new BgJobSubscribeToAllAvailableNotificationsDto
                    {
                        TenantId = AbpSession.GetTenantId(),
                        UserId = AbpSession.GetUserId()
                    }));

            }
            var userString = SerializeObject(output);
            _cookieAppService.Set(AppConsts.CookieKeyCurrentLoginInformations, Encrypt(userString), DateTime.Now.AddDays(1));
            return output;
        }

        private GetCurrentLoginInformationsOutput GetCurrentLoginInformationsFromCookie()
        {
            var cookieUserString = _cookieAppService.Get(AppConsts.CookieKeyCurrentLoginInformations);
            if (cookieUserString.Value != null)
            {
                var decrypted = Decrypt(cookieUserString.Value);
                return DeserializeObject<GetCurrentLoginInformationsOutput>(decrypted);
            }
            return null;
        }
    }
}
