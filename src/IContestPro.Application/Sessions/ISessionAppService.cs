﻿using System.Threading.Tasks;
using Abp.Application.Services;
using IContestPro.Sessions.Dto;

namespace IContestPro.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations(bool forceRefresh = false);
    }
}
