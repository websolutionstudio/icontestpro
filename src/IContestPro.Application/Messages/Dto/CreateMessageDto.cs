﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Messages.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Message))]
    public class CreateMessageDto
    {
        [Required]
        public long UserId { get; set; }
        [Required]
        public long ToUserId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string MessageText { get; set; }
        public string Attachments { get; set; }
        [Required]
        public int MessageType { get; set; }
        [Required]
        public int StatusId { get; set; }
    }
}