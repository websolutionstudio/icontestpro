﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Messages.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.MessageResponse))]
    public class MessageResponseDto : FullAuditedEntityDto
    {
        public long UserId { get; set; }
        public int MessageId { get; set; }
        public string Message { get; set; }
        public string Attachments { get; set; }
        public int StatusId { get; set; }
        public int? TenantId { get; set; }
        
        public UserDto User { get; set; }
        public StatusDto Status { get; set; }
    }
}