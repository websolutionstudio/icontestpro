﻿using System;

namespace IContestPro.Messages.Dto
{
    public class UserMessageResponseDto
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string Attachments { get; set; }
        public DateTime CreationTime { get; set; }
        public int MessageId { get; set; }
        public long UserId { get; set; }
        public string UserImage { get; set; }
        public string UserNickName { get; set; }
        
       //r.Id, r.Message, r.Attachments,  r.CreationTime, r.MessageId, u.Id UserId, u.Image UserImage, u.NickName UserNickName
    }
}