﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace IContestPro.Messages.Dto
{
    public class CreateMessageInputDto
    {
        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        [StringLength(1000)]
        [Required]
        public string MessageText { get; set; }
        [Required]
        public int MessageType { get; set; }
        public string Recipients { get; set; }
        public IFormFileCollection Attachments { get; set; }
        public int? ContestId { get; set; }
        public MessageTo MessageTo { get; set; }
    }
}