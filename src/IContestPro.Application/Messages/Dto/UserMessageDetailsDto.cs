﻿using System;
using System.Collections.Generic;

namespace IContestPro.Messages.Dto
{
    public class UserMessageDetailsDto
    {
        public int Id { get; set; }
        public long FromUserId { get; set; }
        public long ToUserId { get; set; }
        public string FromImage { get; set; }
        public string FromNickName { get; set; }
        public string FromPermalink { get; set; }
        public int MessageType { get; set; }
        public string Title { get; set; }
        public string MessageText { get; set; }
        public string Attachments { get; set; }
        public DateTime CreationTime { get; set; }
        public string StatusName { get; set; }
        public int StatusId { get; set; }
        public ICollection<UserMessageResponseDto> Responses { get; set; }
        
        
    }
}