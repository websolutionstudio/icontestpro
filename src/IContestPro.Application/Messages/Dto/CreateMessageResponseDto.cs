﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace IContestPro.Messages.Dto
{
    public class CreateMessageResponseDto
    {
        [Required]
        public long MessageSenderUserId{ get; set; }
        [Required]
        public int MessageId{ get; set; }
        [Required]
        public int MessageType{ get; set; }
        [Required]
        public string Message{ get; set; }
        public bool CloseTicket{ get; set; }
        public IFormFileCollection Attachments { get; set; }
    }
}