﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Messages.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Message))]
    public class MessageDto : FullAuditedEntityDto
    {
        public long UserId { get; set; }
        public long ToUserId { get; set; }
        public string Title { get; set; }
        public string MessageText { get; set; }
        public string Attachments { get; set; }
        public int StatusId { get; set; }
        public MessageType MessageType { get; set; }

        
        public UserDto User { get; set; }
        public UserDto ToUser { get; set; }
        public StatusDto Status { get; set; }
        //public ICollection<MessageResponseDto> MessageResponses { get; set; }
    }
}