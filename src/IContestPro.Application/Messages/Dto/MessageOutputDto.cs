﻿using System.Collections.Generic;

namespace IContestPro.Messages.Dto
{
    public class MessageOutputDto
    {
        public List<MessageDto> Results { get; set; }

    }
}