﻿using System;

namespace IContestPro.Messages.Dto
{
    public class UserMessageDto
    {
        public int Id { get; set; }
        public long FromUserId { get; set; }
        public string FromImage { get; set; }
        public string FromNickName { get; set; }
        public string FromPermalink { get; set; }
        
        public long ToUserId { get; set; }
        public string ToNickName { get; set; }
        public string ToImage { get; set; }
        public string ToPermalink { get; set; }
        
        public int MessageType { get; set; }
        public string Title { get; set; }
        public string MessageText { get; set; }
        public string Attachments { get; set; }
        public DateTime CreationTime { get; set; }
        public string StatusName { get; set; }
        public int StatusId { get; set; }
        public int TotalItemsCount { get; set; }
        
        //u.Id FromUserId, u.Image FromImage, u.NickName FromNickName, u.Permalink FromPermalink,
        //m.Title, s.Name StatusName,m.MessageType, m.CreationTime, m.Attachments
    }
}