﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Messages.Dto;

namespace IContestPro.Messages
{
    public interface IMessagesAppService : IBaseAppService<MessageDto, int, PagedResultRequestDto, CreateMessageDto, MessageDto>
    {
        Task<PagedResultDto<MessageDto>> GetAllByStatus(string statusName, PagedResultRequestDto input);
        Task<MessageOutputDto> CreateMessage(CreateMessageInputDto input);
        Task<PagedResultDto<MessageDto>> GetMessageResponses(int messageId, PagedResultRequestDto input);

        Task<IReadOnlyList<UserMessageDto>> GetAllByByUser(long userId,
            PagedResultRequestDto input,
            int type = MessageType.All,
            string statusName = "",
            string fetchFrom = MessageFetchType.Inbox
            );

        Task<MessageResponseDto>  CreateMessageResponse(CreateMessageResponseDto model);
        Task<UserMessageDetailsDto> GetMessageDetails(int id, int type);
        Task<UserMessageDetailsDto> GetMessageDetailsWithResponses(int id, int type, int responseSkipCount = 0, int responsePageSize = 100);
        Task<MessageDto> CloseSupportTicket(int messageId);
    }
}