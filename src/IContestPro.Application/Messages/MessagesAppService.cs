﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using IContestPro.AssetUpload;
using IContestPro.AssetUpload.Dto;
using IContestPro.Authorization;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Base;
using IContestPro.Contest;
using IContestPro.EntityFrameworkCore;
using IContestPro.EntityFrameworkCore.Seed;
using IContestPro.EntityFrameworkCore.StoreProcedureManager;
using IContestPro.IContestProEntities;
using IContestPro.Messages.Dto;
using IContestPro.Users;
using IContestPro.Users.Dto;

namespace IContestPro.Messages
{
    [AbpAuthorize]
     public class MessagesAppService : BaseAppService<IContestProEntities.Message, MessageDto, int,
                                PagedResultRequestDto, CreateMessageDto, MessageDto>, IMessagesAppService
    {
        private readonly IRepository<IContestProEntities.Message> _repository;
        private readonly IRepository<IContestProEntities.MessageResponse> _messageResponseRepository;
        private readonly IAssetUploadAppService _assetUploadAppService;
        private readonly IUserAppService _userAppService;
        private readonly IContestAppService _contestAppService;
        private readonly IContestProDbContextFactory _contestProDbContextFactory;

        public MessagesAppService(IRepository<IContestProEntities.Message> repository, IIocResolver  iocResolver, IAssetUploadAppService assetUploadAppService, IRepository<MessageResponse> messageResponseRepository, IUserAppService userAppService, IContestAppService contestAppService, IContestProDbContextFactory contestProDbContextFactory) : base(repository, iocResolver)
        {
            _repository = repository;
            _assetUploadAppService = assetUploadAppService;
            _messageResponseRepository = messageResponseRepository;
            _userAppService = userAppService;
            _contestAppService = contestAppService;
            _contestProDbContextFactory = contestProDbContextFactory;
        }
        public async Task<MessageOutputDto> CreateMessage(CreateMessageInputDto input)
        {
            UserDto adminUser = null;
            if (string.IsNullOrEmpty(input.Title))
            {
                throw new UserFriendlyException("Please enter your message title");
            }
            if (string.IsNullOrEmpty(input.MessageText))
            {
                throw new UserFriendlyException("Please enter your message");
            }
            if (input.MessageType == MessageType.Message)
            {
                if (input.MessageTo == MessageTo.Follower)
                {
                    if (string.IsNullOrEmpty(input.Recipients))
                    {
                        throw new UserFriendlyException("Please select at least one recipient");
                    }  
                }
                else if (input.MessageTo == MessageTo.ContestParticipants)
                    {
                        if (input.ContestId == null)
                        {
                            throw new UserFriendlyException("Please select a contest to message participants.");
                        }
                    }
            }
            if (input.MessageType == MessageType.SupportTicket || input.MessageType == MessageType.Dispute )
            {
                adminUser = await _userAppService.GetTenantAdmin();
            }
            var attachments = "";
            if (input.Attachments != null)
            {
                if (input.Attachments.Any(file => file.ContentType != "image/jpeg" && file.ContentType != "image/gif" && file.ContentType != "image/png"))
                {
                    throw new UserFriendlyException("Only jgp, gif and png files are allowed.");
                }
                
            }
            var messages = new List<MessageDto>();
            var status = StatusSeed.GetItems().First(item => item.Value == AppConsts.StatusNew);
            if (adminUser != null)
            {
                if (input.Attachments != null)
                {
                    foreach (var file in input.Attachments) {
                        var image = await _assetUploadAppService.UploadImage(new UploadDto
                        {
                            File = file,
                            Path = AppConsts.ImageUploadFolder
                        });
                        attachments += image.FileName + ",";
                    }
                    attachments = attachments.TrimEnd(',');
                }
                //Todo: Send support ticket to admin
                 status = StatusSeed.GetItems().First(item => item.Value == AppConsts.StatusUnProcessed);
                  var model = new CreateMessageDto
                    {
                        UserId = AbpSession.GetUserId(),
                        ToUserId = adminUser.Id,
                        Title = input.Title,
                        MessageText = input.MessageText,
                        MessageType = input.MessageType,
                        Attachments = attachments,
                        StatusId = status.Key
                        
                    }; 
                    messages.Add(await Create(model));
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    Message = $"{CurrentUser.NickName} sent you a message. Please visit the message page to find details.",
                    UserId = adminUser.Id
                });
            }
            else if(input.ContestId.HasValue)
            {
                var contestParticipants = await _contestAppService.GetUsersInContest(input.ContestId.Value);
                if (contestParticipants == null || !contestParticipants.Any())
                {
                    throw new UserFriendlyException("The contest you selected does not have any participant.");
                }
                foreach (var recipient in contestParticipants)
                {
                    if (input.Attachments != null)
                    {
                        foreach (var file in input.Attachments) {
                            var image = await _assetUploadAppService.UploadImage(new UploadDto
                            {
                                File = file,
                                Path = AppConsts.ImageUploadFolder
                            });
                            attachments += image.FileName + ",";
                        }
                        attachments = attachments.TrimEnd(',');
                    }
                    var model = new CreateMessageDto
                    {
                        UserId = AbpSession.GetUserId(),
                        ToUserId = recipient.Id,
                        Title = input.Title,
                        MessageText = input.MessageText,
                        MessageType = input.MessageType,
                        Attachments = attachments,
                        StatusId = status.Key
                    }; 
                    messages.Add(await Create(model));
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        Message = $"{CurrentUser.NickName} sent you a message. Please visit the message page to find details.",
                        UserId = recipient.Id
                    });
                } 
            }
            else
            {
                foreach (var recipient in input.Recipients.Split(","))
                {
                    if (input.Attachments != null)
                    {
                        foreach (var file in input.Attachments) {
                            var image = await _assetUploadAppService.UploadImage(new UploadDto
                            {
                                File = file,
                                Path = AppConsts.ImageUploadFolder
                            });
                            attachments += image.FileName + ",";
                        }
                        attachments = attachments.TrimEnd(',');
                    }
                    var userId = long.Parse(recipient);
                    var model = new CreateMessageDto
                    {
                        UserId = AbpSession.GetUserId(),
                        ToUserId = userId,
                        Title = input.Title,
                        MessageText = input.MessageText,
                        MessageType = input.MessageType,
                        Attachments = attachments,
                        StatusId = status.Key
                    }; 
                    messages.Add(await Create(model));
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        Message = $"{CurrentUser.NickName} sent you a message. Please visit the message page to find details.",
                        UserId = userId
                    });
                }
            }
            return new MessageOutputDto{Results = messages};
        }
        public async Task<MessageResponseDto> CreateMessageResponse(CreateMessageResponseDto input)
        {
            if (string.IsNullOrEmpty(input.Message))
            {
                throw new UserFriendlyException("Please enter your message");
            }
            var attachments = "";
            if (input.Attachments != null)
            {
                if (input.Attachments.Any(file => file.ContentType != "image/jpeg" && file.ContentType != "image/gif" && file.ContentType != "image/png"))
                {
                    throw new UserFriendlyException("Only jgp, gif and png files are allowed.");
                }
                
                foreach (var file in input.Attachments) {
                    var image = await _assetUploadAppService.UploadImage(new UploadDto
                    {
                        File = file,
                        Path = AppConsts.ImageUploadFolder
                    });
                    attachments += image.FileName + ",";
                }
            }
            attachments = attachments.TrimEnd(',');
            var status = StatusSeed.GetItems().First(item => item.Value == AppConsts.StatusNew);
            var result = await _messageResponseRepository.InsertAsync(new MessageResponse
            {
                UserId = AbpSession.GetUserId(),
                MessageId = input.MessageId,
                Message = input.Message,
                Attachments = attachments,
                StatusId = status.Key
            });
            if (AbpSession.GetUserId() == input.MessageSenderUserId)
            {
                var msgDetails = await GetMessageDetails(input.MessageId, input.MessageType);
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    Message = $"{CurrentUser.NickName} replied the message he/she sent you. Please visit the message page to find details.",
                    UserId = msgDetails.ToUserId
                });  
            }
            else
            {
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    Message = $"{CurrentUser.NickName} replied your message. Please visit the message page to find details.",
                    UserId = input.MessageSenderUserId
                });  
            }

            if (input.CloseTicket)
            {
                await CloseSupportTicket(input.MessageId);
            }
            return result.MapTo<MessageResponseDto>();
        }

            public override async Task Delete(EntityDto<int> input)
            {
                var message = await Get(input);
                await base.Delete(input);
                if (!string.IsNullOrEmpty(message.Attachments))
                {
                    foreach (var file in message.Attachments.Split(',')) {
                     _assetUploadAppService.Delete(new DeleteAssetDto
                        {
                            Path = AppConsts.ImageUploadFolder + "/" + file
                        });
                    }
                }
            }

        public override async Task<PagedResultDto<MessageDto>> GetAll(PagedResultRequestDto input)
        {
            var query = CreateFilteredQuery(input);
          
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<MessageDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        public async Task<PagedResultDto<MessageDto>> GetAllByStatus(string statusName, PagedResultRequestDto input)
        {
            var query = CreateFilteredQuery(input);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<MessageDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        public async Task<PagedResultDto<MessageDto>> GetMessageResponses(int messageId, PagedResultRequestDto input)
        {
            var query = CreateFilteredQuery(input);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<MessageDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }

        public async Task<IReadOnlyList<UserMessageDto>> GetAllByByUser(long userId,
            PagedResultRequestDto input, int type = MessageType.All,
            string statusName = "",
            string fetchFrom = MessageFetchType.Inbox)
        {
                switch (fetchFrom)
                {
                    case MessageFetchType.Inbox:
                        using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
                        {
                            var isAdmin = await IsGrantedAsync(PermissionNames.Pages_Admin);
                            var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetMessagesForUserInbox)
                                .WithSqlParam("@UserId", AbpSession.GetUserId())
                                .WithSqlParam("@SkipCount", input.SkipCount)
                                .WithSqlParam("@PageSize", input.MaxResultCount)
                                .WithSqlParam("@Type", type)
                                .WithSqlParam("@Status", statusName)
                                .WithSqlParam("@IsAdmin", isAdmin);
                            var items = await cmd.ExecuteStoredProcedureListAsync<UserMessageDto>();
                            return items;
                        }
                    case MessageFetchType.Sent:
                        using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
                        {
                            var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetMessagesForUserSent)
                                .WithSqlParam("@UserId", AbpSession.GetUserId())
                                .WithSqlParam("@SkipCount", input.SkipCount)
                                .WithSqlParam("@PageSize", input.MaxResultCount);
                            var items = await cmd.ExecuteStoredProcedureListAsync<UserMessageDto>();
                            return items;
                        }
                    default:
                        return null;
                } 
            
            
        }
         public async Task<UserMessageDetailsDto> GetMessageDetails(int id, int type)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetMessageDetails)
                    .WithSqlParam("@MessageId", id)
                    .WithSqlParam("@CurrentUserId", AbpSession.GetUserId())
                    .WithSqlParam("@Type", type);
                var item = await cmd.ExecuteStoredProcedureFirstOrDefaultAsync<UserMessageDetailsDto>();
                return item;
            }  
        }
        public async Task<UserMessageDetailsDto> GetMessageDetailsWithResponses(int id, int type, int responseSkipCount = 0, int responsePageSize = 100)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetMessageDetails)
                    .WithSqlParam("@MessageId", id)
                    .WithSqlParam("@CurrentUserId", AbpSession.GetUserId())
                    .WithSqlParam("@Type",type);
                var item = await cmd.ExecuteStoredProcedureFirstOrDefaultAsync<UserMessageDetailsDto>();
                var cmdResponse = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetMessageResonses)
                    .WithSqlParam("@MessageId", id)
                    .WithSqlParam("@SkipCount", responseSkipCount)
                    .WithSqlParam("@PageSize", responsePageSize);
                var items = await cmdResponse.ExecuteStoredProcedureListAsync<UserMessageResponseDto>();
                item.Responses = items;
                return item;
            }  
        }

        public async Task<MessageDto> CloseSupportTicket(int messageId)
        {
            var item = await _repository.GetAsync(messageId);
            var status = StatusSeed.GetItems().First(itm => itm.Value == AppConsts.StatusClosed);
            item.StatusId = status.Key;
            item = await _repository.UpdateAsync(item);
            return MapToEntityDto(item);
        }

        
    }
}