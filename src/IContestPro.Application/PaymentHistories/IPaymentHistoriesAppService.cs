﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.PaymentHistories.Dto;

namespace IContestPro.PaymentHistories
{
    public interface IPaymentHistoriesAppService : IBaseAppService<PaymentHistoryDto, int, PagedResultRequestDto, CreatePaymentHistoryDto, PaymentHistoryDto>
    {
       
        Task<PagedResultDto<PaymentHistoryDto>> GetAllByUser(long userId, PagedResultRequestDto input);
        Task<PagedResultDto<PaymentHistoryDto>> GetAllBetweenDates(PagedResultRequestDto input, DateTime startDate, DateTime endDate);

        Task<PagedResultDto<PaymentHistoryDto>> GetAllByUserBetweenDates(long userId, PagedResultRequestDto input,
            DateTime startDate, DateTime endDate);
        Task<PaymentHistoryDto> GetByReference(string reference);
    }   
}