﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Authorization;
using IContestPro.Authorization.Users;
using IContestPro.Base;
using IContestPro.IContestProEntities;
using IContestPro.PaymentHistories.Dto;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.PaymentHistories
{
    public class PaymentHistoriesAppService : BaseAppService<PaymentHistory, PaymentHistoryDto, int,
                                PagedResultRequestDto, CreatePaymentHistoryDto, PaymentHistoryDto>, IPaymentHistoriesAppService
    {
        private readonly IRepository<PaymentHistory> _repository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<IContestProEntities.Status> _statusRepository;
        public PaymentHistoriesAppService(
            IRepository<PaymentHistory> repository, IRepository<User, long> userRepository, IRepository<IContestProEntities.Status> statusRepository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
            _userRepository = userRepository;
            _statusRepository = statusRepository;
        }
       
        public override async Task<PaymentHistoryDto> Get(EntityDto<int> input)
        {
            var query = await _repository.GetAllIncluding(item => item.Status)
                .FirstOrDefaultAsync(item => item.Id == input.Id);
            return query != null ? query.MapTo<PaymentHistoryDto>() : null;
        } 
        
        [AbpAuthorize(new []{PermissionNames.Pages_Admin})]
         public override async Task<PagedResultDto<PaymentHistoryDto>> GetAll(PagedResultRequestDto input)
        {
            //This must be added to all 
            var query = _repository.GetAllIncluding(item => item.Status);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);

            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<PaymentHistoryDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        } 
        
        [AbpAuthorize(permissions: new []{PermissionNames.Pages_Admin})]
        public async Task<PagedResultDto<PaymentHistoryDto>> GetAllBetweenDates(PagedResultRequestDto input, DateTime startDate, DateTime endDate)
        { 
            //This must be added to all 
            var query = _repository.GetAllIncluding(item => item.Status);
            query = query.Where(itm => itm.CreationTime >= startDate && itm.CreationTime <= endDate);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);

            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<PaymentHistoryDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        [AbpAuthorize]
        public async Task<PagedResultDto<PaymentHistoryDto>> GetAllByUser(long userId, PagedResultRequestDto input)
        {
            var query = _repository.GetAllIncluding(itm => itm.Status)
                            .Where(item => item.UserId == userId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderByDescending(vote => vote.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<PaymentHistoryDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        [AbpAuthorize]
        public async Task<PagedResultDto<PaymentHistoryDto>> GetAllByUserBetweenDates(long userId, PagedResultRequestDto input, DateTime startDate, DateTime endDate)
        { 
            var query = _repository.GetAllIncluding(itm => itm.Status)
                .Where(item => item.UserId == userId);
            query = query.Where(itm => itm.CreationTime >= startDate && itm.CreationTime <= endDate);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderByDescending(vote => vote.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<PaymentHistoryDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        

        public async Task<PaymentHistoryDto> GetByReference(string reference)
        {
            var result = await _repository.GetAllIncluding(itm => itm.Status).FirstOrDefaultAsync(item => item.Reference == reference);
            return result != null ? result.MapTo<PaymentHistoryDto>() : null;
        }
    }
}