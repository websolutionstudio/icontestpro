﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.PaymentHistories.Dto
{
    [AutoMapTo(typeof(IContestProEntities.PaymentHistory))]
    public class CreatePaymentHistoryDto
    {
        [Required]
        [StringLength(500)]
        public string Title { get; set; }
        [StringLength(2000)]
        [Required]
        public string Description { get; set; }
        public string Reference { get; set; }
        public string Response { get; set; }
        public int? TenantId { get; set; }
        [Required]
        public long UserId { get; set; }
        //[Required]
        //public int PaymentTypeId { get; set; }
        [Required]
        public decimal? Amount { get; set; }
        [Required]
        public WalletOperation WalletOperation { get; set; }
        [Required]
        public int StatusId { get; set; }
    }
}