﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.PaymentType.Dto;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.PaymentHistories.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.PaymentHistory))]
    public class PaymentHistoryDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }
        public string Response { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }
        public int PaymentTypeId { get; set; }
        public decimal? Amount { get; set; }
        public WalletOperation WalletOperation { get; set; }
        public int StatusId { get; set; }

        public UserDto User { get; set; }
        public PaymentTypeDto PaymentType { get; set; }
       
        public StatusDto Status { get; set; }
    }
}