﻿namespace IContestPro.PaymentHistories.Dto
{
    public class TransactionResponseDto
    {
       public bool Status { get; set; }
       public string Message { get; set; }
       public ResponseData Data { get; set; }
    }

}