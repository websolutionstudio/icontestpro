﻿using System;

namespace IContestPro.PaymentHistories.Dto
{
    public class ResponseData
    {
      public decimal Amount { get; set; }
      public string Cuurency { get; set; }
      public DateTime Transaction_date { get; set; }
      public string  Status { get; set; }
      public string  Reference { get; set; }
      public string  Domain { get; set; }
      public string  Gateway_response { get; set; }
      public string  Channel { get; set; }
      public string  Ip_address { get; set; }
    
   }
}