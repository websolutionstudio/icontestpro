﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Abp.Application.Services;
using Abp.IdentityFramework;
using Abp.Runtime.Security;
using Abp.Runtime.Session;
using IContestPro.Authorization.Users;
using IContestPro.MultiTenancy;
using Newtonsoft.Json;

namespace IContestPro
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class IContestProAppServiceBase : ApplicationService
    {
        public TenantManager TenantManager { get; set; }

        public UserManager UserManager { get; set; }

        protected IContestProAppServiceBase()
        {
            LocalizationSourceName = IContestProConsts.LocalizationSourceName;
        }

        protected virtual Task<User> GetCurrentUserAsync()
        {
            var user = UserManager.FindByIdAsync(AbpSession.GetUserId().ToString());
            if (user == null)
            {
                throw new Exception("There is no current user!");
            }

            return user;
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
        }
        public string GetCurrentUserAppIdentity()
        {
            return AbpSession.UserId == null ? null : AbpSession.GetUserId().ToString().PadLeft(AppConsts.UserIdentityLength, '0');
        } 
        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
        public string SerializeObject<T>(T value) where T : class
        {
            return JsonConvert.SerializeObject(value);
        }

        public T DeserializeObject<T>(string value) where T : class
        {
            return value == null ? null : JsonConvert.DeserializeObject<T>(value);
        }
        public string Encrypt(string item)
        {
            return new SimpleStringCipher().Encrypt(item);
        }
        public string Decrypt(string item)
        {
            return new SimpleStringCipher().Decrypt(item);
        }
    }
}
