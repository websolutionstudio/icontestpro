﻿using System;
using Abp.AutoMapper;

namespace IContestPro.FeaturedContest.Dto
{
    [AutoMapTo(typeof(CreateFeaturedContestDto))]
    [AutoMapFrom(typeof(FeaturedContestDto))]
    public class CreateEditFeaturedContestDto
    {
        
        public int? Id { get; set; }
        public int? TenantId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? StatusId { get; set; }
        public long? UserId { get; set; }
        public int? AdvertPriceId { get; set; }
        public int? ContestId { get; set; }
       
    }
}