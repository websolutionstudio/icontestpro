﻿using System;
using Abp.AutoMapper;

namespace IContestPro.FeaturedContest.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.FeaturedContest))]
    public class RandomFeaturedContestDto
    {
        public int Id { get; set; }
        public int ContestId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Title { get; set; }
        public bool IsFeatured { get; set; }
        public string Permalink { get; set; }
        public string Picture { get; set; }

        
    }
}