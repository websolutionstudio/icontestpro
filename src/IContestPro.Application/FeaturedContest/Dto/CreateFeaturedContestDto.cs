﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.FeaturedContest.Dto
{
    [AutoMapTo(typeof(IContestProEntities.FeaturedContest))]
    public class CreateFeaturedContestDto
    {
        [Required]
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
         public int StatusId { get; set; }
        [Required]
        public int AdvertPriceId { get; set; }
        [Required]
        public decimal AmountPaid { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int ContestId { get; set; }
    }
}