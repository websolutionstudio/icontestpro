﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.AdvertPrice.Dto;
using IContestPro.Contest.Dto;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.FeaturedContest.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.FeaturedContest))]
    public class FeaturedContestDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StatusId { get; set; }
        public long UserId { get; set; }
        public decimal AmountPaid { get; set; }
        public int AdvertPriceId { get; set; }
        public int? TenantId { get; set; }
        public int ContestId { get; set; }

        public StatusDto Status { get; set; }
        public UserDto User { get; set; }
        public AdvertPriceDto AdvertPrice { get; set; }
        public ContestDto Contest { get; set; }

    }
}