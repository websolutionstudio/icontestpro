﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.FeaturedContest.Dto;
using IContestPro.Contest.Dto;

namespace IContestPro.FeaturedContest
{
    public interface IFeaturedContestAppService : IBaseAppService<FeaturedContestDto, int, PagedResultRequestDto, CreateFeaturedContestDto, FeaturedContestDto>
    {
        Task<IEnumerable<RandomFeaturedContestDto>> GetRandomLive(int pageSize = 5);
        Task<PagedResultDto<FeaturedContestDto>> GetAllByUser(long userId, PagedResultRequestDto input);
        Task<PagedResultDto<FeaturedContestDto>> GetAllByStatus(string statusName, PagedResultRequestDto input);
        Task<FeaturedContestDto> UpdateStatusAsync(UpdateStatusDto model);

        Task NotifyExpiring();
        Task DisableExpired();
    }
}