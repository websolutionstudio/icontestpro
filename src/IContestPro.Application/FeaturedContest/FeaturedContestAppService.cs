﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Notifications;
using Abp.UI;
using Hangfire;
using IContestPro.Base;
using IContestPro.FeaturedContest.Dto;
using IContestPro.AdvertPrice;
using IContestPro.BackgroundJobs;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Contest.Dto;
using IContestPro.EntityFrameworkCore;
using IContestPro.EntityFrameworkCore.StoreProcedureManager;
using IContestPro.Helpers;
using IContestPro.Status;
using IContestPro.Users;
using IContestPro.Wallet;
using IContestPro.Wallet.Dto;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.FeaturedContest
{
     public class FeaturedContestAppService : BaseAppService<IContestProEntities.FeaturedContest, FeaturedContestDto, int,
                                PagedResultRequestDto, CreateFeaturedContestDto, FeaturedContestDto>, IFeaturedContestAppService
    {
        private readonly IRepository<IContestProEntities.FeaturedContest> _repository;
        private readonly IRepository<IContestProEntities.Contest> _contestRepository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAdvertPriceAppService _advertPriceAppService;
        private readonly IWalletAppService _walletAppService;
        private readonly IUserAppService _userAppService;
        private readonly IContestProDbContextFactory _contestProDbContextFactory;
        public FeaturedContestAppService(IRepository<IContestProEntities.FeaturedContest> repository, IStatusAppService statusAppService, IAdvertPriceAppService advertPriceAppService, 
            IWalletAppService walletAppService, IUserAppService userAppService, IIocResolver  iocResolver, IRepository<IContestProEntities.Contest> contestRepository, IContestProDbContextFactory contestProDbContextFactory) : base(repository, iocResolver)
        {
            _repository = repository;
            _statusAppService = statusAppService;
            _advertPriceAppService = advertPriceAppService;
            _walletAppService = walletAppService;
            _userAppService = userAppService;
            _contestRepository = contestRepository;
            _contestProDbContextFactory = contestProDbContextFactory;
        }
        public async Task<IEnumerable<RandomFeaturedContestDto>> GetRandomLive(int pageSize = 5)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetRandomFeaturedContests)
                    .WithSqlParam("PageSize", pageSize);
                var items = await cmd.ExecuteStoredProcedureListAsync<RandomFeaturedContestDto>();
                return items;
            }
          
        } 
        [AbpAuthorize]
        public override async Task<PagedResultDto<FeaturedContestDto>> GetAll(PagedResultRequestDto input)
        {
            
            var query =  _repository.GetAllIncluding(item => item.Status, item => item.Contest);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            
            return new PagedResultDto<FeaturedContestDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        [AbpAuthorize]
        public async Task<PagedResultDto<FeaturedContestDto>> GetAllByStatus(string statusName, PagedResultRequestDto input)
        {
            var status = await _statusAppService.GetByNameAsync(statusName);
            if (status == null)
            {
                return null;
            }
            var query =  _repository.GetAllIncluding(item => item.Status, item => item.Contest).Where(item => item.StatusId == status.Id);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            
            return new PagedResultDto<FeaturedContestDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        [AbpAuthorize]
        public async Task<PagedResultDto<FeaturedContestDto>> GetAllByUser(long userId, PagedResultRequestDto input)
        {
            var query =  _repository.GetAllIncluding(item => item.Status, item => item.Contest).Where(item => item.UserId == userId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<FeaturedContestDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        
       
        [AbpAuthorize]
         public override async Task<FeaturedContestDto> Create(CreateFeaturedContestDto input)
        {
            if (input.StartDate == null)
            {
                throw new UserFriendlyException("StartDate date must be at least one day ahead.");
            }
            
            var wallet = await _walletAppService.GetByUserId(input.UserId);
            if (wallet == null)
            {
                throw new UserFriendlyException("User wallet information not found.");
            }
            var advertPrice = await _advertPriceAppService.Get(new EntityDto{Id = input.AdvertPriceId});
            if (wallet.Balance < advertPrice.Amount)
            {
                throw new UserFriendlyException("Insufficieent balance. Please add fund to your wallet to complete your request.");
            }
            var contest = await _contestRepository.FirstOrDefaultAsync(item => item.Id == input.ContestId);
            if (contest == null)
            {
                throw new UserFriendlyException("Contest not found.");
            }
            if (contest.IsFeatured)
            {
                throw new UserFriendlyException("Contest is already featured. Please select a different contest.");
            }
            var balanceBefore = await _walletAppService.GetBalance(input.UserId);
            var walletAdmin = await _walletAppService.GetAdminWallet();
            var user = await _userAppService.Get(new EntityDto<long> {Id = input.UserId});
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusPending);

                    try
                    {
                        input.StatusId = status.Id;
                        var dbModel = input.MapTo<CreateFeaturedContestDto>();
                        dbModel.AmountPaid = advertPrice.Amount;
                        await _walletAppService.Debit(input.UserId, dbModel.AmountPaid, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Reference = Guid.NewGuid().ToString(),
                            Amount = dbModel.AmountPaid,
                            Title = "Featured contest creation debit occurred",
                            Description = "A debit occurred in your wallet to complete your featured contest creation.",
                            WalletInfoResult = WalletInfoResult.Success,
                            WalletOperation = WalletOperation.Debit,
                            UserId = input.UserId
                        });

                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            Message =
                                $"A debit of {dbModel.AmountPaid.ToNairaString()} occurred in your wallet to complete your featured contest creation.",
                            Severity = NotificationSeverity.Success,
                            UserId = input.UserId
                        });

                        
                        await _walletAppService.Credit(walletAdmin.UserId, dbModel.AmountPaid, false);
                       

                        dbModel.StatusId = status.Id;
                        dbModel.EndDate = dbModel.StartDate.AddDays(advertPrice.Days);

                        var result = await base.Create(dbModel);
                        contest.IsFeatured = true;
                        
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Reference = Guid.NewGuid().ToString(),
                            Amount = dbModel.AmountPaid,
                            Title = "Featured contest creation credit occurred",
                            Description =
                                $"A credit occurred in your wallet from '{user.NickName}' to complete the creation of featured contest.",
                            WalletInfoResult = WalletInfoResult.Success,
                            WalletOperation = WalletOperation.Credit,
                            UserId = walletAdmin.UserId
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            Message =
                                $"A credit of {dbModel.AmountPaid.ToNairaString()} occurred in your wallet from '{user.NickName}' to complete the creation of featured contest.",
                            Severity = NotificationSeverity.Success,
                            UserId = walletAdmin.UserId
                        });//return res
                        return result;
                    }
                    catch (Exception e)
                    {
                        contest.IsFeatured = false;
                        _repository.Delete(itm => itm.ContestId == contest.Id && itm.StatusId == status.Id);
                         var balanceNow = await _walletAppService.GetBalance(input.UserId);
                        if (balanceBefore != balanceNow)
                        {
                        await _walletAppService.Credit(input.UserId, advertPrice.Amount, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Reference = Guid.NewGuid().ToString(),
                            Amount = advertPrice.Amount,
                            Title = "Featured contest creation reversal credit occurred",
                            Description = "A reversal credit occurred in your wallet for the featured contest creation you initiated which was not successful.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Credit,
                            UserId = input.UserId
                        });

                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            Message = $"A reversal credit of NGN{advertPrice.Amount.ToString("N")} occurred in your wallet for the featured contest creation you initiated which was not successful.",
                            Severity = NotificationSeverity.Error,
                            UserId = input.UserId
                        });

                        await _walletAppService.Debit(walletAdmin.UserId, advertPrice.Amount, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Reference = Guid.NewGuid().ToString(),
                            Amount = advertPrice.Amount,
                            Title = "Featured contest creation reversal debit occurred",
                            Description =
                                $"A reversal debit of NGN{advertPrice.Amount.ToString("N")} occurred in your wallet for the featured contest creation which '{user.NickName}' initiated which was not successful.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Debit,
                            UserId = walletAdmin.UserId
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            Message =
                                $"A reversal debit of NGN{advertPrice.Amount.ToString("N")} occurred in your wallet for the featured contest creation which '{user.NickName}' initiated which was not successful.",
                            Severity = NotificationSeverity.Error,
                            UserId = walletAdmin.UserId
                        });
                           await UnitOfWorkManager.Current.SaveChangesAsync();
                        }
                        LoggingAppService.LogException(e);
                        throw new UserFriendlyException(e.Message);
                    }
         
           
            
        }
        [AbpAuthorize]
        public override async Task<FeaturedContestDto> Update(FeaturedContestDto input)
        {
            if (input.StartDate == null)
            {
                throw new UserFriendlyException("StartDate date must be at least one day ahead.");
            }
            

            var itemToUpdate = await _repository.GetAsync(input.Id);
            if (itemToUpdate == null)
            {
                throw new UserFriendlyException("Item not found.");
            }
            var wallet = await _walletAppService.GetByUserId(input.UserId);
            if (wallet == null)
            {
                throw new UserFriendlyException("User wallet information not found.");
            }
            var contest = await _contestRepository.FirstOrDefaultAsync(item => item.Id == input.ContestId);
            if (contest == null)
            {
                throw new UserFriendlyException("Contest not found.");
            }

            if (contest.Id != itemToUpdate.ContestId)
            {
                if (contest.IsFeatured)
                {
                    throw new UserFriendlyException("The contest you selected is already featured. Please select a different contest.");
                }
            }
            
            var advertPrice = await _advertPriceAppService.Get(new EntityDto{Id = input.AdvertPriceId});
           
                    try
                    {
            if (itemToUpdate.AdvertPriceId != advertPrice.Id)
            {
                var advertPriceOld = await _advertPriceAppService.Get(new EntityDto{Id = itemToUpdate.AdvertPriceId});
                var priceDifference = advertPrice.Amount - itemToUpdate.AmountPaid;
                
                //Todo: Credit the admin if he advert owner upgraded the advert
                if (priceDifference > 0)
              {
                  if (wallet.Balance < priceDifference)
                  {
                      throw new UserFriendlyException("Insufficieent balance. Please add fund to your wallet to complete your request.");
                  }
                await _walletAppService.Debit(input.UserId, priceDifference, false);
                await _walletAppService.LogWalletOperation(new WalletInfoDto
            {
                Reference = Guid.NewGuid().ToString(),
                Amount = priceDifference, 
                Title = "Featured contest edit debit occurred",
                Description =  $"A featured contest edit debit occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Debit,
                UserId = input.UserId
            });
            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                Message = $"A featured contest edit debit of {priceDifference.ToNairaString()} occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                Severity = NotificationSeverity.Success,
                UserId = input.UserId
            });
            
            var walletAdmin = await _walletAppService.GetAdminWallet();
            await _walletAppService.Credit(walletAdmin.UserId, priceDifference, false);
            var user = await _userAppService.Get(new EntityDto<long>{Id = input.UserId});
            await _walletAppService.LogWalletOperation(new WalletInfoDto
            {
                Reference = Guid.NewGuid().ToString(),
                Amount = priceDifference, 
                Title = "Featured contest edit credit occurred",
                Description =  $"A featured contest edit credit occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from '{user.NickName}' to complete the amount difference for the edit he initiated.",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Credit,
                UserId = walletAdmin.UserId
            });
            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                Message = $"A featured contest edit credit of {priceDifference.ToNairaString()} occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from '{user.NickName}' to complete the amount difference for the edit he initiated.",
                Severity = NotificationSeverity.Success,
                UserId = walletAdmin.UserId
            });
                }
                else
                {
                    priceDifference = Math.Abs(priceDifference);
                    await _walletAppService.Credit(input.UserId, priceDifference, false);
                    await _walletAppService.LogWalletOperation(new WalletInfoDto
                    {
                        Reference = Guid.NewGuid().ToString(),
                        Amount = priceDifference, 
                        Title = "Featured contest edit credit occurred",
                        Description = $"A featured contest edit credit occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                        WalletInfoResult = WalletInfoResult.Success,
                        WalletOperation = WalletOperation.Credit,
                        UserId = input.UserId
                    });
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        Message = $"A featured contest edit credit of {priceDifference.ToNairaString()} occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                        Severity = NotificationSeverity.Success,
                        UserId = input.UserId
                    });
           
            
            var walletAdmin = await _walletAppService.GetAdminWallet();
            await _walletAppService.Debit(walletAdmin.UserId, priceDifference, false);
            var user = await _userAppService.Get(new EntityDto<long>{Id = input.UserId});
           
                    await _walletAppService.LogWalletOperation(new WalletInfoDto
                    {
                        Reference = Guid.NewGuid().ToString(),
                        Amount = priceDifference, 
                        Title = "Featured contest edit debit occurred",
                        Description =  $"A top edit debit occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from {user.NickName} to complete amount difference for the edit he initiated.",
                        WalletInfoResult = WalletInfoResult.Success,
                        WalletOperation = WalletOperation.Debit,
                        UserId = walletAdmin.UserId
                    });
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        Message = $"A featured contest edit debit occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from {user.NickName} to complete amount difference for the edit he initiated.",
                        Severity = NotificationSeverity.Success,
                        UserId = walletAdmin.UserId
                    });
                    
                }
            }
            
            itemToUpdate.StartDate = input.StartDate;
            itemToUpdate.AdvertPriceId = input.AdvertPriceId;
            itemToUpdate.EndDate =  itemToUpdate.StartDate.AddDays(advertPrice.Days);
            itemToUpdate.AmountPaid =  advertPrice.Amount;
            await CurrentUnitOfWork.SaveChangesAsync();
                        return MapToEntityDto(itemToUpdate);
                    }
                    catch (Exception e)
                    {
                        LoggingAppService.LogException(e);
                        throw new UserFriendlyException(e.Message);
                    }
           
           
        }

        [AbpAuthorize]
        public async Task<FeaturedContestDto> UpdateStatusAsync(UpdateStatusDto model)
        {
            var entry = await _repository.FirstOrDefaultAsync(itm => itm.Id == model.EntityId);
            var status =  await _statusAppService.GetByNameAsync(model.StatusName);
            var pendingStatus =  await _statusAppService.GetByNameAsync(AppConsts.StatusPending);
            if (entry == null || status == null)
            {
                return null;
            }
          
                    try
                    {
            if (status.Name.Equals(AppConsts.StatusLive) && pendingStatus.Id == entry.StatusId)
            {
                entry.StartDate = DateTime.Now;
            } 
            if (status.Name.Equals(AppConsts.StatusCancelled))
            {
                var daysAdvertRan = (DateTime.Now - entry.StartDate).Days;
                var totaldaysForAdvert = await _advertPriceAppService.Get(new EntityDto {Id = entry.AdvertPriceId});
                var amountUsed = entry.AmountPaid - (((decimal)daysAdvertRan * entry.AmountPaid) / (decimal)totaldaysForAdvert.Days);
                var adminWallet = await _walletAppService.GetAdminWallet();
                var currentUser = await _userAppService.GetCurrentUser();
                if (amountUsed > 0)
                {
                        await _walletAppService.Credit(entry.UserId, amountUsed, false);
                        await _walletAppService.LogWalletOperation(
                            new WalletInfoDto
                        {
                            Amount = amountUsed, 
                            Title = "Featured contest cancellation credit",
                            Description =  $"Your featured contest cancellation reversal credit occurred in your account. {amountUsed.ToNairaString()} was credited from {entry.AmountPaid.ToNairaString()} you paid because the advert has ran for {daysAdvertRan} days.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Credit,
                            UserId = entry.UserId,
                        }); 
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = entry.UserId,
                            TenantId = AbpSession.TenantId,
                            Message =  $"Your featured contest cancellation reversal credit occurred in your account. {amountUsed.ToNairaString()} was credited from {entry.AmountPaid.ToNairaString()} you paid because the advert has ran for {daysAdvertRan} days.",
                        });
                        await _walletAppService.Debit(adminWallet.UserId, amountUsed, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Amount = amountUsed, 
                            Title = "Featured contest cancellation debit",
                            Description =  $"Your featured contest cancellation debit occurred in your account from '{currentUser.NickName}'. {amountUsed.ToNairaString()} was debited from {entry.AmountPaid.ToNairaString()} paid for the banner because the advert has ran for {daysAdvertRan} days.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Debit,
                            UserId = adminWallet.UserId,
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = adminWallet.UserId,
                            TenantId = AbpSession.TenantId,
                            Message = $"Your featured contest cancellation debit occurred in your account from '{currentUser.NickName}'. {amountUsed.ToNairaString()} was debited from {entry.AmountPaid.ToNairaString()} paid for the banner because the advert has ran for {daysAdvertRan} days.",
                        });  
                    entry.StatusId = status.Id;
                    UnitOfWorkManager.Current.SaveChanges();
                    return entry.MapTo<FeaturedContestDto>();
                }
                else
                {
                    entry.StatusId = status.Id;
                    UnitOfWorkManager.Current.SaveChanges();
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        UserId = adminWallet.UserId,
                        Message = $"Featured contest cancellation request from '{currentUser.NickName}' was completed successfully.",
                    });
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        UserId = entry.UserId,
                        Message = $"Your featured contest cancellation request from '{currentUser.NickName}' was completed successfully.",
                    });
                    return entry.MapTo<FeaturedContestDto>();
                }
                    
            }
            entry.StatusId = status.Id;
            UnitOfWorkManager.Current.SaveChanges();
            return entry.MapTo<FeaturedContestDto>();
                    }
                    catch (Exception e)
                    {
                        
                        throw new UserFriendlyException(e.Message);
                    }
           
           
        }

        public async Task NotifyExpiring()
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var items = await _repository.GetAllIncluding(itm => itm.User).Where(x => x.EndDate <= DateTime.Now.AddDays(7) && x.StatusId == status.Id).ToListAsync();
           
            if (items != null && items.Count > 0)
            {
                items.ForEach(item =>
                {
                    BackgroundJob.Enqueue<BgJobFeaturedContests>(x => x.NotifyUserOfExpiring(item.User, item));
                });
            }
        }

        public async Task DisableExpired()
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var items = await _repository.GetAllIncluding(itm => itm.User).Where(x => x.EndDate <= DateTime.Now && x.StatusId == status.Id).ToListAsync();
            if (items != null && items.Count > 0)
            {
                var closedStatus = await _statusAppService.GetByNameAsync(AppConsts.StatusExpired);
                items.ForEach(item =>
                {
                    item.StatusId = closedStatus.Id;
                    var entry = _contestRepository.Get(item.ContestId);
                    entry.IsFeatured = false;
                    BackgroundJob.Enqueue<BgJobFeaturedContests>(x => x.NotifyUserOfExpired(item.User, item));

                });
                await CurrentUnitOfWork.SaveChangesAsync();
            }
        }
    }
}