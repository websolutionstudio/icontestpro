﻿using System.Threading.Tasks;
using Abp.Application.Services;
using IContestPro.Transfer.Dto;

namespace IContestPro.Transfer
{
    public interface ITransferAppService : IApplicationService
    {
        Task<TransferDto> Create(CreateTransferDto input);

    }
}