﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Transfer.Dto
{
    [AutoMapTo(typeof(TransferDto))]
    [Serializable]
    public class CreateTransferDto
    {
        [Required]
        public string ToUserAppIdentity { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public string Notes { get; set; }
    }
}