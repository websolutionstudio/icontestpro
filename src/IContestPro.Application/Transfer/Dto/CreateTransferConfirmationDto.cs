﻿using System.ComponentModel.DataAnnotations;

namespace IContestPro.Transfer.Dto
{
    public class CreateTransferConfirmationDto
    {
        [Required]
        public string ToUserAppIdentity { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public double FeePercentage { get; set; }
        public decimal FeeAmount { get; set; }
        public string ToUserFullName { get; set; }        
        public string Notes { get; set; }
    }
}