﻿using Abp.AutoMapper;

namespace IContestPro.Transfer.Dto
{
    [AutoMapFrom(typeof(CreateTransferDto))]
    public class TransferDto 
    {
        public long FromUserId { get; set; }
        public long ToUserId { get; set; }
        public decimal Amount { get; set; }
        public string Notes { get; set; }
        public bool Success { get; set; }
    }
}