﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using IContestPro.AppFees;
using IContestPro.Authorization.Users;
using IContestPro.Logging;
using IContestPro.Transfer.Dto;
using IContestPro.Wallet;
using IContestPro.Wallet.Dto;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.Transfer
{
    [AbpAuthorize]
     public class TransferAppService : ApplicationService, ITransferAppService
    {
        private readonly IWalletAppService _walletAppService;
        private readonly ILoggingAppService _loggingAppService;
        private readonly IAppFeesAppService _appFeesAppService;
        private readonly IRepository<User, long> _userRepository;
        

        public TransferAppService( IWalletAppService walletAppService,
            ILoggingAppService loggingAppService,
            IAppFeesAppService appFeesAppService,
            IRepository<User, long> userRepository)
        {
            _walletAppService = walletAppService;
            _loggingAppService = loggingAppService;
            _appFeesAppService = appFeesAppService;
            _userRepository = userRepository;
        }

        public async Task<TransferDto> Create(CreateTransferDto input)
        {
            var fromUserBalance = await _walletAppService.GetBalance(AbpSession.GetUserId());
            var toUserId = long.Parse(input.ToUserAppIdentity);
            var toUserBalance = await _walletAppService.GetBalance(toUserId);
            var transferFeePercentage = await _appFeesAppService.GetTransferFeePercentage();
            var percentageChargeAmount = input.Amount * ((decimal)transferFeePercentage / 100);
            var adminWallet = await _walletAppService.GetAdminWallet();
            var fromUser = await _userRepository.GetAll().Select(itm => new {itm.Id, itm.NickName}).FirstOrDefaultAsync(itm => itm.Id == AbpSession.GetUserId());
            var toUser = await _userRepository.GetAll().Select(itm => new {itm.Id, itm.NickName}).FirstOrDefaultAsync(itm => itm.Id == toUserId);

            try
            {
                var totalAmount = input.Amount + percentageChargeAmount;
                if (fromUserBalance < totalAmount)
                {
                    throw new UserFriendlyException($"Insuficient account balance. Your account balance must be above {totalAmount} to complete your request.");
                }
                var debitResult = await _walletAppService.Debit(AbpSession.GetUserId(), input.Amount, false);
                if (debitResult == null)
                {
                    throw new UserFriendlyException("Error occurred while creating withdrawal.");
                }

                //Todo: Log the debit in a new thread
                var reference = Guid.NewGuid().ToString("N");
                await _walletAppService.LogWalletOperation(new WalletInfoDto
                {
                    Reference = reference,
                    Amount = input.Amount, 
                    Title = "Transfer debit",
                    Description =  $"A transfer debit to {toUser.NickName} occurred in your account. Ref: {reference}",
                    WalletInfoResult = WalletInfoResult.Success,
                    WalletOperation = WalletOperation.Debit,
                    UserId = AbpSession.GetUserId(),
                    
                });
                var creditToUser = await _walletAppService.Credit(toUserId, input.Amount, false);
                if (creditToUser == null)
                {
                    throw new UserFriendlyException("Error occurred while creating withdrawal.");
                }
                //Todo: Log the debit in a new thread
                 reference = Guid.NewGuid().ToString("N");
                await _walletAppService.LogWalletOperation(new WalletInfoDto
                {
                    Reference = reference,
                    Amount = input.Amount, 
                    Title = "Transfer credit occurred",
                    Description =  $"A transfer credit from {fromUser.NickName} occurred in your account. {(input.Notes != null ? "Remarks: " + input.Notes : "")} Ref: {reference}",
                    WalletInfoResult = WalletInfoResult.Success,
                    WalletOperation = WalletOperation.Credit,
                    UserId = toUserId
                });
                var feeResult = await _walletAppService.Debit(AbpSession.GetUserId(), percentageChargeAmount, false);
                if (feeResult == null)
                {
                    throw new UserFriendlyException("Error occurred while creating withdrawal.");
                }
                //Todo: Log the debit in a new thread
                reference = Guid.NewGuid().ToString("N");
                await _walletAppService.LogWalletOperation(new WalletInfoDto
                {
                    Reference = reference,
                    Amount = percentageChargeAmount, 
                    Title = "Transfer fee debit",
                    Description =  $"{transferFeePercentage}% fee was charged of {input.Amount} on transfer to {toUser.NickName}. Ref: {reference}",
                    WalletInfoResult = WalletInfoResult.Success,
                    WalletOperation = WalletOperation.Debit,
                    UserId = AbpSession.GetUserId(),
                });
                var feeResultAdminCredit= await _walletAppService.Credit(adminWallet.UserId, percentageChargeAmount, false);
                if (feeResultAdminCredit == null)
                {
                    throw new UserFriendlyException("Error occurred while creating withdrawal.");
                }
                //Todo: Log the debit in a new thread
                reference = Guid.NewGuid().ToString("N");
                await _walletAppService.LogWalletOperation(new WalletInfoDto
                {
                    Reference = reference,
                    Amount = percentageChargeAmount, 
                    Title = $"Transfer fee of {percentageChargeAmount} charge  credit",
                    Description =  $"A transfer fee of {transferFeePercentage}% was charged on the transfer between {fromUser.NickName} and {toUser.NickName}. Ref: {reference}",
                    WalletInfoResult = WalletInfoResult.Success,
                    WalletOperation = WalletOperation.Credit,
                    UserId = adminWallet.UserId
                });
                var itemResult = new TransferDto
                {
                    Amount = input.Amount,
                    FromUserId = fromUser.Id,
                    Notes = input.Notes,
                    Success = true,
                    ToUserId = toUserId
                };
               await UnitOfWorkManager.Current.SaveChangesAsync();
                return itemResult;
            }
            catch (Exception e)
            {
                _loggingAppService.LogException(e);
                Task.Run(async () =>
                {
                    // Some specific work to do...
                    var fromUserBalanceAfter = await _walletAppService.GetBalance(AbpSession.GetUserId());
                    //Todo: Check whether debit occurred
                    if (fromUserBalance > fromUserBalanceAfter)
                    {
                        await _walletAppService.Credit(AbpSession.GetUserId(), input.Amount, false);
                        var refernce = Guid.NewGuid().ToString("N");
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Reference = refernce,
                            Amount = input.Amount, 
                            Title = "Transfer reversal occurred",
                            Description =  $"Your transfer to {toUser.NickName} was reversed as the transfer was not successful. Ref: {refernce}",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Credit,
                            UserId = AbpSession.GetUserId(),
                        }); 
                        
                        var toUserBalanceAfter = await _walletAppService.GetBalance(toUser.Id);
                        //Todo: Check whether debit occurred
                        if (toUserBalanceAfter > toUserBalance)
                        {
                            await _walletAppService.Debit(toUser.Id, input.Amount, false);
                            refernce = Guid.NewGuid().ToString("N");
                            await _walletAppService.LogWalletOperation(new WalletInfoDto
                            {
                                Reference = refernce,
                                Amount = input.Amount,
                                Title = "Transfer reversal debit",
                                Description =
                                    $"A transfer between {fromUser.NickName} and you was reversed as the transaction was not successful. Ref: {refernce}",
                                WalletInfoResult = WalletInfoResult.Failed,
                                WalletOperation = WalletOperation.Debit,
                                UserId = toUser.Id,
                            });
                        }

                        await _walletAppService.Credit(AbpSession.GetUserId(), percentageChargeAmount, false);
                        refernce = Guid.NewGuid().ToString("N");
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Reference = refernce,
                            Amount = percentageChargeAmount, 
                            Title = "Transfer fee reversal credit",
                            Description =  $"{transferFeePercentage}% fee charged on {input.Amount} transfer to {toUser.NickName} was reversed. Ref: {refernce}",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Credit,
                            UserId = AbpSession.GetUserId(),
                        });
                        await _walletAppService.Debit(adminWallet.UserId, percentageChargeAmount, false);
                        refernce = Guid.NewGuid().ToString("N");
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Reference = refernce,
                            Amount = percentageChargeAmount, 
                            Title = "Transfer fee reversal debit",
                            Description =  $"{transferFeePercentage}% fee charged on {input.Amount} transfer between {fromUser.NickName} and {toUser.NickName} was reversed. Ref: {refernce}",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Debit,
                            UserId = adminWallet.UserId,
                        });
                    }

                   
                   // throw e;

                });
            }

            return null;
        }
        
    }
}