﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Follow.Dto;
using IContestPro.SponsorFollow.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.SponsorFollow
{
    public interface ISponsorFollowAppService : IBaseAppService<SponsorFollowDto, long, PagedResultRequestDto, CreateSponsorFollowDto, SponsorFollowDto>
    {
        Task<int> CountFollowersById(int id);
        Task<bool> IsUserFollowingSponsor(long userId, int sponsorId);
        Task<IEnumerable<GetSponsorFollowersDto>> GetSponsorFollowers(int sponsorId, int page, int pageSize = 20);

    }
}