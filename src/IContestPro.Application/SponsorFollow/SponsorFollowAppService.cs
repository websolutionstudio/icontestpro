﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using IContestPro.Authorization;
using IContestPro.Base;
using IContestPro.EntityFrameworkCore;
using IContestPro.EntityFrameworkCore.StoreProcedureManager;
using IContestPro.SponsorFollow.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.SponsorFollow
{
    public class SponsorFollowAppService : BaseAppService<IContestProEntities.SponsorFollow, SponsorFollowDto, long, PagedResultRequestDto, CreateSponsorFollowDto, SponsorFollowDto>, ISponsorFollowAppService
    {
        private readonly IRepository<IContestProEntities.SponsorFollow, long> _repository;
        private readonly IContestProDbContextFactory _contestProDbContextFactory;

        public SponsorFollowAppService(IRepository<IContestProEntities.SponsorFollow, long> repository, IIocResolver  iocResolver, IContestProDbContextFactory contestProDbContextFactory) : base(repository, iocResolver)
        {
            _repository = repository;
            _contestProDbContextFactory = contestProDbContextFactory;
        }

        [AbpAuthorize]
        public override async Task<SponsorFollowDto> Create(CreateSponsorFollowDto input)
        {
            if (input.SponsorId == 0 || input.FollowerUserId == 0)
            {
                throw new UserFriendlyException("Follower user and sponsor are required.");
            } 
            
            if (await IsUserFollowingSponsor(input.FollowerUserId, input.SponsorId))
            {
                throw new UserFriendlyException("You are already following this sponsor.");
            }
            return await base.Create(input);
        }

        public async Task<int> CountFollowersById(int id)
        {
            return await _repository.CountAsync(item => item.SponsorId == id);
        }

        public async Task<bool> IsUserFollowingSponsor(long userId, int sponsorId )
        {
            return await _repository.CountAsync(item => item.FollowerUserId == userId && item.SponsorId == sponsorId) > 0;
        }

        public async Task<IEnumerable<GetSponsorFollowersDto>> GetSponsorFollowers(int sponsorId, int page, int pageSize = 20)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.GetSponsorFollowers)
                    .WithSqlParam("SponsorId", sponsorId)
                    .WithSqlParam("Page", page)
                    .WithSqlParam("PageSize", pageSize);
                var items = await cmd.ExecuteStoredProcedureListAsync<GetSponsorFollowersDto>();
                return items;
            }
        }

    }
}