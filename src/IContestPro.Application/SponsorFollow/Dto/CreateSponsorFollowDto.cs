﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.SponsorFollow.Dto
{
    [AutoMapTo(typeof(IContestProEntities.SponsorFollow))]
    public class CreateSponsorFollowDto
    {
        /// <summary>
        /// SponsorId is the id of the sponsor to follow
        /// </summary>
        [Required]
        public int SponsorId { get; set; }
        /// <summary>
        /// FollowerUserId is the id of the user following the sponsor
        /// </summary>
        [Required]
        
        public long FollowerUserId { get; set; }
        
       
    }
}