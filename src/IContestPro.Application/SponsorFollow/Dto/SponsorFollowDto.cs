﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Sponsor.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.SponsorFollow.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.SponsorFollow))]
    public class SponsorFollowDto : FullAuditedEntityDto<long>, IMayHaveTenant
    {
        [Required]
        public int SponsorId { get; set; }
        [Required]
        public long FollowerUserId { get; set; }

        public int? TenantId { get; set; }

        public SponsorDto Sponsor { get; set; }
        public UserDto User { get; set; }
    }
}