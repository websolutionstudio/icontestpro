﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.SponsorFollow.Dto
{
    public class GetSponsorFollowersDto
    {
       // u.Id UserId, u.Image, u.NickName, u.Permalink, sf.SponsorId
        public long UserId { get; set; }
        public string Image { get; set; }
        public string Nickname { get; set; }
        public string Permalink { get; set; }
        public int SponsorId { get; set; }
       
    }
}