﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using IContestPro.Authorization;
using IContestPro.Status.Dto;

namespace IContestPro.Status
{
    public class StatusAppService : AsyncCrudAppService<IContestProEntities.Status, StatusDto, int,
                                PagedResultRequestDto, CreateStatusDto, StatusDto>, IStatusAppService
    {
        private readonly IRepository<IContestProEntities.Status> _repository;
        public StatusAppService(IRepository<IContestProEntities.Status> repository) : base(repository)
        {
            _repository = repository;
        }

        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<StatusDto> Create(CreateStatusDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.Status>());
            UnitOfWorkManager.Current.SaveChanges();
            return item.MapTo<StatusDto>();
        }
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<StatusDto> Update(StatusDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.Status>());
            UnitOfWorkManager.Current.SaveChanges();
            return item.MapTo<StatusDto>();
        }

        public async Task<StatusDto> GetByNameAsync(string name)
        {
            var status = await _repository.FirstOrDefaultAsync(st => st.Name == name);
            return ObjectMapper.Map<StatusDto>(status);
        }
    }
}