﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;

namespace IContestPro.Status.Dto
{
    
    [AutoMapFrom(typeof(IContestProEntities.Status))]
    public class StatusDto : FullAuditedEntityDto, IMayHaveTenant
    {
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(100)]
        public string Description { get; set; }
        public int? TenantId { get; set; }
        
    }
}