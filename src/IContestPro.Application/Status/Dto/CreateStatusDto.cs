﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Status.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Status))]
    public class CreateStatusDto
    {
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(100)]
        public string Description { get; set; }
    }
}