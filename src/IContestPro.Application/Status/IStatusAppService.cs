﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using IContestPro.Status.Dto;

namespace IContestPro.Status
{
    public interface IStatusAppService : IAsyncCrudAppService<StatusDto, int, PagedResultRequestDto, CreateStatusDto, StatusDto>
    {
        Task<StatusDto> GetByNameAsync(string name);
    }
}