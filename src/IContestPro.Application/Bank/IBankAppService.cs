﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Bank.Dto;

namespace IContestPro.Bank
{
    public interface IBankAppService : IBaseAppService<BankDto, int, PagedResultRequestDto, CreateBankDto, UpdateBankDto>
    {
        Task<BankDto> GetByName(string name);
        Task<PagedResultDto<BankDto>> GetAllActive(PagedResultRequestDto input);
    }
}