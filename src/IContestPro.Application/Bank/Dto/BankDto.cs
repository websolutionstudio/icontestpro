﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace IContestPro.Bank.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Bank))]
    public class BankDto : FullAuditedEntityDto
    {
        public bool Active { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}