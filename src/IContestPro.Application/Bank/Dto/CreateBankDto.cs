﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Bank.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Bank))]
    public class CreateBankDto
    {
        /// <summary>
        /// Id is needed because it is not auto incremented
        /// </summary>
        public int Id { get; set; } 
        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
        public bool Active { get; set; }


    }
}