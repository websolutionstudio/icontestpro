﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace IContestPro.Bank.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Bank))]
    public class UpdateBankDto : FullAuditedEntityDto
    {
        public bool Active { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
    }
}