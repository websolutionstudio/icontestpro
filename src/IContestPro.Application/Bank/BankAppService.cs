﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.UI;
using IContestPro.Base;
using IContestPro.Bank.Dto;

namespace IContestPro.Bank
{
    [AbpAuthorize]
     public class BankAppService : BaseAppService<IContestProEntities.Bank, BankDto, int,
                                PagedResultRequestDto, CreateBankDto, UpdateBankDto>, IBankAppService
    {
        private readonly IRepository<IContestProEntities.Bank> _repository;

        public BankAppService(IRepository<IContestProEntities.Bank> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }
        public override async Task<PagedResultDto<BankDto>> GetAll(PagedResultRequestDto input)
        {
            var query = CreateFilteredQuery(input);
          
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.Name);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<BankDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        public async Task<PagedResultDto<BankDto>> GetAllActive(PagedResultRequestDto input)
        {
            var query = CreateFilteredQuery(input).Where(item => item.Active);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.Name);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<BankDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        
        public override async Task<BankDto> Create(CreateBankDto input)
        {
            if (await _repository.CountAsync(itm => itm.Name == input.Name) > 0)
            {
                throw new UserFriendlyException("Bank already exists.");
            }
            if (await _repository.CountAsync(itm => itm.Code == input.Code) > 0)
            {
                throw new UserFriendlyException("Bank code already exists.");
            }
            var lastItem = _repository.GetAll().OrderByDescending(item => item.Id).Select(item => new {item.Id}).FirstOrDefault();
            if (lastItem != null)
            {
                input.Id = lastItem.Id + 1;
            }
            else
            {
                input.Id = 1;
            }
            return await base.Create(input);
        }
        
        public async Task<BankDto> GetByName(string name)
        {
            return  (await _repository.FirstOrDefaultAsync(itm => itm.Name == name)).MapTo<BankDto>();
        }
    }
}