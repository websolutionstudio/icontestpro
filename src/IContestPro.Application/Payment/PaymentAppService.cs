﻿using Abp.Application.Services.Dto;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Base;
using IContestPro.Payment.Dto;

namespace IContestPro.Payment
{
    public class PaymentAppService : BaseAppService<IContestProEntities.Payment, PaymentDto, int,
                                PagedResultRequestDto, CreatePaymentDto, PaymentDto>, IPaymentAppService
    {
        private readonly IRepository<IContestProEntities.Payment> _repository;

        public PaymentAppService(IRepository<IContestProEntities.Payment> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }

        
    }
       
    
}