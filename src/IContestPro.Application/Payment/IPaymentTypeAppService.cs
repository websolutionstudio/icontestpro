﻿using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Payment.Dto;

namespace IContestPro.Payment
{
    public interface IPaymentAppService : IBaseAppService<PaymentDto, int, PagedResultRequestDto, CreatePaymentDto, PaymentDto>
    {
    }
}