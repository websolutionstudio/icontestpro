﻿using System.ComponentModel.DataAnnotations;

namespace IContestPro.Payment.Dto
{
    public class CreatePaymentDto 
    {
        [Required]
        public int ContestId { get; set; }
        [Required]
        public long UserId { get; set; }
        [Required]
        public decimal Amount { get; set; }
    }
}