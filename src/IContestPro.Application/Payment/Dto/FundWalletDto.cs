﻿namespace IContestPro.Payment.Dto
{
    public class FundWalletDto
    {
        public string Reference { get; set; }
        public decimal Amount { get; set; }
    }
}