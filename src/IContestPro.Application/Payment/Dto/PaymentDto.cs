﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using IContestPro.Contest.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Payment.Dto
{
    public class PaymentDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public int ContestId { get; set; }
        public long UserId { get; set; }
        public decimal Amount { get; set; }
        public int? TenantId { get; set; }
        
        public UserDto User { get; set; }
        public ContestDto Contest { get; set; }
    }
}