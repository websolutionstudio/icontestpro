﻿using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using Abp.Application.Services;
using IContestPro.AssetUpload.Dto;

namespace IContestPro.AssetUpload
{
    public interface IAssetUploadAppService : IApplicationService
    {
        /// <summary>
        /// The mathod caters for the image upload.
        /// File is the file to be uploaded.
        /// Path is the relative path from the web root e.g image/uploads.
        /// </summary>
        Task<ImageUploadDto> UploadImage(UploadDto model, int quality = 75);
        Task<Dictionary<string,string>> UploadVideo(VideoUploadDto model);
        bool Delete(DeleteAssetDto model);
        ImageUploadDto CropAndUploadImage(UploadDto model, int width, int height, int quality = 75);
        ImageUploadDto CropAndUploadImage(UploadDto model, int width, int height, Color paddingColor, int quality = 75);
    }
}