﻿using System.ComponentModel.DataAnnotations;

namespace IContestPro.AssetUpload.Dto
{
    
    /// <summary>
    /// The class caters for the image upload
    /// File is the file to be uploaded
    /// Path is the relative path from the web root e.g image/uploads
    /// </summary>
   public class DeleteAssetDto
    {
        /// <summary>
        /// The path from the root. e.g /images/uploads
        /// </summary>
        [Required]
        public string Path { get; set; }
        
    }
}