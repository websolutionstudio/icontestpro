﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace IContestPro.AssetUpload.Dto
{
    
    /// <summary>
    /// The class caters for the image upload
    /// File is the file to be uploaded
    /// Path is the relative path from the web root e.g image/uploads
    /// </summary>
   public class UploadDto
    {
        [Required]
         public IFormFile File { get; set; }
        [Required]
        public string Path { get; set; }
        
    }
}