﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace IContestPro.AssetUpload.Dto
{
    
   public class VideoUploadDto
    {
        [Required]
        public IFormFile File { get; set; }
        [Required]
        public string VideoPath { get; set; }
        [Required]
        public string VideoPosterPath { get; set; }
    }
}