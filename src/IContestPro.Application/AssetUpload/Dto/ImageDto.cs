﻿using Microsoft.AspNetCore.Http;

namespace IContestPro.AssetUpload.Dto
{
    
   public class ImageUploadDto
    {
        public IFormFile File { get; set; }
        public string FileName { get; set; }
    }
}