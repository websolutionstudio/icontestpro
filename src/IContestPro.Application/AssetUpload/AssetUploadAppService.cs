﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using Abp.UI;
using IContestPro.AssetUpload.Dto;
using ImageMagick;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Xabe.FFmpeg;
using Xabe.FFmpeg.Enums;

namespace IContestPro.AssetUpload
{
    public class AssetAssetUploadAppService :  IAssetUploadAppService
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public AssetAssetUploadAppService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        ///CropAndUploadVideo(VideoUploadDto model, int width, int height)
        public ImageUploadDto CropAndUploadImage(UploadDto model, int width, int height, int quality = 75)
        {
            return CropAndUploadImage(model, width, height, Color.White, quality);
        }

        public ImageUploadDto CropAndUploadImage(UploadDto model, int width, int height, Color paddingColor,
            int quality = 75)
        {
            if (model.File == null || model.File.Length == 0)
            {
                throw new UserFriendlyException("Please upload a file.");
            }
            if (model.File.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
            {
                throw new UserFriendlyException("Please select only image file.");
            }
            var image = CropImage(model.File, width, height, paddingColor);
            try
            {
                var uniqueFileName = Guid.NewGuid().ToString("N") + ".jpg";
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, model.Path);
                var filePath = Path.Combine(uploads, uniqueFileName);
                image.Save(filePath, ImageFormat.Jpeg);

                //Give to hangfire
                //Compress image
                var file = new FileInfo(filePath);
                /*Console.WriteLine("Bytes before: " + file.Length);
                var optimizer = new ImageOptimizer();
                optimizer.Compress(file);*/
                //file.Refresh();
                // Console.WriteLine("Bytes after:  " + file.Length);
                using (var imageFile = new MagickImage(file))
                {
                    // Sets the output format to jpeg
                    imageFile.Format = MagickFormat.Jpeg;
                    // Create byte array that contains a jpeg file
                    imageFile.Quality = quality;
                    //image.Resize(40, 40);
                    File.WriteAllBytes(filePath, imageFile.ToByteArray());
                }
                // file.Refresh();
                // Console.WriteLine("Bytes after quality set:  " + file.Length);
                return new ImageUploadDto{FileName = uniqueFileName};
            }
            catch (Exception e)
            {
                throw new UserFriendlyException("Error occurred while uploading your image.");
            }
        }
        public async Task<ImageUploadDto> UploadImage(UploadDto model, int quality = 75)
        {
            if (model.File == null || model.File.Length == 0)
            {
                throw new UserFriendlyException("Please upload a file.");
            }
            if (model.File.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
            {
                throw new UserFriendlyException("Please select only image file.");
            }
            try
            {
                var uniqueFileName = Guid.NewGuid().ToString("N") + ".jpg";
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, model.Path);
                var filePath = Path.Combine(uploads, uniqueFileName);
                await model.File.CopyToAsync(new FileStream(filePath, FileMode.Create));
                
                //Give to hangfire
                //Compress image
                var file = new FileInfo(filePath);
                /*Console.WriteLine("Bytes before: " + file.Length);
                var optimizer = new ImageOptimizer();
                optimizer.Compress(file);*/
                //file.Refresh();
               // Console.WriteLine("Bytes after:  " + file.Length);
                using (var image = new MagickImage(file))
                {
                    // Sets the output format to jpeg
                    image.Format = MagickFormat.Jpeg;
                    // Create byte array that contains a jpeg file
                    image.Quality = quality;
                    //image.Resize(40, 40);
                    File.WriteAllBytes(filePath, image.ToByteArray());
                }
               // file.Refresh();
               // Console.WriteLine("Bytes after quality set:  " + file.Length);
                return new ImageUploadDto{FileName = uniqueFileName};
            }
            catch (Exception e)
            {
                throw new UserFriendlyException("Error occurred while uploading your image.");
            }
           
        }

        public async Task<Dictionary<string, string>> UploadVideo(VideoUploadDto model)
        {
            if (model.File == null || model.File.Length == 0)
            {
                    throw new UserFriendlyException("Please upload your video file.");
            }
            if (model.File.ContentType.IndexOf("mp4", StringComparison.OrdinalIgnoreCase) < 0)
            {
                    throw new UserFriendlyException("Please select only mp4 video file.");
            }
            try
            {
            //var videoUploads = Path.Combine(_hostingEnvironment.WebRootPath, "videos");
            var videoUploads = Path.Combine(_hostingEnvironment.WebRootPath, model.VideoPath);
            var videoFileName = Guid.NewGuid().ToString("N") + ".mp4";
            var fileVideoPath = Path.Combine(videoUploads, videoFileName);
            await model.File.CopyToAsync(new FileStream(fileVideoPath, FileMode.Create));
            var videoPosterName = Guid.NewGuid() + FileExtensions.Png;
            var posterOutput = Path.Combine(_hostingEnvironment.WebRootPath + "/" + model.VideoPosterPath, videoPosterName);
            var snapshotResult = await Conversion.Snapshot(fileVideoPath, posterOutput, TimeSpan.FromSeconds(0))
                .Start();
            //Todo: Optimize image
            var file = new FileInfo(posterOutput);
            
            //Todo: Read image from file
            using (var image = new MagickImage(file))
            {
                // Sets the output format to jpeg
                image.Format = MagickFormat.Jpeg;
                // Create byte array that contains a jpeg file
                image.Quality = 30;
                //image.Resize(40, 40);
                File.WriteAllBytes(posterOutput, image.ToByteArray());
            }

            var result = new Dictionary<string, string>();
            result.Add("video", videoFileName);
            result.Add("poster", videoPosterName);
            return result;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException("Error occurred while uploading your video.");

            }
        }
       
        
        /// <summary>
        /// Used to delete images
        /// </summary>
        /// <param name="model">DeleteAssetDto model. The Path is the file path from the assets root folder</param>
        /// <returns>bool</returns>
        public bool Delete(DeleteAssetDto model)
        {
            try
            {
            var filePath = Path.Combine(_hostingEnvironment.WebRootPath, model.Path);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                return true;
            }
            }
            catch (Exception e)
            {
                return false;
            }
            return false;

        }
        
        public Image CropImage(IFormFile file , int width, int height)
        {
                return CropImage(file, width, height, Color.White); 
           
        }

        public Image CropImage(IFormFile file , int width, int height, Color paddingColor)
        {
            using (var image = Image.FromStream(file.OpenReadStream()))
            {
                return CropImage(image, width, height, paddingColor); 
            }
           
        }

        
        public Image CropImage(string path, int width, int height, Color paddingColor)
        {
            using (var image = Image.FromFile(path))
            {
                var thumbnail = 
                    new Bitmap(width, height); // changed parm names
                var graphic = Graphics.FromImage(thumbnail);

                graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphic.SmoothingMode = SmoothingMode.HighQuality;
                graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphic.CompositingQuality = CompositingQuality.HighQuality;

                /* ------------------ new code --------------- */

                // Figure out the ratio
                var ratioX = (double) width / (double) image.Width;
                var ratioY = (double) height / (double) image.Height;
                // use whichever multiplier is smaller
                var ratio = ratioX < ratioY ? ratioX : ratioY;

                // now we can get the new height and width
                var newHeight = Convert.ToInt32(image.Height * ratio);
                var newWidth = Convert.ToInt32(image.Width * ratio);

                // Now calculate the X,Y position of the upper-left corner 
                // (one of these will always be zero)
                var posX = Convert.ToInt32((width - (image.Width * ratio)) / 2);
                var posY = Convert.ToInt32((height - (image.Height * ratio)) / 2);

                graphic.Clear(paddingColor); // white padding
                graphic.DrawImage(image, posX, posY, newWidth, newHeight);

                /* ------------- end new code ---------------- */

                var encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality,
                    100L);            
                return thumbnail; 
            }
        }

        public Image CropImage(Image image, int width, int height)
        {
            return CropImage(image, width, height, Color.White);
        }
        public Image CropImage(Image image, int width, int height, Color paddingColor)
        {
                var thumbnail = 
                    new Bitmap(width, height); // changed parm names
                var graphic = Graphics.FromImage(thumbnail);

                graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphic.SmoothingMode = SmoothingMode.HighQuality;
                graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphic.CompositingQuality = CompositingQuality.HighQuality;

                /* ------------------ new code --------------- */

                // Figure out the ratio
                var ratioX = (double) width / (double) image.Width;
                var ratioY = (double) height / (double) image.Height;
                // use whichever multiplier is smaller
                var ratio = ratioX < ratioY ? ratioX : ratioY;

                // now we can get the new height and width
                var newHeight = Convert.ToInt32(image.Height * ratio);
                var newWidth = Convert.ToInt32(image.Width * ratio);

                // Now calculate the X,Y position of the upper-left corner 
                // (one of these will always be zero)
                var posX = Convert.ToInt32((width - (image.Width * ratio)) / 2);
                var posY = Convert.ToInt32((height - (image.Height * ratio)) / 2);

                graphic.Clear(paddingColor); // white padding
                graphic.DrawImage(image, posX, posY, newWidth, newHeight);

                /* ------------- end new code ---------------- */

                var encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality,
                    100L);            
                return thumbnail; 
        }

        
    }
}