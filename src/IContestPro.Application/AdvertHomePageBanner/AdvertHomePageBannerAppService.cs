﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Notifications;
using Abp.UI;
using Hangfire;
using IContestPro.Base;
using IContestPro.AdvertHomePageBanner.Dto;
using IContestPro.AdvertPrice;
using IContestPro.AssetUpload;
using IContestPro.AssetUpload.Dto;
using IContestPro.BackgroundJobs;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Contest.Dto;
using IContestPro.EntityFrameworkCore;
using IContestPro.Helpers;
using IContestPro.Status;
using IContestPro.Users;
using IContestPro.Wallet;
using IContestPro.Wallet.Dto;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.AdvertHomePageBanner
{
     public class AdvertHomePageBannerAppService : BaseAppService<IContestProEntities.AdvertHomePageBanner, AdvertHomePageBannerDto, int,
                                PagedResultRequestDto, CreateAdvertHomePageBannerDto, AdvertHomePageBannerDto>, IAdvertHomePageBannerAppService
    {
        private readonly IRepository<IContestProEntities.AdvertHomePageBanner> _repository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAdvertPriceAppService _advertPriceAppService;
        private readonly IAssetUploadAppService _assetUploadAppService;
        private readonly IWalletAppService _walletAppService;
        private readonly IUserAppService _userAppService;
        private readonly IContestProDbContextFactory _contestProDbContextFactory;

        public AdvertHomePageBannerAppService(IRepository<IContestProEntities.AdvertHomePageBanner> repository, IStatusAppService statusAppService, IAdvertPriceAppService advertPriceAppService,
            IAssetUploadAppService assetUploadAppService, IWalletAppService walletAppService, 
            IUserAppService userAppService, IIocResolver  iocResolver,
            IContestProDbContextFactory contestProDbContextFactory) : base(repository, iocResolver)
        {
            _repository = repository;
            _statusAppService = statusAppService;
            _advertPriceAppService = advertPriceAppService;
            _assetUploadAppService = assetUploadAppService;
            _walletAppService = walletAppService;
            _userAppService = userAppService;
            _contestProDbContextFactory = contestProDbContextFactory;
        }
        public async Task<IEnumerable<AdvertHomePageBannerDto>> GetRandomLive(int totalTake = 3)
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var query =  _repository.GetAll().Where(item => item.StatusId == status.Id && item.StartDate.AddHours(-1) <= DateTime.Now && item.EndDate >= DateTime.Now);
            query = query.OrderBy(item => Guid.NewGuid()).Take(totalTake);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new List<AdvertHomePageBannerDto>(
                entities.Select(MapToEntityDto).ToList()
            );
        } 
        [AbpAuthorize]
        public override async Task<PagedResultDto<AdvertHomePageBannerDto>> GetAll(PagedResultRequestDto input)
        {
            
            var query =  _repository.GetAllIncluding(item => item.Status);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            
            return new PagedResultDto<AdvertHomePageBannerDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        [AbpAuthorize]
        public async Task<PagedResultDto<AdvertHomePageBannerDto>> GetAllByStatus(string statusName, PagedResultRequestDto input)
        {
            var status = await _statusAppService.GetByNameAsync(statusName);
            if (status == null)
            {
                return null;
            }
            var query =  _repository.GetAllIncluding(item => item.Status).Where(item => item.StatusId == status.Id);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            
            return new PagedResultDto<AdvertHomePageBannerDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        [AbpAuthorize]
        public async Task<PagedResultDto<AdvertHomePageBannerDto>> GetAllByUser(long userId, PagedResultRequestDto input)
        {
            var query =  _repository.GetAllIncluding(item => item.Status).Where(item => item.UserId == userId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderBy(item => item.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<AdvertHomePageBannerDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
       
        
        [AbpAuthorize]
        public override Task<AdvertHomePageBannerDto> Create(CreateAdvertHomePageBannerDto input)
        {
            throw new UserFriendlyException("You are not allowed to use this method to create home page banner. Please use CreateAndUploadImage method instead.");
        }
        [AbpAuthorize]
        public override Task<AdvertHomePageBannerDto> Update(AdvertHomePageBannerDto input)
        {
            throw new UserFriendlyException("You are not allowed to use this method to update home page banner. Please use UpdateAndUploadImage method instead.");
        }
        [AbpAuthorize]
         public async Task<AdvertHomePageBannerDto> CreateAndUploadImage(CreateEditHomePageBannerDto input)
        {
            if (input.Title == null || input.Description == null || input.StartDate == null)
            {
                throw new UserFriendlyException("Title, description and start date are required.");
            }
            
            if (input.ExternalUrl == null)
            {
                throw new UserFriendlyException("External url is required.");
            }
           
            if (input.UserId == null)
            {
                throw new UserFriendlyException("User id is required.");
            }
            if (input.AdvertPriceId == null)
            {
                throw new UserFriendlyException("Duration is required.");
            }
            
            var wallet = await _walletAppService.GetByUserId(input.UserId.Value);
            if (wallet == null)
            {
                throw new UserFriendlyException("User wallet information not found.");
            }
            var advertPrice = await _advertPriceAppService.Get(new EntityDto{Id = input.AdvertPriceId.Value});
            if (wallet.Balance < advertPrice.Amount)
            {
                throw new UserFriendlyException("Insufficieent balance. Please add fund to your wallet to complete your request.");
            }
            using (var context = _contestProDbContextFactory.CreateDbContext(new []{""}))
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusPending);
            input.StatusId = status.Id;
            var dbModel = input.MapTo<CreateAdvertHomePageBannerDto>();

            dbModel.AmountPaid = advertPrice.Amount;
            await _walletAppService.Debit(input.UserId.Value, dbModel.AmountPaid, false);
           await _walletAppService.LogWalletOperation(new WalletInfoDto
            {
                Reference = Guid.NewGuid().ToString(),
                Amount = dbModel.AmountPaid, 
                Title = "Home banner creation debit occurred",
                Description =  "A debit occurred in your wallet to complete your home page banner creation.",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Debit,
                UserId = input.UserId.Value
            });
            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                Message = $"A debit of {dbModel.AmountPaid.ToNairaString()} occurred in your wallet to complete your home page banner creation.",
                Severity = NotificationSeverity.Success,
                UserId = input.UserId.Value
            });
            
            var walletAdmin = await _walletAppService.GetAdminWallet();
            await _walletAppService.Credit(walletAdmin.UserId, dbModel.AmountPaid, false);
            var user = await _userAppService.Get(new EntityDto<long>{Id = input.UserId.Value});
            await _walletAppService.LogWalletOperation(new WalletInfoDto
            {
                Reference = Guid.NewGuid().ToString(),
                Amount = dbModel.AmountPaid, 
                Title = "Home banner creation credit occurred",
                Description =  $"A credit occurred in your wallet from '{user.NickName}' to complete creation of home page banner.",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Credit,
                UserId = walletAdmin.UserId
            });
            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                Message = $"A credit of {dbModel.AmountPaid.ToNairaString()} occurred in your wallet from '{user.NickName}' to complete creation of home page banner.",
                Severity = NotificationSeverity.Success,
                UserId = walletAdmin.UserId
            });
            
            //Todo: Make sure image dimension is 1920x1200
            var image = await _assetUploadAppService.UploadImage(new UploadDto{File = input.Image, Path = AppConsts.ImageUploadFolder});
            dbModel.Image = image.FileName;
            dbModel.StatusId = status.Id;
            dbModel.EndDate =  dbModel.StartDate.AddDays(advertPrice.Days);
            var savedItem = await base.Create(dbModel);
                        transaction.Commit();
                        return savedItem;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw new UserFriendlyException(e.Message);
                    }
           
                }
            }
        }
        [AbpAuthorize]
        public async Task<AdvertHomePageBannerDto> UpdateAndUploadImage(CreateEditHomePageBannerDto input)
        {
            if (input.StartDate == null)
            {
                throw new UserFriendlyException("StartDate date must be at least one day ahead.");
            }
            if (input.ExternalUrl == null)
            {
                throw new UserFriendlyException("External url is required.");
            }
            if (input.StartDate > input.EndDate || input.StartDate == input.EndDate)
            {
                throw new UserFriendlyException("StartDate date must be less than end date.");
            }
            
            if (input.UserId == null)
            {
                throw new UserFriendlyException("User id is required.");
            }
            if (input.AdvertPriceId == null)
            {
                throw new UserFriendlyException("Duration is required.");
            }
            if (input.Id == null)
                        {
                            throw new UserFriendlyException("ID is required.");
                        }

            var itemToUpdate = await _repository.GetAsync(input.Id.Value);
            if (itemToUpdate == null)
            {
                throw new UserFriendlyException("Item not found.");
            }
            var wallet = await _walletAppService.GetByUserId(input.UserId.Value);
            if (wallet == null)
            {
                throw new UserFriendlyException("User wallet information not found.");
            }
            
            var advertPrice = await _advertPriceAppService.Get(new EntityDto{Id = input.AdvertPriceId.Value});
            using (var context = _contestProDbContextFactory.CreateDbContext(new []{""}))
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
            if (itemToUpdate.AdvertPriceId != advertPrice.Id)
            {
                var advertPriceOld = await _advertPriceAppService.Get(new EntityDto{Id = itemToUpdate.AdvertPriceId});
                var priceDiff = advertPrice.Amount - itemToUpdate.AmountPaid;
                if (priceDiff > 0)
              {
                  if (wallet.Balance < priceDiff)
                  {
                      throw new UserFriendlyException("Insufficieent balance. Please add fund to your wallet to complete your request.");
                  }
                await _walletAppService.Debit(input.UserId.Value, priceDiff, false);
                await _walletAppService.LogWalletOperation(new WalletInfoDto
            {
                Reference = Guid.NewGuid().ToString(),
                Amount = priceDiff, 
                Title = "Home banner edit debit occurred",
                Description =  $"A home page edit debit occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Debit,
                UserId = input.UserId.Value
            });
            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                Message = $"A home page edit debit of {priceDiff.ToNairaString()} occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                Severity = NotificationSeverity.Success,
                UserId = input.UserId.Value
            });
            
            var walletAdmin = await _walletAppService.GetAdminWallet();
            await _walletAppService.Credit(walletAdmin.UserId, priceDiff, false);
            var user = await _userAppService.Get(new EntityDto<long>{Id = input.UserId.Value});
            await _walletAppService.LogWalletOperation(new WalletInfoDto
            {
                Reference = Guid.NewGuid().ToString(),
                Amount = priceDiff, 
                Title = "Home banner edit credit occurred",
                Description =  $"A home banner edit credit occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from '{user.NickName}' to complete the amount difference for the edit he initiated.",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Credit,
                UserId = walletAdmin.UserId
            });
            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                Message = $"A home banner edit credit of {priceDiff.ToNairaString()} occurred in your wallet for the upgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from '{user.NickName}' to complete the amount difference for the edit he initiated.",
                Severity = NotificationSeverity.Success,
                UserId = walletAdmin.UserId
            });
                }
                else
                {
                    priceDiff = Math.Abs(priceDiff);
                    await _walletAppService.Credit(input.UserId.Value, priceDiff, false);
                    await _walletAppService.LogWalletOperation(new WalletInfoDto
                    {
                        Reference = Guid.NewGuid().ToString(),
                        Amount = priceDiff, 
                        Title = "Home banner edit credit occurred",
                        Description = $"A home banner edit credit occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                        WalletInfoResult = WalletInfoResult.Success,
                        WalletOperation = WalletOperation.Credit,
                        UserId = input.UserId.Value
                    });
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        Message = $"A home banner edit credit of {priceDiff.ToNairaString()} occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} to complete the amount difference for the edit you initiated.",
                        Severity = NotificationSeverity.Success,
                        UserId = input.UserId.Value
                    });
           
            
            var walletAdmin = await _walletAppService.GetAdminWallet();
            await _walletAppService.Debit(walletAdmin.UserId, priceDiff, false);
            var user = await _userAppService.Get(new EntityDto<long>{Id = input.UserId.Value});
           
                    await _walletAppService.LogWalletOperation(new WalletInfoDto
                    {
                        Reference = Guid.NewGuid().ToString(),
                        Amount = priceDiff, 
                        Title = "Home banner edit debit occurred",
                        Description =  $"A home page edit debit occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from {user.NickName} to complete amount difference for the edit he initiated.",
                        WalletInfoResult = WalletInfoResult.Success,
                        WalletOperation = WalletOperation.Debit,
                        UserId = walletAdmin.UserId
                    });
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        Message = $"A home page edit debit occurred in your wallet for the downgrade from {advertPriceOld.Name} of {advertPriceOld.Amount.ToNairaString()} to {advertPrice.Name} of {advertPrice.Amount.ToNairaString()} from {user.NickName} to complete amount difference for the edit he initiated.",
                        Severity = NotificationSeverity.Success,
                        UserId = walletAdmin.UserId
                    });
                    
                }
            }
            
            //Todo: Make sure image dimension is 1920x1200
            itemToUpdate.StartDate = input.StartDate.Value;
            itemToUpdate.AdvertPriceId = input.AdvertPriceId.Value;
            itemToUpdate.ExternalUrl = input.ExternalUrl;
            ImageUploadDto imageDto;
            if(input.Image != null)
            {
                imageDto = await _assetUploadAppService.UploadImage(new UploadDto{File = input.Image, Path = AppConsts.ImageUploadFolder});
                _assetUploadAppService.Delete(new DeleteAssetDto{Path = AppConsts.ImageUploadFolder + "/" + itemToUpdate.Image});
                itemToUpdate.Image = imageDto.FileName;
            }
            itemToUpdate.EndDate =  itemToUpdate.StartDate.AddDays(advertPrice.Days);
            itemToUpdate.AmountPaid =  advertPrice.Amount;
            await CurrentUnitOfWork.SaveChangesAsync();
                        transaction.Commit();
            return MapToEntityDto(itemToUpdate);
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw new UserFriendlyException(e.Message);
                    }
           
                }
            }
        }

        [AbpAuthorize]
        public async Task<AdvertHomePageBannerDto> UpdateStatusAsync(UpdateStatusDto model)
        {
            var entry = await _repository.FirstOrDefaultAsync(itm => itm.Id == model.EntityId);
            var status =  await _statusAppService.GetByNameAsync(model.StatusName);
            var pendingStatus =  await _statusAppService.GetByNameAsync(AppConsts.StatusPending);
            if (entry == null || status == null)
            {
                return null;
            }
            using (var context = _contestProDbContextFactory.CreateDbContext(new []{""}))
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
            if (status.Name.Equals(AppConsts.StatusLive) && pendingStatus.Id == entry.StatusId)
            {
                entry.StartDate = DateTime.Now;
            } 
            if (status.Name.Equals(AppConsts.StatusCancelled))
            {
                var daysAdvertRan = (DateTime.Now - entry.StartDate).Days;
                var totaldaysForAdvert = await _advertPriceAppService.Get(new EntityDto {Id = entry.AdvertPriceId});
                var amountUsed = entry.AmountPaid - (((decimal)daysAdvertRan * entry.AmountPaid) / (decimal)totaldaysForAdvert.Days);
                var adminWallet = await _walletAppService.GetAdminWallet();
                var currentUser = await _userAppService.GetCurrentUser();
                if (amountUsed > 0)
                {
                        await _walletAppService.Credit(entry.UserId, amountUsed, false);
                        await _walletAppService.LogWalletOperation(
                            new WalletInfoDto
                        {
                            Amount = amountUsed, 
                            Title = "Home page banner cancellation credit",
                            Description =  $"Your home page banner cancellation reversal credit occurred in your account. {amountUsed.ToNairaString()} was credited from {entry.AmountPaid.ToNairaString()} you paid because the advert has ran for {daysAdvertRan} days.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Credit,
                            UserId = entry.UserId,
                        }); 
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = entry.UserId,
                            TenantId = AbpSession.TenantId,
                            Message =  $"Your home page banner cancellation reversal credit occurred in your account. {amountUsed.ToNairaString()} was credited from {entry.AmountPaid.ToNairaString()} you paid because the advert has ran for {daysAdvertRan} days.",
                        });
                        await _walletAppService.Debit(adminWallet.UserId, amountUsed, false);
                        await _walletAppService.LogWalletOperation(new WalletInfoDto
                        {
                            Amount = amountUsed, 
                            Title = "Home page banner cancellation debit",
                            Description =  $"Your home page banner cancellation debit occurred in your account from '{currentUser.NickName}'. {amountUsed.ToNairaString()} was debited from {entry.AmountPaid.ToNairaString()} paid for the banner because the advert has ran for {daysAdvertRan} days.",
                            WalletInfoResult = WalletInfoResult.Failed,
                            WalletOperation = WalletOperation.Debit,
                            UserId = adminWallet.UserId,
                        });
                        await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                        {
                            UserId = adminWallet.UserId,
                            TenantId = AbpSession.TenantId,
                            Message = $"Your home page banner cancellation debit occurred in your account from '{currentUser.NickName}'. {amountUsed.ToNairaString()} was debited from {entry.AmountPaid.ToNairaString()} paid for the banner because the advert has ran for {daysAdvertRan} days.",
                        });  
                    entry.StatusId = status.Id;
                    UnitOfWorkManager.Current.SaveChanges();
                    return entry.MapTo<AdvertHomePageBannerDto>();
                }
                else
                {
                    entry.StatusId = status.Id;
                    UnitOfWorkManager.Current.SaveChanges();
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        UserId = adminWallet.UserId,
                        Message = $"Horomme page banner cancellation request from '{currentUser.NickName}' was completed successfully.",
                    });
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        UserId = entry.UserId,
                        Message = $"Your home page banner cancellation request from '{currentUser.NickName}' was completed successfully.",
                    });
                    return entry.MapTo<AdvertHomePageBannerDto>();
                }
                    
            }
            entry.StatusId = status.Id;
            UnitOfWorkManager.Current.SaveChanges();
            transaction.Commit();
            return entry.MapTo<AdvertHomePageBannerDto>();
        }
        catch (Exception e)
        {
            transaction.Rollback();
            throw new UserFriendlyException(e.Message);
        }
           
    }
}
        }
        
        public async Task NotifyExpiring()
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var items = await _repository.GetAllIncluding(itm => itm.User).Where(x => x.EndDate <= DateTime.Now.AddDays(7) && x.StatusId == status.Id).ToListAsync();
           
            if (items != null && items.Count > 0)
            {
                items.ForEach(item =>
                {
                    BackgroundJob.Enqueue<BgJobHomePageBanner>(x => x.NotifyUserOfExpiringHomePageBanner(item.User, item));
                });
            }
        }

        public async Task DisableExpired()
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var items = await _repository.GetAllIncluding(itm => itm.User).Where(x => x.EndDate <= DateTime.Now && x.StatusId == status.Id).ToListAsync();
            if (items != null && items.Count > 0)
            {
                var closedStatus = await _statusAppService.GetByNameAsync(AppConsts.StatusExpired);
                items.ForEach(item =>
                {
                    item.StatusId = closedStatus.Id;
                    BackgroundJob.Enqueue<BgJobHomePageBanner>(x => x.NotifyUserOfExpiredHomePageBanner(item.User, item));

                });
                await CurrentUnitOfWork.SaveChangesAsync();
            }
        }
    }
}