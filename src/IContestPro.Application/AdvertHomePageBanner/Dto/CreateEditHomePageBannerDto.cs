﻿using System;
using Abp.AutoMapper;
using Microsoft.AspNetCore.Http;

namespace IContestPro.AdvertHomePageBanner.Dto
{
    [AutoMapTo(typeof(CreateAdvertHomePageBannerDto))]
    [AutoMapFrom(typeof(AdvertHomePageBannerDto))]
    public class CreateEditHomePageBannerDto
    {
        
        public int? Id { get; set; }
        public int? TenantId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public IFormFile Image { get; set; }
        public int? StatusId { get; set; }
        public long? UserId { get; set; }
        public int? AdvertPriceId { get; set; }
        public string ExternalUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

    }
}