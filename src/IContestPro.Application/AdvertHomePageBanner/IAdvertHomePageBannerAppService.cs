﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.AdvertHomePageBanner.Dto;
using IContestPro.Contest.Dto;

namespace IContestPro.AdvertHomePageBanner
{
    public interface IAdvertHomePageBannerAppService : IBaseAppService<AdvertHomePageBannerDto, int, PagedResultRequestDto, CreateAdvertHomePageBannerDto, AdvertHomePageBannerDto>
    {
        Task<IEnumerable<AdvertHomePageBannerDto>> GetRandomLive(int totalTake = 3);
        Task<PagedResultDto<AdvertHomePageBannerDto>> GetAllByUser(long userId, PagedResultRequestDto input);
        Task<PagedResultDto<AdvertHomePageBannerDto>> GetAllByStatus(string statusName, PagedResultRequestDto input);
        Task<AdvertHomePageBannerDto> CreateAndUploadImage(CreateEditHomePageBannerDto input);
        Task<AdvertHomePageBannerDto> UpdateAndUploadImage(CreateEditHomePageBannerDto input);
        Task<AdvertHomePageBannerDto> UpdateStatusAsync(UpdateStatusDto model);

        Task NotifyExpiring();
        Task DisableExpired();
    }
}