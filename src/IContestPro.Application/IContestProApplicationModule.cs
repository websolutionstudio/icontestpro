﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using IContestPro.Authorization;

namespace IContestPro
{
    [DependsOn(
        typeof(IContestProCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class IContestProApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<IContestProAuthorizationProvider>();
            Configuration.Settings.Providers.Add<AppSettingProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(IContestProApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
