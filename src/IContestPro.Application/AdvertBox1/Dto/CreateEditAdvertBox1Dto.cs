﻿using System;
using Abp.AutoMapper;
using Microsoft.AspNetCore.Http;

namespace IContestPro.AdvertBox1.Dto
{
    [AutoMapTo(typeof(CreateAdvertBox1Dto))]
    [AutoMapFrom(typeof(AdvertBox1Dto))]
    public class CreateEditAdvertBox1Dto
    {
        
        public int? Id { get; set; }
        public int? TenantId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public IFormFile Image { get; set; }
        public int? StatusId { get; set; }
        public long? UserId { get; set; }
        public int? AdvertPriceId { get; set; }
        public string ExternalUrl { get; set; }

    }
}