﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.AdvertBox1.Dto;
using IContestPro.Contest.Dto;

namespace IContestPro.AdvertBox1
{
    /// <summary>
    /// This is the advert at the top right side of the pages.
    /// </summary>
    public interface IAdvertBox1AppService : IBaseAppService<AdvertBox1Dto, int, PagedResultRequestDto, CreateAdvertBox1Dto, AdvertBox1Dto>
    {
        Task<AdvertBox1Dto> GetRandomLive(int totalTake = 1);
        Task<PagedResultDto<AdvertBox1Dto>> GetAllByUser(long userId, PagedResultRequestDto input);
        Task<PagedResultDto<AdvertBox1Dto>> GetAllByStatus(string statusName, PagedResultRequestDto input);
        Task<AdvertBox1Dto> CreateAndUploadImage(CreateEditAdvertBox1Dto input);
        Task<AdvertBox1Dto> UpdateAndUploadImage(CreateEditAdvertBox1Dto input);
        Task<AdvertBox1Dto> UpdateStatusAsync(UpdateStatusDto model);
        Task NotifyExpiring();
        Task DisableExpired();
    }
}