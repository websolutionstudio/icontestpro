﻿namespace IContestPro.VoterLeaderBoard.Dto
{
   public class TopUsersWithHighestVotesDto
    {
        public long Id { get; set; }
        public string NickName { get; set; }
        public string Image { get; set; }
        public string Permalink { get; set; }
        public int TotalVotes { get; set; }
        
    }
}