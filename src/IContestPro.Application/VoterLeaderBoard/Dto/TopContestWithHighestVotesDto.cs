﻿using System;

namespace IContestPro.VoterLeaderBoard.Dto
{
    
   public class TopContestWithHighestVotesDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Picture { get; set; }
        public string Permalink { get; set; }
        public int TotalVotes { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}