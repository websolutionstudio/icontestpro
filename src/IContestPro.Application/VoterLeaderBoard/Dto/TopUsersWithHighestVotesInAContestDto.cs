﻿namespace IContestPro.VoterLeaderBoard.Dto
{
    
     public class TopUsersWithHighestVotesInAContestDto
    {
        public long Id { get; set; }
        public string NickName { get; set; }
        public string Image { get; set; }
        public string Permalink { get; set; }
        public int TotalVotes { get; set; }
        public string ContestTitle { get; set; }
        public string ContestPermalink { get; set; }
        public string ContestId { get; set; }
    }
}