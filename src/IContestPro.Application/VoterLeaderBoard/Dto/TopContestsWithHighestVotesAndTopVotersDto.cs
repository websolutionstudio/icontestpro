﻿using System.Collections.Generic;

namespace IContestPro.VoterLeaderBoard.Dto
{
    
   public class TopContestsWithHighestVotesAndTopVotersDto
    {
        public TopContestWithHighestVotesDto Contest { get; set; }
        public List<TopUsersWithHighestVotesInAContestDto> UserVotes { get; set; }
        
    }
}