﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.EntityFrameworkCore;
using IContestPro.EntityFrameworkCore.StoreProcedureManager;
using IContestPro.VoterLeaderBoard.Dto;

namespace IContestPro.VoterLeaderBoard
{
    public class VoterVoterLeaderBoardAppService :  IVoterLeaderBoardAppService
    {
        private readonly IContestProDbContextFactory _contestProDbContextFactory;

        public VoterVoterLeaderBoardAppService(IContestProDbContextFactory contestProDbContextFactory)
        {
            _contestProDbContextFactory = contestProDbContextFactory;
        }

        public async Task<PagedResultDto<TopContestWithHighestVotesDto>> GetTopContestsWithHighestVotes(PagedResultRequestDto input)
        {
           List<TopContestWithHighestVotesDto> votes;
          
                using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
                {
                    var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetTopContestsWithHighestVotes)
                        .WithSqlParam("PageSize", input.MaxResultCount);
                     votes = await cmd.ExecuteStoredProcedureListAsync<TopContestWithHighestVotesDto>();
                }
               
            return new PagedResultDto<TopContestWithHighestVotesDto>(
               items: votes,
               totalCount: votes.Count
            );
        } 
        public async Task<PagedResultDto<TopContestsWithHighestVotesAndTopVotersDto>> GetTopContestsWithHighestVotesAndTopVoters(PagedResultRequestDto input)
        {
            var contests = await GetTopContestsWithHighestVotes(input);
            var items = new List<TopContestsWithHighestVotesAndTopVotersDto>();
            foreach (var contest in contests.Items)
            {
                var votes = await GetTopUsersWithHighestVotesInAContest(contest.Id, input.MaxResultCount);
                items.Add(new TopContestsWithHighestVotesAndTopVotersDto
                {
                    Contest = contest,
                    UserVotes = votes.ToList()
                });
            }
                
               
            return new PagedResultDto<TopContestsWithHighestVotesAndTopVotersDto>(
               items: items,
               totalCount: items.Count
            );
        }

        public async  Task<IReadOnlyList<TopUsersWithHighestVotesDto>> GetTopUsersWithHighestVotes(PagedResultRequestDto input)
        {
            List<TopUsersWithHighestVotesDto> votes;
          
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetTopUsersWithHighestVotes)
                    .WithSqlParam("PageSize", input.MaxResultCount);
                votes = await cmd.ExecuteStoredProcedureListAsync<TopUsersWithHighestVotesDto>();
            }
               
            return votes;
           
        }
        public async Task<IReadOnlyList<TopUsersWithHighestVotesInAContestDto>> GetTopUsersWithHighestVotesInAContest(int contestId, int pageSize = 10)
        {
            List<TopUsersWithHighestVotesInAContestDto> votes;
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetTopUsersWithHighestVotesInAContest)
                    .WithSqlParam("ContestId", contestId)
                    .WithSqlParam("PageSize", pageSize);
                votes = await cmd.ExecuteStoredProcedureListAsync<TopUsersWithHighestVotesInAContestDto>();
            }
               
            return votes;
           
        }
    }
}