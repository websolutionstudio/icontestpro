﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using IContestPro.VoterLeaderBoard.Dto;

namespace IContestPro.VoterLeaderBoard
{
    public interface IVoterLeaderBoardAppService : IApplicationService
    {
        Task<PagedResultDto<TopContestWithHighestVotesDto>> GetTopContestsWithHighestVotes(PagedResultRequestDto input);
        Task<IReadOnlyList<TopUsersWithHighestVotesDto>> GetTopUsersWithHighestVotes(PagedResultRequestDto input);
        Task<IReadOnlyList<TopUsersWithHighestVotesInAContestDto>> GetTopUsersWithHighestVotesInAContest(int contestId, int pageSize = 10);

        Task<PagedResultDto<TopContestsWithHighestVotesAndTopVotersDto>> GetTopContestsWithHighestVotesAndTopVoters(
            PagedResultRequestDto input);
    }
}