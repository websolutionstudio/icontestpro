﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Dependency;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Runtime.Security;
using Abp.Runtime.Session;
using IContestPro.Cookie;
using IContestPro.Logging;
using IContestPro.Notifications;
using IContestPro.Settings;
using IContestPro.Users.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace IContestPro.Base
{
    public class BaseAppService<TEntity, TEntityDto, TPrimaryKey, TGetAllInput, TCreateInput, TUpdateInput>
    : AsyncCrudAppService<TEntity, TEntityDto, TPrimaryKey, TGetAllInput, TCreateInput, TUpdateInput, EntityDto<TPrimaryKey>>,
        IBaseAppService<TEntityDto, TPrimaryKey, TGetAllInput, TCreateInput, TUpdateInput>
    where TEntity : class, IEntity<TPrimaryKey>
    where TEntityDto : IEntityDto<TPrimaryKey>
    where TPrimaryKey : struct
    where TUpdateInput : IEntityDto<TPrimaryKey>
    {
   private readonly IRepository<TEntity, TPrimaryKey> _repository;
   protected readonly IIocResolver  IocResolver;
   protected IAppNotifier AppNotifier { get; set; }
   protected ICookieAppService CookieAppService { get; set; }
   protected ILoggingAppService LoggingAppService { get; set; }
   protected readonly string ConnectionString;
   protected new readonly ISettingsAppService SettingManager;

    protected BaseAppService(IRepository<TEntity, TPrimaryKey> repository, IIocResolver iocResolver)
        : base(repository)
    {
        _repository = repository;
        IocResolver = iocResolver;
        LocalizationSourceName = IContestProConsts.LocalizationSourceName;
        AppNotifier = iocResolver.ResolveAsDisposable<IAppNotifier>().Object;
        CookieAppService = iocResolver.ResolveAsDisposable<ICookieAppService>().Object;
        LoggingAppService = iocResolver.ResolveAsDisposable<ILoggingAppService>().Object;
        SettingManager = iocResolver.ResolveAsDisposable<ISettingsAppService>().Object;
        var options = iocResolver.ResolveAsDisposable<IOptions<ConnectionStrings>>().Object;
        var connectionStringConfig = options.Value;
        ConnectionString = connectionStringConfig.Default;
        

    }

        protected UserDto CurrentUser
        {
            get
            {
                //Todo: Initialize te current user
                var cookieUserString = CookieAppService.Get(AppConsts.CookieKeyCurrentUser);
                if (cookieUserString.Value != null)
                {
                    var decrypted = Decrypt(cookieUserString.Value);
                    return DeserializeObject<UserDto>(decrypted);
                }
                return null;
            }
        }

       
        
        public string Encrypt(string item)
        {
            return new SimpleStringCipher().Encrypt(item);
        }
        public string Decrypt(string item)
        {
            return new SimpleStringCipher().Decrypt(item);
        }


        public async Task<int> CountAsync()
        {
          return  await _repository.CountAsync();
        }

        public async Task<int> CountAsync<T>(IQueryable<T> queryable)
        {
            return await queryable.CountAsync();
        }
        public string GetUserAppIdentity(long userId)
        {
            return userId.ToString().PadLeft(AppConsts.UserIdentityLength, '0');
        }

        public string GetCurrentUserAppIdentity()
        {
            return AbpSession.UserId == null ? null : AbpSession.GetUserId().ToString().PadLeft(AppConsts.UserIdentityLength, '0');
        } 

            public string SerializeObject<T>(T value) where T : class
            {
             return JsonConvert.SerializeObject(value);
            }

            public T DeserializeObject<T>(string value) where T : class
            {
                return value == null ? null : JsonConvert.DeserializeObject<T>(value);
            }

        protected MySqlConnection GetMySqlConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
        
       
    }
}