﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;

namespace IContestPro.Base
{
    public interface IBaseAppService<TEntityDto, TPrimaryKey, in TGetAllInput, in TCreateInput, in TUpdateInput> : 
        IAsyncCrudAppService<TEntityDto, TPrimaryKey, TGetAllInput, TCreateInput, TUpdateInput, EntityDto<TPrimaryKey>>
        where TEntityDto : IEntityDto<TPrimaryKey> where TUpdateInput : IEntityDto<TPrimaryKey>
    {
        Task<int> CountAsync();
        Task<int> CountAsync<T>(IQueryable<T> queryable);
        string GetUserAppIdentity(long userId);
        string GetCurrentUserAppIdentity();
        string SerializeObject<T>(T value) where T : class;
        T DeserializeObject<T>(string value) where T : class;
        string Encrypt(string item);
        string Decrypt(string item);
      

    }
    
}