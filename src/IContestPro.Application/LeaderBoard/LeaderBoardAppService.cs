﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using IContestPro.EntityFrameworkCore;
using IContestPro.EntityFrameworkCore.StoreProcedureManager;
using IContestPro.LeaderBoard.Dto;
using IContestPro.Status;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace IContestPro.LeaderBoard
{
    public class LeaderBoardAppService :  ILeaderBoardAppService
    {
        private readonly IRepository<IContestProEntities.Contest> _contestRepository;
        private readonly IContestProDbContextFactory _contestProDbContextFactory;
        private readonly IStatusAppService _statusAppService;
        private readonly string _connectionString;

        public LeaderBoardAppService(IRepository<IContestProEntities.Contest> contestRepository,
            IContestProDbContextFactory contestProDbContextFactory,
            IStatusAppService statusAppService, IOptions<ConnectionStrings> connectionStringConfig)
        {
            _contestRepository = contestRepository;
            _contestProDbContextFactory = contestProDbContextFactory;
            _statusAppService = statusAppService;
            _connectionString = connectionStringConfig.Value.Default;
        }

        public async Task<PagedResultDto<LeaderBoardContestDto>> GetTopContestEntriesWithHighestVote(string displayType,
            PagedResultRequestDto input, int pageSize = 10)
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var query = _contestRepository.GetAll()
                .Select(item => new {item.Id, item.Title, item.StartDate, item.EndDate, 
                    item.Picture, item.CreationTime, item.Permalink, item.StatusId})
                .Where(item => item.StatusId == status.Id);
            
            //Todo: Filter result by the display type: Lifetime, Monthly and Weekly
            //Todo: Default id LifeTime as no filtering is applied
            if (displayType == AppConsts.LeaderBoardDisplayTypeMonthly)
            {
                query = query.Where(item => item.StartDate >= DateTime.Now.AddDays(-30) && item.StartDate <= DateTime.Now);
            }else if (displayType == AppConsts.LeaderBoardDisplayTypeWeekly)
            {
                query = query.Where(item => item.StartDate >= DateTime.Now.AddDays(-7) && item.StartDate <= DateTime.Now);
            }
            var totalCount = await query.CountAsync();
            query = query.OrderByDescending(item => item.CreationTime);
            query = query.Skip(input.SkipCount).Take(input.MaxResultCount);
            var entities = await query.ToListAsync();
            var items = new List<LeaderBoardContestDto>();
            foreach (var entity in entities)
            {
                var item = new LeaderBoardContestDto
                {
                     Id = entity.Id,
                     EndDate = entity.EndDate,
                     StartDate = entity.StartDate,
                     Picture = entity.Picture,
                     Title = entity.Title,
                     Permalink = entity.Permalink
                };
                using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
                {
                    var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetTopContestEntriesWithHighestVote)
                        .WithSqlParam("ContestId", entity.Id)
                        .WithSqlParam("PageSize", pageSize);
                    var votes = await cmd.ExecuteStoredProcedureListAsync<LeaderBoardUserVotesDto>();
                    item.Votes = votes;
                }
               
                items.Add(item);
            }
            return new PagedResultDto<LeaderBoardContestDto>(
                totalCount,
                items
            );
           
        }

        public async Task<LeaderBoardContestDto> GetTopContestEntriesWithHighestVoteByContestPermalink(string contestPermalink, int totalEntryTake = 50)
        {

            
            
            var entity = _contestRepository.GetAll()
                .Select(item => new {item.Id, item.Title, item.StartDate, item.EndDate, 
                    item.Picture, item.CreationTime, item.Permalink, item.StatusId})
                .FirstOrDefault(item => item.Permalink == contestPermalink);
            if (entity == null)
            {
                return null;
            }
            var itemResult = new LeaderBoardContestDto
            {
                Id = entity.Id,
                EndDate = entity.EndDate,
                StartDate = entity.StartDate,
                Picture = entity.Picture,
                Title = entity.Title,
                Permalink = entity.Permalink
            };
            
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
             
                
                //itemsd.First().
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetTopContestEntriesWithHighestVote)
                        .WithSqlParam("ContestId", entity.Id)
                        .WithSqlParam("PageSize", totalEntryTake)
                    ;
                var votes = await cmd.ExecuteStoredProcedureListAsync<LeaderBoardUserVotesDto>();
                itemResult.Votes = votes;
            }
            return itemResult;
           
        }
    }
}