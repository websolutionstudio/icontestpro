﻿using System;
using System.Collections.Generic;

namespace IContestPro.LeaderBoard.Dto
{
    
    /// <summary>
    /// The class caters for the image upload
    /// File is the file to be uploaded
    /// Path is the relative path from the web root e.g image/uploads
    /// </summary>
   public class LeaderBoardContestDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Picture { get; set; }
        public string Permalink { get; set; }
        public IEnumerable<LeaderBoardUserVotesDto> Votes { get; set; }
    }
}