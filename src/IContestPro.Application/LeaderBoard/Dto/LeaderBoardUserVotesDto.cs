﻿namespace IContestPro.LeaderBoard.Dto
{
    
    /// <summary>
    /// The class caters for the image upload
    /// File is the file to be uploaded
    /// Path is the relative path from the web root e.g image/uploads
    /// </summary>
   public class LeaderBoardUserVotesDto
    {
        public string NickName { get; set; }
        public string Image { get; set; }
        public string Permalink { get; set; }
        public int TotalVotes { get; set; }
        public int ContestId { get; set; }
        public int ContestEntryId { get; set; }
        
        
    }
}