﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using IContestPro.LeaderBoard.Dto;

namespace IContestPro.LeaderBoard
{
    public interface ILeaderBoardAppService : IApplicationService
    {
        Task<PagedResultDto<LeaderBoardContestDto>> GetTopContestEntriesWithHighestVote(string displayType, PagedResultRequestDto input,
            int pageSize = 10);
        Task<LeaderBoardContestDto> GetTopContestEntriesWithHighestVoteByContestPermalink(string contestPermalink, int totalEntryTake = 50);
    }
}