using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.AssetUpload.Dto;
using IContestPro.Base;
using IContestPro.Roles.Dto;
using IContestPro.Users.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace IContestPro.Users
{
    public interface IUserAppService : IBaseAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
        Task<IdentityResult> AddToSponsorRoleAsync(long userId);
        Task<bool> UpdateProfilePictureNameAsync(string fileName);
        Task<ImageUploadDto> UploadProfilePictureAsync(IFormFile profilepic);
        Task<UserDto> GetByPermalink(string permalink);
        Task<UserDto> GetTenantAdmin();
        Task<UserDetailsDto> GetDetailsByPermalink(string permalink);
        Task<IdentityResult> ChangePasswordAsync(ChangePasswordDto model);
        Task<UserDto> GetCurrentUser();
    }
}
    