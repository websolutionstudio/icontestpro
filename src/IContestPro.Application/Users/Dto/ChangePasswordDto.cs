

using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Users;

namespace IContestPro.Users.Dto
{
    public class ChangePasswordDto
    {
        
        [Required]
        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
        public string CurrentPassword { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
            public string NewPassword { get; set; }

    }
}
