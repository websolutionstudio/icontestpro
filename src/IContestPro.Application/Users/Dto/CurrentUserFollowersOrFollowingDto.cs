using Abp.Application.Services.Dto;

namespace IContestPro.Users.Dto
{
    public class CurrentUserFollowersOrFollowingDto : EntityDto<long>
    {
        public string NickName { get; set; }
        public string Permalink { get; set; }
        public string Image { get; set; }
    }
}
