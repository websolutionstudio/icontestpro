using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using IContestPro.Authorization.Users;
using IContestPro.CountryState.Dto;
using IContestPro.Sponsor.Dto;
using StateDto = IContestPro.CountryState.Dto.StateDto;

namespace IContestPro.Users.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserDto : EntityDto<long>
    {
        
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }
        
        public string CountryId { get; set; }

        public int? StateId { get; set; }
        [StringLength(255)]
        public string Profession { get; set; }
        [StringLength(50)]
        public string NickName { get; set; }
        [StringLength(255)]
        public string Permalink { get; set; }
 
        public string PhoneNumber { get; set; }
        public bool IsPhoneNumberConfirmed { get; set; }
        public string Image { get; set; }
        public string FaceBook { get; set; }
        public string LinkedIn { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }

        public CountryDto Country { get; set; }
        public StateDto State { get; set; }
        

        public bool IsActive { get; set; }

        public string FullName { get; set; }

        public DateTime? LastLoginTime { get; set; }

        public DateTime CreationTime { get; set; }

        public string[] RoleNames { get; set; }
        
        /*Company info*/
        public SponsorDto Sponsor { get; set; }

    }
}
