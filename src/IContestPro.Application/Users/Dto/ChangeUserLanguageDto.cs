using System.ComponentModel.DataAnnotations;

namespace IContestPro.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}