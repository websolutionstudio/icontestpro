﻿namespace IContestPro.Users.Dto
{
    public class UserDetailsDto
    {
        public UserDto User { get; set; }
        public int TotalFollowing { get; set; }
        public int TotalFollowers { get; set; }
        public int TotalEntries { get; set; }
        public int TotalVotes { get; set; }
       
    }
}