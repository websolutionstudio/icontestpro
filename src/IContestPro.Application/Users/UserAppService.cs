using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Localization;
using Abp.Runtime.Session;
using IContestPro.AssetUpload;
using IContestPro.AssetUpload.Dto;
using IContestPro.Authorization;
using IContestPro.Authorization.Roles;
using IContestPro.Authorization.Users;
using IContestPro.Base;
using IContestPro.Contest.ContestEntry;
using IContestPro.Contest.ContestVote;
using IContestPro.Cookie;
using IContestPro.CountryState;
using IContestPro.Follow;
using IContestPro.Roles.Dto;
using IContestPro.Sessions.Dto;
using IContestPro.Users.Dto;
using Microsoft.AspNetCore.Http;

namespace IContestPro.Users
{
    public class UserAppService : BaseAppService<User, UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>, IUserAppService
    {
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IRepository<Role> _roleRepository;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IFollowAppService _followAppService;
        private readonly IContestEntryAppService _contestEntryAppService;
        private readonly IContestEntryVoteAppService _contestEntryVoteAppService;
        private readonly IAssetUploadAppService _assetUploadAppService;
        private readonly  IRepository<User, long> _repository;
        private readonly ICountryStateAppService _countryStateAppService;
        private readonly ICookieAppService _cookieAppService;
        private readonly ISettingManager _settingManager;

        public UserAppService(
            IRepository<User, long> repository,
            UserManager userManager,
            RoleManager roleManager,
            IRepository<Role> roleRepository,
            IPasswordHasher<User> passwordHasher, IFollowAppService followAppService, 
            IContestEntryAppService contestEntryAppService,
            IContestEntryVoteAppService contestEntryVoteAppService, IAssetUploadAppService assetUploadAppService,
            ICountryStateAppService countryStateAppService, ICookieAppService cookieAppService,
             IIocResolver  iocResolver, ISettingManager settingManager) : base(repository, iocResolver)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _roleRepository = roleRepository;
            _passwordHasher = passwordHasher;
            _followAppService = followAppService;
            _contestEntryAppService = contestEntryAppService;
            _contestEntryVoteAppService = contestEntryVoteAppService;
            _assetUploadAppService = assetUploadAppService;
            _countryStateAppService = countryStateAppService;
            _cookieAppService = cookieAppService;
            _settingManager = settingManager;
            _repository = repository;
        }

        public override async Task<UserDto> Get(EntityDto<long> input)
        {
            var user = await _repository.GetAll().FirstOrDefaultAsync(usr => usr.Id == input.Id);
            return ObjectMapper.Map<UserDto>(user);
        }
        public async Task<UserDto> GetCurrentUser()
        {
            var cookieUserString = _cookieAppService.Get(AppConsts.CookieKeyCurrentUser);
            if (cookieUserString.Value != null)
            {
                var decrypted = Decrypt(cookieUserString.Value);
                return DeserializeObject<UserDto>(decrypted);
            }
            var user = await _repository.GetAsync(AbpSession.GetUserId());
            return user?.MapTo<UserDto>();
        }

        

        public async Task<GetCurrentLoginInformationsOutput> GetUserProfileDetailsAsyc(EntityDto<long> input)
        {
            var output = new GetCurrentLoginInformationsOutput();
           
                output.User = ObjectMapper.Map<UserLoginInfoDto>(await _userManager.FindByIdAsync(input.Id.ToString()));
                if (output.User.CountryId != null)
                {
                    output.User.CountryName = (await _countryStateAppService.Get(new EntityDto<string>{Id = output.User.CountryId}))?.Name;
                }
                if (output.User.StateId != null)
                {
                    output.User.StateName = (await _countryStateAppService.GetStateByIdAsync(output.User.StateId.Value))?.Name;
                }
               
                 output.Followers =  await _followAppService.CountFollowersByUserId(input.Id);
                 output.Following = await _followAppService.CountFollowingByUserId(input.Id);
            

            return output;
        }
        
        public override async Task<UserDto> Create(CreateUserDto input)
        {
            var user = ObjectMapper.Map<User>(input);

            user.TenantId = AbpSession.TenantId;
            user.IsEmailConfirmed = true;

            await _userManager.InitializeOptionsAsync(AbpSession.TenantId);

            CheckErrors(await _userManager.CreateAsync(user, input.Password));

            if (input.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRoles(user, input.RoleNames));
            }

            CurrentUnitOfWork.SaveChanges();

            return MapToEntityDto(user);
        }

        [AbpAuthorize]
        public override async Task<UserDto> Update(UserDto input)
        {
            var user = await _userManager.GetUserByIdAsync(input.Id);

            user = MapAndReturnEntity(input, user);

            CheckErrors(await _userManager.UpdateAsync(user));

            if (input.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRoles(user, input.RoleNames));
            }
            await UnitOfWorkManager.Current.SaveChangesAsync();
            return await Get(input);
        }
        [AbpAuthorize]
        public async Task<IdentityResult> ChangePasswordAsync(ChangePasswordDto model)
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.GetUserId());
            var result = await _userManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);
            if (!result.Succeeded)
            {
                foreach (var item in result.Errors)
                {
                    if (item.Description.Contains("Incorrect password"))
                    {
                        item.Description = "Current password does not match your password.";
                    }
                }
            }
             return result;
          
        }
     
        
        
        [AbpAuthorize(PermissionNames.Pages_Users)]
        public override async Task Delete(EntityDto<long> input)
        {
            var user = await _userManager.GetUserByIdAsync(input.Id);
            await _userManager.DeleteAsync(user);
        }

        [AbpAuthorize(PermissionNames.Pages_Users)]
        public async Task<ListResultDto<RoleDto>> GetRoles()
        {
            var roles = await _roleRepository.GetAllListAsync();
            return new ListResultDto<RoleDto>(ObjectMapper.Map<List<RoleDto>>(roles));
        }

        [AbpAuthorize(PermissionNames.Pages_Users)]
        public async Task ChangeLanguage(ChangeUserLanguageDto input)
        {
            await _settingManager.ChangeSettingForUserAsync(
                AbpSession.ToUserIdentifier(),
                LocalizationSettingNames.DefaultLanguage,
                input.LanguageName
            );
        }

        public async Task<IdentityResult> AddToSponsorRoleAsync(long userId)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(usr => usr.Id == userId);
            await _userManager.RemoveFromRoleAsync(user, StaticRoleNames.Tenants.Users);
           var result = await _userManager.AddToRoleAsync(user, StaticRoleNames.Tenants.Sponsors);
            return result;
        }

        [AbpAuthorize]
        public async Task<bool> UpdateProfilePictureNameAsync(string fileName)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(usr => usr.Id == AbpSession.UserId);
            _assetUploadAppService.Delete(new DeleteAssetDto{Path = "images/uploads/" + user.Image});
            user.Image = fileName;
            UnitOfWorkManager.Current.SaveChanges();
            return true;
        }

        [AbpAuthorize]
        public async Task<ImageUploadDto> UploadProfilePictureAsync(IFormFile profilepic)
        {
            var uploaded = await _assetUploadAppService.UploadImage(new UploadDto{File = profilepic, Path = "images/uploads"}, 80);
            await UpdateProfilePictureNameAsync(uploaded.FileName);
            return uploaded;
        }

        public async Task<UserDto> GetByPermalink(string permalink)
        {
            var user = await _userManager.Users.Include(usr => usr.Country)
                .Include(usr => usr.State)
                .FirstOrDefaultAsync(usr =>usr.Permalink == permalink);
            return ObjectMapper.Map<UserDto>(user);
        }

        [AbpAuthorize]
        public async Task<UserDto> GetTenantAdmin()
        {
            var admin = await _userManager.Users.FirstOrDefaultAsync(user => user.EmailAddress == AppConsts.DefaultTenantAdminEmail);
            return admin.MapTo<UserDto>();
        }

        public async Task<UserDetailsDto> GetDetailsByPermalink(string permalink)
        {
            var user = await _userManager.Users.Include(usr => usr.Country)
                .Include(usr => usr.State)
                .FirstOrDefaultAsync(usr =>usr.Permalink == permalink);
            var userDto = user.MapTo<UserDto>();
            var totalFollowers = await _followAppService.CountFollowersByUserId(userDto.Id);
            var totalFollowing = await _followAppService.CountFollowingByUserId(userDto.Id);
             var totalEntries =  await _contestEntryAppService.CountByUserId(userDto.Id);
             var totalVotes = await _contestEntryVoteAppService.CountUserVotes(userDto.Id);
            var userDetails = new UserDetailsDto
            {
                User = userDto,
                TotalEntries = totalEntries,
                TotalVotes = totalVotes,
                TotalFollowers = totalFollowers,
                TotalFollowing = totalFollowing
                
            };
            return userDetails;
        }

        protected override User MapToEntity(CreateUserDto createInput)
        {
            var user = ObjectMapper.Map<User>(createInput);
            user.SetNormalizedNames();
            return user;
        }

        protected override void MapToEntity(UserDto input, User user)
        {
            ObjectMapper.Map(input, user);
            user.SetNormalizedNames();
        }
        private User MapAndReturnEntity(UserDto input, User user)
        {
            var item = ObjectMapper.Map(input, user);
            return item;
        }
        protected override UserDto MapToEntityDto(User user)
        {
            var roles = _roleManager.Roles.Where(r => user.Roles.Any(ur => ur.RoleId == r.Id)).Select(r => r.NormalizedName);
            var userDto = base.MapToEntityDto(user);
            userDto.RoleNames = roles.ToArray();
            return userDto;
        }
       

        protected override IQueryable<User> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.Roles);
        }

        protected override async Task<User> GetEntityByIdAsync(long id)
        {
            var user = await Repository.GetAllIncluding(x => x.Roles).FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new EntityNotFoundException(typeof(User), id);
            }

            return user;
        }

        protected override IQueryable<User> ApplySorting(IQueryable<User> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.UserName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
