﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.PaymentHistories.Dto;
using IContestPro.Wallet.Dto;

namespace IContestPro.Wallet
{
    public interface IWalletAppService : IBaseAppService<WalletDto, int, PagedResultRequestDto, CreateWalletDto, WalletDto>
    {
        Task<WalletDto> GetByUserId(long userId);
        Task<WalletDto> GetAdminWallet();
        Task<decimal> GetBalance(long userId);
        Task<WalletDto> Credit(long userId, decimal amount, bool logOperation = true);
        Task<WalletDto> Debit(long userId, decimal amount, bool logOperation = true);
        Task<PaymentHistoryDto> LogWalletOperation(WalletInfoDto input);
    }
}