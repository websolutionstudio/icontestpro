﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Users.Dto;

namespace IContestPro.Wallet.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Wallet))]
    public class WalletDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public long UserId { get; set; }
        public decimal Balance { get; set; }
        public decimal? LastCreditedAmount { get; set; }
        public decimal? LastDebitedAmount { get; set; }
        public int? TenantId { get; set; }

        public UserDto User { get; set; }
    }
}