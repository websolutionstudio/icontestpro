﻿using System.ComponentModel.DataAnnotations;

namespace IContestPro.Wallet.Dto
{
    public class WalletInfoDto
    {
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Title { get; set; } 
        [Required]
        public WalletInfoResult WalletInfoResult { get; set; }
        [Required]
        public WalletOperation WalletOperation { get; set; }
        [Required]
        public long UserId { get; set; }
        public string Reference { get; set; }

    }

    public enum WalletInfoResult
    {
        Success,
        Failed
    }
}