﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Wallet.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Wallet))]
    public class CreateCardPaymentDto
    {
        public decimal Amount { get; set; }
        [Required]
        public long UserId { get; set; }
        
    }
}