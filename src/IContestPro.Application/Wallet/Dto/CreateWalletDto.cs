﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Wallet.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Wallet))]
    public class CreateWalletDto
    {
        [Required]
        public long UserId { get; set; }
        public decimal Balance { get; set; }
        public decimal? LastCreditedAmount { get; set; }
        public decimal? LastDebitedAmount { get; set; }
        public int? TenantId { get; set; }


    }
}