﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Base;
using IContestPro.Helpers;
using IContestPro.PaymentHistories;
using IContestPro.PaymentHistories.Dto;
using IContestPro.Status;
using IContestPro.Users;
using IContestPro.Wallet.Dto;

namespace IContestPro.Wallet
{
    [AbpAuthorize]
     public class WalletAppService : BaseAppService<IContestProEntities.Wallet, WalletDto, int,
                                PagedResultRequestDto, CreateWalletDto, WalletDto>, IWalletAppService
    {
        private readonly IRepository<IContestProEntities.Wallet, int> _repository;
        private readonly IStatusAppService _statusAppService;
        private readonly IPaymentHistoriesAppService _paymentHistoriesAppService;
        private readonly IUserAppService _userAppService;
       
        public WalletAppService(IRepository<IContestProEntities.Wallet, int> repository, IStatusAppService statusAppService, 
            IPaymentHistoriesAppService paymentHistoriesAppService, IUserAppService userAppService, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
            _statusAppService = statusAppService;
            _paymentHistoriesAppService = paymentHistoriesAppService;
            _userAppService = userAppService;
        }
        
        public override async Task<WalletDto> Create(CreateWalletDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.Wallet>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        public override async Task<WalletDto> Update(WalletDto input)
        {
            var balance = await GetBalance(AbpSession.GetUserId());
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.Wallet>());
            CurrentUnitOfWork.SaveChanges();
             await LogWalletOperation(new WalletInfoDto
            {
                Amount = input.Balance, 
                Title = "Wallet Update occurred",
                Description =  $"Your wallet was updated. Balance before update NGN{balance.ToString("N")}. UPdated with the amount {input.Balance.ToNairaString()}",
                WalletInfoResult = WalletInfoResult.Success,
                WalletOperation = WalletOperation.Credit,
                UserId = AbpSession.GetUserId(),
            });
           await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                UserId = input.UserId,
                TenantId = AbpSession.TenantId,
                Message = $"Wallet Update occurred. Your wallet was updated. Balance before update NGN{balance.ToString("N")}. UPdated with the amount {input.Balance.ToNairaString()}",
            });
            return MapToEntityDto(item);
        }
        public async Task<WalletDto> GetByUserId(long userId)
        {
            var wallet = await _repository.FirstOrDefaultAsync(itm => itm.UserId == userId);
            return wallet != null ? wallet.MapTo<WalletDto>() : null;
        }

        public async Task<WalletDto> GetAdminWallet()
        {
            var admin = await _userAppService.GetTenantAdmin();
            var wallet = await _repository.FirstOrDefaultAsync(itm => itm.UserId == admin.Id);
            return wallet?.MapTo<WalletDto>();
        }

        public async Task<decimal> GetBalance(long userId)
        {
            var wallet = await _repository.FirstOrDefaultAsync(itm => itm.UserId == userId);
            return  wallet != null ? wallet.Balance : 0;
        }
        public async Task<WalletDto> Credit(long userId, decimal amount, bool logOperation = true)
        {
            var wallet = await _repository.FirstOrDefaultAsync(itm => itm.UserId == userId);
            wallet.Balance += amount;
            var item = await _repository.UpdateAsync(wallet);
            await UnitOfWorkManager.Current.SaveChangesAsync();
            if (logOperation)
            {
                await LogWalletOperation(new WalletInfoDto
                {
                    Amount = amount,
                    Title = "Credit occurred",
                    Description = "Credit occurred in your wallet.",
                    WalletInfoResult = WalletInfoResult.Success,
                    WalletOperation = WalletOperation.Credit,
                    UserId = userId
                });
               await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = userId,
                    TenantId = AbpSession.TenantId,
                    Message = $"A Credit  of NGN{amount.ToString("N")} occurred in your wallet.",
                });
            }

            return MapToEntityDto(item);
        }
        public async Task<WalletDto> Debit(long userId, decimal amount, bool logOperation = true)
        {
            var wallet = await _repository.FirstOrDefaultAsync(itm => itm.UserId == userId);
            if (wallet.Balance < amount)
            {
                throw new UserFriendlyException("Insuficient balance.");
            }
            wallet.Balance -= amount;
            var item = await _repository.UpdateAsync(wallet);
            CurrentUnitOfWork.SaveChanges();

            if (logOperation)
            {
                await LogWalletOperation(new WalletInfoDto
                {
                    Amount = amount,
                    Title = "Debit occurred",
                    Description = "Debit occurred in your wallet.",
                    WalletInfoResult = WalletInfoResult.Success,
                    WalletOperation = WalletOperation.Debit,
                    UserId = userId,
                });
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = userId,
                    TenantId = AbpSession.TenantId,
                    Message = $"A debit  of NGN{amount.ToString("N")} occurred in your wallet.",
                });
            }

            return MapToEntityDto(item);
        }
       
        public async Task<PaymentHistoryDto> LogWalletOperation(WalletInfoDto input)
        {
            var status = await _statusAppService.GetByNameAsync
                (input.WalletInfoResult == WalletInfoResult.Success ? AppConsts.StatusSuccess : AppConsts.StatusFailed);

          var result =  await _paymentHistoriesAppService.Create(new CreatePaymentHistoryDto
            {
                Reference = input.Reference ?? Guid.NewGuid().ToString("N"),
                Amount = input.Amount,
                Description = input.Description,
                Title = input.Title,
                UserId = input.UserId,
                WalletOperation = input.WalletOperation,
                StatusId = status.Id
                
            });
            //This must be added to all 
            return result;
        } 
        
    }
}