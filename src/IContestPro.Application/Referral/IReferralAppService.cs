﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Referral.Dto;

namespace IContestPro.Referral
{
    public interface IReferralAppService : IBaseAppService<ReferralDto, int, PagedResultRequestDto, CreateReferralDto, ReferralDto>
    {
        Task<PagedResultDto<ReferralDto>> GetAllByUserId(long userId, PagedResultRequestDto input);
        Task<bool> isUserReferred(long userId);
        Task<ReferralDto> GetUserReferrer(long userId);
        Task<ReferralDto> CreditUserReferrerVotePointAward(long userId);
        Task<ReferralDto> CreditUserReferrerSponsorAmount(long userId, decimal contestAmount);
    }
}