﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Notifications;
using IContestPro.AppFees;
using IContestPro.Authorization.Users;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Base;
using IContestPro.Referral.Dto;
using IContestPro.PaymentHistories;
using IContestPro.PaymentHistories.Dto;
using IContestPro.Status;
using IContestPro.UserVotePoint;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.Referral
{
    public class ReferralAppService : BaseAppService<IContestProEntities.Referral, ReferralDto, int, PagedResultRequestDto, CreateReferralDto, ReferralDto>, IReferralAppService
    {
        private readonly IRepository<IContestProEntities.Referral> _repository;
        private readonly IAppFeesAppService _appFeesAppService;
        private readonly IUserVotePointAppService _userVotePointAppService;
        private readonly IRepository<IContestProEntities.Wallet> _walletRepository;
        private readonly UserManager _userManager;
        private readonly IPaymentHistoriesAppService _paymentHistoriesAppService;
        private readonly IStatusAppService _statusAppService;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        


        public ReferralAppService(IRepository<IContestProEntities.Referral> repository, 
            IAppFeesAppService appFeesAppService,
            IUserVotePointAppService userVotePointAppService,IRepository<IContestProEntities.Wallet> walletRepository, UserManager userManager,
            IPaymentHistoriesAppService paymentHistoriesAppService, IStatusAppService statusAppService, 
            INotificationSubscriptionManager notificationSubscriptionManager, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
            _appFeesAppService = appFeesAppService;
            _userVotePointAppService = userVotePointAppService;
            _walletRepository = walletRepository;
            _userManager = userManager;
            _paymentHistoriesAppService = paymentHistoriesAppService;
            _statusAppService = statusAppService;
            _notificationSubscriptionManager = notificationSubscriptionManager;
           
        }
        public override async Task<PagedResultDto<ReferralDto>> GetAll(PagedResultRequestDto input)
        {
            var query = _repository.GetAllIncluding(itm => itm.User, itm => itm.UnderUser);
            query = ApplyPaging(query, input);
            query = query.OrderByDescending(item => item.CreationTime);
                var totalCount = await AsyncQueryableExecuter.CountAsync(query);
                var entities = await AsyncQueryableExecuter.ToListAsync(query);
                return new PagedResultDto<ReferralDto>(
                    totalCount,
                    entities.Select(MapToEntityDto).ToList()
                );
        }
        public async Task<PagedResultDto<ReferralDto>> GetAllByUserId(long userId,  PagedResultRequestDto input)
        {
            
           var query = _repository.GetAllIncluding(itm => itm.User)
                .Where(itm => itm.UnderUserId == userId);
            query = ApplyPaging(query, input);
            query = query.OrderByDescending(item => item.CreationTime);
                var totalCount = await AsyncQueryableExecuter.CountAsync(query);
                var entities = await AsyncQueryableExecuter.ToListAsync(query);
                return new PagedResultDto<ReferralDto>(
                    totalCount,
                    entities.Select(MapToEntityDto).ToList()
                );
        }

        public async Task<bool> isUserReferred(long userId)
        {
            var count = await _repository.CountAsync(itm => itm.UserId == userId);
            return count > 0;
        }

        public async Task<ReferralDto> GetUserReferrer(long userId)
        {
            var query = await _repository.FirstOrDefaultAsync(itm => itm.UserId == userId);
            return query != null ? query.MapTo<ReferralDto>() : null;
        }
        public async Task<ReferralDto> CreditUserReferrerVotePointAward(long userId)
        {
            var query = await _repository.FirstOrDefaultAsync(itm => itm.UserId == userId && itm.ReferrerAwardedVotePoint == null);
            if (query == null)
            {
                return null;
            }
            var votePointAward = await _appFeesAppService.GetReferralVotePointAward();
            var votePoint = await _userVotePointAppService.UpdateByUserId(query.UnderUserId, votePointAward);
            if (votePoint != null && votePoint.Id > 0)
            {
                query.ReferrerAwardedVotePoint = votePointAward;
                query.ReferrerAwardedDate = DateTime.Now;
                await UnitOfWorkManager.Current.SaveChangesAsync();
            }
            else
            {
                return null;
            }
            return query.MapTo<ReferralDto>();
        }

        public async Task<ReferralDto> CreditUserReferrerSponsorAmount(long userId, decimal contestAmount)
        {
            var shouldPayReferrerSponsorFirstContestPercentageEnabled = await SettingManager.GetSettingValueAsync<bool>(AppConsts
                .SettingsShouldPercentageBePaidIfReferredSponsorCreatesFirstContest);
            if (!shouldPayReferrerSponsorFirstContestPercentageEnabled)
            {
                return null;
            }
            var referral = await _repository.FirstOrDefaultAsync(itm => itm.UserId == userId && itm.ReferrerSponsorAmount == null);
            if (referral == null)
            {
                return null;
            }
            var sponsorPercentage = await SettingManager.GetSettingValueAsync<double>(AppConsts
                .SettingsReferrerPercentageIfReferredUserBecomesSponsor);
          
            var amount = contestAmount * ((decimal)sponsorPercentage / 100);
            var wallet = await _walletRepository.FirstOrDefaultAsync(itm => itm.UserId == referral.UnderUserId);
            wallet.Balance += amount;
            var item = await _walletRepository.UpdateAsync(wallet);
            if (item != null && item.Id > 0)
            {
                referral.ReferrerSponsorAmount = amount;
                referral.ReferrerSponsorPercentage = sponsorPercentage;
                referral.ReferrerSponsorDate = DateTime.Now;
                var user = await _userManager.Users.FirstOrDefaultAsync(itm => itm.Id == userId);
                var reference = Guid.NewGuid().ToString("N");
                var status = await _statusAppService.GetByNameAsync(AppConsts.StatusSuccess);
                var description =
                    $"The user {user.NickName} you referred to the system created and paid for a contest. As a rule you are rewarded with {sponsorPercentage}% of the amount paid for this first contest.<br />Ref: {reference}";
                  await _paymentHistoriesAppService.Create(
                      
                    new CreatePaymentHistoryDto
                {
                    Reference = reference,
                    StatusId = status.Id,
                    Amount = amount, 
                    Title = "User referrer contest credit",
                    Description = description,
                    WalletOperation = WalletOperation.Credit,
                    UserId = referral.UnderUserId,
                
                });
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = referral.UnderUserId,
                    Message = description
                });
                await UnitOfWorkManager.Current.SaveChangesAsync();
            }
            else
            {
                return null;
            }
            return referral.MapTo<ReferralDto>();
        }
    }
}