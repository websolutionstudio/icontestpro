﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Users.Dto;

namespace IContestPro.Referral.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Referral))]
    public class ReferralDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public int? TenantId { get; set; }
        
        public long UserId { get; set; }
        public long UnderUserId { get; set; }
        public int? ReferrerAwardedVotePoint { get; set; }
        public DateTime? ReferrerAwardedDate { get; set; }
        public int? ReferrerSponsorPercentage { get; set; }
        public decimal? ReferrerSponsorAmount { get; set; }
        public DateTime? ReferrerSponsorDate { get; set; }
        
        public UserDto User { get; set; }
        public UserDto UnderUser { get; set; }
        
        
    }
}