﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Referral.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Referral))]
    public class CreateReferralDto
    {
       
        [Required]
        public long UserId { get; set; }
        [Required]
        public long UnderUserId { get; set; }
    }
}