﻿using Abp.AutoMapper;

namespace IContestPro.ExportAppService.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.PaymentHistory))]

    public class ExportTransactionHistoryDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }
        public string FullName { get; set; }
        public string Status { get; set; }
        public decimal? Amount { get; set; }
        public string CreationTime { get; set; }
    }
}