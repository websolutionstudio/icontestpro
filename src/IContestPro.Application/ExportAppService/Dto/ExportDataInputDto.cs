﻿using System;

namespace IContestPro.ExportAppService.Dto
{
    public class ExportDataInputDto
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}