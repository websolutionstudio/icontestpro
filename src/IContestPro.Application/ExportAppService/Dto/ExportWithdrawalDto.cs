﻿using Abp.AutoMapper;

namespace IContestPro.ExportAppService.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Withdraw))]

    public class ExportWithdrawalDto
    {
        public string FullName { get; set; }
        public decimal Amount { get; set; }
        public string PayIn { get; set; }
        public string PayTo { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string BankName { get; set; }
        public string Status { get; set; }
        public int FeePercentage { get; set; }
        public decimal FeeAmount { get; set; }
        public string CreationTime { get; set; }
    }
}