﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Abp.Application.Services;
using IContestPro.ExportAppService.Dto;

namespace IContestPro.ExportAppService
{
    public interface IExportAppService : IApplicationService
    {
        DataTable ConvertToDatatable<T>(IEnumerable<T> data);
        Task<IEnumerable<ExportTransactionHistoryDto>> ExportTranscations(ExportDataInputDto input);
        Task<IEnumerable<ExportWithdrawalDto>> ExportWithdrawals(ExportDataInputDto input);

    }
}