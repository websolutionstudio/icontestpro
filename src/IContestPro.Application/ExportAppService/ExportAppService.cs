﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using IContestPro.Authorization;
using IContestPro.Authorization.Users;
using IContestPro.ExportAppService.Dto;
using IContestPro.IContestProEntities;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.ExportAppService
{
    public class ExportAppService : IContestProAppServiceBase, IExportAppService
    {
        private readonly IRepository<PaymentHistory> _paymentHistoryRepository;
        private readonly IRepository<IContestProEntities.Withdraw> _withdrawalRepository;
        private readonly IRepository<IContestProEntities.BankDetails> _bankDetailsRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<IContestProEntities.Status> _statusRepository;
        private readonly IRepository<IContestProEntities.Bank> _bankRepository;
        public ExportAppService(IRepository<PaymentHistory> paymentHistoryRepository,
            IRepository<User, long> userRepository, IRepository<IContestProEntities.Status> statusRepository, 
            IRepository<IContestProEntities.Withdraw> withdrawalRepository, 
            IRepository<IContestProEntities.BankDetails> bankDetailsRepository, IRepository<IContestProEntities.Bank> bankRepository)
        {
            _paymentHistoryRepository = paymentHistoryRepository;
            _userRepository = userRepository;
            _statusRepository = statusRepository;
            _withdrawalRepository = withdrawalRepository;
            _bankDetailsRepository = bankDetailsRepository;
            _bankRepository = bankRepository;
        }
        [AbpAuthorize]
        public async Task<IEnumerable<ExportTransactionHistoryDto>> ExportTranscations(ExportDataInputDto input)
        {
            var isAdmin = await PermissionChecker.IsGrantedAsync(PermissionNames.Pages_Admin);
            var query = _paymentHistoryRepository.GetAll();
            if (!isAdmin)
            {
                query = query.Where(itm => itm.UserId == AbpSession.GetUserId());
            }
            if (input.StartDate != null)
            {
                query =  query.Where(itm => itm.CreationTime >= input.StartDate);
            }
            if (input.EndDate != null)
            {
                query =  query.Where(itm => itm.CreationTime <= input.EndDate);
            }
            var entities = await query.ToListAsync();
            var items = new List<ExportTransactionHistoryDto>();
            foreach (var entity in entities)
            {
                var user = await _userRepository.GetAll().Select(itm => new {itm.FullName, itm.Id})
                    .FirstOrDefaultAsync(itm => itm.Id == entity.UserId);
                var status = await _statusRepository.GetAll().Select(itm => new {itm.Name, itm.Id})
                    .FirstOrDefaultAsync(itm => itm.Id == entity.StatusId);
                var item = new ExportTransactionHistoryDto
                {
                    Title = entity.Title,
                    Description = entity.Description,
                    Reference = entity.Reference,
                    FullName = user.FullName,
                    Status = status.Name,
                    Amount = entity.Amount,
                    CreationTime = entity.CreationTime.ToString("MM/dd/yyyy h:mm:ss tt")
                };
                items.Add(item);
            }
            return items;
        }
        
        public async Task<IEnumerable<ExportWithdrawalDto>> ExportWithdrawals(ExportDataInputDto input)
        {
            var isAdmin = await PermissionChecker.IsGrantedAsync(PermissionNames.Pages_Admin);
            var query = _withdrawalRepository.GetAll();
            if (!isAdmin)
            {
                query = query.Where(itm => itm.UserId == AbpSession.GetUserId());
            }
            if (input.StartDate != null)
            {
                query =  query.Where(itm => itm.CreationTime >= input.StartDate);
            }
            if (input.EndDate != null)
            {
                query =  query.Where(itm => itm.CreationTime <= input.EndDate);
            }
            var entities = await query.ToListAsync();
            var items = new List<ExportWithdrawalDto>();
            foreach (var entity in entities)
            {
                var user = await _userRepository.GetAll().Select(itm => new {itm.FullName, itm.Id})
                    .FirstOrDefaultAsync(itm => itm.Id == entity.UserId);
                var status = await _statusRepository.GetAll().Select(itm => new {itm.Name, itm.Id})
                    .FirstOrDefaultAsync(itm => itm.Id == entity.StatusId); 
                var bankDetails = await _bankDetailsRepository.GetAll().Select(itm => new {itm.Id, itm.AccountName, itm.AccountNumber, itm.BankId})
                    .FirstOrDefaultAsync(itm => itm.Id == entity.BankDetailsId);
               var bank = await _bankRepository.GetAll().Select(itm => new {itm.Id, itm.Name})
                    .FirstOrDefaultAsync(itm => itm.Id == bankDetails.BankId);
                var item = new ExportWithdrawalDto
                {
                    FullName = user.FullName,
                    Amount = entity.Amount,
                    PayIn = entity.PayIn,
                    PayTo = entity.PayTo,
                    AccountName = bankDetails.AccountName,
                    AccountNumber = bankDetails.AccountNumber,
                    BankName = bank.Name,
                    Status = status.Name,
                    FeePercentage = entity.FeePercentage,
                    FeeAmount = entity.FeeAmount,
                    CreationTime = entity.CreationTime.ToString("MM/dd/yyyy h:mm:ss tt")
                };
                items.Add(item);
            }
            return items;
            
           
        }
        
        
        
        public DataTable ConvertToDatatable<T>(IEnumerable<T> data)
        {
            var props = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            for (var i = 0; i < props.Count; i++)
            {
                var prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]); 
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }

            var values = new object[props.Count];
            foreach (var item in data)
            {
                for (var i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
      
    }
}