﻿namespace IContestPro.OpenVoteTokenHash.Dto
{
    public class OpenVoteTokenHashDto
    {
        public int ContestId { get; set; }
        public string Hash { get; set; }
    }
}