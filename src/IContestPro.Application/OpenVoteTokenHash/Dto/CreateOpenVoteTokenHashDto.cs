﻿namespace IContestPro.OpenVoteTokenHash.Dto
{
    public class CreateOpenVoteTokenHashDto
    {
        public int ContestId { get; set; }
        public string Hash { get; set; }
    }
}