﻿using Abp.Dependency;
using IContestPro.OpenVoteTokenHash.Dto;

namespace IContestPro.OpenVoteTokenHash
{
    public interface IOpenVoteTokenHashAppService : ITransientDependency
    {
        OpenVoteTokenHashDto Create(CreateOpenVoteTokenHashDto input);
        OpenVoteTokenHashDto Update(OpenVoteTokenHashDto input);
        OpenVoteTokenHashDto GetByContestId(int contestId);
        OpenVoteTokenHashDto GetByHash(string hash);
    }
}