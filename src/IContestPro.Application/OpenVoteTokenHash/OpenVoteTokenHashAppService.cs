﻿using System.Collections.Generic;
using System.Linq;
using Abp.AutoMapper;
using Abp.Json;
using IContestPro.Cookie;
using IContestPro.OpenVoteTokenHash.Dto;
using Newtonsoft.Json;

namespace IContestPro.OpenVoteTokenHash
{
    public class OpenVoteTokenHashAppService :  IOpenVoteTokenHashAppService
    {
        private readonly ICookieAppService _cookieAppService;
        private readonly List<OpenVoteTokenHashDto> _openVoteTokenHashes;

        public OpenVoteTokenHashAppService(ICookieAppService cookieAppService)
        {
            _cookieAppService = cookieAppService;
            var cookieObj = _cookieAppService.Get(AppConsts.CookieKeyOpenVoteUserHash);
            _openVoteTokenHashes = cookieObj?.Value != null ?
                JsonConvert.DeserializeObject<List<OpenVoteTokenHashDto>>(cookieObj.Value) : 
                new List<OpenVoteTokenHashDto>();
            
        }
        
        public OpenVoteTokenHashDto Create(CreateOpenVoteTokenHashDto input)
        {
            //SimpleStringCipher.Encrypt();
            _openVoteTokenHashes.Add(input.MapTo<OpenVoteTokenHashDto>());
            var jsonStr = _openVoteTokenHashes.ToJsonString();
            _cookieAppService.Set(AppConsts.CookieKeyOpenVoteUserHash, jsonStr);
            return input.MapTo<OpenVoteTokenHashDto>();
        }
        public OpenVoteTokenHashDto Update(OpenVoteTokenHashDto input)
        {
            var item = _openVoteTokenHashes.FirstOrDefault(itm => itm.ContestId == input.ContestId);
            _openVoteTokenHashes.Insert(_openVoteTokenHashes.IndexOf(item), input.MapTo<OpenVoteTokenHashDto>());
            var jsonStr = _openVoteTokenHashes.ToJsonString();
            _cookieAppService.Set(AppConsts.CookieKeyOpenVoteUserHash, jsonStr);
            return input.MapTo<OpenVoteTokenHashDto>();
        }
       
        public OpenVoteTokenHashDto GetByHash(string hash)
        {
            var item = _openVoteTokenHashes.FirstOrDefault(itm => itm.Hash == hash);
            return item?.MapTo<OpenVoteTokenHashDto>();
        }
        public OpenVoteTokenHashDto GetByContestId(int contestId)
        {
            var item = _openVoteTokenHashes.FirstOrDefault(itm => itm.ContestId == contestId);
            return item?.MapTo<OpenVoteTokenHashDto>();
        }
    }
       
    
}