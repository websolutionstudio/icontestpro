﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using IContestPro.CountryState.Dto;

namespace IContestPro.CountryState
{
    public interface ICountryStateAppService : IAsyncCrudAppService<CountryDto, string, PagedResultRequestDto, CreateCountryDto, CountryDto>
    {
        IListResult<StateDto> GetStates();
        IListResult<StateDto> GetStatesByCountryId(string countryId);
        Task<StateDto> GetStateByIdAsync(int stateId);
    }
}