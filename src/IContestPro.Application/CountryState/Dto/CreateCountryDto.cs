﻿namespace IContestPro.CountryState.Dto
{
    public class CreateCountryDto
    {
        public string Name { get; set; }
        public string Iso { get; set; }
        public int? TenantId { get; set; }
    }
}