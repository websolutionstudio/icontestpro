﻿using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using IContestPro.IContestProEntities;

namespace IContestPro.CountryState.Dto
{
    public class StateDto : FullAuditedEntity, IMayHaveTenant
    {
        [StringLength(50)]
        public string Name { get; set; }
        public int? TenantId { get; set; }
        public string CountryId { get; set; }
        
        public Country Country { get; set; }

    }
}