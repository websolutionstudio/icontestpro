﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;

namespace IContestPro.CountryState.Dto
{
    public class CountryDto : FullAuditedEntityDto<string>, IMayHaveTenant
    {
        [StringLength(50)]
        public string Name { get; set; }
        public string Iso { get; set; }
        public int? TenantId { get; set; }

        public ICollection<StateDto> States { get; set; }
    }
}