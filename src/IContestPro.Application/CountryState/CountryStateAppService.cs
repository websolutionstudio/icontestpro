﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using IContestPro.CountryState.Dto;
using IContestPro.IContestProEntities;

namespace IContestPro.CountryState
{
    public class CountryStateAppService : AsyncCrudAppService<Country, CountryDto, string, PagedResultRequestDto, CreateCountryDto, CountryDto>, ICountryStateAppService
    {
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<Country, string> _countryRepository;

        public CountryStateAppService(IRepository<State> stateRepository, IRepository<Country, 
                                string> countryRepository) : base(countryRepository)
        {
            _stateRepository = stateRepository;
            _countryRepository = countryRepository;
        }
        public override async Task<PagedResultDto<CountryDto>> GetAll(PagedResultRequestDto input)
        {
            CheckGetAllPermission();

            var query = CreateFilteredQuery(input);

            var totalCount = await AsyncQueryableExecuter.CountAsync(query);

            query = query.OrderBy(country => country.Name);
            query = ApplyPaging(query, input);

            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<CountryDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        
        public IListResult<StateDto> GetStates()
        {
            var entities = _stateRepository.GetAll().OrderBy(state => state.Name).ToList();
            return new ListResultDto<StateDto>
            {
                Items = ObjectMapper.Map<List<StateDto>>(entities)
            };
        }
        public IListResult<StateDto> GetStatesByCountryId(string countryId)
        {
            var entities = _stateRepository.GetAll().Where(state => state.CountryId == countryId)
                .OrderBy(state => state.Name)
                .ToList();
            return new ListResultDto<StateDto>
            {
                Items = ObjectMapper.Map<List<StateDto>>(entities)
            };
        }

        public async Task<StateDto> GetStateByIdAsync(int stateId)
        {
            var entity = await _stateRepository.FirstOrDefaultAsync(stateId);
            return entity.MapTo<StateDto>();
        }
    }
}