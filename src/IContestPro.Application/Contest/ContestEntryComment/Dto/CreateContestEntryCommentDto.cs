﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Contest.ContestEntryComment.Dto
{
    [AutoMapTo(typeof(IContestProEntities.ContestEntryComment))]
    public class CreateContestEntryCommentDto
    {
        [Required]
         public long UserId { get; set; }
        [Required]
        public int ContestEntryId { get; set; }
        [StringLength(255)]
        [Required]
        public string Comment { get; set; } 
        public int? TenantId { get; set; }
        
    }
}