﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Contest.ContestEntry.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Contest.ContestEntryComment.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.ContestEntryComment))]
    public class ContestEntryCommentDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public long UserId { get; set; }
        public int ContestEntryId { get; set; }
        [StringLength(255)]
        public string Comment { get; set; } 
        public int? TenantId { get; set; }
        
        public UserDto User { get; set; }
        public ContestEntryDto ContestEntry { get; set; }
    }
}