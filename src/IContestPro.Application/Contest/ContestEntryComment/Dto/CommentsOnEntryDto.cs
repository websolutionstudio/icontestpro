﻿using System;

namespace IContestPro.Contest.ContestEntryComment.Dto
{
    public class CommentsOnEntryDto
    {
        public int ContestEntryId { get; set; }
        public long? UserId { get; set; }
        public string Image { get; set; }
        public string NickName { get; set; }
        public string Comment { get; set; }
        public string UserPermalink { get; set; }
        public DateTime CreationTime { get; set; }
    }
}