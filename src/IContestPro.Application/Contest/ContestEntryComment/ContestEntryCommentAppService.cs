﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using IContestPro.Base;
using IContestPro.Contest.ContestEntryComment.Dto;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.Contest.ContestEntryComment
{
    public class ContestEntryCommentAppService : BaseAppService<IContestProEntities.ContestEntryComment, ContestEntryCommentDto, int,
                                PagedResultRequestDto, CreateContestEntryCommentDto, ContestEntryCommentDto>, IContestEntryCommentAppService
    {
        private readonly IRepository<IContestProEntities.ContestEntryComment, int> _repository;

        public ContestEntryCommentAppService(IRepository<IContestProEntities.ContestEntryComment> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }
        [AbpAuthorize]
        public override async Task<ContestEntryCommentDto> Create(CreateContestEntryCommentDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.ContestEntryComment>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        [AbpAuthorize]
        public override async Task<ContestEntryCommentDto> Update(ContestEntryCommentDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.ContestEntryComment>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        public async Task<PagedResultDto<CommentsOnEntryDto>> GetAllByContestEntryId(int contestEntryId, PagedResultRequestDto input)
        {
            /*var query = _repository.GetAllIncluding(itm => itm.User);
            query = query.Where(cont => cont.ContestEntryId == contestEntryId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<ContestEntryCommentDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );*/
            var query = _repository.GetAll().Select(cont => new{cont.Id, cont.Comment, cont.UserId, cont.ContestEntryId, cont.CreationTime, cont.User})
                .Where(itm => itm.ContestEntryId == contestEntryId);
            
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(itm => itm.CreationTime);
            query = query.PageBy(input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            
            return new PagedResultDto<CommentsOnEntryDto>(
                totalCount,
                entities.Select(item => new CommentsOnEntryDto
                {
                    ContestEntryId = item.ContestEntryId,
                    Comment = item.Comment,
                    CreationTime = item.CreationTime,
                    Image = item.User?.Image,
                    NickName = item.User?.NickName,
                    UserPermalink = item.User?.Permalink,
                    UserId = item.UserId
                }).ToList()
            );
            
            
        }

        public async Task<int> CountByContestEntryId(int contestEntryId)
        {
            return await _repository.GetAll().CountAsync(comm => comm.ContestEntryId == contestEntryId);
        }
        
        
    }
}