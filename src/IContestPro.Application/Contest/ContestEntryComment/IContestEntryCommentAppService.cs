﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestEntryComment.Dto;

namespace IContestPro.Contest.ContestEntryComment
{
    public interface IContestEntryCommentAppService : IBaseAppService<ContestEntryCommentDto, int, PagedResultRequestDto, CreateContestEntryCommentDto, ContestEntryCommentDto>
    {
        Task<PagedResultDto<CommentsOnEntryDto>> GetAllByContestEntryId(int contestEntryId, PagedResultRequestDto input);
        Task<int> CountByContestEntryId(int contestEntryId);

    }
}