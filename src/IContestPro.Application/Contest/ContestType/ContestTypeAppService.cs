﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Authorization;
using IContestPro.Base;
using IContestPro.Contest.ContestType.Dto;

namespace IContestPro.Contest.ContestType
{
    /// <summary>
    /// - There shall 3 kinds of contest:
    /// Paid Contest (participants require to pay certain amount to participate) – limited to N500
    /// Coded Contest (participants shall require to enter certain code to participate)
    /// Free Contest (entrance to participation shall be free)
    /// 
    /// - Contest Sponsoring FEE per Month (additional month shall cost 50% per month):
    /// Free Contest: N50,000
    /// Paid Contest: N100,000
    /// Coded Contest: N150,000 (per batch of 2500 codes)
    /// </summary>
    public class ContestTypeAppService : BaseAppService<IContestProEntities.ContestType, ContestTypeDto, int,
                                PagedResultRequestDto, CreateContestTypeDto, ContestTypeDto>, IContestTypeAppService
    {
        private readonly IRepository<IContestProEntities.ContestType, int> _repository;

        public ContestTypeAppService(IRepository<IContestProEntities.ContestType, int> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }
        
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<ContestTypeDto> Create(CreateContestTypeDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.ContestType>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<ContestTypeDto> Update(ContestTypeDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.ContestType>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }

        public async Task<ContestTypeDto> GetByName(string name)
        {
            return  (await _repository.FirstOrDefaultAsync(itm => itm.Name == name)).MapTo<ContestTypeDto>();
        }
    }
}