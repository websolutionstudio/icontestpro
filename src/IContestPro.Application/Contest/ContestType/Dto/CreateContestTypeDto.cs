﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Contest.ContestType.Dto
{
    [AutoMapTo(typeof(IContestProEntities.ContestType))]
    public class CreateContestTypeDto
    {
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
    }
}