﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestType.Dto;

namespace IContestPro.Contest.ContestType
{
    public interface IContestTypeAppService : IBaseAppService<ContestTypeDto, int, PagedResultRequestDto, CreateContestTypeDto, ContestTypeDto>
    {
        Task<ContestTypeDto> GetByName(string name);
    }
}