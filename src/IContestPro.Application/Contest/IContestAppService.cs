﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.Dto;
using IContestPro.Status.Dto;

namespace IContestPro.Contest
{
    public interface IContestAppService : IBaseAppService<ContestDto, int, PagedResultRequestDto, CreateContestDto, ContestDto>
    {
        Task<PagedResultDto<ContestWithFilterDto>>
            GetContestsWithFilter(
                bool filterByPaid = false, 
                bool filterByCoded = false,
                bool filterByVp = false,
                bool filterByFree = false,
                bool filterByEveryoneWin = false, 
                bool filterByHighestVote = false,
                string statusName = AppConsts.StatusLive,
                int page = 1,
                int pageSize = 20
            );
        Task<PagedResultDto<ContestWithFilterDto>>
            SearchContestsByTag(
                string tag,
                int page = 1,
                int pageSize = 20
            );
        Task<ContestDetailsDto> GetWithDetails(EntityDto<int> input);
        Task<ContestDto> GetByPermalink(string permalink);
        Task<ContestDetailsDto> GetByPermalinkWithDetails(string permalink);
        Task<PagedResultDto<ContestDetailsDto>> GetAllWithDetails(PagedResultRequestDto input);
        Task<PagedResultDto<ContestDto>> GetAllByStatus(string statusName, PagedResultRequestDto input);
        Task<PagedResultDto<ContestDetailsDto>> GetAllByStatusWithDetails(string statusName,
            PagedResultRequestDto input);

        Task<PagedResultDto<ContestDto>> GetAllByUserId(long userId, PagedResultRequestDto input);
        Task<PagedResultDto<ContestDetailsDto>> GetAllWithDetailsByUserId(long userId, PagedResultRequestDto input);
        Task<PagedResultDto<PastWinnersDto>> PastWinners(int page, int pageSize);

        Task<ContestDto> UpdateStatusAsync(UpdateStatusDto model);
        Task<bool> IsUserContestOwner(int contestId);
        Task<StatusDto> GetContestStatus(int contestId);
        Task NotifyExpiringContests();
        Task DisableExpiredContests();

        Task<PagedResultDto<ContestDto>> GetAllByUserIdAndStatus(long userId, string statusName,
            PagedResultRequestDto input, bool includeFeatured = true);

        Task<List<UsersInContestDto>> GetUsersInContest(int contestId);
        Task<List<ContestsEndingSoonDto>> GetContestsEndingSoon(int pageSize = 5);
        Task<PagedResultDto<ViewsDto>> GetViewsByPermalink(string permalink, int page, int pageSize);

    }
}   