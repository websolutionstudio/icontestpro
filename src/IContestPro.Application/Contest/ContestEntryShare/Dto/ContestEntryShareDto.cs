﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;

namespace IContestPro.Contest.ContestEntryShare.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.ContestEntryShare))]
    public class ContestEntryShareDto : FullAuditedEntityDto<long>, IMayHaveTenant
    {
        public long? UserId { get; set; }
        public int ContestEntryId { get; set; }
        public int? TenantId { get; set; }
    }
}