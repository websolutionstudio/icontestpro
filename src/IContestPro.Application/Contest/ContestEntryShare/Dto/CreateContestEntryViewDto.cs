﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Contest.ContestEntryShare.Dto
{
    [AutoMapTo(typeof(IContestProEntities.ContestEntryShare))]
    public class CreateContestEntryShareDto
    {
        public long? UserId { get; set; }
        [Required]
        public int ContestEntryId { get; set; }
    }
}