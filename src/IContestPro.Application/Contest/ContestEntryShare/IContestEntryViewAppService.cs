﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestEntryShare.Dto;

namespace IContestPro.Contest.ContestEntryShare
{
    public interface IContestEntryShareAppService : IBaseAppService<ContestEntryShareDto, long, PagedResultRequestDto, CreateContestEntryShareDto, ContestEntryShareDto>
    {
        Task<int> GetSharesByContestEntryId(int contestEntryId);
    }
}