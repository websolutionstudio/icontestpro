﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Base;
using IContestPro.Contest.ContestEntryShare.Dto;

namespace IContestPro.Contest.ContestEntryShare
{
    public class ContestEntryShareAppService : BaseAppService<IContestProEntities.ContestEntryShare, ContestEntryShareDto, long,
                                PagedResultRequestDto, CreateContestEntryShareDto, ContestEntryShareDto>, IContestEntryShareAppService
    {
        private readonly IRepository<IContestProEntities.ContestEntryShare, long> _repository;

        public ContestEntryShareAppService(IRepository<IContestProEntities.ContestEntryShare, long> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }
        public override async Task<ContestEntryShareDto> Create(CreateContestEntryShareDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.ContestEntryShare>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        [AbpAuthorize]
        public override async Task<ContestEntryShareDto> Update(ContestEntryShareDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.ContestEntryShare>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }

        public async Task<int> GetSharesByContestEntryId(int contestEntryId)
        {
            return await _repository.CountAsync(item => item.ContestEntryId == contestEntryId);
        }
    }
}