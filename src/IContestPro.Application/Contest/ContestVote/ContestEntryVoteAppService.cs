﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using IContestPro.Base;
using IContestPro.Contest.ContestVote.Dto;
using IContestPro.Contest.Dto;
using IContestPro.IContestProEntities;
using IContestPro.OpenVoteTokenHash;
using IContestPro.OpenVoteTokenHash.Dto;
using IContestPro.PaymentHistories;
using IContestPro.PaymentHistories.Dto;
using IContestPro.Referral;
using IContestPro.Status;
using IContestPro.VoteType;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using IContestPro.Helpers;

namespace IContestPro.Contest.ContestVote
{
    public class ContestEntryVoteAppService : BaseAppService<Vote, VoteDto, long,
                                PagedResultRequestDto, CreateVoteDto, VoteDto>, IContestEntryVoteAppService
    {
        private readonly IRepository<IContestProEntities.Contest> _contestRepository;
        private readonly IRepository<IContestProEntities.ContestEntry> _contestEntryRepository;
        private readonly IRepository<IContestProEntities.Wallet> _walletRepository;
        private readonly IRepository<IContestProEntities.UserVotePoint> _userVotePointRepository;
        private readonly IRepository<Vote, long> _repository;
        private readonly IVoteTypeAppService _voteTypeAppService;
        private readonly IOpenVoteTokenHashAppService _openVoteTokenHashAppService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IPaymentHistoriesAppService _paymentHistoriesAppService;
        private readonly IStatusAppService _statusAppService;
        private readonly IReferralAppService _referralAppService;

        public ContestEntryVoteAppService(
            IRepository<Vote, long> repository, 
            IRepository<IContestProEntities.Contest> contestRepository, 
            IRepository<IContestProEntities.ContestEntry> contestEntryRepository, 
            IRepository<IContestProEntities.Wallet> walletRepository, 
            IVoteTypeAppService voteTypeAppService, 
            IOpenVoteTokenHashAppService openVoteTokenHashAppService, 
            IRepository<IContestProEntities.UserVotePoint> userVotePointRepository,
            IHttpContextAccessor httpContextAccessor,
            IPaymentHistoriesAppService paymentHistoriesAppService,
            IStatusAppService statusAppService, IReferralAppService referralAppService, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
            _contestRepository = contestRepository;
            _contestEntryRepository = contestEntryRepository;
            _voteTypeAppService = voteTypeAppService;
            _openVoteTokenHashAppService = openVoteTokenHashAppService;
            _userVotePointRepository = userVotePointRepository;
            _httpContextAccessor = httpContextAccessor;
            _paymentHistoriesAppService = paymentHistoriesAppService;
            _statusAppService = statusAppService;
            _referralAppService = referralAppService;
            _walletRepository = walletRepository;
          
        }
        
        public override async Task<VoteDto> Create(CreateVoteDto model)
        {
            var contest = (await _contestRepository.GetAllIncluding(item => item.Status)
                .FirstOrDefaultAsync(item => item.Id == model.ContestId))?.MapTo<ContestDto>();
            var contestEntry = await _contestEntryRepository.GetAll()
                .Select(itm => new {itm.Id, itm.UserId})
                .FirstOrDefaultAsync(item => item.Id == model.ContestEntryId);
            if (contest == null)
            {
                throw new UserFriendlyException("Contest not found.");
            }
            if (contest.StartDate > DateTime.Now)
            {
                throw new UserFriendlyException("You cannot vote for a contest that is not started.");
            }
            if (contest.EndDate < DateTime.Now)
            {
                throw new UserFriendlyException("Contest is closed. Voting not acceptable.");
            }
            if (contest.Status.Name != AppConsts.StatusLive)
            {
                throw new UserFriendlyException("Contest not live.");
            }
            if (contestEntry == null)
            {
                throw new UserFriendlyException("Contest entry not live.");
            }
           
            if (AbpSession.UserId != null && contestEntry.UserId == AbpSession.UserId)
            {
                throw new UserFriendlyException("You cannot vote for yourself.");
            }
            
            
            var voteType = await _voteTypeAppService.Get(new EntityDto {Id = contest.VoteTypeId});
            if (voteType.Name != AppConsts.VoteTypeOpen && AbpSession.UserId == null)
            {
                throw new UserFriendlyException("Please login to cast your vote.");
            }
            Vote voteSaved;
            //Get the reward for this contest voting type
            // var voteReward = await _voterRewardPointAppService.GetByVoteTypeId(contest.VoteTypeId);
            
            //User can only vote for 3 entries in an open vote. Can vote once every hour
            if (voteType.Name == AppConsts.VoteTypeOpen)
            {
                OpenVoteTokenHashDto userCookie = null;
                var isMobile = _httpContextAccessor.HttpContext.Request.Headers[AppConsts.IsMobile];
                if (isMobile.Count == 0)
                {
                    userCookie =  _openVoteTokenHashAppService.GetByContestId(model.ContestId);  
                }

                var openVoteTokenHashHeader = _httpContextAccessor.HttpContext.Request.Headers[AppConsts.OpenVoteTokenHash];
                var openVoteTokenHash = isMobile.Count > 0 && openVoteTokenHashHeader.Count > 0 ? openVoteTokenHashHeader[0] : userCookie?.Hash;
                if (!string.IsNullOrEmpty(openVoteTokenHash))
                {
                    var userLastVotedOnEntry = await GetUserLastVoteOnEntryByHash(openVoteTokenHash, model.ContestEntryId);
                    if (userLastVotedOnEntry != null)
                    {
                        var minutes = (DateTime.Now - userLastVotedOnEntry.CreationTime).TotalMinutes;
                        if (minutes < 60)
                        {
                            var dateTime = userLastVotedOnEntry.CreationTime.AddHours(1);
                            throw new UserFriendlyException($"You are allowed to vote once in 1 hour on this entry. Please wait till {dateTime:hh:mmtt} to vote again.");
                        } 
                        var entriesVoted = await GetAllByHash(openVoteTokenHash, new PagedResultRequestDto());
                        //If user has voted for 3 entries and wants to vote for another entry deny that
                        if (entriesVoted.TotalCount >= 3)
                        {
                            //Is entry in the entries voted
                            if (entriesVoted.Items.FirstOrDefault(itm => 
                                    itm.ContestEntryId == model.ContestEntryId) == null)
                            {
                                throw new UserFriendlyException("You are allowed to vote for only three (3) entries in this contest.");
                            }
                        }
                    }

                    model.Hash = openVoteTokenHash;
                    voteSaved = await _repository.InsertAsync(model.MapTo<Vote>());
                    CurrentUnitOfWork.SaveChanges();
                    if (voteSaved.Id > 0)
                    {
                        if (voteSaved.UserId != null)
                        {
                            await Task.Run(async () =>
                            {
                                if (await _referralAppService.isUserReferred(voteSaved.UserId.Value))
                                {
                                    var referrer =
                                        await _referralAppService.CreditUserReferrerVotePointAward(voteSaved.UserId.Value);
                                }
                            });
                        }

                        return voteSaved.MapTo<VoteDto>();
                    }
                    throw new UserFriendlyException("Error occurred while casting your vote.");
                }
              
                   //At first vote
                    var hash = Guid.NewGuid().ToString("N");
                    _openVoteTokenHashAppService.
                        Create(new CreateOpenVoteTokenHashDto{ContestId = model.ContestId, Hash = hash});   
                    model.Hash = hash;
                    voteSaved = await _repository.InsertAsync(model.MapTo<Vote>());
                    CurrentUnitOfWork.SaveChanges();
                    if (voteSaved.Id > 0)
                    {
                        if (voteSaved.UserId != null)
                        {
                            await Task.Run(async () =>
                            {
                                if (await _referralAppService.isUserReferred(voteSaved.UserId.Value))
                                {
                                    var referrer =
                                        await _referralAppService.CreditUserReferrerVotePointAward(voteSaved.UserId.Value);
                                }
                            });
                        }
                        return voteSaved.MapTo<VoteDto>();
                    }
                    throw new UserFriendlyException("Error occurred while casting your vote.");
            }

            //Get the Id of the user. If null exception is thrown
            var userId = AbpSession.GetUserId();
            var userVotePoint =  await _userVotePointRepository.FirstOrDefaultAsync(u => u.UserId == userId);

            if (voteType.Name == AppConsts.VoteTypePaid)
                {
                    var wallet = await _walletRepository.FirstOrDefaultAsync(w => w.UserId == userId);

                    if (contest.VoteAmount == null || contest.VoteAmount == 0)
                    {
                        throw new UserFriendlyException("Vote amount cannot be null or zero on paid vote."); 
                    }
                    
                    var contestAmount = contest.VoteAmount ?? 0;
                    
                    if (wallet.Balance < contestAmount)
                    {
                        throw new UserFriendlyException("Insufficient balance. Please add fund to your wallet to complete your vote.");
                    }

                        wallet.Balance -= contestAmount;
                        voteSaved = await _repository.InsertAsync(model.MapTo<Vote>());
                        userVotePoint.Points += voteType.RewardPoint;
                        CurrentUnitOfWork.SaveChanges();
                        if (voteSaved.Id > 0)
                        {
                            var status = await  _statusAppService.GetByNameAsync(AppConsts.StatusPaymentConfirmed);
                           
                            await _paymentHistoriesAppService.Create(new CreatePaymentHistoryDto
                            {
                                Reference =  Guid.NewGuid().ToAppString(),
                                Amount = contestAmount,
                                Description = string.Format("Payment for vote casted on the contest '{0}'.", contest.Title),
                                Title = "Payment for vote",
                                UserId = AbpSession.GetUserId(),
                                WalletOperation = WalletOperation.Debit,
                                StatusId = status.Id
                
                            });
                            if (voteSaved.UserId != null)
                            {
                                await Task.Run(async () =>
                                {
                                    if (await _referralAppService.isUserReferred(voteSaved.UserId.Value))
                                    {
                                        var referrer =
                                            await _referralAppService.CreditUserReferrerVotePointAward(voteSaved.UserId.Value);
                                    }
                                });
                            }
                           return voteSaved.MapTo<VoteDto>();
                        }
                        throw new UserFriendlyException("Error occurred while casting your vote.");
                    
                }

                var userLastVote = await GetUserLastVoteOnEntry(AbpSession.GetUserId(), model.ContestEntryId);
                if (voteType.Name == AppConsts.VoteTypeHourly)
                {
                    if (userLastVote != null)
                    {
                        var minutes = (DateTime.Now - userLastVote.CreationTime).TotalMinutes;
                        if (minutes < 60)
                        {
                            var dateTime = userLastVote.CreationTime.AddHours(1);
                            throw new UserFriendlyException($"You are allowed to vote once every hour on this entry. Please wait till {dateTime:hh:mmtt} to vote again.");
                        }  
                    }
                     userVotePoint =  await _userVotePointRepository.FirstOrDefaultAsync(u => u.UserId == userId);
                    voteSaved = await _repository.InsertAsync(model.MapTo<Vote>());
                    userVotePoint.Points += voteType.RewardPoint;
                    CurrentUnitOfWork.SaveChanges();
                    if (voteSaved.Id > 0)
                    {
                        if (voteSaved.UserId != null)
                        {
                           await Task.Run(async () =>
                            {
                                if (await _referralAppService.isUserReferred(voteSaved.UserId.Value))
                                {
                                    var referrer =
                                        await _referralAppService.CreditUserReferrerVotePointAward(voteSaved.UserId.Value);
                                }
                            });
                        }
                        return voteSaved.MapTo<VoteDto>();
                    }
                    throw new UserFriendlyException("Error occurred while casting your vote.");
                }
            if (voteType.Name == AppConsts.VoteTypeDaily)
            {
                if (userLastVote != null)
                {
                    var hours = (DateTime.Now - userLastVote.CreationTime).TotalHours;
                    if (hours < 24)
                    {
                        var dateTime = userLastVote.CreationTime.AddHours(24);
                        throw new UserFriendlyException($"You are allowed to vote only once in 24 hours on this entry. Please wait till {dateTime:M}@{dateTime:hh:mmtt} to vote again.");
                    }  
                }
                 userVotePoint =  await _userVotePointRepository.FirstOrDefaultAsync(u => u.UserId == userId);
                voteSaved = await _repository.InsertAsync(model.MapTo<Vote>());
                userVotePoint.Points += voteType.RewardPoint;
                CurrentUnitOfWork.SaveChanges();
                if (voteSaved.Id > 0)
                {
                    if (voteSaved.UserId != null)
                    {
                        await Task.Run(async () =>
                        {
                            if (await _referralAppService.isUserReferred(voteSaved.UserId.Value))
                            {
                                var referrer =
                                    await _referralAppService.CreditUserReferrerVotePointAward(voteSaved.UserId.Value);
                            }
                        });
                    }
                    return voteSaved.MapTo<VoteDto>();
                }
                throw new UserFriendlyException("Error occurred while casting your vote.");
            }
            
            // Unique contest
            if (voteType.Name == AppConsts.VoteTypeUnique)
            {
                if (userLastVote != null)
                {
                        throw new UserFriendlyException("You are allowed to vote only once on this entry.");
                }
                 userVotePoint =  await _userVotePointRepository.FirstOrDefaultAsync(u => u.UserId == userId);
                voteSaved = await _repository.InsertAsync(model.MapTo<Vote>());
                userVotePoint.Points += voteType.RewardPoint;
                CurrentUnitOfWork.SaveChanges();
                if (voteSaved.Id > 0)
                {
                    if (voteSaved.UserId != null)
                    {
                        await Task.Run(async () =>
                        {
                            if (await _referralAppService.isUserReferred(voteSaved.UserId.Value))
                            {
                                var referrer =
                                    await _referralAppService.CreditUserReferrerVotePointAward(voteSaved.UserId.Value);
                            }
                        });
                    }
                    return voteSaved.MapTo<VoteDto>();
                }
                throw new UserFriendlyException("Error occurred while casting your vote.");
            }
            
            
            //Vote point contest
            if (voteType.Name == AppConsts.VoteTypeVotePoint)
            {
                //Get the amount of vote points this user has.
                 userVotePoint =  await _userVotePointRepository.FirstOrDefaultAsync(u => u.UserId == userId);
                if (userVotePoint.Points < voteType.VotePoint)
                {
                        throw new UserFriendlyException($"Insuficient vote point. This contest requires {voteType.VotePoint} vote point to vote. You have {userVotePoint.Points} vote point(s). Please go to your dashboard and buy vote points to vote.");
                }

                voteSaved = await _repository.InsertAsync(model.MapTo<Vote>());
                userVotePoint.Points -= voteType.VotePoint;
                userVotePoint.Points += voteType.RewardPoint;
                CurrentUnitOfWork.SaveChanges();
                if (voteSaved.Id > 0)
                {
                    if (voteSaved.UserId != null)
                    {
                        await Task.Run(async () =>
                        {
                            if (await _referralAppService.isUserReferred(voteSaved.UserId.Value))
                            {
                                var referrer =
                                    await _referralAppService.CreditUserReferrerVotePointAward(voteSaved.UserId.Value);
                            }
                        });
                    }
                    return voteSaved.MapTo<VoteDto>();
                }
                throw new UserFriendlyException("Error occurred while casting your vote.");
            }
            throw new UserFriendlyException("Error occurred while casting your vote.");   
            
        }

        public async Task<PagedResultDto<VoteDto>> GetAllByHash(string hash, PagedResultRequestDto input)
        {
            var query = _repository.GetAll()
                                       .Where(vote => vote.Hash == hash);
                       var totalCount = await AsyncQueryableExecuter.CountAsync(query);
                       query = ApplyPaging(query, input);
                       query = query.OrderByDescending(vote => vote.CreationTime);
                       var entities = await AsyncQueryableExecuter.ToListAsync(query);
           
                       return new PagedResultDto<VoteDto>(
                           totalCount,
                           entities?.Select(MapToEntityDto).ToList()
         );
        }
       public async Task<VoteDto> GetByHash(string hash)
        {
            var entity =  await _repository.FirstOrDefaultAsync(vote => vote.Hash == hash);
            return  entity != null ? entity.MapTo<VoteDto>() : null;
        }

        [AbpAuthorize]
        public override async Task<VoteDto> Update(VoteDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.Vote>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }


        public async Task<int> CountByContestId(int contestId)
        {
            return await _repository.GetAll().CountAsync(comm => comm.ContestId == contestId);
        }

        public async Task<int> CountUserVotes(long userId)
        {
            return await _repository.CountAsync(item => item.UserId == userId);
        }

        public async Task<int> CountByContestEntryId(int conestEntryId)
        {
            return await _repository.GetAll().CountAsync(comm => comm.ContestEntryId == conestEntryId);

        }

        public async Task<int> CountUserVotesOnContest(long userId, int contestId)
        {
            return await _repository.GetAll().CountAsync(cnt => cnt.UserId == userId &&  cnt.ContestId == contestId );
        }

        public async Task<int> CountUserVotesOnEntry(long userId, int conestEntryId)
        {
            return await _repository.GetAll().CountAsync(cnt => cnt.UserId == userId && cnt.ContestEntryId == conestEntryId);

        }
        public async Task<int> CountEntriesUserHasVoted(long userId, int conestId)
        {
            var items = await _repository.GetAll().Where(cnt => cnt.UserId == userId && cnt.ContestId == conestId).ToListAsync();
           return items.Distinct().OrderBy(value => value.Id).Count();
        }
       public async Task<int> CountEntriesUserHasVotedByHash(string hash, int conestId)
        {
            var items = await _repository.GetAll().Where(cnt => cnt.Hash == hash && cnt.ContestId == conestId).ToListAsync();
            return items.Distinct().OrderBy(value => value.Id).Count();
        }

        public async Task<PagedResultDto<VoteDto>> GetUserVotes(long userId, PagedResultRequestDto input)
        {
            var query = _repository.GetAll()
                            .Where(vote => vote.UserId == userId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderByDescending(vote => vote.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<VoteDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }

        public async Task<PagedResultDto<VoteDto>> GetUserVotesOnContest(long userId, int contestId, PagedResultRequestDto input)
        {
            var query = _repository.GetAll()
                .Where(vote => vote.UserId == userId && vote.ContestId == contestId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderByDescending(vote => vote.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<VoteDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }

        public async Task<PagedResultDto<VoteDto>> GetUserVotesOnEntry(long userId, int conestEntryId, PagedResultRequestDto input)
        {
            var query = _repository.GetAll()
                .Where(vote => vote.UserId == userId && vote.ContestEntryId == conestEntryId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderByDescending(vote => vote.CreationTime);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<VoteDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }

        public async Task<VoteDto> GetUserLastVoteOnEntry(long userId, int conestEntryId)
        {
            var entity =  await _repository.GetAll()
                .OrderByDescending(vote => vote.CreationTime)
                .FirstOrDefaultAsync(vote => vote.UserId == userId && vote.ContestEntryId == conestEntryId);
            return  entity != null ? entity.MapTo<VoteDto>() : null;
        }
        public async Task<VoteDto> GetUserLastVoteOnContest(long userId, int conestId)
        {
            var entity =  await _repository.GetAll()
                .OrderByDescending(vote => vote.CreationTime)
                .FirstOrDefaultAsync(vote => vote.UserId == userId && vote.ContestId == conestId);
            return  entity != null ? entity.MapTo<VoteDto>() : null;
        }
     public async Task<VoteDto> GetUserLastVoteOnEntryByHash(string hash, int conestEntryId)
        {
            var entity =  await _repository.GetAll()
                .OrderByDescending(vote => vote.CreationTime)
                .FirstOrDefaultAsync(vote => vote.Hash == hash && vote.ContestEntryId == conestEntryId);
            return  entity?.MapTo<VoteDto>();
        }

       

        public async Task<VoteDto> GetUserLastVoteOnContestByHash(string hash, int conestId)
                    {
                        var entity =  await _repository.GetAll()
                            .OrderByDescending(vote => vote.CreationTime)
                            .FirstOrDefaultAsync(vote => vote.Hash == hash && vote.ContestId == conestId);
                        return  entity != null ? entity.MapTo<VoteDto>() : null;
                    }

        public async Task<PagedResultDto<VoteDto>> GetAllByEntryId(int entryId, PagedResultRequestDto input)
        {
            var query =  _repository.GetAllIncluding(entr => entr.User)
                          .Where(ent => ent.ContestEntryId == entryId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<VoteDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }

        public async Task<PagedResultDto<VoteDto>> GetAllByContestId(int contestId, PagedResultRequestDto input)
        {
            var query =  _repository.GetAllIncluding(entr => entr.User)
                .Where(ent => ent.ContestId == contestId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<VoteDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }

        public async Task<PagedResultDto<VoteDto>> GetAllByEntryPermalink(string permalink, PagedResultRequestDto input)
        {
            var entry = await _contestEntryRepository.FirstOrDefaultAsync(itm => itm.Permalink == permalink);
            if (entry == null)
            {
                return null;
            }
            var query =  _repository.GetAllIncluding(entr => entr.User)
                .Where(ent => ent.ContestEntryId == entry.Id);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<VoteDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }

        public async Task<PagedResultDto<VoteDto>> GetAllByContestPermalink(string permalink, PagedResultRequestDto input)
        {
            var item = await _contestRepository.GetAllIncluding(itm => itm.Status).Select(cont => new{cont.Id,cont.Status,cont.StatusId, cont.Permalink}).FirstOrDefaultAsync(itm => itm.Permalink == permalink);
            if (item == null)
            {
                return null;
            }
            if (item.Status.Name != AppConsts.StatusLive)
            {
                throw new UserFriendlyException("Contest not live.");
            }
            var query =  _repository.GetAllIncluding(entr => entr.User)
                .Where(ent => ent.ContestId == item.Id);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(itm => itm.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<VoteDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        public async Task<PagedResultDto<ContestEntryUserVotesDto>> GetUsersVotedForEntry(int contestEntryId, PagedResultRequestDto input)
        {
            var query = _repository.GetAll().Select(cont => new{cont.Id, cont.UserId, cont.ContestEntryId, cont.CreationTime, cont.User})
                           .Where(itm => itm.ContestEntryId == contestEntryId);
            
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(itm => itm.CreationTime);
            query = query.PageBy(input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            
            return new PagedResultDto<ContestEntryUserVotesDto>(
                totalCount,
                entities.Select(item => new ContestEntryUserVotesDto
                {
                    ContestEntryId = item.ContestEntryId,
                    CreationTime = item.CreationTime,
                    Image = item.User?.Image,
                    NickName = item.User?.NickName,
                    UserId = item.UserId
                }).ToList()
            );
        }
    }
}