﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using IContestPro.Contest.ContestEntry.Dto;
using IContestPro.Contest.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Contest.ContestVote.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Vote))]
    public class CreateVoteDto
    {
        public long? UserId { get; set; }
        [Required]
        public int ContestId { get; set; }
        [Required]
        public int ContestEntryId { get; set; }
        public int? TenantId { get; set; }
        [StringLength(50)]
        public string Hash { get; set; }

        public UserDto User { get; set; }
        public ContestDto Contest { get; set; }
        public ContestEntryDto ContestEntry { get; set; }
    }
}