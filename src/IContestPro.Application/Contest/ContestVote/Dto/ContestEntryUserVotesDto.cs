﻿using System;

namespace IContestPro.Contest.ContestVote.Dto
{
    public class ContestEntryUserVotesDto
    {
        public int ContestEntryId { get; set; }
        public long? UserId { get; set; }
        public string Image { get; set; }
        public string NickName { get; set; }
        public DateTime CreationTime { get; set; }
    }
}