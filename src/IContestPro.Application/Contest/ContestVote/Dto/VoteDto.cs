﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Contest.ContestEntry.Dto;
using IContestPro.Contest.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Contest.ContestVote.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Vote))]
    public class VoteDto : FullAuditedEntityDto<long>, IMayHaveTenant
    {
        public long? UserId { get; set; }
        public int ContestId { get; set; }
        public int ContestEntryId { get; set; }
        public int? TenantId { get; set; }
        public string Hash { get; set; }

        public UserDto User { get; set; }
        public ContestDto Contest { get; set; }
        public ContestEntryDto ContestEntry { get; set; }
    }
}