﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestVote.Dto;

namespace IContestPro.Contest.ContestVote
{
    public interface IContestEntryVoteAppService : IBaseAppService<VoteDto, long, PagedResultRequestDto, CreateVoteDto, VoteDto>
    {
        Task<int> CountByContestId(int contestId);
        Task<int> CountUserVotes(long userId);
        Task<int> CountByContestEntryId(int conestEntryId);
        Task<int> CountUserVotesOnContest(long userId, int contestId);
        Task<int> CountUserVotesOnEntry(long userId, int conestEntryId);
        
        Task<PagedResultDto<VoteDto>> GetUserVotes(long userId, PagedResultRequestDto input);
        Task<PagedResultDto<VoteDto>> GetUserVotesOnContest(long userId, int contestId, PagedResultRequestDto input);
        Task<PagedResultDto<VoteDto>> GetUserVotesOnEntry(long userId, int conestEntryId, PagedResultRequestDto input);
        Task<VoteDto> GetUserLastVoteOnEntry(long userId, int conestEntryId);
        Task<VoteDto> GetUserLastVoteOnContest(long userId, int conestId);

        Task<PagedResultDto<VoteDto>> GetAllByEntryId(int entryId, PagedResultRequestDto input);
        Task<PagedResultDto<VoteDto>> GetAllByContestId(int contestId, PagedResultRequestDto input);
        
        Task<PagedResultDto<VoteDto>> GetAllByEntryPermalink(string permalink, PagedResultRequestDto input);
        Task<PagedResultDto<VoteDto>> GetAllByContestPermalink(string permalink, PagedResultRequestDto input);
        Task<PagedResultDto<VoteDto>> GetAllByHash(string hash, PagedResultRequestDto input);
        Task<int> CountEntriesUserHasVoted(long userId, int conestId);
        Task<int> CountEntriesUserHasVotedByHash(string hash, int conestId);
        Task<VoteDto> GetByHash(string hash);
        Task<VoteDto> GetUserLastVoteOnContestByHash(string hash, int conestId);
        Task<VoteDto> GetUserLastVoteOnEntryByHash(string hash, int conestEntryId);
        Task<PagedResultDto<ContestEntryUserVotesDto>> GetUsersVotedForEntry(int contestEntryId, PagedResultRequestDto input);

    }   
}