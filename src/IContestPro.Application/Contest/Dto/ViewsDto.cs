﻿using System;

namespace IContestPro.Contest.Dto
{
    public class ViewsDto
    {
        public long? UserId { get; set; }
        public int ContestId { get; set; }
        public string Nickname { get; set; }
        public string Image { get; set; }
        public string Permalink { get; set; }
        public DateTime CreationTime { get; set; }
        public int TotalCount { get; set; }
    }
}