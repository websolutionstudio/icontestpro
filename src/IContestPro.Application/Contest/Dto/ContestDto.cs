﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Contest.ContestCategory.Dto;
using IContestPro.Contest.ContestEntryType.Dto;
using IContestPro.Contest.ContestFee.Dto;
using IContestPro.Contest.ContestType.Dto;
using IContestPro.Helpers.DatabaseFieldLengths;
using IContestPro.Periodic.Dto;
using IContestPro.Sponsor.Dto;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;
using IContestPro.VoteType.Dto;

namespace IContestPro.Contest.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Contest))]
    public class ContestDto : FullAuditedEntityDto<int>, IMayHaveTenant
    {
        public const int TitleLength = 250;
        public const int PictureLength = 50;
        public const int DescriptionLength = 1950;
        public const int ObjectiveLength = 400;
        public const int EveryoneIsAWinnerUrlLength = 200;
        public const int TagsLength = 80;
        
        [StringLength(250)]
        public string Title { get; set; } 
        [StringLength(50)]
        public string Picture { get; set; } 
        [StringLength(1950)]
        public string Description { get; set; } 
        [StringLength(450)]
        public string Objective { get; set; } 
        public int MinimumParticipants { get; set; } 
        public DateTime StartDate { get; set; } 
        public DateTime EndDate { get; set; } 
        public DateTime EntryStartDate { get; set; } 
        public DateTime EntryEndDate { get; set; } 
        public int? TenantId { get; set; }
        public int StatusId { get; set; }
        public int VoteTypeId { get; set; }
        [StringLength(200)]
        public string EveryoneIsAWinnerUrl { get; set; }
        public int ContestFeeId { get; set; }
        public int SponsorId { get; set; }
        public bool IsFeatured { get; set; }
        
        public int ContestTypeId { get; set; }
        public int ContestEntryTypeId { get; set; }
        public int ContestCategoryId { get; set; }
        public long UserId { get; set; }
        [StringLength(80)]
        public string Tags { get; set; }
        [StringLength(StandardFieldLength.MaxPermalinkLength)]
        public string Permalink { get; set; }
        public decimal? EntryAmount { get; set; }
        public decimal? VoteAmount { get; set; }
        public int? PeriodicId { get; set; }
        public DateTime? NextPaymentDate { get; set; }

        public ContestTypeDto ContestType { get; set; }
        public ContestEntryTypeDto ContestEntryType { get; set; }
        public ContestCategoryDto ContestCategory { get; set; }
        public UserDto User { get; set; }
        public StatusDto Status { get; set; }
        public VoteTypeDto VoteType { get; set; }
        public PeriodicDto Periodic { get; set; }
        public ContestFeeDto ContestFee { get; set; }
        public SponsorDto Sponsor { get; set; }

    }
}