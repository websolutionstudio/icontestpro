﻿namespace IContestPro.Contest.Dto
{

    public class ContestDetailsDto
    {
        public ContestDto Contest { get; set; }       
        //public CompanyDto Company { get; set; }       
        public int TotalComments { get; set; }       
        public int TotalVotes { get; set; }       
        public int TotalEntries { get; set; }
    }
}