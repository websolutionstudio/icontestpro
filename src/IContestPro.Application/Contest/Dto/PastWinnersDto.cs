﻿using System;

namespace IContestPro.Contest.Dto
{

    public class PastWinnersDto
    {
        public long UserId { get; set; }       
        public string PhoneNumber { get; set; }       
        public string NickName { get; set; }       
        public string Image { get; set; }       
        public string Profession { get; set; }       
        public string FaceBook { get; set; }       
        public string LinkedIn { get; set; }       
        public string Twitter { get; set; }       
        public string Instagram { get; set; }       
        public string Permalink { get; set; }       
        public string ContestId { get; set; }       
        public string ContestTitle { get; set; }       
        public string ContestPermalink { get; set; }       
        public string ContestType { get; set; }       
        public string ContestCategory { get; set; }       
        public string ContestPicture { get; set; }       
        public int TotalVotes { get; set; }       
        public int TotalCount { get; set; }       
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreationTime { get; set; }
    }
}