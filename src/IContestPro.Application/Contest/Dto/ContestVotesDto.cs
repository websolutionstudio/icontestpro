﻿using System;

namespace IContestPro.Contest.Dto
{
    public class ContestVotesDto
    {
        public string NickName { get; set; } 
        public string Image { get; set; } 
        public string Location { get; set; } 
        public DateTime DateVoted { get; set; } 
        

    }
}