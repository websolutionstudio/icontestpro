﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using IContestPro.Helpers.DatabaseFieldLengths;

namespace IContestPro.Contest.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Contest))]
    [AutoMapFrom(typeof(ContestDto))]
    public class CreateContestDto
    {
        public CreateContestDto()
        {
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
        }
        
        

       
        [StringLength(ContestDto.TitleLength)]
        [Required]
        public string Title { get; set; } 
        [StringLength(ContestDto.PictureLength)]
        [Required]
        public string Picture { get; set; } 
        [StringLength(ContestDto.DescriptionLength)]
        [Required]
        public string Description { get; set; } 
        [StringLength(ContestDto.ObjectiveLength)]
        [Required]
        public string Objective { get; set; } 
        public int MinimumParticipants { get; set; } 
        [Required]
        public DateTime StartDate { get; set; } 
        [Required]
        public DateTime EndDate { get; set; } 
        
        [Required]
        public DateTime EntryStartDate { get; set; } 
        [Required]
        public DateTime EntryEndDate { get; set; } 
        
        [StringLength(ContestDto.EveryoneIsAWinnerUrlLength)]
        public string EveryoneIsAWinnerUrl { get; set; }
        [StringLength(ContestDto.TagsLength)]
        public string Tags { get; set; }
        [StringLength(StandardFieldLength.MaxPermalinkLength)]
        public string Permalink { get; set; }
        public decimal? EntryAmount { get; set; }
        public int ContestFeeId { get; set; }
        [Required]
        public int SponsorId { get; set; }
        [Required]
        public int ContestTypeId { get; set; }
        [Required]
        public int ContestEntryTypeId { get; set; }
        [Required]
        public int ContestCategoryId { get; set; }
        [Required]
        public long UserId { get; set; }
        [Required]
        public int VoteTypeId { get; set; }
        public decimal? VoteAmount { get; set; }
        public int? PeriodicId { get; set; }
    }
}