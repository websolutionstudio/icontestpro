﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;

namespace IContestPro.Contest.Dto
{
    [AutoMapTo(typeof(ContestDto))]
    public class EditContestDto: FullAuditedEntityDto<int>, IMayHaveTenant
    {
        [StringLength(255)]
        [Required]
        public string Title { get; set; } 
        [StringLength(50)]
        [Required]
        public string Picture { get; set; } 
        [StringLength(2000)]
        [Required]
        public string Description { get; set; } 
        [StringLength(500)]
        [Required]
        public string Objective { get; set; } 
        public int MinimumParticipants { get; set; } 
        [Required]
        public DateTime StartDate { get; set; } 
        [Required]
        public DateTime EndDate { get; set; } 
        [Required]
        public DateTime EntryStartDate { get; set; } 
        [Required]
        public DateTime EntryEndDate { get; set; } 
        [StringLength(255)]
        public string EveryoneIsAWinnerUrl { get; set; }
        [StringLength(100)]
        public string Tags { get; set; }
        [StringLength(255)]
        public string Permalink { get; set; }
        public decimal? EntryAmount { get; set; }
        [Required]
        public int SponsorId { get; set; }
        public int ContestFeeId { get; set; }
        public DateTime? NextPaymentDate { get; set; }

        public int? TenantId { get; set; }
        public int StatusId { get; set; }

        [Required]
        public int ContestTypeId { get; set; }
        [Required]
        public int ContestEntryTypeId { get; set; }
        [Required]
        public int ContestCategoryId { get; set; }
        [Required]
        public long UserId { get; set; }
        [Required]
        public long VoteTypeId { get; set; }
        public decimal? VoteAmount { get; set; }
        public int? PeriodicId { get; set; }
      
    }
}