﻿using System;

namespace IContestPro.Contest.Dto
{
    public class ContestsEndingSoonDto
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Permalink { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
      
    }
}