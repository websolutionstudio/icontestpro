﻿using System.ComponentModel.DataAnnotations;

namespace IContestPro.Contest.Dto
{
    public class UpdateStatusDto
    {
        [Required]
        public int EntityId { get; set; }
        [Required]
        public string StatusName { get; set; }
      
    }
}