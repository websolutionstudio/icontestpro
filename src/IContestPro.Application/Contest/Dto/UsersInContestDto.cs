﻿namespace IContestPro.Contest.Dto
{
    public class UsersInContestDto
    {
        //u.Id, u.Image, u.NickName, u.Permalink
        public long Id { get; set; }
        public string Image { get; set; }
        public string NickName { get; set; }
        public string Permalink { get; set; }
      
    }
}