﻿using System;

namespace IContestPro.Contest.Dto
{
    public class ContestWithFilterDto
    {
        public int Id { get; set; }
        public string Picture { get; set; }
        public string Title { get; set; }
        public string Permalink { get; set; }
        public string Description { get; set; }
        public string SponsorName { get; set; }
        public string SponsorLogo { get; set; }
        public string SponsorPermalink { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime EntryStartDate { get; set; } 
        public DateTime EntryEndDate { get; set; } 
        public int TotalEntries { get; set; }
        public int TotalVotes { get; set; }
        public int TotalViews { get; set; }
        public int TotalCount { get; set; }

        
    }
}