﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Contest.ContestType.Dto;

namespace IContestPro.Contest.ContestFee.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.ContestFee))]
    public class ContestFeeDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public int ContestTypeId { get; set; }
        public int CodeBatch { get; set; }
        public decimal Amount { get; set; }
        public int PercentagePerMonth { get; set; }
        
        public int? TenantId { get; set; }
        
        public ContestTypeDto ContestType { get; set; }

    }
}