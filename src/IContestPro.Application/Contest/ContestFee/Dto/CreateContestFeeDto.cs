﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Contest.ContestFee.Dto
{
    [AutoMapTo(typeof(IContestProEntities.ContestFee))]
    public class CreateContestFeeDto
    {
        [Required]
        public int ContestTypeId { get; set; }
        public int CodeBatch { get; set; }
        public int PercentagePerMonth { get; set; }

        public decimal Amount { get; set; }

    }
}