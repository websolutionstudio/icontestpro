﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestFee.Dto;

namespace IContestPro.Contest.ContestFee
{
    public interface IContestFeeAppService : IBaseAppService<ContestFeeDto, int, PagedResultRequestDto, CreateContestFeeDto, ContestFeeDto>
    {
        Task<ContestFeeDto> GetByContestTypeId(int contestTypeId);
    }
}        