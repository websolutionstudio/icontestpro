﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Authorization;
using IContestPro.Base;
using IContestPro.Contest.ContestFee.Dto;

namespace IContestPro.Contest.ContestFee
{
     public class ContestFeeAppService : BaseAppService<IContestProEntities.ContestFee, ContestFeeDto, int,
                                PagedResultRequestDto, CreateContestFeeDto, ContestFeeDto>, IContestFeeAppService
    {
        private readonly IRepository<IContestProEntities.ContestFee, int> _repository;

        public ContestFeeAppService(IRepository<IContestProEntities.ContestFee, int> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }
        public override async Task<PagedResultDto<ContestFeeDto>> GetAll(PagedResultRequestDto input)
        {
            var query = _repository.GetAllIncluding(item => item.ContestType);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<ContestFeeDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<ContestFeeDto> Create(CreateContestFeeDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.ContestFee>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<ContestFeeDto> Update(ContestFeeDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.ContestFee>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }

        public async Task<ContestFeeDto> GetByContestTypeId(int contestTypeId)
        {
                return  (await _repository.FirstOrDefaultAsync(itm => itm.ContestTypeId == contestTypeId)).MapTo<ContestFeeDto>();
        }
    }
}