﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestView.Dto;

namespace IContestPro.Contest.ContestView
{
    public interface IContestViewAppService : IBaseAppService<ContestViewDto, long, PagedResultRequestDto, CreateContestViewDto, ContestViewDto>
    {
        Task<int> GetViewsByContestId(int contestId);
    }
}