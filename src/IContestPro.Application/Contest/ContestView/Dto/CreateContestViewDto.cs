﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Contest.ContestView.Dto
{
    [AutoMapTo(typeof(IContestProEntities.ContestView))]
    public class CreateContestViewDto
    {
        public long? UserId { get; set; }
        [Required]
        public int ContestId { get; set; }
    }
}