﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;

namespace IContestPro.Contest.ContestView.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.ContestView))]
    public class ContestViewDto : FullAuditedEntityDto<long>, IMayHaveTenant
    {
        public long? UserId { get; set; }
        public int ContestId { get; set; }
        public int? TenantId { get; set; }
    }
}