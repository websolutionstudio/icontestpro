﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Base;
using IContestPro.Contest.ContestView.Dto;

namespace IContestPro.Contest.ContestView
{
    public class ContestViewAppService : BaseAppService<IContestProEntities.ContestView, ContestViewDto, long,
                                PagedResultRequestDto, CreateContestViewDto, ContestViewDto>, IContestViewAppService
    {
        private readonly IRepository<IContestProEntities.ContestView, long> _repository;

        public ContestViewAppService(IRepository<IContestProEntities.ContestView, long> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }
        public override async Task<ContestViewDto> Create(CreateContestViewDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.ContestView>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        
        [AbpAuthorize]
        public override async Task<ContestViewDto> Update(ContestViewDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.ContestView>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }

        public async Task<int> GetViewsByContestId(int contestId)
        {
            return await _repository.CountAsync(item => item.ContestId == contestId);
        }
    }
}