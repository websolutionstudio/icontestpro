﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using Castle.Components.DictionaryAdapter;
using Hangfire;
using IContestPro.Authorization;
using IContestPro.Authorization.Users;
using IContestPro.BackgroundJobs;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestEntry.Dto;
using IContestPro.Contest.ContestEntryComment;
using IContestPro.Contest.ContestEntryShare;
using IContestPro.Contest.ContestEntryView;
using IContestPro.Contest.ContestVote;
using IContestPro.Contest.Dto;
using IContestPro.EntityFrameworkCore.Seed;
using IContestPro.Helpers;
using IContestPro.PaymentHistories;
using IContestPro.PaymentHistories.Dto;
using IContestPro.Referral;
using IContestPro.Sponsor;
using IContestPro.VoteType;
using ImageMagick;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using SendGrid.Helpers.Mail;
using Xabe.FFmpeg;
using Xabe.FFmpeg.Enums;

namespace IContestPro.Contest.ContestEntry
{
    public class ContestEntryAppService : BaseAppService<IContestProEntities.ContestEntry, ContestEntryDto, int,
                                PagedResultRequestDto, CreateContestEntryDto, ContestEntryDto>, IContestEntryAppService
    {
        private readonly IRepository<IContestProEntities.ContestEntry> _repository;
        private readonly IRepository<IContestProEntities.Contest> _contestRepository;
        private readonly IRepository<IContestProEntities.ContestEntryType> _contestEntryTypRepository;
        private readonly IRepository<IContestProEntities.Status> _statusRepository;
        private readonly IContestEntryCommentAppService _contestEntryCommentAppService;
        private readonly IContestEntryVoteAppService _contestEntryVoteAppService;
        private readonly IVoteTypeAppService _voteTypeAppService;
        private readonly IContestEntryViewAppService _contestEntryViewAppService;
        private readonly IContestEntryShareAppService _contestEntryShareAppService;
        private readonly UserManager _userManager;
        private readonly ISponsorAppService _sponsorAppService;
        private readonly IRepository<IContestProEntities.Sponsor> _companyRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IRepository<IContestProEntities.Wallet> _walletRepository;
        private readonly IReferralAppService _referralAppService;
        private readonly IPaymentHistoriesAppService _paymentHistoriesAppService;


        public ContestEntryAppService(IRepository<IContestProEntities.ContestEntry> repository, 
            IRepository<IContestProEntities.Contest> contestRepository, IContestEntryCommentAppService contestEntryCommentAppService, 
            IContestEntryVoteAppService contestEntryVoteAppService, IRepository<IContestProEntities.Status> statusRepository, 
            UserManager userManager, IRepository<IContestProEntities.ContestEntryType> contestEntryTypRepositorye,
            IVoteTypeAppService voteTypeAppService, ISponsorAppService sponsorAppService, 
            IContestEntryViewAppService contestEntryViewAppService, IContestEntryShareAppService contestEntryShareAppService,
            IRepository<IContestProEntities.Sponsor> companyRepository, IHostingEnvironment hostingEnvironment,
            IRepository<IContestProEntities.Wallet> walletRepository, IReferralAppService referralAppService,
            IIocResolver  iocResolver, IPaymentHistoriesAppService paymentHistoriesAppService) : base(repository, iocResolver)
        {
            _repository = repository;
            _contestRepository = contestRepository;
            _contestEntryCommentAppService = contestEntryCommentAppService;
            _contestEntryVoteAppService = contestEntryVoteAppService;
            _statusRepository = statusRepository;
            _userManager = userManager;
            _contestEntryTypRepository = contestEntryTypRepositorye;
            _voteTypeAppService = voteTypeAppService;
            _sponsorAppService = sponsorAppService;
            _contestEntryViewAppService = contestEntryViewAppService;
            _contestEntryShareAppService = contestEntryShareAppService;
            _companyRepository = companyRepository;
            _hostingEnvironment = hostingEnvironment;
            _walletRepository = walletRepository;
            _referralAppService = referralAppService;
            _paymentHistoriesAppService = paymentHistoriesAppService;
        }
        [AbpAuthorize]
        public override async Task<ContestEntryDto> Create(CreateContestEntryDto input)
        {
            decimal accountBalanceBefore = 0;
            var contest = await _contestRepository.GetAllIncluding(itm => itm.ContestType, itm => itm.Status)
                .Select(itm => new{itm.Id,itm.StatusId,itm.Status,itm.ContestTypeId, 
                    itm.ContestType, itm.Title, itm.EntryAmount, itm.UserId})
                            .FirstOrDefaultAsync(cont => cont.Id == input.ContestId);
            if (contest == null)
            {
                throw new UserFriendlyException("Contest was not found.");
            } 
            if (contest.UserId == input.UserId)
            {
                throw new UserFriendlyException("You cannot join the contest you created.");
            }
            if (contest.Status.Name != AppConsts.StatusLive)
            {
                throw new UserFriendlyException("Entry cannot be created on a contest that is not live.");
            }
            var wallet = await _walletRepository.FirstOrDefaultAsync(w => w.UserId == input.UserId);
            accountBalanceBefore = wallet.Balance;
            IContestProEntities.Status status; 
                    try
                    {
           
            if (contest.ContestType.Name == AppConsts.ContestTypePaid)
            {
                 status = await _statusRepository.GetAll()
                    .FirstOrDefaultAsync(stat => stat.Name == AppConsts.StatusPendingPaymentConfirmation);
                var contestAmount = contest.EntryAmount ?? 0;
                if (wallet.Balance < contestAmount)
                {
                    throw new UserFriendlyException("Insufficient balance. Please add fund to your wallet to complete your entry.");
                }

                wallet.Balance -= contestAmount;
                var statusSuccess = StatusSeed.GetItems().First(itm => itm.Value == AppConsts.StatusSuccess);
                await Task.Run(async ()  => 
                {
                    await _paymentHistoriesAppService.Create(new CreatePaymentHistoryDto
                    {
                        Reference =  Guid.NewGuid().ToString("N"),
                        Amount = contest.EntryAmount,
                        Description = $"A occurred in your account for the creation of your entry on '{contest.Title}' contest. Thank you.",
                        Title = "Contest entry creation debit",
                        UserId = input.UserId,
                        WalletOperation = WalletOperation.Debit,
                        StatusId = statusSuccess.Key
                
                    }); 
                });
              
            }
            else
            {
                status = await _statusRepository.GetAll()
                    .FirstOrDefaultAsync(stat => stat.Name == AppConsts.StatusPending);
            }
           
            input.StatusId = status.Id;
            var permalink = contest.Title.Substring(0, contest.Title.Length > 100 ? 100 : contest.Title.Length)+ "-entry-" ;
            input.Permalink =  permalink.GeneratePermalink();
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.ContestEntry>());
            CurrentUnitOfWork.SaveChanges();
            if (item != null && item.Id > 0)
            {
                item.Permalink = permalink.GeneratePermalinkWithId(item.Id);
                await Task.Run(async () =>
                {
                    if (await _referralAppService.isUserReferred(input.UserId))
                    {
                        var referrer = await _referralAppService.CreditUserReferrerVotePointAward(input.UserId);
                    } 
                });
                CurrentUnitOfWork.SaveChanges();
                
                var body =
                    $"Thank you for joining '{contest.Title}' contest. You can get more votes and visibility by promoting your entry on the platform.";
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = input.UserId,
                    Message = body
                });
                BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                    (
                        new SendEmailDto
                        {
                            ShouldUseDefaultEmailTemplate = true,
                            Tos = new List<EmailAddress>{new EmailAddress(CurrentUser.EmailAddress, CurrentUser.NickName)},
                            Subject = $"Thank you for joining your '{contest.Title}' contest",
                            Body = body
                        })
                );
               

                var adminUser = await _userManager.FindByEmailAsync(AppConsts.DefaultTenantAdminEmail);
                var bodyAdmin =
                    $"The user '{CurrentUser.NickName}' joined the contest '{contest.Title}'. Thank you.";
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = adminUser.Id,
                    Message = bodyAdmin
                });
                BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                    (
                        new SendEmailDto
                        {
                            ShouldUseDefaultEmailTemplate = true,
                            Tos = new List<EmailAddress>{new EmailAddress(adminUser.EmailAddress, adminUser.NickName)},
                            Subject = "A user joined a contest",
                            Body = bodyAdmin
                        })
                );
               
                var contestnOwner = await _userManager.FindByIdAsync(contest.UserId.ToString());
                var bodyOwner =
                    $"The user '{CurrentUser.NickName}' joined your contest '{contest.Title}'. Please verify and make the entry live. Thank you.";
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = contestnOwner.Id,
                    Message = bodyOwner
                });
                BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                    (
                        new SendEmailDto
                        {
                            ShouldUseDefaultEmailTemplate = true,
                            Tos = new List<EmailAddress>{new EmailAddress(contestnOwner.EmailAddress, contestnOwner.NickName)},
                            Subject = $"A user joined your contest '{contest.Title}'",
                            Body = bodyOwner
                        })
                );
               
            }
                        return MapToEntityDto(item);
                    }
                    catch (Exception e)
                    {
                        var walletOnError = await _walletRepository.FirstOrDefaultAsync(w => w.UserId == input.UserId);
                        if (accountBalanceBefore != walletOnError.Balance)
                        {
                            var difference = accountBalanceBefore - walletOnError.Balance;
                            walletOnError.Balance += difference;
                           await CurrentUnitOfWork.SaveChangesAsync();
                            var body =
                                $"A reversal credit occurred in your account as a result of error in creating your entry on '{contest.Title}' contest.";
                            await Task.Run(async ()  => 
                            {
                                var statusFailure = StatusSeed.GetItems().First(itm => itm.Value == AppConsts.StatusFailed);
                                await _paymentHistoriesAppService.Create(new CreatePaymentHistoryDto
                                {
                                    Reference =  Guid.NewGuid().ToString("N"),
                                    Amount = contest.EntryAmount,
                                    Description = body,
                                    Title = "Contest entry creation reversal credit",
                                    UserId = input.UserId,
                                    WalletOperation = WalletOperation.Debit,
                                    StatusId = statusFailure.Key
                
                                }); 
                            });
                            await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                            {
                                UserId = input.UserId,
                                Message = body
                            });
                        }
                        throw new UserFriendlyException(e.Message);
                    }
           
        }
        
        [AbpAuthorize]
        public override async Task<ContestEntryDto> Update(ContestEntryDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.ContestEntry>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        public async Task<PagedResultDto<ContestEntryDto>> GetAllByContestId(int contestId, PagedResultRequestDto input)
        {
            var query = CreateFilteredQuery(input);
            query = query.Where(cont => cont.ContestId == contestId);
            query = query.OrderByDescending(item => item.CreationTime);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<ContestEntryDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        public async Task<PagedResultDto<ContestEntryDetailsDto>> GetAllByContestIdWithDetails(int contestId, PagedResultRequestDto input)
        {
            var query =  _repository.GetAllIncluding(itm => itm.Status, itm => itm.User)
                         .Where(item => item.ContestId == contestId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            var items = new List<ContestEntryDetailsDto>();
            foreach (var entity in entityDtos)
            {
                var contest = await _contestRepository.GetAllIncluding(itm => itm.ContestEntryType).FirstOrDefaultAsync(cont => cont.Id == entity.ContestId);
                entity.Contest = contest.MapTo<ContestDto>();
                var totalComments = await _contestEntryCommentAppService.CountByContestEntryId(entity.Id);
                var totalVotes = await _contestEntryVoteAppService.CountByContestEntryId(entity.Id);
                var voteType = await _voteTypeAppService.Get(new EntityDto{Id = entity.Contest.VoteTypeId});
                var totalViews = await _contestEntryViewAppService.GetViewsByContestEntryId(entity.Id);
                var totalShares = await _contestEntryShareAppService.GetSharesByContestEntryId(entity.Id);
                var item = new ContestEntryDetailsDto
                {
                    ContestEntry = entity,
                    TotalComments = totalComments,
                    TotalVotes = totalVotes,
                    VoteType = voteType,
                    TotalViews = totalViews,
                    TotalShares = totalShares,
                    VoteAmount =  entity.Contest.VoteAmount
                };
                items.Add(item);
            }

            return new PagedResultDto<ContestEntryDetailsDto>(
                totalCount,
                items
                
            );
        }

        public async Task<PagedResultDto<ContestEntryDto>> GetAllByContestPermalink(string permalink, PagedResultRequestDto input)
        {
            var contest = await _contestRepository.GetAll().Select(itm => new {itm.Id,itm.StatusId,itm.Status, itm.Permalink})
                .FirstOrDefaultAsync(item => item.Permalink == permalink);
            if (contest == null)
            {
                throw new UserFriendlyException("Contest not found.");
            }
           
            var query = CreateFilteredQuery(input);
            query = query.Where(cont => cont.ContestId == contest.Id);
            query = query.OrderByDescending(item => item.CreationTime);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<ContestEntryDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }

        public async Task<PagedResultDto<ContestEntryDetailsDto>> GetAllByContestPermalinkWithDetails(string permalink, PagedResultRequestDto input)
        {
            var contest = await _contestRepository.GetAllIncluding(item => item.Status).Select(itm => new {itm.Id,itm.StatusId,itm.Status, itm.Permalink})
                .FirstOrDefaultAsync(item => item.Permalink == permalink);
            if (contest == null)
            {
                throw new UserFriendlyException("Contest not found.");
            }
            if (contest.Status.Name != AppConsts.StatusLive)
            {
                throw new UserFriendlyException("Contest not live.");
            }
            var query =  _repository.GetAllIncluding(itm => itm.Status, itm => itm.User)
                .Where(item => item.ContestId == contest.Id);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            var items = new List<ContestEntryDetailsDto>();
            foreach (var entity in entityDtos)
            {
                var contestIn = await _contestRepository.GetAllIncluding(itm => itm.ContestEntryType).FirstOrDefaultAsync(cont => cont.Id == entity.ContestId);
                entity.Contest = contestIn.MapTo<ContestDto>();
                var totalComments = await _contestEntryCommentAppService.CountByContestEntryId(entity.Id);
                var totalVotes = await _contestEntryVoteAppService.CountByContestEntryId(entity.Id);
                var voteType = await _voteTypeAppService.Get(new EntityDto{Id = entity.Contest.VoteTypeId});
                var totalViews = await _contestEntryViewAppService.GetViewsByContestEntryId(entity.Id);
                var totalShares = await _contestEntryShareAppService.GetSharesByContestEntryId(entity.Id);
                var item = new ContestEntryDetailsDto
                {
                    ContestEntry = entity,
                    TotalComments = totalComments,
                    TotalVotes = totalVotes,
                    VoteType = voteType,
                    TotalViews = totalViews,
                    TotalShares = totalShares,
                    VoteAmount =  entity.Contest.VoteAmount
                };
                items.Add(item);
            }

            return new PagedResultDto<ContestEntryDetailsDto>(
                totalCount,
                items
                
            );
        }

        public async Task<bool> IsUserInContest(long userId, int contestId)
        {
            return await _repository.FirstOrDefaultAsync(item => item.UserId == userId &&
                                                         item.ContestId == contestId) != null;
        }

        public async Task<bool> IsCurrentUserInContest(int contestId)
        {
            return await IsUserInContest(AbpSession.GetUserId(), contestId);
        }

        [AbpAuthorize]
        public async Task<bool> IsUserContestOwner(int contestId)
        {
            return await _contestRepository.FirstOrDefaultAsync(item => item.UserId == AbpSession.GetUserId() &&
                                                                 item.Id == contestId) != null;
        }

        public async Task<PagedResultDto<ActiveContestEntryDto>> GetActiveContestEntries(PagedResultRequestDto input)
        {
           
            var contestStatus =  await _statusRepository.GetAll().Select(stat => new {stat.Id, stat.Name})
                                .FirstOrDefaultAsync(stat => stat.Name == AppConsts.StatusLive);
            
            var status =  await _statusRepository.GetAll().Select(stat => new {stat.Id, stat.Name})
                                .FirstOrDefaultAsync(stat => stat.Name == AppConsts.StatusActive);
            var contestIds = await _contestRepository.GetAll().Where(item => item.StatusId == contestStatus.Id && item.StartDate < DateTime.Now && item.EndDate > DateTime.Now).Select(itm => itm.Id).ToArrayAsync();
            if (contestIds == null)
            {
                throw new UserFriendlyException("There is no live contest.");
            }
            
            var query = _repository.GetAll()
                            .Select(item => 
                                new IContestProEntities.ContestEntry {Id = item.Id, Image = item.Image, Poster = item.Poster,ContestId = item.ContestId, 
                                    UserId = item.UserId, CreationTime = item.CreationTime, StatusId = item.StatusId,
                                    Permalink  = item.Permalink, Video = item.Video})
                
                                    .Where(item => item.StatusId == status.Id && contestIds.Contains(item.ContestId));
            query = query.OrderByDescending(item => item.CreationTime);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            var items = new List<ActiveContestEntryDto>();
            foreach (var entity in entityDtos)
            {
                var totalComments = await _contestEntryCommentAppService.CountByContestEntryId(entity.Id);
                var totalVotes = await _contestEntryVoteAppService.CountByContestEntryId(entity.Id);
                var user = await _userManager.Users.Include(usr => usr.State).Include(usr => usr.Country)
                    .Select(usr => new {usr.Id, usr.NickName, usr.State, usr.Country, usr.StateId, 
                        usr.CountryId, usr.Image})
                    .FirstOrDefaultAsync(usr => usr.Id == entity.UserId);
                
                var contest = await _contestRepository.GetAllIncluding(cnt =>cnt.VoteType)
                    .Select(itm => new {itm.Id, itm.VoteType,itm.VoteTypeId, itm.VoteAmount, 
                                        itm.Title, itm.ContestEntryTypeId, itm.Permalink, itm.UserId, 
                                        itm.ContestType, itm.EntryAmount, itm.StartDate, itm.EndDate, itm.EntryStartDate, itm.EntryEndDate})
                    .FirstOrDefaultAsync(itm => itm.Id == entity.ContestId);
                var contestEntryType = await _contestEntryTypRepository.GetAll()
                                                    .Select(itm => new {itm.Id, itm.Name})
                                                    .FirstOrDefaultAsync(itm => itm.Id == contest.ContestEntryTypeId);
                var totalViews = await _contestEntryViewAppService.GetViewsByContestEntryId(entity.Id);
                var totalShares = await _contestEntryShareAppService.GetSharesByContestEntryId(entity.Id);
                var company = await _companyRepository.GetAll()
                    .Select(itm => new {itm.Logo, itm.UserId, itm.Name})
                    .FirstOrDefaultAsync(itm => itm.UserId == contest.UserId);
                var item = new ActiveContestEntryDto
                {
                    Id = entity.Id,
                    Poster = entity.Image ?? entity.Poster,
                    Video = entity.Video,
                    Permalink = entity.Permalink,
                    ContestPermalink = contest.Permalink,
                    UserId = entity.UserId,
                    CompanyName = company.Name,
                    CompanyLogo = company.Logo,
                    UserImage = user.Image,
                    NickName = user.NickName,
                    UserLocation = user.State != null ? user.State.Name + ", " + user.Country?.Name : user.Country.Name,
                    TotalComments = totalComments,
                    VoteType = contest.VoteType.Name,
                    VoteAmount = contest.VoteAmount,
                    VotePoint = contest.VoteType.VotePoint,
                    TotalVotes = totalVotes,
                    ContestId = entity.ContestId,
                    ContestTitle = contest.Title,
                    ContestType = contest.ContestType.Name,
                    EntryAmount = contest.EntryAmount,
                    ContestEntryType = contestEntryType.Name,
                    TotalViews = totalViews,
                    TotalShares = totalShares,
                    StartDate = contest.StartDate,
                    EndDate = contest.EndDate,
                    EntryStartDate = contest.EntryStartDate,
                    EntryEndDate = contest.EntryEndDate,
                };
                items.Add(item);
            }

            return new PagedResultDto<ActiveContestEntryDto>(
                totalCount,
                items
                
            );
        }
       
        public async Task<PagedResultDto<ActiveContestEntryDto>> GetEntriesByContestPermalink(string permalink, PagedResultRequestDto input)
        {
            
           var status =  await _statusRepository.GetAll().Select(stat => new {stat.Id, stat.Name})
                                .FirstOrDefaultAsync(stat => stat.Name == AppConsts.StatusActive);
            var contest = await _contestRepository.GetAllIncluding(cnt =>cnt.VoteType, cnt =>cnt.Status)
                .Select(itm => new {itm.Id, itm.StatusId, itm.Status, itm.VoteType,itm.VoteTypeId, 
                    itm.VoteAmount, itm.Title, itm.ContestEntryTypeId, itm.Permalink, itm.UserId, 
                    itm.ContestType, itm.EntryAmount, itm.StartDate, itm.EndDate,itm.EntryStartDate, itm.EntryEndDate})
                .FirstOrDefaultAsync(itm => itm.Permalink == permalink);
           
            if (contest == null)
            {
                throw new UserFriendlyException("Contest not found.");
            }
            
            if (status == null)
            {
                return null;
            }
            var query = _repository.GetAll()
                            .Select(item => 
                                new IContestProEntities.ContestEntry {Id = item.Id, Image = item.Image, Poster = item.Poster,ContestId = item.ContestId, 
                                    UserId = item.UserId, CreationTime = item.CreationTime, StatusId = item.StatusId,
                                    Permalink  = item.Permalink, Video = item.Video})
                                    .Where(item => item.StatusId == status.Id && item.ContestId == contest.Id);
            query = query.OrderByDescending(item => item.CreationTime);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            var items = new List<ActiveContestEntryDto>();
            foreach (var entity in entityDtos)
            {
                var totalComments = await _contestEntryCommentAppService.CountByContestEntryId(entity.Id);
                var totalVotes = await _contestEntryVoteAppService.CountByContestEntryId(entity.Id);
                var user = await _userManager.Users.Include(usr => usr.State).Include(usr => usr.Country)
                    .Select(usr => new {usr.Id, usr.NickName, usr.State, usr.Country, 
                                        usr.StateId, usr.CountryId, usr.Image})
                                    .FirstOrDefaultAsync(usr => usr.Id == entity.UserId);
               

                var contestEntryType = await _contestEntryTypRepository.GetAll()
                                                    .Select(itm => new {itm.Id, itm.Name})
                                                    .FirstOrDefaultAsync(itm => itm.Id == contest.ContestEntryTypeId);
                var totalViews = await _contestEntryViewAppService.GetViewsByContestEntryId(entity.Id);
                var totalShares = await _contestEntryShareAppService.GetSharesByContestEntryId(entity.Id);
                var company = await _companyRepository.GetAll()
                    .Select(itm => new {itm.Logo, itm.UserId, itm.Name})
                    .FirstOrDefaultAsync(itm => itm.UserId == contest.UserId);
                var item = new ActiveContestEntryDto
                {
                    Id = entity.Id,
                    Poster = entity.Image ?? entity.Poster,
                    Video = entity.Video,
                    Permalink = entity.Permalink,
                    ContestPermalink = contest.Permalink,
                    UserId = entity.UserId,
                    CompanyName = company.Name,
                    CompanyLogo = company.Logo,
                    UserImage = user.Image,
                    NickName = user.NickName,
                    UserLocation = user.State != null ? user.State.Name + ", " + user.Country?.Name : user.Country.Name,
                    TotalComments = totalComments,
                    VoteType = contest.VoteType.Name,
                    VoteAmount = contest.VoteAmount,
                    VotePoint = contest.VoteType.VotePoint,
                    TotalVotes = totalVotes,
                    ContestId = entity.ContestId,
                    ContestTitle = contest.Title,
                    ContestType = contest.ContestType.Name,
                    EntryAmount = contest.EntryAmount,
                    ContestEntryType = contestEntryType.Name,
                    TotalViews = totalViews,
                    TotalShares = totalShares,
                    StartDate = contest.StartDate,
                    EndDate = contest.EndDate,
                    EntryStartDate = contest.EntryStartDate,
                    EntryEndDate = contest.EntryEndDate
                };
                items.Add(item);
            }

            return new PagedResultDto<ActiveContestEntryDto>(
                totalCount,
                items
                
            );
        }
        public async Task<ContestDetailsEntryDetailsDto> GetEntriesAndContestDetailsByContestPermalink(string permalink, PagedResultRequestDto input)
        {
            
           
            var contest = await _contestRepository.GetAllIncluding(cnt =>cnt.VoteType)
                .Select(itm => new {itm.Id, itm.StatusId, itm.VoteType,itm.VoteTypeId, 
                    itm.VoteAmount, itm.Title, itm.ContestEntryTypeId, itm.Permalink, itm.UserId, 
                    itm.ContestType, itm.EntryAmount, itm.StartDate, itm.EndDate,itm.EntryStartDate, itm.EntryEndDate})
                .FirstOrDefaultAsync(itm => itm.Permalink == permalink);
           
            if (contest == null)
            {
                throw new UserFriendlyException("Contest not found.");
            }
            
            var status =  await _statusRepository.GetAll().Select(stat => new {stat.Id, stat.Name})
                .FirstOrDefaultAsync(stat => stat.Id == contest.StatusId);
            if (status == null)
            {
                return null;
            }
            var entryStatus =  await _statusRepository.GetAll().Select(stat => new {stat.Id, stat.Name})
                .FirstOrDefaultAsync(stat => stat.Name == AppConsts.StatusActive);

            var query = _repository.GetAll()
                            .Select(item => 
                                new IContestProEntities.ContestEntry {Id = item.Id, Image = item.Image, Poster = item.Poster,ContestId = item.ContestId, 
                                    UserId = item.UserId, CreationTime = item.CreationTime, StatusId = item.StatusId,
                                    Permalink  = item.Permalink, Video = item.Video})
                                    .Where(item => item.StatusId == entryStatus.Id && item.ContestId == contest.Id);
            query = query.OrderByDescending(item => item.CreationTime);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            var items = new List<ActiveContestEntryDto>();
            foreach (var entity in entityDtos)
            {
                var totalComments = await _contestEntryCommentAppService.CountByContestEntryId(entity.Id);
                var totalVotes = await _contestEntryVoteAppService.CountByContestEntryId(entity.Id);
                var user = await _userManager.Users.Include(usr => usr.State).Include(usr => usr.Country)
                    .Select(usr => new {usr.Id, usr.NickName, usr.State, usr.Country, 
                                        usr.StateId, usr.CountryId, usr.Image})
                                    .FirstOrDefaultAsync(usr => usr.Id == entity.UserId);
               

                var contestEntryType = await _contestEntryTypRepository.GetAll()
                                                    .Select(itm => new {itm.Id, itm.Name})
                                                    .FirstOrDefaultAsync(itm => itm.Id == contest.ContestEntryTypeId);
                var totalViews = await _contestEntryViewAppService.GetViewsByContestEntryId(entity.Id);
                var totalShares = await _contestEntryShareAppService.GetSharesByContestEntryId(entity.Id);
                var company = await _companyRepository.GetAll()
                    .Select(itm => new {itm.Logo, itm.UserId, itm.Name})
                    .FirstOrDefaultAsync(itm => itm.UserId == contest.UserId);
                var item = new ActiveContestEntryDto
                {
                    Id = entity.Id,
                    Poster = entity.Image ?? entity.Poster,
                    Video = entity.Video,
                    Permalink = entity.Permalink,
                    ContestPermalink = contest.Permalink,
                    UserId = entity.UserId,
                    CompanyName = company.Name,
                    CompanyLogo = company.Logo,
                    UserImage = user.Image,
                    NickName = user.NickName,
                    UserLocation = user.State != null ? user.State.Name + ", " + user.Country?.Name : user.Country.Name,
                    TotalComments = totalComments,
                    VoteType = contest.VoteType.Name,
                    VoteAmount = contest.VoteAmount,
                    VotePoint = contest.VoteType.VotePoint,
                    TotalVotes = totalVotes,
                    ContestId = entity.ContestId,
                    ContestTitle = contest.Title,
                    ContestType = contest.ContestType.Name,
                    EntryAmount = contest.EntryAmount,
                    ContestEntryType = contestEntryType.Name,
                    TotalViews = totalViews,
                    TotalShares = totalShares,
                    StartDate = contest.StartDate,
                    EndDate = contest.EndDate,
                    EntryStartDate = contest.EntryStartDate,
                    EntryEndDate = contest.EntryEndDate
                };
                items.Add(item);
            }

            var pagedItems = new PagedResultDto<ActiveContestEntryDto>(
                totalCount,
                items
                
            );
            return new ContestDetailsEntryDetailsDto
            {
                Id = contest.Id,
                Title = contest.Title,
                Permalink = contest.Permalink,
                EntryStartDate = contest.EntryStartDate,
                EntryEndDate = contest.EntryEndDate,
                StartDate = contest.StartDate,
                EndDate = contest.EndDate,
                StatusName = status.Name,
                Entries = pagedItems
            };
        }


        public async Task<int> CountByContestId(int contestId)
        {
            return await _repository.GetAll().CountAsync(comm => comm.ContestId == contestId);
        }

        public async Task<int> CountActiveEntriesByContestId(int contestId)
        {
            var status =  await _statusRepository.GetAll().Select(stat => new {stat.Id, stat.Name})
                .FirstOrDefaultAsync(stat => stat.Name == AppConsts.StatusActive);
            return await _repository.GetAll().CountAsync(comm => comm.ContestId == contestId && comm.StatusId == status.Id);
        }

        public async Task<int> CountByUserId(long userId)
        {
            return await _repository.CountAsync(item => item.UserId == userId);
        }

         public async Task<PagedResultDto<ContestEntryDto>> GetAllByStatus(string statusName, PagedResultRequestDto input)
        {
            var status =  await _statusRepository.GetAll().Select(stat => new {stat.Id, stat.Name})
                .FirstOrDefaultAsync(stat => stat.Name == statusName);
            if (status == null)
            {
                return new PagedResultDto<ContestEntryDto>(
                    0,
                    new ContestEntryDto[]{}
                );
            }
            var query =  CreateFilteredQuery(input);
             query =  query.Where(item => item.StatusId == status.Id);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);

            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<ContestEntryDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        public async Task<PagedResultDto<ContestEntryDetailsDto>> GetAllByStatusWithDetails(string statusName, PagedResultRequestDto input)
        {
            var status =  await _statusRepository.GetAll().Select(stat => new {stat.Id, stat.Name})
                .FirstOrDefaultAsync(stat => stat.Name == statusName);
            if (status == null)
            {
                return new PagedResultDto<ContestEntryDetailsDto>(
                    0,
                    new ContestEntryDetailsDto[]{}
                );
            }
            var query =  _repository.GetAllIncluding(itm => itm.Status, itm => itm.User)
                .Where(item => item.StatusId == status.Id);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            var items = new List<ContestEntryDetailsDto>();
            foreach (var entity in entityDtos)
            {
                var contestIn = await _contestRepository.GetAllIncluding(itm => itm.ContestEntryType).FirstOrDefaultAsync(cont => cont.Id == entity.ContestId);
                entity.Contest = contestIn.MapTo<ContestDto>();
                var totalComments = await _contestEntryCommentAppService.CountByContestEntryId(entity.Id);
                var voteType = await _voteTypeAppService.Get(new EntityDto{Id = entity.Contest.VoteTypeId});
                var totalVotes = await _contestEntryVoteAppService.CountByContestEntryId(entity.Id);
                var totalViews = await _contestEntryViewAppService.GetViewsByContestEntryId(entity.Id);
                var totalShares = await _contestEntryShareAppService.GetSharesByContestEntryId(entity.Id);
                var item = new ContestEntryDetailsDto
                {
                    ContestEntry = entity,
                    TotalComments = totalComments,
                    TotalVotes = totalVotes,
                    VoteType = voteType,
                    TotalViews = totalViews,
                    TotalShares = totalShares,
                    VoteAmount =  entity.Contest.VoteAmount
                };
                items.Add(item);
            }
           

            return new PagedResultDto<ContestEntryDetailsDto>(
                totalCount,
                items
                
            );
        }

        public async Task<ContestEntryDetailsDto> GetWithDetails(EntityDto<int> input)
        {
            var query = await _repository.GetAllIncluding(itm => itm.Status, itm => itm.User)
                .FirstOrDefaultAsync(cont => cont.Id == input.Id);
            if (query == null)
            {
                return null;
            }
            var entity = MapToEntityDto(query);
            var contestIn = await _contestRepository.GetAllIncluding(itm => itm.ContestEntryType).FirstOrDefaultAsync(cont => cont.Id == entity.ContestId);
            entity.Contest = contestIn.MapTo<ContestDto>();
            var totalComments = await _contestEntryCommentAppService.CountByContestEntryId(query.Id);
            var totalVotes = await _contestEntryVoteAppService.CountByContestEntryId(query.Id);
            var voteType = await _voteTypeAppService.Get(new EntityDto{Id = entity.Contest.VoteTypeId});
            var company = await _sponsorAppService.GetByUserId(query.Contest.UserId);
            var totalViews = await _contestEntryViewAppService.GetViewsByContestEntryId(entity.Id);
            var totalShares = await _contestEntryShareAppService.GetSharesByContestEntryId(entity.Id);
            var item = new ContestEntryDetailsDto
            {
                ContestEntry = entity,
                TotalComments = totalComments,
                TotalVotes = totalVotes,
                VoteType = voteType,
                VoteAmount =  entity.Contest.VoteAmount,
                Sponsor = company,
                TotalViews = totalViews,
                TotalShares = totalShares
            };
            return item;
        }

        public async Task<ContestEntryDto> GetByPermalink(string permalink)
        {
            var query = await _repository
                .FirstOrDefaultAsync(cont => cont.Permalink == permalink);
            return MapToEntityDto(query);
        }

        public async Task<ContestEntryDetailsDto> GetByPermalinkWithDetails(string permalink, bool isAdmin = false)
        {
            IContestProEntities.ContestEntry query;
            if (!isAdmin)
            {
                var status =  await _statusRepository.GetAll().Select(stat => new {stat.Id, stat.Name})
                    .FirstOrDefaultAsync(stat => stat.Name == AppConsts.StatusActive);
                 query = await _repository.GetAllIncluding(itm => itm.Status, itm => itm.User)
                    .FirstOrDefaultAsync(cont => cont.Permalink == permalink && cont.StatusId == status.Id);
                if (query == null)
                {
                    return null;
                } 
            }
            else
            {
                 query = await _repository.GetAllIncluding(itm => itm.Status, itm => itm.User)
                    .FirstOrDefaultAsync(cont => cont.Permalink == permalink);
               
                if (query == null)
                {
                    return null;
                }
              
            }
            

            var entity = MapToEntityDto(query);
            var contest = await _contestRepository.GetAllIncluding(itm => itm.ContestEntryType).FirstOrDefaultAsync(cont => cont.Id == entity.ContestId);
            entity.Contest = contest.MapTo<ContestDto>();
            var totalComments = await _contestEntryCommentAppService.CountByContestEntryId(query.Id);
            var totalVotes = await _contestEntryVoteAppService.CountByContestEntryId(query.Id);
            var voteType = await _voteTypeAppService.Get(new EntityDto{Id = contest.VoteTypeId});
            var company = await _sponsorAppService.GetByUserId(contest.UserId);
            
            var totalViews = await _contestEntryViewAppService.GetViewsByContestEntryId(entity.Id);
            var totalShares = await _contestEntryShareAppService.GetSharesByContestEntryId(entity.Id);
            var item = new ContestEntryDetailsDto
            {
                ContestEntry = entity,
                TotalComments = totalComments,
                TotalVotes = totalVotes,
                VoteType = voteType,
                VoteAmount =  entity.Contest.VoteAmount,
                Sponsor = company,
                TotalViews = totalViews,
                TotalShares = totalShares
            };
            return item;
        }

        public async Task<PagedResultDto<ContestEntryDetailsDto>> GetAllWithDetails(PagedResultRequestDto input)
        {
           
            var query =  _repository.GetAllIncluding(itm => itm.Status, itm => itm.User );
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            var items = new List<ContestEntryDetailsDto>();
            foreach (var entity in entityDtos)
            {
                var contestIn = await _contestRepository.GetAllIncluding(itm => itm.ContestEntryType).FirstOrDefaultAsync(cont => cont.Id == entity.ContestId);
                entity.Contest = contestIn.MapTo<ContestDto>();
                var totalComments = await _contestEntryCommentAppService.CountByContestEntryId(entity.Id);
                var totalVotes = await _contestEntryVoteAppService.CountByContestEntryId(entity.Id);
                var voteType = await _voteTypeAppService.Get(new EntityDto{Id = entity.Contest.VoteTypeId});
                var totalViews = await _contestEntryViewAppService.GetViewsByContestEntryId(entity.Id);
                var totalShares = await _contestEntryShareAppService.GetSharesByContestEntryId(entity.Id);
                var item = new ContestEntryDetailsDto
                {
                    ContestEntry = entity,
                    TotalComments = totalComments,
                    TotalVotes = totalVotes,
                    VoteType = voteType,
                    VoteAmount =  entity.Contest.VoteAmount,
                    TotalViews = totalViews,
                    TotalShares = totalShares
                };
                items.Add(item);
            }
            return new PagedResultDto<ContestEntryDetailsDto>(
                totalCount,
                items
                
            );
        }

        public async Task<PagedResultDto<ContestEntryDto>> GetAllByUserId(long userId, PagedResultRequestDto input)
        {
            var query =  CreateFilteredQuery(input);
            query =  query.Where(item => item.UserId == userId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);

            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<ContestEntryDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }

        public async Task<PagedResultDto<ContestEntryDetailsDto>> GetAllByUserIdWithDetails(long userId, PagedResultRequestDto input, bool isAdminArea = false)
        {
            var query =  _repository.GetAllIncluding(itm => itm.Status, itm => itm.User)
                .Where(itm => itm.UserId == userId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            var items = new List<ContestEntryDetailsDto>();
                foreach (var entity in entityDtos)
                {
                    var contestIn = await _contestRepository.GetAllIncluding(itm => itm.ContestEntryType).FirstOrDefaultAsync(cont => cont.Id == entity.ContestId);
                    entity.Contest = contestIn.MapTo<ContestDto>();
                    var totalComments = await _contestEntryCommentAppService.CountByContestEntryId(entity.Id);
                    var totalVotes = await _contestEntryVoteAppService.CountByContestEntryId(entity.Id);
                    var voteType = await _voteTypeAppService.Get(new EntityDto{Id = entity.Contest.VoteTypeId});
                    if (isAdminArea)
                    {
                        var contest = await _contestRepository.GetAllIncluding(cont => cont.ContestCategory,
                                cont => cont.ContestEntryType, cont => cont.ContestType,
                                cont => cont.Status,cont => cont.VoteType, cont => cont.ContestFee)
                                 .FirstOrDefaultAsync(cont => cont.Id == entity.ContestId);
                        entity.Contest = contest.MapTo<ContestDto>();
                    }
                    var totalViews = await _contestEntryViewAppService.GetViewsByContestEntryId(entity.Id);
                    var totalShares = await _contestEntryShareAppService.GetSharesByContestEntryId(entity.Id);
                    var item = new ContestEntryDetailsDto
                    {
                        ContestEntry = entity,
                        TotalComments = totalComments,
                        TotalVotes = totalVotes,
                        VoteType = voteType,
                        VoteAmount =  entity.Contest.VoteAmount,
                        TotalViews = totalViews,
                        TotalShares = totalShares
                    };
                    items.Add(item);
                }
                return new PagedResultDto<ContestEntryDetailsDto>(
                    totalCount,
                    items
                ); 
           
            
            
        }

        public async Task<PagedResultDto<ProfileContestEntriesDto>> GetUserProfileEntries(long userId, PagedResultRequestDto input)
        {
            var query =  _repository.GetAll()
                .Where(itm => itm.UserId == userId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            query = query.Select(item => new IContestProEntities.ContestEntry
            {
                Id = item.Id,
                Permalink = item.Permalink,
                Image = item.Image,
                Video = item.Video,
                ArticleImage = item.ArticleImage,
                ContestId = item.ContestId,
                CreationTime = item.CreationTime
            });
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
                return new PagedResultDto<ProfileContestEntriesDto>(
                    totalCount,
                    entities.MapTo<List<ProfileContestEntriesDto>>()
                ); 
        }
       
        [AbpAuthorize(PermissionNames.Pages_Admin, PermissionNames.Pages_Contest_Create)]
        public async Task<ContestEntryDto> UpdateStatusAsync(UpdateStatusDto model)
        {
            var entry = await _repository.FirstOrDefaultAsync(itm => itm.Id == model.EntityId);
            var status =  await _statusRepository.GetAll().Select(stat => new {stat.Id, stat.Name})
                .FirstOrDefaultAsync(stat => stat.Name == model.StatusName);
            if (entry == null || status == null)
            {
                return null;
            }
            entry.StatusId = status.Id;
            UnitOfWorkManager.Current.SaveChanges();
            return entry.MapTo<ContestEntryDto>();
        }

        private async Task<ContestEntryDto> CreateEntry(ContestEntryInputDto model)
        {
            var contest = await _contestRepository.GetAllIncluding(item => item.ContestEntryType)
                .Select(item => new IContestProEntities.Contest
                {
                    Id = item.Id,
                    ContestTypeId = item.ContestTypeId,
                    ContestEntryType = item.ContestEntryType
                }).FirstOrDefaultAsync(item => item.Id == model.ContestId);
            if (contest == null)
            {
                throw new UserFriendlyException("Contest not found.");
            }

            if (contest.ContestEntryType.Name == AppConsts.ContestEntryTypeImage)
            {
                var uniqueFileName = Guid.NewGuid().ToString("N") + ".jpg";
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploads");
                var filePath = Path.Combine(uploads, uniqueFileName);
                var dbModel = new CreateContestEntryDto();
                if (model.Image == null || model.Image.Length == 0)
                {
                    throw new UserFriendlyException("Please upload a file.");
                }

                if (model.Image.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    throw new UserFriendlyException("Please select only image file.");
                }

                await model.Image.CopyToAsync(new FileStream(filePath, FileMode.Create));
                dbModel.Image = uniqueFileName;
                dbModel.UserId = AbpSession.GetUserId();
                dbModel.ContestId = contest.Id;
                var savedItem = await Create(dbModel);
                if (savedItem.Id > 0)
                {
                    return savedItem;
                }
            }
            else if (contest.ContestEntryType.Name == AppConsts.ContestEntryTypeVideo)
            {
                /*if (model.Poster == null || model.Poster.Length == 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please upload a video poster image."));
                    return View(model); 
                }
                if (model.Poster.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please select only image file for the poster."));
                    return View(model);
                }*/

                if (model.Video == null || model.Video.Length == 0)
                {
                    throw new UserFriendlyException("Please select only mp4 video file.");
                }

                if (model.Video.ContentType.IndexOf("mp4", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    throw new UserFriendlyException("Please select only mp4 video file.");
                }
                /*var uniqueFileName = Guid.NewGuid().ToString("N") + ".jpg";
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploads");
                var filePath = Path.Combine(uploads, uniqueFileName);
                await model.Poster.CopyToAsync(new FileStream(filePath, FileMode.Create));
*/

                var videoUploads = Path.Combine(_hostingEnvironment.WebRootPath, "videos");
                var uniqueVideoFileName = Guid.NewGuid().ToString("N") + ".mp4";
                var fileVideoPath = Path.Combine(videoUploads, uniqueVideoFileName);
                await model.Video.CopyToAsync(new FileStream(fileVideoPath, FileMode.Create));
                var dbModel = new CreateContestEntryDto();
                //dbModel.Poster = uniqueFileName;
                dbModel.UserId = AbpSession.GetUserId();
                dbModel.ContestId = contest.Id;
                dbModel.Video = uniqueVideoFileName;
                var savedItem = await Create(dbModel);
                if (savedItem.Id > 0)
                {
                    return savedItem;

                }
            }
            else if (contest.ContestEntryType.Name == AppConsts.ContestEntryTypeArticle)
            {
                if (model.Poster == null || model.Poster.Length == 0)
                {
                    throw new UserFriendlyException(
                        "Please select only image file for the poster. Only jpg or png is allowed.");
                }



                if (model.Article == null)
                {
                    throw new UserFriendlyException("Please write your article.");
                }

                var uniqueFileName = Guid.NewGuid().ToString("N") + ".jpg";
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploads");
                var filePath = Path.Combine(uploads, uniqueFileName);
                var dbModel = new CreateContestEntryDto();
                await model.Poster.CopyToAsync(new FileStream(filePath, FileMode.Create));

                dbModel.Poster = uniqueFileName;
                dbModel.UserId = AbpSession.GetUserId();
                dbModel.ContestId = contest.Id;
                dbModel.Article = model.Article;
                var savedItem = await Create(dbModel);
                if (savedItem.Id > 0)
                {
                    return savedItem;
                }

            }
            throw new UserFriendlyException("Error occurred while completing your request.");

        }

        [AbpAuthorize]
        public async Task<IReadOnlyList<AddPosterToVideoDto>> AddPosterToVideos()
        {
            List<AddPosterToVideoDto> items = new EditableList<AddPosterToVideoDto>();
            try
            {
                var contestEntryType =
                await _contestEntryTypRepository.FirstOrDefaultAsync(item =>
                    item.Name == AppConsts.ContestEntryTypeVideo);
            var videoContestIds = await _contestRepository.GetAll().Where(item => item.ContestEntryTypeId == contestEntryType.Id).Select(item => item.Id).ToListAsync();
            foreach (var videoContestId in videoContestIds)
            {
                var entries = _repository.GetAll().Where(item => item.ContestId == videoContestId && (item.Poster == null || item.Poster == ""));
                foreach (var contestEntry in entries)
                {
                    var fileName = Guid.NewGuid().ToString("N");
                    var fileNameExtension = fileName + FileExtensions.Png;
                    var output = Path.Combine(_hostingEnvironment.WebRootPath + "/images/uploads/", fileNameExtension);
                    var result = await Conversion.Snapshot(_hostingEnvironment.WebRootPath + "/videos/" + contestEntry.Video, output, TimeSpan.FromSeconds(0))
                        .Start();
            
                    ///Optimize image
                    var file = new FileInfo(output);

                    //Console.WriteLine("Bytes before: " + file.Length);
                    var sizeBefore = "Bytes before png: " + file.Length;
            
                    // Read image from file
                    using (var image = new MagickImage(file))
                    {
                        // Sets the output format to jpeg
                        image.Format = MagickFormat.Jpeg;
                        // Create byte array that contains a jpeg file
                        image.Quality = 30;
                        //image.Resize(40, 40);
                        fileName = fileName + ".jpg";
                        var outputNew = Path.Combine(_hostingEnvironment.WebRootPath + "/images/uploads/", fileName);
                        contestEntry.Poster = fileName;
                        File.WriteAllBytes(outputNew, image.ToByteArray());
                        File.Delete(output);
                        items.Add(new AddPosterToVideoDto
                        {
                            Id = contestEntry.Id,
                            ContestId = videoContestId,
                            Video = contestEntry.Video,
                            Poster = fileName,
                        });
                    }
                }
            }
                UnitOfWorkManager.Current.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
           
            return items;
        }


    }
}