﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestEntry.Dto;
using IContestPro.Contest.Dto;

namespace IContestPro.Contest.ContestEntry
{
    public interface IContestEntryAppService : IBaseAppService<ContestEntryDto, int, PagedResultRequestDto, CreateContestEntryDto, ContestEntryDto>
    {
        Task<PagedResultDto<ContestEntryDto>> GetAllByContestId(int contestId, PagedResultRequestDto input);
        Task<PagedResultDto<ContestEntryDetailsDto>> GetAllByContestIdWithDetails(int contestId,
            PagedResultRequestDto input);
        Task<PagedResultDto<ContestEntryDto>> GetAllByContestPermalink(string permalink, PagedResultRequestDto input);
        Task<PagedResultDto<ContestEntryDetailsDto>> GetAllByContestPermalinkWithDetails(string permalink, PagedResultRequestDto input);
        Task<int> CountByContestId(int contestId);
        Task<int> CountActiveEntriesByContestId(int contestId);

        Task<int> CountByUserId(long userId);
        Task<PagedResultDto<ContestEntryDto>> GetAllByStatus(string statusName, PagedResultRequestDto input);
        Task<PagedResultDto<ContestEntryDetailsDto>> GetAllByStatusWithDetails(string statusName,
            PagedResultRequestDto input);
        
        Task<ContestEntryDetailsDto> GetWithDetails(EntityDto<int> input);
        Task<ContestEntryDto> GetByPermalink(string permalink);
        Task<ContestEntryDetailsDto> GetByPermalinkWithDetails(string permalink, bool isAdmin = false);
        Task<PagedResultDto<ContestEntryDetailsDto>> GetAllWithDetails(PagedResultRequestDto input);
       
        Task<PagedResultDto<ContestEntryDto>> GetAllByUserId(long userId, PagedResultRequestDto input);
        Task<PagedResultDto<ContestEntryDetailsDto>> GetAllByUserIdWithDetails(long userId, PagedResultRequestDto input, bool isAdminArea = false);

        Task<ContestEntryDto> UpdateStatusAsync(UpdateStatusDto model);

        
        Task<bool> IsUserInContest(long userId, int contestId);
        Task<bool> IsCurrentUserInContest(int contestId);
        Task<bool> IsUserContestOwner(int contestId);
        
        Task<PagedResultDto<ActiveContestEntryDto>> GetActiveContestEntries(PagedResultRequestDto input);
        Task<PagedResultDto<ActiveContestEntryDto>> GetEntriesByContestPermalink(string permalink, PagedResultRequestDto input);
        Task<ContestDetailsEntryDetailsDto> GetEntriesAndContestDetailsByContestPermalink(string permalink, PagedResultRequestDto input);
        Task<PagedResultDto<ProfileContestEntriesDto>> GetUserProfileEntries(long userId, PagedResultRequestDto input);
        Task<IReadOnlyList<AddPosterToVideoDto>> AddPosterToVideos();
    }
}