﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using IContestPro.Helpers.DatabaseFieldLengths;
using IContestPro.Status.Dto;

namespace IContestPro.Contest.ContestEntry.Dto
{
    [AutoMapTo(typeof(IContestProEntities.ContestEntry))]
    public class CreateContestEntryDto
    {
        
        [StringLength(ContestEntryFieldLength.MaxImageLength)]
        public string Image { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxVideoLength)]
        public string Video { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxPosterLength)]
        public string Poster { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxArticleLength)]
        public string Article { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxArticleImageLength)]
        public string ArticleImage { get; set; } 
        [StringLength(StandardFieldLength.MaxPermalinkLength)]
        public string Permalink { get; set; } 
        public int StatusId { get; set; }
        public StatusDto Status { get; set; }
        public bool IsFeatured { get; set; }
        public long UserId { get; set; }
        public int ContestId { get; set; }
    }
}