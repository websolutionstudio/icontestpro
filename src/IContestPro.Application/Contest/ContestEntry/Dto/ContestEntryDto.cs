﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Contest.Dto;
using IContestPro.Helpers.DatabaseFieldLengths;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Contest.ContestEntry.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.ContestEntry))]
    public class ContestEntryDto : FullAuditedEntityDto, IMayHaveTenant
    {
        [StringLength(ContestEntryFieldLength.MaxImageLength)]
        public string Image { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxVideoLength)]
        public string Video { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxPosterLength)]
        public string Poster { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxArticleLength)]
        public string Article { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxArticleImageLength)]
        public string ArticleImage { get; set; } 
        [StringLength(StandardFieldLength.MaxPermalinkLength)]
        public string Permalink { get; set; } 
        public int? TenantId { get; set; }
        public long UserId { get; set; }
        public int ContestId { get; set; }
        public int StatusId { get; set; }
        public bool IsFeatured { get; set; }

        public StatusDto Status { get; set; }
        public UserDto User { get; set; }
        public ContestDto Contest { get; set; }
    }
}