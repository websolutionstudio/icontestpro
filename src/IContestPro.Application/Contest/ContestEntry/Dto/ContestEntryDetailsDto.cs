﻿using IContestPro.Sponsor.Dto;
using IContestPro.VoteType.Dto;

namespace IContestPro.Contest.ContestEntry.Dto
{
    public class ContestEntryDetailsDto
    {
        public ContestEntryDto ContestEntry { get; set; }       
        public VoteTypeDto VoteType { get; set; }       
        public SponsorDto Sponsor { get; set; }       
        public int TotalComments { get; set; }       
        public int TotalVotes { get; set; }
        public int TotalViews { get; set; }       
        public int TotalShares { get; set; }
        public decimal? VoteAmount { get; set; }
    }
}