﻿namespace IContestPro.Contest.ContestEntry.Dto
{
    public class AddPosterToVideoDto
    {
        public int Id { get; set; }
        public int ContestId { get; set; }
        public string Video { get; set; }
        public string Poster { get; set; }
    }
}