﻿using System;
using Abp.Application.Services.Dto;

namespace IContestPro.Contest.ContestEntry.Dto
{
    public class ContestDetailsEntryDetailsDto
    {
        public int Id { get; set; }       
        public string Title { get; set; }       
        public string Permalink { get; set; }       
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime EntryStartDate { get; set; } 
        public DateTime EntryEndDate { get; set; } 
        public string StatusName { get; set; } 
        
        public PagedResultDto<ActiveContestEntryDto> Entries { get; set; } 

    }
}