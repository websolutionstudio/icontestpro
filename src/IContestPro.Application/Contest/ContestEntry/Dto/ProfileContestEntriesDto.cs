﻿using System;
using Abp.AutoMapper;

namespace IContestPro.Contest.ContestEntry.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.ContestEntry))]
    public class ProfileContestEntriesDto
    {
        public int Id { get; set; } 
        public string Permalink { get; set; } 
        public string Image { get; set; } 
        public string Video { get; set; } 
        public string ArticleImage { get; set; } 
        public int ContestId { get; set; }
        public DateTime CreationTime { get; set; }
       
    }
}