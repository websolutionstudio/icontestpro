﻿using System;

namespace IContestPro.Contest.ContestEntry.Dto
{
    public class ActiveContestEntryDto
    {
        public int Id { get; set; }       
        public long UserId { get; set; }       
        public int ContestId { get; set; }       
        public string NickName { get; set; }       
        public string CompanyName { get; set; }       
        public string CompanyLogo { get; set; }       
        public string UserImage { get; set; }       
        public string Video { get; set; }       
        public string UserLocation { get; set; }       
        public string ContestTitle { get; set; }       
        public string Poster { get; set; }       
        public string Permalink { get; set; }       
        public string ContestPermalink { get; set; }       
        public string ContestEntryType { get; set; }       
        public int TotalVotes { get; set; }       
        public int TotalViews { get; set; }       
        public int TotalComments { get; set; }
        public int TotalShares { get; set; }
        public string VoteType { get; set; }
        public decimal? VoteAmount { get; set; }
        public int VotePoint { get; set; }
        public string ContestType { get; set; }
        public decimal? EntryAmount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime EntryStartDate { get; set; } 
        public DateTime EntryEndDate { get; set; } 

    }
}