﻿using System.ComponentModel.DataAnnotations;
using IContestPro.Helpers.DatabaseFieldLengths;
using Microsoft.AspNetCore.Http;

namespace IContestPro.Contest.ContestEntry.Dto
{
    public class ContestEntryInputDto
    {
        
        public IFormFile Image { get; set; } 
        public IFormFile Video { get; set; } 
        public IFormFile Poster { get; set; } 
        [StringLength(ContestEntryFieldLength.MaxArticleLength)]
        public string Article { get; set; } 
        public IFormFile ArticleImage { get; set; } 
        [Required]
        public int ContestId { get; set; }
    }
}