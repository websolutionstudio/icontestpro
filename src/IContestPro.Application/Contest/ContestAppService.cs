﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using Hangfire;
using IContestPro.Authorization;
using IContestPro.BackgroundJobs;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestComment;
using IContestPro.Contest.ContestEntry;
using IContestPro.Contest.ContestFee;
using IContestPro.Contest.ContestType;
using IContestPro.Contest.ContestVote;
using IContestPro.Contest.Dto;
using IContestPro.EntityFrameworkCore;
using IContestPro.EntityFrameworkCore.StoreProcedureManager;
using IContestPro.Helpers;
using IContestPro.Periodic;
using IContestPro.Sponsor;
using IContestPro.Status;
using IContestPro.Status.Dto;
using IContestPro.Users;
using Microsoft.EntityFrameworkCore;
using SendGrid.Helpers.Mail;

namespace IContestPro.Contest
{
    
    public class ContestAppService : BaseAppService<IContestProEntities.Contest, ContestDto, int,
                                PagedResultRequestDto, CreateContestDto, ContestDto>, IContestAppService
    {

        private readonly IStatusAppService _statusAppService;
        private readonly IContestCommentAppService _contestCommentAppService;
        private readonly IContestEntryVoteAppService _contestEntryVoteAppService;
        private readonly IContestEntryAppService _contestEntryAppService;
        private readonly ISponsorAppService _sponsorAppService;
        private readonly IContestTypeAppService _contestTypeAppService;
        private readonly IPeriodicAppService _periodicAppService;
        private readonly IContestFeeAppService _contestFeeAppService;
        private readonly IUserAppService _userAppService;
        private readonly IRepository<IContestProEntities.Contest> _repository;
        private readonly IContestProDbContextFactory _contestProDbContextFactory;

       // private readonly ICustomPermissionChecker _customPermissionChecker;
        public ContestAppService( 
            IStatusAppService statusAppService, IContestCommentAppService contestCommentAppService, 
            ISponsorAppService sponsorAppService, IContestEntryVoteAppService contestEntryVoteAppService, 
            IContestEntryAppService contestEntryAppService,IContestTypeAppService contestTypeAppService,
            IPeriodicAppService periodicAppService, IContestFeeAppService contestFeeAppService,
                 IRepository<IContestProEntities.Contest> repository, IIocResolver  iocResolver, IUserAppService userAppService, IContestProDbContextFactory contestProDbContextFactory) : base(repository, iocResolver)
        {
            _statusAppService = statusAppService;
            _contestCommentAppService = contestCommentAppService;
            _sponsorAppService = sponsorAppService;
            _contestEntryAppService = contestEntryAppService;
            _contestTypeAppService = contestTypeAppService;
            _periodicAppService = periodicAppService;
            _contestFeeAppService = contestFeeAppService;
            _repository = repository;
            _userAppService = userAppService;
            _contestProDbContextFactory = contestProDbContextFactory;
            _contestEntryVoteAppService = contestEntryVoteAppService;
        }
        
        public async Task<PagedResultDto<ContestWithFilterDto>> GetContestsWithFilter(
            bool filterByPaid = false,
            bool filterByCoded = false,
            bool filterByVp = false,
            bool filterByFree = false,
            bool filterByEveryoneWin = false,
            bool filterByHighestVote = false,
            string statusName = AppConsts.StatusLive,
            int page = 1,
            int pageSize = 20)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetContests)
                    .WithSqlParam("FilterPaid", filterByPaid)
                    .WithSqlParam("FilterCoded", filterByCoded)
                    .WithSqlParam("FilterVP", filterByVp)
                    .WithSqlParam("FilterFree", filterByFree)
                    .WithSqlParam("FilterEveryoneWin", filterByEveryoneWin)
                    .WithSqlParam("FilterHighestVote", filterByHighestVote)
                    .WithSqlParam("StatusName", statusName)
                    .WithSqlParam("Page", page)
                    .WithSqlParam("PageSize", pageSize);
                var items = await cmd.ExecuteStoredProcedureListAsync<ContestWithFilterDto>();
                return new PagedResultDto<ContestWithFilterDto>(
                    items.Count > 0 ? items.First().TotalCount : 0,
                    items
                );
            }
        }

        public async Task<PagedResultDto<ContestWithFilterDto>> SearchContestsByTag(string tag, int page = 1, int pageSize = 20)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpSearchContestsByTag)
                    .WithSqlParam("Tag", tag)
                    .WithSqlParam("Page", page)
                    .WithSqlParam("PageSize", pageSize);
                var items = await cmd.ExecuteStoredProcedureListAsync<ContestWithFilterDto>();
                return new PagedResultDto<ContestWithFilterDto>(
                    items.Count > 0 ? items.First().TotalCount : 0,
                    items
                );
            }
        }

        public async Task<ContestDetailsDto> GetWithDetails(EntityDto<int> input)
        {
            var contest = await _repository.GetAllIncluding(cont => cont.ContestCategory,
                    cont => cont.ContestEntryType, cont => cont.ContestType,
                    cont => cont.Status,cont => cont.VoteType, cont => cont.ContestFee, cont => cont.Sponsor)
                .FirstOrDefaultAsync(cont => cont.Id == input.Id);
            if (contest == null)
            {
                return null;
            }
            var totalComments = await _contestCommentAppService.CountByContestId(contest.Id);
            var totalVotes = await _contestEntryVoteAppService.CountByContestId(contest.Id);
            var totalEntries = IsGranted(PermissionNames.Pages_Admin) ?
                await _contestEntryAppService.CountByContestId(contest.Id) : 
                await _contestEntryAppService.CountActiveEntriesByContestId(contest.Id);
            return new ContestDetailsDto
            {
                Contest = contest.MapTo<ContestDto>(),
                TotalComments = totalComments,
                TotalVotes = totalVotes,
                TotalEntries = totalEntries
            };
           
        }
        public async Task<ContestDto> GetByPermalink(string permalink)
        {
            var query = await _repository
                .FirstOrDefaultAsync(cont => cont.Permalink == permalink);
            return MapToEntityDto(query);
        }
        public async Task<ContestDetailsDto> GetByPermalinkWithDetails(string permalink)
        {
            var contest = await _repository.GetAllIncluding(cont => cont.ContestCategory,
                    cont => cont.ContestEntryType, cont => cont.ContestType,
                    cont => cont.Status,cont => cont.VoteType, cont => cont.ContestFee, cont => cont.Sponsor)
                .FirstOrDefaultAsync(cont => cont.Permalink == permalink);
            if (contest == null)
            {
                return null;
            }
            var totalComments = await _contestCommentAppService.CountByContestId(contest.Id);
            var totalVotes = await _contestEntryVoteAppService.CountByContestId(contest.Id);
            var totalEntries = IsGranted(PermissionNames.Pages_Admin) ?
                await _contestEntryAppService.CountByContestId(contest.Id) : 
                await _contestEntryAppService.CountActiveEntriesByContestId(contest.Id);
            return new ContestDetailsDto
            {
                Contest = contest.MapTo<ContestDto>(),
                TotalComments = totalComments,
                TotalVotes = totalVotes,
                TotalEntries = totalEntries
            };
        }

        public override async Task<PagedResultDto<ContestDto>> GetAll(PagedResultRequestDto input)
        {
            //This must be added to all 
            var query = CreateFilteredQuery(input);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);

            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<ContestDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        } 
        public async Task<PagedResultDto<ContestDetailsDto>> GetAllWithDetails(PagedResultRequestDto input)
        {
            
            var query =  _repository.GetAllIncluding(itm => itm.Status, itm => itm.ContestType,
                itm => itm.ContestEntryType, itm => itm.ContestCategory,cont => cont.VoteType,
                cont => cont.ContestFee, cont => cont.Sponsor);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            var items = new List<ContestDetailsDto>();
            foreach (var entity in entityDtos)
            {
                var totalComments = await _contestCommentAppService.CountByContestId(entity.Id);
                var totalVotes = await _contestEntryVoteAppService.CountByContestId(entity.Id);
                var totalEntries = IsGranted(PermissionNames.Pages_Admin) ?
                    await _contestEntryAppService.CountByContestId(entity.Id) : 
                    await _contestEntryAppService.CountActiveEntriesByContestId(entity.Id);
                var item = new ContestDetailsDto
                {
                    Contest = entity,
                    TotalComments = totalComments,
                    TotalVotes = totalVotes,
                    TotalEntries = totalEntries
                };
                items.Add(item);
            }
           

            return new PagedResultDto<ContestDetailsDto>(
                totalCount,
                items
                
            );
        } 
        public async Task<PagedResultDto<ContestDto>> GetAllByStatus(string statusName, PagedResultRequestDto input)
        {
            var status = await _statusAppService.GetByNameAsync(statusName);
            if (status == null)
            {
                return new PagedResultDto<ContestDto>(0, new ContestDto[]{});
            }
            var query =  CreateFilteredQuery(input);
             query =  query.Where(item => item.StatusId == status.Id);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);

            var entities = await AsyncQueryableExecuter.ToListAsync(query);

            return new PagedResultDto<ContestDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        public async Task<PagedResultDto<ContestDetailsDto>> GetAllByStatusWithDetails(string statusName, PagedResultRequestDto input)
        {
            var status = await _statusAppService.GetByNameAsync(statusName);
            if (status == null)
            {
                return new PagedResultDto<ContestDetailsDto>(
                    0,
                    new ContestDetailsDto[]{}
                );
            }
            var query =  _repository.GetAllIncluding(itm => itm.Status, itm => itm.ContestType,
                itm => itm.ContestEntryType, itm => itm.ContestCategory,cont => cont.VoteType, 
                    cont => cont.ContestFee, cont => cont.Sponsor)
                .Where(item => item.StatusId == status.Id);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            var items = new List<ContestDetailsDto>();
            foreach (var entity in entityDtos)
            {
                var totalComments = await _contestCommentAppService.CountByContestId(entity.Id);
                var totalVotes = await _contestEntryVoteAppService.CountByContestId(entity.Id);
                var totalEntries = IsGranted(PermissionNames.Pages_Admin) ?
                    await _contestEntryAppService.CountByContestId(entity.Id) : 
                    await _contestEntryAppService.CountActiveEntriesByContestId(entity.Id);
                var item = new ContestDetailsDto
                {
                    Contest = entity,
                    TotalComments = totalComments,
                    TotalVotes = totalVotes,
                    TotalEntries = totalEntries
                };
                items.Add(item);
            }
           

            return new PagedResultDto<ContestDetailsDto>(
                totalCount,
                items
                
            );
        }

        
        public async Task<PagedResultDto<ContestDto>> GetAllByUserId(long userId, PagedResultRequestDto input)
        {
            //This must be added to all 
            var query = CreateFilteredQuery(input);
            query = query.Where(cont => cont.UserId == userId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<ContestDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        public async Task<PagedResultDto<ContestDto>> GetAllByUserIdAndStatus(long userId, string statusName,
            PagedResultRequestDto input, bool includeFeatured = true)
        {
            //This must be added to all 
            var status = await _statusAppService.GetByNameAsync(statusName);
            var query = CreateFilteredQuery(input);
            query = includeFeatured ? 
                query.Where(cont => cont.UserId == userId && cont.StatusId == status.Id) :
                query.Where(cont => cont.UserId == userId && cont.StatusId == status.Id && cont.IsFeatured == false);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<ContestDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }

        public async Task<List<UsersInContestDto>> GetUsersInContest(int contestId)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetUsersInContest)
                    .WithSqlParam("ContestId", contestId);
                var items = await cmd.ExecuteStoredProcedureListAsync<UsersInContestDto>();
                return items;
            }
        }

        public async Task<List<ContestsEndingSoonDto>> GetContestsEndingSoon(int pageSize = 5)
        {
           return await _repository.GetAll()
               .OrderBy(item => item.Id).Select(item => new ContestsEndingSoonDto
            {
                Id = item.Id,
                Title = item.Title,
                Permalink = item.Permalink,
                StartDate = item.StartDate,
                EndDate = item.EndDate
            })
                .Where(cont => cont.EndDate <  DateTime.Now.AddDays(30))
                .Take(pageSize).ToListAsync();
            
        }

        public async Task<PagedResultDto<ViewsDto>> GetViewsByPermalink(string permalink, int page, int pageSize)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetContestViews)
                    .WithSqlParam("Permalink", permalink)
                    .WithSqlParam("Page", page)
                    .WithSqlParam("PageSize", pageSize);
                var items = await cmd.ExecuteStoredProcedureListAsync<ViewsDto>();
                return new PagedResultDto<ViewsDto>
                {
                    Items = items,
                    TotalCount = items.Count > 0 ? items.First().TotalCount : 0
                };
            }
        }

        public async Task<PagedResultDto<ContestDetailsDto>> GetAllWithDetailsByUserId(long userId, PagedResultRequestDto input)
        {
            //This must be added to all 
            var query =  _repository.GetAllIncluding(itm => itm.Status, itm => itm.ContestType,
                itm => itm.ContestEntryType, itm => itm.ContestCategory,
                cont => cont.VoteType, cont => cont.ContestFee, cont => cont.Sponsor);
            query = query.Where(cont => cont.UserId == userId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = query.OrderByDescending(item => item.CreationTime);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var entityDtos = entities.Select(MapToEntityDto).ToList();
            var items = new List<ContestDetailsDto>();
            foreach (var entity in entityDtos)
            {
                var totalComments = await _contestCommentAppService.CountByContestId(entity.Id);
                var totalVotes = await _contestEntryVoteAppService.CountByContestId(entity.Id);
                var totalEntries = IsGranted(PermissionNames.Pages_Admin) || IsGranted(PermissionNames.Pages_Sponsor) ?
                    await _contestEntryAppService.CountByContestId(entity.Id) : 
                    await _contestEntryAppService.CountActiveEntriesByContestId(entity.Id);
                var item = new ContestDetailsDto
                {
                    Contest = entity,
                    TotalComments = totalComments,
                    TotalVotes = totalVotes,
                    TotalEntries = totalEntries
                };
                items.Add(item);
            }
            return new PagedResultDto<ContestDetailsDto>(
                totalCount,
                items
                
            );
        }

        public async Task<PagedResultDto<PastWinnersDto>> PastWinners(int page, int pageSize)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetPastWinners)
                    .WithSqlParam("Page", page)
                    .WithSqlParam("PageSize", pageSize);
                var items = await cmd.ExecuteStoredProcedureListAsync<PastWinnersDto>();
                return new PagedResultDto<PastWinnersDto>
                {
                    Items = items,
                    TotalCount = items != null && items.Count > 0 ? items.First().TotalCount : 0
                };
            }
        }

        [AbpAuthorize(PermissionNames.Pages_Contest_Create)]
        public async Task<ContestDto> UpdateStatusAsync(UpdateStatusDto model)
        {
            var contest = await _repository.GetAllIncluding(itm => itm.User).FirstOrDefaultAsync(itm => itm.Id == model.EntityId);
            var status = await _statusAppService.GetByNameAsync(model.StatusName);
            if (contest == null || status == null)
            {
                return null;
            }
            contest.StatusId = status.Id;
            UnitOfWorkManager.Current.SaveChanges();
            if (model.StatusName == AppConsts.StatusLive)
            {
                var url = AppConsts.DomainName + AppRoutingConsts.ContestDetails(contest.Permalink);
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = contest.UserId,
                    TenantId = AbpSession.TenantId,
                    Message = $"Your contest '{contest.Title}' has gone live. You can make it featured to promote the visibility. You can also share this link {url} on social media.",
                }); 
                BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                    (
                        new SendEmailDto
                        {
                            ShouldUseDefaultEmailTemplate = true,
                            Tos = new List<EmailAddress>{new EmailAddress(contest.User.EmailAddress, contest.User.NickName)},
                            From = new EmailAddress(AppConsts.IContestProEmail, AppConsts.IContestProDisplayName),
                            Subject = $"Contest '{contest.Title}' gone live",
                            Body = $"Your contest '{contest.Title}' has gone live. You can make it featured to promote the visibility. You can also share this link {url} on social media.",
                        })
                );
            }
            else
            {
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = contest.UserId,
                    TenantId = AbpSession.TenantId,
                    Message = $"The status of your contest '{contest.Title}' has been changed to {status.Name}.",
                });   
            }
            return contest.MapTo<ContestDto>();
        }


        [AbpAuthorize(PermissionNames.Pages_Contest_Create)]
        public override async Task<ContestDto> Create(CreateContestDto input)
        {
            // CheckCreatePermission();

            var contest = ObjectMapper.Map<IContestProEntities.Contest>(input);

            contest.TenantId = AbpSession.TenantId;
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusPaymentPending);
            if (status == null)
            {
                throw new UserFriendlyException("Status not found");
            }
            var company = await _sponsorAppService.GetByUserId(contest.UserId);
            if (company == null)
            {
                throw new UserFriendlyException("Company not found");
            }
            contest.StatusId = status.Id;
            contest.SponsorId = company.Id;
            contest.Permalink = Guid.NewGuid().ToString("N");
            var contestType = await _contestTypeAppService.Get(new EntityDto{Id = input.ContestTypeId});
            if (contestType.Name != AppConsts.ContestTypeCoded)
            {
                var periodic =  await _periodicAppService.GetByName(AppConsts.PeriodicMonthly);
                contest.PeriodicId = periodic.Id; 
            }
            
            if (contestType.Name != AppConsts.ContestTypeCoded)
            {
                var periodic =  await _periodicAppService.GetByName(AppConsts.PeriodicMonthly);
                contest.PeriodicId = periodic.Id;
            }
            
            var contestFee = await _contestFeeAppService.GetByContestTypeId(input.ContestTypeId);
            contest.ContestFeeId = contestFee.Id; 

           var item = await _repository.InsertAsync(contest);
            CurrentUnitOfWork.SaveChanges();
            item.Permalink = contest.Title.GeneratePermalinkWithId(item.Id);
            CurrentUnitOfWork.SaveChanges();
            var body =
                $"Your contest '{input.Title}' was created successfully. You can promote the contest from your account for optimal visibility on the platform. Thank you.";

            
             await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                UserId = input.UserId,
                Message = body
            });
            BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                (
                    new SendEmailDto
                    {
                        ShouldUseDefaultEmailTemplate = true,
                        Tos = new List<EmailAddress>{new EmailAddress(CurrentUser.EmailAddress, CurrentUser.NickName)},
                        Subject = "Your contest was successfully created",
                        Body = body
                    })
            );

            var adminUser = await _userAppService.GetTenantAdmin();
            var bodyAdmin =
                $"A user '{CurrentUser.NickName}' just created a contest with the title '{input.Title}'. Please check to assist him/her on what next to do. Thank you.";

             await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                UserId = adminUser.Id,
                Message = bodyAdmin
            });
            BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                (
                    new SendEmailDto
                    {
                        ShouldUseDefaultEmailTemplate = true,
                        Tos = new List<EmailAddress>{new EmailAddress(adminUser.EmailAddress, adminUser.NickName)},
                        Subject = "A user create a contest",
                        Body = bodyAdmin
                    })
            );
            return MapToEntityDto(contest);
        }
        [AbpAuthorize(PermissionNames.Pages_Contest_Update)]
        public override async Task<ContestDto> Update(ContestDto input)
        {
            var currentStatus = await _statusAppService.Get(new EntityDto{Id = input.StatusId});
            if (currentStatus == null)
            {
                throw new UserFriendlyException("Status not found");
            }
            
            var contest = ObjectMapper.Map<IContestProEntities.Contest>(input);
            if (currentStatus.Name != AppConsts.StatusPaymentPending &&
                currentStatus.Name != AppConsts.StatusPendingPaymentConfirmation &&
                currentStatus.Name != AppConsts.StatusPaymentConfirmed)
            {
                var status = await _statusAppService.GetByNameAsync(AppConsts.StatusPending);
                if (status == null)
                {
                    throw new UserFriendlyException("Status not found");
                }
                contest.StatusId = status.Id;
  
            }
            contest = await _repository.UpdateAsync(contest);
            CurrentUnitOfWork.SaveChanges();

            return MapToEntityDto(contest);
        }
        
        [AbpAuthorize]
        public async Task<bool> IsUserContestOwner(int contestId)
        {
            return await _repository.FirstOrDefaultAsync(item => item.UserId == AbpSession.GetUserId() &&
                                                                        item.Id == contestId) != null;
        }

        public async Task<StatusDto> GetContestStatus(int contestId)
        {
            var contest = await _repository.GetAllIncluding(item => item.Status)
                .Select(item => new {item.Id, item.Status}).FirstOrDefaultAsync(item => item.Id == contestId);
            return contest?.Status.MapTo<StatusDto>();
        }

        
        /// <summary>
        /// NotifyExpiryingContests called by background jobs
        /// </summary>
        public async Task NotifyExpiringContests()
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var items = await _repository.GetAllIncluding(itm => itm.User).Where(x => x.EndDate <= DateTime.Now.AddDays(7) && x.StatusId == status.Id).ToListAsync();
           
            if (items != null && items.Count > 0)
            {
                items.ForEach(item =>
                    {
                        BackgroundJob.Enqueue<BgJobContestProcesses>(x => x.NotifyUserOfExpiringContest(item.User, item));
                    });
            }
        }

        public async Task DisableExpiredContests()
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusLive);
            var items = await _repository.GetAllIncluding(itm => itm.User).Where(x => x.EndDate <= DateTime.Now && x.StatusId == status.Id).ToListAsync();
            if (items != null && items.Count > 0)
            {
                var closedStatus = await _statusAppService.GetByNameAsync(AppConsts.StatusClosed);
                items.ForEach(item =>
                {
                    item.StatusId = closedStatus.Id;
                    item.IsFeatured = false;
                    BackgroundJob.Enqueue<BgJobContestProcesses>(x => x.NotifyUserOfExpiredContest(item.User, item));

                });
               await CurrentUnitOfWork.SaveChangesAsync();
            }
        }
    }
}