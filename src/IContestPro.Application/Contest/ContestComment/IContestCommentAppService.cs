﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestComment.Dto;

namespace IContestPro.Contest.ContestComment
{
    public interface IContestCommentAppService : IBaseAppService<ContestCommentDto, int, PagedResultRequestDto,
                            CreateContestCommentDto, ContestCommentDto>
    {
        Task<PagedResultDto<ContestCommentDto>> GetAllByContestId(int contestId, PagedResultRequestDto input);
        Task<int> CountByContestId(int contestId);
        
    }
}