﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Base;
using IContestPro.Contest.ContestComment.Dto;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.Contest.ContestComment
{
    public class ContestCommentAppService : BaseAppService<IContestProEntities.ContestComment, ContestCommentDto, int,
        PagedResultRequestDto, CreateContestCommentDto, ContestCommentDto>, IContestCommentAppService
    {
        private readonly IRepository<IContestProEntities.ContestComment, int> _repository;

        public ContestCommentAppService(IRepository<IContestProEntities.ContestComment, int> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }
        [AbpAuthorize]
        public override async Task<ContestCommentDto> Create(CreateContestCommentDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.ContestComment>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        [AbpAuthorize]
        public override async Task<ContestCommentDto> Update(ContestCommentDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.ContestComment>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        public async Task<PagedResultDto<ContestCommentDto>> GetAllByContestId(int contestId, PagedResultRequestDto input)
        {
            var query = _repository.GetAllIncluding(itm => itm.User);
            query = query.Where(cont => cont.ContestId == contestId);
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<ContestCommentDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }

        public async Task<int> CountByContestId(int contestId)
        {
            return await _repository.GetAll().CountAsync(comm => comm.ContestId == contestId);
        }
    }
}