﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using IContestPro.Contest.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Contest.ContestComment.Dto
{
    [AutoMapTo(typeof(IContestProEntities.ContestComment))]
    public class CreateContestCommentDto
    {
        public long UserId { get; set; }
        [Required]
        public int ContestId { get; set; }
        [Required]
        [StringLength(255)]
        public string Comment { get; set; } 
        public int? TenantId { get; set; }
        
        public UserDto User { get; set; }
        public ContestDto Contest { get; set; }
    }
}