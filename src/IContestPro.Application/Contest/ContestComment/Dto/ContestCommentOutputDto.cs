﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Contest.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Contest.ContestComment.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.ContestComment))]
    public class ContestCommentOutputDto : FullAuditedEntityDto, IMayHaveTenant
    {
        public long UserId { get; set; }
        public int ContestId { get; set; }
        [StringLength(255)]
        public string Comment { get; set; } 
        public int? TenantId { get; set; }
        
        public UserDto User { get; set; }
        public ContestDto Contest { get; set; }
    }
}