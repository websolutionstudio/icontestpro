﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Base;
using IContestPro.Contest.ContestEntryType.Dto;

namespace IContestPro.Contest.ContestEntryType
{
    public class ContestEntryTypeAppService : BaseAppService<IContestProEntities.ContestEntryType, ContestEntryTypeDto, int,
                                PagedResultRequestDto, CreateContestEntryTypeDto, ContestEntryTypeDto>, IContestEntryTypeAppService
    {
        private readonly IRepository<IContestProEntities.ContestEntryType, int> _repository;

        public ContestEntryTypeAppService(IRepository<IContestProEntities.ContestEntryType, int> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }
        [AbpAuthorize]
        public override async Task<ContestEntryTypeDto> Create(CreateContestEntryTypeDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.ContestEntryType>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        [AbpAuthorize]
        public override async Task<ContestEntryTypeDto> Update(ContestEntryTypeDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.ContestEntryType>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        public async Task<ContestEntryTypeDto> GetByName(string name)
        {
            return  (await _repository.FirstOrDefaultAsync(itm => itm.Name == name)).MapTo<ContestEntryTypeDto>();
        }
    }
}