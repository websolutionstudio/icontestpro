﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestEntryType.Dto;

namespace IContestPro.Contest.ContestEntryType
{
    public interface IContestEntryTypeAppService : IBaseAppService<ContestEntryTypeDto, int, PagedResultRequestDto, CreateContestEntryTypeDto, ContestEntryTypeDto>
    {
        Task<ContestEntryTypeDto> GetByName(string name);

    }
}