﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Contest.ContestEntryType.Dto
{
    [AutoMapTo(typeof(IContestProEntities.ContestEntryType))]
    public class CreateContestEntryTypeDto
    {
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
    }
}