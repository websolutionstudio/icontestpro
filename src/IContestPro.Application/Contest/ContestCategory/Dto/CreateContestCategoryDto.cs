﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Contest.ContestCategory.Dto
{
    [AutoMapTo(typeof(IContestProEntities.ContestCategory))]
    public class CreateContestCategoryDto
    {
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
    }
}