﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;

namespace IContestPro.Contest.ContestCategory.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.ContestCategory))]
    public class ContestCategoryDto : FullAuditedEntityDto, IMayHaveTenant
    {
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }

        public int? TenantId { get; set; }
    }
}