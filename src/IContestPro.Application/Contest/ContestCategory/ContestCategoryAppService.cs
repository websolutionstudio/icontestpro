﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Authorization;
using IContestPro.Base;
using IContestPro.Contest.ContestCategory.Dto;

namespace IContestPro.Contest.ContestCategory
{
    public class ContestCategoryAppService : BaseAppService<IContestProEntities.ContestCategory, ContestCategoryDto, int,
                                PagedResultRequestDto, CreateContestCategoryDto, ContestCategoryDto>, IContestCategoryAppService
    {
        private readonly IRepository<IContestProEntities.ContestCategory> _repository;

        public ContestCategoryAppService(IRepository<IContestProEntities.ContestCategory> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }

        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<ContestCategoryDto> Create(CreateContestCategoryDto input)
        {
           var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.ContestCategory>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public override async Task<ContestCategoryDto> Update(ContestCategoryDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.ContestCategory>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }

       
        public async Task<ContestCategoryDto> GetByName(string name)
        {
            return  (await _repository.FirstOrDefaultAsync(itm => itm.Name == name)).MapTo<ContestCategoryDto>();
        }
    }
       
    
}