﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestCategory.Dto;

namespace IContestPro.Contest.ContestCategory
{
    public interface IContestCategoryAppService : IBaseAppService<ContestCategoryDto, int, PagedResultRequestDto, CreateContestCategoryDto, ContestCategoryDto>
    {
        Task<ContestCategoryDto> GetByName(string name);
    }
}