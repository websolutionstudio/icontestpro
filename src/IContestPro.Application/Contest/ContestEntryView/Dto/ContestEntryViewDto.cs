﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;

namespace IContestPro.Contest.ContestEntryView.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.ContestEntryView))]
    public class ContestEntryViewDto : FullAuditedEntityDto<long>, IMayHaveTenant
    {
        public long? UserId { get; set; }
        public int ContestEntryId { get; set; }
        public int? TenantId { get; set; }
    }
}