﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.Contest.ContestEntryView.Dto
{
    [AutoMapTo(typeof(IContestProEntities.ContestEntryView))]
    public class CreateContestEntryViewDto
    {
        public long? UserId { get; set; }
        [Required]
        public int ContestEntryId { get; set; }
    }
}