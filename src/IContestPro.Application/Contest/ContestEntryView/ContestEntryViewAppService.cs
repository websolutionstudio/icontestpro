﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using IContestPro.Base;
using IContestPro.Contest.ContestEntryView.Dto;

namespace IContestPro.Contest.ContestEntryView
{
    public class ContestEntryViewAppService : BaseAppService<IContestProEntities.ContestEntryView, ContestEntryViewDto, long,
                                PagedResultRequestDto, CreateContestEntryViewDto, ContestEntryViewDto>, IContestEntryViewAppService
    {
        private readonly IRepository<IContestProEntities.ContestEntryView, long> _repository;

        public ContestEntryViewAppService(IRepository<IContestProEntities.ContestEntryView, long> repository, IIocResolver  iocResolver) : base(repository, iocResolver)
        {
            _repository = repository;
        }
        public override async Task<ContestEntryViewDto> Create(CreateContestEntryViewDto input)
        {
            var item = await _repository.InsertAsync(input.MapTo<IContestProEntities.ContestEntryView>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }
        
        [AbpAuthorize]
        public override async Task<ContestEntryViewDto> Update(ContestEntryViewDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.ContestEntryView>());
            CurrentUnitOfWork.SaveChanges();
            return MapToEntityDto(item);
        }

        public async Task<int> GetViewsByContestEntryId(int contestEntryId)
        {
            return await _repository.CountAsync(item => item.ContestEntryId == contestEntryId);
        }
    }
}