﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Contest.ContestEntryView.Dto;

namespace IContestPro.Contest.ContestEntryView
{
    public interface IContestEntryViewAppService : IBaseAppService<ContestEntryViewDto, long, PagedResultRequestDto, CreateContestEntryViewDto, ContestEntryViewDto>
    {
        Task<int> GetViewsByContestEntryId(int contestEntryId);
    }
}