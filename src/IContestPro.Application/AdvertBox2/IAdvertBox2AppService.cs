﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.AdvertBox2.Dto;
using IContestPro.Contest.Dto;

namespace IContestPro.AdvertBox2
{
    /// <summary>
    /// This is the advert at the bottom right side of the pages.
    /// </summary>
    public interface IAdvertBox2AppService : IBaseAppService<AdvertBox2Dto, int, PagedResultRequestDto, CreateAdvertBox2Dto, AdvertBox2Dto>
    {
        Task<AdvertBox2Dto> GetRandomLive(int totalTake = 1);
        Task<PagedResultDto<AdvertBox2Dto>> GetAllByUser(long userId, PagedResultRequestDto input);
        Task<PagedResultDto<AdvertBox2Dto>> GetAllByStatus(string statusName, PagedResultRequestDto input);
        Task<AdvertBox2Dto> CreateAndUploadImage(CreateEditAdvertBox2Dto input);
        Task<AdvertBox2Dto> UpdateAndUploadImage(CreateEditAdvertBox2Dto input);
        Task<AdvertBox2Dto> UpdateStatusAsync(UpdateStatusDto model);
        Task NotifyExpiring();
        Task DisableExpired();
    }
}