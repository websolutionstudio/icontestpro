﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace IContestPro.AdvertBox2.Dto
{
    [AutoMapTo(typeof(IContestProEntities.AdvertBox2))]
    public class CreateAdvertBox2Dto
    {
        [Required]
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [Required]
        public string Image { get; set; }
        public int StatusId { get; set; }
        [Required]
        public int AdvertPriceId { get; set; }
        [Required]
        public decimal AmountPaid { get; set; }
        [Required]
        public string ExternalUrl { get; set; }

        [Required]
        public int UserId { get; set; }

    }
}