﻿using System;
using Abp.AutoMapper;
using Microsoft.AspNetCore.Http;

namespace IContestPro.AdvertBox2.Dto
{
    [AutoMapTo(typeof(CreateAdvertBox2Dto))]
    [AutoMapFrom(typeof(AdvertBox2Dto))]
    public class CreateEditAdvertBox2Dto
    {
        
        public int? Id { get; set; }
        public int? TenantId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public IFormFile Image { get; set; }
        public int? StatusId { get; set; }
        public long? UserId { get; set; }
        public int? AdvertPriceId { get; set; }
        public string ExternalUrl { get; set; }

    }
}