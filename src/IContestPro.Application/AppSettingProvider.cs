﻿using System.Collections.Generic;
using Abp.Configuration;

namespace IContestPro
{
    public class AppSettingProvider : SettingProvider
    {
        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            return new[]
            {
                new SettingDefinition(
                    AppConsts.SettingsWithdrawalRequestFeePercentage,
                    "1"
                    ), new SettingDefinition(
                    AppConsts.SettingsWithdrawalMinimumAmount,
                    "5000"
                ), new SettingDefinition(
                    AppConsts.SettingsTransferFeePercentage,
                    "0.1"
                ), new SettingDefinition(
                    AppConsts.SettingsReferralVotePointAward,
                    "1000"
                ), new SettingDefinition(
                    AppConsts.SettingsReferrerPercentageIfReferredUserBecomesSponsor,
                    "10"
                ), new SettingDefinition(
                    AppConsts.SettingsShouldPercentageBePaidIfReferredSponsorCreatesFirstContest,
                    "true"
                ), new SettingDefinition(
                    AppConsts.SettingsCodeSize,
                    "6"
                ), new SettingDefinition(
                    AppConsts.SettingsBatchNoSize,
                    "6"
                ), new SettingDefinition(
                    AppConsts.SettingsMaximumBatchSize,
                    "2000"
                ), new SettingDefinition(
                    AppConsts.SettingsMaxCodesAssignablePerTime,
                    "2000"
                )
            };
        }
    }
}