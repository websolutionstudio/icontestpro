﻿using System;
using IContestPro.Cookie.Dto;
using Microsoft.AspNetCore.Http;

namespace IContestPro.Cookie
{
     public class CookieAppService : ICookieAppService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CookieAppService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }


        public CookieDto Get(string key)
        {
            if (_httpContextAccessor?.HttpContext?.Request?.Cookies == null)
            {
                return null;
            }
           var item = _httpContextAccessor.HttpContext.Request.Cookies[key];
            return new CookieDto{Key = key, Value = item};
        }

        public void Set(string key, string value, DateTime? expireTime = null)
        {
            if (_httpContextAccessor?.HttpContext?.Request?.Cookies == null)
            {
                return;
            }
            var option = new CookieOptions();  
            if (expireTime.HasValue)  
                option.Expires = expireTime;  
            _httpContextAccessor.HttpContext.Response.Cookies.Append(key, value, option);  
        }

        public void Remove(string key)
        {
            if (_httpContextAccessor?.HttpContext?.Request?.Cookies == null)
            {
                return;
            }
            _httpContextAccessor.HttpContext.Response.Cookies.Delete(key); 
        }
    }
}