﻿using System;
using Abp.Application.Services;
using IContestPro.Cookie.Dto;

namespace IContestPro.Cookie
{
    public interface ICookieAppService : IApplicationService
    {
        CookieDto Get(string key);
        void Set(string key, string value, DateTime? expireTime = null);
        void Remove(string key);
    }
}