﻿using System;

namespace IContestPro.Cookie.Dto
{
    public class CookieDto
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public DateTime ExpireTime { get; set; }
        public string Domain { get; set; }
        public string Path { get; set; }
    }
}