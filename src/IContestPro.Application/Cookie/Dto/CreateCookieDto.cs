﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IContestPro.Cookie.Dto
{
    public class CreateCookieDto
    {
        [Required]
        public string Key { get; set; }
        [Required]
        public DateTime ExpireTime { get; set; }
        public string Domain { get; set; }
        public string Path { get; set; }

    }
}