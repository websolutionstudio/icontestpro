﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using IContestPro.Authorization.Users;
using IContestPro.Base;
using IContestPro.Blog.Dto;
using IContestPro.Contest.Dto;
using IContestPro.EntityFrameworkCore;
using IContestPro.EntityFrameworkCore.StoreProcedureManager;
using IContestPro.IContestProEntities;
using IContestPro.Status;
using IContestPro.Status.Dto;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.Blog
{
     public class BlogAppService : BaseAppService<IContestProEntities.Blog, BlogDto, int,
                                PagedResultRequestDto, CreateBlogDto, BlogDto>, IBlogAppService
    {
        private readonly IRepository<IContestProEntities.Blog> _repository;
        private readonly IRepository<IContestProEntities.BlogResponse> _BlogResponseRepository;
        private readonly IRepository<IContestProEntities.Status> _statusRepository;
        private readonly UserManager _userManager;
        private readonly IStatusAppService _statusAppService;
        private readonly IContestProDbContextFactory _contestProDbContextFactory;

        public BlogAppService(IRepository<IContestProEntities.Blog> repository, IIocResolver  iocResolver, IStatusAppService statusAppService, IRepository<BlogResponse> BlogResponseRepository, IContestProDbContextFactory contestProDbContextFactory, UserManager userManager, IRepository<IContestProEntities.Status> statusRepository) : base(repository, iocResolver)
        {
            _repository = repository;
            _statusAppService = statusAppService;
            _BlogResponseRepository = BlogResponseRepository;
            _contestProDbContextFactory = contestProDbContextFactory;
            _userManager = userManager;
            _statusRepository = statusRepository;
        }
        public override async Task<PagedResultDto<BlogDto>> GetAll(PagedResultRequestDto input)
        {
            var query = _repository.GetAllIncluding(itm => itm.Status, itm => itm.User);
          
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);
            query = ApplyPaging(query, input);
            query = query.OrderByDescending(item => item.Id);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            return new PagedResultDto<BlogDto>(
                totalCount,
                entities.Select(MapToEntityDto).ToList()
            );
        }
        
        public async Task<PagedResultDto<BlogDtoWithDetails>> GetAllByStatusAsync(
            int page = 1,
            int pageSize = 20, string statusName = "All")
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetBlogsByStatus)
                    .WithSqlParam("StatusName", statusName)
                    .WithSqlParam("Page", page)
                    .WithSqlParam("PageSize", pageSize);
                var items = await cmd.ExecuteStoredProcedureListAsync<BlogDtoWithDetails>();
                return new PagedResultDto<BlogDtoWithDetails>(
                    items.Count > 0 ? items.First().TotalCount : 0,
                    items
                );
            }
        }

        public async Task<PagedResultDto<BlogDtoWithDetails>> SearchBlogsByTag(string tag, int page = 1, int pageSize = 20)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpSearchBlogsByTag)
                    .WithSqlParam("Tag", tag)
                    .WithSqlParam("Page", page)
                    .WithSqlParam("PageSize", pageSize);
                var items = await cmd.ExecuteStoredProcedureListAsync<BlogDtoWithDetails>();
                return new PagedResultDto<BlogDtoWithDetails>(
                    items.Count > 0 ? items.First().TotalCount : 0,
                    items
                );
            }
        }

        public async Task<IReadOnlyList<BlogTickerDto>> GetBlogTickersAsync(int pageSize = 10)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpGetBlogTickers)
                    .WithSqlParam("PageSize", pageSize);
                var items = await cmd.ExecuteStoredProcedureListAsync<BlogTickerDto>();
                return new List<BlogTickerDto>(
                    items
                );
            }
        }

        
        public override async Task<BlogDto> Create(CreateBlogDto input)
        {
            if (await _repository.CountAsync(itm => itm.Title == input.Title) > 0)
            {
                throw new UserFriendlyException("News with this title already exists.");
            }
            input.UserId = AbpSession.GetUserId();
            return await Create(input, false);
        }
        
        public async Task<BlogDto> Create(CreateBlogDto input, bool isActive)
        {
            if (await _repository.CountAsync(itm => itm.Title == input.Title) > 0)
            {
                throw new UserFriendlyException("News with this title already exists.");
            }

            StatusDto status;
            if (isActive)
            {
                status =  await _statusAppService.GetByNameAsync(AppConsts.StatusActive); 
            }
            else
            {
                status =  await _statusAppService.GetByNameAsync(AppConsts.StatusPending);
            }
            input.StatusId = status.Id;
            input.UserId = AbpSession.GetUserId();
            return await base.Create(input);
        }
       public override async Task<BlogDto> Update(BlogDto input)
        {
            var item = await _repository.UpdateAsync(input.MapTo<IContestProEntities.Blog>());
            return item.MapTo<BlogDto>();
        }
       
        public async Task<BlogResponseDto> CreateResponse(CreateBlogResponseDto input)
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusActive); 
           
            input.StatusId = status.Id;
            input.UserId = AbpSession.GetUserId();
            var item = await _BlogResponseRepository.InsertAsync(new BlogResponse
            {
                BlogId = input.BlogId,
                UserId = input.UserId,
                StatusId = input.StatusId,
                Message = input.Message
            });
            return item.MapTo<BlogResponseDto>();
        }
        
        public async Task<BlogDto> UpdateStatusAsync(UpdateStatusDto model)
        {
            var entry = await _repository.FirstOrDefaultAsync(itm => itm.Id == model.EntityId);
            var status =  await _statusAppService.GetByNameAsync(model.StatusName);
            entry.StatusId = status.Id;
            UnitOfWorkManager.Current.SaveChanges();
            return entry.MapTo<BlogDto>();
        }
        
        public async Task<BlogDto> GetByTitle(string title)
        {
            return  (await _repository.FirstOrDefaultAsync(itm => itm.Title == title)).MapTo<BlogDto>();
        }

        public async Task<BlogDtoWithDetails> GetDetailsById(int id, PagedResultRequestDto input)
        {
            var item =  await _repository.GetAll()
                .Select(
                    itm => 
                        new {
                            itm.Id, itm.Title, itm.Content, itm.UserId,
                            itm.StatusId, itm.Image, itm.Permalink, 
                            itm.CreationTime 
                        }
                )
                .Where(itm => itm.Id == id)
                .FirstOrDefaultAsync();
            var user = await _userManager.Users
                .Where(itm => itm.Id == item.UserId)
                .Select(itm => new {itm.Id,itm.NickName})
                .FirstOrDefaultAsync();
            
            var status = await _statusRepository
                .GetAll().Where(itm => itm.Id == item.StatusId)
                .Select(itm => new {itm.Id, itm.Name})
                .FirstOrDefaultAsync();
            
            var responses = await GetResponsesWithDetails(id, input);
           
            return new BlogDtoWithDetails
            {
                Id = item.Id,
                Title = item.Title,
                Content = item.Content,
                UserId = item.UserId,
                Nickname = user.NickName,
                StatusId = item.StatusId,
                StatusName = status.Name,
                Image = item.Image,
                Permalink = item.Permalink,
                CreationTime = item.CreationTime,
                Responses = responses
            };
          
        }

        public async Task<BlogDtoWithDetails> GetDetailsByPermalink(string permalink, PagedResultRequestDto input)
        {
            var item = await _repository.GetAll()
                .Select(
                    itm => 
                    new {
                        itm.Id, itm.Title, itm.Content, itm.UserId,
                        itm.StatusId, itm.Image, itm.Permalink,
                        itm.CreationTime, itm.Tags
                    }
                )
                .Where(itm => itm.Permalink == permalink)
                .FirstOrDefaultAsync();
            
            var user = await _userManager.Users
                .Where(itm => itm.Id == item.UserId)
                .Select(itm => new {itm.Id,itm.NickName})
                .FirstOrDefaultAsync();
            
            var status = await _statusRepository.GetAll()
                .Where(itm => itm.Id == item.StatusId)
                .Select(itm => new {itm.Id, itm.Name})
                .FirstOrDefaultAsync();
            
            var responses = await GetResponsesWithDetails(item.Id, input);
           
            return new BlogDtoWithDetails
            {
                Id = item.Id,
                Title = item.Title,
                Content = item.Content,
                UserId = item.UserId,
                Nickname = user.NickName,
                StatusId = item.StatusId,
                StatusName = status.Name,
                Image = item.Image,
                Permalink = item.Permalink,
                CreationTime = item.CreationTime,
                Tags = item.Tags,
                Responses = responses
            };

        }

        

        public async Task<PagedResultDto<BlogResponseDto>> GetResponses(int BlogId, PagedResultRequestDto input)
        {
            var query = _BlogResponseRepository.GetAll().Where(itm => itm.BlogId == BlogId);
            var totalCount = query.Count();
            query = query.Skip(input.SkipCount).Take(input.MaxResultCount);
            query = query.OrderByDescending(itm => itm.Id);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
           var responses = new PagedResultDto<BlogResponseDto>(
                totalCount,
                entities.Select(itm => ObjectMapper.Map<BlogResponseDto>(itm)).ToList()
            );
            return responses;
        }
        public async Task<PagedResultDto<BlogResponseDetailsDto>> GetResponsesWithDetails(int BlogId, PagedResultRequestDto input)
        {
            var query = _BlogResponseRepository.GetAll().Where(itm => itm.BlogId == BlogId);
            var totalCount = query.Count();
            query = query.Skip(input.SkipCount).Take(input.MaxResultCount);
            query = query.OrderByDescending(itm => itm.Id);
            var entities = await AsyncQueryableExecuter.ToListAsync(query);
            var outputList = new List<BlogResponseDetailsDto>();
            foreach (var entity in entities)
            {
                var user = await _userManager.Users.Where(itm => itm.Id == entity.UserId).Select(itm => new {itm.Id,itm.Image,itm.NickName}).FirstOrDefaultAsync();
                var status = await _statusRepository.GetAll().Where(itm => itm.Id == entity.StatusId).Select(itm => new {itm.Id, itm.Name}).FirstOrDefaultAsync();
                outputList.Add(new BlogResponseDetailsDto
                {
                    Id = entity.Id,
                    BlogId = BlogId,
                    UserId = entity.UserId,
                    Nickname = user.NickName,
                    UserImage = user.Image,
                    StatusId = entity.StatusId,
                    StatusName = status.Name,
                    Message = entity.Message,
                    CreationTime = entity.CreationTime
                });
                
            }
           return new PagedResultDto<BlogResponseDetailsDto>(
                totalCount, outputList
            );
            
           
        }
    }
}