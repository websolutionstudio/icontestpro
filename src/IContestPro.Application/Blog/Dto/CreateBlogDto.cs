﻿using System.ComponentModel.DataAnnotations;

namespace IContestPro.Blog.Dto
{
    public class CreateBlogDto
    {
        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        public int StatusId { get; set; }
        public long UserId { get; set; }
        public string Tags { get; set; }
        public string Image { get; set; }
        public string Permalink { get; set; }
    }
}