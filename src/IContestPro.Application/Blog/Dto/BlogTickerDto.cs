﻿namespace IContestPro.Blog.Dto
{
    public class BlogTickerDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Permalink { get; set; }
        
    }
}