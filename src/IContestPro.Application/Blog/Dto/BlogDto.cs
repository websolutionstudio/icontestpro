﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Blog.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Blog))]
    public class BlogDto : FullAuditedEntityDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public long UserId { get; set; }
        public string Tags { get; set; }
        public int StatusId { get; set; }
        public string Image { get; set; }
        public string Permalink { get; set; }
        public int? TenantId { get; set; }
        
        public StatusDto Status { get; set; }
        public UserDto User { get; set; }

        
    }
}