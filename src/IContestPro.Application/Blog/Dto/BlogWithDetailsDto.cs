﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Blog.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.Blog))]
    public class BlogDtoWithDetails : FullAuditedEntityDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public long UserId { get; set; }
        public string Nickname { get; set; }
        public string Tags { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string Image { get; set; }
        public string Permalink { get; set; }
        public int TotalCount { get; set; }

        public PagedResultDto<BlogResponseDetailsDto> Responses { get; set; }
       
        
    }
}