﻿using Abp.Application.Services.Dto;

namespace IContestPro.Blog.Dto
{
    public class BlogResponseDetailsDto : FullAuditedEntityDto
    {
        public int BlogId { get; set; }
        public long UserId { get; set; }
        public string Nickname { get; set; }
        public string UserImage { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string Message { get; set; }
        
    }
}