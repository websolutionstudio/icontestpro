﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Blog.Dto
{
    [AutoMapFrom(typeof(IContestProEntities.BlogResponse))]
    public class BlogResponseDto : FullAuditedEntityDto
    {
        public int BlogId { get; set; }
        public long UserId { get; set; }
        public int StatusId { get; set; }
        public string Message { get; set; }
        public int? TenantId { get; set; }
        
        public StatusDto Status { get; set; }
        public UserDto User { get; set; }
    }
}