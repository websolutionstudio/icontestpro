﻿using System.ComponentModel.DataAnnotations;

namespace IContestPro.Blog.Dto
{
    public class CreateBlogResponseDto
    {
        [Required]
        public int BlogId { get; set; }
        public long UserId { get; set; }
        public int StatusId { get; set; }
        [Required]
        public string Message { get; set; }
    }
}