﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using IContestPro.Status.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Blog.Dto
{
    [AutoMapTo(typeof(IContestProEntities.Blog))]
    public class EditBlogDto: FullAuditedEntityDto, IMayHaveTenant
    {
        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        public int StatusId { get; set; }
        public long UserId { get; set; }
        public string Tags { get; set; }
        public string Image { get; set; }
        public string Permalink { get; set; }

        public int? TenantId { get; set; }
        
        public StatusDto Status { get; set; }
        public UserDto User { get; set; }
        
        
    }
}