﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using IContestPro.Base;
using IContestPro.Blog.Dto;
using IContestPro.Contest.Dto;

namespace IContestPro.Blog
{
    public interface IBlogAppService : IBaseAppService<BlogDto, int, PagedResultRequestDto, CreateBlogDto, BlogDto>
    {
        Task<BlogDto> Create(CreateBlogDto input, bool isActive);
        Task<PagedResultDto<BlogDtoWithDetails>> GetAllByStatusAsync(
            int page = 1,
            int pageSize = 20, 
            string statusName = "All"
                );
        
        Task<PagedResultDto<BlogDtoWithDetails>> SearchBlogsByTag(
            string tag,
            int page = 1,
            int pageSize = 20
                );
        Task<BlogDto> UpdateStatusAsync(UpdateStatusDto model);
        Task<BlogDto> GetByTitle(string title);
        Task<BlogDtoWithDetails> GetDetailsById(int id, PagedResultRequestDto input);
        Task<BlogDtoWithDetails> GetDetailsByPermalink(string permalink, PagedResultRequestDto input);

        Task<BlogResponseDto> CreateResponse(CreateBlogResponseDto model);
        Task<IReadOnlyList<BlogTickerDto>> GetBlogTickersAsync(int pageSize = 10);
    }
}