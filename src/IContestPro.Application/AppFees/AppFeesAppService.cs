﻿using System.Threading.Tasks;
using IContestPro.Settings;

namespace IContestPro.AppFees
{
    public class AppFeesAppService :  IAppFeesAppService
    {
        private readonly ISettingsAppService _settingManager;

        public AppFeesAppService(ISettingsAppService settingManager)
        {
            _settingManager = settingManager;
        }

        public async Task<double> GetWithdrawalRequestFeePercentage()
        {
            return await _settingManager.GetSettingValueAsync<double>(AppConsts
                .SettingsWithdrawalRequestFeePercentage);
        }
        public async Task<double> GetTransferFeePercentage()
        {
            return await _settingManager.GetSettingValueAsync<double>(AppConsts
                .SettingsTransferFeePercentage);
        }
        public async Task<int> GetReferralVotePointAward()
        {
            return await _settingManager.GetSettingValueAsync<int>(AppConsts
                .SettingsReferralVotePointAward);
        }
        
        
    }
}