﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace IContestPro.AppFees
{
    public interface IAppFeesAppService : IApplicationService
    {
        Task<double> GetWithdrawalRequestFeePercentage();
        Task<double> GetTransferFeePercentage();
        Task<int> GetReferralVotePointAward();
    }
}