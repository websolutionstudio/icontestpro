﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Dependency;
using IContestPro.Search.Dto;

namespace IContestPro.Search
{
    public interface ISearchAppService : ITransientDependency
    {
        Task<IReadOnlyList<SearchDto>> SearchForHomePages(string searchType, string searchText);
    }
}