﻿namespace IContestPro.Search.Dto
{
    public class SearchDto
    {
        public string Name { get; set; }
        public string Permalink { get; set; }
    }
}