﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IContestPro.EntityFrameworkCore;
using IContestPro.EntityFrameworkCore.StoreProcedureManager;
using IContestPro.Search.Dto;

namespace IContestPro.Search
{
     public class SearchAppService : ISearchAppService
    {
        private readonly IContestProDbContextFactory _contestProDbContextFactory;

        public SearchAppService(IContestProDbContextFactory contestProDbContextFactory)
        {
            _contestProDbContextFactory = contestProDbContextFactory;
        }
        public async Task<IReadOnlyList<SearchDto>> SearchForHomePages(string searchType, string searchText)
        {
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
                var cmd = context.LoadStoredProcedure(AppStoreProcedureConsts.SpSearchForHomePages)
                    .WithSqlParam("SearchType", searchType)
                    .WithSqlParam("SearchText", searchText);
                var items = await cmd.ExecuteStoredProcedureListAsync<SearchDto>();
                return items;
            }
        }
    }
}