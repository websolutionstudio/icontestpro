﻿using Abp.Dependency;
using IContestPro.Contest;
using IContestPro.Contest.ContestEntry;
using IContestPro.Contest.Dto;
using IContestPro.Events.ContestPayment.Dto;
using IContestPro.PaymentHistories;
using IContestPro.PaymentHistories.Dto;
using IContestPro.Status;

namespace IContestPro.Events.ContestPayment
{
    public class ContestPaymentEventHandler : ITransientDependency
    {
        private readonly IContestAppService _contestAppService;
        private readonly IStatusAppService _statusAppService;
        private readonly IPaymentHistoriesAppService _paymentHistoriesAppService;
        private readonly IContestEntryAppService _contestEntryAppService;

        public ContestPaymentEventHandler(IContestAppService contestAppService, 
            IStatusAppService statusAppService,
            IPaymentHistoriesAppService paymentHistoriesAppService, IContestEntryAppService contestEntryAppService)
        {
            _contestAppService = contestAppService;
            _statusAppService = statusAppService;
            _paymentHistoriesAppService = paymentHistoriesAppService;
            _contestEntryAppService = contestEntryAppService;
        }

        public async void OnContestPaymentCompleted(object sender, ContestPaymentEventDto e)
        {
            await _contestAppService.UpdateStatusAsync(new UpdateStatusDto{EntityId = e.ContestId, StatusName = e.Status});
           
            //TODO: Make api call with reference to verify payment and get amount
            
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusSuccess);
            await _paymentHistoriesAppService.Create(new CreatePaymentHistoryDto
            {
                Amount = e.Amount,
                Description = e.Description,
                Title = e.Title,
                UserId = e.UserId,
                WalletOperation = WalletOperation.NoEffect,
                StatusId = status.Id
                
            });
        }
        public async void OnContestEntryPaymentCompleted(object sender, ContestPaymentEventDto e)
        {
            await _contestEntryAppService.UpdateStatusAsync(new UpdateStatusDto{EntityId = e.ContestId, StatusName = e.Status});
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusSuccess);

            await _paymentHistoriesAppService.Create(new CreatePaymentHistoryDto
            { Amount = e.Amount,
                Description = e.Description,
                Title = e.Title,
                UserId = e.UserId,
                WalletOperation = WalletOperation.NoEffect,
                StatusId = status.Id
                
            });
        }
    }
}