﻿using System;

namespace IContestPro.Events.ContestPayment.Dto
{
    public class ContestPaymentEventDto : EventArgs
    {
        public int ContestId{ get; set; }
        public long UserId { get; set; }
        public string Status { get; set; }
        public string PaymentHistoryStatus { get; set; }
        public string Reference { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public decimal Amount { get; set; }
    }
}