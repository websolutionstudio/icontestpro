﻿using System;
using Abp.Dependency;
using IContestPro.Events.ContestPayment.Dto;

namespace IContestPro.Events.ContestPayment
{
   
    public class ContestPaymentEvents : ITransientDependency
    {
        private event EventHandler<ContestPaymentEventDto> ContestPaymentCompleted;
        private event EventHandler<ContestPaymentEventDto> ContestEntryPaymentCompleted;
        public ContestPaymentEvents(ContestPaymentEventHandler contestPaymentEventHandler)
        {
            ContestPaymentCompleted += contestPaymentEventHandler.OnContestPaymentCompleted;
            ContestEntryPaymentCompleted += contestPaymentEventHandler.OnContestEntryPaymentCompleted;
        }
        public virtual void OnContestPaymentCompleted(ContestPaymentEventDto e)=>
            ContestPaymentCompleted?.Invoke(this, e);

        public virtual void OnContestEntryPaymentCompleted(ContestPaymentEventDto e) =>
            ContestEntryPaymentCompleted?.Invoke(this, e);


    }
}
