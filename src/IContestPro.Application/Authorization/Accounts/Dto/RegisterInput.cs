﻿using System.ComponentModel.DataAnnotations;
using Abp.Auditing;
using Abp.Authorization.Users;

namespace IContestPro.Authorization.Accounts.Dto
{
   public class RegisterInput //: IValidatableObject
    {
        [Required]
        [StringLength(50)]
        public string NickName { get; set; }
        
        [Required]
        [StringLength(20)]
        public string PhoneNumber { get; set; }

        
        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
        [DisableAuditing]
        public string Password { get; set; }

        [Required]
        public string CountryId { get; set; }
        public int? StateId { get; set; }
        
        [DisableAuditing]
        public string CaptchaResponse { get; set; }

        /*public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!UserName.IsNullOrEmpty())
            {
                if (!UserName.Equals(EmailAddress) && ValidationHelper.IsEmail(UserName))
                {
                    yield return new ValidationResult("Username cannot be an email address unless it's the same as your email address!");
                }
            }
        }*/
    }
}
