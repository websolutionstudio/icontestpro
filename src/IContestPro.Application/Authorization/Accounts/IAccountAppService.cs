﻿using System.Threading.Tasks;
using Abp.Application.Services;
using IContestPro.Authorization.Accounts.Dto;

namespace IContestPro.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
