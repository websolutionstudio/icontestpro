using System.Threading.Tasks;
using Abp.UI;
using IContestPro.Authorization.Accounts.Dto;
using IContestPro.Authorization.Users;
using IContestPro.Settings;

namespace IContestPro.Authorization.Accounts
{
    public class AccountAppService : IContestProAppServiceBase, IAccountAppService
    {
        private readonly UserRegistrationManager _userRegistrationManager;
        private readonly ISettingsAppService _settingsAppService;

        public AccountAppService(
            UserRegistrationManager userRegistrationManager, ISettingsAppService settingsAppService)
        {
            _userRegistrationManager = userRegistrationManager;
            _settingsAppService = settingsAppService;
        }

        public async Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input)
        {
            var tenant = await TenantManager.FindByTenancyNameAsync(input.TenancyName);
            if (tenant == null)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.NotFound);
            }

            if (!tenant.IsActive)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.InActive);
            }

            return new IsTenantAvailableOutput(TenantAvailabilityState.Available, tenant.Id);
        }

        public async Task<RegisterOutput> Register(RegisterInput input)
        {
            if (await _userRegistrationManager.IsPhoneNumberExists(input.PhoneNumber))
            {
                throw new UserFriendlyException("Phone number you entered is taken.");
            }
            var user = await _userRegistrationManager.RegisterAsync(
                input.NickName,
                input.PhoneNumber,
                input.EmailAddress,
                input.Password,
                false, // Assumed email address is always confirmed. Change this if you want to implement email confirmation.
                input.StateId,
                input.CountryId
            );
           
            var isEmailConfirmationRequiredForLogin = await _settingsAppService.GetSettingValueAsync<bool>(AppConsts.SettingsConfirmEmailAfterAccountCreation);

            return new RegisterOutput
            {
                CanLogin = user.IsActive && (user.IsEmailConfirmed || !isEmailConfirmationRequiredForLogin)
            };
        }
    }
}
