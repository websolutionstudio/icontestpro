﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using IContestPro.Configuration.Dto;

namespace IContestPro.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : IContestProAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
