﻿using System.Threading.Tasks;
using IContestPro.Configuration.Dto;

namespace IContestPro.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
