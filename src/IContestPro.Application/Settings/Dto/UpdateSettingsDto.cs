﻿using System.ComponentModel.DataAnnotations;

namespace IContestPro.Settings.Dto
{
    
   public class UpdateSettingsDto
    {
        [Required]
        public string NameKey { get; set; }
        [Required]
        public string Value { get; set; }
    }
}