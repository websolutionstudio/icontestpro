﻿namespace IContestPro.Settings.Dto
{
    
   public class SettingsDto
    {
        public virtual int? TenantId { get; set; }
         public virtual long? UserId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }
    }
}