﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using IContestPro.Settings.Dto;

namespace IContestPro.Settings
{
    public interface ISettingsAppService : IApplicationService
    {
        Task<SettingsDto> UpdateSettings(string nameKey, string value);
        Task<SettingsDto> Get(EntityDto<long> input);
        Task<PagedResultDto<SettingsDto>> GetAll(PagedResultRequestDto input);
        Task<T> GetSettingValueAsync<T>(string key);
    }
}