﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.UI;
using IContestPro.Helpers;
using IContestPro.Settings.Dto;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.Settings
{
    public class SettingsAppService :  DomainServiceBase, ISettingsAppService
    {
        private readonly IRepository<Setting, long> _repository;

        public SettingsAppService(IRepository<Setting, long> repository)
        {
            _repository = repository;
        }

        public async Task<SettingsDto> Get(EntityDto<long> input)
        {
            var query = await  _repository.GetAll().FirstOrDefaultAsync(itm => itm.Id == input.Id);
            return query?.MapTo<SettingsDto>();
        } 
        public async Task<T> GetSettingValueAsync<T>(string key)
        {
            var query = await  _repository.GetAll().FirstOrDefaultAsync(itm => itm.Name == key);
            if (query == null)
            {
                    throw new UserFriendlyException($"No settings found with {key} value.");
            }
            var value = query.Value.ConvertValue<T>();
           
            return value;
        }
        public async Task<PagedResultDto<SettingsDto>> GetAll(PagedResultRequestDto input)
        {
            var query =  _repository.GetAll();
            var totalCount = await query.CountAsync();
            query = query.Skip(input.SkipCount).Take(input.MaxResultCount);
            var items = await query.ToListAsync();
            return new PagedResultDto<SettingsDto>(
                totalCount,
                items.MapTo<List<SettingsDto>>()
            );
        }
        public async Task<SettingsDto> UpdateSettings(string nameKey, string value)
        {
            var item = await _repository.FirstOrDefaultAsync(itm => itm.Name == nameKey);
            if (item == null)
            {
                throw new UserFriendlyException($"No settings found with {nameKey} value.");
            }
            item.Value = value;
            item = await _repository.UpdateAsync(item);
            await CurrentUnitOfWork.SaveChangesAsync();
            return item.MapTo<SettingsDto>();
        }
       
        
    }
}