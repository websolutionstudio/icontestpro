﻿using Abp.Application.Services;

namespace IContestPro.Helpers
{
    public interface IUtilityHelperAppService : IApplicationService
    {
        bool DeleteFile(string fileName);
        string ToNearestNumberString(int item);
    }
}