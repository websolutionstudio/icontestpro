﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace IContestPro.Helpers.CustomPermissionChecker
{
    public interface ICustomPermissionChecker : IApplicationService
    {
        Task CheckPermissionForUserOrRole(string permission);
    }
}