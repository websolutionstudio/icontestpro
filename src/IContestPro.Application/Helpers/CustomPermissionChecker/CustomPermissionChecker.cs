﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Domain.Repositories;
using IContestPro.Authorization.Roles;
using IContestPro.Authorization.Users;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.Helpers.CustomPermissionChecker
{
    public class CustomPermissionChecker : ApplicationService, ICustomPermissionChecker
    {
        private readonly IPermissionChecker _permissionChecker;
        private readonly RoleManager _roleManager;
        private readonly UserManager _userManager;
        private readonly IRepository<PermissionSetting, long> _permissionRepository;

        public CustomPermissionChecker(IPermissionChecker permissionChecker, RoleManager roleManager, UserManager userManager, IRepository<PermissionSetting, long> permissionRepository)
        {
            _permissionChecker = permissionChecker;
            _roleManager = roleManager;
            _userManager = userManager;
            _permissionRepository = permissionRepository;
        }

        public async Task CheckPermissionForUserOrRole(string permission)
        {
            /**
             * Check whether role is granted to user
             */
            if (!_permissionChecker.IsGranted(permission))
            {
               var user = await _userManager.Users.FirstOrDefaultAsync(usr => usr.Id == AbpSession.UserId);
               var userRoles = await _userManager.GetRolesAsync(user);
                if (userRoles == null || !userRoles.Any())
                {
                throw new AbpAuthorizationException("You are not authorized to perform this action!");
                }
                foreach (var userRole in userRoles)
                {
                    var role = _roleManager.GetRoleByNameAsync(userRole);
                    var userGrantedPermissions = _permissionRepository.GetAll().IgnoreQueryFilters()
                        .OfType<RolePermissionSetting>()
                        .Where(p => p.TenantId == AbpSession.TenantId && p.RoleId == role.Id)
                        .Select(p => p.Name)
                        .ToList();
                    if (userGrantedPermissions.Contains(permission))
                    {
                        break;
                    }
                }
                throw new AbpAuthorizationException("You are not authorized to perform this action!");

            }
        }
    }
}