﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace IContestPro.Helpers
{
    public class UtilityHelperAppService : IUtilityHelperAppService
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public UtilityHelperAppService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public bool DeleteFile(string fileName)
        {
            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploads");
            var filePath = Path.Combine(uploads, fileName);
            if (File.Exists(filePath))
            {
               File.Delete(filePath);
                return true;
            }
            return false;
        }
        public static string ConvertToNearestNumberString(int item) {
            if (item < 1000){
                return item.ToString();
            }
            if (item >= 1000 && item < 1000000){
                var number = (double)item / (double)1000;
                return Math.Round(number, 1) + "K";
            }
            if (item >= 1000000 && item < 1000000000){
                var number1 = (double)item / (double)1000000;
                return Math.Round(number1, 1) + "M";
            }
            if (item >= 1000000000){
                var number2 = (double)item / (double)1000000000;
                return Math.Round(number2, 1) + "B";
            }
            return "";
        }
        public string ToNearestNumberString(int item)
        {
            return ConvertToNearestNumberString(item);
        }
    }
}