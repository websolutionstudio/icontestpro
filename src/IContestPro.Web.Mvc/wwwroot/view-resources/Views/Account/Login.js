﻿$(function () {
    $('#ReturnUrlHash').val(location.hash);

    var $loginForm = $('#LoginForm');

    $loginForm.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.input-group').append(error);
        }
        
    });

    $loginForm.submit(function (e) {
        /*e.preventDefault();

        if (!$loginForm.valid()) {
            return;
        }

        abp.ui.setBusy(
            $('.login-box'),

            abp.ajax({
                contentType: 'application/x-www-form-urlencoded',
                url: $loginForm.attr('action'),
                data: $loginForm.serialize(),
                success: function (response) {
                    if (response !== null){
                        if (response.error !== null){
                            if (response.type && response.type === 'EmailConfirmation'){
                                abp.message.error(
                                    response.error.message, 'Email Confirmation Required'
                                )   ;
                                $('.email-activation-link').show();
                            }
                        }
                    } 
                    
                    abp.ui.clearBusy($('.login-box'));
                }
            })
        );*/
        
    });

    $('a.social-login-link').click(function () {
        var $a = $(this);
        var $form = $a.closest('form');
        $form.find('input[name=provider]').val($a.attr('data-provider'));
        $form.submit();
    });

    $loginForm.find('input[type=text]:first-child').focus();
});
