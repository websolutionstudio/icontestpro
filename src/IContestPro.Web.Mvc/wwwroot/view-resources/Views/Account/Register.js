﻿(function ($) {

    if (!$) {
        return;
    }

    $(function () {

        var $registerForm = $('#RegisterForm');

        $.validator.addMethod("customUsername", function (value, element) {
            if (value === $registerForm.find('input[name="EmailAddress"]').val()) {
                return true;
            }

            //Username can not be an email address (except the email address entered)
            return !$.validator.methods.email.apply(this, arguments);
        }, abp.localization.localize("RegisterFormUserNameInvalidMessage", "IContestPro"));
        
        $registerForm.validate({
            rules: {
                UserName: {
                    required: true,
                    customUsername: true
                }
            },

            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },

            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },

            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });
        setTimeout(function() {
            $('#CountryId').val('NG');
            $('#CountryId').trigger('change');
        }, 1000);
        
        $('#CountryId').on('change', function() {
            var id = $(this).val();
            if (id === 'NG'){
                $('#state-select-wrapper').slideDown(500);
                $('#state-select-wrapper .state-select').removeAttr('disabled');
                abp.ui.setBusy(
                    $('#RegisterForm'),
                    $.getJSON('/Countries/States/' + id, function (response) {
                        var states = '';
                        for (var index in response.result.items){
                            states += '<option value="' + response.result.items[index].id + '">'+ response.result.items[index].name +'</option>'
                        }
                        $('#StateId').html(states);
                        $('#StateId').val($('#selected-state').val());
                        abp.ui.clearBusy();
                    })
                );  
            }else{
                $('#state-select-wrapper').slideUp(500);
                $('#state-select-wrapper .state-select').attr('disabled', 'disabled');
               
            } 
           
        });
        
        $('.register-view-password').on('mousedown', function () {
            $(this).removeClass('fa-eye-slash').addClass('fa-eye');
           $('#password').attr('type','text');
        });
        $('.register-view-password').on('mouseup', function () {
            $(this).removeClass('fa-eye').addClass('fa-eye-slash');
            $('#password').attr('type','password');
        });
    });

})(jQuery);