var $angularScope = {};
var app = angular.module('myContestApp', []);
app.controller('sponsorController', function($scope, $interval, $http) {
    var vm = $scope;
    $interval(function () {
        //alert(vm.sponsorId);
        vm.follow = function (id) {
            if (abp.session.userId === null){
                abp.message.error('Please login to follow this sponsor.');
                return;
            }
            $('.follow-button i').addClass('fa-spinner fa-spin').removeClass('fa-plus');
            $.post('/Sponsor/Follow',
                {FollowerUserId: abp.session.userId, SponsorId : id}, function (data) {
                    if (data.result.success){
                        abp.notify.success(data.result.message);
                    }else{
                        abp.message.error(data.result.message);
                    }
                    $('.follow-button i').addClass('fa-plus').removeClass('fa-spinner fa-spin');

                });

        };
        $angularScope = vm;
    }, 500);


});
$(function () {
    

});
