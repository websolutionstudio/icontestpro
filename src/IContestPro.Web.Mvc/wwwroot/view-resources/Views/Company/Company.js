﻿var fileInput = null;
var logoFileInput = null;
var uploadTypeSelected = 'banner';
var uploadClass = null;
$(function () {
    fileInput = $("#banner-file-browse");
    logoFileInput = $("#logo-file-browse");
    //Change id to your id
    var _URL = window.URL || window.webkitURL;
    $(fileInput).on("change", function (e) {
        var file = $(this)[0].files[0];
         uploadClass = new Upload(file, 'banner');
        if (uploadClass.getType() !== 'image/jpeg' && uploadClass.getType() !== 'image/png'){
            abp.message.error('Please upload image file. Supported image formats (jpg, png)');
        }else {
            var img = new Image();
            img.onload = function () {
                if (img.width !== 880 || img.height !== 150) {
                    $(fileInput).val('');
                    uploadClass = null;
                    abp.message.error('Please resize your image to 880px by 150px');
                    return;
                }
                uploadClass.doUpload();
                
            };
            img.src = _URL.createObjectURL(file);
            // maby check size or type here with upload.getSize() and upload.getType()
            // execute upload
        }
    });
    $(logoFileInput).on("change", function (e) {
        var file = $(this)[0].files[0];
        uploadClass = new Upload(file, 'logo');
        if (uploadClass.getType() !== 'image/jpeg' && uploadClass.getType() !== 'image/png'){
            abp.message.error('Please upload image file. Supported image formats (jpg, png)');
        }else {
            var img = new Image();
            img.onload = function () {
                if (img.width !== 150 || img.height !== 150) {
                    $(logoFileInput).val('');
                    uploadClass = null;
                    abp.message.error('Please resize your image to 150px by 150px');
                    return;
                }
                uploadClass.doUpload();

            };
            img.src = _URL.createObjectURL(file);
            // maby check size or type here with upload.getSize() and upload.getType()
            // execute upload
        }
    });
    $('#save-banner').on('click', function () {
        if (uploadClass === null){
            abp.message.error('Please select image file to upload.');
        }else{
            uploadClass.doUpload();
        } 
    });
    $('#save-logo').on('click', function () {
        if (uploadClass === null){
            abp.message.error('Please select image file to upload.');
        }else{
            uploadClass.doUpload();
        } 
    }); 
    $('.delete-banner').on('click', function () {
        abp.message.confirm('Are you sure you want to delete?', function (result) {
            if (result) {
                $.post('/Sponsor/DeleteFile', {filename: $('#banner-img-input').val()}, function (data) {

                    if (data.result.success) {
                        abp.notify.success(data.result.message);
                        $('#banner-thumnail-wrapper').hide();
                    } else {
                        abp.message.error(data.result.message);
                    }
                });
            }
        });
    });
    
    $('.delete-logo').on('click', function () {
        abp.message.confirm('Are you sure you want to delete?', function (result) {
            if (result) {
                $.post('/Sponsor/DeleteFile', {filename: $('#logo-img-input').val()}, function (data) {

                    if (data.result.success) {
                        abp.notify.success(data.result.message);
                        $('#logo-thumnail-wrapper').hide();
                    } else {
                        abp.message.error(data.result.message);
                    }
                });
            }
        });
    });

    var $companyForm = $('#company-form');

    $companyForm.submit(function (e) {
        if ($('#banner-img-input').val() === ''){
            e.preventDefault();
            abp.message.warn('Please select and save a banner.');
        }else if ($('#logo-img-input').val() === ''){
            e.preventDefault();
            abp.message.warn('Please select and save a logo.');
        }
    });
});

var Upload = function (file, type) {
    this.file = file;
    this.uploadType = type === undefined ? uploadTypeSelected : type;
};

Upload.prototype.getType = function() {
    return this.file.type;
};
Upload.prototype.getSize = function() {
    return this.file.size;
};
Upload.prototype.getName = function() {
    return this.file.name;
};
Upload.prototype.getUploadType = function() {
    return this.uploadType;
};
Upload.prototype.saveSelected = function() {
    return this.doUpload();
};
var progressBarId = '';
Upload.prototype.progressHandling = function (event) {
    var percent = 0;
    var position = event.loaded || event.position;
    var total = event.total;
    if (uploadClass.getUploadType() === 'banner'){
         progressBarId = '#banner-progress-wrp';
    } else if (uploadClass.getUploadType() === 'logo'){
         progressBarId = "#logo-progress-wrp";
    }
    if (event.lengthComputable) {
        percent = Math.ceil(position / total * 100);
    }
    // update progressbars classes so it fits your code
    $(progressBarId).fadeIn(500);
    if (uploadClass.getUploadType() === 'banner'){
        $(progressBarId + " .banner-progress-bar").css("width", +percent + "%");
        $(progressBarId + " .banner-status").text(percent + "%");
        if (percent >= 100){
            $(progressBarId + " .banner-progress-bar").css("background-color", "green");
            $(progressBarId + " .banner-status").css("color",  '#fff');
            
        }
    } else if (uploadClass.getUploadType() === 'logo'){
        $(progressBarId + " .logo-progress-bar").css("width", +percent + "%");
        $(progressBarId + " .logo-status").text(percent + "%");
        if (percent >= 100){
            $(progressBarId + " .logo-progress-bar").css("background-color", "green");
            $(progressBarId + " .logo-status").css("color",  '#fff');
        } 
    }
};
Upload.prototype.doUpload = function () {
    var that = this;
    var formData = new FormData();
if (this.file === undefined || this.file === null){
    abp.message.error('Please select image file to upload.');
    return;
} 
    // add assoc key values, this will be posts values
    formData.append("file", this.file, this.getName());
    if (uploadClass.getUploadType() === 'banner') {
        formData.append("removeImage", $('#banner-img-input').val());
        abp.ui.setBusy($('.compnay-banner-wrapper'));
    } else if (uploadClass.getUploadType() === 'logo') {
        formData.append("removeImage", $('#logo-img-input').val());
        abp.ui.setBusy($('.compnay-logo-wrapper'));
    }
    $.ajax({
        type: "POST",
        url: "/Sponsor/UploadFile",
        xhr: function () {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress', that.progressHandling, false);
            }
            return myXhr;
        },
        success: function (data) {
            if (!data.result.success){
                abp.message.error(data.result.message)
            }else{
                abp.notify.success(data.result.message);
               
                if (uploadClass.getUploadType() === 'banner') {
                    $(fileInput).val('');
                    abp.ui.clearBusy($('.compnay-banner-wrapper'));
                    $('#banner-img-input').val(data.result.filename);
                    $('#banner-thumnail').attr('src','/images/uploads/' + data.result.filename);
                    $('#banner-thumnail-wrapper').show();
                } else if (uploadClass.getUploadType() === 'logo') {
                    $(logoFileInput).val('');
                    $('#logo-img-input').val(data.result.filename);
                    $('#logo-thumnail').attr('src','/images/uploads/' + data.result.filename);
                    $('#logo-thumnail-wrapper').show();
                    abp.ui.clearBusy($('.compnay-logo-wrapper'));
                }
                uploadClass = null;
                $(progressBarId).fadeOut(10000);
            }
           
        },
        error: function (error) {
            // handle error
        },
        async: true,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        timeout: 60000
    });
};


