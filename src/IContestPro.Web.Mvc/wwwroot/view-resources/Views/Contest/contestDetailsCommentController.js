var $angularScope = {};
var app = angular.module('myContestApp', []);
app.directive('imageExists', function($http) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            attrs.$observe('ngSrc', function(ngSrc) {
                $http.get(ngSrc).then(function(){
                }, function(){
                    element.attr('src', '/images/user.png'); // set default image
                });
            });
        }
    };
});
app.factory('Utils', function($q) {
    return {
        isImage: function(src) {
            var deferred = $q.defer();

            var image = new Image();
            image.onerror = function() {
                deferred.resolve(false);
            };
            image.onload = function() {
                deferred.resolve(true);
            };
            image.src = src;

            return deferred.promise;
        }
    };
});
app.controller('contestDetailsCommentController', function($scope, $interval, $http, Utils) {
    var vm = $scope;
    vm.comments = [];
    vm.contestId = null;
    vm.userId = 0;
    vm.nickname = '';
    vm.profileImage = '';
    vm.comment = '';
    vm.processing = false;
    vm.errorLogin = null;
    vm.errorMessage = null;
    vm.page = 1;
    vm.pageSize = 20;
    vm.totalCount = 0;
    vm.endOfItems = false;
    $interval(function () {
        /* abp.ui.setBusy('body');
         abp.message.warn('body');
         abp.notify.success('body');*/
        vm.getComments = function () {
            if (vm.processing === false && vm.endOfItems === false) {
                vm.processing = true;
                //abp.ui.setBusy('#comment-wrp');
                    $.get('/ContestComment/Comments?contestId=' + vm.contestId + '&page=' + vm.page + '&pageSize=' + vm.pageSize,
                        function (data) {
                            if (data.result.success){
                                if (data.result.data.length > 0){
                                    data.result.data.forEach(function (item) {
                                        vm.comments.push(item)
                                    });
                                    vm.processing = false;
                                    vm.totalCount = data.result.totalCount;
                                    vm.page++;

                                }else{
                                    vm.endOfItems = true;
                                    vm.processing = false;
                                }
                               // console.log('Request finished.');
                               // abp.ui.clearBusy('#comment-wrp');
                            }else{
                                abp.message.error(data.result.message);
                            }
                        });
               
               
            }

        };
        vm.postComments = function () {
            if (vm.userId === 0 || vm.userId === undefined){
                vm.errorLogin = 'Please login to comment.';
                return;
            } else if (vm.comment === ''){
                abp.message.warn('Please enter your comment.');
                return;
            }
            $.post('/ContestComment/Create',
                {ContestId: vm.contestId, Comment : vm.comment},function (data) {
                    if (data.result.success){
                        abp.notify.success('Comment posted successfully.');
                        vm.comment = '';
                        vm.processing = false;
                        vm.endOfItems = false;
                        vm.comments = [];
                        vm.page = 1;
                        vm.getComments();
                    }else{
                        abp.message.error(data.result.message);
                    }

                });

        };
       /* vm.isImage(src).then(function(data) {
            console.log(data);
        });*/
        $angularScope = vm;
    }, 500);
  
    
});
$(function () {
    var element_position = $('#author-post').offset().top;
    $(window).on('scroll', function() {
        var y_scroll_pos = window.pageYOffset;
        if(y_scroll_pos > element_position) {
            if (!$angularScope.processing) 
            $angularScope.getComments()
        }
    });
   
});
