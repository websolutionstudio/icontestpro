var $angularScope = {};
var app = angular.module('myContestApp', []);
app.controller('contestController', function($scope, $interval, $http) {
    var vm = $scope;
    vm.userIdToFollow = 0;
    $interval(function () {
        //alert(vm.userIdToFollow);
        vm.follow = function (id) {
            if (abp.session.userId === null){
                abp.message.error('Please login to follow this user.');
                return;
            } else if (vm.userIdToFollow === abp.session.userId){
                abp.message.error('You cannot follow yourself.');
                return;
            } 
            $.post('/Users/Follow',
                {FollowerUserId: abp.session.userId, FollowingUserId : id}, function (data) {
                    if (data.result.success){
                        abp.notify.success(data.result.message);
                    }else{
                        abp.message.error(data.result.message);
                    }

                });

        };
        $angularScope = vm;
    }, 500);


});
$(function () {
    

});
