var $angularScope = {};
var app = angular.module('myContestApp', []);
app.controller('entriesController', function($scope, $interval, $http) {
    var vm = $scope;
    vm.contestEntryId = null;
    vm.contestId = null;
    vm.voteTypeName = '';
    vm.voteAmount = 0;
    vm.votePoint = 0;
    vm.totalVotes = 0;
    vm.totalComments = 0;
    vm.userId = abp.session.userId;
     vm.votingProcessing = false;
    $interval(function () {
        vm.vote = function (contestId, contestEntryId, voteTypeName, voteAmount, votePoint, totalVotes, totalComments) {
            if (contestEntryId === undefined ||  contestId === undefined || voteTypeName === undefined || voteAmount === undefined || votePoint === undefined) {
                    abp.message.error('Required fields not supplied. Please refersh your page and try again.');
                    return;
            }
            vm.contestId = contestId;
            vm.contestEntryId = contestEntryId;
            vm.voteTypeName = voteTypeName;
            vm.voteAmount = voteAmount;
            vm.votePoint = votePoint;
            vm.totalVotes = totalVotes;
            vm.totalComments = totalComments;
            if (vm.voteTypeName !== 'Open') {
                vm.isLoginRequired = abp.session.userId === null;
                if (abp.session.userId === null) {
                    abp.message.error('Please login to vote for this entry.');
                    return;
                }
            }
            if (vm.voteTypeName === 'Paid') {
                abp.message.confirm('This vote requires you to pay NGN' + vm.voteAmount + ' which will be deducted from your wallet. Do you want to continue?', function (result) {
                    if (!result){
                        return;
                    }
                    if (!vm.votingProcessing){
                        vm.votingProcessing = true;
                        abp.ui.setBusy('#entry-' + vm.contestEntryId);
                        $.get('/ContestEntry/Vote?contestEntryId=' + contestEntryId + '&contestId=' + contestId+ '&userId=' + abp.session.userId,
                            function (data) {
                                if (data.result.success){
                                    vm.totalVotes++;
                                    $('#entry-votes-' + vm.contestEntryId).html(vm.totalVotes);
                                    abp.notify.success(data.result.message);
                                }else{
                                    abp.message.error(data.result.message);
                                }
                                vm.votingProcessing = false;
                                abp.ui.clearBusy('#entry-' + vm.contestEntryId);

                            });
                    }else{
                        abp.notify.error('Please wait for voting to complete.');
                    }
                });
            }else if (vm.voteTypeName === 'VotePoint'){
                abp.message.confirm('This contest requires you to have ' + vm.votePoint + ' vote points to complete your vote. Do you want to continue?', function (result) {
                    if (!result) {
                        return;
                    }
                    if (!vm.votingProcessing){
                        vm.votingProcessing = true;
                        abp.ui.setBusy('#entry-' + vm.contestEntryId);

                        $.get('/ContestEntry/Vote?contestEntryId=' + contestEntryId + '&contestId=' + contestId + '&userId=' + abp.session.userId,
                            function (data) {
                                if (data.result.success) {
                                    vm.totalVotes++;
                                    $('#entry-votes-' + vm.contestEntryId).html(vm.totalVotes);
                                    abp.notify.success(data.result.message);
                                } else {
                                    abp.message.error(data.result.message);
                                }
                                vm.votingProcessing = false;
                                abp.ui.clearBusy('#entry-' + vm.contestEntryId);

                            });
                    }else{
                        abp.notify.error('Please wait for voting to complete.');
                    }
                });
            } else{
                var query = '';
                if (abp.session.userId === null){
                    query = 'contestEntryId=' + contestEntryId + '&contestId=' + contestId;

                } else{
                    query = 'contestEntryId=' + contestEntryId + '&contestId=' + contestId
                        + '&userId=' + abp.session.userId;
                }
                if (!vm.votingProcessing){
                    vm.votingProcessing = true;
                    abp.ui.setBusy('#entry-' + vm.contestEntryId);

                    $.get('/ContestEntry/Vote?' + query,
                        function (data) {
                            if (data.result.success){
                                vm.totalVotes++;
                                $('#entry-votes-' + vm.contestEntryId).html(vm.totalVotes);
                                abp.notify.success(data.result.message);
                            }else{
                                abp.message.error(data.result.message);
                            }
                            vm.votingProcessing = false;
                            abp.ui.clearBusy('#entry-' + vm.contestEntryId);

                        });
                }else{
                    abp.notify.error('Please wait for voting to complete.');
                }
            }
        };
        
        $angularScope = vm;
    }, 500);


});
jQuery(function ($) {
    setTimeout(function(){
        console.log($angularScope);

    },1000);

    
    
});
var interValItem = null;
 videoPlayingHandler = function(event) {
    clearInterval(interValItem);
    console.log(event);
    interValItem =  setInterval(function() {
        console.log(event.currentTime);
        if (event.currentTime >= 3) {
            clearInterval(interValItem);
            $.ajax({
                type: "POST",
                url: "/api/services/app/ContestEntryView/Create",
                data: {"contestEntryId": $angularScope.contestEntryId, 'userId' : abp.session.userId },
                contentType: "application/json;",
                dataType: "json",
                success: function(data){
                    if (data.success){
                        $angularScope.totalViews++;
                    }
                },
                failure: function(errMsg) {

                }
            });
            /* $.ajax('/api/services/app/ContestEntryView/Create', 
                 {"contestEntryId": 1, 'userId' : abp.session.userId }
                 );*/
        }
    },1000);
};
