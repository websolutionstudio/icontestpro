
$(function() {
     $('.multiple-val-input').on('click',
        function() {
            $(this).find('input:text').focus();
        });
    $('.multiple-val-input ul input:text').on('input propertychange',
        function() {
            $(this).siblings('span.input_hidden').text($(this).val());
            var inputWidth = $(this).siblings('span.input_hidden').width();
            $(this).width(inputWidth);
        });
    $(document).on('click',
        '.suggested-recipients-wrapper a',
        function() {
            var toAppend = $(this).html();
            var token = parseInt($(this).data('token'));
            var itemSelected = undefined;
            if (toAppend !== '') {
                var selectedValues = $('#multival-recipients-input').val();
                if (selectedValues === '') {
                    $('#multival-recipients-input').val(token);
                } else {
                    var items = selectedValues.split(',');
                    itemSelected = items.find(item => parseInt(item) === token);
                    if (itemSelected === undefined) {
                        selectedValues += ',' + token;
                        $('#multival-recipients-input').val(selectedValues);
                    }
                }
                if (itemSelected === undefined) {
                    $('<li><a data-token="' + token + '">×</a><div>' + toAppend + '</div></li>')
                        .insertBefore($('.multiple-val-input ul input:text'));
                }
                if ($('.multiple-val-input ul input:text').val() !== '') {
                    getCurrentyUserFollowers('');
                }
                $('.multiple-val-input ul input:text').val('');
                $('.multiple-val-input ul input:text').width("70");
            } else {
                return false;
            }
            return false;
        });

    $(document).on('click',
        '.multiple-val-input ul li a',
        function(e) {
            e.preventDefault();
            var token = parseInt($(this).data('token'));
            var selectedValues = $('#multival-recipients-input').val();
            var items = selectedValues.split(',');
            items = items.filter(item => parseInt(item) !== token);
            $('#multival-recipients-input').val(items.join(','));
            $(this).parents('li').remove();
        });
    $('.multiple-val-input ul input:text').on('keyup',
        function() {
            var toAppend = $(this).val();
            if (toAppend !== '' && toAppend.length >= 4) {
                getCurrentyUserFollowers(toAppend);
            } else {
                if (toAppend === '') {
                    getCurrentyUserFollowers('');
                }
            }
        });
    function getCurrentyUserFollowers(nickNameSuggestion) {
        nickNameSuggestion = nickNameSuggestion || '';
        $('.loading-followers').addClass('fa-spinner fa-spin');
        $.ajax({
            url: abp.appPath + 'Users/GetCurrentUserFollowers?nickNameSuggestion=' + nickNameSuggestion,
            type: 'POST',
            contentType: 'application/html',
            success: function(content) {
                $('.suggested-recipients-wrapper').html(content);
                $('.loading-followers').removeClass('fa-spinner fa-spin');
            },
            error: function(e) {
                $('.loading-followers').removeClass('fa-spinner fa-spin');
            }
        });
    };
    getCurrentyUserFollowers();
    
});

