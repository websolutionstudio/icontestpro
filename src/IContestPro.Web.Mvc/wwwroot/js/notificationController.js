var $angularScope = {};
var app = angular.module('myContestApp', []);
app.filter('unsafe', function($sce) { return $sce.trustAsHtml; });
app.controller('notificationController', function($scope, $interval, $http) {
    var vm = $scope;
    vm.totalUnread = 0;
    vm.userId = abp.session.userId;
    vm.notifications = [];
    vm.formattedNotifications = [];
    vm.checkingMessages = true;
    vm.processing = false;
    $interval(function () {
        vm.getNotifications = function () {
            if (!vm.userId) {
                return;
            }
            if (!vm.processing) {
            vm.checkingMessages = true;
                vm.processing = true;
            $.get('/Notifications/Unread',
                function (data) {
                    if (data.success) {
                        vm.formattedNotifications = [];
                        vm.notifications = [];
                        data.result.value.forEach(function (item) {
                            vm.notifications.push(item);
                            if (item.notification.data.type === 'Abp.Notifications.LocalizableMessageNotificationData') {
                                var localizedText = abp.localization.localize(
                                    item.notification.data.message.name,
                                    item.notification.data.message.sourceName
                                );
                                
                                $.each(item.notification.data.properties, function (key, value) {
                                    localizedText = localizedText.replace('{' + key + '}', value);
                                });
                                vm.formattedNotifications.push({id: item.id, message: localizedText});
                            } else if (item.notification.data.type === 'Abp.Notifications.MessageNotificationData') {
                                vm.formattedNotifications.push({id: item.id, message: item.notification.data.message});
                            }
                        });
                       
                        $('.notifications-wrapper').show();
                        $('.notification-badge').hide();
                        
                        if (vm.formattedNotifications.length > 0) {
                            vm.markRead();
                        }
                    }
                    vm.checkingMessages = false;
                    vm.processing = false;

                });
        }
        };
        vm.countUnread = function () {
            if (!vm.userId) {
                return;
            }
            $.get('/Notifications/CountUnread',
                function (data) {
                console.log(data);
                    if (data.success) {
                        vm.totalUnread = data.result.value;
                        if (vm.totalUnread > 0){
                            $('.notification-badge').show();
                        } 
                    }
                });
        };
        vm.markRead = function () {
            if (!vm.userId) {
                return;
            }
            var ids = [];
            vm.notifications.forEach(function (item) {
                ids.push(item.id);
                console.log(item);

            });
            $.post('/Notifications/MarkReadList',{ids: ids});
        }; 
        vm.delete = function (id) {
            $.post('/Notifications/Delete', {id: id}, function (data) {
                vm.formattedNotifications = vm.formattedNotifications.filter(function(item){return item.id !== id});
            });
        };
        $angularScope = vm;

    }, 500);
});
           



jQuery(function ($) {
    setTimeout(function(){
        $angularScope.countUnread();
        $('#check-messages').click(function () {
            $('.notification-badge').removeClass('fadeIn');
            $angularScope.getNotifications();
        });
        $('.close-notification').click(function () {
            $('.notifications-wrapper').hide();
        });
    },1000);

    
    
});
