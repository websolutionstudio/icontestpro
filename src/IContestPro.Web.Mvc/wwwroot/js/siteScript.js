﻿(function($) {
    "use strict";

    /************************************************************************************
 * validate image width and height of file input
 ***********************************************************************************/
var _URL = window.URL || window.webkitURL;
$('input[type=file]').on("change", function (e) {
    var $this = $(this);
    if ($this.data('width') && $this.data('height') && $this.attr('required')){
        var file = $(this)[0].files[0];
        if (file.type !== 'image/jpeg' && file.type !== 'image/png'){
            $this.closest('form').find('button[type=submit]').attr('disabled','disabled');
            $this.closest('form').find('input[type=submit]').attr('disabled','disabled');
            abp.message.error('Please select image file. Supported image formats (jpg, png)');
        } else {
            var img = new Image();
            img.onload = function () {
                var width = parseInt($this.data('width'));
                var height = parseInt($this.data('height'));
                if (img.width !== width  || img.height !== height) {
                    $this.val('');
                    abp.message.error('Please resize your image to ' + width + 'px by ' + height +'px');
                    $this.closest('form').find('button[type=submit]').attr('disabled','disabled');
                    $this.closest('form').find('input[type=submit]').attr('disabled','disabled');
                }else{
                    $this.closest('form').find('button[type=submit]').removeAttr('disabled');
                    $this.closest('form').find('input[type=submit]').removeAttr('disabled');
                }
            };
            img.src = _URL.createObjectURL(file);
            // maby check size or type here with upload.getSize() and upload.getType()
            // execute upload
        }
    }

});
if ( $("#future_date")[0]) {
    $("#future_date").countdowntimer({
        startDate: "2018/10/16 00:00:00", //set server date and time as "<?php echo date('Y/m/d H:i:s'); ?>".
        dateAndTime: "2018/10/31 00:00:00", //end date
        size: "lg",
        regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
        regexpReplaceWith: "$1<sup>d</sup> - $2<sup>h</sup> - $3<sup>m</sup> - $4<sup>s</sup>",
        timeUp: function () {
            
        }
    });
}
/* var $validateForms = $('form');
if ($validateForms[0]){
    $validateForms.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.input-group').append(error);
        }
    });
} */
   
    /*var $inputWithLengthAttribute = $('input[maxlength], textarea[maxlength]');
    $inputWithLengthAttribute.on('keyup', function () {
        var maxLength = $(this).attr('maxlength');
        var textInField = $(this).val().length;
        var remainingText = maxLength - textInField;
        var container = $(this).parent().find('.input-length-counter-wrp');
        container.show().stop().fadeIn(50);
        if (!container[0]){
            $(this).parent().append('<small class="pull-right input-length-counter-wrp"><span class="input-length-counter">Remaning: ' + remainingText + ' of ' + maxLength + '</span></small>\n');
        } else{
            container.find('.input-length-counter').html('Remaning: ' + remainingText + ' of ' + maxLength);
        }
        $('.input-length-counter-wrp').fadeOut(10000);
        
    });*/

    $('.reveal-password').on('mousedown', function () {
        $(this).removeClass('fa-eye-slash').addClass('fa-eye');
        $('#reveal-password-field').attr('type','text');
    });
    $('.reveal-password').on('mouseup', function () {
        $(this).removeClass('fa-eye').addClass('fa-eye-slash');
        $('#reveal-password-field').attr('type','password');
    });

    //<Todo: search field in pages>
    $('.sidebar-search .search-input').focus(function() {
       $(this).attr('placeholder', 'Type Search...');
    });
    $('.sidebar-search .search-input').blur(function() {
        $(this).attr('placeholder', 'Search Here...');
    });

    var searchWaitTime = null;
    $('.sidebar-search .search-input').keyup(function() {
        clearTimeout(searchWaitTime);
        if ($(this).val().length >= 3){
            var searchText =  $(this).val();
            searchWaitTime = setTimeout(function () {
                $('.sidebar-search .search-button i').removeClass('fa-search');
                $('.sidebar-search .search-button i').addClass('fa-spinner fa-spin');
                var searchType = $('.search-cretieria-wrapper input[type=radio]:checked').val();
                $.ajax({
                    url: abp.appPath + 'Home/Search?searchType=' + searchType + '&searchText=' + searchText,
                    type: 'POST',
                    contentType: 'application/html',
                    success: function (content) {
                        $('.search-results-wrapper').html(content);
                        $('.sidebar-search .search-button i').removeClass('fa-spinner fa-spin');
                        $('.sidebar-search .search-button i').addClass('fa-search');

                    },
                    error: function (e) {
                        $('.sidebar-search .search-button i').removeClass('fa-spinner fa-spin');
                        $('.sidebar-search .search-button i').addClass('fa-search');
                    }
                }); 
            }, 50);
             
        }
        
    });
    $('.search-cretieria-wrapper input[type=radio]').change(function () {
        $('.sidebar-search .search-input').trigger('keyup');
    });
    $('.search-results-wrapper .close-search-results').on('click', function () {
        $('.sidebar-search .search-input').val('');
        $('.search-results-wrapper').html('');
    });
    //</Todo: search field in pages>
    
   // <Todo: Searches on mobile view>
    
    $('.mobile-search-placeholder input').on('focus', function () {
       $('.mobile-search .mobile-search-inner').fadeIn(500);
       $('.mobile-search .mobile-search-inner .search-input').focus();
       $(this).parent().hide();
    });
    $('.mobile-search .close-search').on('click', function () {
       $('.mobile-search .mobile-search-inner').hide();
       $('.mobile-search-placeholder').fadeIn(500);
    });
    //</Todo: Searches on mobile view>
    
})(jQuery);

//<Todo: Pause all videos when video is started>
var videos = document.querySelectorAll('video');
for(var i=0; i<videos.length; i++)
    videos[i].addEventListener('play', function(){pauseAll(this)}, true);
function pauseAll(elem){
    for(var i=0; i<videos.length; i++){
        //Is this the one we want to play?
        if(videos[i] === elem) continue;
        //Have we already played it && is it already paused?
        if(videos[i].played.length > 0 && !videos[i].paused){
            // Then pause it now
            videos[i].pause();
        }
    }
}
//</Todo: Pause all videos when video is started>


//</Todo: Convert views votes to nearest number>
var convertToNearestNumber = function(value){
    var item = typeof value === 'string' ? parseInt(value) : value;
    if (typeof item !== 'number'){
        return;
    }
    if (item < 1000){
        console.log(item);
        return item;
    }
    if (item >= 1000 && item < 1000000){
        var number = item / 1000;
        return (Math.round(number * 10) / 10) + 'K';
    }
    if (item >= 1000000 && item < 1000000000){
        var number1 = item / 1000000;
        return (Math.round(number1 * 10) / 10) + 'M';
    }
    if (item >= 1000000000){
        var number2 = item / 1000000000;
        return (Math.round(number2 * 10) / 10) + 'B';
    }
};


function GenericScriptService() {
    var self=this;

    // public methods
    self.saveItemToClipBoard = function(idOfInputWithHash) {
        var copyTextarea = document.querySelector(idOfInputWithHash);
        if (copyTextarea !== null){
            copyTextarea.focus();
            copyTextarea.select();
            try {
                var successful = document.execCommand('copy');
                abp.notify.success('Copied to clipboard.');
                return successful;
            } catch (err) {
                abp.notify.error('Oops, unable to copy');
                return false;
            } 
        }else{
            console.log('Element with the id: ' + idOfInputWithHash + ' was not found.');
            return false;

        }
    };
   
}
var genericScriptService = new GenericScriptService();

//</Todo: Convert views votes to nearest number>

