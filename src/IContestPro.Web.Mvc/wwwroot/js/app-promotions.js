(function() {
    $(function() {
        //<Todo: Featured contests>
        $.ajax({
            url: abp.appPath + 'Contest/GetRandomFeaturedContests',
            type: 'POST',
            contentType: 'application/html',
            success: function (content) {
                $('#featured-contest-wrapper').html(content);
            },
            error: function (e) { }
        });
        //</Todo: Featured contests> 
        
        // <Todo: Featured contest entries>
        $.ajax({
            url: abp.appPath + 'ContestEntry/GetRandomFeaturedContestEntries',
            type: 'POST',
            contentType: 'application/html',
            success: function (content) {
                $('#featured-contest-entries-wrapper').html(content);
            },
            error: function (e) { }
        });
        //</Todo: Featured contests> 
        
        // <Todo: CONTESTS ENDING SOON IN 30DAYS>
        $.ajax({
            url: abp.appPath + 'Contest/GetContestsEndingSoon',
            type: 'POST',
            contentType: 'application/html',
            success: function (content) {
                $('#contest-ending-soon-main-wrapper').html(content);
            },
            error: function (e) { }
        });
        //</Todo: CONTESTS ENDING SOON IN 30DAYS>

        // <Todo: GET ADVERTBOX ONE>
        $.ajax({
            url: abp.appPath + 'AdvertBoxOne/GetRandomLive',
            type: 'POST',
            contentType: 'application/html',
            success: function (content) {
                $('#advertboxone-wrapper').html(content);
            },
            error: function (e) { }
        });
        //</Todo: GET ADVERTBOX ONE>
        
        // <Todo: GET ADVERTBOX TWO>
        $.ajax({
            url: abp.appPath + 'AdvertBoxTwo/GetRandomLive',
            type: 'POST',
            contentType: 'application/html',
            success: function (content) {
                $('#advertboxtwo-wrapper').html(content);
            },
            error: function (e) { }
        });
        //</Todo: GET ADVERTBOX TWO>
        
    });
})();