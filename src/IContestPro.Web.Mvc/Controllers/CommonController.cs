﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Alerts;
using IContestPro.Authorization;
using IContestPro.Contest.ContestCategory;
using IContestPro.Contest.ContestCategory.Dto;
using IContestPro.Contest.ContestEntryType;
using IContestPro.Contest.ContestEntryType.Dto;
using IContestPro.Contest.ContestFee;
using IContestPro.Contest.ContestFee.Dto;
using IContestPro.Contest.ContestType;
using IContestPro.Contest.ContestType.Dto;
using IContestPro.PaymentType;
using IContestPro.PaymentType.Dto;
using IContestPro.Periodic;
using IContestPro.Periodic.Dto;
using IContestPro.VoteType;
using IContestPro.VoteType.Dto;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Contest})]
    public class CommonController : BaseContestController
    {
        private readonly IContestTypeAppService _contestTypeAppService;
        private readonly IContestCategoryAppService _contestCategoryAppService;
        private readonly IContestEntryTypeAppService _contestEntryTypeAppService;
        private readonly IVoteTypeAppService _voteTypeAppService;
        private readonly IContestFeeAppService _contestFeeAppService;
        private readonly IPeriodicAppService _periodicAppService;
        private readonly IPaymentTypeAppService _paymentTypeAppService;
        public CommonController( 
                                     IContestTypeAppService contestTypeAppService,
                                      IContestCategoryAppService contestCategoryAppService, 
                                    IContestEntryTypeAppService contestEntryTypeAppService, 
                                   IVoteTypeAppService voteTypeAppService,
            IContestFeeAppService contestFeeAppService,
            IPeriodicAppService periodicAppService, IIocResolver iocResolver, IPaymentTypeAppService paymentTypeAppService) : base(iocResolver)
        {
            _contestTypeAppService = contestTypeAppService;
            _contestCategoryAppService = contestCategoryAppService;
            _contestEntryTypeAppService = contestEntryTypeAppService;
            _voteTypeAppService = voteTypeAppService;
            _contestFeeAppService = contestFeeAppService;
            _periodicAppService = periodicAppService;
            _paymentTypeAppService = paymentTypeAppService;
        }
        // GET
       
      

        #region ContestType
        
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> Types(int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _contestTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items); 
        }
       
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> DeleteType(EntityDto model)
        {
            await _contestTypeAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest type deleted successfully."));
            return RedirectToAction("Types");
        } 
       
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditType(int? id)
        {
            if(id == null)
            return View(new ContestTypeDto());
         
            //Edit mode
                var item = await _contestTypeAppService.Get(new EntityDto{Id = id.Value});
                return View(item);
         
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditType(int? id, string name, string description)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(description))
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Name and description are required."));
                return View(new ContestTypeDto{Name = name, Description = description});

            }
            if (id == null)
            {
                var itemExists = await _contestTypeAppService.GetByName(name);
                if (itemExists != null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"Contest type with the name '{itemExists.Name}' already exists."));
                    return View(new ContestTypeDto{Name = name, Description = description}); 
                }
                var model = new CreateContestTypeDto {Name = name, Description = description};
                var item = await _contestTypeAppService.Create(model);
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest type created successfully."));
                    return RedirectToAction("Types");
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating contest type."));
                return View(new ContestTypeDto{Name = name, Description = description});
            }
            
            //Edit mode
            var updateModel = new ContestTypeDto {Id = id.Value, Name = name, Description = description, TenantId = AbpSession.GetTenantId()};
            var updateItem = await _contestTypeAppService.Update(updateModel);
            if (updateItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest type updated successfully."));
                return RedirectToAction("Types");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating contest type."));
            return View(new ContestTypeDto{Id = updateModel.Id, Name = name, Description = description});
          
        } 
        
        #endregion
        
        #region ContestEntryType
        
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> EntryTypes(int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _contestEntryTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items); 
        }
       
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> DeleteEntryType(EntityDto model)
        {
            await _contestEntryTypeAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest entry type deleted successfully."));
            return RedirectToAction("EntryTypes");
        } 
       
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditEntryType(int? id)
        {
            if(id == null)
            return View(new ContestEntryTypeDto());
         
            //Edit mode
                var item = await _contestEntryTypeAppService.Get(new EntityDto{Id = id.Value});
                return View(item);
         
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditEntryType(int? id, string name, string description)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(description))
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Name and description are required."));
                return View(new ContestEntryTypeDto{Name = name, Description = description});

            }
            if (id == null)
            {
                var itemExists = await _contestEntryTypeAppService.GetByName(name);
                if (itemExists != null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"Contest entry type with the name '{itemExists.Name}' already exists."));
                    return View(new ContestEntryTypeDto{Name = name, Description = description}); 
                }
                var model = new CreateContestEntryTypeDto {Name = name, Description = description};
                var item = await _contestEntryTypeAppService.Create(model);
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest entry type created successfully."));
                    return RedirectToAction("EntryTypes");
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating contest entry type."));
                return View(new ContestEntryTypeDto{Name = name, Description = description});
            }
            
            //Edit mode
            var updateModel = new ContestEntryTypeDto {Id = id.Value, Name = name, Description = description, TenantId = AbpSession.GetTenantId()};
            var updateItem = await _contestEntryTypeAppService.Update(updateModel);
            if (updateItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest entry type updated successfully."));
                return RedirectToAction("EntryTypes");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating contest entry type."));
            return View(new ContestEntryTypeDto{Id = updateModel.Id, Name = name, Description = description});
          
        } 
        #endregion
        
        
        #region ContestCategory
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> Categories(int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _contestCategoryAppService.GetAll(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items); 
        }
       
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> DeleteCategory(EntityDto model)
        {
            await _contestCategoryAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest category deleted successfully."));
            return RedirectToAction("Categories");
        } 
       
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditCategory(int? id)
        {
            if(id == null)
            return View(new ContestCategoryDto());
         
            //Edit mode
                var item = await _contestCategoryAppService.Get(new EntityDto{Id = id.Value});
                return View(item);
         
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditCategory(int? id, string name, string description)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(description))
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Name and description are required."));
                return View(new ContestCategoryDto{Name = name, Description = description});

            }
            if (id == null)
            {
                var itemExists = await _contestCategoryAppService.GetByName(name);
                if (itemExists != null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"Contest category with the name '{itemExists.Name}' already exists."));
                    return View(new ContestCategoryDto{Name = name, Description = description}); 
                }
                var model = new CreateContestCategoryDto {Name = name, Description = description};
                var item = await _contestCategoryAppService.Create(model);
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest category created successfully."));
                    return RedirectToAction("Categories");
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating contest category."));
                return View(new ContestCategoryDto{Name = name, Description = description});
            }
            
            //Edit mode
            var updateModel = new ContestCategoryDto {Id = id.Value, Name = name, Description = description, TenantId = AbpSession.GetTenantId()};
            var updateItem = await _contestCategoryAppService.Update(updateModel);
            if (updateItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest category updated successfully."));
                return RedirectToAction("Categories");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating contest type."));
            return View(new ContestCategoryDto{Id = updateModel.Id, Name = name, Description = description});
          
        } 
        #endregion
        
        #region VoteType
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> VoteTypes(int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _voteTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items); 
        }
       
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> DeleteVoteType(EntityDto model)
        {
            await _voteTypeAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Vote type deleted successfully."));
            return RedirectToAction("VoteTypes");
        } 
       
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditVoteType(int? id)
        {
            if(id == null)
            return View(new VoteTypeDto());
         
            //Edit mode
                var item = await _voteTypeAppService.Get(new EntityDto{Id = id.Value});
                return View(item);
         
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditVoteType(int? id, string name, string description, int votePoint = 0, decimal amount = 0, decimal minAmount = 0, decimal maxAmount = 0)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(description))
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Name and description are required."));
                return View(new VoteTypeDto{Name = name, Description = description, VotePoint = votePoint, Amount = amount, MinAmount = minAmount, MaxAmount = maxAmount});

            }
            if (id == null)
            {    
                var itemExists = await _voteTypeAppService.GetByName(name);
                if (itemExists != null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"Vote type with the name '{itemExists.Name}' already exists."));
                    return View(new VoteTypeDto{Name = name, Description = description, VotePoint = votePoint, Amount = amount, MinAmount = minAmount, MaxAmount = maxAmount}); 
                }
                var model = new CreateVoteTypeDto {Name = name, Description = description, VotePoint = votePoint, Amount = amount, MinAmount = minAmount, MaxAmount = maxAmount};
                var item = await _voteTypeAppService.Create(model);
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Vote type created successfully."));
                    return RedirectToAction("VoteTypes");
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating vote type."));
                return View(new VoteTypeDto{Name = name, Description = description, VotePoint = votePoint, Amount = amount, MinAmount = minAmount, MaxAmount = maxAmount});
            }
            
            //Edit mode
            var updateModel = new VoteTypeDto {Id = id.Value, Name = name, Description = description, VotePoint = votePoint, Amount = amount, MinAmount = minAmount, MaxAmount = maxAmount, TenantId = AbpSession.GetTenantId()};
            var updateItem = await _voteTypeAppService.Update(updateModel);
            if (updateItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Vote type updated successfully."));
                return RedirectToAction("VoteTypes");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating vote type."));
            return View(new VoteTypeDto{Id = updateModel.Id, Name = name, Description = description, VotePoint = votePoint, Amount = amount, MinAmount = minAmount, MaxAmount = maxAmount});
        } 
        #endregion
        
        #region Periodic
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> Periodic(int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _periodicAppService.GetAll(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items); 
        }
       
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> DeletePeriodic(EntityDto model)
        {
            await _periodicAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Periodic deleted successfully."));
            return RedirectToAction("Periodic");
        } 
       
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditPeriodic(int? id)
        {
            if(id == null)
            return View(new PeriodicDto());
         
            //Edit mode
                var item = await _periodicAppService.Get(new EntityDto{Id = id.Value});
                return View(item);
         
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditPeriodic(int? id, string name, string description)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(description))
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Name and description are required."));
                return View(new PeriodicDto{Name = name, Description = description});

            }
            if (id == null)
            {    
                var itemExists = await _periodicAppService.GetByName(name);
                if (itemExists != null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"Periodic with the name '{itemExists.Name}' already exists."));
                    return View(new PeriodicDto{Name = name, Description = description}); 
                }
                var model = new CreatePeriodicDto {Name = name, Description = description};
                var item = await _periodicAppService.Create(model);
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Periodic created successfully."));
                    return RedirectToAction("Periodic");
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating periodic."));
                return View(new PeriodicDto{Name = name, Description = description});
            }
            
            //Edit mode
            var updateModel = new PeriodicDto {Id = id.Value, Name = name, Description = description, TenantId = AbpSession.GetTenantId()};
            var updateItem = await _periodicAppService.Update(updateModel);
            if (updateItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Periodic updated successfully."));
                return RedirectToAction("Periodic");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating periodic."));
            return View(new PeriodicDto{Id = updateModel.Id, Name = name, Description = description});
        } 
        #endregion
        
        #region ContestFee
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> ContestFee(int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _contestFeeAppService.GetAll(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items); 
        }
       
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> DeleteContestFee(EntityDto model)
        {
            await _contestFeeAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest fee deleted successfully."));
            return RedirectToAction("ContestFee");
        } 
       
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditContestFee(int? id)
        {
            ViewBag.ContestTypes = new SelectList((await _contestTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0,MaxResultCount = 100}))
                .Items.Select(itm => new {itm.Id, itm.Name}).ToList(), "Id", "Name" );
            if(id == null)
            return View(new ContestFeeDto());
         
            //Edit mode
                var item = await _contestFeeAppService.Get(new EntityDto{Id = id.Value});
                return View(item);
         
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditContestFee(int? id, ContestFeeDto model)
        {
            ViewBag.ContestTypes = new SelectList((await _contestTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0,MaxResultCount = 100}))
                .Items.Select(itm => new {itm.Id, itm.Name}).ToList(), "Id", "Name" );
            model.TenantId = AbpSession.GetTenantId();
            if (id == null)
            {    
                
               // var model = new CreateContestFeeDto {ContestTypeId = contestTypeId, CodeBatch = codeBatch,Amount = amount, PercentagePerMonth = percentagePerMonth};
                var item = await _contestFeeAppService.Create(model.MapTo<CreateContestFeeDto>());
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest fee created successfully."));
                    return RedirectToAction("ContestFee");
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating vote type."));
                return View(model);
            }
            
            //Edit mode
             var updateItem = await _contestFeeAppService.Update(model);
            if (updateItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest fee updated successfully."));
                return RedirectToAction("ContestFee");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating contest type."));
            return View(model);
        } 
        #endregion
        
        #region PaymentType
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> PaymentTypes(int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _paymentTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items); 
        }
       
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> DeletePaymentType(EntityDto model)
        {
            await _paymentTypeAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Payment type point deleted successfully."));
            return RedirectToAction("PaymentTypes");
        } 
       
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditPaymentType(int? id)
        {
            if(id == null)
            return View(new PaymentTypeDto());
            
            //Edit mode
             var item = await _paymentTypeAppService.Get(new EntityDto{Id = id.Value});
             return View(item);
         
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEditPaymentType(int? id, string name, string description)
        {
            if (id == null)
            {    
                var itemExists = await _paymentTypeAppService.GetByName(name);
                if (itemExists != null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Payment type already exists."));
                    return View(new PaymentTypeDto{Name = name, Description = description}); 
                }
                var model = new CreatePaymentTypeDto {Name = name, Description = description};
                var item = await _paymentTypeAppService.Create(model);
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Payment type created successfully."));
                    return RedirectToAction("PaymentTypes");
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating your record."));
                return View(new PaymentTypeDto{Name = name, Description = description});
            }
            
            //Edit mode
            var updateModel = new PaymentTypeDto {Id = id.Value, Name = name, Description = description, TenantId = AbpSession.GetTenantId()};
            var updateItem = await _paymentTypeAppService.Update(updateModel);
            if (updateItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Payment type updated successfully."));
                return RedirectToAction("PaymentTypes");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating your record."));
            return View(new PaymentTypeDto{Id = updateModel.Id, Name = name, Description = description});
        } 
        #endregion
        
        #region HowItWorks
        [AllowAnonymous]
        public  async Task<IActionResult> HowItWorks()
        {
            ViewBag.ActiveHowItWorks = "active";
            return View(); 
        }
        #endregion
        
    }
}