﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.AspNetCore.Mvc.Extensions;
using Abp.Dependency;
using Abp.Notifications;
using Abp.Runtime.Session;
using Castle.LoggingFacility.MsLogging;
using IContestPro.Notifications;
using IContestPro.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]
    public class NotificationsController : BaseController
    {
        private readonly IUserAppService _userAppService;
        private readonly IAppNotifier _appNotifier;
        private readonly IUserNotificationManager _userNotificationManager;


        public NotificationsController(IUserAppService userAppService,
            IIocResolver iocResolver, IAppNotifier appNotifier, IUserNotificationManager userNotificationManager): base(iocResolver)
        {
            _userAppService = userAppService;
            _appNotifier = appNotifier;
            _userNotificationManager = userNotificationManager;
        }
        // GET
        public async Task<IActionResult> Index(int page = 1, int pageSize = 20, string state = null)
        {
            UserNotificationState? notificationState = null;
            switch (state)
            {
                case "unread":
                    notificationState = UserNotificationState.Unread;
                    break;
                case "read":
                    notificationState = UserNotificationState.Read;
                    break;
            }
            var userIdentifier = new UserIdentifier(AbpSession.TenantId, AbpSession.GetUserId());
            var skipCount = pageSize * (page - 1);
            
            ViewBag.NotificationCount = await _userNotificationManager.GetUserNotificationCountAsync(userIdentifier, notificationState);
            ViewBag.Page = page;
            ViewBag.PageSize = pageSize;
            ViewBag.NotificationState = notificationState;
            ViewBag.Type = state ?? "All";
            var notifications = await _userNotificationManager.GetUserNotificationsAsync(userIdentifier, notificationState, skipCount, pageSize);
            await Task.Run(async () =>
            {
                foreach (var userNotification in notifications)
                {
                    if (userNotification.State == UserNotificationState.Unread)
                    {
                        await _userNotificationManager.UpdateUserNotificationStateAsync(AbpSession.TenantId, userNotification.Id, UserNotificationState.Read);
                    }   
                    Logger.Log(LogLevel.Error, userNotification.Id.ToString());
                }
            });
            
           
            return View(notifications);
        }  
        
        public async Task<IActionResult> Unread()
        {
            var userIdentifier = new UserIdentifier(AbpSession.TenantId, AbpSession.GetUserId());
            ViewBag.NotificationCount = await _userNotificationManager.GetUserNotificationCountAsync(userIdentifier, UserNotificationState.Unread);
            var notifications = await _userNotificationManager.GetUserNotificationsAsync(userIdentifier, UserNotificationState.Unread, 0, 10);
            if (notifications != null && notifications.Any())
            {
                return Json(new JsonResult(notifications)); 
            }
            else
            {
                 notifications = await _userNotificationManager.GetUserNotificationsAsync(userIdentifier, null, 0, 10);
                return Json(new JsonResult(notifications)); 

            }
        } 
        public async Task<IActionResult> CountUnread()
        {
            var userIdentifier = new UserIdentifier(AbpSession.TenantId, AbpSession.GetUserId());
            var notificationCount = await _userNotificationManager.GetUserNotificationCountAsync(userIdentifier, UserNotificationState.Unread);
            return Json(new JsonResult(notificationCount));
        } 
        
        [HttpPost]
         public async Task<IActionResult> Delete(Guid id)
        {
             await _userNotificationManager.DeleteUserNotificationAsync(AbpSession.TenantId, id);
            if (Request.IsAjaxRequest())
            {
                return Json(new JsonResult(true));
            }
            return RedirectToAction("Index");
        }
       
        [HttpPost]
         public async Task<IActionResult> MarkRead(Guid id)
        {
                await _userNotificationManager.UpdateUserNotificationStateAsync(AbpSession.TenantId, id, UserNotificationState.Read);
            return Json(new JsonResult(true));

        }
        [HttpPost]
        public async Task<IActionResult> MarkReadList(List<Guid> ids)
        {
            foreach (var id in ids)
            {
                await _userNotificationManager.UpdateUserNotificationStateAsync(AbpSession.TenantId, id, UserNotificationState.Read);
            }
            return Json(new JsonResult(true));

        }

       
    }
}