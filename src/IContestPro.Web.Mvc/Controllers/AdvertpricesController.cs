﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Alerts;
using IContestPro.AdvertPrice;
using IContestPro.AdvertPrice.Dto;
using IContestPro.Authorization;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize(new []{PermissionNames.Pages_Admin})]
    public class AdvertpricesController : BaseController
    {
        private readonly IAdvertPriceAppService _advertPriceAppService;


        public AdvertpricesController(IIocResolver iocResolver, IAdvertPriceAppService advertPriceAppService): base(iocResolver)
        {
            _advertPriceAppService = advertPriceAppService;
        }
        // GET
        public async Task<IActionResult> Index(int page = 1, int pageSize = 20, string startDate = null, string endDate = null)
        {
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            var offset = (page - 1) * pageSize;

            var items = await _advertPriceAppService.GetAll( new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
             return View(items);
        }
        
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> Delete(EntityDto model)
        {
            await _advertPriceAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Advert price deleted successfully."));
            return RedirectToAction("Index");
        } 
       
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEdit(int? id)
        {
            if(id == null)
            return View(new AdvertPriceDto());
         
            //Edit mode
                var item = await _advertPriceAppService.Get(new EntityDto{Id = id.Value});
                return View(item);
         
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> CreateEdit(int? id, string name, int? days, decimal? amount, AdvertType? type)
        {
            if (string.IsNullOrEmpty(name) || days == null || amount == null || type == null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Name, days, amount and type are required."));
                return View(new AdvertPriceDto{Name = name, Days = days ?? 0 , Amount = amount ?? 0, Type = type ?? AdvertType.HomePageBanner});

            }
            if (id == null)
            {
                var itemExists = await _advertPriceAppService.GetByName(name);
                if (itemExists != null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"Advert price with the name '{itemExists.Name}' already exists."));
                    return View(new AdvertPriceDto{Name = name, Days = days.Value, Amount = amount.Value, Type = type.Value});
                }
                var model = new CreateAdvertPriceDto {Name = name, Days = days.Value, Amount = amount.Value, Type = type.Value};
                var item = await _advertPriceAppService.Create(model);
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Advert price created successfully."));
                    return RedirectToAction("Index");
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating contest type."));
                return View(new AdvertPriceDto{Name = name, Days = days.Value , Amount = amount.Value, Type = type.Value});
            }
            
            //Edit mode
            var updateModel = new AdvertPriceDto {Id = id.Value, Name = name, Days = days.Value , Amount = amount.Value, TenantId = AbpSession.GetTenantId(), Type = type.Value};
            var updateItem = await _advertPriceAppService.Update(updateModel);
            if (updateItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Advert price updated successfully."));
                return RedirectToAction("Index");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating contest type."));
            return View(new AdvertPriceDto{Id = updateModel.Id, Name = name,Days = days.Value , Amount = amount.Value, Type = type.Value});
          
        } 
    }
}