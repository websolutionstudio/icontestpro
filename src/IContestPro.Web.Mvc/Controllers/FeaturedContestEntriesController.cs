﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Mvc.Alerts;
using IContestPro.AdvertPrice;
using IContestPro.AdvertPrice.Dto;
using IContestPro.Authorization;
using IContestPro.Contest.Dto;
using IContestPro.FeaturedContestEntry;
using IContestPro.FeaturedContestEntry.Dto;
using IContestPro.Helpers;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]
    public class FeaturedContestEntriesController : BaseController
    {
        private readonly IFeaturedContestEntryAppService _featuredContestEntryAppService;
        private readonly IAdvertPriceAppService _advertPriceAppService;


        public FeaturedContestEntriesController(IIocResolver iocResolver,
            IAdvertPriceAppService advertPriceAppService, IFeaturedContestEntryAppService featuredContestEntryAppService): base(iocResolver)
        {
            _advertPriceAppService = advertPriceAppService;
            _featuredContestEntryAppService = featuredContestEntryAppService;
        }
        // GET
       
        public async Task<IActionResult> Index(int page = 1, int pageSize = 20)
        {
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var offset = (page - 1) * pageSize;
            PagedResultDto<FeaturedContestEntryDto> items;
            if (await IsGrantedAsync(PermissionNames.Pages_Admin))
            {
                 items = await _featuredContestEntryAppService.GetAll( new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            }
            else
            {
                 items = await _featuredContestEntryAppService.GetAllByUser(AbpSession.GetUserId(), new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            }
             return View(items);
        }
        
        [HttpPost]
        [UnitOfWork]
        public  async Task<IActionResult> Delete(EntityDto model)
        {
            await _featuredContestEntryAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Featured contest entry deleted successfully."));
            return RedirectToAction("Index");
        } 
       
        public  async Task<IActionResult> CreateEdit(CreateEditFeaturedContestEntryDto model, int? reference)
        {
            var selectList = await _advertPriceAppService.GetAllByType(AdvertType.Contest, new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            var items = selectList.Items.Select(item => 
                new AdvertPriceDto {Id = item.Id, Amount = item.Amount, 
                    Name = $"{item.Name} | {item.Amount.ToNairaString()}"}).ToList();
            ViewBag.AdvertPrices = new SelectList(items, "Id", "Name");
            var entriesToPromote = (await _featuredContestEntryAppService.GetContestEntriesToPromote(AbpSession.GetUserId())).ToList();
            ViewBag.EntriesToPromote = new SelectList(entriesToPromote, "Id", "Title", reference);

            if (model.Id == null)
            {
                return View(new CreateEditFeaturedContestEntryDto());  
            }
         
            //Edit mode
                var itemToEdit = await _featuredContestEntryAppService.Get(new EntityDto{Id = model.Id.Value});
                ViewBag.AdvertPrices = new SelectList(selectList.Items, "Id", "Name", itemToEdit.AdvertPriceId);
                 ViewBag.EntriesToPromote = new SelectList(entriesToPromote, "Id", "Title", itemToEdit.ContestEntryId);
                var input = itemToEdit.MapTo<CreateEditFeaturedContestEntryDto>(); 
                return View(input);
         
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize]
        public  async Task<IActionResult> CreateEdit(CreateEditFeaturedContestEntryDto model, string forgetMe)
        {
            try
            {
                if (model.StartDate == null || model.AdvertPriceId == null || model.ContestEntryId == null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "StartDate date, duration and contest are required."));
                    return View(model);

                }
            var selectList = await _advertPriceAppService.GetAllByType(AdvertType.Contest,new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            var items = selectList.Items.Select(item => 
                new AdvertPriceDto {Id = item.Id, Amount = item.Amount, 
                    Name = $"{item.Name} | {item.Amount.ToNairaString()}"}).ToList();
                ViewBag.AdvertPrices = new SelectList(items, "Id", "Name", model.AdvertPriceId);
                var entriesToPromote = (await _featuredContestEntryAppService.GetContestEntriesToPromote(AbpSession.GetUserId())).ToList();
                ViewBag.EntriesToPromote = new SelectList(entriesToPromote, "Id", "Title", model.ContestEntryId);

            if (model.Id == null)
            {
                model.UserId = AbpSession.GetUserId();
                var item = await _featuredContestEntryAppService.Create(model.MapTo<CreateFeaturedContestEntryDto>());
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Featured contest entry created successfully."));
                    return RedirectToAction("Index");
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
                return View(model);
            }
                
            //Edit mode
                var dbModel = new FeaturedContestEntryDto
                {
                    Id = model.Id.Value,
                    TenantId = model.TenantId,
                    StartDate = model.StartDate.Value,
                    StatusId = model.StatusId.Value,
                    UserId = model.UserId.Value,
                    AdvertPriceId = model.AdvertPriceId.Value,
                    ContestEntryId = model.ContestEntryId.Value,
                };
            var updateItem = await _featuredContestEntryAppService.Update(dbModel);
            if (updateItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Featured contest entry updated successfully."));
                return RedirectToAction("Index");
            }
            }
            catch (UserFriendlyException e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, e.Message));
                return View(model); 
            }
            catch (Exception)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
                return View(model);
            }
            
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
            return View(model);
          
        } 
        
        [HttpPost]
        public  async Task<IActionResult> ChangeStatus(UpdateStatusDto model)
        {
            var savedItem = await _featuredContestEntryAppService.UpdateStatusAsync(model);
            if (savedItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Featured contest entry status updated successfully."));
                return RedirectToAction("Index");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while updating request."));
            return RedirectToAction("Index");
        }
        
        
    }
}