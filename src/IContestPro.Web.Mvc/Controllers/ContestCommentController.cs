﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.AspNetCore.Mvc.Extensions;
using Abp.Dependency;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Alerts;
using IContestPro.Authorization;
using IContestPro.Contest.ContestComment;
using IContestPro.Contest.ContestComment.Dto;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Contest})]
    public class ContestCommentController : BaseContestController
    {
        private readonly IContestCommentAppService _contestCommentAppService;
        public ContestCommentController(IIocResolver iocResolver,
            IContestCommentAppService contestCommentAppService) : base(iocResolver)
        {
            _contestCommentAppService = contestCommentAppService;
        }

        /// <summary>
        /// Fetches the comments of a particular contest
        /// </summary>
        /// <param name="contestId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public  async Task<IActionResult> Comments(int? contestId, int page = 1, int pageSize = 20)
        {
            if (contestId == null)
            {
                return View("NotFound");
            }
            var offset = (page - 1) * pageSize;
            var itemGotten = await _contestCommentAppService.GetAllByContestId(contestId.Value, new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize} );
            if (Request.IsAjaxRequest())
            {
                if (itemGotten == null)
                {
                    return Json(new {success = false, message = "There are no comments for this contest now."});
                } 
                return Json(new {success = true, data = itemGotten.Items, totalCount = itemGotten.TotalCount});

            }
            if (itemGotten == null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "The contest you are looking for was not found."));
                return View("NotFound");
            }
            return View(itemGotten);
        }

        public IActionResult Create()
        {
            return View(new CreateContestCommentDto());
        }

        [HttpPost]
        public  async Task<IActionResult> Create(CreateContestCommentDto model)
        {
            
            if (Request.IsAjaxRequest())
            {
                if (model.Comment == null)
                {
                    return Json(new {success = false, message = "Please enter your comment."});

                }

                model.UserId = AbpSession.GetUserId();
                var item = await _contestCommentAppService.Create(model);
                if (item.Id > 0)
                {
                    return Json(new {success = true, message = "Comment submitted successfully.", data = item});  
                }
                return Json(new {success = false, data = "Error occurred while posting your comment."});

            }
            
            if (model.Comment == null)
            {
                return Json(new {success = false, message = "Please enter your comment."});

            }

            var item2 = await _contestCommentAppService.Create(model);
            if (item2.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Comment submitted successfully."));

            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Comment submitted successfully."));

            return View(model);
        }
    }
}