﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Alerts;
using IContestPro.Sponsor;
using IContestPro.Users;
using IContestPro.Users.Dto;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]
    public class ManageController : BaseController
    {
        private readonly IUserAppService _userAppService;
        private readonly ISponsorAppService _sponsorAppService;


        public ManageController(IUserAppService userAppService,
            IIocResolver iocResolver, ISponsorAppService sponsorAppService): base(iocResolver)
        {
            _userAppService = userAppService;
            _sponsorAppService = sponsorAppService;
        }
        // GET
        public async Task<IActionResult> Index()
        {
            var user = await _userAppService.Get(new EntityDto<long>{Id = AbpSession.GetUserId()});
            user.Sponsor = await _sponsorAppService.GetByUserId(user.Id);
            ViewBag.ReferralLink = GetCurrentUserReferralLink();
             ViewBag.ReferralVotePoint = await SettingManager.GetSettingValueAsync<int>(AppConsts.SettingsReferralVotePointAward);
             ViewBag.ShouldConfirmPhoneNumber = await SettingManager.GetSettingValueAsync<bool>(AppConsts.SettingsPhoneNumberAfterAccountCreation);
            return View(user);
        }
        public async Task<IActionResult> Update()
        {
            var user = await _userAppService.Get(new EntityDto<long>{Id = AbpSession.GetUserId()});
            return View(user);
        }
        
        [HttpPost]
        [UnitOfWork]
        public async Task<IActionResult> Update(UserDto model)
        {
            if (ModelState.IsValid)
            {
             var user =  await _userAppService.Update(model);
                AddAlertMessage(
                    new AlertMessageCustom(
                        AlertType.Success, "Profile updated successfully."
                        ));
                return RedirectToAction("Index");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, 
                "Some required fields where not supply. Please check and try again."));

            return View(model);
        }
        
        public async Task<IActionResult> Sponsor()
        {
            var user = await _userAppService.Get(new EntityDto<long>{Id = AbpSession.GetUserId()});
            return View(user);
        }

         public async Task<IActionResult> ProfilePicture(IFormFile profilepic)
        {
            
               if (profilepic == null || profilepic.Length == 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please upload a file."));
                    return RedirectToAction("Index");    
                  }
                if (profilepic.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please select only image file."));
                    return RedirectToAction("Index");  
                }
            
                
            /*var uniqueFileName = Guid.NewGuid().ToString("N") + ".jpg";
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploads");
                var filePath = Path.Combine(uploads, uniqueFileName);
                 await _userAppService.UpdateProfilePictureAsync(uniqueFileName);
                await profilepic.CopyToAsync(new FileStream(filePath, FileMode.Create));*/
                await _userAppService.UploadProfilePictureAsync(profilepic);

                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Profile picture saved successfully."));
                return RedirectToAction("Index"); 
                
            }

        #region PrivateMoethods
        public IActionResult ChangePassword()
        {
        return View(); 
        } 
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordDto model)
        {
            var result = await _userAppService.ChangePasswordAsync(model);
            if (result.Succeeded)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, 
                    "Password changed successfully."));
                return RedirectToAction("Index"); 
            }
    
        var errors = "";
        foreach (var identityError in result.Errors)
        {
            errors += identityError.Description;
        }
        AddAlertMessage(new AlertMessageCustom(AlertType.Danger, 
            errors));
        return View(model); 
        }
        #endregion
       
    }
}