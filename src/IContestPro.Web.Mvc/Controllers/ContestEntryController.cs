﻿using System;
using System.IO;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.AspNetCore.Mvc.Extensions;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using Abp.Web.Mvc.Alerts;
using Abp.Web.Mvc.Models;
using IContestPro.AssetUpload;
using IContestPro.AssetUpload.Dto;
using IContestPro.Authorization;
using IContestPro.Contest;
using IContestPro.Contest.ContestEntry;
using IContestPro.Contest.ContestEntry.Dto;
using IContestPro.Contest.ContestEntryComment;
using IContestPro.Contest.ContestEntryComment.Dto;
using IContestPro.Contest.ContestEntryView;
using IContestPro.Contest.ContestEntryView.Dto;
using IContestPro.Contest.ContestVote;
using IContestPro.Contest.ContestVote.Dto;
using IContestPro.Contest.Dto;
using IContestPro.FeaturedContestEntry;
using IContestPro.Wallet;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Controllers
{
    public class ContestEntryController : BaseContestController
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IContestEntryAppService _contestEntryAppService;
        private readonly IContestEntryVoteAppService _contestEntryVoteAppService;
        private readonly IContestAppService _contestAppService;
        private readonly IContestEntryCommentAppService _contestEntryCommentAppService;
        private readonly IContestEntryViewAppService _contestEntryViewAppService;
        private readonly IWalletAppService _walletAppService;
        private readonly IAssetUploadAppService _assetUploadAppService;
        private readonly IFeaturedContestEntryAppService _featuredContestEntryAppService;

        public ContestEntryController(IHostingEnvironment hostingEnvironment, 
            IContestEntryAppService contestEntryAppService, IContestAppService contestAppService, 
             IIocResolver iocResolver,
            IContestEntryVoteAppService contestEntryVoteAppService, 
            IContestEntryCommentAppService contestEntryCommentAppService, IContestEntryViewAppService contestEntryViewAppService, IWalletAppService walletAppService, IAssetUploadAppService assetUploadAppService, IFeaturedContestEntryAppService featuredContestEntryAppService) : base(iocResolver)
        {
            _hostingEnvironment = hostingEnvironment;
            _contestEntryAppService = contestEntryAppService;
            _contestAppService = contestAppService;
            _contestEntryVoteAppService = contestEntryVoteAppService;
            _contestEntryCommentAppService = contestEntryCommentAppService;
            _contestEntryViewAppService = contestEntryViewAppService;
            _walletAppService = walletAppService;
            _assetUploadAppService = assetUploadAppService;
            _featuredContestEntryAppService = featuredContestEntryAppService;
        }
       #region PublicPages
        [AbpMvcAuthorize]
        public async Task<ActionResult> AddPosterToVideos()
        {

         var items =  await _contestEntryAppService.AddPosterToVideos();
            return View(items);
        }
        [AllowAnonymous]
        public async Task<IActionResult> Active(int page = 1, int pageSize = 20)
        {
            ViewBag.ActiveContestEntries = "active";
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _contestEntryAppService.GetActiveContestEntries(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items); 
        } 
        
          [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Contest_Create})]
        public async Task<IActionResult> Create(string id)
        {
           
            var contest = await _contestAppService.GetByPermalinkWithDetails(id);
            if (contest == null)
            {
                return View("NotFoundAdmin");
            }

            if (contest.Contest.UserId == AbpSession.GetUserId())
            {
               AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Sorry! You cannot join this contest because it was created by you.",
                   "Error!", true, null,null, "fa-times"));
                return RedirectToAction("Index", "Manage");
            }
            if (await _contestEntryAppService.IsUserInContest(AbpSession.GetUserId(), contest.Contest.Id))
            {
               AddAlertMessage(new AlertMessageCustom(AlertType.Danger, 
                   $"You are already part of '{contest.Contest.Title}' contest.", "Error! User already joined.", true, null,null, "fa-times"));
                return RedirectToAction("Index", "Manage");
            }
            ViewBag.Contest = contest;
            return View(new ContestEntryInputDto());

           
        }
        
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize]
        public  async Task<IActionResult> Create(ContestEntryInputDto model)
        {
            var contest = await _contestAppService.GetWithDetails(new EntityDto{Id = model.ContestId});
            if (contest == null)
            {
                if (IsMobile)
                {
                    return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message = "Contest not found."}});
                }
                return View("NotFoundAdmin");
            }
            ViewBag.Contest = contest;
            if (await _contestEntryAppService.IsCurrentUserInContest(model.ContestId))
            {
                if (IsMobile)
                {
                    return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message = "You are already part of this contest."}});
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "You are already part of this contest."));
                return View(model); 
            }
            if (await _contestEntryAppService.IsUserContestOwner(model.ContestId))
            {
                if (IsMobile)
                {
                    return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message = "You cannot join the contest you created."}});
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "You cannot join the contest you created."));
                return View(model); 
            }

            var status = await _contestAppService.GetContestStatus(model.ContestId);
            if (status != null && status.Name != AppConsts.StatusLive)
            {
                if (IsMobile)
                {
                    return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message = "Entry cannot be created on a contest that is not live."}});
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Entry cannot be created on a contest that is not live."));
                return View(model); 
            }
            
            if (contest.Contest.ContestType.Name == AppConsts.ContestTypePaid)
            {
                var balance = await _walletAppService.GetBalance(AbpSession.GetUserId());
                if (balance < contest.Contest.EntryAmount)
                {
                    if (IsMobile)
                    {
                        return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message = "Insufficient balance. Please goto your profile and add fund to your wallet to complete your entry."}});
                    }
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Insufficient balance. Please goto your profile and add fund to your wallet to complete your entry."));
                    return View(model);   
                }
            }
            
            if (contest.Contest.ContestEntryType.Name == AppConsts.ContestEntryTypeImage)
            {
               // var uniqueFileName = Guid.NewGuid().ToString("N") + ".jpg";
               // var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploads");
                //var filePath = Path.Combine(uploads, uniqueFileName);
                var dbModel = new CreateContestEntryDto();
                if (model.Image == null || model.Image.Length == 0)
                {
                    if (IsMobile)
                    {
                        return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message = "Please upload a file."}});
                    }
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please upload a file."));
                    return View(model); 
                }
                if (model.Image.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    if (IsMobile)
                    {
                        return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message = "Please select only image file."}});
                    }
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please select only image file."));
                    return View(model);
                }

                var result = await _assetUploadAppService.UploadImage(new UploadDto{File = model.Image, Path =  "images/uploads"});
                dbModel.Image = result.FileName;
                dbModel.UserId = AbpSession.GetUserId();
                dbModel.ContestId = contest.Contest.Id;
                var savedItem = await _contestEntryAppService.Create(dbModel);
                if (savedItem.Id > 0)
                {
                    if (IsMobile)
                    {
                        return Json(new AjaxResponse());
                    }
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest created successfully. Please wait for the admin to activate your entry."));
                    return RedirectToAction("PersonalDetails", new {id = savedItem.Permalink});
                }
            } else if (contest.Contest.ContestEntryType.Name == AppConsts.ContestEntryTypeVideo)
            {
              
                if (model.Video == null || model.Video.Length == 0)
                {
                    if (IsMobile)
                    {
                        return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message = "Please upload your video file."}});
                    }
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please upload your video file."));
                    return View(model); 
                }
                if (model.Video.ContentType.IndexOf("mp4", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    if (IsMobile)
                    {
                        return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message = "Please select only mp4 video file."}});
                    }
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please select only mp4 video file."));
                    return View(model);
                }

                var result = await _assetUploadAppService.UploadVideo(new VideoUploadDto{File = model.Video, VideoPath = "videos", VideoPosterPath = "images/uploads"});
                var dbModel = new CreateContestEntryDto();
                //dbModel.Poster = uniqueFileName;
                dbModel.UserId = AbpSession.GetUserId();
                dbModel.ContestId = contest.Contest.Id;
                dbModel.Video = result["video"];
                dbModel.Poster = result["poster"];
                var savedItem = await _contestEntryAppService.Create(dbModel);
                if (savedItem.Id > 0)
                {
                    if (IsMobile)
                    {
                        return Json(new AjaxResponse());
                    }
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest created successfully. Please wait for the admin to activate your entry."));
                    return RedirectToAction("PersonalDetails", new {id = savedItem.Permalink});

                }
            } 
            else if (contest.Contest.ContestEntryType.Name == AppConsts.ContestEntryTypeArticle)
            {
                if (model.Poster == null || model.Poster.Length == 0)
                {
                    if (IsMobile)
                    {
                        return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message = "Please upload image file for poster. Only jpg or png is allowed."}});
                    }
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please upload image file for poster. Only jpg or png is allowed."));
                    return View(model); 
                }
                if (model.Poster.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    if (IsMobile)
                    {
                        return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message = "Please select only image file for the poster. Only jpg or png is allowed."}});
                    }
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please select only image file for the poster. Only jpg or png is allowed."));
                    return View(model);
                }
               
                if (model.Article == null)
                {
                    if (IsMobile)
                    {
                        return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message = "Please write your article."}});
                    }
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please write your article."));
                    return View(model); 
                }
                var uniqueFileName = Guid.NewGuid().ToString("N") + ".jpg";
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploads");
                var filePath = Path.Combine(uploads, uniqueFileName);
                var dbModel = new CreateContestEntryDto();
                await model.Poster.CopyToAsync(new FileStream(filePath, FileMode.Create));
                
                dbModel.Poster = uniqueFileName;
                dbModel.UserId = AbpSession.GetUserId();
                dbModel.ContestId = contest.Contest.Id;
                dbModel.Article = model.Article;
                var savedItem = await _contestEntryAppService.Create(dbModel);
                if (savedItem.Id > 0)
                {
                    if (IsMobile)
                    {
                        return Json(new AjaxResponse());
                    }
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest entry created successfully."));
                    return RedirectToAction("PersonalDetails", new {id = savedItem.Permalink});
                }
            }
            if (IsMobile)
            {
                return Json(new AjaxResponse{Success = false, Error = new ErrorInfo{Message =  "Error occurred while completing your request."}});
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while completing your request."));
            return View(model);
        }

        [AllowAnonymous]
        public async Task<IActionResult> Details(string id)
        {
            ViewBag.ActiveActiveContest = "active";
            var items = await _contestEntryAppService.GetByPermalinkWithDetails(id);
            if (items == null)
            {
                return View("NotFound");
            }
            return View("DetailsImage", items); 
        } 
        [AllowAnonymous]
        public async Task<IActionResult> PersonalDetails(string id)
        {
            ViewBag.ActiveActiveContest = "active";
            var item = await _contestEntryAppService.GetByPermalinkWithDetails(id, true);
            if (item == null)
            {
                return View("NotFoundAdmin");
            }
            return View(item); 
        } 
        [AllowAnonymous]
        public async Task<IActionResult> Entries(string id, int page = 1, int pageSize = 20)
        {
            try
            {
            ViewBag.ActiveActiveContest = "active";
            ViewBag.ContestPermalink = id;
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _contestEntryAppService.GetEntriesAndContestDetailsByContestPermalink(id, new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items); 
            }
            catch (UserFriendlyException e)
            {
                if (Request.IsAjaxRequest())
                {
                    return Json(new { success = false, message = e.Message });
                } 
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, e.Message));
                return View("Error", new ErrorViewModel{Exception = e});  
            }
        }
       
        [AllowAnonymous]
        public  async Task<IActionResult> Vote(CreateVoteDto model)
        {
            try
            {
                var vote = await _contestEntryVoteAppService.Create(model);
                if (vote.Id > 0)
                {
                    if (Request.IsAjaxRequest())
                    {
                        return Json(new { success = true, message = "Vote successfully casted." });
                    } 
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Vote successfully casted."));
                    return View(new CreateVoteDto());
                }
                if (Request.IsAjaxRequest())
                {
                    return Json(new { success = true, message = "Error occurred while casting your vote." });
                }      
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while casting your vote."));
                return View(model);
            }
            catch (UserFriendlyException e)
            {
                if (Request.IsAjaxRequest())
                {
                    return Json(new { success = false, message = e.Message });
                } 
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, e.Message));
                return View(new CreateVoteDto());  
            }
        }
        /// <summary>
        /// Fetches the comments of a particular entry
        /// </summary>
        /// <param name="contestEntryId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public  async Task<IActionResult> Comments(int? contestEntryId, int page = 1, int pageSize = 20)
        {
            if (contestEntryId == null)
            {
                return View("NotFound");
            }
            var offset = (page - 1) * pageSize;
            var itemGotten = await _contestEntryCommentAppService.GetAllByContestEntryId(contestEntryId.Value, new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize} );
            if (Request.IsAjaxRequest())
            {
                if (itemGotten == null)
                {
                    return Json(new {success = false, message = "There are no comments for this entry now."});
                } 
                return Json(new {success = true, data = itemGotten.Items, totalCount = itemGotten.TotalCount});

            }
            if (itemGotten == null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "The contest you are looking for was not found."));
                return View("NotFound");
            }
            return View(itemGotten);
        }

        

        public  async Task<IActionResult> CreateComment(CreateContestEntryCommentDto model)
        {
            
            if (Request.IsAjaxRequest())
            {
                if (model.Comment == null)
                {
                    return Json(new {success = false, message = "Please enter your comment."});

                }

                model.UserId = AbpSession.GetUserId();
                var item = await _contestEntryCommentAppService.Create(model);
                if (item.Id > 0)
                {
                    return Json(new {success = true, message = "Comment submitted successfully.", data = item});  
                }
                return Json(new {success = false, data = "Error occurred while posting your comment."});

            }
            
            if (model.Comment == null)
            {
                return Json(new {success = false, message = "Please enter your comment."});

            }

            var item2 = await _contestEntryCommentAppService.Create(model);
            if (item2.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Comment submitted successfully."));

            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while submitting your comment."));

            return View(model);
        } 
        [HttpPost]
        public  async Task<IActionResult> IncrementView(CreateContestEntryViewDto model)
        {
            if (Request.IsAjaxRequest())
            {
                model.UserId = AbpSession.UserId;
                var item = await _contestEntryViewAppService.Create(model);
                if (item.Id > 0)
                {
                    return Json(new {success = true, message = "View submitted successfully."});  
                }
                return Json(new {success = false, data = "Error occurred while posting your view."});

            }
            return RedirectToAction("Active");
        }
        [AllowAnonymous]
        public async Task<ActionResult> GetRandomFeaturedContestEntries()
        {
            var items = await _featuredContestEntryAppService.GetRandomLive();
            return View("_FeaturedRandomContestEntryLive", items);
        }
        #endregion
        #region AdminPages
        [AbpMvcAuthorize]
        public async Task<IActionResult> PersonalEntries(int page = 1, int pageSize = 20)
        {
            ViewBag.ActiveActiveContest = "active";
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _contestEntryAppService.GetAllByUserIdWithDetails(AbpSession.GetUserId(),
                new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize}, true);
            return View(items); 
        } 
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin, PermissionNames.Pages_Sponsor})]
        public async Task<IActionResult> EntriesAdmin(string id, int page = 1, int pageSize = 20)
        {
            try
            {
                ViewBag.ContestPermalink = id;
                var offset = (page - 1) * pageSize;  
                ViewBag.PageSize = pageSize;
                ViewBag.Page = page;
                var items = await _contestEntryAppService.GetAllByContestPermalinkWithDetails(id, new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
                return View(items); 
            }
            catch (Exception e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, e.Message));
                return RedirectToAction("Contests", "Contest");
            }
           
        }
        
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin, PermissionNames.Pages_Sponsor})]
        public  async Task<IActionResult> ChangeStatus(UpdateStatusDto model, string permalink)
        {
            var savedItem = await _contestEntryAppService.UpdateStatusAsync(model);
            if (savedItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Item updated successfully."));
                return RedirectToAction("EntriesAdmin", new {id = permalink});
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while updating contest."));
            return RedirectToAction("EntriesAdmin", new {id = permalink});
        }
        #endregion
    }
}