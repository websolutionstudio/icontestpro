﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Dependency;
using Dapper;
using Hangfire;
using IContestPro.Authorization.Users;
using IContestPro.BackgroundJobs;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.EmailTemplate;
using IContestPro.EntityFrameworkCore;
using ImageMagick;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SendGrid.Helpers.Mail;
using Xabe.FFmpeg;
using Xabe.FFmpeg.Enums;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]
    public class TestController : BaseContestController
    {
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager _userManager;
        private readonly IEmailTemplateAppService _emailTemplateAppService;
        private readonly IContestProDbContextFactory _contestProDbContextFactory;

        public TestController(IIocResolver iocResolver, IConfiguration configuration, IHostingEnvironment hostingEnvironment, UserManager userManager, IEmailTemplateAppService emailTemplateAppService, IContestProDbContextFactory contestProDbContextFactory) : base(iocResolver)
        {
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
            _emailTemplateAppService = emailTemplateAppService;
            _contestProDbContextFactory = contestProDbContextFactory;
        }
        
        [AllowAnonymous]
        public async Task<ActionResult> TestVideoSnapshot(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Json("Please supply the name parameter.");  
            }

            var guid = Guid.NewGuid();
            var fileName = guid + FileExtensions.Png;
            var output = Path.Combine(_hostingEnvironment.WebRootPath + "/images/uploads/", fileName);
            var result = await Conversion.Snapshot(_hostingEnvironment.WebRootPath + "/videos/" + name + ".mp4", output, TimeSpan.FromSeconds(0))
                .Start();
            
            //Todo: Optimize image
            var file = new FileInfo(output);

            //Console.WriteLine("Bytes before: " + file.Length);
            var sizeBefore = "Bytes before png: " + file.Length;
            
            // Read image from file
            using (var image = new MagickImage(file))
            {
                // Sets the output format to jpeg
                image.Format = MagickFormat.Jpeg;
                // Create byte array that contains a jpeg file
                image.Quality = 10;
                //image.Resize(40, 40);
                System.IO.File.WriteAllBytes(output, image.ToByteArray());
            }
           
            return Json($"{sizeBefore}, {output}");
        }
        public async Task<IActionResult> TestPostRequestFromApp(string reference)
        {
            /*
            var header = new Dictionary<string, string>();
            header.Add("Authorization", AppConsts.PaystackApiKey);
            var httpRequest = HttpRequestAppService.Get<TransactionResponseDto>(
                $"{AppConsts.PaystackApiBase}transaction/verify/{reference}", header);
            return Json(httpRequest);
            */

            using (var client = new HttpClient())
            {
                var uri = new Uri($"{AppConsts.PaystackApiBase}transaction/verify/{reference}");
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {AppConsts.PaystackApiPublicKey}");
                var response = await client.GetAsync(uri);

                string textResult = await response.Content.ReadAsStringAsync();
                return Json(textResult);
            }
        } 
        public ActionResult TestEmailSending()
        {
            //var apiKey = _configuration.GetSection("SENDGRID_API_KEY").Value;
            //var client = new SendGridClient(apiKey);
            var from = new EmailAddress("no-reply@icontestpro.com", "iContestPRO");
            var tos = new List<EmailAddress>
            {
                new EmailAddress("fidelisekeh@gmail.com", "Fidelis Gmail"),
                new EmailAddress("fidelisekeh30@yahoo.com", "Fidelis Yahoo"),
            };
        
            const string subject = "Hello world email from iContestPRO";
            const string htmlContent = "<strong>Hello world with HTML content</strong>";
            BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                (
                    new SendEmailDto
                    {
                        ShouldUseDefaultEmailTemplate = true,
                        Body = htmlContent,
                        From = from,
                        Subject = subject,
                        Tos = tos
                    })
            );
           
            /*const bool displayRecipients = true; // set this to true if you want recipients to see each others mail id 
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, tos, subject, "", htmlContent, displayRecipients);
            var response = await client.SendEmailAsync(msg);*/
            return View();
        }
        public async Task<ActionResult> TestGetUser(long userId)
        {
            var user = await _userManager.FindByEmailAsync(AppConsts.DefaultTenantAdminEmail);
            return Json(user);
        }
        
       public async Task<ActionResult> TestNewUserCreationEmail(long userId)
        {
            var user = await _userManager.FindByEmailAsync("fidelisekeh@gmail.com");
            var from = new EmailAddress("no-reply@icontestpro.com", "iContestPRO");
            var tos = new List<EmailAddress>
            {
                new EmailAddress("fidelisekeh@gmail.com", "Fidelis Gmail"),
                new EmailAddress("fidelisekeh30@yahoo.com", "Fidelis Yahoo"),
            };
        
            const string subject = "Account creation was successful";
            var item = await _emailTemplateAppService.GetEmailTemplateByName(AppConsts.EmailTemplateNewUserCreation);
            var htmlContent = item.Template.Replace("[newusernickname]", user.NickName);
            htmlContent = htmlContent.Replace("[activationlink]", "http://www.icontestpro.com/Account/Register");
            BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                (
                    new SendEmailDto
                    {
                        Body = htmlContent,
                        From = from,
                        Subject = subject,
                        Tos = tos
                    })
            );
            return Json(user);
        }

        public async Task<ActionResult> TestIndexColumns(string tableName, string columnName){
            if (string.IsNullOrEmpty(tableName) || string.IsNullOrEmpty(columnName))
            {
                return Json("Please enter table name and column name");
            }
            var sql = $"CREATE NONCLUSTERED INDEX [IX_{tableName}_{columnName}] ON [dbo].[{tableName}]([{columnName}] ASC)"; 
            using(var context = _contestProDbContextFactory.CreateDbContext(new[] {""}))
            {
               await context.Database.GetDbConnection().ExecuteScalarAsync(sql);
            }  
            return Json(sql);
        }
        
 public async Task<ActionResult> GenerateUniqueCodes(int codeLength, int totalToGenerate = 2000)
 {
         //var code =  new StringBuilder();
         var code =  new List<string>();
         var collisionList =  new List<string>();
     while (totalToGenerate > 0)
     {
         var item = Guid.NewGuid().ToString("n").Substring(0, codeLength).ToUpper();
         if (code.FirstOrDefault(i => i == item) != null)
         {
             collisionList.Add(item);    
         }
         code.Add(item);
         totalToGenerate--;
     }
     var generated = "<h1>Collisions</h1>" +  collisionList.Count + "<br />";
      generated += string.Join("<br />", code);
     return Content(generated);

     }
        

        
    }
}
