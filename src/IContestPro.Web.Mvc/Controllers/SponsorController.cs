﻿using System;
using System.IO;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.AspNetCore.Mvc.Extensions;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Mvc.Alerts;
using IContestPro.AssetUpload;
using IContestPro.AssetUpload.Dto;
using IContestPro.Authorization.Roles;
using IContestPro.Sponsor;
using IContestPro.Sponsor.Dto;
using IContestPro.SponsorFollow;
using IContestPro.SponsorFollow.Dto;
using IContestPro.Users;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Controllers
{
    /// <summary>
    /// Manages the company information
    /// </summary>
    [AbpMvcAuthorize]
    public class SponsorController : BaseController
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ISponsorAppService _sponsorAppService;
        private readonly IUserAppService _userAppService;
        private readonly IAssetUploadAppService _assetUploadAppService;
        private readonly ISponsorFollowAppService _sponsorFollowAppService;
         

        public SponsorController(IIocResolver iocResolver, IHostingEnvironment hostingEnvironment, 
            ISponsorAppService sponsorAppService, IUserAppService userAppService, IAssetUploadAppService assetUploadAppService, ISponsorFollowAppService sponsorFollowAppService): base(iocResolver)
        {
            _hostingEnvironment = hostingEnvironment;
            _sponsorAppService = sponsorAppService;
            _userAppService = userAppService;
            _assetUploadAppService = assetUploadAppService;
            _sponsorFollowAppService = sponsorFollowAppService;
        }
        // GET
        public async Task<IActionResult> Index()
        {
            if (!User.IsInRole(StaticRoleNames.Tenants.Sponsors))
            {
                return View(new SponsorDto());  
            }
            var company = await _sponsorAppService.GetByUserId(AbpSession.GetUserId());
            return View(company);  

        }
        
        [AllowAnonymous]
        public async Task<IActionResult> Company(string id)
        {
            var company = await _sponsorAppService.GetByPermalink(id);
            return View("DetailsAdmin", company);  

        }
        /// <summary>
        /// Details page for public area
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public  async Task<IActionResult> Details(string id)
        {
            var item = await _sponsorAppService.GetByPermalink(id);
            if (item == null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "The contest you are looking for was not found."));
                return View("NotFound");
            }
            return View(item);
        } 
        
        
        public IActionResult Upgrade()
        {
            return View(new CreateSponsorDto());
        }

        
        [HttpPost]
        public async Task<IActionResult> Upgrade(CreateSponsorDto model)
        {
            if (ModelState.IsValid)
            {
             var company =  await _sponsorAppService.Create(model);
                if (company.Id > 0)
                {
                        var result = await _userAppService.AddToSponsorRoleAsync(model.UserId);
                    if (result.Succeeded)
                    {
                        await UnitOfWorkManager.Current.SaveChangesAsync();
                        
                        AddAlertMessage(
                            new AlertMessageCustom(
                                AlertType.Success, "Company information updated successfully."
                            ));
                        return RedirectToAction("Logout", "Account");
                    }
                       
                }
                AddAlertMessage(
                    new AlertMessageCustom(
                        AlertType.Danger, "Error occurred while completing your request. Please try again later."
                    ));  
                return View(model);
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Some required fields where not supply. Please check and try again."));

            return View(model);
        }
        
        public  async Task<IActionResult> Edit(int id)
        {
            var company = await _sponsorAppService.Get(new EntityDto{Id = id});

            return View(company);
        }

        [HttpPost]
        [UnitOfWork]
        public async Task<IActionResult> Edit(SponsorDto model)
        {
            if (ModelState.IsValid)
            {
                var company =  await _sponsorAppService.Update(model);
                if (company.Id > 0)
                {
                    AddAlertMessage(
                        new AlertMessageCustom(
                            AlertType.Success, "Company information updated successfully."
                        ));
                    return RedirectToAction("Index");
                }
                AddAlertMessage(
                    new AlertMessageCustom(
                        AlertType.Danger, "Error occurred while completing your request. Please try again later."
                    ));  
                return View(model);
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Some required fields where not supply. Please check and try again."));

            return View(model);
        }
        public async Task<IActionResult> UploadFile(IFormFile file, string removeImage = "")
        {
            if(Request.IsAjaxRequest() || IsMobile)
            {
                if (file == null || file.Length == 0)
                {
                    return Json(new {success = false, message = "Please upload a file."});    
                  }
                if (file.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    return Json(new {success = false, message = "Please select only image file."});    
                }
                //var uniqueFileName = Guid.NewGuid().ToString("N") + ".jpg";
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploads");
                //var filePath = Path.Combine(uploads, uniqueFileName);
                var result = await _assetUploadAppService.UploadImage(new UploadDto{File = file, Path = "images/uploads"});
                //await file.CopyToAsync(new FileStream(filePath, FileMode.Create));
                if (!string.IsNullOrEmpty(removeImage))
                {
                    var fileToDelete = Path.Combine(uploads, removeImage);
                    if (System.IO.File.Exists(fileToDelete))
                    {
                      System.IO.File.Delete(fileToDelete);
                    }
                }
             
                    return Json(new {success = true, message = "Banner uploaded successfully.", filename = result.FileName});    

                
            }

            return View();

        }
        public IActionResult DeleteFile(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                if (Request.IsAjaxRequest())
                {
                    var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploads");
                    var fileToDelete = Path.Combine(uploads, filename);
                    if (System.IO.File.Exists(fileToDelete))
                    {
                        System.IO.File.Delete(fileToDelete);
                        return Json(new {success = true, message = "File deleted successfully."});    
                    }
                    else
                    {
                        return Json(new {success = false, message = "File was not found."});      
                    }
                }
            }
            else
            {
                return Json(new {success = false, message = "File deleted successfully."});    
            }
            return View();
        }
        
        [AbpMvcAuthorize]
        [HttpPost]
        public async Task<ActionResult> Follow(CreateSponsorFollowDto model)
        {
            try
            {
                var item = await _sponsorFollowAppService.Create(model);
                if (item.Id > 0)
                {
                    return Json(new {success = true, message = "You are now following this sponsor."});  
                }
                return Json(new {success = false, message = "Error occurred while completing your request."});
            }
            catch (UserFriendlyException e)
            {
                return Json(e.Message.Contains("already following") ?
                    new {success = false, message = e.Message} : 
                    new {success = false, message = "Error occurred while completing your request."});
            }
           
           
        } 
    }
}