﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Mvc.Alerts;
using IContestPro.AdvertPrice;
using IContestPro.AdvertPrice.Dto;
using IContestPro.Authorization;
using IContestPro.Contest;
using IContestPro.Contest.Dto;
using IContestPro.FeaturedContest;
using IContestPro.FeaturedContest.Dto;
using IContestPro.Helpers;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]
    public class FeaturedContestsController : BaseController
    {
        private readonly IFeaturedContestAppService _featuredContestAppService;
        private readonly IAdvertPriceAppService _advertPriceAppService;
        private readonly IContestAppService _contestAppService;


        public FeaturedContestsController(IIocResolver iocResolver,
            IAdvertPriceAppService advertPriceAppService, IFeaturedContestAppService featuredContestAppService, IContestAppService contestAppService): base(iocResolver)
        {
            _advertPriceAppService = advertPriceAppService;
            _featuredContestAppService = featuredContestAppService;
            _contestAppService = contestAppService;
        }
        // GET
       
        public async Task<IActionResult> Index(int page = 1, int pageSize = 20)
        {
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var offset = (page - 1) * pageSize;
            //var itemsRandom = await _featuredContestAppService.GetRandomLive();
            PagedResultDto<FeaturedContestDto> items;
            if (await IsGrantedAsync(PermissionNames.Pages_Admin))
            {
                 items = await _featuredContestAppService.GetAll( new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            }
            else
            {
                 items = await _featuredContestAppService.GetAllByUser(AbpSession.GetUserId(), new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            }
             return View(items);
        }
        
        [HttpPost]
        [UnitOfWork]
        public  async Task<IActionResult> Delete(EntityDto model)
        {
            await _featuredContestAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Featured contest deleted successfully."));
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> CreateEdit(CreateEditFeaturedContestDto model, int? reference)
        {
            var selectList = await _advertPriceAppService.GetAllByType(AdvertType.Contest,
                new PagedResultRequestDto {SkipCount = 0, MaxResultCount = 100});
            var items = selectList.Items.Select(item =>
                new AdvertPriceDto
                {
                    Id = item.Id,
                    Amount = item.Amount,
                    Name = $"{item.Name} | {item.Amount.ToNairaString()}"
                }).ToList();
            ViewBag.AdvertPrices = new SelectList(items, "Id", "Name");
            var contestItems = await _contestAppService.GetAllByUserIdAndStatus(AbpSession.GetUserId(),
                AppConsts.StatusLive, new PagedResultRequestDto {SkipCount = 0, MaxResultCount = 100});
            if (!contestItems.Items.Any())
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "There is no contest to promote."));
                return RedirectToAction("Index");
            }

            ViewBag.Contests = new SelectList(contestItems.Items, "Id", "Title", reference);
            if (model.Id == null)
            {
            return View(new CreateEditFeaturedContestDto());
              }

        //Edit mode
                var itemToEdit = await _featuredContestAppService.Get(new EntityDto{Id = model.Id.Value});
                ViewBag.AdvertPrices = new SelectList(items, "Id", "Name", itemToEdit.AdvertPriceId);
                ViewBag.Contests =  new SelectList(contestItems.Items, "Id", "Title", itemToEdit.ContestId);
                var input = itemToEdit.MapTo<CreateEditFeaturedContestDto>(); 
                return View(input);
         
        } 
        [HttpPost]
        [UnitOfWork(false)]
        [AbpMvcAuthorize]
        public  async Task<IActionResult> CreateEdit(CreateEditFeaturedContestDto model, string forgetMe)
        {
            try
            {
                var selectList = await _advertPriceAppService.GetAllByType(AdvertType.Contest,new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
                var items = selectList.Items.Select(item => 
                    new AdvertPriceDto {
                        Id = item.Id, 
                        Amount = item.Amount, 
                        Name = $"{item.Name} | {item.Amount.ToNairaString()}"
                    }).ToList();
                ViewBag.AdvertPrices = new SelectList(items, "Id", "Name", model.AdvertPriceId);
                var contestItems = await _contestAppService.GetAllByUserIdAndStatus(AbpSession.GetUserId(), AppConsts.StatusLive, 
                    new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
                ViewBag.Contests =  new SelectList(contestItems.Items, "Id", "Title", model.ContestId);

                if (model.StartDate == null || model.AdvertPriceId == null || model.ContestId == null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "StartDate date, duration and contest are required."));
                    return View(model);

                }
          
            if (model.Id == null)
            {
                model.UserId = AbpSession.GetUserId();
                var item = await _featuredContestAppService.Create(model.MapTo<CreateFeaturedContestDto>());
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Featured contest created successfully."));
                    return RedirectToAction("Index");
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
                return View(model);
            }
           
            //Edit mode
                var dbModel = new FeaturedContestDto
                {
                    Id = model.Id.Value,
                    TenantId = model.TenantId,
                    StartDate = model.StartDate.Value,
                    StatusId = model.StatusId.Value,
                    UserId = model.UserId.Value,
                    AdvertPriceId = model.AdvertPriceId.Value,
                    ContestId = model.ContestId.Value,
                };
            var updateItem = await _featuredContestAppService.Update(dbModel);
            if (updateItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Featured contest updated successfully."));
                return RedirectToAction("Index");
            }
            }
            catch (UserFriendlyException e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, e.Message));
                return View(model); 
            }
            catch (Exception e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
                return View(model);
            }
            
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
            return View(model);
          
        } 
        
        [HttpPost]
        [UnitOfWork(false)]
        public  async Task<IActionResult> ChangeStatus(UpdateStatusDto model)
        {
            var savedItem = await _featuredContestAppService.UpdateStatusAsync(model);
            if (savedItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Featured contest status updated successfully."));
                return RedirectToAction("Index");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while updating request."));
            return RedirectToAction("Index");
        }
        
        
    }
}