﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.UI;
using IContestPro.Authorization;
using IContestPro.Controllers;
using IContestPro.Follow;
using IContestPro.Follow.Dto;
using IContestPro.Users;
using IContestPro.Web.Models.Users;
using Microsoft.AspNetCore.Authorization;

namespace IContestPro.Web.Controllers
{
    public class UsersController : IContestProControllerBase
    {
        private readonly IUserAppService _userAppService;
        private readonly IFollowAppService _followAppService;

        public UsersController(IUserAppService userAppService, IFollowAppService followAppService)
        {
            _userAppService = userAppService;
            _followAppService = followAppService;
        }
        
        [AbpMvcAuthorize(PermissionNames.Pages_Admin)]
        public async Task<ActionResult> Index()
        {
            var users = (await _userAppService.GetAll(new PagedResultRequestDto {MaxResultCount = int.MaxValue})).Items; // Paging not implemented yet
            var roles = (await _userAppService.GetRoles()).Items;
            var model = new UserListViewModel
            {
                Users = users,
                Roles = roles
            };
            return View(model);
        }

        [AbpMvcAuthorize(PermissionNames.Pages_Admin)]
        public async Task<ActionResult> EditUserModal(long userId)
        {
            var user = await _userAppService.Get(new EntityDto<long>(userId));
            var roles = (await _userAppService.GetRoles()).Items;
            var model = new EditUserModalViewModel
            {
                User = user,
                Roles = roles
            };
            return View("_EditUserModal", model);
        }
        [AbpMvcAuthorize]
        [HttpPost]
        public async Task<ActionResult> Follow(CreateFollowDto model)
        {
            try
            {
                var item = await _followAppService.Create(model);
                if (item.Id > 0)
                {
                    return Json(new {success = true, message = "You are now following this user."});  
                }
                return Json(new {success = false, message = "Error occurred while completing your request."});
            }
            catch (UserFriendlyException e)
            {
                return Json(new {success = false, message = e.Message});
  
            }
           
           
        } 
        [AllowAnonymous]
        public async Task<ActionResult> Details(string id)
        {
            var user = await _userAppService.GetDetailsByPermalink(id);
            if (user == null)
            {
                return View("NotFound");
            }
            return View(user);
        }
        [AbpMvcAuthorize]
        public async Task<IActionResult> GetCurrentUserFollowers(string nickNameSuggestion = "")
        {
            var items = await _followAppService.GetCurrentUserFollowers(nickNameSuggestion);
            return View("_CurrentUserFollowers", items);
        }
    }
}
