﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Mvc.Alerts;
using IContestPro.AdvertHomePageBanner;
using IContestPro.AdvertHomePageBanner.Dto;
using IContestPro.AdvertPrice;
using IContestPro.AdvertPrice.Dto;
using IContestPro.Authorization;
using IContestPro.Contest.Dto;
using IContestPro.Helpers;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomePageBannersController : BaseController
    {
        private readonly IAdvertHomePageBannerAppService _advertHomePageBannerAppService;
        private readonly IAdvertPriceAppService _advertPriceAppService;


        public HomePageBannersController(IIocResolver iocResolver, IAdvertHomePageBannerAppService advertHomePageBannerAppService, 
            IAdvertPriceAppService advertPriceAppService): base(iocResolver)
        {
            _advertHomePageBannerAppService = advertHomePageBannerAppService;
            _advertPriceAppService = advertPriceAppService;
        }
        // GET
        public async Task<IActionResult> Index(int page = 1, int pageSize = 20)
        {
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var offset = (page - 1) * pageSize;
            PagedResultDto<AdvertHomePageBannerDto> items;
            if (await IsGrantedAsync(PermissionNames.Pages_Admin))
            {
                 items = await _advertHomePageBannerAppService.GetAll( new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            }
            else
            {
                 items = await _advertHomePageBannerAppService.GetAllByUser(AbpSession.GetUserId(), new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            }
             return View(items);
        }
        
        [HttpPost]
        [UnitOfWork]
        public  async Task<IActionResult> Delete(EntityDto model)
        {
            await _advertHomePageBannerAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Home page banner deleted successfully."));
            return RedirectToAction("Index");
        } 
       
        public  async Task<IActionResult> CreateEdit(CreateEditHomePageBannerDto model)
        {
            var selectList = await _advertPriceAppService.GetAllByType(AdvertType.HomePageBanner, new PagedAndSortedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            var items = selectList.Items.Select(item => 
                new AdvertPriceDto {Id = item.Id, Amount = item.Amount, 
                    Name = $"{item.Name} | {item.Amount.ToNairaString()}"}).ToList();
            ViewBag.AdvertPrices = new SelectList(items, "Id", "Name");
            if(model.Id == null)
            return View(new CreateEditHomePageBannerDto());
         
            //Edit mode
                var itemToEdit = await _advertHomePageBannerAppService.Get(new EntityDto{Id = model.Id.Value});
                ViewBag.AdvertPrices = new SelectList(selectList.Items, "Id", "Name", itemToEdit.AdvertPriceId);
                ViewBag.Image = itemToEdit.Image;
            var input = new CreateEditHomePageBannerDto
            {
            AdvertPriceId = itemToEdit.AdvertPriceId,
            EndDate = itemToEdit.EndDate,
            Id = itemToEdit.Id,
            StartDate = itemToEdit.StartDate,
            StatusId = itemToEdit.StatusId,
            TenantId = itemToEdit.TenantId,
            UserId = itemToEdit.UserId,
            Title = itemToEdit.Title,
            Description = itemToEdit.Description,
            ExternalUrl = itemToEdit.ExternalUrl
            };
                return View(input);
         
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize]
        public  async Task<IActionResult> CreateEdit(CreateEditHomePageBannerDto model, string notUsed)
        {
            try
            {
                if (model.Title == null || model.Description == null || model.StartDate == null || model.ExternalUrl == null || model.AdvertPriceId == null)
                {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Title, description, start date, external url and duration are required."));
                return View(model);

            }
            var selectList = await _advertPriceAppService.GetAllByType(AdvertType.HomePageBanner, new PagedAndSortedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            var items = selectList.Items.Select(item => 
                new AdvertPriceDto {Id = item.Id, Amount = item.Amount, 
                    Name = $"{item.Name} | {item.Amount.ToNairaString()}"}).ToList();
            ViewBag.AdvertPrices = new SelectList(items, "Id", "Name");
            
            if (model.Id == null)
            {
                if(model.Image == null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "ContestEntryImage is required."));
                    return View(model);
                }
                using (var imageCheck = Image.FromStream(model.Image.OpenReadStream()))
                {
                    if (imageCheck.Width != 1920 || imageCheck.Height != 1200)
                    {
                        AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Banner dimension must be width: 1920px and height: 1200px."));
                        return View(model); 
                    }
                }
                model.UserId = AbpSession.GetUserId();
                var item = await _advertHomePageBannerAppService.CreateAndUploadImage(model);
                if (item.Id > 0)
                {
                    await GrayImageColor($"images/uploads/{item.Image}");
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Home page banner created successfully."));
                    return RedirectToAction("Index");
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
                return View(model);
            }
            
            //Edit mode
            if(model.Image != null)
            {
                using (var imageCheck = Image.FromStream(model.Image.OpenReadStream()))
                {
                    if (imageCheck.Width != 1920 || imageCheck.Height != 1200)
                    {
                        AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Banner dimension must be width: 1920px and height: 1200px."));
                        return View(model); 
                    }
                }
            }
            var updateItem = await _advertHomePageBannerAppService.UpdateAndUploadImage(model);
            if (updateItem.Id > 0)
            {
                await GrayImageColor($"images/uploads/{updateItem.Image}");
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Home page banner updated successfully."));
                return RedirectToAction("Index");
            }
            }
            catch (UserFriendlyException e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, e.Message));
                return View(model); 
            }
            catch (Exception e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
                return View(model);
            }
            
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
            return View(model);
          
        } 
        [HttpPost]
        public  async Task<IActionResult> ChangeHomePageBannerStatus(UpdateStatusDto model)
        {
            var savedItem = await _advertHomePageBannerAppService.UpdateStatusAsync(model);
            if (savedItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Home page banner request updated successfully."));
                return RedirectToAction("Index");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while updating request."));
            return RedirectToAction("Index");
        }
        
        
    }
}