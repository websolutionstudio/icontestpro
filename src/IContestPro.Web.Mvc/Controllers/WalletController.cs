﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Alerts;
using IContestPro.Authorization;
using IContestPro.BankDetails;
using IContestPro.BankDetails.Dto;
using IContestPro.Contest.Dto;
using IContestPro.ExportAppService;
using IContestPro.ExportAppService.Dto;
using IContestPro.Helpers;
using IContestPro.PaymentHistories;
using IContestPro.PaymentHistories.Dto;
using IContestPro.Transfer;
using IContestPro.Transfer.Dto;
using IContestPro.Users;
using IContestPro.Wallet;
using IContestPro.Web.Helpers.AlertExtension;
using IContestPro.Withdraw;
using IContestPro.Withdraw.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using OfficeOpenXml;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]
    public class WalletController : BaseController
    {
        private readonly IWalletAppService _walletAppService;
        private readonly IPaymentHistoriesAppService _paymentHistoriesAppService;
        private readonly IBankDetailsAppService _bankDetailsAppService;
        private readonly IWithdrawAppService _withdrawAppService;
        private readonly IExportAppService _exportAppService;
        private readonly IUserAppService _userAppService;
        private readonly ITransferAppService _transferAppService;


        public WalletController(IIocResolver iocResolver, IWalletAppService walletAppService, 
            IPaymentHistoriesAppService paymentHistoriesAppService, IBankDetailsAppService bankDetailsAppService
, IWithdrawAppService withdrawAppService, IExportAppService exportAppService, IUserAppService userAppService, ITransferAppService transferAppService): base(iocResolver)
        {
            _walletAppService = walletAppService;
            _paymentHistoriesAppService = paymentHistoriesAppService;
            _bankDetailsAppService = bankDetailsAppService;
             _withdrawAppService = withdrawAppService;
            _exportAppService = exportAppService;
            _userAppService = userAppService;
            _transferAppService = transferAppService;
        }
        // GET
        public async Task<IActionResult> Index(int page = 1, int pageSize = 20, string startDate = null, string endDate = null)
        {
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            var offset = (page - 1) * pageSize;
            var startDateParseResult = DateTime.TryParse(startDate, out var startDateParse);
            var endDateParseResult = DateTime.TryParse(endDate, out var endDateParse);
            ViewBag.Wallet = await _walletAppService.GetByUserId(AbpSession.GetUserId());
            PagedResultDto<PaymentHistoryDto> items;
            if (await IsGrantedAsync(PermissionNames.Pages_Admin))
            {
                if (startDateParseResult && endDateParseResult)
                    items = await _paymentHistoriesAppService.GetAllBetweenDates(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize}, startDateParse, endDateParse);
                else
                items = await _paymentHistoriesAppService.GetAll(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
                return View(items); 
            }
            
            if (startDateParseResult && endDateParseResult)
                items = await _paymentHistoriesAppService.GetAllByUserBetweenDates(AbpSession.GetUserId(), new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize}, startDateParse, endDateParse);
            else
            items = await _paymentHistoriesAppService.GetAllByUser(AbpSession.GetUserId(), new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items);
        }
        
        public IActionResult Deposit()
        {
          return View();
        }
       
        public async Task<IActionResult> Withdraw(int page = 1, int pageSize = 20, string startDate = null, string endDate = null)
        {
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            var startDateParseResult = DateTime.TryParse(startDate, out var startDateParse);
            var endDateParseResult = DateTime.TryParse(endDate, out var endDateParse);
            var offset = (page - 1) * pageSize;
            PagedResultDto<WithdrawDto> items;
            if (await IsGrantedAsync(PermissionNames.Pages_Admin))
            {
                if (startDateParseResult && endDateParseResult)
                    items = await _withdrawAppService.GetAllBetweenDates(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize}, startDateParse, endDateParse);
                    else
                 items = await _withdrawAppService.GetAll(new PagedResultRequestDto
                {
                    SkipCount = offset,
                    MaxResultCount = pageSize
                });
                return View(items); 
            }
            if (startDateParseResult && endDateParseResult)
                items = await _withdrawAppService.GetAllByUserBetweenDates(AbpSession.GetUserId(), new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize}, startDateParse, endDateParse);
                else
            items = await _withdrawAppService.GetByUserId(AbpSession.GetUserId(), new PagedResultRequestDto
            {
                SkipCount = offset,
                MaxResultCount = pageSize
            });
            return View(items); 
        }
        
        public async Task<IActionResult> CreateWithdrawal()
        {           
            var balance = await _walletAppService.GetBalance(AbpSession.GetUserId());
            var minimumWithdrawAmount = await SettingManager.GetSettingValueAsync<decimal>(AppConsts.SettingsWithdrawalMinimumAmount);
            var withdrawalFeePercentage = await SettingManager.GetSettingValueAsync<decimal>(AppConsts.SettingsWithdrawalRequestFeePercentage);

            if (balance < minimumWithdrawAmount)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"Insuficient account balance. Your account balance must be above {minimumWithdrawAmount} to complete your request."));
                return RedirectToAction("Withdraw", "Wallet");
            }
            var selectList = await _bankDetailsAppService.GetByUserId(AbpSession.GetUserId(), new PagedAndSortedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            if (selectList.Items.Count == 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Please add bank details to complete your request."));
                return RedirectToAction("CreateBankDetails", "Bank");
            }
             var defaultDetails = selectList.Items.FirstOrDefault(itm => itm.IsDefault);
            var items = selectList.Items.Select(selectListItem => 
                                new BankDetailsDto {Id = selectListItem.Id, AccountName = $"{(selectListItem.AccountName.Length > 10 ? selectListItem.AccountName.Substring(0,10) : selectListItem.AccountName)}|{selectListItem.AccountNumber}|{selectListItem.Bank.Name}" +
                                     (defaultDetails?.Id == selectListItem.Id ? "(Default)" : "")}).ToList();
            ViewBag.BankDetails = new SelectList(items, "Id", "AccountName", defaultDetails?.Id);
            ViewBag.AccountBalance = balance;
            ViewBag.MinimumWithdrawAmount = minimumWithdrawAmount;
            ViewBag.WithdrawalFeePercentage = withdrawalFeePercentage;
            return View(new CreateWithdrawDto()); 
        }
        [HttpPost]
        [AbpMvcAuthorize]
        public  async Task<IActionResult> CreateWithdrawal(CreateWithdrawDto model)
        {
            var selectList = await _bankDetailsAppService.GetByUserId(AbpSession.GetUserId(), new PagedAndSortedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            var defaultDetails = selectList.Items.FirstOrDefault(itm => itm.IsDefault);
            var items = selectList.Items.Select(selectListItem => 
                new BankDetailsDto {Id = selectListItem.Id,AccountName = $"{(selectListItem.AccountName.Length > 10 ? selectListItem.AccountName.Substring(0,10) : selectListItem.AccountName)}|{selectListItem.AccountNumber}|{selectListItem.Bank.Name}" +
                                                  (defaultDetails?.Id == selectListItem.Id ? "(Default)" : "")}).ToList();
            ViewBag.BankDetails = new SelectList(items, "Id", "AccountName", model.BankDetailsId);

            var balance = await _walletAppService.GetBalance(AbpSession.GetUserId());
            var minimumWithdrawAmount = await SettingManager.GetSettingValueAsync<decimal>(AppConsts.SettingsWithdrawalMinimumAmount);
            var withdrawalFeePercentage = await SettingManager.GetSettingValueAsync<decimal>(AppConsts.SettingsWithdrawalRequestFeePercentage);
            if (model.Amount < minimumWithdrawAmount)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"Withdrawal amount must be from {minimumWithdrawAmount} and above."));
                return View(model);

            }
            if (balance < minimumWithdrawAmount)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"Insuficient account balance. Your account balance must be above {minimumWithdrawAmount} to complete your request."));
                return RedirectToAction("Withdraw", "Wallet");
            }
            ViewBag.AccountBalance = balance;
            ViewBag.MinimumWithdrawAmount = minimumWithdrawAmount;
            ViewBag.WithdrawalFeePercentage = withdrawalFeePercentage;
            var result = await _withdrawAppService.Create(model);
            if (result != null && result.Id > 0)
            {
                
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Withdrawal request created successfully. Thank you."));
                return RedirectToAction("Withdraw");
            }
               
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"Error occurred while completing your withdrawal request. Please try again later."));
            return View(model);
          
        }
        [HttpPost]
        public  async Task<IActionResult> CancelRequest(EntityDto model)
        {
            var savedItem = await _withdrawAppService.CancelWithdrawalRequestAsync(model);
            if (savedItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Withdrawal request was cancelled successfully."));
                return RedirectToAction("Withdraw");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while cancelling request."));
            return RedirectToAction("Withdraw");
        }
        
        [HttpPost]
        public  async Task<IActionResult> ChangeWithdrawStatus(UpdateStatusDto model)
        {
            var savedItem = await _withdrawAppService.UpdateStatusAsync(model);
            if (savedItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Withdrawal request updated successfully."));
                return RedirectToAction("Withdraw");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while updating request."));
            return RedirectToAction("Withdraw");
        }
        
        
          public async Task<IActionResult> Transfer()
        {           
            var balance = await _walletAppService.GetBalance(AbpSession.GetUserId());
            var transferFeePercentage = await SettingManager.GetSettingValueAsync<decimal>(AppConsts.SettingsTransferFeePercentage);
            ViewBag.AccountBalance = balance;
            ViewBag.TransferFeePercentage = transferFeePercentage;
            var model = TempData.ContainsKey("model") ? _walletAppService.DeserializeObject<CreateTransferDto>((string)TempData["model"])  : new CreateTransferDto();
            return View(model); 
        }
        
        [HttpPost]
        public async Task<IActionResult> TransferConfirmation(CreateTransferDto model)
        {           
            var balance = await _walletAppService.GetBalance(AbpSession.GetUserId());
            var transferFeePercentage = await SettingManager.GetSettingValueAsync<double>(AppConsts.SettingsTransferFeePercentage);
            ViewBag.AccountBalance = balance;
            ViewBag.TransferFeePercentage = transferFeePercentage;
            TempData["model"] = _walletAppService.SerializeObject(model);
            var chargeAmount = model.Amount * ((decimal) transferFeePercentage / 100);
            var total = model.Amount + chargeAmount;
            if (balance < total)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"Insufficient account balance. Transfer and charge is {total.ToNairaString()}. Please add fund to your wallet to complete your transfer."));
                return  RedirectToAction("Transfer");
            }
            long toUserId;
            var convertResult = long.TryParse(model.ToUserAppIdentity, out toUserId);
            if (!convertResult)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Please enter valid user identity code."));
                return  RedirectToAction("Transfer");
            }
            var toUser = await _userAppService.Get(new EntityDto<long> {Id = toUserId});
            if (toUser == null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Please enter valid user identity code."));
                return  RedirectToAction("Transfer");
            }
            var item = new CreateTransferConfirmationDto
            {
                Amount = model.Amount,
                ToUserAppIdentity = model.ToUserAppIdentity,
                Notes = model.Notes,
                FeePercentage = transferFeePercentage,
                FeeAmount = chargeAmount,
                ToUserFullName = toUser.NickName,
            };
            return View(item); 
        }
        [HttpPost]
        public async Task<IActionResult> TransferComplete(CreateTransferDto model)
        {           
            var balance = await _walletAppService.GetBalance(AbpSession.GetUserId());
            var transferFeePercentage = await SettingManager.GetSettingValueAsync<decimal>(AppConsts.SettingsTransferFeePercentage);
            ViewBag.AccountBalance = balance;
            ViewBag.TransferFeePercentage = transferFeePercentage;
            TempData["model"] = _walletAppService.SerializeObject(model);
          var result =  await _transferAppService.Create(model);
            if (result != null && result.Success)
            {            
                TempData.Remove("model") ;
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Transfer completed successfully."));
                return  RedirectToAction("Index"); 
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while completing your transfer."));
            return  RedirectToAction("Transfer"); 
        }
      
        public async Task<IActionResult> ExportTransactions(ExportDataInputDto input)
        {
            var items = await _exportAppService.ExportTranscations(input);
            var name = "transactions-export-" + DateTime.Now.ToString("yy-MM-dd-h_mm_sstt");
            var dt = _exportAppService.ConvertToDatatable(items);
            byte[] fileContents;
            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(name);
                worksheet.Cells["A1"].LoadFromDataTable(dt, true);
                fileContents = package.GetAsByteArray();
            }
            if (fileContents == null || fileContents.Length == 0)
            {
                return NotFound();
            }
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: name + ".xlsx"
            );
           
        }
        public async Task<IActionResult> ExportWithdrawals(ExportDataInputDto input)
        {
            var items = await _exportAppService.ExportWithdrawals(input);
            var name = "withdrawal-export-" + DateTime.Now.ToString("yy-MM-dd-h_mm_sstt");
            var dt = _exportAppService.ConvertToDatatable(items);
            byte[] fileContents;
            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(name);
                worksheet.Cells["A1"].LoadFromDataTable(dt, true);
                fileContents = package.GetAsByteArray();
            }
            if (fileContents == null || fileContents.Length == 0)
            {
                return NotFound();
            }
            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: name + ".xlsx"
            );
           
        }

    }
}