﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Dependency;
using IContestPro.Authorization;
using IContestPro.Contest.ContestVote;
using IContestPro.Contest.ContestVote.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Contest})]
    public class VotesController : BaseContestController
    {
        private readonly IContestEntryVoteAppService _contestEntryVoteAppService;
        public VotesController(IIocResolver iocResolver, 
                             IContestEntryVoteAppService contestEntryVoteAppService) : base(iocResolver)
        {
           _contestEntryVoteAppService = contestEntryVoteAppService;
        }
        // GET
        /// <summary>
        /// Get the list of voters using the contest permalink
        /// </summary>
        /// <param name="id"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns>List of <see cref="VoteDto"/> </returns>
        [AllowAnonymous]
        public  async Task<IActionResult> Contest(string id, int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            ViewBag.Permalink = id;
            var items = await _contestEntryVoteAppService.GetAllByContestPermalink(id, new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            if (items == null)
            {
                return View("NotFound");
            }
            return View(items);
        }
    }
}