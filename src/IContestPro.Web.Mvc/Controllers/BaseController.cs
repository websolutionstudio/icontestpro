﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Runtime.Security;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Alerts;
using IContestPro.Authorization.Roles;
using IContestPro.Controllers;
using IContestPro.Cookie;
using IContestPro.Logging;
using IContestPro.Notifications;
using IContestPro.Settings;
using IContestPro.Users;
using IContestPro.Users.Dto;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Image = System.Drawing.Image;

namespace IContestPro.Web.Controllers
{
    public class BaseController : IContestProControllerBase
    {
        protected readonly IAlertManagerExtension AlertManagerExtension;
        protected readonly IIocResolver IocResolver;
        protected readonly ICookieAppService CookieAppService;
        protected readonly IUserAppService UserAppService;
        protected readonly IAppNotifier AppNotifier;
        protected readonly IHostingEnvironment HostingEnvironment;
        protected readonly ILoggingAppService LoggingAppService;
        protected new readonly ISettingsAppService SettingManager;

        public BaseController(IIocResolver iocResolver)
        {
            IocResolver = iocResolver;
            AlertManagerExtension = iocResolver.ResolveAsDisposable<IAlertManagerExtension>().Object;
            CookieAppService = iocResolver.ResolveAsDisposable<ICookieAppService>().Object;
            UserAppService = iocResolver.ResolveAsDisposable<IUserAppService>().Object;
            AppNotifier = iocResolver.ResolveAsDisposable<IAppNotifier>().Object;
            HostingEnvironment = iocResolver.ResolveAsDisposable<IHostingEnvironment>().Object;
            LoggingAppService = iocResolver.ResolveAsDisposable<ILoggingAppService>().Object;
            SettingManager = iocResolver.ResolveAsDisposable<ISettingsAppService>().Object;
        }


        public UserDto CurrentUser
        {
            get
            {
                //Todo: Initialize the current user
                var cookieUserString = CookieAppService.Get(AppConsts.CookieKeyCurrentUser);
                if (cookieUserString.Value != null)
                {
                    var decrypted = Decrypt(cookieUserString.Value);
                    return DeserializeObject<UserDto>(decrypted);
                }

                return null;
            }
        }

        protected void SetCurrentUser(UserDto user)
        {
            var userString = SerializeObject(user);
            CookieAppService.Set(AppConsts.CookieKeyCurrentUser, Encrypt(userString), DateTime.Now.AddDays(1));
        }


        protected void AddAlertMessage(AlertMessageCustom alertMessageCustom)
        {
            AlertManagerExtension.Alerts.Clear();
            AlertManagerExtension.Alerts.Add(alertMessageCustom);
        }

        protected static string GetUserAppIdentity(long userId)
        {
            return userId.ToString().PadLeft(AppConsts.UserIdentityLength, '0');
        }

        protected string GetCurrentUserAppIdentity()
        {
            return AbpSession.GetUserId().ToString().PadLeft(AppConsts.UserIdentityLength, '0');
        }


        protected string GetCurrentUserReferralLink()
        {
            var link = Url.Action("Register", "Account", new {identity = GetCurrentUserAppIdentity()},
                protocol: Request.Scheme);

            return link;
        }

        private void PrepareAccountUpgradeNotification()
        {
            if (User == null || !User.IsInRole(StaticRoleNames.Tenants.Users))
            {
                return;
            }

            if (AlertManagerExtension.Alerts.Count == 0)
                AddAlertMessage(
                    new AlertMessageCustom(
                        AlertType.Info,
                        "Becoming a sponsor will enable you create contests that members can compete and vote on. " +
                        "This enables you to promote your brand and give it utmost visibility on the platform.",
                        "Become a sponsor",
                        true,
                        new List<LinkAction>
                        {
                            new LinkAction(
                                "Signup as a sponsor",
                                "/Sponsor/Upgrade",
                                ButtonType.Success
                            ),
                            new LinkAction(
                                "Read more...",
                                "/Sponsor/Index",
                                ButtonType.Danger
                            )
                        },
                        new List<LinkActionDropdown>
                        {
                            new LinkActionDropdown(
                                "Remind me tomorrow",
                                "#",
                                "remind-me-tomorrow"
                            ),
                            new LinkActionDropdown(
                                "Don't show me again",
                                "#",
                                "dont-show-again"
                            )
                        },
                        "fa-cloud-upload")
                );
        }

        public IActionResult HideSponsorNotification(DateTime? expiry)
        {
            CookieAppService.Set(AppConsts.CookieKeyHideSponsorNotification, "634348734763gu3e7834", expiry);
            return Json(new {success = true, message = "Notification saved successfully."});
        }

        protected Image CropImage(IFormFile file, int width, int height, long quality = 100L)
        {
            return CropImage(Path.GetFileName(file.FileName), width, height, quality);
        }

        protected Image CropImage(string path, int width, int height, long quality = 100L)
        {
            using (var image = Image.FromFile(path))
            {
                var thumbnail =
                    new Bitmap(width, height); // changed parm names
                var graphic = Graphics.FromImage(thumbnail);

                graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphic.SmoothingMode = SmoothingMode.HighQuality;
                graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphic.CompositingQuality = CompositingQuality.HighQuality;

                /* ------------------ new code --------------- */

                // Figure out the ratio
                var ratioX = (double) width / (double) image.Width;
                var ratioY = (double) height / (double) image.Height;
                // use whichever multiplier is smaller
                var ratio = ratioX < ratioY ? ratioX : ratioY;

                // now we can get the new height and width
                var newHeight = Convert.ToInt32(image.Height * ratio);
                var newWidth = Convert.ToInt32(image.Width * ratio);

                // Now calculate the X,Y position of the upper-left corner 
                // (one of these will always be zero)
                var posX = Convert.ToInt32((width - (image.Width * ratio)) / 2);
                var posY = Convert.ToInt32((height - (image.Height * ratio)) / 2);

                graphic.Clear(Color.White); // white padding
                graphic.DrawImage(image, posX, posY, newWidth, newHeight);

                /* ------------- end new code ---------------- */

                var encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);
                return thumbnail;
            }
        }

        public byte[] ImageToByteArray(Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }

        public Image ByteArrayToImage(byte[] byteArrayIn)
        {
            using (var ms = new MemoryStream(byteArrayIn))
            {
                var returnImage = Image.FromStream(ms);
                return returnImage;
            }
        }

        [AllowAnonymous]
        public IActionResult GetImage(string path, int width, int height, string extension = "png")
        {
            if (string.IsNullOrEmpty(path))
            {
                return Content("");
            }

            var rootPath = Path.Combine(HostingEnvironment.WebRootPath, path);
            var resizedImage = CropImage(rootPath, width, height);
            var resizedBytes = ImageToByteArray(resizedImage);
            // Convert byte[] to Base64 String
            // var base64String = Convert.ToBase64String(resizedBytes);
            //return Content("data:image;base64," + base64String);
            return File(resizedBytes, "image/" + extension);
        }

        public string SerializeObject<T>(T value) where T : class
        {
            return JsonConvert.SerializeObject(value);
        }

        public T DeserializeObject<T>(string value) where T : class
        {
            return value == null ? null : JsonConvert.DeserializeObject<T>(value);
        }

        public string Encrypt(string item)
        {
            return new SimpleStringCipher().Encrypt(item);
        }

        public string Decrypt(string item)
        {
            return new SimpleStringCipher().Decrypt(item);
        }

        public async Task GrayImageColor(string path, GrayColorType colorType = GrayColorType.Red)
        {
            await Task.Run(() =>
            {
                var filePath = Path.Combine(HostingEnvironment.WebRootPath, path);
                var bt = new Bitmap(filePath);

                for (var i = 0; i < bt.Width; i++)
                {
                    for (var j = 0; j < bt.Height; j++)
                    {
                        var c = bt.GetPixel(i, j);
                        int pixelR;
                        int pixelG;
                        int pixelB;
                        switch (colorType)
                        {
                            case GrayColorType.Red:
                                pixelR = c.R;
                                pixelG = c.G - 100;
                                pixelB = c.B - 100;
                                break;
                            case GrayColorType.Green:
                                pixelR = c.R - 50;
                                pixelG = c.G;
                                pixelB = c.B - 50;
                                break;
                            case GrayColorType.Blue:
                                pixelR = c.R - 50;
                                pixelG = c.G - 50;
                                pixelB = c.B;
                                break;
                            default:
                                pixelR = c.R;
                                pixelG = c.G - 100;
                                pixelB = c.B - 100;
                                break;
                        }

                        pixelR = Math.Max(pixelR, 0);
                        pixelR = Math.Min(255, pixelR);

                        pixelG = Math.Max(pixelG, 0);
                        pixelG = Math.Min(255, pixelG);

                        pixelB = Math.Max(pixelB, 0);
                        pixelB = Math.Min(255, pixelB);
                        bt.SetPixel(i, j, Color.FromArgb((byte) pixelR, (byte) pixelG, (byte) pixelB));
                    }
                }

                bt.Save(filePath);
            });
        }

        public async Task GrayImage(string path)
        {
            await Task.Run(() =>
            {
                var filePath = Path.Combine(HostingEnvironment.WebRootPath, path);
                var bt = new Bitmap(filePath);
                for (var y = 0; y < bt.Height; y++)
                {
                    for (var x = 0; x < bt.Width; x++)
                    {
                        var c = bt.GetPixel(x, y);

                        var r = c.R;
                        var g = c.G;
                        var b = c.B;
                        var a = c.A;
                        var avg = (r + g + b) / 3;
                        bt.SetPixel(x, y, Color.FromArgb(a, avg, avg, avg));
                    }
                }

                bt.Save(filePath);
            });
        }
    }
}