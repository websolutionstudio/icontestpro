﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using Abp.Dependency;
using IContestPro.CountryState;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Controllers
{
    public class CountriesController : BaseController
    {
        private readonly ICountryStateAppService _countryStateAppService;

        public CountriesController(ICountryStateAppService countryStateAppService, 
            IIocResolver iocResolver): base(iocResolver)
        {
            _countryStateAppService = countryStateAppService;
        }

        // GET
        public async Task<IActionResult> Index(int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;
            var countries = await this._countryStateAppService.GetAll(
                new PagedResultRequestDto
                {
                    SkipCount = offset, MaxResultCount = pageSize
                });
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            return View(countries);
        }

        public IActionResult States(string id)
        {
            var states = _countryStateAppService.GetStatesByCountryId(id);
            if (Request.IsAjaxRequest())
            {
                var itemsAjax = states.Items.Select(state => new { Id = state.Id, Name = state.Name }).ToList();
                return Json(new
                {
                    items = itemsAjax
                }); 
            }
            return View(states);
        }
       
    }
}