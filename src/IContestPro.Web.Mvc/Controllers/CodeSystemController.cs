﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Configuration;
using Microsoft.AspNetCore.Mvc;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Validation;
using Abp.UI;
using Abp.Web.Mvc.Alerts;
using IContestPro.Authorization;
using IContestPro.CodeSystem;
using IContestPro.CodeSystem.Dto;
using IContestPro.MemoryCache;
using IContestPro.Messages.Dto;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.Extensions.Caching.Memory;

namespace IContestPro.Web.Controllers
{
    [AbpAuthorize(PermissionNames.Pages_Admin, PermissionNames.Pages_Sponsor )]
    [DisableValidation]
    public class CodeSystemController : BaseContestController
    {
        private readonly ICodeSystemAppService _codeSystemAppService;
        private readonly ISettingManager _settingManager;
        private readonly IMemoryCacheAppService _memoryCacheAppService;
        
        public CodeSystemController(IIocResolver iocResolver, ICodeSystemAppService codeSystemAppService, ISettingManager settingManager, IMemoryCache memoryCache, IMemoryCacheAppService memoryCacheAppService):base(iocResolver)
        {
            _codeSystemAppService = codeSystemAppService;
            _settingManager = settingManager;
            _memoryCacheAppService = memoryCacheAppService;
        }
        
        [AbpAuthorize(PermissionNames.Pages_Admin, PermissionNames.Pages_Sponsor )]
        public async Task<IActionResult> Index(string batchNo = "", string status = "", int page = 1, int pageSize = 20)
        {
            status = status != null && status.ToLower().Equals("all") ? "" : status;
            ViewBag.BatchNo = batchNo; 
            ViewBag.Status = status;
            var offset = (page - 1) * pageSize;  
            var items = await _codeSystemAppService.GetAllWithFilter(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize},
                batchNo, status);
            var result = new AppPagedRequestResultDto<PagedResultDto<CodeSystemDto>>
            {
                Page = page,
                PageSize = pageSize,
                Result = items
            };
            return View(result);
        } 
        
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public async Task<IActionResult> Create()
        {
            ViewBag.BatchNoSize = await _settingManager.GetSettingValueAsync<int>(AppConsts.SettingsBatchNoSize);
            ViewBag.CodeSize = await SettingManager.GetSettingValueAsync<int>(AppConsts.SettingsCodeSize);
            ViewBag.SettingsMaximumBatchSize = await SettingManager.GetSettingValueAsync<int>(AppConsts.SettingsMaximumBatchSize);
            var batchNo = await _memoryCacheAppService.Get<string>(AppCacheKeys.BatchNoKey);
            if (string.IsNullOrEmpty(batchNo))
            {
                batchNo = await _codeSystemAppService.GenerateUniqueBatchNo();
                await _memoryCacheAppService.Set(AppCacheKeys.BatchNoKey, batchNo);
            }
            var model = new CreateCodeSystemWithCountDto
            {
                BatchNo = batchNo
            };
            return View(model); 
        } 
       
        
        [HttpPost]
        [UnitOfWork(false)]
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public async Task<IActionResult> Create(CreateCodeSystemWithCountDto model)
        {
           
            ViewBag.BatchNoSize = await _settingManager.GetSettingValueAsync<int>(AppConsts.SettingsBatchNoSize);
            ViewBag.CodeSize = await SettingManager.GetSettingValueAsync<int>(AppConsts.SettingsCodeSize);
            var settingsMaximumBatchSize = await SettingManager.GetSettingValueAsync<int>(AppConsts.SettingsMaximumBatchSize);
            ViewBag.SettingsMaximumBatchSize = settingsMaximumBatchSize;
            try{
                if (model.TotalToGenerate == 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Please enter total codes to be generated."));
                    return View(model); 
                }
                if (model.TotalToGenerate > settingsMaximumBatchSize)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"Total codes to generate must be less than or equal {settingsMaximumBatchSize}. This can be changed from the settings page."));
                    return View(model); 
                }

                  var item = await _codeSystemAppService.CreateCodes(model);
                if (item != null)
                {
                    await _memoryCacheAppService.Remove(AppCacheKeys.BatchNoKey);
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, $"{item.TotalCodesGenerated} codes generated successfully. Thank you."));
                    return RedirectToAction("Index", new { batchNo = model.BatchNo });  
                }
            
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while completing your request. Please try again later."));
                return View(model);   
            }
            catch (UserFriendlyException e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, e.Message));
                return View(model); 
            }
            catch (Exception e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while processing your request."));
                return View(model);
            }
        } 
        
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public async Task<IActionResult> Allocate()
        {
            ViewBag.NotAllocatedCodesCount = await _codeSystemAppService.CountByStatus(AppConsts.StatusNotAllocated);
            return View(new AllocateCodeDto());
        } 
        
        [HttpPost]
        [UnitOfWork(false)]
        [AbpAuthorize(PermissionNames.Pages_Admin)]
        public async Task<IActionResult> Allocate(AllocateCodeDto model)
        {
            try
            {
                ViewBag.NotAllocatedCodesCount = await _codeSystemAppService.CountByStatus(AppConsts.StatusNotAllocated);
                if (string.IsNullOrEmpty(model.Recipients))
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please select user to assign code to."));
                    return View(model); 
                }
                if (model.TotalCodesToAllocate <= 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Total codes to allocate must be greater than 0."));
                    return View(model); 
                }
                var item = await _codeSystemAppService.AllocateToUsers(model);
                if (item != null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, $"{item.TotalCodesGenerated} codes allocated successfully. Thank you."));
                    return RedirectToAction("Index");  
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while completing your request. Please try again later."));
                return View(model); 
            }
            catch (UserFriendlyException e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, e.Message));
                return View(model); 
            }
            catch (Exception e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while completing your request. Please try again later."));
                return View(model);
            }
        } 
        public async Task<IActionResult> Details(string id)
        {
            var item = await _codeSystemAppService.GetCodeDetailsByCode(id);
            return View(item); 
        } 
    }
}
