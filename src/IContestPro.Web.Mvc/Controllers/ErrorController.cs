﻿using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Abp.Dependency;
using Abp.Web.Models;
using Abp.Web.Mvc.Models;
using IContestPro.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;

namespace IContestPro.Web.Controllers
{
    public class ErrorController : BaseContestController
    {
        private readonly IErrorInfoBuilder _errorInfoBuilder;
        private readonly ILoggingAppService _loggingAppService;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ErrorController(IErrorInfoBuilder errorInfoBuilder, IIocResolver iocResolver, ILoggingAppService loggingAppService, IHostingEnvironment hostingEnvironment):base(iocResolver)
        {
            _errorInfoBuilder = errorInfoBuilder;
            _loggingAppService = loggingAppService;
            _hostingEnvironment = hostingEnvironment;
        }

        public ActionResult Index()
        {
            var exHandlerFeature = HttpContext.Features.Get<IExceptionHandlerFeature>();

            var exception = exHandlerFeature != null
                                ? exHandlerFeature.Error
                                : new Exception("Unhandled exception!");

            return View(
                "Error",
                new ErrorViewModel(
                    _errorInfoBuilder.BuildForException(exception),
                    exception
                )
            );
        } 
        public ActionResult ConfirmEmail()
        {
           return View();
        }
        public ActionResult Error500()
        {
            return View();
        }

        public ActionResult Error404()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult Log(int page=1,string fileName=null)
        {
            try
            {
                var files = Directory.GetFiles(_hostingEnvironment.ContentRootPath + AppConsts.AppExceptionLogFolder);
                //ViewBag.ExceptionFiles = files.Select(s => s.Split('\\').Last().Split('.').First());
                ViewBag.ExceptionFiles = files.Select(s => s.Split('\\').Last().Split('.').First());
                ViewBag.FileName = fileName;
                return View(_loggingAppService.ReadLogFile(page, 20, fileName));
            }
            catch (Exception)
            {
                return View(_loggingAppService.ReadLogFile(page));
            }
        
        }
        
        [Authorize]
        public ActionResult ClearLog()
        {
            _loggingAppService.ClearLog();
            return RedirectToAction("Log");
        }
        public ActionResult DeleteException(int lineIndex, int page=1, string fileName=null)
        {
            _loggingAppService.DeleteException(lineIndex, fileName);
            return RedirectToAction("Log",new { page , fileName });
        }

        [Authorize]
        public ActionResult DeleteLogFile(string fileName)
        {
            _loggingAppService.DeleteLogFile(fileName);
            return RedirectToAction("Log");
        }
        [AllowAnonymous]
        public ActionResult DisplayUrlError()
        {
            return View("Error404");
        }
        [AllowAnonymous]
        public ActionResult DisplayHttpReferrerError()
        {
            return View("Error404");
        }
    }
}
