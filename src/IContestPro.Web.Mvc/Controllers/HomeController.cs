﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Dependency;
using IContestPro.Authorization.Roles;
using IContestPro.Contest;
using IContestPro.Contest.Dto;
using IContestPro.FeaturedContest.Dto;
using IContestPro.Search;
using IContestPro.Search.Dto;
using Microsoft.AspNetCore.Authorization;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]   
    public class HomeController : BaseContestController
    {
        private readonly IContestAppService _contestAppService;
        private readonly ISearchAppService _searchAppService;
        public HomeController(IIocResolver iocResolver, IContestAppService contestAppService, ISearchAppService searchAppService) : base(iocResolver)
        {
            _contestAppService = contestAppService;
            _searchAppService = searchAppService;
        }
        [AllowAnonymous]
        public async Task<ActionResult> Index(string type = "all", string view = "grid", int page = 1, int pageSize = 20)
        {
            //string paid, string vp, string free, string coded, string everyoneWin, string highestVote
            ViewBag.ActiveHome = "active";
            ViewBag.Page = page;
            ViewBag.PageSize = pageSize;
            ViewBag.ViewType = view;
            ViewBag.Type = type;
            PagedResultDto<ContestWithFilterDto> items = null;
            if (string.IsNullOrEmpty(type) || type.Equals("all"))
            {
                items = await _contestAppService.GetContestsWithFilter(statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);

            }else{

            if (type.Equals("paid"))
                {
                    items = await _contestAppService.GetContestsWithFilter(filterByPaid: true, statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);
                } else if (type.Equals("vp"))
                {
                    items = await _contestAppService.GetContestsWithFilter(filterByVp: true, statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);  
                }else if (type.Equals("free"))
                {
                    items = await _contestAppService.GetContestsWithFilter(filterByFree: true, statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);  
                }else if (type.Equals("coded"))
                {
                    items = await _contestAppService.GetContestsWithFilter(filterByCoded: true, statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);  
                }else if (type.Equals("everyoneWin"))
                {
                    items = await _contestAppService.GetContestsWithFilter(filterByEveryoneWin: true, statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);  
                }else if (type.Equals("highestVote"))
                {
                    items = await _contestAppService.GetContestsWithFilter(filterByHighestVote: true, statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);  
                }
            }
            return View(items); 
        }
        public ActionResult Dashboard()
        {
            if (User.IsInRole(StaticRoleNames.Tenants.Admin) || User.IsInRole(StaticRoleNames.Tenants.AdminLevel2))
            {
                return View(); 
            }
             return RedirectToAction("Index","Manage");
        }
       
        [AllowAnonymous]
        public async Task<IActionResult> Search(string searchType, string searchText)
        {
            if (string.IsNullOrEmpty(searchType) || string.IsNullOrEmpty(searchText))
            {
                return View("_SearchFields", new List<SearchDto>());
            }

            ViewBag.SearchType = searchType;
            ViewBag.SearchText = searchText;
            var items = await _searchAppService.SearchForHomePages(searchType, searchText);
            return View("_SearchFields", items);
            
        }
       
        

        
    }
}
