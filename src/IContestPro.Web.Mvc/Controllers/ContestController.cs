﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.AspNetCore.Mvc.Extensions;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Alerts;
using IContestPro.Authorization;
using IContestPro.Authorization.Roles;
using IContestPro.Contest;
using IContestPro.Contest.ContestCategory;
using IContestPro.Contest.ContestEntryType;
using IContestPro.Contest.ContestType;
using IContestPro.Contest.Dto;
using IContestPro.Cookie;
using IContestPro.VoteType;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using IContestPro.AssetUpload;
using IContestPro.AssetUpload.Dto;
using IContestPro.Contest.ContestView;
using IContestPro.Contest.ContestView.Dto;
using IContestPro.FeaturedContest;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Contest})]
    public class ContestController : BaseContestController
    {
        private readonly IContestAppService _contestAppService;
        private readonly IContestTypeAppService _contestTypeAppService;
        private readonly IContestCategoryAppService _contestCategoryAppService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IContestEntryTypeAppService _contestEntryTypeAppService;
        private readonly IVoteTypeAppService _voteTypeAppService;
        private readonly IAssetUploadAppService _assetUploadAppService;
        private readonly IFeaturedContestAppService _featuredContestAppService;
        private readonly IContestViewAppService _contestViewAppService;
        public ContestController( IContestAppService contestAppService,
                                     IContestTypeAppService contestTypeAppService,
                                      IContestCategoryAppService contestCategoryAppService, 
                                    IContestEntryTypeAppService contestEntryTypeAppService, 
                                    IHostingEnvironment hostingEnvironment, 
                                        IVoteTypeAppService voteTypeAppService, IIocResolver iocResolver, 
                                    IAssetUploadAppService assetUploadAppService, IFeaturedContestAppService featuredContestAppService, IContestViewAppService contestViewAppService) : base(iocResolver)
        {
           _contestTypeAppService = contestTypeAppService;
            _contestCategoryAppService = contestCategoryAppService;
            _contestEntryTypeAppService = contestEntryTypeAppService;
            _hostingEnvironment = hostingEnvironment;
            _contestAppService = contestAppService;
            _voteTypeAppService = voteTypeAppService;
            _assetUploadAppService = assetUploadAppService;
            _featuredContestAppService = featuredContestAppService;
            _contestViewAppService = contestViewAppService;
        }
        
      
        
        // GET
        [AllowAnonymous]
        public async Task<ActionResult> Index(string type = "all", string view = "grid", int page = 1, int pageSize = 20)
        {
            //string paid, string vp, string free, string coded, string everyoneWin, string highestVote
            ViewBag.ActiveContest = "active";
            ViewBag.Page = page;
            ViewBag.PageSize = pageSize;
            ViewBag.ViewType = view;
            ViewBag.Type = type;
            PagedResultDto<ContestWithFilterDto> items = null;
            if (string.IsNullOrEmpty(type) || type.Equals("all"))
            {
                items = await _contestAppService.GetContestsWithFilter(statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);

            }else{

            if (type.Equals("paid"))
                {
                    items = await _contestAppService.GetContestsWithFilter(filterByPaid: true, statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);
                } else if (type.Equals("vp"))
                {
                    items = await _contestAppService.GetContestsWithFilter(filterByVp: true, statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);  
                }else if (type.Equals("free"))
                {
                    items = await _contestAppService.GetContestsWithFilter(filterByFree: true, statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);  
                }else if (type.Equals("coded"))
                {
                    items = await _contestAppService.GetContestsWithFilter(filterByCoded: true, statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);  
                }else if (type.Equals("everyoneWin"))
                {
                    items = await _contestAppService.GetContestsWithFilter(filterByEveryoneWin: true, statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);  
                }else if (type.Equals("highestVote"))
                {
                    items = await _contestAppService.GetContestsWithFilter(filterByHighestVote: true, statusName: AppConsts.StatusLive, page: page, pageSize: pageSize);  
                }
            }
            return View(items); 
        } 
        [AllowAnonymous]
        public async Task<ActionResult> Tag(string id, string view = "grid", int page = 1, int pageSize = 20)
        {
            //string paid, string vp, string free, string coded, string everyoneWin, string highestVote
            ViewBag.ActiveContest = "active";
            ViewBag.Page = page;
            ViewBag.PageSize = pageSize;
            ViewBag.ViewType = view;
            ViewBag.Tag = id;
            PagedResultDto<ContestWithFilterDto> items = null;
            if (!string.IsNullOrEmpty(id))
            {
                items = await _contestAppService.SearchContestsByTag(HttpUtility.UrlDecode(id), page: page, pageSize: pageSize);
            }else{
                return View("NotFound"); 
            }
            return View(items); 
        }
       
        [AllowAnonymous]
        public async Task<IActionResult> ContestWithDetails(int page = 1, int pageSize = 20)
        {
               ViewBag.ActiveHome = "active";
                var offset = (page - 1) * pageSize;  
                ViewBag.PageSize = pageSize;
                ViewBag.Page = page;
                var items = await _contestAppService.GetAllByStatusWithDetails(AppConsts.StatusLive, new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View("IndexWithDetails", items); 
        } 
        
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public async Task<IActionResult> Contests(int page = 1, int pageSize = 20)
        {
                var offset = (page - 1) * pageSize;  
                ViewBag.PageSize = pageSize;
                ViewBag.Page = page;
                var items = await _contestAppService.GetAllWithDetails(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items); 
        }
       
        
        /// <summary>
        /// List of contests posted by the current user logged in.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<IActionResult> Personal(int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _contestAppService.GetAllWithDetailsByUserId(AbpSession.GetUserId(), new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items);
        }
        /// <summary>
        /// Details page for the admin area
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public  async Task<IActionResult> Details(string id)
        {
            var item = await _contestAppService.GetByPermalinkWithDetails(id);
            if (item == null)
            {
                return View("NotFoundAdmin");
            }
            return View(item);
        }
        /// <summary>
        /// Details page for public area
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public  async Task<IActionResult> MainDetails(string id)
        {
            var item = await _contestAppService.GetByPermalinkWithDetails(id);
            if (item == null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "The contest you are looking for was not found."));
                return View("NotFound");
            }
            return View(item);
        } 
        
        [AllowAnonymous]
        public  async Task<IActionResult> EntryDetails(string id)
        {
            var item = await _contestAppService.GetByPermalink(id);
            if (item == null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "The contest you are looking for was not found."));
                return RedirectToAction("Personal");
            }
            return View(item);
        }
       
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Contest_Create})]
        public async Task<IActionResult> Create(int? id)
        {
            if (!User.IsInRole(StaticRoleNames.Tenants.Sponsors))
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Only sponsor accounts can create contest. Please upgrade your account to sponsor to be able to create a contest. Click on the sponsor link on the nav bar for more details.", icon:"fa-info"));
                return RedirectToAction("Index", "Manage");  
            }
            ViewBag.ContestType = new SelectList((await _contestTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0,MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id", "Name" );
            ViewBag.ContestEntryType = new SelectList((await _contestEntryTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id","Name");
            ViewBag.ContestCategory = new SelectList((await _contestCategoryAppService.GetAll(new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id","Name");
            ViewBag.VoteTypes = new SelectList((await _voteTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0,MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id","Name");
            if (id == null)
            {
                return View(new CreateContestDto());
            }
            var contestDto = await _contestAppService.Get(new EntityDto{Id = id.Value});
            var model = contestDto.MapTo<CreateContestDto>();
            return View(model);
        }
        
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Contest_Create})]
        public  async Task<IActionResult> Create(CreateContestDto model, IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Please upload a file."));
                return View(model); 
            }
            if (file.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Please select only image file."));
                return View(model);
                
            }
            var uploaded = await _assetUploadAppService.UploadImage(new UploadDto{File = file, Path = "images/uploads"});
            model.Picture = uploaded.FileName;
            var savedItem = await _contestAppService.Create(model);
            if (savedItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest created successfully."));
                return RedirectToAction("Details", new {id = savedItem.Permalink});
            }
            ViewBag.ContestType = new SelectList((await _contestTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0,MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id", "Name" );
            ViewBag.ContestEntryType = new SelectList((await _contestEntryTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id","Name");
            ViewBag.ContestCategory = new SelectList((await _contestCategoryAppService.GetAll(new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id","Name");
            ViewBag.VoteTypes = new SelectList((await _voteTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0,MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id","Name");
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating contest."));
            return View(model);
        }

        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Contest_Update})]
        public  async Task<IActionResult> Edit(int id)
        {
            var itemGotten = await _contestAppService.Get(new EntityDto{Id = id});
            if (itemGotten == null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "The contest you are looking for was not found."));
                return RedirectToAction("Personal");
            }
            ViewBag.VoteTypes = new SelectList((await _voteTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0,MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id","Name");
            ViewBag.ContestType = new SelectList((await _contestTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0,MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id", "Name", itemGotten.ContestTypeId );
            ViewBag.ContestEntryType = new SelectList((await _contestEntryTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id","Name", itemGotten.ContestEntryTypeId);
            ViewBag.ContestCategory = new SelectList((await _contestCategoryAppService.GetAll(new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id","Name", itemGotten.ContestCategoryId);
            return View(itemGotten.MapTo<EditContestDto>());
        }
        
        [HttpPost]
        [UnitOfWork(false)]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Contest_Update})]
        public  async Task<IActionResult> Edit(EditContestDto model, IFormFile file)
        {
            if (file != null)
            {
                if (file.Length == 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Please upload a file."));
                    return View(model); 
                }

                if (file.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Please select only image file."));
                    return View(model);
                
                }
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploads");
                var filePath = Path.Combine(uploads, model.Picture);
                await file.CopyToAsync(new FileStream(filePath, FileMode.Create));
            }
           
            var savedItem = await _contestAppService.Update(model.MapTo<ContestDto>());
            if (savedItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest edited successfully."));
                return RedirectToAction("Details", new {id = savedItem.Permalink});
            }
            ViewBag.VoteTypes = new SelectList((await _voteTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0,MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id","Name");
            ViewBag.ContestType = new SelectList((await _contestTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0,MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id", "Name", model.ContestTypeId );
            ViewBag.ContestEntryType = new SelectList((await _contestEntryTypeAppService.GetAll(new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id","Name", model.ContestEntryTypeId);
            ViewBag.ContestCategory = new SelectList((await _contestCategoryAppService.GetAll(new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100})).Items.Select(item => new {item.Id, item.Name}).ToList(), "Id","Name", model.ContestCategoryId);
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating contest."));
            return View(model);
        }
        
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> ChangeStatus(UpdateStatusDto model)
        {
            var savedItem = await _contestAppService.UpdateStatusAsync(model);
            if (savedItem != null && savedItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest updated successfully."));
                return RedirectToAction("Details", new {id = savedItem.Permalink});
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while updating contest."));
            return RedirectToAction("Personal");
        }

        [AllowAnonymous]
        public async Task<ActionResult> GetRandomFeaturedContests()
        {
            var items = await _featuredContestAppService.GetRandomLive();
            return View("_FeaturedRandomContestLive", items);
        }
         [AllowAnonymous]
        public async Task<ActionResult> GetContestsEndingSoon()
        {
            var items = await _contestAppService.GetContestsEndingSoon();
            return View("_GetContestsEndingSoon", items);
        }
         
        [AllowAnonymous]
        public async Task<ActionResult> PastWinners(int page = 1, int pageSize = 20)
        {
            ViewBag.ActivePastWinners = "active";
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _contestAppService.PastWinners( page, pageSize);
            return View(items);
        }
        
        [HttpPost]
        [AllowAnonymous]
        public  async Task<IActionResult> IncrementView(CreateContestViewDto model)
        {
            model.UserId = AbpSession.UserId;
            var item = await _contestViewAppService.Create(model);
            if (item.Id > 0)
            {
                return Json(new {success = true, message = "View submitted successfully."});  
            }
            return Json(new {success = false, data = "Error occurred while posting your view."});

        }
        
        /// <summary>
        /// Get the list of voters using the contest permalink
        /// </summary>
        /// <param name="id"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns>List of <see cref="ViewsDto"/> </returns>
        [AllowAnonymous]
        public  async Task<IActionResult> Views(string id, int page = 1, int pageSize = 20)
        {
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            ViewBag.Permalink = id;
            var items = await _contestAppService.GetViewsByPermalink(id, page, pageSize);
            if (items == null)
            {
                return View("NotFound");
            }
            return View(items);
        }
        
    }
}