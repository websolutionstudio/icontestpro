﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.Runtime.Validation;
using Abp.UI;
using Abp.Web.Mvc.Alerts;
using IContestPro.Contest;
using Microsoft.AspNetCore.Mvc;
using IContestPro.Messages;
using IContestPro.Messages.Dto;
using IContestPro.Web.Helpers;
using IContestPro.Web.Helpers.AlertExtension;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]
    [DisableValidation]
    public class MessagesController : BaseController
    {
        private readonly IMessagesAppService _messagesAppService;
        private readonly IContestAppService _contestAppService;
        public MessagesController( IIocResolver iocResolver, IMessagesAppService messagesAppService, IContestAppService contestAppService) : base(iocResolver)
        {
            _messagesAppService = messagesAppService;
            _contestAppService = contestAppService;
        }
        
        // GET
        public async Task<IActionResult> Index(string status = "", string fetchType = MessageFetchType.Inbox, int page = 1, int pageSize = 20)
        {
            
            var offset = (page - 1) * pageSize;  
            IReadOnlyList<UserMessageDto> items;
            const int type = MessageType.Message; 
            ViewBag.Type = type;
            ViewBag.Status = status ?? "";
            if (fetchType == MessageFetchType.Inbox)
            {
                ViewBag.InboxActive = "active";
            }else if (fetchType == MessageFetchType.Sent)
            {
                ViewBag.SentActive = "active";
            }
                 items = await _messagesAppService.GetAllByByUser(AbpSession.GetUserId(), new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize}, type, status, fetchType);
           var result = new AppPagedRequestResultDto<IReadOnlyList<UserMessageDto>>
           {
               Page = page,
               PageSize = pageSize,
               Result = items
           };
            return View(result); 
        } 
       
        public async Task<IActionResult> Create()
        {
            return View(new CreateMessageInputDto()); 
        } 
        
        [HttpPost]
        [UnitOfWork(false)]
        public async Task<IActionResult> Create(CreateMessageInputDto model)
        {
           try{
            if ( model.Attachments != null)
            {
                foreach (var file in model.Attachments) {
                    if (file.ContentType != "image/jpeg" && file.ContentType != "image/gif" && file.ContentType != "image/png") {
                        AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Only jgp, gif and png files are allowed."));
                        return View(model);
                    }
                }
            }
            var item = await _messagesAppService.CreateMessage(model);
            if (item != null && item.Results.Count > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Your message was sent successfully. Thank you."));
                return RedirectToAction("Index", new { type = model.MessageType });  
            }
            
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating your message. Please try again later."));
                return View(model);   
           }
           catch (UserFriendlyException e)
           {
               AddAlertMessage(new AlertMessageCustom(AlertType.Danger, e.Message));
               return View(model); 
           }
           catch (Exception e)
           {
               AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while processing your request."));
               return View(model);
           }
        } 
        [HttpPost]
        [UnitOfWork(false)]
        public async Task<IActionResult> CloseTicket(int messageId)
        {
           
           try{
            var item = await _messagesAppService.CloseSupportTicket(messageId);
            if (item != null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Support ticket closed successfully. Thank you."));
                return RedirectToAction("Details", new { id = messageId});
            }
            
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while closing your ticket. Please try again later."));
            return RedirectToAction("Details", new { id = messageId});
           }
           catch (UserFriendlyException e)
           {
               AddAlertMessage(new AlertMessageCustom(AlertType.Danger, e.Message));
               return RedirectToAction("Details", new { id = messageId});
           }
           catch (Exception e)
           {
               AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
               return RedirectToAction("Details", new { id = messageId});
           }
        } 
        
        public async Task<IActionResult> Details(int id, int type)
        {
            TempData.Put("ResponseModel", new CreateMessageResponseDto());

            var item = await _messagesAppService.GetMessageDetailsWithResponses(id, type);
            return View(item); 
        } 
        
        [HttpPost]
        [UnitOfWork(false)]
        public async Task<IActionResult> Delete(int id)
        {
            await _messagesAppService.Delete(new EntityDto{Id = id});
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Message deleted successfully. Thank you."));
            return RedirectToAction("Index");
        } 
        [HttpPost]
        [UnitOfWork(false)]
       public async Task<IActionResult> NewMessageResponse(CreateMessageResponseDto model)
        {
            try{
           TempData.Put("ResponseModel",model);;
            if (model.Attachments != null)
            {
                foreach (var file in model.Attachments) {
                    if (file.ContentType != "image/jpeg" && file.ContentType != "image/gif" && file.ContentType != "image/png") {
                        AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Only jgp, gif and png files are allowed."));
                        return RedirectToAction("Details", new { id = model.MessageId});
                    }
                }
            }
            var item = await _messagesAppService.CreateMessageResponse(model);
            if (item != null)
            {
                TempData.Remove("ResponseModel");
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Your message response was sent successfully. Thank you."));
                return RedirectToAction("Details", new { id = model.MessageId });  
            }
            
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating your message. Please try again later."));
            return RedirectToAction("Details", new { id = model.MessageId});
            }
            catch (UserFriendlyException e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, e.Message));
                return RedirectToAction("Details", new { id = model.MessageId});
            }
            catch (Exception e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
                return RedirectToAction("Details", new { id = model.MessageId});
            }

        } 
        public async Task<IActionResult> GetCurrentUserContests()
        {
            var items = await _contestAppService.GetAllByUserId(AbpSession.GetUserId(), new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            return View("_CurrentUserContests", items);
        }
    }
}