﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Dependency;
using IContestPro.Authorization;
using Microsoft.AspNetCore.Mvc;
using IContestPro.VoterLeaderBoard;
using IContestPro.VoterLeaderBoard.Dto;

namespace IContestPro.Web.Controllers
{
    
    public class VoterLeaderBoardController : BaseController
    {
        private readonly IVoterLeaderBoardAppService _voterLeaderBoardAppService;
        public VoterLeaderBoardController( IIocResolver iocResolver, IVoterLeaderBoardAppService voterLeaderBoardAppService) : base(iocResolver)
        {
            _voterLeaderBoardAppService = voterLeaderBoardAppService;
        }
        
        // GET
        public async Task<IActionResult> Index(int page = 1, int pageSize = 10)
        {
            
            ViewBag.ActiveMore = "active";
            ViewBag.ActiveMoreVoterLeaderBoard = "active";
                var offset = (page - 1) * pageSize;  
                ViewBag.PageSize = pageSize;
                ViewBag.Page = page;
                var items = await _voterLeaderBoardAppService.GetTopContestsWithHighestVotesAndTopVoters(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
           var result = new AppPagedRequestResultDto<PagedResultDto<TopContestsWithHighestVotesAndTopVotersDto>>
           {
               Page = page,
               PageSize = pageSize,
               Result = items
           };
            return View(result); 
        } 
       
        [AbpAuthorize(permissions: PermissionNames.Pages_Admin)]
           public async Task<IActionResult> AdminIndex(int page = 1, int pageSize = 10)
        {
            
            ViewBag.ActiveMore = "active";
            ViewBag.ActiveMoreVoterLeaderBoard = "active";
                var offset = (page - 1) * pageSize;  
                ViewBag.PageSize = pageSize;
                ViewBag.Page = page;
                var items = await _voterLeaderBoardAppService.GetTopContestsWithHighestVotesAndTopVoters(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
           var result = new AppPagedRequestResultDto<PagedResultDto<TopContestsWithHighestVotesAndTopVotersDto>>
           {
               Page = page,
               PageSize = pageSize,
               Result = items
           };
            return View(result); 
        } 
       
         
       
        

        
    }
}