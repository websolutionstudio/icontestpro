﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Alerts;
using Hangfire;
using IContestPro.BackgroundJobs;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.Contest;
using IContestPro.Contest.ContestEntry;
using IContestPro.Contest.Dto;
using IContestPro.Helpers;
using IContestPro.HttpRequest;
using IContestPro.Payment.Dto;
using IContestPro.PaymentHistories;
using IContestPro.PaymentHistories.Dto;
using IContestPro.Referral;
using IContestPro.Status;
using IContestPro.Wallet;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Mvc;
using SendGrid.Helpers.Mail;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]
    public class PaymentController : BaseController
    {
        private readonly IContestAppService _contestAppService;
        private readonly IContestEntryAppService _contestEntryAppService;
        private readonly IPaymentHistoriesAppService _paymentHistoriesAppService;
        private readonly IStatusAppService _statusAppService;
        private readonly IWalletAppService _walletAppService;
        private readonly IReferralAppService _referralAppService;

         public PaymentController(IIocResolver iocResolver, IContestAppService contestAppService, 
             IContestEntryAppService contestEntryAppService, IPaymentHistoriesAppService paymentHistoriesAppService,
             IStatusAppService statusAppService, IWalletAppService walletAppService, IReferralAppService referralAppService): base(iocResolver)
         {
             _contestAppService = contestAppService;
             _contestEntryAppService = contestEntryAppService;
             _paymentHistoriesAppService = paymentHistoriesAppService;
             _statusAppService = statusAppService;
             _walletAppService = walletAppService;
             _referralAppService = referralAppService;
         }
        
       

        
        // GET
        [HttpPost]
        [AbpMvcAuthorize]
        public async Task<IActionResult> FundWallet(FundWalletDto model)
        {
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusPendingPaymentConfirmation);
            model.Reference = Guid.NewGuid().ToString("N");

            await _paymentHistoriesAppService.Create(new CreatePaymentHistoryDto
            {
                Reference = model.Reference,
                Amount = model.Amount,
                Description = "A Wallet topup was initiated. Your wallet will be credited after confirmation.",
                Title = "Wallet topup",
                UserId = AbpSession.GetUserId(),
                WalletOperation = WalletOperation.Credit,
                StatusId = status.Id
                
            });
           
            
            ViewBag.User = CurrentUser;
            return View(model);
        }
        
        [AbpMvcAuthorize]
        public async Task<IActionResult> FundWalletResponse(string reference)
        {
            var history = await _paymentHistoriesAppService.GetByReference(reference);
            ViewBag.Reference = reference;
            if (history == null)
            {
                ViewBag.WalletError = true;
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Transaction not found."));
                return View();
            }
            var header = new Dictionary<string, string>();
            header.Add("Authorization", $"Bearer {AppConsts.PaystackApiSecretKey}");
            var httpRequest = HttpRequestAppService.Get<TransactionResponseDto>(
                $"{AppConsts.PaystackApiBase}transaction/verify/{reference}", header);
            if (httpRequest.Success && history.Status.Name != AppConsts.StatusSuccess)
            {
                var wallet = await _walletAppService.Credit(AbpSession.GetUserId(), (httpRequest.Result.Data.Amount / 100));
                if (wallet != null)
                {
                    var status = await _statusAppService.GetByNameAsync(AppConsts.StatusSuccess);
                    history.StatusId = status.Id;
                    await _paymentHistoriesAppService.Update(history);
                    
                    await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                    {
                        UserId = AbpSession.GetUserId(),
                        Message =  $"A Wallet topup of {history.Amount?.ToNairaString()} was successfully completed. Your wallet has been credited accordingly.",
                    });
                    BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                        (
                   
                            new SendEmailDto
                            {
                                Tos = new List<EmailAddress>{new EmailAddress(CurrentUser.EmailAddress, CurrentUser.NickName)},
                                From = new EmailAddress(AppConsts.IContestProEmail,AppConsts.IContestProDisplayName),
                                Subject ="Wallet topup",
                                Body = $"A Wallet topup of {history.Amount.Value.ToNairaString()} was successfully completed. Your wallet has been credited accordingly.",

                            })
                    );
                }
                
            }
            
            return View(httpRequest);
        }

        [UnitOfWork(isTransactional:false)]
        public async Task<IActionResult> ContestPaymentSuccess(int token, string reference)
        {
            try
            {
 //TODO: Make api call with reference to verify payment and get amount
            ViewBag.Reference = reference;
            var header = new Dictionary<string, string>();
            header.Add("Authorization", $"Bearer {AppConsts.PaystackApiSecretKey}");
            var httpRequest = HttpRequestAppService.Get<TransactionResponseDto>(
                $"{AppConsts.PaystackApiBase}transaction/verify/{reference}", header);
            if (httpRequest.Success)
            {
                var status = await _statusAppService.GetByNameAsync(AppConsts.StatusSuccess);
                var history = await _paymentHistoriesAppService.GetByReference(reference);
                if (history != null && history.StatusId == status.Id)
                {
                    AlertManagerExtension.Alerts.Add(new AlertMessageCustom(AlertType.Success, "Payment verified successfully."));
                    return RedirectToAction("Index", "Manage");
                }
                var amount = httpRequest.Result.Data.Amount / 100;
                var adminUser = await UserAppService.GetTenantAdmin();
                var contest =  await _contestAppService.Get(new EntityDto{Id = token});
                await _walletAppService.Credit(adminUser.Id, amount);
                var userId = CurrentUser.Id;
                await _paymentHistoriesAppService.Create(new CreatePaymentHistoryDto
                {
                    Reference = reference,
                    Amount = amount,
                    Description = $"Payment for your contest '{contest.Title}' was completed successfully.",
                    Title = $"Payment for '{contest.Title}' contest",
                    UserId = userId,
                    WalletOperation = WalletOperation.NoEffect,
                    StatusId = status.Id

                });
                  await _paymentHistoriesAppService.Create(new CreatePaymentHistoryDto
                {
                    Reference = Guid.NewGuid().ToString(),
                    Amount = amount,
                    Description = $"A payment of {amount.ToNairaString()} was made for '{contest.Title}' contest by '{CurrentUser.NickName}'.",
                    Title = $"Payment for '{contest.Title}' contest",
                    UserId = adminUser.Id,
                    WalletOperation = WalletOperation.Credit,
                    StatusId = status.Id

                });
                  
                
                await _contestAppService.UpdateStatusAsync(new UpdateStatusDto
                {
                    EntityId = token,
                    StatusName = AppConsts.StatusPaymentConfirmed
                });
               
                await Task.Run(async () =>
                {
                    if (await _referralAppService.isUserReferred(AbpSession.GetUserId()))
                    {
                        var referrer =
                            await _referralAppService.CreditUserReferrerSponsorAmount(AbpSession.GetUserId(), amount);
                    }
                }); 
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = adminUser.Id,
                    Message = $"A payment of {amount.ToNairaString()} was made for '{contest.Title}' contest by '{CurrentUser.NickName}'.",
                });
                await AppNotifier.SendMessageAsync(new BgJobNotifyUserDto
                {
                    UserId = AbpSession.GetUserId(),
                    Message =  $"Payment of {amount.ToNairaString()} for your contest was completed successfully.",
                });
               
                BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                    (
                   
                        new SendEmailDto
                        {
                            Tos = new List<EmailAddress>{new EmailAddress(CurrentUser.EmailAddress, CurrentUser.NickName)},
                            From = new EmailAddress(AppConsts.IContestProEmail,AppConsts.IContestProDisplayName),
                            Subject = "Contest payment",
                            Body = $"Payment of {amount.ToNairaString()} for your contest was completed successfully.",

                        })
                );
            }
            else
            {
                await _contestAppService.UpdateStatusAsync(new UpdateStatusDto
                {
                    EntityId = token,
                    StatusName = AppConsts.StatusPendingPaymentConfirmation
                });
            }
                return View();

            }
            catch (Exception e)
            {
               LoggingAppService.LogException(e);
                
            }
           

            return View("PaymentVerificationFailure");
        }

        public async Task<IActionResult> EntrySuccess(int token, string reference)
        {
            ViewBag.Reference = reference;
            var randomInt = new Random();
            await _contestEntryAppService.UpdateStatusAsync(new UpdateStatusDto{EntityId = token, StatusName = AppConsts.StatusPendingPaymentConfirmation});
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusSuccess);

            
            await _paymentHistoriesAppService.Create(new CreatePaymentHistoryDto
            {
                Reference = reference,
                Amount = randomInt.Next(1000, 50000),
                Description = "Payment for your entry was completed successfully.",
                Title = "Contest entry payment",
                UserId = AbpSession.GetUserId(),
                WalletOperation = WalletOperation.NoEffect,
                StatusId =  status.Id
                
            });
            /*_appNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                UserId = AbpSession.GetUserId(),
                Message =  $"Payment of {amount.ToNairaString()} for your contest was completed successfully.",
            });
            var user = await GetCurrentUser();

            BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                (
                   
                    new SendEmailDto
                    {
                        Tos = new List<EmailAddress>{new EmailAddress(user.EmailAddress, user.NickName)},
                        From = new EmailAddress(AppConsts.IContestProEmail,AppConsts.IContestProDisplayName),
                        Subject = "Contest payment",
                        Body = $"Payment of {amount.ToNairaString()} for your contest was completed successfully.",

                    })
            );*/
            return View();
        }
        public async Task<IActionResult> Failure(int token, string reference)
        {
            ViewBag.Reference = reference;
            var status = await _statusAppService.GetByNameAsync(AppConsts.StatusPaymentFailed);
            await _paymentHistoriesAppService.Create(new CreatePaymentHistoryDto
            {
                WalletOperation = WalletOperation.NoEffect,
                StatusId =  status.Id,
                Reference = reference,
                UserId = AbpSession.GetUserId(),
                Description = "Payment for your contest was completed with errors.",
                Title = "Contest payment"
            });
            /*_appNotifier.SendMessageAsync(new BgJobNotifyUserDto
            {
                UserId = AbpSession.GetUserId(),
                Message =  $"Payment of {amount.ToNairaString()} for your contest was completed successfully.",
            });
            var user = await GetCurrentUser();

            BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                (
                   
                    new SendEmailDto
                    {
                        Tos = new List<EmailAddress>{new EmailAddress(user.EmailAddress, user.NickName)},
                        From = new EmailAddress(AppConsts.IContestProEmail,AppConsts.IContestProDisplayName),
                        Subject = "Contest payment",
                        Body = $"Payment of {amount.ToNairaString()} for your contest was completed successfully.",

                    })
            );*/
            return View();
        }
        

        
        
    }
}