﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Extensions;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.MultiTenancy;
using Abp.Notifications;
using Abp.Threading;
using Abp.UI;
using Abp.Web.Models;
using Abp.Zero.Configuration;
using Hangfire;
using IContestPro.Authorization;
using IContestPro.Authorization.Roles;
using IContestPro.Authorization.Users;
using IContestPro.BackgroundJobs;
using IContestPro.BackgroundJobs.Dto;
using IContestPro.CountryState;
using IContestPro.EmailTemplate;
using IContestPro.Identity;
using IContestPro.MultiTenancy;
using IContestPro.Sessions;
using IContestPro.Users.Dto;
using IContestPro.Web.Helpers.Claims;
using IContestPro.Web.Models.Account;
using IContestPro.Web.Views.Shared.Components.TenantChange;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SendGrid.Helpers.Mail;

namespace IContestPro.Web.Controllers
{
    public class AccountController : BaseController
    {
        private readonly UserManager _userManager;
        private readonly TenantManager _tenantManager;
        private readonly IMultiTenancyConfig _multiTenancyConfig;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly AbpLoginResultTypeHelper _abpLoginResultTypeHelper;
        private readonly LogInManager _logInManager;
        private readonly SignInManager _signInManager;
        private readonly UserRegistrationManager _userRegistrationManager;
        private readonly ISessionAppService _sessionAppService;
        private readonly ITenantCache _tenantCache;
        private readonly ICountryStateAppService _countryStateAppService;
        private readonly IEmailTemplateAppService _emailTemplateAppService;

        public AccountController(
            UserManager userManager,
            IMultiTenancyConfig multiTenancyConfig,
            TenantManager tenantManager,
            IUnitOfWorkManager unitOfWorkManager,
            AbpLoginResultTypeHelper abpLoginResultTypeHelper,
            LogInManager logInManager,
            SignInManager signInManager,
            UserRegistrationManager userRegistrationManager,
            ISessionAppService sessionAppService,
            ITenantCache tenantCache,
            INotificationPublisher notificationPublisher,
            ICountryStateAppService countryStateAppService, 
            RoleManager roleManager,
            IIocResolver iocResolver, 
            IEmailTemplateAppService emailTemplateAppService
            ): base(iocResolver)
        {
            _userManager = userManager;
            _multiTenancyConfig = multiTenancyConfig;
            _tenantManager = tenantManager;
            _unitOfWorkManager = unitOfWorkManager;
            _abpLoginResultTypeHelper = abpLoginResultTypeHelper;
            _logInManager = logInManager;
            _signInManager = signInManager;
            _userRegistrationManager = userRegistrationManager;
            _sessionAppService = sessionAppService;
            _tenantCache = tenantCache;
            _countryStateAppService = countryStateAppService;
            _emailTemplateAppService = emailTemplateAppService;
        }

        #region Login / Logout

        public ActionResult Login(string userNameOrEmailAddress = "", string returnUrl = "", string successMessage = "")
        {
            if (AbpSession.UserId != null)
            {
                return RedirectToAction("Index", "Manage");
            }
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                returnUrl = GetAppHomeUrl();
            }

            return View(new LoginFormViewModel
            {
                ReturnUrl = returnUrl,
                IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled,
                IsSelfRegistrationAllowed = IsSelfRegistrationEnabled(),
                MultiTenancySide = AbpSession.MultiTenancySide
            });
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<IActionResult> Login(LoginViewModel loginModel, string returnUrl = "", string returnUrlHash = "")
        {
            try
            {
            returnUrl = NormalizeReturnUrl(returnUrl);
            if (!string.IsNullOrWhiteSpace(returnUrlHash))
            {
                returnUrl = returnUrl + returnUrlHash;
            }

            var loginResult = await _logInManager.LoginAsync(loginModel.UsernameOrEmailAddress, loginModel.Password, GetTenancyNameOrNull());

            switch (loginResult.Result)
            {
                    
                case AbpLoginResultType.Success:
                    await _signInManager.SignInAsync(loginResult.Identity, loginModel.RememberMe);
                    //ADD CLAIM HERE!!!!
                    if ((await _userManager.GetClaimsAsync(loginResult.User)).Count == 0)
                    {
                        if (loginResult.User.UserName == AbpUserBase.AdminUserName)
                        {
                            await _userManager.AddClaimAsync(loginResult.User,
                                new Claim(CustomClaimTypes.ClaimTypeNickname, loginResult.User.Name));
                            await _userManager.AddClaimAsync(loginResult.User,
                                new Claim(CustomClaimTypes.ClaimTypeImage, loginResult.User.Name));

                        }
                        else
                        {
                            await _userManager.AddClaimAsync(loginResult.User,
                                new Claim(CustomClaimTypes.ClaimTypeNickname, loginResult.User.NickName));
                            if (loginResult.User.Image != null)
                            {
                                await _userManager.AddClaimAsync(loginResult.User,
                                    new Claim(CustomClaimTypes.ClaimTypeImage, loginResult.User.Image));
                            }
                           
                        }
                        await UnitOfWorkManager.Current.SaveChangesAsync();

                    }

                    SetCurrentUser(loginResult.User.MapTo<UserDto>());
                    //return Json(new AjaxResponse { TargetUrl = returnUrl });
                    if (await _userManager.IsInRoleAsync(loginResult.User, StaticRoleNames.Tenants.Admin)
                     || await _userManager.IsInRoleAsync(loginResult.User, StaticRoleNames.Tenants.AdminLevel2))
                    {
                       var url = returnUrl.Equals("/Home") ? NormalizeReturnUrl("/Manage/Index") : returnUrl;
                        return RedirectPermanent(url);
                    }
                    if (await _userManager.IsInRoleAsync(loginResult.User, StaticRoleNames.Tenants.Sponsors)
                        || await _userManager.IsInRoleAsync(loginResult.User, StaticRoleNames.Tenants.Users))
                    {
                        var url = string.IsNullOrEmpty(returnUrl) || returnUrl == "/Home/Dashboard" ? NormalizeReturnUrl("/Manage") : returnUrl;
                         url = url.Equals("/Home") ? NormalizeReturnUrl("/Contest") : url;
                        return RedirectPermanent(url);
                    }
                    return RedirectToAction("UnrecognizedAccount");
                 case AbpLoginResultType.UserEmailIsNotConfirmed:
                     if (!Request.IsAjaxRequest())
                     {
                         return View("EmailConfirmationError", new UserDto{EmailAddress = loginModel.UsernameOrEmailAddress});  
                     }
                     else
                     {
                         return Json(new { Error  = new ErrorInfo(L("UserEmailIsNotConfirmedAndCanNotLogin")),
                             Success = false, Type = "EmailConfirmation" });
                     }
                    
                default:
                    throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(loginResult.Result, loginModel.UsernameOrEmailAddress, GetTenancyNameOrNull());
            }
            }
            catch (UserFriendlyException e)
            {
                ViewBag.ErrorMessage = e.Details;
                return View(new LoginFormViewModel
                {
                    ReturnUrl = returnUrl,
                    IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled,
                    IsSelfRegistrationAllowed = IsSelfRegistrationEnabled(),
                    MultiTenancySide = AbpSession.MultiTenancySide
                });
            }
            
        }

        public async Task<ActionResult> Logout()
        {
            
            await _signInManager.SignOutAsync();
            CookieAppService.Remove(AppConsts.CookieKeyCurrentUser);
            CookieAppService.Remove(AppConsts.CookieKeyCurrentLoginInformations);

            return RedirectToAction("Index","Home");
        } 
        
        public async Task<ActionResult> UnrecognizedAccount()
        {
            await _signInManager.SignOutAsync();
            return View();
        }


        private async Task<AbpLoginResult<Tenant, User>> GetLoginResultAsync(string usernameOrEmailAddress, string password, string tenancyName)
        {
            var loginResult = await _logInManager.LoginAsync(usernameOrEmailAddress, password, tenancyName);

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    return loginResult;
                default:
                    throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(loginResult.Result, usernameOrEmailAddress, tenancyName);
            }
        }

        #endregion

        #region Register

        public async Task<ActionResult> Register(string identity)
        {
            User referredUser ;
            if (!string.IsNullOrEmpty(identity))
            {
                var convertResult = long.TryParse(identity, out var userId);
                if (convertResult)
                {
                    referredUser = await _userManager.Users.FirstOrDefaultAsync(itm => itm.Id == userId);
                    if (referredUser != null)
                    {
                        ViewBag.ReferredUser = referredUser.MapTo<UserDto>();
                    }
                }
                ViewBag.Identity = identity;
            }
            var countries = _countryStateAppService.GetAll(new PagedResultRequestDto
            {
                SkipCount  = 0, MaxResultCount = 300
            });
            ViewBag.Countyries = countries.Result.Items.Select(item => new SelectListItem(item.Name, item.Id)).ToList();
            return RegisterView(new RegisterViewModel());
        }

        private ActionResult RegisterView(RegisterViewModel model)
        {
            ViewBag.IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled;
            var countries = _countryStateAppService.GetAll(new PagedResultRequestDto
            {
                SkipCount  = 0, MaxResultCount = 300
            });
            ViewBag.Countyries = countries.Result.Items.Select(item => new SelectListItem(item.Name, item.Id)).ToList();
            return View("Register", model);
        }

        private bool IsSelfRegistrationEnabled()
        {
            if (!AbpSession.TenantId.HasValue)
            {
                return false; // No registration enabled for host users!
            }

            return true;
        }

        [HttpPost]
        [UnitOfWork]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            try
            {
                User referredUser ;
                if (model.ReferredUserId != null)
                {
                        referredUser = await _userManager.Users.FirstOrDefaultAsync(itm => itm.Id == model.ReferredUserId);
                        if (referredUser != null)
                        {
                            ViewBag.ReferredUser = referredUser.MapTo<UserDto>();
                        }
                    ViewBag.Identity = GetUserAppIdentity(model.ReferredUserId.Value);
                }
                ExternalLoginInfo externalLoginInfo = null;
                if (model.IsExternalLogin)
                {
                    externalLoginInfo = await _signInManager.GetExternalLoginInfoAsync();
                    if (externalLoginInfo == null)
                    {
                        throw new Exception("Can not external login!");
                    }

                   // model.UserName = model.EmailAddress;
                    model.Password = Authorization.Users.User.CreateRandomPassword();
                }
                else
                {
                    if (model.NickName.IsNullOrEmpty() || model.PhoneNumber.IsNullOrEmpty() || model.EmailAddress.IsNullOrEmpty()
                        || model.CountryId.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException(L("FormIsNotValidMessage"));
                    }
                    var countries = _countryStateAppService.GetAll(new PagedResultRequestDto
                    {
                        SkipCount  = 0, MaxResultCount = 300
                    });
                    ViewBag.Countyries = countries.Result.Items.Select(item => new SelectListItem(item.Name, item.Id, item.Id == model.CountryId )).ToList();

                    if (await _userRegistrationManager.IsPhoneNumberExists(model.PhoneNumber))
                    {
                        ViewBag.ErrorMessage = L("PhoneNumberTaken");
                        return RegisterView(model);
                    }
                   
                }

                var user = await _userRegistrationManager.RegisterAsync(
                    model.NickName,
                    model.PhoneNumber,
                    model.EmailAddress,
                    model.Password,
                    true, // Assumed email address is always confirmed. Change this if you want to implement email confirmation.
                    model.StateId,
                    model.CountryId,
                    model.ReferredUserId
                );

                // Getting tenant-specific settings
                var isEmailConfirmationRequiredForLogin = await SettingManager.GetSettingValueAsync<bool>(AppConsts.SettingsConfirmEmailAfterAccountCreation);

                if (model.IsExternalLogin)
                {
                    Debug.Assert(externalLoginInfo != null);
                    
                    if (string.Equals(externalLoginInfo.Principal.FindFirstValue(ClaimTypes.Email), model.EmailAddress, StringComparison.OrdinalIgnoreCase))
                    {
                        user.IsEmailConfirmed = false;
                    }

                    user.Logins = new List<UserLogin>
                    {
                        new UserLogin
                        {
                            LoginProvider = externalLoginInfo.LoginProvider,
                            ProviderKey = externalLoginInfo.ProviderKey,
                            TenantId = user.TenantId
                        }
                    };
                }

                await _unitOfWorkManager.Current.SaveChangesAsync();

                Debug.Assert(user.TenantId != null);

                var tenant = await _tenantManager.GetByIdAsync(user.TenantId.Value);

                // Directly login if possible
                if (user.IsActive && (user.IsEmailConfirmed || !isEmailConfirmationRequiredForLogin))
                {
                    AbpLoginResult<Tenant, User> loginResult;
                    if (externalLoginInfo != null)
                    {
                        loginResult = await _logInManager.LoginAsync(externalLoginInfo, tenant.TenancyName);
                    }
                    else
                    {
                        loginResult = await GetLoginResultAsync(user.UserName, model.Password, tenant.TenancyName);
                    }

                    if (loginResult.Result == AbpLoginResultType.Success)
                    {
                        await _signInManager.SignInAsync(loginResult.Identity, false);
                        if ((await _userManager.GetClaimsAsync(loginResult.User)).Count == 0)
                        {
                            if (loginResult.User.UserName == AbpUserBase.AdminUserName)
                            {
                                 await _userManager.AddClaimAsync(loginResult.User,
                                    new Claim(CustomClaimTypes.ClaimTypeNickname, loginResult.User.Name));
                                await _userManager.AddClaimAsync(loginResult.User,
                                    new Claim(CustomClaimTypes.ClaimTypeImage, loginResult.User.Name));

                            }
                            else
                            {
                                await _userManager.AddClaimAsync(loginResult.User,
                                    new Claim(CustomClaimTypes.ClaimTypeNickname, loginResult.User.NickName));
                                if (loginResult.User.Image != null)
                                {
                                    await _userManager.AddClaimAsync(loginResult.User,
                                        new Claim(CustomClaimTypes.ClaimTypeImage, loginResult.User.Image));
                                }
                            }
                            await UnitOfWorkManager.Current.SaveChangesAsync();

                        }
                        return Redirect(GetAppHomeUrl());
                    }

                    Logger.Warn("New registered user could not be login. This should not be normally. login result: " + loginResult.Result);
                }

                if (isEmailConfirmationRequiredForLogin)
                {
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    
                    // for some weird reason the following commented out line (which should return an absolute URL) returns null instead
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Scheme);

                    //Send mail here
                    var emailTemplate = await _emailTemplateAppService.GetEmailTemplateByName(AppConsts.EmailTemplateNewUserCreation);
                    if (emailTemplate != null)
                    {
                        var htmlContent = emailTemplate.Template.Replace("[newusernickname]", model.NickName);
                        htmlContent = htmlContent.Replace("[activationlink]", callbackUrl);
                        BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                            (
                                new SendEmailDto
                                {
                                    Tos = new List<EmailAddress>{new EmailAddress(model.EmailAddress, model.NickName)},
                                    Subject = "Account creation was successful",
                                    Body = htmlContent
                                })
                        ); 
                    } 
                }
               
               
                //<Todo: Remove here when email is working>
                //var confirmation = await _userManager.ConfirmEmailAsync(user, code);
                //</Todo: Remove here when email is working>

                return View("RegisterResult", new RegisterResultViewModel
                {
                    TenancyName = tenant.TenancyName,
                    NameAndSurname = user.Name + " " + user.Surname,
                    UserName = user.UserName,
                    EmailAddress = user.EmailAddress,
                    IsEmailConfirmed = user.IsEmailConfirmed,
                    IsActive = user.IsActive,
                    IsEmailConfirmationRequiredForLogin = isEmailConfirmationRequiredForLogin
                });
            }
            catch (UserFriendlyException ex)
            {
                ViewBag.ErrorMessage = ex.Message;

                return View("Register", model);
            }
        }

        /// <summary>
        /// Used to activate email from the link created during login
        /// </summary>
        /// <param name="code">UserId</param>
        /// <param name="userId"></param>
        /// <returns>View</returns>
        public async Task<IActionResult> ConfirmEmail(long userId, string code)
        {
            //TODO: Activate email from link
           var user = await _userManager.Users.FirstOrDefaultAsync(usr => usr.Id == userId);
            if (user == null)
            {
                return RedirectToAction("ConfirmEmail", "Error");
            }
            var confirmation = await _userManager.ConfirmEmailAsync(user, code);
            if (confirmation.Succeeded)
            {
                
                return View(); 
            }

            var errors = confirmation.Errors.Select(error => error.Description).ToList();
            if (errors.Contains("Invalid Token"))
            {
                ViewBag.ErrorMessage = "Token expired. Please try to resend the activation link.";
            }
            else
            {
                ViewBag.ErrorMessage = string.Join("<br />", errors);
            }
            return View("AccountProcessingError");

        }
        /// <summary>
                /// Used to resend email activation link
                /// </summary>
                /// <param name="email">UserId</param>
                /// <returns>View</returns>
        public async Task<IActionResult> ResendActivationLink(string email)
        {
            //TODO: Activate email from link
            if (email == null)
            {
                return RedirectToAction("ConfirmEmail", "Error");
            }
            var user = await _userManager.Users.FirstOrDefaultAsync(usr => usr.EmailAddress == email);
            if (user == null)
            {
                return RedirectToAction("ConfirmEmail", "Error");
            }
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            code = HttpUtility.UrlEncode(code);
             var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Scheme);
            
            var emailTemplate = await _emailTemplateAppService.GetEmailTemplateByName(AppConsts.EmailTemplateNewUserCreation);
            if (emailTemplate != null)
            {
                var htmlContent = emailTemplate.Template.Replace("[newusernickname]", user.NickName);
                htmlContent = htmlContent.Replace("[activationlink]", callbackUrl);
                BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                    (
                        new SendEmailDto
                        {
                            Tos = new List<EmailAddress>{new EmailAddress(user.EmailAddress, user.NickName)},
                            Subject = "Account creation was successful",
                            Body = htmlContent
                        })
                ); 
            }
           
            return View();

        }
        public IActionResult ForgotPassword()
        {
                return View(); 

        }
        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            //TODO: Activate email from link
            var user = await _userManager.Users.FirstOrDefaultAsync(usr => usr.EmailAddress == model.EmailAddress);
            if (user != null)
            {
                var emailTemplate = await _emailTemplateAppService.GetEmailTemplateByName(AppConsts.EmailTemplateResetPasswordEmailTemplate);
                if (emailTemplate != null)
                {
                    var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                    code = HttpUtility.UrlEncode(code);
                    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Scheme);
            
                    var htmlContent = emailTemplate.Template.Replace("[usernickname]", user.NickName);
                    htmlContent = htmlContent.Replace("[resetpasswordlink]", callbackUrl);
                    BackgroundJob.Enqueue<BgJobEmailSender>(x => x.Send
                        (
                            new SendEmailDto
                            {
                                Tos = new List<EmailAddress>{new EmailAddress(user.EmailAddress, user.NickName)},
                                Subject = "Your login details for iContestPRO",
                                Body = htmlContent
                            })
                    ); 
                }
                
            }
            TempData["SuccessMessage"] = "The password reset process has now been started. Please check your email for instructions on what to do next.";
            return RedirectToAction("Login");

        }
        public async Task<IActionResult> ResetPassword(long userId, string code)
        {
            //TODO: Activate email from link
            var user = await _userManager.Users.FirstOrDefaultAsync(usr => usr.Id == userId);
            if (user == null)
            {
                TempData["Message"] = "Error occurred while trying to reset your password.";
                return RedirectToAction("ForgotPassword");
            }

            var model = new ResetPasswordViewModel
            {
            Code = code,
            UserId = userId,
            EmailAddress = user.EmailAddress
            };
                return View(model); 
           

        }
        [HttpPost]
        [UnitOfWork(false)]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!model.Password.Equals(model.ConformPassword))
            {
                ViewBag.ErrorMessage = "Password and confirm password does not match.";
                return View(model);
            }
            //TODO: Activate email from link
            var user = await _userManager.Users.FirstOrDefaultAsync(usr => usr.Id == model.UserId);
            if (user == null)
            {
                TempData["ErrorMessage"] = "Error occurred while trying to reset your password.";
                return RedirectToAction("ForgotPassword");
            }

            var code = HttpUtility.UrlDecode(model.Code);
            var result = await _userManager.ResetPasswordAsync(user, code, model.Password);
            if (result.Succeeded)
            {
                TempData["SuccessMessage"] = "Password reset completed successfully.";
                return RedirectToAction("Login");
            }
            var errors = result.Errors.Select(error => error.Description).ToList();
            if (errors.Contains("Invalid token"))
            {
                ViewBag.ErrorMessage = "Token expired. Please try to resend the forgot password link.";
                return View(model);
            }
            
            TempData["ErrorMessage"] = "Error occurred while trying to reset your password.";
            return RedirectToAction("ForgotPassword");

        }
        #endregion

        #region External Login

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            var redirectUrl = Url.Action(
                "ExternalLoginCallback",
                "Account",
                new
                {
                    ReturnUrl = returnUrl
                });

            return Challenge(
                // TODO: ...?
                // new Microsoft.AspNetCore.Http.Authentication.AuthenticationProperties
                // {
                //     Items = { { "LoginProvider", provider } },
                //     RedirectUri = redirectUrl
                // },
                provider
            );
        }

        [UnitOfWork]
        public virtual async Task<ActionResult> ExternalLoginCallback(string returnUrl, string remoteError = null)
        {
            returnUrl = NormalizeReturnUrl(returnUrl);
            
            if (remoteError != null)
            {
                Logger.Error("Remote Error in ExternalLoginCallback: " + remoteError);
                throw new UserFriendlyException(L("CouldNotCompleteLoginOperation"));
            }

            var externalLoginInfo = await _signInManager.GetExternalLoginInfoAsync();
            if (externalLoginInfo == null)
            {
                Logger.Warn("Could not get information from external login.");
                return RedirectToAction(nameof(Login));
            }

            await _signInManager.SignOutAsync();

            var tenancyName = GetTenancyNameOrNull();

            var loginResult = await _logInManager.LoginAsync(externalLoginInfo, tenancyName);

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    await _signInManager.SignInAsync(loginResult.Identity, false);
                    return Redirect(returnUrl);
                case AbpLoginResultType.UnknownExternalLogin:
                    return await RegisterForExternalLogin(externalLoginInfo);
                default:
                    throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(
                        loginResult.Result,
                        externalLoginInfo.Principal.FindFirstValue(ClaimTypes.Email) ?? externalLoginInfo.ProviderKey,
                        tenancyName
                    );
            }
        }

        private async Task<ActionResult> RegisterForExternalLogin(ExternalLoginInfo externalLoginInfo)
        {
            var email = externalLoginInfo.Principal.FindFirstValue(ClaimTypes.Email);
            var nameinfo = ExternalLoginInfoHelper.GetNameAndSurnameFromClaims(externalLoginInfo.Principal.Claims.ToList());

            var viewModel = new RegisterViewModel
            {
                EmailAddress = email,
                //Name = nameinfo.name,
                //Surname = nameinfo.surname,
                IsExternalLogin = true,
                ExternalLoginAuthSchema = externalLoginInfo.LoginProvider
            };

            if (nameinfo.name != null &&
                nameinfo.surname != null &&
                email != null)
            {
                return await Register(viewModel);
            }

            return RegisterView(viewModel);
        }

        [UnitOfWork]
        protected virtual async Task<List<Tenant>> FindPossibleTenantsOfUserAsync(UserLoginInfo login)
        {
            List<User> allUsers;
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                allUsers = await _userManager.FindAllAsync(login);
            }

            return allUsers
                .Where(u => u.TenantId != null)
                .Select(u => AsyncHelper.RunSync(() => _tenantManager.FindByIdAsync(u.TenantId.Value)))
                .ToList();
        }

        #endregion

        #region Helpers

        public ActionResult RedirectToAppHome()
        {
            return RedirectToAction("Index", "Manage");
        }

        public string GetAppHomeUrl()
        {
            return Url.Action("Index", "Manage");
        }

        #endregion

        #region Change Tenant

        public async Task<ActionResult> TenantChangeModal()
        {
            var loginInfo = await _sessionAppService.GetCurrentLoginInformations();
            return View("/Views/Shared/Components/TenantChange/_ChangeModal.cshtml", new ChangeModalViewModel
            {
                TenancyName = loginInfo.Tenant?.TenancyName
            });
        }

        #endregion

        #region Common

        private string GetTenancyNameOrNull()
        {
            if (!AbpSession.TenantId.HasValue)
            {
                return null;
            }

            return _tenantCache.GetOrNull(AbpSession.TenantId.Value)?.TenancyName;
        }

        private string NormalizeReturnUrl(string returnUrl, Func<string> defaultValueBuilder = null)
        {
            if (defaultValueBuilder == null)
            {
                defaultValueBuilder = GetAppHomeUrl;
            }

            if (returnUrl.IsNullOrEmpty())
            {
                return defaultValueBuilder();
            }

            if (Url.IsLocalUrl(returnUrl))
            {
                return returnUrl;
            }

            return defaultValueBuilder();
        }

        #endregion

        #region Etc

        

        #endregion

       
    }
}
