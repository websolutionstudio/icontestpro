﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using IContestPro.Controllers;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : IContestProControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Contact()
        {
            return View();
        }
	}
}
