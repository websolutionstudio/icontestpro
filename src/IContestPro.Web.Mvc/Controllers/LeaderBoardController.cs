﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Dependency;
using Abp.Web.Mvc.Alerts;
using Microsoft.AspNetCore.Mvc;
using IContestPro.LeaderBoard;
using IContestPro.Web.Helpers.AlertExtension;

namespace IContestPro.Web.Controllers
{
    public class LeaderBoardController : BaseController
    {
        private readonly ILeaderBoardAppService _leaderBoardAppService;
        public LeaderBoardController( IIocResolver iocResolver, ILeaderBoardAppService leaderBoardAppService) : base(iocResolver)
        {
            _leaderBoardAppService = leaderBoardAppService;
        }
        
        // GET
        public async Task<IActionResult> Index(string displayType = "", int page = 1, int pageSize = 10)
        {
            
            ViewBag.ActiveLeaderBoard = "active";
            ViewBag.DisplayType = displayType;
                var offset = (page - 1) * pageSize;  
                ViewBag.PageSize = pageSize;
                ViewBag.Page = page;
                var items = await _leaderBoardAppService.GetTopContestEntriesWithHighestVote(displayType, new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items); 
        } 
       
         public async Task<IActionResult> ShowMore(string id, int pageSize = 50)
        {
            if (string.IsNullOrEmpty(id))
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please select a contest to view."));
                return RedirectToAction("Index");
            }

            ViewBag.TotalTake = pageSize;
            ViewBag.ActiveLeaderBoard = "active";
                var items = await _leaderBoardAppService.GetTopContestEntriesWithHighestVoteByContestPermalink(id, pageSize);
            if (items == null)
            {
                AddAlertMessage(
                    new AlertMessageCustom(
                        AlertType.Danger, "Contest not found."
                    ));
                return RedirectToAction("Index");
            }
            return View(items); 
        } 
       
        

        
    }
}