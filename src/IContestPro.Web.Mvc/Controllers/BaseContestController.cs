﻿
using Abp.Dependency;

namespace IContestPro.Web.Controllers
{
    public class BaseContestController : BaseController
    {

        public BaseContestController(IIocResolver iocResolver) : base(iocResolver)
        {
         }
    }
}