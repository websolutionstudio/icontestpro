﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using Abp.Web.Mvc.Alerts;
using IContestPro.Authorization;
using IContestPro.Authorization.Roles;
using IContestPro.Bank;
using IContestPro.Bank.Dto;
using IContestPro.BankDetails;
using IContestPro.BankDetails.Dto;
using IContestPro.HttpRequest;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Contest})]
    public class BankController : BaseContestController
    {
        private readonly IBankAppService _bankAppService;
        private readonly IBankDetailsAppService _bankDetailsAppService;
        public BankController( 
            IBankAppService bankAppService,
            IBankDetailsAppService bankDetailsAppService, IIocResolver iocResolver) : base(iocResolver)
        {
            _bankAppService = bankAppService;
            _bankDetailsAppService = bankDetailsAppService;
        }
        // GET
       
      

        #region Banks
        
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> Index(int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _bankAppService.GetAll(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items); 
        }
       
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public IActionResult Create()
        {
               return View();
         
        } 
        [HttpPost]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> Create(CreateBankDto model)
        {
            try
            {
                var item = await _bankAppService.Create(model);
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Bank created successfully."));
                    return RedirectToAction("Index");
                }
            }
            catch (UserFriendlyException e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, e.Message));
                return View(model);  
            }
            //Edit mode
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating bank."));
            return View(model);
          
        } 
        
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public async Task<IActionResult> Edit(EntityDto input)
        {
            var item = await _bankAppService.Get(input);
            if (item == null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Bank not found."));
                return RedirectToAction("Index");
            }
            return View(item.MapTo<UpdateBankDto>());
         
        } 
        [HttpPost]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> Edit(UpdateBankDto model)
        {
            var item = await _bankAppService.Update(model);
            if (item.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Bank updated successfully."));
                return RedirectToAction("Index");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while updating bank."));
            return View(model);
          
        }
        
        [HttpPost]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> Delete(EntityDto model)
        {
            await _bankAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Bank deleted successfully."));
            return RedirectToAction("Index");
        } 
        
        #endregion
        
      #region BankDetails
        
        [AbpMvcAuthorize]
        public  async Task<IActionResult> BankDetails(int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            PagedResultDto<BankDetailsDto> items;
            if (User.IsInRole(StaticRoleNames.Tenants.Admin) || User.IsInRole(StaticRoleNames.Tenants.AdminLevel2))
            {
                 items = await _bankDetailsAppService.GetAll(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
                return View(items);  
            }
            
             items = await _bankDetailsAppService.GetByUserId(AbpSession.GetUserId(), new PagedResultRequestDto
            {
                SkipCount = offset,
                MaxResultCount = pageSize
            });
            return View(items);  

        }
       
        [AbpMvcAuthorize]
        public async Task<IActionResult> CreateBankDetails()
        {
            var selectList = await _bankAppService.GetAllActive(new PagedAndSortedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            ViewBag.Banks = new SelectList(selectList.Items, "Id", "Name");
               return View();
         
        } 
        
        [HttpPost]
        [AbpMvcAuthorize]
        public  async Task<IActionResult> CreateBankDetails(CreateBankDetailsDto model)
        {
            try
            {
               
                /*var bank = await _bankAppService.Get(new EntityDto{Id = model.BankId});
            var httpRequest = HttpRequestAppService.Get<BankAccountValidateResponseDto>(
                $"{AppConsts.NigeriaUniformBankAccountNumberUrl}{bank.Code}/{model.AccountNumber}");
            if (httpRequest.Success && httpRequest.Result.valid)
            {
                //Todo: Bank account and bank are valid. Create bank details
            }*/
                var selectList = await _bankAppService.GetAllActive(new PagedAndSortedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
                ViewBag.Banks = new SelectList(selectList.Items, "Id", "Name", model.BankId);
                if (await _bankDetailsAppService.AcountExists(model.AccountNumber))
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Bank account alraedy exists."));
                    return View(model);  
                }
                var item = await _bankDetailsAppService.Create(model);
               
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Bank details created successfully."));
                    return RedirectToAction("BankDetails");
                }

            }
            catch (UserFriendlyException e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, e.Message));
                return View(model);  
            }
             AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating bank details."));
            return View(model);
          
        } 
        
        [AbpMvcAuthorize]
        public async Task<IActionResult> EditBankDetails(EntityDto input)
        {
            var selectList = await _bankAppService.GetAllActive(new PagedAndSortedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            var item = await _bankDetailsAppService.Get(input);
            if (item == null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Bank details not found."));
                return RedirectToAction("BankDetails");
            }
            ViewBag.Banks = new SelectList(selectList.Items, "Id", "Name", item.BankId);
            return View(item);
         
        } 
        [HttpPost]
        [AbpMvcAuthorize]
        public  async Task<IActionResult> EditBankDetails(BankDetailsDto model)
        {
            var selectList = await _bankAppService.GetAllActive(new PagedAndSortedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            ViewBag.Banks = new SelectList(selectList.Items, "Id", "Name", model.BankId);
            var item = await _bankDetailsAppService.Update(model);
            if (item.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Bank details updated successfully."));
                return RedirectToAction("BankDetails");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while updating bank."));
            return View(model);
          
        }
        
        [HttpPost]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> DeleteDetails(EntityDto model)
        {
            await _bankDetailsAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Bank details deleted successfully."));
            return RedirectToAction("BankDetails");
        } 
        public  async Task<IActionResult> ValidateBankAccount(int? bankId, string accountNumber)
        {
            var ajaxResponse  = new AjaxResponse();
            try
            {
                if (bankId == null || accountNumber == null)
                {
                    ajaxResponse.Success  = false;
                    ajaxResponse.Error = new ErrorInfo{Message = "Bank id and account number are required.", Code = 500};
                    return Json(ajaxResponse);
                }
                var bank = await _bankAppService.Get(new EntityDto{Id = bankId.Value});
                if (bank == null)
                {
                    ajaxResponse.Success  = false;
                    ajaxResponse.Error = new ErrorInfo{Message = "Bank not found.", Code = 500};
                    return Json(ajaxResponse);
                }
                var httpRequest = HttpRequestAppService.Get<BankAccountValidateResponseDto>(
                    $"{AppConsts.NigeriaUniformBankAccountNumberUrl}{bank.Code}/{accountNumber}");
                ajaxResponse.Success  = httpRequest.Result.valid;
                ajaxResponse.Result = httpRequest.Result;
                return Json(ajaxResponse);  
            }
            catch (Exception e)
            {
                ajaxResponse.Success  = false;
            ajaxResponse.Error = new ErrorInfo{Message = "Error occurred while verifying your account.", Code = 500};
            return Json(ajaxResponse);
            }
            

            
        } 
        
        #endregion
        
     
        
    }
}