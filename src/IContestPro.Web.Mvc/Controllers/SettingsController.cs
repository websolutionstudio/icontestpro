﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Configuration;
using Microsoft.AspNetCore.Mvc;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.UI;
using Abp.Web.Mvc.Alerts;
using IContestPro.Authorization;
using IContestPro.CodeSystem;
using IContestPro.CodeSystem.Dto;
using IContestPro.MemoryCache;
using IContestPro.Messages.Dto;
using IContestPro.Settings;
using IContestPro.Settings.Dto;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.Extensions.Caching.Memory;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Admin)]
    public class SettingsController : BaseContestController
    {
        public SettingsController(IIocResolver iocResolver):base(iocResolver)
        {
        }

        public async Task<IActionResult> Index()
        {
            var items = await SettingManager.GetAll(new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            return View(items);
            
        } 
        [HttpPost]
        [UnitOfWork(false)]
        public async Task<IActionResult> UpdateSettings(UpdateSettingsDto model)
        {
            await SettingManager.UpdateSettings(model.NameKey, model.Value);
            return RedirectToAction("Index"); 
        } 
       

    }
}
