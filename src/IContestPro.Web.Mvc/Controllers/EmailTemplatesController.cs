﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Web.Mvc.Alerts;
using IContestPro.Authorization;
using IContestPro.EmailTemplate;
using IContestPro.EmailTemplate.Dto;
using Microsoft.AspNetCore.Mvc;
using IContestPro.Web.Helpers.AlertExtension;

namespace IContestPro.Web.Controllers
{
    [AbpAuthorize(PermissionNames.Pages_Admin)]
    public class EmailTemplatesController : BaseController
    {
        private readonly IEmailTemplateAppService _emailTemplateAppService;
        public EmailTemplatesController( IIocResolver iocResolver, IEmailTemplateAppService emailTemplateAppService) : base(iocResolver)
        {
            _emailTemplateAppService = emailTemplateAppService;
        }
        
        // GET
        public async Task<IActionResult> Index(int page = 1, int pageSize = 10)
        {
            var offset = (page - 1) * pageSize;
            var items = await _emailTemplateAppService.GetAll(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
          var viewItems = new AppPagedRequestResultDto<PagedResultDto<EmailTemplateDto>>
          {
              Page = page,
              PageSize = pageSize,
              Result = items
          };
            return View(viewItems); 
        } 
        public async Task<IActionResult> CreateEditTemplate(string type)
        {
            if (string.IsNullOrEmpty(type))
            {
                AlertManager.Alerts.Add(new AlertMessageCustom(AlertType.Warning, "Please select template to edit. Thank you."));
                return RedirectToAction("Index");
            }
            var item = await _emailTemplateAppService.GetEmailTemplateByName(type);
            if (item == null)
            {
                AlertManager.Alerts.Add(new AlertMessageCustom(AlertType.Danger, "Template not found. Please select from the list."));
                return RedirectToAction("Index");
            }

            ViewBag.Template = type;
            return View(item); 
        } 
       
        [HttpPost]
        [UnitOfWork(false)]
        public async Task<IActionResult> CreateEditTemplate(EmailTemplateDto model)
        {
            await _emailTemplateAppService.Update(model);
            AlertManager.Alerts.Add(new AlertMessageCustom(AlertType.Success, "Template updated successfully. Thank you."));
            return RedirectToAction("Index"); 
        } 
       
        
       
        

        
    }
}