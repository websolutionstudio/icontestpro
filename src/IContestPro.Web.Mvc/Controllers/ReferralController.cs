﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Dependency;
using Abp.Runtime.Session;
using IContestPro.Authorization.Roles;
using IContestPro.Referral;
using IContestPro.Referral.Dto;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]
    public class ReferralController : BaseContestController
    {
        private readonly IReferralAppService _referralAppService;
        public ReferralController(IIocResolver iocResolver, IReferralAppService referralAppService) : base(iocResolver)
        {
            _referralAppService = referralAppService;
        }
       
        public  async Task<IActionResult> Index( int page = 1, int pageSize = 20)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            ViewBag.ReferrerVotePoint = await SettingManager.GetSettingValueAsync<int>(AppConsts.SettingsReferralVotePointAward);
            ViewBag.ReferrerPercentage = await SettingManager.GetSettingValueAsync<double>(AppConsts.SettingsReferrerPercentageIfReferredUserBecomesSponsor);
            ViewBag.Page = page;
            PagedResultDto<ReferralDto> items;
            if (User.IsInRole(StaticRoleNames.Tenants.Admin) || User.IsInRole(StaticRoleNames.Tenants.AdminLevel2))
            {
                items = await _referralAppService.GetAll(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
                return View(items); 
            }
            
            items = await _referralAppService.GetAllByUserId(AbpSession.GetUserId(), new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(items);
        }
        
         
        
    }
}