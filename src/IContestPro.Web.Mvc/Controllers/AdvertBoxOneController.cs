﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Mvc.Alerts;
using IContestPro.AdvertPrice;
using IContestPro.AdvertPrice.Dto;
using IContestPro.AdvertBox1;
using IContestPro.AdvertBox1.Dto;
using IContestPro.Authorization;
using IContestPro.Contest.Dto;
using IContestPro.Helpers;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AdvertBoxOneController : BaseController
    {
        private readonly IAdvertBox1AppService _advertBox1AppService;
        private readonly IAdvertPriceAppService _advertPriceAppService;


        public AdvertBoxOneController(IIocResolver iocResolver,
            IAdvertPriceAppService advertPriceAppService, IAdvertBox1AppService advertBox1AppService): base(iocResolver)
        {
            _advertPriceAppService = advertPriceAppService;
            _advertBox1AppService = advertBox1AppService;
        }
        // GET
       
        public async Task<IActionResult> Index(int page = 1, int pageSize = 20)
        {
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var offset = (page - 1) * pageSize;
            PagedResultDto<AdvertBox1Dto> items;
            if (await IsGrantedAsync(PermissionNames.Pages_Admin))
            {
                 items = await _advertBox1AppService.GetAll( new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            }
            else
            {
                 items = await _advertBox1AppService.GetAllByUser(AbpSession.GetUserId(), new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            }
             return View(items);
        }
        
        [HttpPost]
        [UnitOfWork]
        public  async Task<IActionResult> Delete(EntityDto model)
        {
            await _advertBox1AppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Top banner deleted successfully."));
            return RedirectToAction("Index");
        } 
       
        public  async Task<IActionResult> CreateEdit(CreateEditAdvertBox1Dto model)
        {
            var selectList = await _advertPriceAppService.GetAllByType(AdvertType.AdvertBoxOne, new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            var items = selectList.Items.Select(item => 
                new AdvertPriceDto {Id = item.Id, Amount = item.Amount, 
                    Name = $"{item.Name} | {item.Amount.ToNairaString()}"}).ToList();
            ViewBag.AdvertPrices = new SelectList(items, "Id", "Name");

            if(model.Id == null)
            return View(new CreateEditAdvertBox1Dto());
         
            //Edit mode
                var itemToEdit = await _advertBox1AppService.Get(new EntityDto{Id = model.Id.Value});
                ViewBag.AdvertPrices = new SelectList(selectList.Items, "Id", "Name", itemToEdit.AdvertPriceId);
                ViewBag.Image = itemToEdit.Image;
            var input = new CreateEditAdvertBox1Dto
            {
            AdvertPriceId = itemToEdit.AdvertPriceId,
            EndDate = itemToEdit.EndDate,
            Id = itemToEdit.Id,
            StartDate = itemToEdit.StartDate,
            StatusId = itemToEdit.StatusId,
            TenantId = itemToEdit.TenantId,
            UserId = itemToEdit.UserId
            };
                return View(input);
         
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize]
        public  async Task<IActionResult> CreateEdit(CreateEditAdvertBox1Dto model, string notUsed)
        {
            try
            {
                if (model.StartDate == null || model.ExternalUrl == null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "StartDate date and external url are required."));
                    return View(model);

                }
            var selectList = await _advertPriceAppService.GetAllByType(AdvertType.AdvertBoxOne,new PagedResultRequestDto{SkipCount = 0, MaxResultCount = 100});
            var items = selectList.Items.Select(item => 
                new AdvertPriceDto {Id = item.Id, Amount = item.Amount, 
                    Name = $"{item.Name} | {item.Amount.ToNairaString()}"}).ToList();
            ViewBag.AdvertPrices = new SelectList(items, "Id", "Name");
            
            if (model.Id == null)
            {
                if(model.Image == null)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "ContestEntryImage is required."));
                    return View(model);
                }
                using (var imageCheck = Image.FromStream(model.Image.OpenReadStream()))
                {
                    if (imageCheck.Width != 500 || imageCheck.Height != 700)
                    {
                        AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Banner dimension must be width: 500px and height: 700px."));
                        return View(model); 
                    }
                }
                model.UserId = AbpSession.GetUserId();
                var item = await _advertBox1AppService.CreateAndUploadImage(model);
                if (item.Id > 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Top banner created successfully."));
                    return RedirectToAction("Index");
                }
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
                return View(model);
            }
            
            //Edit mode
            if(model.Image != null)
            {
                using (var imageCheck = Image.FromStream(model.Image.OpenReadStream()))
                {
                    if (imageCheck.Width != 500 || imageCheck.Height != 700)
                    {
                        AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Banner dimension must be width: 500x and height: 700px."));
                        return View(model); 
                    }
                }
            }
            var updateItem = await _advertBox1AppService.UpdateAndUploadImage(model);
            if (updateItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Top banner updated successfully."));
                return RedirectToAction("Index");
            }
            }
            catch (UserFriendlyException e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, e.Message));
                return View(model); 
            }
            catch (Exception e)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
                return View(model);
            }
            
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating advert."));
            return View(model);
          
        } 
        [HttpPost]
        public  async Task<IActionResult> ChangeStatus(UpdateStatusDto model)
        {
            var savedItem = await _advertBox1AppService.UpdateStatusAsync(model);
            if (savedItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Top banner status updated successfully."));
                return RedirectToAction("Index");
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while updating request."));
            return RedirectToAction("Index");
        }
       
        [AllowAnonymous]
        public async Task<ActionResult> GetRandomLive()
        {
            var items = await _advertBox1AppService.GetRandomLive();
            return View("_GetRandomAdvertboxOne", items);
        }
        
    }
}