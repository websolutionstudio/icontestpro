﻿using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Alerts;
using IContestPro.AssetUpload;
using IContestPro.AssetUpload.Dto;
using IContestPro.Authorization;
using IContestPro.Blog;
using IContestPro.Blog.Dto;
using IContestPro.Contest.ContestType.Dto;
using IContestPro.Contest.Dto;
using IContestPro.Helpers;
using Microsoft.AspNetCore.Mvc;
using IContestPro.LeaderBoard;
using IContestPro.Status;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IContestPro.Web.Controllers
{
    public class BlogsController : BaseController
    {
        private readonly IBlogAppService _blogAppService;
        private readonly IAssetUploadAppService _assetUploadAppService;
        private readonly IStatusAppService _statusAppService;

        public BlogsController(IIocResolver iocResolver, IBlogAppService blogAppService, IAssetUploadAppService assetUploadAppService, IStatusAppService statusAppService) : base(iocResolver)
        {
            _blogAppService = blogAppService;
            _assetUploadAppService = assetUploadAppService;
            _statusAppService = statusAppService;
        }

        #region Admin
        // GET
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public async Task<IActionResult> IndexAdmin(int page = 1, int pageSize = 10, string status = "All")
        {
                ViewBag.PageSize = pageSize;
                ViewBag.Page = page;
                ViewBag.Status = status;
                var items = await _blogAppService.GetAllByStatusAsync(page,pageSize, status);
                return View(items); 
        } 
       
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> Delete(EntityDto model)
        {
            await _blogAppService.Delete(model);
            AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Contest type deleted successfully."));
            return RedirectToAction("Index");
        } 
       
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> Create()
        {
            return View(new CreateBlogDto());
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public  async Task<IActionResult> Create(CreateBlogDto model, IFormFile file, bool activateNews)
        {
            ViewBag.ActivateNews = activateNews;
            if (file == null || file.Length == 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "Please upload a file."));
                return View(model); 
            }
            if (file.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Please select only image file."));
                return View(model);
            }
            var uploaded = _assetUploadAppService.CropAndUploadImage(new UploadDto{File = file, Path = "images/uploads"}, 650, 300);
            model.Image = uploaded.FileName;

            model.Permalink = model.Title.GeneratePermalink();
            var itemExists = await _blogAppService.GetByTitle(model.Title);
            if (itemExists != null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Danger, $"News with the title '{itemExists.Title}' already exists."));
                return View(model); 
            }
            var item = await _blogAppService.Create(model, activateNews);
            if (item.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "News created successfully."));
                return RedirectToAction("DetailsAdmin", new {id = item.Id});
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating news."));
            return View(model);
        } 
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Contest_Update})]
        public  async Task<IActionResult> Edit(int id)
        {
            var itemGotten = await _blogAppService.Get(new EntityDto{Id = id});
            if (itemGotten == null)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Warning, "The blog you are looking for was not found."));
                return RedirectToAction("IndexAdmin");
            }

            ViewBag.Status = await _statusAppService.Get(new EntityDto{Id = itemGotten.StatusId});
            return View(itemGotten.MapTo<EditBlogDto>());
        }
        
        [HttpPost]
        [UnitOfWork(false)]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Contest_Update})]
        public  async Task<IActionResult> Edit(EditBlogDto model, IFormFile file)
        {
            ViewBag.Status = await _statusAppService.Get(new EntityDto{Id = model.StatusId});
            if (file != null)
            {
                if (file.Length == 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Please upload a file."));
                    return View(model); 
                }

                if (file.ContentType.IndexOf("image", StringComparison.OrdinalIgnoreCase) < 0)
                {
                    AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Please select only image file."));
                    return View(model);
                
                }
                var uploaded = _assetUploadAppService.CropAndUploadImage(new UploadDto{File = file, Path = "images/uploads"}, 650, 300);
                _assetUploadAppService.Delete(new DeleteAssetDto{Path = $"/images/uploads/{model.Image}"});
                model.Image = uploaded.FileName;
            }
           
            var savedItem = await _blogAppService.Update(model.MapTo<BlogDto>());
            if (savedItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Blog edited successfully."));
                return RedirectToAction("DetailsAdmin", new {id = savedItem.Permalink});
            }
             AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while editing blog."));
            return View(model);
        }
        
        
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin})]
        public async Task<IActionResult> DetailsAdmin(string id, int page = 1, int pageSize = 50)
        {
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var item = await _blogAppService.GetDetailsByPermalink(id, new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(item); 
        } 
        [HttpPost]
        [UnitOfWork]
        [AbpMvcAuthorize(Permissions = new []{PermissionNames.Pages_Admin, PermissionNames.Pages_Sponsor})]
        public  async Task<IActionResult> ChangeStatus(UpdateStatusDto model, string permalink)
        {
            var savedItem = await _blogAppService.UpdateStatusAsync(model);
            if (savedItem.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Blog updated successfully."));
                return RedirectToAction("DetailsAdmin", new {id = permalink});
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while updating contest."));
            return RedirectToAction("DetailsAdmin", new {id = permalink});
        }
        #endregion
        
        
        #region PublicPages
        public async Task<IActionResult> Index(int page = 1, int pageSize = 10)
        {
            ViewBag.ActiveOurBlog = "active";
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var items = await _blogAppService.GetAllByStatusAsync(page, pageSize, AppConsts.StatusActive);
            return View(items); 
        } 
        public async Task<IActionResult> Tag(string id, int page = 1, int pageSize = 10)
        {
            ViewBag.ActiveOurBlog = "active";
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            ViewBag.Tag = id;
            var items = await _blogAppService.SearchBlogsByTag(id, page, pageSize);
            return View(items); 
        } 
        
        /// <summary>
        /// This method fetches the details of a news
        /// </summary>
        /// <param name="id">The permalink of the news</param>
        /// <param name="page">Current page</param>
        /// <param name="pageSize">Size of the page</param>
        /// <returns>Action result</returns>
        
        public async Task<IActionResult> Details(string id, int page = 1, int pageSize = 50)
        {
            ViewBag.ActiveOurBlog = "active";
            var offset = (page - 1) * pageSize;  
            ViewBag.PageSize = pageSize;
            ViewBag.Page = page;
            var item = await _blogAppService.GetDetailsByPermalink(id, new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
            return View(item); 
        } 
        [HttpPost]
        [UnitOfWork]
        public  async Task<IActionResult> NewBlogResponse(CreateBlogResponseDto model, string permalink)
        {
            var item = await _blogAppService.CreateResponse(model);
            if (item.Id > 0)
            {
                AddAlertMessage(new AlertMessageCustom(AlertType.Success, "Comment posted successfully."));
                return RedirectToAction("Details", new {id = permalink});
            }
            AddAlertMessage(new AlertMessageCustom(AlertType.Danger, "Error occurred while creating news."));
            return RedirectToAction("Details", new {id = permalink});
        } 
        #endregion
    }
}