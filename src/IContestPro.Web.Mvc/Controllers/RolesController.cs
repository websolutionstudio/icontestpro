using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using IContestPro.Authorization;
using IContestPro.Controllers;
using IContestPro.Roles;
using IContestPro.Roles.Dto;
using IContestPro.Web.Models.Roles;

namespace IContestPro.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Roles)]
    public class RolesController : IContestProControllerBase
    {
        private readonly IRoleAppService _roleAppService;

        public RolesController(IRoleAppService roleAppService)
        {
            _roleAppService = roleAppService;
        }

        public async Task<IActionResult> Index()
        {
            var roles = (await _roleAppService.GetRolesAsync(new GetRolesInput())).Items;
            var permissions = (await _roleAppService.GetAllPermissions()).Items;
            var model = new RoleListViewModel
            {
                Roles = roles,
                Permissions = permissions
            };

            return View(model);
        }

        public async Task<ActionResult> EditRoleModal(int roleId)
        {
            /* int width = 128;
             int height = 128;
             var file = args[0];
             Console.WriteLine($"Loading {file}");
             using(FileStream pngStream = new FileStream("",FileMode.Open, FileAccess.Read))
             using(var image = new Bitmap(pngStream))
             {
                 var resized = new Bitmap(width, height);
                 using (var graphics = Graphics.FromImage(resized))
                 {
                     graphics.CompositingQuality = CompositingQuality.HighSpeed;
                     graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                     graphics.CompositingMode = CompositingMode.SourceCopy;
                     graphics.DrawImage(image, 0, 0, width, height);
                     resized.Save($"resized-{file}", ImageFormat.Png);
                     Console.WriteLine($"Saving resized-{file} thumbnail");
                 }       
             } */
            
            
            
            
            
            
            var output = await _roleAppService.GetRoleForEdit(new EntityDto(roleId));
            var model = new EditRoleModalViewModel(output);

            return View("_EditRoleModal", model);
        }
    }
}
