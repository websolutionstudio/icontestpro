﻿using System.Threading.Tasks;
using Abp.Dependency;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Controllers
{
    
    public class WinnersController : BaseController
    {
        public WinnersController( IIocResolver iocResolver) : base(iocResolver)
        {
        }
        
        // GET
        public async Task<IActionResult> Index(int page = 1, int pageSize = 10)
        {
            
            ViewBag.ActiveMore = "active";
            ViewBag.ActiveMoreWinners = "active";
                /*var offset = (page - 1) * pageSize;  
                ViewBag.PageSize = pageSize;
                ViewBag.Page = page;
                var items = await _voterLeaderBoardAppService.GetTopContestsWithHighestVotesAndTopVoters(new PagedResultRequestDto{SkipCount = offset, MaxResultCount = pageSize});
           var result = new AppPagedRequestResultDto<PagedResultDto<TopContestsWithHighestVotesAndTopVotersDto>>
           {
               Page = page,
               PageSize = pageSize,
               Result = items
           };*/
            return View(); 
        } 
       
       
        

        
    }
}