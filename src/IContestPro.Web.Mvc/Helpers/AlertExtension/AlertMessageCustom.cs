﻿using System.Collections.Generic;
using Abp.Web.Mvc.Alerts;

namespace IContestPro.Web.Helpers.AlertExtension
{
    public class AlertMessageCustom : AlertMessage
    {
        public List<LinkAction> Actions { get; set; }
        public List<LinkActionDropdown> DropdownActions { get; set; }
        public string Icon { get; set; }
        public AlertMessageCustom(AlertType type, string text, 
            string title = null, bool dismissible = true, List<LinkAction> actions = null,
            List<LinkActionDropdown> dropdownActions = null, string icon = null) : 
            base(type, text, title, dismissible)
        {
            Actions = actions;
            DropdownActions = dropdownActions;
            Icon = icon;
        }

    }

  
}