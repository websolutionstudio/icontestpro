﻿namespace IContestPro.Web.Helpers.AlertExtension
{
    public class LinkAction
    {
        public string Text { get; set; }
        public ButtonType ButtonType { get; set; }
        public string Href { get; set; }
        public string Icon { get; set; }
        public LinkAction(string text, string href, ButtonType? buttonType = null,  string icon = null)
        {
            Text = text;
            if (buttonType != null) ButtonType = buttonType.Value;
            Href = href;
            Icon = icon;
        }
    }
}