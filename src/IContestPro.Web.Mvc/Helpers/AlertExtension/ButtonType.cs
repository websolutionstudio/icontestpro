﻿namespace IContestPro.Web.Helpers.AlertExtension
{
    public enum ButtonType
    {
        Info,
        Success,
        Waring,
        Danger,
        Primary
    }
}