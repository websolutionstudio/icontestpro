﻿namespace IContestPro.Web.Helpers.AlertExtension
{
    public class LinkActionDropdown
    {
        public string Text { get; set; }
        public string Href { get; set; }
        public string Id { get; set; }
        public LinkActionDropdown(string text, string href, string id = null)
        {
            Text = text;
            Href = href;
            Id = id;
        }
    }
}