﻿namespace IContestPro.Web.Helpers.AlertExtension
{
    public interface IAlertManagerExtension
    {
        AlertListCustom Alerts { get; set; }
    }
    
}