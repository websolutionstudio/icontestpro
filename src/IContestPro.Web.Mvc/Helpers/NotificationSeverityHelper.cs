﻿using Abp.Notifications;

namespace IContestPro.Web.Helpers
{
    public class NotificationSeverityHelper
    {
        public static string GetSeverityClass(NotificationSeverity severity)
        {
            switch (severity)
            {
            case NotificationSeverity.Info:
                        return "text-info";
            case NotificationSeverity.Warn:
                        return "text-warning";
            case NotificationSeverity.Error:
                        return "text-danger";
            case NotificationSeverity.Fatal:
                        return "text-danger";
            case NotificationSeverity.Success:
                        return "text-success";
            default:
             return "";
            }
        }
    }
}