﻿using System.Linq;
using System.Security.Claims;

namespace IContestPro.Web.Helpers.Claims
{
    public static class ClaimsPrincipalExtension
    {
        /*public static string GetFullName(this ClaimsPrincipal principal)
        {
            var item = principal.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ClaimTypeFullname);
            return item?.Value;
        }  */
        public static string GetNickName(this ClaimsPrincipal principal)
        {
            var item = principal.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ClaimTypeNickname);
            return item?.Value;
        } 
        public static string GetImage(this ClaimsPrincipal principal)
        {
            var item = principal.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ClaimTypeImage);
            return item?.Value;
        }  
         
    }
}