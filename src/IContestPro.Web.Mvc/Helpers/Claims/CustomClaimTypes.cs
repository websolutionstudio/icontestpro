﻿namespace IContestPro.Web.Helpers.Claims
{
    public class CustomClaimTypes
    {
        public const string ClaimTypeFullname = "Fullname";
        public const string ClaimTypeNickname = "Nickname";
        public const string ClaimTypeImage = "Image";
    }
}