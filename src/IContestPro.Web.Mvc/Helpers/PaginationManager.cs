﻿using System;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IContestPro.Web.Helpers
{
    public static class PaginationManager
    {
        
        public static IHtmlContent Paginate(this IHtmlHelper htmlHelper, int totalCount, int page, int pageSize, [AspMvcController] string controler, [AspMvcAction] string action, string myParams = null)
    {
        
        var last = (int)Math.Ceiling(Convert.ToDouble(totalCount) / pageSize);
        const int links = 7; //Total links to display
        var start = page - links > 0 ? page - links : 1;
        var end = page + links < last ? page + links : last;
 
        var html = "<div class='text-center'>" +
                         "<div aria-label='Page navigation' class='pagination-area'>" +
                         "<ul>";
        if (page == 1)
        {
            html       += "<li class='disabled'><a><i class='fa fa-angle-left'></i></a></li>";  
        }
        else
        {
            html       += $"<li ><a href='/{controler}/{action}?page={page - 1}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'><i class='fa fa-angle-left'></i></a></li>";

        }

        if (start > 1 ) {
            html   += $"<li><a href='/{controler}/{action}?page=1&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>First</a></li>";
            html   += "<li class='page-item disabled'><span class='no-border'>...</span></li>";
        }
 
        for ( var i = start ; i <= end; i++ ) {
            if (page == i)
            {
                html  += $"<li class='active'><a onclick='return false;'>{i}</a></li>";
            }
            else
            {
                if (i == 1)
                {
                    html  += $"<li><a href='/{controler}/{action}?page=1&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>First</a></li>";  
                }
                else 
                {
                    if (last == i && last > 1)
                    {
                        html  += $"<li><a href='/{controler}/{action}?page={i}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>Last</a></li>";   
                    }
                    else
                    {
                        if (last > 1)
                        {
                            html  += $"<li><a href='/{controler}/{action}?page={i}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>{i}</a></li>";  
                        }
                    }
                }
                
            }
        }
 
        if (end < last) {
            html   += "<li class='page-item disabled'><span class='no-border'>...</span></li>";
            html   += $"<li><a href='/{controler}/{action}?page={last}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>Last</a></li>";
        }
        
        if (page == last)
        {
            html   += "<li class='disabled'><a><i class='fa fa-angle-right'></i></a></li>";
        }
        else
        {
            html   += $"<li><a href='/{controler}/{action}?page={page + 1}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'><i class='fa fa-angle-right'></i></a></li>";
        }
         html   += "</ul>" +
                   "</div>" +
                   "</div";
 
        return new HtmlString(html);
    } 
        public static IHtmlContent PaginateCustomUrl(this IHtmlHelper htmlHelper, int totalCount, int page, int pageSize, string url, string myParams = null)
    {
        
        var last = (int)Math.Ceiling(Convert.ToDouble(totalCount) / pageSize);
        const int links = 7; //Total links to display
        var start = page - links > 0 ? page - links : 1;
        var end = page + links < last ? page + links : last;
 
        var html = "<div class='text-center'>" +
                         "<div aria-label='Page navigation' class='pagination-area'>" +
                         "<ul>";
        if (page == 1)
        {
            html       += "<li class='disabled'><a><i class='fa fa-angle-left'></i></a></li>";  
        }
        else
        {
            html       += $"<li ><a href='/{url}?page={page - 1}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'><i class='fa fa-angle-left'></i></a></li>";

        }

        if (start > 1 ) {
           // html   += $"<li><a href='/{url}?page=1&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>First</a></li>";
            html   += $"<li><a href='/{url}?page=1&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'><i class='fa fa-arrow-double-left'></i></a></li>";
            html   += "<li class='page-item disabled'><span class='no-border'>...</span></li>";
        }
 
        for ( var i = start ; i <= end; i++ ) {
            if (page == i)
            {
                html  += $"<li class='active'><a onclick='return false;'>{i}</a></li>";
            }
            else
            {
                if (i == 1)
                {
                    html  += $"<li><a href='/{url}?page=1&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>First</a></li>";  
                }
                else 
                {
                    if (last == i && last > 1)
                    {
                        html  += $"<li><a href='/{url}?page={i}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>Last</a></li>";   
                    }
                    else
                    {
                        if (last > 1)
                        {
                            html  += $"<li><a href='/{url}?page={i}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>{i}</a></li>";  
                        }
                    }
                }
                
            }
        }
 
        if (end < last) {
            html   += "<li class='page-item disabled'><span class='no-border'>...</span></li>";
            html   += $"<li><a href='/{url}?page={last}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>Last</a></li>";
        }
        
        if (page == last)
        {
            html   += "<li class='disabled'><a><i class='fa fa-angle-right'></i></a></li>";
        }
        else
        {
            html   += $"<li><a href='/{url}?page={page + 1}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'><i class='fa fa-angle-right'></i></a></li>";
        }
         html   += "</ul>" +
                   "</div>" +
                   "</div";
        
        /*var last = (int)Math.Ceiling(Convert.ToDouble(totalCount) / pageSize);
        const int links = 7; //Total links to display
        var start = page - links > 0 ? page - links : 1;
        var end = page + links < last ? page + links : last;
 
        var html = "<div class='text-center'>" +
                         "<div aria-label='Page navigation' class='pagination-area'>" +
                         "<ul>";
        if (page == 1)
        {
            html       += "<li class='disabled'><a><i class='fa fa-angle-left'></i></a></li>";  
        }
        else
        {
            html       += $"<li ><a href='/{url}?page={page - 1}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'><i class='fa fa-angle-left'></i></a></li>";

        }

        if (start > 1 ) {
            html   += $"<li><a href='/{url}?page=1&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>First</a></li>";
            html   += "<li class='page-item disabled'><span class='no-border'>...</span></li>";
        }
 
        for ( var i = start ; i <= end; i++ ) {
            if (page == i)
            {
                html  += $"<li class='active'><a onclick='return false;'>{i}</a></li>";
            }
            else
            {
                if (i == 1)
                {
                    html  += $"<li><a href='/{url}?page=1&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>First</a></li>";  
                }
                else 
                {
                    if (last == i && last > 1)
                    {
                        html  += $"<li><a href='/{url}?page={i}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>Last</a></li>";   
                    }
                    else
                    {
                        if (last > 1)
                        {
                            html  += $"<li><a href='/{url}?page={i}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>{i}</a></li>";  
                        }
                    }
                }
                
            }
        }
 
        if (end < last) {
            html   += "<li class='page-item disabled'><span class='no-border'>...</span></li>";
            html   += $"<li><a href='/{url}?page={last}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>Last</a></li>";
        }
        
        if (page == last)
        {
            html   += "<li class='disabled'><a><i class='fa fa-angle-right'></i></a></li>";
        }
        else
        {
            html   += $"<li><a href='/{url}?page={page + 1}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'><i class='fa fa-angle-right'></i></a></li>";
        }
         html   += "</ul>" +
                   "</div>" +
                   "</div";*/
 
        return new HtmlString(html);
    } 
        
        public static IHtmlContent PaginateAdmin(this IHtmlHelper htmlHelper, int totalCount, int page, int pageSize, [AspMvcController] string controler, [AspMvcAction] string action, [CanBeNull] string myParams = null)
    {
        
        var last       = (int)Math.Ceiling(Convert.ToDouble(totalCount) / pageSize);
        const int links = 7; //Total links to display
        var start      = page - links > 0 ? page - links : 1;
        var end        = page + links < last ? page + links : last;
 
        var html       = "<nav aria-label='Page navigation' class='text-center'>" +
                         "<ul class='pagination justify-content-center'>";
        var myClass    = page == 1 ? "disabled" : "";
        if (page == 1)
        {
            html += $"<li class='page-item {myClass}'><a>Previous</a></li>";
        }
        else
        {
            html += $"<li class='page-item {myClass}'><a href='/{controler}/{action}?page={page - 1}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>Previous</a></li>";
        }

        if (start > 1 ) {
            html   += $"<li><a href='/{controler}/{action}?page=1&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>First</a></li>";
            html   += "<li class='page-item disabled'><span class='no-border'>...</span></li>";
        }
 
        for ( var i = start ; i <= end; i++ ) {
            if (page == i)
            {
                html  += $"<li class='page-item active'><span>{i}</span></li>";
            }
            else
            {
                if (i == 1)
                {
                    html  += $"<li><a href='/{controler}/{action}?page=1&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>First</a></li>";  
                }else if (last == i)
                {
                    html  += $"<li><a href='/{controler}/{action}?page={i}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>Last</a></li>";   
                }
                else
                {
                    html  += $"<li><a href='/{controler}/{action}?page={i}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>{i}</a></li>";
                }
            }
        }
 
        if ( end < last ) {
            html   += "<li class='page-item disabled'><span class='no-border'>...</span></li>";
            html   += $"<li><a href='/{controler}/{action}?page={last}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>Last</a></li>";
        }
        
         myClass = page == last ? "disabled" : "";
        if (page == last)
        {
            html   += $"<li class='page-item {myClass}'><a>Next</a></li>";
        }
        else
        {
            html   += $"<li class='page-item {myClass}'><a href='/{controler}/{action}?page={page + 1}&pageSize={pageSize}{(!string.IsNullOrEmpty(myParams) ? "&" + myParams : "")}'>Next</a></li>";
        }
         html   += "</ul>" +
                   "</nav>";
        
        //Todo: Paging display area
        var returnHtml = "<div class='pager'>";
        returnHtml += html;
        returnHtml += "<div class='text-right' style='margin-right: 30px;'>";
        var firstItemOnPage = pageSize * (page - 1) + 1;
        var lastItemOnPage = firstItemOnPage + (pageSize - 1);
        lastItemOnPage = lastItemOnPage < totalCount ? lastItemOnPage : totalCount;
        var totalPages = Math.Ceiling((double) totalCount / pageSize);
        returnHtml += $"Showing {firstItemOnPage} to {lastItemOnPage} of {totalCount} ({totalPages} Pages)";
        returnHtml += "</div>";
         returnHtml += " </div>";
        return new HtmlString(returnHtml);
    }
    }
}