﻿using System;

namespace IContestPro.Web.Helpers
{
    public static class DateTimeExtension
    {
        public static string ToAppDateString(this DateTime dateTime)
        {
            
            return dateTime.ToString("dd MMMM yyyy");
        }
        public static string ToAppDateTimeString(this DateTime dateTime)
        {
            return dateTime.ToString("dd MMMM yyyy hh:h:mm:ss tt");
        }
    }
}