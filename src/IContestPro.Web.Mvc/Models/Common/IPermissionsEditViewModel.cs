﻿using System.Collections.Generic;
using IContestPro.Roles.Dto;

namespace IContestPro.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}