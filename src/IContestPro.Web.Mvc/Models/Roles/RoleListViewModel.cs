﻿using System.Collections.Generic;
using IContestPro.Roles.Dto;

namespace IContestPro.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleListDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
