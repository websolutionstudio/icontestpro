using System.ComponentModel.DataAnnotations;
using Abp.Auditing;
using Abp.Authorization.Users;

namespace IContestPro.Web.Models.Account
{
    public class RegisterViewModel
    {
      

        [Required]
        [StringLength(50)]
        public string NickName { get; set; }
        
        [Required]
        [StringLength(20)]
        public string PhoneNumber { get; set; }

        
        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        [Required]
        public string CountryId { get; set; }
        public int? StateId { get; set; }
        public long? ReferredUserId { get; set; }

        
        [Required]
        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
        [DisableAuditing]
        public string Password { get; set; }
        
        
        
        

        public bool IsExternalLogin { get; set; }

        public string ExternalLoginAuthSchema { get; set; }

        /*public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!UserName.IsNullOrEmpty())
            {
                var emailRegex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
                if (!UserName.Equals(EmailAddress) && emailRegex.IsMatch(UserName))
                {
                    yield return new ValidationResult("Username cannot be an email address unless it's the same as your email address!");
                }
            }
        }*/
    }
}
