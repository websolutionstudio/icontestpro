﻿using System.ComponentModel.DataAnnotations;

namespace IContestPro.Web.Models.Account
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }
    }
}