using System.Collections.Generic;
using IContestPro.Roles.Dto;
using IContestPro.Users.Dto;

namespace IContestPro.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
