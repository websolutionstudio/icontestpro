﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace IContestPro.Web.Startup
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //InitLocalWebHost(args).Run();
            BuildServerWebHost(args).Run();
        }

        public static IWebHost BuildServerWebHost(string[] args)
        {
            /*var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("hosting.json", optional: true)
                .Build();
            return new WebHostBuilder()
                .UseKestrel()
                .UseConfiguration(config)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseUrls("http://*:80") // listen on port 5000 on all network interfaces
                .UseSetting("https_port", "5001")
                .UseStartup<Startup>()
                .Build();*/
           return WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseUrls("http://*:5000") // listen on port 5000 on all network interfaces
                .UseStartup<Startup>()
                .Build();
          
        }
        public static IWebHost InitLocalWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
        }
    }
}
