﻿using System;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Castle.Facilities.Logging;
using Abp.AspNetCore;
using Abp.Castle.Logging.Log4Net;
using IContestPro.Authentication.JwtBearer;
using IContestPro.Configuration;
using IContestPro.Identity;
using IContestPro.Web.Resources;
using Abp.AspNetCore.SignalR.Hubs;
using Hangfire;
using IContestPro.EntityFrameworkCore;
using IContestPro.Logging;
using IContestPro.Web.Controllers;
using IContestPro.Web.Helpers.AlertExtension;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;

namespace IContestPro.Web.Startup
{
    public class Startup
    {
        private readonly IConfigurationRoot _appConfiguration;
        private readonly ILoggingAppService _loggingAppService;

        public Startup(IHostingEnvironment env)
        {
            _appConfiguration = env.GetAppConfiguration();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
           
            // MVC
            services.AddMvc(
                options => options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute())
            ).AddApplicationPart(typeof(ContestController).Assembly);

            IdentityRegistrar.Register(services);
            AuthConfigurer.Configure(services, _appConfiguration);

            services.AddScoped<IWebResourceManager, WebResourceManager>();

            services.AddSignalR();
            services.AddSingleton<IAlertManagerExtension, AlertListCustom>();
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            });
            services.Configure<DataProtectionTokenProviderOptions>(options =>
            {
                options.TokenLifespan = TimeSpan.FromHours(3); // .FromDays(1) ...
            });
            services.AddHangfire(config =>
            {
                config.UseSqlServerStorage(_appConfiguration.GetConnectionString("Default"));
            });
           
            // Configure Abp and Dependency Injection
            services.Configure<ConnectionStrings>(_appConfiguration.GetSection("ConnectionStrings"));
            services.AddDbContext<IContestProDbContext>(ServiceLifetime.Transient);
            services.AddMemoryCache();
            return services.AddAbp<IContestProWebMvcModule>(
                // Configure Log4Net logging
                options => options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig("log4net.config")
                )
            );
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            //VPS server ngix or apache forwarder
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            app.UseAbp(); // Initializes ABP framework.

            app.UseExceptionHandler(errorApp =>
            {
                
                errorApp.Run(
                    async context =>
                {
                    var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
                    _loggingAppService.LogException(errorFeature.Error);
                    // the IsTrusted() extension method doesn't exist and
                    // you should implement your own as you may want to interpret it differently
                    // i.e. based on the current principal
 
                    /*var problemDetails = new ProblemDetails
                    {
                        Instance = $"urn:myorganization:error:{Guid.NewGuid()}"
                    };
 
                    if (exception is BadHttpRequestException badHttpRequestException)
                    {
                        problemDetails.Title = "Invalid request";
                        problemDetails.Status = (int)typeof(BadHttpRequestException).GetProperty("StatusCode", 
                            BindingFlags.NonPublic | BindingFlags.Instance).GetValue(badHttpRequestException);
                        problemDetails.Detail = badHttpRequestException.Message;
                    }
                    else
                    {
                        problemDetails.Title = "An unexpected error occurred!";
                        problemDetails.Status = 500;
                        problemDetails.Detail = exception.Message.ToString();
                    }
 
                    // log the exception etc..
 
                    context.Response.StatusCode = problemDetails.Status.Value;*/
                   // context.Response.WriteJson(problemDetails, "application/problem+json");
                });
            });
            app.UseDeveloperExceptionPage();

            /*if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");

            }*/
            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseJwtTokenMiddleware();

            app.UseSignalR(routes =>
            {
                routes.MapHub<AbpCommonHub>("/signalr");
            });
           // app.UseHangfireServer();
            app.UseHangfireServer(
                new BackgroundJobServerOptions
                {
                    WorkerCount = 1
                });
            app.UseHangfireDashboard("/hangfirebackend"/*, new DashboardOptions
            {
                Authorization = new[] { new AbpHangfireAuthorizationFilter(PermissionNames.Pages_Admin) }
            }*/);
            app.UseMvc(routes =>
            {
                /*routes.MapRoute(
                    name: "Contests",
                    template: "{controller=Contest}/{action=Index}/{permalink?}"
                );*/
                  routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}"
                    );
                //Contest routes
                routes.MapRoute(
                    name: "defaultWithArea",
                    template: "{area}/{controller=Contest}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "ContestDetails",
                    template: "Contest/{id}",
                    defaults: new { controller = "Contest", action = "MainDetails" }
                   
                );
                //Contest entries
                routes.MapRoute(
                    name: "ContestActiveEntries",
                    template: "Contest/Entries/Active",
                    defaults: new { controller = "ContestEntry", action = "Active" }
                   
                );
                routes.MapRoute(
                    name: "ContestEntries",
                    template: "Contest/Entries/{id}",
                    defaults: new { controller = "ContestEntry", action = "Entries" }
                   
                );
                routes.MapRoute(
                    name: "ContestEntryVotes",
                    template: "Contest/Entry/{id}/Votes",
                    defaults: new { controller = "ContestEntry", action = "Votes" }
                   
                );
                routes.MapRoute(
                    name: "ContestEntriesJoin",
                    template: "Contest/Entries/Join/{id}",
                    defaults: new { controller = "ContestEntry", action = "Create" }
                   
                );
                routes.MapRoute(
                    name: "ContestEntryDetails",
                    template: "Contest/Entry/{id}",
                    defaults: new { controller = "ContestEntry", action = "Details" }
                );
                
                //Sponsor
                routes.MapRoute(
                    name: "SponsorInfo",
                    template: "Sponsor/{id}",
                    defaults: new { controller = "Sponsor", action = "Details" }
                   
                );
                routes.MapRoute(
                    name: "UserProfile",
                    template: "User/{id}",
                    defaults: new { controller = "Users", action = "Details" }
                   
                );
            });
            
            
            //Todo: Initial background jobs
            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.NotifyExpiringContestsJobId, x => x.NotifyExpiringContests(), Cron.Daily);
            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.DisableExpiredContestsJobId, x => x.DisableExpiredContests(), Cron.Daily);
            
            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.NotifyExpiringHomePageBannerJobId, x => x.NotifyExpiringHomePageBanner(), Cron.Daily);
            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.DisableExpiredHomePageBannerJobId, x => x.DisableExpiredHomePageBanner(), Cron.Daily);

            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.NotifyExpiringTopBannerJobId, x => x.NotifyExpiringTopBanner(), Cron.Daily);
            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.DisableExpiredTopBannerJobId, x => x.DisableExpiredTopBanner(), Cron.Daily);
            
            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.NotifyExpiringFeaturedContestJobId, x => x.NotifyExpiringFeaturedContests(), Cron.Daily);
            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.DisableExpiredFeaturedContestJobId, x => x.DisableExpiredFeaturedContests(), Cron.Daily);

            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.NotifyExpiringFeaturedContestEntryJobId, x => x.NotifyExpiringFeaturedContestEntries(), Cron.Daily);
            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.DisableExpiredFeaturedContestEntryJobId, x => x.DisableExpiredFeaturedContestEntries(), Cron.Daily);
            
            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.NotifyExpiringAdvertBox1JobId, x => x.NotifyExpiringAdvertBox1(), Cron.Daily);
            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.DisableExpiredAdvertBox1JobId, x => x.DisableExpiredAdvertBox1(), Cron.Daily);
 
            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.NotifyExpiringAdvertBox2JobId, x => x.NotifyExpiringAdvertBox2(), Cron.Daily);
            RecurringJob.AddOrUpdate<BackgroundJobManager>(AppConsts.DisableExpiredAdvertBox2JobId, x => x.DisableExpiredAdvertBox2(), Cron.Daily);

        }
    }
    public static class HttpExtensions
    {
        private static readonly JsonSerializer Serializer = new JsonSerializer 
        {
            NullValueHandling = NullValueHandling.Ignore
        };
 
        public static void WriteJson<T>(this HttpResponse response, T obj, string contentType = null)
        {
            response.ContentType = contentType ?? "application/json";
            using (var writer = new HttpResponseStreamWriter(response.Body, Encoding.UTF8))
            {
                using (var jsonWriter = new JsonTextWriter(writer))
                {
                    jsonWriter.CloseOutput = false;
                    jsonWriter.AutoCompleteOnClose = false;
 
                    Serializer.Serialize(jsonWriter, obj);
                }
            }
        }
    }
}
