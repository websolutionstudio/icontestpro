﻿using Abp.Application.Navigation;
using Abp.Localization;
using IContestPro.Authorization;

namespace IContestPro.Web.Startup
{
    /// <summary>
    /// This class defines menus for the application.
    /// </summary>
    public class IContestProNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(
                    new MenuItemDefinition(
                        PageNames.Home,
                        L("Dashboard"),
                        url: "/Home/Dashboard",
                        icon: "fa-home",
                        requiresAuthentication: true
                    )
                ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Users,
                            L("Users"),
                            url: "Users",
                            icon: "fa-users",
                            requiredPermissionName: PermissionNames.Pages_Admin
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Manage,
                        L("Account"),
                        url: "Manage",
                        requiresAuthentication: true,
                        icon: "fa-user"
                    )
                    ).AddItem( // Menu items below is just for demonstration!
                        new MenuItemDefinition(
                            PageNames.Contests,
                            L("Contests"),
                            icon: "fa-list"
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ContestIndex,
                                L("Contests"),
                                url: "Contest/Contests",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ContestTypes,
                                L("ContestTypes"),
                                url: "Common/Types",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ContestFee,
                                L("ContestFee"),
                                url: "Common/ContestFee",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ContestCategories,
                                L("ContestCategories"),
                                url: "Common/Categories",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.VoteTypes,
                                L("VoteTypes"),
                                url: "Common/VoteTypes",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Periodic,
                                L("Periodic"),
                                url: "Common/Periodic",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.PaymentTypes,
                                L("PaymentTypes"),
                                url: "Common/PaymentTypes",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.PaymentHistories,
                                L("PaymentHistories"),
                                url: "PaymentHistories/Index",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.VoterRewardPoint,
                                L("VoterRewardPoints"),
                                url: "Common/VoterRewardPoints",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ContestEntryTypes,
                                L("ContestEntryTypes"),
                                url: "Common/EntryTypes",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ContestPersonal,
                                L("ContestPersonal"),
                                url: "Contest/Personal",
                                requiresAuthentication: true
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.PersonalEntries,
                                L("PersonalEntries"),
                                url: "ContestEntry/PersonalEntries",
                                requiresAuthentication: true
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ContestIndex,
                                L("ContestCreate"),
                                url: "Contest/Create",
                                requiresAuthentication: true
                            )
                        )
                    
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Sponsor,
                        L("BecomeASponsor"),
                        url: "Sponsor",
                        requiresAuthentication: true,
                        icon: "fa-users"
                    )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Roles,
                            L("Roles"),
                            url: "Roles",
                            requiredPermissionName: PermissionNames.Pages_Roles,
                            icon: "fa-users"
                        )
                    ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Countries,
                                L("Countries"),
                                url: "Countries",
                                requiredPermissionName: PermissionNames.Pages_Admin,
                                icon: "fa-user"
                            )
                        ).AddItem( // Menu items below is just for demonstration!
                        new MenuItemDefinition(
                            PageNames.Wallet,
                            L("Wallet"),
                            icon: "fa-money"
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Wallet,
                                L("Wallet"),
                                url: "Wallet/Index",
                                requiresAuthentication: true
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.WalletDeposit,
                                L("Deposit"),
                                url: "Wallet/Deposit",
                                requiresAuthentication: true
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.WalletWithdraw,
                                L("Withdraw"),
                                url: "Wallet/Withdraw",
                                requiresAuthentication: true
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.WalletTransfer,
                                L("Transfer"),
                                url: "Wallet/Transfer",
                                requiresAuthentication: true
                            )
                        )
                    ).AddItem( // Menu items below is just for demonstration!
                    new MenuItemDefinition(
                        PageNames.Banks,
                        L("Banks"),
                        icon: "fa-home"
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Banks,
                            L("Banks"),
                            url: "Bank/Index",
                            requiredPermissionName: PermissionNames.Pages_Admin
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.BankDetails,
                            L("BankDetails"),
                            url: "Bank/BankDetails",
                            requiresAuthentication: true
                        )
                    )
                ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Referral,
                            L("Referral"),
                            url: "Referral",
                            requiresAuthentication: true,
                            icon: "fa-users"
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.CodeSystem,
                            L("CodeSystem"),
                            url: "CodeSystem",
                            requiredPermissionName: PermissionNames.Pages_Code_System,
                            icon: "fa-code"
                        )
                    ).AddItem( // Menu items below is just for demonstration!
                        new MenuItemDefinition(
                            PageNames.Advert,
                            L("Advert"),
                            icon: "fa-money"
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.HomePageBanner,
                                L("HomePageBanner"),
                                url: "HomePageBanners/Index",
                                requiresAuthentication: true
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.TopBanner,
                                L("TopBanner"),
                                url: "TopBanners/Index",
                                requiresAuthentication: true
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.FeaturedContest,
                                L("FeaturedContests"),
                                url: "FeaturedContests/Index",
                                requiresAuthentication: true
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.FeaturedContestEntry,
                                L("FeaturedContestEntries"),
                                url: "FeaturedContestEntries/Index",
                                requiresAuthentication: true
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.AdvertBoxOne,
                                L("AdvertBoxOne"),
                                url: "AdvertBoxOne/Index",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.AdvertBoxTwo,
                                L("AdvertBoxTwo"),
                                url: "AdvertBoxTwo/Index",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.AdvertPrices,
                                L("AdvertPricing"),
                                url: "Advertprices/Index",
                                requiredPermissionName: PermissionNames.Pages_Admin
                            )
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.VoterRecord,
                            L("VoterLeaderBoard"),
                            url: "VoterLeaderBoard/AdminIndex",
                            requiredPermissionName: PermissionNames.Pages_Admin,
                            icon: "fa-list"
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.LeaderBoard,
                            L("LeaderBoard"),
                            url: "LeaderBoard/Index",
                            requiresAuthentication: true,
                            icon: "fa-envelope-square"
                        )
                    ).AddItem( // Menu items below is just for demonstration!
                        new MenuItemDefinition(
                            PageNames.Report,
                            L("Report"),
                            icon: "fa-line-chart"
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ReportUserList,
                                L("UserList"),
                                url: "Report/User",
                                requiredPermissionName: PermissionNames.Pages_Report_UserList
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.Transactions,
                                L("Transactions"),
                                url: "Report/Transactions",
                                requiredPermissionName: PermissionNames.Pages_Report_Transactions
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.ContestStatistics,
                                L("ContestStatistics"),
                                url: "Report/ContestStatistics",
                                requiredPermissionName: PermissionNames.Pages_Report_Contest_Statistic
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.VoteStatistics,
                                L("VoteStatistics"),
                                url: "Report/VoteStatistics",
                                requiredPermissionName: PermissionNames.Pages_Report_Vote_Statistic
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                                PageNames.AccountPosition,
                                L("AccountPosition"),
                                url: "Report/AccountPosition",
                                requiredPermissionName: PermissionNames.Pages_Report_Account_Position
                            )
                        )
                    ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Messages,
                        L("Messages"),
                        url: "Messages",
                        icon: "fa-envelope",
                        requiresAuthentication: true
                    )
                ).AddItem(
                        new MenuItemDefinition(
                            PageNames.SupportTicket,
                            L("SupportTicket"),
                            url: "SupportTicket",
                            requiresAuthentication: true,
                            icon: "fa-address-card"
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Disputes,
                            L("Disputes"),
                            url: "Disputes",
                            requiresAuthentication: true,
                            icon: "fa-users"
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.Settings,
                            L("Settings"),
                            url: "Settings",
                            requiresAuthentication: true,
                            icon: "fa-cog"
                        )
                    )
                .AddItem(
                    new MenuItemDefinition(
                        PageNames.Tenants,
                        L("Tenants"),
                        url: "Tenants",
                        icon: "fa-home",
                        requiredPermissionName: PermissionNames.Pages_Tenants
                    )
                ).AddItem( // Menu items below is just for demonstration!
                    new MenuItemDefinition(
                        PageNames.EmailTemplates,
                        L("EmailTemplates"),
                        url: "EmailTemplates",
                        icon: "fa-envelop",
                        requiredPermissionName: PermissionNames.Pages_Admin
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.EmailTemplates,
                            L("Templates"),
                            url: "EmailTemplates/Index",
                            requiredPermissionName: PermissionNames.Pages_Admin
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.DefaultEmailTemplate,
                            L("DefaultEmailTemplate"),
                            url: $"EmailTemplates/CreateEditTemplate?type={AppConsts.EmailTemplateDefaultEmailTemplate}",
                            requiredPermissionName: PermissionNames.Pages_Admin
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.EmailTemplatesNewUser,
                            L("NewUserCreation"),
                            url: $"EmailTemplates/CreateEditTemplate?type={AppConsts.EmailTemplateNewUserCreation}",
                            requiredPermissionName: PermissionNames.Pages_Admin
                        )
                    ).AddItem(
                        new MenuItemDefinition(
                            PageNames.EmailTemplatesResetPassword,
                            L("ResetPasswordEmailTemplate"),
                            url: $"EmailTemplates/CreateEditTemplate?type={AppConsts.EmailTemplateResetPasswordEmailTemplate}",
                            requiredPermissionName: PermissionNames.Pages_Admin
                        )
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Blogs,
                        L("Blogs"),
                        url: "Blogs/IndexAdmin",
                        requiredPermissionName: PermissionNames.Pages_Admin
                    )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, IContestProConsts.LocalizationSourceName);
        }
    }
}
