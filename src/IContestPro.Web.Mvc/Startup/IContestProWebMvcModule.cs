﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using IContestPro.Configuration;

namespace IContestPro.Web.Startup
{
    [DependsOn(typeof(IContestProWebCoreModule))]
    public class IContestProWebMvcModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public IContestProWebMvcModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<IContestProNavigationProvider>();
           
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(IContestProWebMvcModule).GetAssembly());
        }
        public override void PostInitialize()
        {
       }
    }
}
