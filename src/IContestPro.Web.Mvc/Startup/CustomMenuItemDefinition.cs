﻿using Abp.Application.Features;
using Abp.Application.Navigation;
using Abp.Authorization;
using Abp.Localization;

namespace IContestPro.Web.Startup
{
    public class CustomMenuItemDefinition : MenuItemDefinition
    {
        /// <summary>
       ///This is the role that allows a user to view a menu.
       ///Note: Role is considered only if requiredPermissionName = null
        /// </summary>
        public string[] RequireRoles { get; set; }
        public CustomMenuItemDefinition(string name, ILocalizableString displayName, string icon = null, string url = null,
            bool requiresAuthentication = false, string requiredPermissionName = null, int order = 0, 
            object customData = null, IFeatureDependency featureDependency = null, 
            string target = null, bool isEnabled = true, bool isVisible = true, 
            IPermissionDependency permissionDependency = null, string[] requireRoles = null) 
            : base(name, displayName, icon, url, requiresAuthentication,
            requiredPermissionName, order, customData, featureDependency,
            target, isEnabled, isVisible, permissionDependency)
        {
            this.RequireRoles = requireRoles;
        }
    }
}