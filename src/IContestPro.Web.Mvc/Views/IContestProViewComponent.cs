﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace IContestPro.Web.Views
{
    public abstract class IContestProViewComponent : AbpViewComponent
    {
        protected IContestProViewComponent()
        {
            LocalizationSourceName = IContestProConsts.LocalizationSourceName;
        }
    }
}
