﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace IContestPro.Web.Views
{
    public abstract class IContestProRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected IContestProRazorPage()
        {
            LocalizationSourceName = IContestProConsts.LocalizationSourceName;
        }
    }
}
