﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Views.Shared.Components.ProfileImage
{
    public class ProfileImageViewComponent : IContestProViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}
