﻿using System.Threading.Tasks;
using IContestPro.FeaturedContestEntry;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Views.Shared.Components.FeaturedContestEntryAdvert
{
    public class FeaturedContestEntryAdvertViewComponent : IContestProViewComponent
    {
        private readonly IFeaturedContestEntryAppService _featuredContestEntryAppService;

        public FeaturedContestEntryAdvertViewComponent(IFeaturedContestEntryAppService featuredContestEntryAppService)
        {
            _featuredContestEntryAppService = featuredContestEntryAppService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var items = await _featuredContestEntryAppService.GetRandomLive();
            return View(items);
            //return View();
            
        }
    }
}
