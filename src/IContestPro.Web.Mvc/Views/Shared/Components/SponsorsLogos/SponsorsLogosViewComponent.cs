﻿using System.Threading.Tasks;
using IContestPro.AdvertHomePageBanner;
using IContestPro.Sponsor;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Views.Shared.Components.SponsorsLogos
{
    public class SponsorsLogosViewComponent : IContestProViewComponent
    {
        private readonly ISponsorAppService _sponsorAppService;

        public SponsorsLogosViewComponent(ISponsorAppService sponsorAppService)
        {
            _sponsorAppService = sponsorAppService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var items = await _sponsorAppService.GetRandomSponsors();
           return View(items);
           // return View();
        }
    }
}
