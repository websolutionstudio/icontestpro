﻿using System.Threading.Tasks;
using IContestPro.FeaturedContest;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Views.Shared.Components.FeaturedContestsAdvert
{
    public class FeaturedContestsAdvertViewComponent : IContestProViewComponent
    {
        private readonly IFeaturedContestAppService _featuredContestAppService;

        public FeaturedContestsAdvertViewComponent(IFeaturedContestAppService featuredContestAppService)
        {
            _featuredContestAppService = featuredContestAppService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var items = await _featuredContestAppService.GetRandomLive();
            return View(items);
           // return View();
        }
    }
}
