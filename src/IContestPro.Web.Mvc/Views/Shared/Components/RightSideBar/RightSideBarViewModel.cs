﻿using IContestPro.Configuration.Ui;

namespace IContestPro.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
