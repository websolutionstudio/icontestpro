﻿using System.Threading.Tasks;
using IContestPro.AdvertHomePageBanner;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Views.Shared.Components.HomePageBannerAdvert
{
    public class HomePageBannerAdvertViewComponent : IContestProViewComponent
    {
        private readonly IAdvertHomePageBannerAppService _advertHomePageBannerAppService;

        public HomePageBannerAdvertViewComponent(IAdvertHomePageBannerAppService advertHomePageBannerAppService)
        {
            _advertHomePageBannerAppService = advertHomePageBannerAppService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var items = await _advertHomePageBannerAppService.GetRandomLive();
           return View(items);
           // return View();
        }
    }
}
