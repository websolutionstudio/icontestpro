﻿using System.Threading.Tasks;
using IContestPro.Blog;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Views.Shared.Components.BlogTickers
{
    public class BlogTickersViewComponent : IContestProViewComponent
    {
        private readonly IBlogAppService _blogAppService;

        public BlogTickersViewComponent(IBlogAppService blogAppService)
        {
            _blogAppService = blogAppService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var items = await _blogAppService.GetBlogTickersAsync(pageSize: 10);
            return View(items);
           // return View();
        }
    }
}
