﻿using System.Threading.Tasks;
using IContestPro.AdvertTopBanner;
using Microsoft.AspNetCore.Mvc;

namespace IContestPro.Web.Views.Shared.Components.TopBannerAdvert
{
    public class TopBannerAdvertViewComponent : IContestProViewComponent
    {
        private readonly IAdvertTopBannerAppService _advertTopBannerAppService;

        public TopBannerAdvertViewComponent(IAdvertTopBannerAppService advertTopBannerAppService)
        {
            _advertTopBannerAppService = advertTopBannerAppService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
           var items = await _advertTopBannerAppService.GetRandomLive();
            return View(items);
            //return View();
        }
    }
}
