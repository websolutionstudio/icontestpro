-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2019 at 02:53 AM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `IContestProDb`
--

-- --------------------------------------------------------

--
-- Table structure for table `AbpAuditLogs`
--

CREATE TABLE IF NOT EXISTS `AbpAuditLogs` (
  `Id` bigint(20) NOT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) DEFAULT NULL,
  `ServiceName` varchar(256) DEFAULT NULL,
  `MethodName` varchar(256) DEFAULT NULL,
  `Parameters` varchar(1024) DEFAULT NULL,
  `ExecutionTime` datetime(6) NOT NULL,
  `ExecutionDuration` int(11) NOT NULL,
  `ClientIpAddress` varchar(64) DEFAULT NULL,
  `ClientName` varchar(128) DEFAULT NULL,
  `BrowserInfo` varchar(512) DEFAULT NULL,
  `Exception` varchar(2000) DEFAULT NULL,
  `ImpersonatorUserId` bigint(20) DEFAULT NULL,
  `ImpersonatorTenantId` int(11) DEFAULT NULL,
  `CustomData` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpAuditLogs`
--

INSERT INTO `AbpAuditLogs` (`Id`, `TenantId`, `UserId`, `ServiceName`, `MethodName`, `Parameters`, `ExecutionTime`, `ExecutionDuration`, `ClientIpAddress`, `ClientName`, `BrowserInfo`, `Exception`, `ImpersonatorUserId`, `ImpersonatorTenantId`, `CustomData`) VALUES
(1, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:13.542492', 5, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(2, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:13.542492', 5, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(3, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:16.496360', 0, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(4, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:16.496360', 0, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(5, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:17.892795', 1, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(6, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:17.960396', 1, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(7, 1, 3, 'IContestPro.Web.Controllers.AccountController', 'Login', '{}', '2019-04-12 02:43:18.034659', 26, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(8, 1, 3, 'IContestPro.Status.StatusAppService', 'GetByNameAsync', '{"name":"Live"}', '2019-04-12 02:43:18.031048', 109, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(9, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:18.367309', 1, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(10, 1, 3, 'IContestPro.LeaderBoard.LeaderBoardAppService', 'GetTopContestEntriesWithHighestVote', '{"displayType":"Lifetime","input":{"skipCount":10,"maxResultCount":10},"totalEntryTake":10}', '2019-04-12 02:43:17.972272', 396, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(11, 1, 3, 'IContestPro.Web.Controllers.LeaderBoardController', 'Index', '{"displayType":"Lifetime","page":2}', '2019-04-12 02:43:17.252723', 1137, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(12, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:18.414075', 1, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(13, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:19.244382', 1, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(14, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:19.309961', 0, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(15, 1, 3, 'IContestPro.Users.UserAppService', 'Get', '{"input":{"id":3}}', '2019-04-12 02:43:19.398132', 208, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(16, 1, 3, 'IContestPro.Sponsor.SponsorAppService', 'GetByUserId', '{"userId":3}', '2019-04-12 02:43:19.612756', 44, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(17, 1, 3, 'IContestPro.Web.Controllers.ManageController', 'Index', '{}', '2019-04-12 02:43:19.351534', 327, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(18, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"CookieKeyHideSponsorNotification"}', '2019-04-12 02:43:19.754626', 1, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(19, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:21.437619', 1, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(20, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:21.534226', 1, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(21, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"CurrentLoginInformations"}', '2019-04-12 02:43:21.617070', 0, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(22, 1, 3, 'IContestPro.Wallet.WalletAppService', 'GetByUserId', '{"userId":3}', '2019-04-12 02:43:21.830501', 178, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(23, 1, 3, 'IContestPro.UserVotePoint.UserVotePointAppService', 'GetByUserId', '{"userId":3}', '2019-04-12 02:43:22.017273', 209, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(24, 1, 3, 'IContestPro.Follow.FollowAppService', 'CountFollowersByUserId', '{"userId":3}', '2019-04-12 02:43:22.233810', 39, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(25, 1, 3, 'IContestPro.Follow.FollowAppService', 'CountFollowingByUserId', '{"userId":3}', '2019-04-12 02:43:22.280377', 23, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(26, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Set', '{"key":"CurrentLoginInformations","value":"f/efQuFM8R306aVltyS7yOgffU9wkhSR364KwdkfeXa5w6tNLrOD8sawTgsqqJY3ojXPyKOiRN8ncWyWIBT9CeGP89wCGU6oJAtoo6QFS/ZF25DwIBqcKhE+MAo7NB1prJkoJRdHBy/T7Rq1MNo8ejDInzSHI88EeXJcDNGFa6pP8LDT4mUm9AJZwRUnqRTxUSkaNtU8wOnocVYaf4pI0gtSHpJZXzAmGc13v0i4HmaKU7VZeEYypCxnqYZrGycJ7cWUgrHISRBva/gZW0Npv2vm8j79CVi7NW6YVyLiBC1WYAYTubpPzWtUboJdFAHleHODX5PGAP2uzS63d8qasQ6pTTj1sc2fRLq7bYt6Xd8+HMzWQEjh/mbV8uaqTAI0f7T2mmQAQZXAwXcXQALV41+z2QX3+hA8FBkKlXriuXjCVN1Q+gWBXHQce5NcDWNiLU3oH0ag9pOJVzqUzYLwBd1oNON9IErlyLezh4oToYKVB2qqANY7mYTz0nED83zPxp0EKV8MjCdV1MFnVAkWonK2aG3YrMMgJMZKa/LsNYSdN56KvaqoHa9zqzVNHw990XjQfipu/LmSK/9DubK2nLm2SRlp+Q4CVjF993CZj9FUb7vV4gk5qL3UAN1lG46ZEPfaPgIkduYXiTeR6JM8HgrY/xjutMArR/O3nBBkTPRB7+nDIa2zCzGL4beMMDvW0EMfxkULIyBlBEJFtRQbl9tDWilCg/Fvo83q+4awuTpz0kJHawrH1kXvdigcci8ZrLc5hAUv+VvvsZ/vBcEeETemwHXpexo0tBu/gS2v9GDjZhodmExr3VihM080f4PfQbVtfdTb1gIhSKNE2873yF2boVTEyQ6I+PB5eBunlzfk1AYU3AHzclutp5lrrhfWMCikWSK6J5wH/st68mCUKqBYb/rXbNxR6I0iXVeQ2aA3DBMbw7FTVSeJnpciaArK4tJqTOERLXMqjJBzgl...', '2019-04-12 02:43:22.472200', 4, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(27, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:26.696281', 1, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(28, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:26.832405', 3, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(29, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:26.900732', 1, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(30, 1, 3, 'IContestPro.Cookie.CookieAppService', 'Get', '{"key":"OpenVoteTokenHash"}', '2019-04-12 02:43:27.014484', 1, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL),
(31, 1, 3, 'IContestPro.Web.Controllers.NotificationsController', 'CountUnread', '{}', '2019-04-12 02:43:27.072802', 89, '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `AbpBackgroundJobs`
--

CREATE TABLE IF NOT EXISTS `AbpBackgroundJobs` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `JobType` varchar(512) NOT NULL,
  `JobArgs` longtext NOT NULL,
  `TryCount` smallint(6) NOT NULL,
  `NextTryTime` datetime(6) NOT NULL,
  `LastTryTime` datetime(6) DEFAULT NULL,
  `IsAbandoned` bit(1) NOT NULL,
  `Priority` tinyint(3) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbpEditions`
--

CREATE TABLE IF NOT EXISTS `AbpEditions` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Name` varchar(32) NOT NULL,
  `DisplayName` varchar(64) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpEditions`
--

INSERT INTO `AbpEditions` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `Name`, `DisplayName`) VALUES
(1, '2019-04-10 17:30:37.091931', NULL, NULL, NULL, b'0', NULL, NULL, 'Standard', 'Standard');

-- --------------------------------------------------------

--
-- Table structure for table `AbpEntityChanges`
--

CREATE TABLE IF NOT EXISTS `AbpEntityChanges` (
  `Id` bigint(20) NOT NULL,
  `ChangeTime` datetime(6) NOT NULL,
  `ChangeType` tinyint(3) unsigned NOT NULL,
  `EntityChangeSetId` bigint(20) NOT NULL,
  `EntityId` varchar(48) DEFAULT NULL,
  `EntityTypeFullName` varchar(192) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbpEntityChangeSets`
--

CREATE TABLE IF NOT EXISTS `AbpEntityChangeSets` (
  `Id` bigint(20) NOT NULL,
  `BrowserInfo` varchar(512) DEFAULT NULL,
  `ClientIpAddress` varchar(64) DEFAULT NULL,
  `ClientName` varchar(128) DEFAULT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `ExtensionData` longtext,
  `ImpersonatorTenantId` int(11) DEFAULT NULL,
  `ImpersonatorUserId` bigint(20) DEFAULT NULL,
  `Reason` varchar(256) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbpEntityPropertyChanges`
--

CREATE TABLE IF NOT EXISTS `AbpEntityPropertyChanges` (
  `Id` bigint(20) NOT NULL,
  `EntityChangeId` bigint(20) NOT NULL,
  `NewValue` varchar(512) DEFAULT NULL,
  `OriginalValue` varchar(512) DEFAULT NULL,
  `PropertyName` varchar(96) DEFAULT NULL,
  `PropertyTypeFullName` varchar(192) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbpFeatures`
--

CREATE TABLE IF NOT EXISTS `AbpFeatures` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `Name` varchar(128) NOT NULL,
  `Value` varchar(2000) NOT NULL,
  `Discriminator` longtext NOT NULL,
  `EditionId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbpLanguages`
--

CREATE TABLE IF NOT EXISTS `AbpLanguages` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `Name` varchar(10) NOT NULL,
  `DisplayName` varchar(64) NOT NULL,
  `Icon` varchar(128) DEFAULT NULL,
  `IsDisabled` bit(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpLanguages`
--

INSERT INTO `AbpLanguages` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `TenantId`, `Name`, `DisplayName`, `Icon`, `IsDisabled`) VALUES
(1, '2019-04-10 17:30:37.646525', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'en', 'English', 'famfamfam-flags gb', b'0'),
(2, '2019-04-10 17:30:37.646916', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'ar', '???????', 'famfamfam-flags sa', b'0'),
(3, '2019-04-10 17:30:37.646918', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'de', 'German', 'famfamfam-flags de', b'0'),
(4, '2019-04-10 17:30:37.646923', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'it', 'Italiano', 'famfamfam-flags it', b'0'),
(5, '2019-04-10 17:30:37.646923', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'fr', 'Français', 'famfamfam-flags fr', b'0'),
(6, '2019-04-10 17:30:37.646925', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'pt-BR', 'Português', 'famfamfam-flags br', b'0'),
(7, '2019-04-10 17:30:37.646925', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'tr', 'Türkçe', 'famfamfam-flags tr', b'0'),
(8, '2019-04-10 17:30:37.646925', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'ru', '???????', 'famfamfam-flags ru', b'0'),
(9, '2019-04-10 17:30:37.646925', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'zh-Hans', '????', 'famfamfam-flags cn', b'0'),
(10, '2019-04-10 17:30:37.646926', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'es-MX', 'Español México', 'famfamfam-flags mx', b'0'),
(11, '2019-04-10 17:30:37.646926', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'nl', 'Nederlands', 'famfamfam-flags nl', b'0'),
(12, '2019-04-10 17:30:37.646926', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'ja', '???', 'famfamfam-flags jp', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `AbpLanguageTexts`
--

CREATE TABLE IF NOT EXISTS `AbpLanguageTexts` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `LanguageName` varchar(10) NOT NULL,
  `Source` varchar(128) NOT NULL,
  `Key` varchar(256) NOT NULL,
  `Value` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbpNotifications`
--

CREATE TABLE IF NOT EXISTS `AbpNotifications` (
  `Id` char(36) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `NotificationName` varchar(96) NOT NULL,
  `Data` longtext,
  `DataTypeName` varchar(512) DEFAULT NULL,
  `EntityTypeName` varchar(250) DEFAULT NULL,
  `EntityTypeAssemblyQualifiedName` varchar(512) DEFAULT NULL,
  `EntityId` varchar(96) DEFAULT NULL,
  `Severity` tinyint(3) unsigned NOT NULL,
  `UserIds` longtext,
  `ExcludedUserIds` longtext,
  `TenantIds` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbpNotificationSubscriptions`
--

CREATE TABLE IF NOT EXISTS `AbpNotificationSubscriptions` (
  `Id` char(36) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `NotificationName` varchar(96) DEFAULT NULL,
  `EntityTypeName` varchar(250) DEFAULT NULL,
  `EntityTypeAssemblyQualifiedName` varchar(512) DEFAULT NULL,
  `EntityId` varchar(96) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbpOrganizationUnits`
--

CREATE TABLE IF NOT EXISTS `AbpOrganizationUnits` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `ParentId` bigint(20) DEFAULT NULL,
  `Code` varchar(95) NOT NULL,
  `DisplayName` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbpPermissions`
--

CREATE TABLE IF NOT EXISTS `AbpPermissions` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `Name` varchar(128) NOT NULL,
  `IsGranted` bit(1) NOT NULL,
  `Discriminator` longtext NOT NULL,
  `RoleId` int(11) DEFAULT NULL,
  `UserId` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpPermissions`
--

INSERT INTO `AbpPermissions` (`Id`, `CreationTime`, `CreatorUserId`, `TenantId`, `Name`, `IsGranted`, `Discriminator`, `RoleId`, `UserId`) VALUES
(1, '2019-04-10 17:30:38.034428', NULL, NULL, 'Pages.Users', b'1', 'RolePermissionSetting', 1, NULL),
(2, '2019-04-10 17:30:38.089940', NULL, NULL, 'Pages.Advert.Update', b'1', 'RolePermissionSetting', 1, NULL),
(3, '2019-04-10 17:30:38.089979', NULL, NULL, 'Pages.Advert.Delete', b'1', 'RolePermissionSetting', 1, NULL),
(4, '2019-04-10 17:30:38.090022', NULL, NULL, 'Pages.Advert.Process', b'1', 'RolePermissionSetting', 1, NULL),
(5, '2019-04-10 17:30:38.090062', NULL, NULL, 'Pages.Advert.Report', b'1', 'RolePermissionSetting', 1, NULL),
(6, '2019-04-10 17:30:38.090103', NULL, NULL, 'Pages.Voterecord', b'1', 'RolePermissionSetting', 1, NULL),
(7, '2019-04-10 17:30:38.090147', NULL, NULL, 'Pages.Voterecord.Create', b'1', 'RolePermissionSetting', 1, NULL),
(8, '2019-04-10 17:30:38.090187', NULL, NULL, 'Pages.Voterecord.Update', b'1', 'RolePermissionSetting', 1, NULL),
(9, '2019-04-10 17:30:38.090229', NULL, NULL, 'Pages.Voterecord.Delete', b'1', 'RolePermissionSetting', 1, NULL),
(10, '2019-04-10 17:30:38.090268', NULL, NULL, 'Pages.LeaderBoard', b'1', 'RolePermissionSetting', 1, NULL),
(11, '2019-04-10 17:30:38.090309', NULL, NULL, 'Pages.OverLeaderBoard', b'1', 'RolePermissionSetting', 1, NULL),
(12, '2019-04-10 17:30:38.090350', NULL, NULL, 'Pages.ContestLeaderBoard', b'1', 'RolePermissionSetting', 1, NULL),
(13, '2019-04-10 17:30:38.090393', NULL, NULL, 'Pages.Voterecord.VoterLeaderBoard', b'1', 'RolePermissionSetting', 1, NULL),
(14, '2019-04-10 17:30:38.090432', NULL, NULL, 'Pages.Report.UserList', b'1', 'RolePermissionSetting', 1, NULL),
(15, '2019-04-10 17:30:38.089898', NULL, NULL, 'Pages.Advert.Create', b'1', 'RolePermissionSetting', 1, NULL),
(16, '2019-04-10 17:30:38.090473', NULL, NULL, 'Pages.Report.Transaction', b'1', 'RolePermissionSetting', 1, NULL),
(17, '2019-04-10 17:30:38.090552', NULL, NULL, 'Pages.Report.ContestStatistic', b'1', 'RolePermissionSetting', 1, NULL),
(18, '2019-04-10 17:30:38.090591', NULL, NULL, 'Pages.Report.VoteStatistic', b'1', 'RolePermissionSetting', 1, NULL),
(19, '2019-04-10 17:30:38.090632', NULL, NULL, 'Pages.Report.AccountPosition', b'1', 'RolePermissionSetting', 1, NULL),
(20, '2019-04-10 17:30:38.090671', NULL, NULL, 'Pages.Support', b'1', 'RolePermissionSetting', 1, NULL),
(21, '2019-04-10 17:30:38.090715', NULL, NULL, 'Pages.Support.Create', b'1', 'RolePermissionSetting', 1, NULL),
(22, '2019-04-10 17:30:38.090755', NULL, NULL, 'Pages.Support.Update', b'1', 'RolePermissionSetting', 1, NULL),
(23, '2019-04-10 17:30:38.090795', NULL, NULL, 'Pages.Support.Delete', b'1', 'RolePermissionSetting', 1, NULL),
(24, '2019-04-10 17:30:38.090835', NULL, NULL, 'Pages.Dispute', b'1', 'RolePermissionSetting', 1, NULL),
(25, '2019-04-10 17:30:38.090875', NULL, NULL, 'Pages.Dispute.Create', b'1', 'RolePermissionSetting', 1, NULL),
(26, '2019-04-10 17:30:38.090915', NULL, NULL, 'Pages.Dispute.Update', b'1', 'RolePermissionSetting', 1, NULL),
(27, '2019-04-10 17:30:38.090955', NULL, NULL, 'Pages.Dispute.Delete', b'1', 'RolePermissionSetting', 1, NULL),
(28, '2019-04-10 17:30:38.090994', NULL, NULL, 'Pages.Appsettings', b'1', 'RolePermissionSetting', 1, NULL),
(29, '2019-04-10 17:30:38.091035', NULL, NULL, 'Pages.Appsettings.Create', b'1', 'RolePermissionSetting', 1, NULL),
(30, '2019-04-10 17:30:38.090512', NULL, NULL, 'Pages.Report.TransactionFee', b'1', 'RolePermissionSetting', 1, NULL),
(31, '2019-04-10 17:30:38.091075', NULL, NULL, 'Pages.Appsettings.Update', b'1', 'RolePermissionSetting', 1, NULL),
(32, '2019-04-10 17:30:38.089856', NULL, NULL, 'Pages.Advert', b'1', 'RolePermissionSetting', 1, NULL),
(33, '2019-04-10 17:30:38.089776', NULL, NULL, 'Pages.Codesystem.Delete', b'1', 'RolePermissionSetting', 1, NULL),
(34, '2019-04-10 17:30:38.088213', NULL, NULL, 'Pages.Roles', b'1', 'RolePermissionSetting', 1, NULL),
(35, '2019-04-10 17:30:38.088504', NULL, NULL, 'Pages.Tenants', b'1', 'RolePermissionSetting', 1, NULL),
(36, '2019-04-10 17:30:38.088581', NULL, NULL, 'Pages.Admin', b'1', 'RolePermissionSetting', 1, NULL),
(37, '2019-04-10 17:30:38.088639', NULL, NULL, 'Pages.Sponsor', b'1', 'RolePermissionSetting', 1, NULL),
(38, '2019-04-10 17:30:38.088691', NULL, NULL, 'Pages.Manage', b'1', 'RolePermissionSetting', 1, NULL),
(39, '2019-04-10 17:30:38.088736', NULL, NULL, 'Pages.Manage.Create', b'1', 'RolePermissionSetting', 1, NULL),
(40, '2019-04-10 17:30:38.088783', NULL, NULL, 'Pages.Manage.Update', b'1', 'RolePermissionSetting', 1, NULL),
(41, '2019-04-10 17:30:38.088832', NULL, NULL, 'Pages.Manage.Delete', b'1', 'RolePermissionSetting', 1, NULL),
(42, '2019-04-10 17:30:38.088879', NULL, NULL, 'Pages.Wallet', b'1', 'RolePermissionSetting', 1, NULL),
(43, '2019-04-10 17:30:38.088920', NULL, NULL, 'Pages.Wallet.Deposit', b'1', 'RolePermissionSetting', 1, NULL),
(44, '2019-04-10 17:30:38.088963', NULL, NULL, 'Pages.Wallet.Withdraw', b'1', 'RolePermissionSetting', 1, NULL),
(45, '2019-04-10 17:30:38.089003', NULL, NULL, 'Pages.Wallet.Transfer', b'1', 'RolePermissionSetting', 1, NULL),
(46, '2019-04-10 17:30:38.089046', NULL, NULL, 'Pages.WalletManagement', b'1', 'RolePermissionSetting', 1, NULL),
(47, '2019-04-10 17:30:38.089815', NULL, NULL, 'Pages.Codesystem.Validate', b'1', 'RolePermissionSetting', 1, NULL),
(48, '2019-04-10 17:30:38.089094', NULL, NULL, 'Pages.WalletManagement.ProcessDeposit', b'1', 'RolePermissionSetting', 1, NULL),
(49, '2019-04-10 17:30:38.089177', NULL, NULL, 'Pages.Wallet.ProcessTransfer', b'1', 'RolePermissionSetting', 1, NULL),
(50, '2019-04-10 17:30:38.089217', NULL, NULL, 'Pages.Referral', b'1', 'RolePermissionSetting', 1, NULL),
(51, '2019-04-10 17:30:38.089263', NULL, NULL, 'Pages.Referral.Update', b'1', 'RolePermissionSetting', 1, NULL),
(52, '2019-04-10 17:30:38.089306', NULL, NULL, 'Pages.Contest', b'1', 'RolePermissionSetting', 1, NULL),
(53, '2019-04-10 17:30:38.089348', NULL, NULL, 'Pages.Contest.StartDate', b'1', 'RolePermissionSetting', 1, NULL),
(54, '2019-04-10 17:30:38.089395', NULL, NULL, 'Pages.Contest.Create', b'1', 'RolePermissionSetting', 1, NULL),
(55, '2019-04-10 17:30:38.089436', NULL, NULL, 'Pages.Contest.Update', b'1', 'RolePermissionSetting', 1, NULL),
(56, '2019-04-10 17:30:38.089477', NULL, NULL, 'Pages.Contest.Delete', b'1', 'RolePermissionSetting', 1, NULL),
(57, '2019-04-10 17:30:38.089528', NULL, NULL, 'Pages.Contest.Choosewinner', b'1', 'RolePermissionSetting', 1, NULL),
(58, '2019-04-10 17:30:38.089570', NULL, NULL, 'Pages.Contest.Closewinner', b'1', 'RolePermissionSetting', 1, NULL),
(59, '2019-04-10 17:30:38.089610', NULL, NULL, 'Pages.Codesystem', b'1', 'RolePermissionSetting', 1, NULL),
(60, '2019-04-10 17:30:38.089678', NULL, NULL, 'Pages.Codesystem.Create', b'1', 'RolePermissionSetting', 1, NULL),
(61, '2019-04-10 17:30:38.089722', NULL, NULL, 'Pages.Codesystem.Update', b'1', 'RolePermissionSetting', 1, NULL),
(62, '2019-04-10 17:30:38.089136', NULL, NULL, 'Pages.Wallet.ProcessWithdraw', b'1', 'RolePermissionSetting', 1, NULL),
(63, '2019-04-10 17:30:38.091115', NULL, NULL, 'Pages.Appsettings.Delete', b'1', 'RolePermissionSetting', 1, NULL),
(64, '2019-04-10 17:30:38.618748', NULL, 1, 'Pages.Users', b'1', 'RolePermissionSetting', 2, NULL),
(65, '2019-04-10 17:30:38.620700', NULL, 1, 'Pages.Advert.Delete', b'1', 'RolePermissionSetting', 2, NULL),
(66, '2019-04-10 17:30:38.620744', NULL, 1, 'Pages.Advert.Process', b'1', 'RolePermissionSetting', 2, NULL),
(67, '2019-04-10 17:30:38.620785', NULL, 1, 'Pages.Advert.Report', b'1', 'RolePermissionSetting', 2, NULL),
(68, '2019-04-10 17:30:38.620827', NULL, 1, 'Pages.Voterecord', b'1', 'RolePermissionSetting', 2, NULL),
(69, '2019-04-10 17:30:38.620872', NULL, 1, 'Pages.Voterecord.Create', b'1', 'RolePermissionSetting', 2, NULL),
(70, '2019-04-10 17:30:38.620944', NULL, 1, 'Pages.Voterecord.Update', b'1', 'RolePermissionSetting', 2, NULL),
(71, '2019-04-10 17:30:38.620988', NULL, 1, 'Pages.Voterecord.Delete', b'1', 'RolePermissionSetting', 2, NULL),
(72, '2019-04-10 17:30:38.621031', NULL, 1, 'Pages.LeaderBoard', b'1', 'RolePermissionSetting', 2, NULL),
(73, '2019-04-10 17:30:38.621073', NULL, 1, 'Pages.OverLeaderBoard', b'1', 'RolePermissionSetting', 2, NULL),
(74, '2019-04-10 17:30:38.621118', NULL, 1, 'Pages.ContestLeaderBoard', b'1', 'RolePermissionSetting', 2, NULL),
(75, '2019-04-10 17:30:38.621161', NULL, 1, 'Pages.Voterecord.VoterLeaderBoard', b'1', 'RolePermissionSetting', 2, NULL),
(76, '2019-04-10 17:30:38.621202', NULL, 1, 'Pages.Report.UserList', b'1', 'RolePermissionSetting', 2, NULL),
(77, '2019-04-10 17:30:38.621249', NULL, 1, 'Pages.Report.Transaction', b'1', 'RolePermissionSetting', 2, NULL),
(78, '2019-04-10 17:30:38.621291', NULL, 1, 'Pages.Report.TransactionFee', b'1', 'RolePermissionSetting', 2, NULL),
(79, '2019-04-10 17:30:38.621334', NULL, 1, 'Pages.Report.ContestStatistic', b'1', 'RolePermissionSetting', 2, NULL),
(80, '2019-04-10 17:30:38.621375', NULL, 1, 'Pages.Report.VoteStatistic', b'1', 'RolePermissionSetting', 2, NULL),
(81, '2019-04-10 17:30:38.621417', NULL, 1, 'Pages.Report.AccountPosition', b'1', 'RolePermissionSetting', 2, NULL),
(82, '2019-04-10 17:30:38.621461', NULL, 1, 'Pages.Support', b'1', 'RolePermissionSetting', 2, NULL),
(83, '2019-04-10 17:30:38.621505', NULL, 1, 'Pages.Support.Create', b'1', 'RolePermissionSetting', 2, NULL),
(84, '2019-04-10 17:30:38.621546', NULL, 1, 'Pages.Support.Update', b'1', 'RolePermissionSetting', 2, NULL),
(85, '2019-04-10 17:30:38.621589', NULL, 1, 'Pages.Support.Delete', b'1', 'RolePermissionSetting', 2, NULL),
(86, '2019-04-10 17:30:38.621630', NULL, 1, 'Pages.Dispute', b'1', 'RolePermissionSetting', 2, NULL),
(87, '2019-04-10 17:30:38.621672', NULL, 1, 'Pages.Dispute.Create', b'1', 'RolePermissionSetting', 2, NULL),
(88, '2019-04-10 17:30:38.621713', NULL, 1, 'Pages.Dispute.Update', b'1', 'RolePermissionSetting', 2, NULL),
(89, '2019-04-10 17:30:38.621756', NULL, 1, 'Pages.Dispute.Delete', b'1', 'RolePermissionSetting', 2, NULL),
(90, '2019-04-10 17:30:38.621796', NULL, 1, 'Pages.Appsettings', b'1', 'RolePermissionSetting', 2, NULL),
(91, '2019-04-10 17:30:38.621839', NULL, 1, 'Pages.Appsettings.Create', b'1', 'RolePermissionSetting', 2, NULL),
(92, '2019-04-10 17:30:38.620656', NULL, 1, 'Pages.Advert.Update', b'1', 'RolePermissionSetting', 2, NULL),
(93, '2019-04-10 17:30:38.620614', NULL, 1, 'Pages.Advert.Create', b'1', 'RolePermissionSetting', 2, NULL),
(94, '2019-04-10 17:30:38.620572', NULL, 1, 'Pages.Advert', b'1', 'RolePermissionSetting', 2, NULL),
(95, '2019-04-10 17:30:38.620530', NULL, 1, 'Pages.Codesystem.Validate', b'1', 'RolePermissionSetting', 2, NULL),
(96, '2019-04-10 17:30:38.619082', NULL, 1, 'Pages.Roles', b'1', 'RolePermissionSetting', 2, NULL),
(97, '2019-04-10 17:30:38.619173', NULL, 1, 'Pages.Admin', b'1', 'RolePermissionSetting', 2, NULL),
(98, '2019-04-10 17:30:38.619230', NULL, 1, 'Pages.Sponsor', b'1', 'RolePermissionSetting', 2, NULL),
(99, '2019-04-10 17:30:38.619285', NULL, 1, 'Pages.Manage', b'1', 'RolePermissionSetting', 2, NULL),
(100, '2019-04-10 17:30:38.619334', NULL, 1, 'Pages.Manage.Create', b'1', 'RolePermissionSetting', 2, NULL),
(101, '2019-04-10 17:30:38.619388', NULL, 1, 'Pages.Manage.Update', b'1', 'RolePermissionSetting', 2, NULL),
(102, '2019-04-10 17:30:38.619441', NULL, 1, 'Pages.Manage.Delete', b'1', 'RolePermissionSetting', 2, NULL),
(103, '2019-04-10 17:30:38.619497', NULL, 1, 'Pages.Wallet', b'1', 'RolePermissionSetting', 2, NULL),
(104, '2019-04-10 17:30:38.619545', NULL, 1, 'Pages.Wallet.Deposit', b'1', 'RolePermissionSetting', 2, NULL),
(105, '2019-04-10 17:30:38.619591', NULL, 1, 'Pages.Wallet.Withdraw', b'1', 'RolePermissionSetting', 2, NULL),
(106, '2019-04-10 17:30:38.619662', NULL, 1, 'Pages.Wallet.Transfer', b'1', 'RolePermissionSetting', 2, NULL),
(107, '2019-04-10 17:30:38.619766', NULL, 1, 'Pages.WalletManagement', b'1', 'RolePermissionSetting', 2, NULL),
(108, '2019-04-10 17:30:38.619814', NULL, 1, 'Pages.WalletManagement.ProcessDeposit', b'1', 'RolePermissionSetting', 2, NULL),
(109, '2019-04-10 17:30:38.621883', NULL, 1, 'Pages.Appsettings.Update', b'1', 'RolePermissionSetting', 2, NULL),
(110, '2019-04-10 17:30:38.619856', NULL, 1, 'Pages.Wallet.ProcessWithdraw', b'1', 'RolePermissionSetting', 2, NULL),
(111, '2019-04-10 17:30:38.619948', NULL, 1, 'Pages.Referral', b'1', 'RolePermissionSetting', 2, NULL),
(112, '2019-04-10 17:30:38.620007', NULL, 1, 'Pages.Referral.Update', b'1', 'RolePermissionSetting', 2, NULL),
(113, '2019-04-10 17:30:38.620054', NULL, 1, 'Pages.Contest', b'1', 'RolePermissionSetting', 2, NULL),
(114, '2019-04-10 17:30:38.620097', NULL, 1, 'Pages.Contest.StartDate', b'1', 'RolePermissionSetting', 2, NULL),
(115, '2019-04-10 17:30:38.620140', NULL, 1, 'Pages.Contest.Create', b'1', 'RolePermissionSetting', 2, NULL),
(116, '2019-04-10 17:30:38.620182', NULL, 1, 'Pages.Contest.Update', b'1', 'RolePermissionSetting', 2, NULL),
(117, '2019-04-10 17:30:38.620223', NULL, 1, 'Pages.Contest.Delete', b'1', 'RolePermissionSetting', 2, NULL),
(118, '2019-04-10 17:30:38.620268', NULL, 1, 'Pages.Contest.Choosewinner', b'1', 'RolePermissionSetting', 2, NULL),
(119, '2019-04-10 17:30:38.620310', NULL, 1, 'Pages.Contest.Closewinner', b'1', 'RolePermissionSetting', 2, NULL),
(120, '2019-04-10 17:30:38.620356', NULL, 1, 'Pages.Codesystem', b'1', 'RolePermissionSetting', 2, NULL),
(121, '2019-04-10 17:30:38.620399', NULL, 1, 'Pages.Codesystem.Create', b'1', 'RolePermissionSetting', 2, NULL),
(122, '2019-04-10 17:30:38.620445', NULL, 1, 'Pages.Codesystem.Update', b'1', 'RolePermissionSetting', 2, NULL),
(123, '2019-04-10 17:30:38.620487', NULL, 1, 'Pages.Codesystem.Delete', b'1', 'RolePermissionSetting', 2, NULL),
(124, '2019-04-10 17:30:38.619901', NULL, 1, 'Pages.Wallet.ProcessTransfer', b'1', 'RolePermissionSetting', 2, NULL),
(125, '2019-04-10 17:30:38.621927', NULL, 1, 'Pages.Appsettings.Delete', b'1', 'RolePermissionSetting', 2, NULL),
(126, '2019-04-10 17:30:38.789657', NULL, 1, 'Pages.Users', b'1', 'RolePermissionSetting', 3, NULL),
(127, '2019-04-10 17:30:38.791217', NULL, 1, 'Pages.Advert.Delete', b'1', 'RolePermissionSetting', 3, NULL),
(128, '2019-04-10 17:30:38.791262', NULL, 1, 'Pages.Advert.Process', b'1', 'RolePermissionSetting', 3, NULL),
(129, '2019-04-10 17:30:38.791310', NULL, 1, 'Pages.Advert.Report', b'1', 'RolePermissionSetting', 3, NULL),
(130, '2019-04-10 17:30:38.791354', NULL, 1, 'Pages.Voterecord', b'1', 'RolePermissionSetting', 3, NULL),
(131, '2019-04-10 17:30:38.791400', NULL, 1, 'Pages.Voterecord.Create', b'1', 'RolePermissionSetting', 3, NULL),
(132, '2019-04-10 17:30:38.791443', NULL, 1, 'Pages.Voterecord.Update', b'1', 'RolePermissionSetting', 3, NULL),
(133, '2019-04-10 17:30:38.791493', NULL, 1, 'Pages.Voterecord.Delete', b'1', 'RolePermissionSetting', 3, NULL),
(134, '2019-04-10 17:30:38.791537', NULL, 1, 'Pages.LeaderBoard', b'1', 'RolePermissionSetting', 3, NULL),
(135, '2019-04-10 17:30:38.791582', NULL, 1, 'Pages.OverLeaderBoard', b'1', 'RolePermissionSetting', 3, NULL),
(136, '2019-04-10 17:30:38.791626', NULL, 1, 'Pages.ContestLeaderBoard', b'1', 'RolePermissionSetting', 3, NULL),
(137, '2019-04-10 17:30:38.791670', NULL, 1, 'Pages.Voterecord.VoterLeaderBoard', b'1', 'RolePermissionSetting', 3, NULL),
(138, '2019-04-10 17:30:38.791714', NULL, 1, 'Pages.Report.UserList', b'1', 'RolePermissionSetting', 3, NULL),
(139, '2019-04-10 17:30:38.791759', NULL, 1, 'Pages.Report.Transaction', b'1', 'RolePermissionSetting', 3, NULL),
(140, '2019-04-10 17:30:38.791803', NULL, 1, 'Pages.Report.TransactionFee', b'1', 'RolePermissionSetting', 3, NULL),
(141, '2019-04-10 17:30:38.791846', NULL, 1, 'Pages.Report.ContestStatistic', b'1', 'RolePermissionSetting', 3, NULL),
(142, '2019-04-10 17:30:38.791891', NULL, 1, 'Pages.Report.VoteStatistic', b'1', 'RolePermissionSetting', 3, NULL),
(143, '2019-04-10 17:30:38.791934', NULL, 1, 'Pages.Report.AccountPosition', b'1', 'RolePermissionSetting', 3, NULL),
(144, '2019-04-10 17:30:38.791984', NULL, 1, 'Pages.Support', b'1', 'RolePermissionSetting', 3, NULL),
(145, '2019-04-10 17:30:38.792070', NULL, 1, 'Pages.Support.Create', b'1', 'RolePermissionSetting', 3, NULL),
(146, '2019-04-10 17:30:38.792170', NULL, 1, 'Pages.Support.Update', b'1', 'RolePermissionSetting', 3, NULL),
(147, '2019-04-10 17:30:38.792226', NULL, 1, 'Pages.Support.Delete', b'1', 'RolePermissionSetting', 3, NULL),
(148, '2019-04-10 17:30:38.792272', NULL, 1, 'Pages.Dispute', b'1', 'RolePermissionSetting', 3, NULL),
(149, '2019-04-10 17:30:38.792332', NULL, 1, 'Pages.Dispute.Create', b'1', 'RolePermissionSetting', 3, NULL),
(150, '2019-04-10 17:30:38.792396', NULL, 1, 'Pages.Dispute.Update', b'1', 'RolePermissionSetting', 3, NULL),
(151, '2019-04-10 17:30:38.792449', NULL, 1, 'Pages.Dispute.Delete', b'1', 'RolePermissionSetting', 3, NULL),
(152, '2019-04-10 17:30:38.792500', NULL, 1, 'Pages.Appsettings', b'1', 'RolePermissionSetting', 3, NULL),
(153, '2019-04-10 17:30:38.792553', NULL, 1, 'Pages.Appsettings.Create', b'1', 'RolePermissionSetting', 3, NULL),
(154, '2019-04-10 17:30:38.791171', NULL, 1, 'Pages.Advert.Update', b'1', 'RolePermissionSetting', 3, NULL),
(155, '2019-04-10 17:30:38.792606', NULL, 1, 'Pages.Appsettings.Update', b'1', 'RolePermissionSetting', 3, NULL),
(156, '2019-04-10 17:30:38.791118', NULL, 1, 'Pages.Advert.Create', b'1', 'RolePermissionSetting', 3, NULL),
(157, '2019-04-10 17:30:38.791029', NULL, 1, 'Pages.Codesystem.Validate', b'1', 'RolePermissionSetting', 3, NULL),
(158, '2019-04-10 17:30:38.789810', NULL, 1, 'Pages.Roles', b'1', 'RolePermissionSetting', 3, NULL),
(159, '2019-04-10 17:30:38.789862', NULL, 1, 'Pages.Admin', b'1', 'RolePermissionSetting', 3, NULL),
(160, '2019-04-10 17:30:38.789906', NULL, 1, 'Pages.Manage', b'1', 'RolePermissionSetting', 3, NULL),
(161, '2019-04-10 17:30:38.789954', NULL, 1, 'Pages.Manage.Create', b'1', 'RolePermissionSetting', 3, NULL),
(162, '2019-04-10 17:30:38.790001', NULL, 1, 'Pages.Manage.Update', b'1', 'RolePermissionSetting', 3, NULL),
(163, '2019-04-10 17:30:38.790046', NULL, 1, 'Pages.Manage.Delete', b'1', 'RolePermissionSetting', 3, NULL),
(164, '2019-04-10 17:30:38.790090', NULL, 1, 'Pages.Wallet', b'1', 'RolePermissionSetting', 3, NULL),
(165, '2019-04-10 17:30:38.790137', NULL, 1, 'Pages.Wallet.Deposit', b'1', 'RolePermissionSetting', 3, NULL),
(166, '2019-04-10 17:30:38.790182', NULL, 1, 'Pages.Wallet.Withdraw', b'1', 'RolePermissionSetting', 3, NULL),
(167, '2019-04-10 17:30:38.790226', NULL, 1, 'Pages.Wallet.Transfer', b'1', 'RolePermissionSetting', 3, NULL),
(168, '2019-04-10 17:30:38.790271', NULL, 1, 'Pages.WalletManagement', b'1', 'RolePermissionSetting', 3, NULL),
(169, '2019-04-10 17:30:38.790315', NULL, 1, 'Pages.WalletManagement.ProcessDeposit', b'1', 'RolePermissionSetting', 3, NULL),
(170, '2019-04-10 17:30:38.790360', NULL, 1, 'Pages.Wallet.ProcessWithdraw', b'1', 'RolePermissionSetting', 3, NULL),
(171, '2019-04-10 17:30:38.790404', NULL, 1, 'Pages.Wallet.ProcessTransfer', b'1', 'RolePermissionSetting', 3, NULL),
(172, '2019-04-10 17:30:38.790449', NULL, 1, 'Pages.Referral', b'1', 'RolePermissionSetting', 3, NULL),
(173, '2019-04-10 17:30:38.790493', NULL, 1, 'Pages.Referral.Update', b'1', 'RolePermissionSetting', 3, NULL),
(174, '2019-04-10 17:30:38.790536', NULL, 1, 'Pages.Contest', b'1', 'RolePermissionSetting', 3, NULL),
(175, '2019-04-10 17:30:38.790583', NULL, 1, 'Pages.Contest.StartDate', b'1', 'RolePermissionSetting', 3, NULL),
(176, '2019-04-10 17:30:38.790629', NULL, 1, 'Pages.Contest.Create', b'1', 'RolePermissionSetting', 3, NULL),
(177, '2019-04-10 17:30:38.790673', NULL, 1, 'Pages.Contest.Update', b'1', 'RolePermissionSetting', 3, NULL),
(178, '2019-04-10 17:30:38.790719', NULL, 1, 'Pages.Contest.Delete', b'1', 'RolePermissionSetting', 3, NULL),
(179, '2019-04-10 17:30:38.790763', NULL, 1, 'Pages.Contest.Choosewinner', b'1', 'RolePermissionSetting', 3, NULL),
(180, '2019-04-10 17:30:38.790808', NULL, 1, 'Pages.Contest.Closewinner', b'1', 'RolePermissionSetting', 3, NULL),
(181, '2019-04-10 17:30:38.790852', NULL, 1, 'Pages.Codesystem', b'1', 'RolePermissionSetting', 3, NULL),
(182, '2019-04-10 17:30:38.790897', NULL, 1, 'Pages.Codesystem.Create', b'1', 'RolePermissionSetting', 3, NULL),
(183, '2019-04-10 17:30:38.790940', NULL, 1, 'Pages.Codesystem.Update', b'1', 'RolePermissionSetting', 3, NULL),
(184, '2019-04-10 17:30:38.790985', NULL, 1, 'Pages.Codesystem.Delete', b'1', 'RolePermissionSetting', 3, NULL),
(185, '2019-04-10 17:30:38.791074', NULL, 1, 'Pages.Advert', b'1', 'RolePermissionSetting', 3, NULL),
(186, '2019-04-10 17:30:38.792656', NULL, 1, 'Pages.Appsettings.Delete', b'1', 'RolePermissionSetting', 3, NULL),
(187, '2019-04-10 17:30:38.893052', NULL, 1, 'Pages.Users', b'1', 'RolePermissionSetting', 4, NULL),
(188, '2019-04-10 17:30:38.895089', NULL, 1, 'Pages.Dispute.Create', b'1', 'RolePermissionSetting', 4, NULL),
(189, '2019-04-10 17:30:38.895040', NULL, 1, 'Pages.Dispute', b'1', 'RolePermissionSetting', 4, NULL),
(190, '2019-04-10 17:30:38.894979', NULL, 1, 'Pages.Support.Delete', b'1', 'RolePermissionSetting', 4, NULL),
(191, '2019-04-10 17:30:38.894932', NULL, 1, 'Pages.Support.Update', b'1', 'RolePermissionSetting', 4, NULL),
(192, '2019-04-10 17:30:38.894877', NULL, 1, 'Pages.Support.Create', b'1', 'RolePermissionSetting', 4, NULL),
(193, '2019-04-10 17:30:38.894795', NULL, 1, 'Pages.Support', b'1', 'RolePermissionSetting', 4, NULL),
(194, '2019-04-10 17:30:38.894715', NULL, 1, 'Pages.Voterecord.VoterLeaderBoard', b'1', 'RolePermissionSetting', 4, NULL),
(195, '2019-04-10 17:30:38.894631', NULL, 1, 'Pages.ContestLeaderBoard', b'1', 'RolePermissionSetting', 4, NULL),
(196, '2019-04-10 17:30:38.894546', NULL, 1, 'Pages.OverLeaderBoard', b'1', 'RolePermissionSetting', 4, NULL),
(197, '2019-04-10 17:30:38.894479', NULL, 1, 'Pages.LeaderBoard', b'1', 'RolePermissionSetting', 4, NULL),
(198, '2019-04-10 17:30:38.894435', NULL, 1, 'Pages.Voterecord.Delete', b'1', 'RolePermissionSetting', 4, NULL),
(199, '2019-04-10 17:30:38.894390', NULL, 1, 'Pages.Voterecord.Update', b'1', 'RolePermissionSetting', 4, NULL),
(200, '2019-04-10 17:30:38.894345', NULL, 1, 'Pages.Voterecord.Create', b'1', 'RolePermissionSetting', 4, NULL),
(201, '2019-04-10 17:30:38.894298', NULL, 1, 'Pages.Voterecord', b'1', 'RolePermissionSetting', 4, NULL),
(202, '2019-04-10 17:30:38.895133', NULL, 1, 'Pages.Dispute.Update', b'1', 'RolePermissionSetting', 4, NULL),
(203, '2019-04-10 17:30:38.894251', NULL, 1, 'Pages.Advert.Update', b'1', 'RolePermissionSetting', 4, NULL),
(204, '2019-04-10 17:30:38.894108', NULL, 1, 'Pages.Advert', b'1', 'RolePermissionSetting', 4, NULL),
(205, '2019-04-10 17:30:38.894063', NULL, 1, 'Pages.Contest.Update', b'1', 'RolePermissionSetting', 4, NULL),
(206, '2019-04-10 17:30:38.894018', NULL, 1, 'Pages.Contest.Create', b'1', 'RolePermissionSetting', 4, NULL),
(207, '2019-04-10 17:30:38.893968', NULL, 1, 'Pages.Contest', b'1', 'RolePermissionSetting', 4, NULL),
(208, '2019-04-10 17:30:38.893924', NULL, 1, 'Pages.Referral', b'1', 'RolePermissionSetting', 4, NULL),
(209, '2019-04-10 17:30:38.893879', NULL, 1, 'Pages.Wallet.Transfer', b'1', 'RolePermissionSetting', 4, NULL),
(210, '2019-04-10 17:30:38.893834', NULL, 1, 'Pages.Wallet.Withdraw', b'1', 'RolePermissionSetting', 4, NULL),
(211, '2019-04-10 17:30:38.893783', NULL, 1, 'Pages.Wallet.Deposit', b'1', 'RolePermissionSetting', 4, NULL),
(212, '2019-04-10 17:30:38.893715', NULL, 1, 'Pages.Wallet', b'1', 'RolePermissionSetting', 4, NULL),
(213, '2019-04-10 17:30:38.893621', NULL, 1, 'Pages.Manage.Delete', b'1', 'RolePermissionSetting', 4, NULL),
(214, '2019-04-10 17:30:38.893475', NULL, 1, 'Pages.Manage.Update', b'1', 'RolePermissionSetting', 4, NULL),
(215, '2019-04-10 17:30:38.893327', NULL, 1, 'Pages.Manage.Create', b'1', 'RolePermissionSetting', 4, NULL),
(216, '2019-04-10 17:30:38.893267', NULL, 1, 'Pages.Manage', b'1', 'RolePermissionSetting', 4, NULL),
(217, '2019-04-10 17:30:38.893218', NULL, 1, 'Pages.Sponsor', b'1', 'RolePermissionSetting', 4, NULL),
(218, '2019-04-10 17:30:38.894152', NULL, 1, 'Pages.Advert.Create', b'1', 'RolePermissionSetting', 4, NULL),
(219, '2019-04-10 17:30:38.895179', NULL, 1, 'Pages.Dispute.Delete', b'1', 'RolePermissionSetting', 4, NULL),
(220, '2019-04-10 17:30:38.993562', NULL, 1, 'Pages.Users', b'1', 'RolePermissionSetting', 5, NULL),
(221, '2019-04-10 17:30:38.994980', NULL, 1, 'Pages.Support.Create', b'1', 'RolePermissionSetting', 5, NULL),
(222, '2019-04-10 17:30:38.994932', NULL, 1, 'Pages.Support', b'1', 'RolePermissionSetting', 5, NULL),
(223, '2019-04-10 17:30:38.994878', NULL, 1, 'Pages.Voterecord.VoterLeaderBoard', b'1', 'RolePermissionSetting', 5, NULL),
(224, '2019-04-10 17:30:38.994775', NULL, 1, 'Pages.ContestLeaderBoard', b'1', 'RolePermissionSetting', 5, NULL),
(225, '2019-04-10 17:30:38.994691', NULL, 1, 'Pages.OverLeaderBoard', b'1', 'RolePermissionSetting', 5, NULL),
(226, '2019-04-10 17:30:38.994631', NULL, 1, 'Pages.LeaderBoard', b'1', 'RolePermissionSetting', 5, NULL),
(227, '2019-04-10 17:30:38.994502', NULL, 1, 'Pages.Voterecord.Delete', b'1', 'RolePermissionSetting', 5, NULL),
(228, '2019-04-10 17:30:38.994451', NULL, 1, 'Pages.Voterecord.Update', b'1', 'RolePermissionSetting', 5, NULL),
(229, '2019-04-10 17:30:38.994406', NULL, 1, 'Pages.Voterecord.Create', b'1', 'RolePermissionSetting', 5, NULL),
(230, '2019-04-10 17:30:38.994363', NULL, 1, 'Pages.Voterecord', b'1', 'RolePermissionSetting', 5, NULL),
(231, '2019-04-10 17:30:38.994323', NULL, 1, 'Pages.Advert.Update', b'1', 'RolePermissionSetting', 5, NULL),
(232, '2019-04-10 17:30:38.994279', NULL, 1, 'Pages.Advert.Create', b'1', 'RolePermissionSetting', 5, NULL),
(233, '2019-04-10 17:30:38.994238', NULL, 1, 'Pages.Advert', b'1', 'RolePermissionSetting', 5, NULL),
(234, '2019-04-10 17:30:38.994195', NULL, 1, 'Pages.Contest.Update', b'1', 'RolePermissionSetting', 5, NULL),
(235, '2019-04-10 17:30:38.994155', NULL, 1, 'Pages.Contest.Create', b'1', 'RolePermissionSetting', 5, NULL),
(236, '2019-04-10 17:30:38.994112', NULL, 1, 'Pages.Contest', b'1', 'RolePermissionSetting', 5, NULL),
(237, '2019-04-10 17:30:38.994071', NULL, 1, 'Pages.Referral', b'1', 'RolePermissionSetting', 5, NULL),
(238, '2019-04-10 17:30:38.994028', NULL, 1, 'Pages.Wallet.Transfer', b'1', 'RolePermissionSetting', 5, NULL),
(239, '2019-04-10 17:30:38.993985', NULL, 1, 'Pages.Wallet.Withdraw', b'1', 'RolePermissionSetting', 5, NULL),
(240, '2019-04-10 17:30:38.993941', NULL, 1, 'Pages.Wallet.Deposit', b'1', 'RolePermissionSetting', 5, NULL),
(241, '2019-04-10 17:30:38.993900', NULL, 1, 'Pages.Wallet', b'1', 'RolePermissionSetting', 5, NULL),
(242, '2019-04-10 17:30:38.993856', NULL, 1, 'Pages.Manage.Delete', b'1', 'RolePermissionSetting', 5, NULL),
(243, '2019-04-10 17:30:38.993812', NULL, 1, 'Pages.Manage.Update', b'1', 'RolePermissionSetting', 5, NULL),
(244, '2019-04-10 17:30:38.993766', NULL, 1, 'Pages.Manage.Create', b'1', 'RolePermissionSetting', 5, NULL),
(245, '2019-04-10 17:30:38.993713', NULL, 1, 'Pages.Manage', b'1', 'RolePermissionSetting', 5, NULL),
(246, '2019-04-10 17:30:38.995024', NULL, 1, 'Pages.Support.Update', b'1', 'RolePermissionSetting', 5, NULL),
(247, '2019-04-10 17:30:38.995070', NULL, 1, 'Pages.Support.Delete', b'1', 'RolePermissionSetting', 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `AbpRoleClaims`
--

CREATE TABLE IF NOT EXISTS `AbpRoleClaims` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `RoleId` int(11) NOT NULL,
  `ClaimType` varchar(256) DEFAULT NULL,
  `ClaimValue` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbpRoles`
--

CREATE TABLE IF NOT EXISTS `AbpRoles` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `Name` varchar(32) NOT NULL,
  `DisplayName` varchar(64) NOT NULL,
  `IsStatic` bit(1) NOT NULL,
  `IsDefault` bit(1) NOT NULL,
  `NormalizedName` varchar(32) NOT NULL,
  `ConcurrencyStamp` varchar(128) DEFAULT NULL,
  `Description` longtext
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpRoles`
--

INSERT INTO `AbpRoles` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `TenantId`, `Name`, `DisplayName`, `IsStatic`, `IsDefault`, `NormalizedName`, `ConcurrencyStamp`, `Description`) VALUES
(1, '2019-04-10 17:30:37.836300', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'Admin', 'Admin', b'1', b'1', 'ADMIN', '81f17360-00dc-459c-8327-52684f5b678a', NULL),
(2, '2019-04-10 17:30:38.595540', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Admin', 'Admin', b'1', b'0', 'ADMIN', '3f187f7c-f145-49eb-8968-b3702f699b79', NULL),
(3, '2019-04-10 17:30:38.762247', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'AdminLevel2', 'AdminLevel2', b'0', b'0', 'ADMINLEVEL2', '8bf2389f-94e2-495d-90d1-0b05a34c3d9b', NULL),
(4, '2019-04-10 17:30:38.858232', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Sponsors', 'Sponsors', b'0', b'0', 'SPONSORS', 'a4231f65-c05c-4db4-87c1-7b7a5159f753', NULL),
(5, '2019-04-10 17:30:38.953843', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Users', 'Users', b'0', b'0', 'USERS', '28f254a7-cbdf-4c7a-85ca-f325a9e9ea38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `AbpSettings`
--

CREATE TABLE IF NOT EXISTS `AbpSettings` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) DEFAULT NULL,
  `Name` varchar(256) NOT NULL,
  `Value` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpSettings`
--

INSERT INTO `AbpSettings` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `TenantId`, `UserId`, `Name`, `Value`) VALUES
(1, '2019-04-10 17:30:38.412756', NULL, NULL, NULL, NULL, NULL, 'Abp.Net.Mail.DefaultFromAddress', 'admin@icontestpro.com'),
(2, '2019-04-10 17:30:38.452170', NULL, NULL, NULL, NULL, NULL, 'Abp.Net.Mail.DefaultFromDisplayName', 'icontestpro.com mailer'),
(3, '2019-04-10 17:30:38.464957', NULL, NULL, NULL, NULL, NULL, 'Abp.Localization.DefaultLanguageName', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `AbpTenantNotifications`
--

CREATE TABLE IF NOT EXISTS `AbpTenantNotifications` (
  `Id` char(36) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `NotificationName` varchar(96) NOT NULL,
  `Data` longtext,
  `DataTypeName` varchar(512) DEFAULT NULL,
  `EntityTypeName` varchar(250) DEFAULT NULL,
  `EntityTypeAssemblyQualifiedName` varchar(512) DEFAULT NULL,
  `EntityId` varchar(96) DEFAULT NULL,
  `Severity` tinyint(3) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpTenantNotifications`
--

INSERT INTO `AbpTenantNotifications` (`Id`, `CreationTime`, `CreatorUserId`, `TenantId`, `NotificationName`, `Data`, `DataTypeName`, `EntityTypeName`, `EntityTypeAssemblyQualifiedName`, `EntityId`, `Severity`) VALUES
('b333ff5d-8e58-45b6-a615-5435512a30e8', '2019-04-11 12:55:19.280798', NULL, 1, 'App.SimpleMessage', '{"Message":"Welcome to iContestPRO. Your account was created successfully.","Type":"Abp.Notifications.MessageNotificationData","Properties":{"Message":"Welcome to iContestPRO. Your account was created successfully."}}', 'Abp.Notifications.MessageNotificationData, Abp, Version=4.3.0.0, Culture=neutral, PublicKeyToken=null', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `AbpTenants`
--

CREATE TABLE IF NOT EXISTS `AbpTenants` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `TenancyName` varchar(64) NOT NULL,
  `Name` varchar(128) NOT NULL,
  `ConnectionString` varchar(1024) DEFAULT NULL,
  `IsActive` bit(1) NOT NULL,
  `EditionId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpTenants`
--

INSERT INTO `AbpTenants` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `TenancyName`, `Name`, `ConnectionString`, `IsActive`, `EditionId`) VALUES
(1, '2019-04-10 17:30:38.510780', NULL, NULL, NULL, b'0', NULL, NULL, 'Default', 'Default', NULL, b'1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `AbpUserAccounts`
--

CREATE TABLE IF NOT EXISTS `AbpUserAccounts` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `UserLinkId` bigint(20) DEFAULT NULL,
  `UserName` varchar(256) DEFAULT NULL,
  `EmailAddress` varchar(256) DEFAULT NULL,
  `LastLoginTime` datetime(6) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpUserAccounts`
--

INSERT INTO `AbpUserAccounts` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `TenantId`, `UserId`, `UserLinkId`, `UserName`, `EmailAddress`, `LastLoginTime`) VALUES
(1, '2019-04-10 17:30:41.620056', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 1, NULL, 'admin', 'admin@icontestpro.com', NULL),
(2, '2019-04-10 17:30:41.893607', NULL, '2019-04-11 12:53:47.520769', NULL, b'0', NULL, NULL, 1, 2, NULL, 'admintenant', 'admin@defaulttenant.com', '2019-04-11 12:53:47.462310'),
(3, '2019-04-11 12:54:42.768595', NULL, '2019-04-11 18:52:58.434015', NULL, b'0', NULL, NULL, 1, 3, NULL, 'fidelisekeh@gmail.com', 'fidelisekeh@gmail.com', '2019-04-11 18:52:57.018630');

-- --------------------------------------------------------

--
-- Table structure for table `AbpUserClaims`
--

CREATE TABLE IF NOT EXISTS `AbpUserClaims` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `ClaimType` varchar(256) DEFAULT NULL,
  `ClaimValue` longtext
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpUserClaims`
--

INSERT INTO `AbpUserClaims` (`Id`, `CreationTime`, `CreatorUserId`, `TenantId`, `UserId`, `ClaimType`, `ClaimValue`) VALUES
(1, '2019-04-11 11:34:21.108064', NULL, 1, 2, 'Fullname', 'admin admin'),
(2, '2019-04-11 11:34:21.176558', NULL, 1, 2, 'Nickname', 'Admin'),
(3, '2019-04-11 11:34:21.188002', NULL, 1, 2, 'ContestEntryImage', ''),
(4, '2019-04-11 18:52:57.861920', NULL, 1, 3, 'Fullname', ' '),
(5, '2019-04-11 18:52:57.911038', NULL, 1, 3, 'Nickname', 'Fidelis Kalu'),
(6, '2019-04-11 18:52:57.920987', NULL, 1, 3, 'ContestEntryImage', '');

-- --------------------------------------------------------

--
-- Table structure for table `AbpUserLoginAttempts`
--

CREATE TABLE IF NOT EXISTS `AbpUserLoginAttempts` (
  `Id` bigint(20) NOT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `TenancyName` varchar(64) DEFAULT NULL,
  `UserId` bigint(20) DEFAULT NULL,
  `UserNameOrEmailAddress` varchar(255) DEFAULT NULL,
  `ClientIpAddress` varchar(64) DEFAULT NULL,
  `ClientName` varchar(128) DEFAULT NULL,
  `BrowserInfo` varchar(512) DEFAULT NULL,
  `Result` tinyint(3) unsigned NOT NULL,
  `CreationTime` datetime(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpUserLoginAttempts`
--

INSERT INTO `AbpUserLoginAttempts` (`Id`, `TenantId`, `TenancyName`, `UserId`, `UserNameOrEmailAddress`, `ClientIpAddress`, `ClientName`, `BrowserInfo`, `Result`, `CreationTime`) VALUES
(1, 1, 'Default', NULL, 'fidelisekeh@gmail.com', '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 2, '2019-04-11 11:34:09.231591'),
(2, 1, 'Default', 2, 'admin@defaulttenant.com', '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1, '2019-04-11 11:34:20.991276'),
(3, 1, 'Default', NULL, 'fidelisekeh@gmail.com', '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 2, '2019-04-11 12:53:41.811456'),
(4, 1, 'Default', 2, 'admin@defaulttenant.com', '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1, '2019-04-11 12:53:47.492733'),
(5, NULL, 'Default', NULL, 'fidelisekeh@gmail.com', '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 7, '2019-04-11 12:55:16.918828'),
(6, NULL, 'Default', NULL, 'fidelisekeh@gmail.com', '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 7, '2019-04-11 12:56:34.169710'),
(7, NULL, 'Default', NULL, 'fidelisekeh@gmail.com', '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 7, '2019-04-11 12:57:14.584047'),
(8, NULL, 'Default', NULL, 'fidelisekeh@gmail.com', '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 7, '2019-04-11 15:12:51.318046'),
(9, 1, 'Default', 3, 'fidelisekeh@gmail.com', '::1', NULL, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 1, '2019-04-11 18:52:57.733168');

-- --------------------------------------------------------

--
-- Table structure for table `AbpUserLogins`
--

CREATE TABLE IF NOT EXISTS `AbpUserLogins` (
  `Id` bigint(20) NOT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `LoginProvider` varchar(128) NOT NULL,
  `ProviderKey` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbpUserNotifications`
--

CREATE TABLE IF NOT EXISTS `AbpUserNotifications` (
  `Id` char(36) NOT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `TenantNotificationId` char(36) NOT NULL,
  `State` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpUserNotifications`
--

INSERT INTO `AbpUserNotifications` (`Id`, `TenantId`, `UserId`, `TenantNotificationId`, `State`, `CreationTime`) VALUES
('6784af6a-914c-4864-9562-be5cea34f252', 1, 3, 'b333ff5d-8e58-45b6-a615-5435512a30e8', 1, '2019-04-11 12:55:19.335214');

-- --------------------------------------------------------

--
-- Table structure for table `AbpUserOrganizationUnits`
--

CREATE TABLE IF NOT EXISTS `AbpUserOrganizationUnits` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `OrganizationUnitId` bigint(20) NOT NULL,
  `IsDeleted` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbpUserRoles`
--

CREATE TABLE IF NOT EXISTS `AbpUserRoles` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `RoleId` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpUserRoles`
--

INSERT INTO `AbpUserRoles` (`Id`, `CreationTime`, `CreatorUserId`, `TenantId`, `UserId`, `RoleId`) VALUES
(1, '2019-04-10 17:30:38.360513', NULL, NULL, 1, 1),
(2, '2019-04-10 17:30:38.726710', NULL, 1, 2, 2),
(3, '2019-04-11 12:54:42.565112', NULL, 1, 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `AbpUsers`
--

CREATE TABLE IF NOT EXISTS `AbpUsers` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `AuthenticationSource` varchar(64) DEFAULT NULL,
  `UserName` varchar(256) NOT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `EmailAddress` varchar(256) NOT NULL,
  `Name` varchar(32) NOT NULL,
  `Surname` varchar(32) NOT NULL,
  `Password` varchar(128) NOT NULL,
  `EmailConfirmationCode` varchar(328) DEFAULT NULL,
  `PasswordResetCode` varchar(328) DEFAULT NULL,
  `LockoutEndDateUtc` datetime(6) DEFAULT NULL,
  `AccessFailedCount` int(11) NOT NULL,
  `IsLockoutEnabled` bit(1) NOT NULL,
  `PhoneNumber` varchar(32) DEFAULT NULL,
  `IsPhoneNumberConfirmed` bit(1) NOT NULL,
  `SecurityStamp` varchar(128) DEFAULT NULL,
  `IsTwoFactorEnabled` bit(1) NOT NULL,
  `IsEmailConfirmed` bit(1) NOT NULL,
  `IsActive` bit(1) NOT NULL,
  `LastLoginTime` datetime(6) DEFAULT NULL,
  `NormalizedUserName` varchar(256) NOT NULL,
  `NormalizedEmailAddress` varchar(256) NOT NULL,
  `ConcurrencyStamp` varchar(128) DEFAULT NULL,
  `CountryId` varchar(255) DEFAULT NULL,
  `StateId` int(11) DEFAULT NULL,
  `Profession` varchar(255) DEFAULT NULL,
  `NickName` varchar(50) DEFAULT NULL,
  `Image` varchar(50) DEFAULT NULL,
  `FaceBook` varchar(255) DEFAULT NULL,
  `LinkedIn` varchar(255) DEFAULT NULL,
  `Twitter` varchar(255) DEFAULT NULL,
  `Permalink` varchar(255) DEFAULT NULL,
  `Instagram` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbpUsers`
--

INSERT INTO `AbpUsers` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `AuthenticationSource`, `UserName`, `TenantId`, `EmailAddress`, `Name`, `Surname`, `Password`, `EmailConfirmationCode`, `PasswordResetCode`, `LockoutEndDateUtc`, `AccessFailedCount`, `IsLockoutEnabled`, `PhoneNumber`, `IsPhoneNumberConfirmed`, `SecurityStamp`, `IsTwoFactorEnabled`, `IsEmailConfirmed`, `IsActive`, `LastLoginTime`, `NormalizedUserName`, `NormalizedEmailAddress`, `ConcurrencyStamp`, `CountryId`, `StateId`, `Profession`, `NickName`, `Image`, `FaceBook`, `LinkedIn`, `Twitter`, `Permalink`, `Instagram`) VALUES
(1, '2019-04-10 17:30:38.203674', NULL, NULL, NULL, b'0', NULL, NULL, NULL, 'admin', NULL, 'admin@icontestpro.com', 'admin', 'admin', 'AQAAAAEAACcQAAAAEOhvc2qF72K53bqjX1QvgZhlvAh66L6yTbjOcyp1RL9PFWJaFTWSHE0MDeKXiGlJaQ==', NULL, NULL, NULL, 0, b'1', NULL, b'0', '0f3a5214-73ff-72eb-3ced-39ed1a431c7d', b'0', b'1', b'1', NULL, 'ADMIN', 'ADMIN@ICONTESTPRO.COM', '570ba182-baaf-4fe2-8240-d82c7a608e12', NULL, NULL, NULL, 'admin', '', NULL, NULL, NULL, NULL, NULL),
(2, '2019-04-10 17:30:38.685644', NULL, '2019-04-11 12:53:47.476252', NULL, b'0', NULL, NULL, NULL, 'admintenant', 1, 'admin@defaulttenant.com', 'admin', 'admin', 'AQAAAAEAACcQAAAAEPpthcgoLblbMd+tHDLHzMpmSc6dSYkcGGf1DYIXpto4ATbWUwkxmhqFMokeA7VUUQ==', NULL, NULL, NULL, 0, b'1', NULL, b'0', 'b2493dc7-af79-8c53-fcdf-39ed1a431e5d', b'0', b'1', b'1', '2019-04-11 12:53:47.462310', 'ADMINTENANT', 'ADMIN@DEFAULTTENANT.COM', 'bd53072f-1558-44c6-83c8-88ce13774223', NULL, NULL, 'Admin', 'Admin', NULL, NULL, NULL, NULL, 'admin-1357-24680', NULL),
(3, '2019-04-11 12:54:42.264069', NULL, '2019-04-11 18:52:57.923907', NULL, b'0', NULL, NULL, NULL, 'fidelisekeh@gmail.com', 1, 'fidelisekeh@gmail.com', '', '', 'AQAAAAEAACcQAAAAEMbzlboe1ZSMjjJJ1cgYXDuoOQpsVKkIyBqQhr0Z1MT0mlK9+KrelZri58LAiJK7Gw==', NULL, NULL, NULL, 0, b'1', '08038814982', b'0', 'FEVHMPTWWTGV3SN3QLV4UQ7KU5UX7OCC', b'0', b'1', b'1', '2019-04-11 18:52:57.018630', 'FIDELISEKEH@GMAIL.COM', 'FIDELISEKEH@GMAIL.COM', 'be7d16ab-b8bb-4bc7-8815-46d552ed5433', 'NG', 5, NULL, 'Fidelis Kalu', NULL, NULL, NULL, NULL, 'fidelis-kalu-89d9b81e2a6e4d36b3e4', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `AbpUserTokens`
--

CREATE TABLE IF NOT EXISTS `AbpUserTokens` (
  `Id` bigint(20) NOT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `LoginProvider` varchar(128) DEFAULT NULL,
  `Name` varchar(128) DEFAULT NULL,
  `Value` varchar(512) DEFAULT NULL,
  `ExpireDate` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AdvertHomePageBanners`
--

CREATE TABLE IF NOT EXISTS `AdvertHomePageBanners` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `StartDate` datetime(6) NOT NULL,
  `EndDate` datetime(6) NOT NULL,
  `Image` varchar(100) NOT NULL,
  `Title` varchar(50) NOT NULL,
  `Description` varchar(150) NOT NULL,
  `StatusId` int(11) NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `AmountPaid` decimal(65,30) NOT NULL,
  `AdvertPriceId` int(11) NOT NULL,
  `ExternalUrl` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AdvertPrices`
--

CREATE TABLE IF NOT EXISTS `AdvertPrices` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Days` int(11) NOT NULL,
  `Amount` decimal(65,30) NOT NULL,
  `Type` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AdvertPrices`
--

INSERT INTO `AdvertPrices` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `TenantId`, `Name`, `Days`, `Amount`, `Type`) VALUES
(1, '2019-04-10 17:30:41.177618', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Day', 1, 3000.000000000000000000000000000000, 2),
(2, '2019-04-10 17:30:41.178166', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Week', 7, 15000.000000000000000000000000000000, 0),
(3, '2019-04-10 17:30:41.178166', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Two Weeks', 14, 25000.000000000000000000000000000000, 0),
(4, '2019-04-10 17:30:41.178166', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Month', 30, 35000.000000000000000000000000000000, 0),
(5, '2019-04-10 17:30:41.178167', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Two Months', 60, 65000.000000000000000000000000000000, 0),
(6, '2019-04-10 17:30:41.178167', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Three Months', 90, 90000.000000000000000000000000000000, 0),
(7, '2019-04-10 17:30:41.178167', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Six Months', 180, 170000.000000000000000000000000000000, 0),
(8, '2019-04-10 17:30:41.178165', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Three Days', 3, 8000.000000000000000000000000000000, 0),
(9, '2019-04-10 17:30:41.178167', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Year', 365, 300000.000000000000000000000000000000, 0),
(10, '2019-04-10 17:30:41.178168', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Three Days', 3, 7500.000000000000000000000000000000, 1),
(11, '2019-04-10 17:30:41.178168', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Week', 7, 14500.000000000000000000000000000000, 1),
(12, '2019-04-10 17:30:41.178168', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Two Weeks', 14, 22000.000000000000000000000000000000, 1),
(13, '2019-04-10 17:30:41.178169', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Month', 30, 30000.000000000000000000000000000000, 1),
(14, '2019-04-10 17:30:41.178169', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Two Months', 60, 60000.000000000000000000000000000000, 1),
(15, '2019-04-10 17:30:41.178170', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Three Months', 90, 85000.000000000000000000000000000000, 1),
(16, '2019-04-10 17:30:41.178168', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Day', 1, 4500.000000000000000000000000000000, 1),
(17, '2019-04-10 17:30:41.178165', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Day', 1, 5000.000000000000000000000000000000, 0),
(18, '2019-04-10 17:30:41.178165', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Year', 365, 100000.000000000000000000000000000000, 3),
(19, '2019-04-10 17:30:41.178165', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Six Months', 180, 50000.000000000000000000000000000000, 3),
(20, '2019-04-10 17:30:41.178148', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Three Days', 3, 4500.000000000000000000000000000000, 2),
(21, '2019-04-10 17:30:41.178153', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Week', 7, 5000.000000000000000000000000000000, 2),
(22, '2019-04-10 17:30:41.178153', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Two Weeks', 14, 7500.000000000000000000000000000000, 2),
(23, '2019-04-10 17:30:41.178154', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Month', 30, 13000.000000000000000000000000000000, 2),
(24, '2019-04-10 17:30:41.178161', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Two Months', 60, 25000.000000000000000000000000000000, 2),
(25, '2019-04-10 17:30:41.178162', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Three Months', 90, 35000.000000000000000000000000000000, 2),
(26, '2019-04-10 17:30:41.178162', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Six Months', 180, 60000.000000000000000000000000000000, 2),
(27, '2019-04-10 17:30:41.178162', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Year', 365, 110000.000000000000000000000000000000, 2),
(28, '2019-04-10 17:30:41.178163', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Day', 1, 1500.000000000000000000000000000000, 3),
(29, '2019-04-10 17:30:41.178163', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Three Days', 3, 3000.000000000000000000000000000000, 3),
(30, '2019-04-10 17:30:41.178163', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Week', 7, 5000.000000000000000000000000000000, 3),
(31, '2019-04-10 17:30:41.178164', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Two Weeks', 14, 7000.000000000000000000000000000000, 3),
(32, '2019-04-10 17:30:41.178164', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Month', 30, 10000.000000000000000000000000000000, 3),
(33, '2019-04-10 17:30:41.178164', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Two Months', 60, 17000.000000000000000000000000000000, 3),
(34, '2019-04-10 17:30:41.178164', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Three Months', 90, 30000.000000000000000000000000000000, 3),
(35, '2019-04-10 17:30:41.178170', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'Six Months', 180, 160000.000000000000000000000000000000, 1),
(36, '2019-04-10 17:30:41.178170', NULL, NULL, NULL, b'0', NULL, NULL, 1, 'One Year', 365, 250000.000000000000000000000000000000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `AdvertTopBanners`
--

CREATE TABLE IF NOT EXISTS `AdvertTopBanners` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `StartDate` datetime(6) NOT NULL,
  `EndDate` datetime(6) NOT NULL,
  `Image` varchar(100) NOT NULL,
  `StatusId` int(11) NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `AmountPaid` decimal(65,30) NOT NULL,
  `AdvertPriceId` int(11) NOT NULL,
  `ExternalUrl` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `BankDetails`
--

CREATE TABLE IF NOT EXISTS `BankDetails` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `BankId` int(11) NOT NULL,
  `AccountName` varchar(100) DEFAULT NULL,
  `AccountNumber` varchar(10) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `IsDefault` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Banks`
--

CREATE TABLE IF NOT EXISTS `Banks` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Code` varchar(255) DEFAULT NULL,
  `Active` bit(1) NOT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Banks`
--

INSERT INTO `Banks` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `Name`, `Code`, `Active`, `TenantId`) VALUES
(1, '2019-04-10 17:30:39.072408', NULL, NULL, NULL, b'0', NULL, NULL, 'Access Bank Plc', '044', b'1', 1),
(2, '2019-04-10 17:30:39.082294', NULL, NULL, NULL, b'0', NULL, NULL, 'Citibank Nigeria Limited', '023', b'1', 1),
(3, '2019-04-10 17:30:39.082965', NULL, NULL, NULL, b'0', NULL, NULL, 'Diamond Bank Plc', '063', b'1', 1),
(4, '2019-04-10 17:30:39.083729', NULL, NULL, NULL, b'0', NULL, NULL, 'Ecobank Nigeria Plc', '050', b'1', 1),
(5, '2019-04-10 17:30:39.084410', NULL, NULL, NULL, b'0', NULL, NULL, 'Fidelity Bank Plc', '070', b'1', 1),
(6, '2019-04-10 17:30:39.084911', NULL, NULL, NULL, b'0', NULL, NULL, 'First Bank of Nigeria Limited', '011', b'1', 1),
(7, '2019-04-10 17:30:39.085398', NULL, NULL, NULL, b'0', NULL, NULL, 'First City Monument Bank Plc', '214', b'1', 1),
(8, '2019-04-10 17:30:39.085899', NULL, NULL, NULL, b'0', NULL, NULL, 'Guaranty Trust Bank Plc', '058', b'1', 1),
(9, '2019-04-10 17:30:39.086481', NULL, NULL, NULL, b'0', NULL, NULL, 'Heritage Bank Limited', '030', b'1', 1),
(10, '2019-04-10 17:30:39.086991', NULL, NULL, NULL, b'0', NULL, NULL, 'Keystone Bank Limited', '082', b'1', 1),
(12, '2019-04-10 17:30:39.087469', NULL, NULL, NULL, b'0', NULL, NULL, 'Providus Bank Limited', '101', b'1', 1),
(13, '2019-04-10 17:30:39.087954', NULL, NULL, NULL, b'0', NULL, NULL, 'Stanbic IBTC Bank Plc', '221', b'1', 1),
(14, '2019-04-10 17:30:39.088455', NULL, NULL, NULL, b'0', NULL, NULL, 'Standard Chartered', '068', b'1', 1),
(15, '2019-04-10 17:30:39.088943', NULL, NULL, NULL, b'0', NULL, NULL, 'Sterling Bank Plc', '232', b'1', 1),
(16, '2019-04-10 17:30:39.089411', NULL, NULL, NULL, b'0', NULL, NULL, 'Union Bank of Nigeria Plc', '032', b'1', 1),
(17, '2019-04-10 17:30:39.089860', NULL, NULL, NULL, b'0', NULL, NULL, 'United Bank for Africa Plc', '033', b'1', 1),
(18, '2019-04-10 17:30:39.090323', NULL, NULL, NULL, b'0', NULL, NULL, 'Unity Bank Plc', '215', b'1', 1),
(19, '2019-04-10 17:30:39.090765', NULL, NULL, NULL, b'0', NULL, NULL, 'Wema Bank Plc', '035', b'1', 1),
(20, '2019-04-10 17:30:39.091242', NULL, NULL, NULL, b'0', NULL, NULL, 'Zenith Bank Plc', '057', b'1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ContestCategories`
--

CREATE TABLE IF NOT EXISTS `ContestCategories` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ContestCategories`
--

INSERT INTO `ContestCategories` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `Name`, `Description`, `TenantId`) VALUES
(1, '2019-04-10 17:30:40.342825', NULL, NULL, NULL, b'0', NULL, NULL, 'Fashion', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ContestComments`
--

CREATE TABLE IF NOT EXISTS `ContestComments` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `ContestId` int(11) NOT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ContestEntries`
--

CREATE TABLE IF NOT EXISTS `ContestEntries` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Image` varchar(255) DEFAULT NULL,
  `Video` varchar(255) DEFAULT NULL,
  `Poster` varchar(50) DEFAULT NULL,
  `Article` longtext,
  `ArticleImage` varchar(50) DEFAULT NULL,
  `Permalink` varchar(255) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `IsFeatured` bit(1) NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `StatusId` int(11) NOT NULL,
  `ContestId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ContestEntryComments`
--

CREATE TABLE IF NOT EXISTS `ContestEntryComments` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `ContestEntryId` int(11) NOT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ContestEntryShares`
--

CREATE TABLE IF NOT EXISTS `ContestEntryShares` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `UserId` bigint(20) DEFAULT NULL,
  `ContestEntryId` int(11) NOT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ContestEntryTypes`
--

CREATE TABLE IF NOT EXISTS `ContestEntryTypes` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ContestEntryTypes`
--

INSERT INTO `ContestEntryTypes` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `Name`, `Description`, `TenantId`) VALUES
(1, '2019-04-10 17:30:39.870610', NULL, NULL, NULL, b'0', NULL, NULL, 'Image', NULL, 1),
(2, '2019-04-10 17:30:39.881901', NULL, NULL, NULL, b'0', NULL, NULL, 'Video', NULL, 1),
(3, '2019-04-10 17:30:39.882709', NULL, NULL, NULL, b'0', NULL, NULL, 'Article', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ContestEntryViews`
--

CREATE TABLE IF NOT EXISTS `ContestEntryViews` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `UserId` bigint(20) DEFAULT NULL,
  `ContestEntryId` int(11) NOT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ContestFees`
--

CREATE TABLE IF NOT EXISTS `ContestFees` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `ContestTypeId` int(11) NOT NULL,
  `CodeBatch` int(11) NOT NULL,
  `Amount` decimal(65,30) NOT NULL,
  `PercentagePerMonth` int(11) NOT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ContestFees`
--

INSERT INTO `ContestFees` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `ContestTypeId`, `CodeBatch`, `Amount`, `PercentagePerMonth`, `TenantId`) VALUES
(1, '2019-04-10 17:30:40.149127', NULL, NULL, NULL, b'0', NULL, NULL, 1, 0, 100000.000000000000000000000000000000, 50, 1),
(2, '2019-04-10 17:30:40.191821', NULL, NULL, NULL, b'0', NULL, NULL, 2, 2500, 150000.000000000000000000000000000000, 0, 1),
(3, '2019-04-10 17:30:40.209137', NULL, NULL, NULL, b'0', NULL, NULL, 3, 0, 50000.000000000000000000000000000000, 50, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ContestPayments`
--

CREATE TABLE IF NOT EXISTS `ContestPayments` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `ContestId` int(11) NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `Amount` decimal(65,30) NOT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Contests`
--

CREATE TABLE IF NOT EXISTS `Contests` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Picture` varchar(50) DEFAULT NULL,
  `Description` varchar(2000) DEFAULT NULL,
  `Objective` varchar(500) DEFAULT NULL,
  `MinimumParticipants` int(11) NOT NULL,
  `StartDate` datetime(6) NOT NULL,
  `EndDate` datetime(6) NOT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `StatusId` int(11) NOT NULL,
  `EveryoneIsAWinnerUrl` varchar(255) DEFAULT NULL,
  `Tags` varchar(100) DEFAULT NULL,
  `Permalink` varchar(255) DEFAULT NULL,
  `EntryAmount` decimal(65,30) DEFAULT NULL,
  `ContestTypeId` int(11) NOT NULL,
  `ContestEntryTypeId` int(11) NOT NULL,
  `ContestCategoryId` int(11) NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `VoteTypeId` int(11) NOT NULL,
  `ContestFeeId` int(11) NOT NULL,
  `SponsorId` int(11) NOT NULL,
  `IsFeatured` bit(1) NOT NULL,
  `PeriodicId` int(11) DEFAULT NULL,
  `NextPaymentDate` datetime(6) DEFAULT NULL,
  `VoteAmount` decimal(65,30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ContestTypes`
--

CREATE TABLE IF NOT EXISTS `ContestTypes` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ContestTypes`
--

INSERT INTO `ContestTypes` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `Name`, `Description`, `TenantId`) VALUES
(1, '2019-04-10 17:30:40.010756', NULL, NULL, NULL, b'0', NULL, NULL, 'Paid', NULL, 1),
(2, '2019-04-10 17:30:40.025033', NULL, NULL, NULL, b'0', NULL, NULL, 'Coded', NULL, 1),
(3, '2019-04-10 17:30:40.025699', NULL, NULL, NULL, b'0', NULL, NULL, 'Free', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Countries`
--

CREATE TABLE IF NOT EXISTS `Countries` (
  `Id` varchar(255) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Iso` longtext,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Countries`
--

INSERT INTO `Countries` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `Name`, `Iso`, `TenantId`) VALUES
('AD', '2019-04-10 17:30:39.245503', NULL, NULL, NULL, b'0', NULL, NULL, 'Andorra', '+376', 1),
('AE', '2019-04-10 17:30:39.485100', NULL, NULL, NULL, b'0', NULL, NULL, 'United Arab Emirates', '+971', 1),
('AF', '2019-04-10 17:30:39.221847', NULL, NULL, NULL, b'0', NULL, NULL, 'Afghanistan', '+93', 1),
('AG', '2019-04-10 17:30:39.249577', NULL, NULL, NULL, b'0', NULL, NULL, 'Antigua and Barbuda', '+1268', 1),
('AI', '2019-04-10 17:30:39.248458', NULL, NULL, NULL, b'0', NULL, NULL, 'Anguilla', '+1 264', 1),
('AL', '2019-04-10 17:30:39.241827', NULL, NULL, NULL, b'0', NULL, NULL, 'Albania', '+355', 1),
('AM', '2019-04-10 17:30:39.252148', NULL, NULL, NULL, b'0', NULL, NULL, 'Armenia', '+374', 1),
('AN', '2019-04-10 17:30:39.414972', NULL, NULL, NULL, b'0', NULL, NULL, 'Netherlands Antilles', '+599', 1),
('AO', '2019-04-10 17:30:39.247276', NULL, NULL, NULL, b'0', NULL, NULL, 'Angola', '+244', 1),
('AQ', '2019-04-10 17:30:39.498588', NULL, NULL, NULL, b'0', NULL, NULL, 'Antarctica', '+672', 1),
('AR', '2019-04-10 17:30:39.250886', NULL, NULL, NULL, b'0', NULL, NULL, 'Argentina', '+54', 1),
('AS', '2019-04-10 17:30:39.244396', NULL, NULL, NULL, b'0', NULL, NULL, 'AmericanSamoa', '+1 684', 1),
('AT', '2019-04-10 17:30:39.255510', NULL, NULL, NULL, b'0', NULL, NULL, 'Austria', '+43', 1),
('AU', '2019-04-10 17:30:39.254394', NULL, NULL, NULL, b'0', NULL, NULL, 'Australia', '+61', 1),
('AW', '2019-04-10 17:30:39.253267', NULL, NULL, NULL, b'0', NULL, NULL, 'Aruba', '+297', 1),
('AX', '2019-04-10 17:30:39.497148', NULL, NULL, NULL, b'0', NULL, NULL, 'Åland Islands', '+358', 1),
('AZ', '2019-04-10 17:30:39.256722', NULL, NULL, NULL, b'0', NULL, NULL, 'Azerbaijan', '+994', 1),
('BA', '2019-04-10 17:30:39.271027', NULL, NULL, NULL, b'0', NULL, NULL, 'Bosnia and Herzegovina', '+387', 1),
('BB', '2019-04-10 17:30:39.261905', NULL, NULL, NULL, b'0', NULL, NULL, 'Barbados', '+1 246', 1),
('BD', '2019-04-10 17:30:39.260686', NULL, NULL, NULL, b'0', NULL, NULL, 'Bangladesh', '+880', 1),
('BE', '2019-04-10 17:30:39.264286', NULL, NULL, NULL, b'0', NULL, NULL, 'Belgium', '+32', 1),
('BF', '2019-04-10 17:30:39.276952', NULL, NULL, NULL, b'0', NULL, NULL, 'Burkina Faso', '+226', 1),
('BG', '2019-04-10 17:30:39.275758', NULL, NULL, NULL, b'0', NULL, NULL, 'Bulgaria', '+359', 1),
('BH', '2019-04-10 17:30:39.259550', NULL, NULL, NULL, b'0', NULL, NULL, 'Bahrain', '+973', 1),
('BI', '2019-04-10 17:30:39.278125', NULL, NULL, NULL, b'0', NULL, NULL, 'Burundi', '+257', 1),
('BJ', '2019-04-10 17:30:39.266960', NULL, NULL, NULL, b'0', NULL, NULL, 'Benin', '+229', 1),
('BL', '2019-04-10 17:30:39.535581', NULL, NULL, NULL, b'0', NULL, NULL, 'Saint Barthélemy', '+590', 1),
('BM', '2019-04-10 17:30:39.268339', NULL, NULL, NULL, b'0', NULL, NULL, 'Bermuda', '+1 441', 1),
('BN', '2019-04-10 17:30:39.501816', NULL, NULL, NULL, b'0', NULL, NULL, 'Brunei Darussalam', '+673', 1),
('BO', '2019-04-10 17:30:39.499906', NULL, NULL, NULL, b'0', NULL, NULL, 'Bolivia, Plurinational State of', '+591', 1),
('BR', '2019-04-10 17:30:39.273397', NULL, NULL, NULL, b'0', NULL, NULL, 'Brazil', '+55', 1),
('BS', '2019-04-10 17:30:39.258350', NULL, NULL, NULL, b'0', NULL, NULL, 'Bahamas', '+1 242', 1),
('BT', '2019-04-10 17:30:39.269789', NULL, NULL, NULL, b'0', NULL, NULL, 'Bhutan', '+975', 1),
('BW', '2019-04-10 17:30:39.272176', NULL, NULL, NULL, b'0', NULL, NULL, 'Botswana', '+267', 1),
('BY', '2019-04-10 17:30:39.263124', NULL, NULL, NULL, b'0', NULL, NULL, 'Belarus', '+375', 1),
('BZ', '2019-04-10 17:30:39.265706', NULL, NULL, NULL, b'0', NULL, NULL, 'Belize', '+501', 1),
('CA', '2019-04-10 17:30:39.281929', NULL, NULL, NULL, b'0', NULL, NULL, 'Canada', '+1', 1),
('CC', '2019-04-10 17:30:39.503332', NULL, NULL, NULL, b'0', NULL, NULL, 'Cocos (Keeling) Islands', '+61', 1),
('CD', '2019-04-10 17:30:39.506011', NULL, NULL, NULL, b'0', NULL, NULL, 'Congo, The Democratic Republic of the', '+243', 1),
('CF', '2019-04-10 17:30:39.285881', NULL, NULL, NULL, b'0', NULL, NULL, 'Central African Republic', '+236', 1),
('CG', '2019-04-10 17:30:39.294538', NULL, NULL, NULL, b'0', NULL, NULL, 'Congo', '+242', 1),
('CH', '2019-04-10 17:30:39.467944', NULL, NULL, NULL, b'0', NULL, NULL, 'Switzerland', '+41', 1),
('CI', '2019-04-10 17:30:39.508519', NULL, NULL, NULL, b'0', NULL, NULL, 'Cote d''Ivoire', '+225', 1),
('CK', '2019-04-10 17:30:39.295753', NULL, NULL, NULL, b'0', NULL, NULL, 'Cook Islands', '+682', 1),
('CL', '2019-04-10 17:30:39.288337', NULL, NULL, NULL, b'0', NULL, NULL, 'Chile', '+56', 1),
('CM', '2019-04-10 17:30:39.280446', NULL, NULL, NULL, b'0', NULL, NULL, 'Cameroon', '+237', 1),
('CN', '2019-04-10 17:30:39.289652', NULL, NULL, NULL, b'0', NULL, NULL, 'China', '+86', 1),
('CO', '2019-04-10 17:30:39.291953', NULL, NULL, NULL, b'0', NULL, NULL, 'Colombia', '+57', 1),
('CR', '2019-04-10 17:30:39.297213', NULL, NULL, NULL, b'0', NULL, NULL, 'Costa Rica', '+506', 1),
('CU', '2019-04-10 17:30:39.300179', NULL, NULL, NULL, b'0', NULL, NULL, 'Cuba', '+53', 1),
('CV', '2019-04-10 17:30:39.283129', NULL, NULL, NULL, b'0', NULL, NULL, 'Cape Verde', '+238', 1),
('CX', '2019-04-10 17:30:39.290837', NULL, NULL, NULL, b'0', NULL, NULL, 'Christmas Island', '+61', 1),
('CY', '2019-04-10 17:30:39.302019', NULL, NULL, NULL, b'0', NULL, NULL, 'Cyprus', '+537', 1),
('CZ', '2019-04-10 17:30:39.303247', NULL, NULL, NULL, b'0', NULL, NULL, 'Czech Republic', '+420', 1),
('DE', '2019-04-10 17:30:39.332471', NULL, NULL, NULL, b'0', NULL, NULL, 'Germany', '+49', 1),
('DJ', '2019-04-10 17:30:39.305828', NULL, NULL, NULL, b'0', NULL, NULL, 'Djibouti', '+253', 1),
('DK', '2019-04-10 17:30:39.304663', NULL, NULL, NULL, b'0', NULL, NULL, 'Denmark', '+45', 1),
('DM', '2019-04-10 17:30:39.306976', NULL, NULL, NULL, b'0', NULL, NULL, 'Dominica', '+1 767', 1),
('DO', '2019-04-10 17:30:39.308665', NULL, NULL, NULL, b'0', NULL, NULL, 'Dominican Republic', '+1 849', 1),
('DZ', '2019-04-10 17:30:39.243147', NULL, NULL, NULL, b'0', NULL, NULL, 'Algeria', '+213', 1),
('EC', '2019-04-10 17:30:39.310560', NULL, NULL, NULL, b'0', NULL, NULL, 'Ecuador', '+593', 1),
('EE', '2019-04-10 17:30:39.318140', NULL, NULL, NULL, b'0', NULL, NULL, 'Estonia', '+372', 1),
('EG', '2019-04-10 17:30:39.312125', NULL, NULL, NULL, b'0', NULL, NULL, 'Egypt', '+20', 1),
('ER', '2019-04-10 17:30:39.316694', NULL, NULL, NULL, b'0', NULL, NULL, 'Eritrea', '+291', 1),
('ES', '2019-04-10 17:30:39.460538', NULL, NULL, NULL, b'0', NULL, NULL, 'Spain', '+34', 1),
('ET', '2019-04-10 17:30:39.319635', NULL, NULL, NULL, b'0', NULL, NULL, 'Ethiopia', '+251', 1),
('FI', '2019-04-10 17:30:39.323726', NULL, NULL, NULL, b'0', NULL, NULL, 'Finland', '+358', 1),
('FJ', '2019-04-10 17:30:39.322353', NULL, NULL, NULL, b'0', NULL, NULL, 'Fiji', '+679', 1),
('FK', '2019-04-10 17:30:39.510313', NULL, NULL, NULL, b'0', NULL, NULL, 'Falkland Islands (Malvinas)', '+500', 1),
('FM', '2019-04-10 17:30:39.527155', NULL, NULL, NULL, b'0', NULL, NULL, 'Micronesia, Federated States of', '+691', 1),
('FO', '2019-04-10 17:30:39.320922', NULL, NULL, NULL, b'0', NULL, NULL, 'Faroe Islands', '+298', 1),
('FR', '2019-04-10 17:30:39.324997', NULL, NULL, NULL, b'0', NULL, NULL, 'France', '+33', 1),
('GA', '2019-04-10 17:30:39.328891', NULL, NULL, NULL, b'0', NULL, NULL, 'Gabon', '+241', 1),
('GB', '2019-04-10 17:30:39.486280', NULL, NULL, NULL, b'0', NULL, NULL, 'United Kingdom', '+44', 1),
('GD', '2019-04-10 17:30:39.338709', NULL, NULL, NULL, b'0', NULL, NULL, 'Grenada', '+1 473', 1),
('GE', '2019-04-10 17:30:39.331266', NULL, NULL, NULL, b'0', NULL, NULL, 'Georgia', '+995', 1),
('GF', '2019-04-10 17:30:39.326116', NULL, NULL, NULL, b'0', NULL, NULL, 'French Guiana', '+594', 1),
('GG', '2019-04-10 17:30:39.511734', NULL, NULL, NULL, b'0', NULL, NULL, 'Guernsey', '+44', 1),
('GH', '2019-04-10 17:30:39.333840', NULL, NULL, NULL, b'0', NULL, NULL, 'Ghana', '+233', 1),
('GI', '2019-04-10 17:30:39.334984', NULL, NULL, NULL, b'0', NULL, NULL, 'Gibraltar', '+350', 1),
('GL', '2019-04-10 17:30:39.337410', NULL, NULL, NULL, b'0', NULL, NULL, 'Greenland', '+299', 1),
('GM', '2019-04-10 17:30:39.330089', NULL, NULL, NULL, b'0', NULL, NULL, 'Gambia', '+220', 1),
('GN', '2019-04-10 17:30:39.343540', NULL, NULL, NULL, b'0', NULL, NULL, 'Guinea', '+224', 1),
('GP', '2019-04-10 17:30:39.340015', NULL, NULL, NULL, b'0', NULL, NULL, 'Guadeloupe', '+590', 1),
('GQ', '2019-04-10 17:30:39.315223', NULL, NULL, NULL, b'0', NULL, NULL, 'Equatorial Guinea', '+240', 1),
('GR', '2019-04-10 17:30:39.336130', NULL, NULL, NULL, b'0', NULL, NULL, 'Greece', '+30', 1),
('GS', '2019-04-10 17:30:39.459339', NULL, NULL, NULL, b'0', NULL, NULL, 'South Georgia and the South Sandwich Islands', '+500', 1),
('GT', '2019-04-10 17:30:39.342294', NULL, NULL, NULL, b'0', NULL, NULL, 'Guatemala', '+502', 1),
('GU', '2019-04-10 17:30:39.341166', NULL, NULL, NULL, b'0', NULL, NULL, 'Guam', '+1 671', 1),
('GW', '2019-04-10 17:30:39.344648', NULL, NULL, NULL, b'0', NULL, NULL, 'Guinea-Bissau', '+245', 1),
('GY', '2019-04-10 17:30:39.345778', NULL, NULL, NULL, b'0', NULL, NULL, 'Guyana', '+595', 1),
('HK', '2019-04-10 17:30:39.514259', NULL, NULL, NULL, b'0', NULL, NULL, 'Hong Kong', '+852', 1),
('HN', '2019-04-10 17:30:39.348573', NULL, NULL, NULL, b'0', NULL, NULL, 'Honduras', '+504', 1),
('HR', '2019-04-10 17:30:39.298711', NULL, NULL, NULL, b'0', NULL, NULL, 'Croatia', '+385', 1),
('HT', '2019-04-10 17:30:39.347132', NULL, NULL, NULL, b'0', NULL, NULL, 'Haiti', '+509', 1),
('HU', '2019-04-10 17:30:39.349756', NULL, NULL, NULL, b'0', NULL, NULL, 'Hungary', '+36', 1),
('ID', '2019-04-10 17:30:39.353637', NULL, NULL, NULL, b'0', NULL, NULL, 'Indonesia', '+62', 1),
('IE', '2019-04-10 17:30:39.355917', NULL, NULL, NULL, b'0', NULL, NULL, 'Ireland', '+353', 1),
('IL', '2019-04-10 17:30:39.357369', NULL, NULL, NULL, b'0', NULL, NULL, 'Israel', '+972', 1),
('IM', '2019-04-10 17:30:39.517143', NULL, NULL, NULL, b'0', NULL, NULL, 'Isle of Man', '+44', 1),
('IN', '2019-04-10 17:30:39.352431', NULL, NULL, NULL, b'0', NULL, NULL, 'India', '+91', 1),
('IO', '2019-04-10 17:30:39.274563', NULL, NULL, NULL, b'0', NULL, NULL, 'British Indian Ocean Territory', '+246', 1),
('IQ', '2019-04-10 17:30:39.354797', NULL, NULL, NULL, b'0', NULL, NULL, 'Iraq', '+964', 1),
('IR', '2019-04-10 17:30:39.515763', NULL, NULL, NULL, b'0', NULL, NULL, 'Iran, Islamic Republic of', '+98', 1),
('IS', '2019-04-10 17:30:39.351177', NULL, NULL, NULL, b'0', NULL, NULL, 'Iceland', '+354', 1),
('IT', '2019-04-10 17:30:39.358772', NULL, NULL, NULL, b'0', NULL, NULL, 'Italy', '+39', 1),
('JE', '2019-04-10 17:30:39.518520', NULL, NULL, NULL, b'0', NULL, NULL, 'Jersey', '+44', 1),
('JM', '2019-04-10 17:30:39.360183', NULL, NULL, NULL, b'0', NULL, NULL, 'Jamaica', '+1 876', 1),
('JO', '2019-04-10 17:30:39.362837', NULL, NULL, NULL, b'0', NULL, NULL, 'Jordan', '+962', 1),
('JP', '2019-04-10 17:30:39.361446', NULL, NULL, NULL, b'0', NULL, NULL, 'Japan', '+81', 1),
('KE', '2019-04-10 17:30:39.365408', NULL, NULL, NULL, b'0', NULL, NULL, 'Kenya', '+254', 1),
('KG', '2019-04-10 17:30:39.369309', NULL, NULL, NULL, b'0', NULL, NULL, 'Kyrgyzstan', '+996', 1),
('KH', '2019-04-10 17:30:39.279246', NULL, NULL, NULL, b'0', NULL, NULL, 'Cambodia', '+855', 1),
('KI', '2019-04-10 17:30:39.366559', NULL, NULL, NULL, b'0', NULL, NULL, 'Kiribati', '+686', 1),
('KM', '2019-04-10 17:30:39.293336', NULL, NULL, NULL, b'0', NULL, NULL, 'Comoros', '+269', 1),
('KN', '2019-04-10 17:30:39.538255', NULL, NULL, NULL, b'0', NULL, NULL, 'Saint Kitts and Nevis', '+1 869', 1),
('KP', '2019-04-10 17:30:39.519725', NULL, NULL, NULL, b'0', NULL, NULL, 'Korea, Democratic People''s Republic of', '+850', 1),
('KR', '2019-04-10 17:30:39.520872', NULL, NULL, NULL, b'0', NULL, NULL, 'Korea, Republic of', '+82', 1),
('KW', '2019-04-10 17:30:39.368029', NULL, NULL, NULL, b'0', NULL, NULL, 'Kuwait', '+965', 1),
('KY', '2019-04-10 17:30:39.284455', NULL, NULL, NULL, b'0', NULL, NULL, 'Cayman Islands', '+ 345', 1),
('KZ', '2019-04-10 17:30:39.364138', NULL, NULL, NULL, b'0', NULL, NULL, 'Kazakhstan', '+7 7', 1),
('LA', '2019-04-10 17:30:39.522106', NULL, NULL, NULL, b'0', NULL, NULL, 'Lao People''s Democratic Republic', '+856', 1),
('LB', '2019-04-10 17:30:39.371683', NULL, NULL, NULL, b'0', NULL, NULL, 'Lebanon', '+961', 1),
('LC', '2019-04-10 17:30:39.539509', NULL, NULL, NULL, b'0', NULL, NULL, 'Saint Lucia', '+1 758', 1),
('LI', '2019-04-10 17:30:39.376730', NULL, NULL, NULL, b'0', NULL, NULL, 'Liechtenstein', '+423', 1),
('LK', '2019-04-10 17:30:39.461742', NULL, NULL, NULL, b'0', NULL, NULL, 'Sri Lanka', '+94', 1),
('LR', '2019-04-10 17:30:39.374547', NULL, NULL, NULL, b'0', NULL, NULL, 'Liberia', '+231', 1),
('LS', '2019-04-10 17:30:39.372840', NULL, NULL, NULL, b'0', NULL, NULL, 'Lesotho', '+266', 1),
('LT', '2019-04-10 17:30:39.378725', NULL, NULL, NULL, b'0', NULL, NULL, 'Lithuania', '+370', 1),
('LU', '2019-04-10 17:30:39.380195', NULL, NULL, NULL, b'0', NULL, NULL, 'Luxembourg', '+352', 1),
('LV', '2019-04-10 17:30:39.370528', NULL, NULL, NULL, b'0', NULL, NULL, 'Latvia', '+371', 1),
('LY', '2019-04-10 17:30:39.523268', NULL, NULL, NULL, b'0', NULL, NULL, 'Libyan Arab Jamahiriya', '+218', 1),
('MA', '2019-04-10 17:30:39.406872', NULL, NULL, NULL, b'0', NULL, NULL, 'Morocco', '+212', 1),
('MC', '2019-04-10 17:30:39.401310', NULL, NULL, NULL, b'0', NULL, NULL, 'Monaco', '+377', 1),
('MD', '2019-04-10 17:30:39.528483', NULL, NULL, NULL, b'0', NULL, NULL, 'Moldova, Republic of', '+373', 1),
('ME', '2019-04-10 17:30:39.404246', NULL, NULL, NULL, b'0', NULL, NULL, 'Montenegro', '+382', 1),
('MF', '2019-04-10 17:30:39.540787', NULL, NULL, NULL, b'0', NULL, NULL, 'Saint Martin', '+590', 1),
('MG', '2019-04-10 17:30:39.381614', NULL, NULL, NULL, b'0', NULL, NULL, 'Madagascar', '+261', 1),
('MH', '2019-04-10 17:30:39.390695', NULL, NULL, NULL, b'0', NULL, NULL, 'Marshall Islands', '+692', 1),
('MK', '2019-04-10 17:30:39.525975', NULL, NULL, NULL, b'0', NULL, NULL, 'Macedonia, The Former Yugoslav Republic of', '+389', 1),
('ML', '2019-04-10 17:30:39.386840', NULL, NULL, NULL, b'0', NULL, NULL, 'Mali', '+223', 1),
('MM', '2019-04-10 17:30:39.408696', NULL, NULL, NULL, b'0', NULL, NULL, 'Myanmar', '+95', 1),
('MN', '2019-04-10 17:30:39.402808', NULL, NULL, NULL, b'0', NULL, NULL, 'Mongolia', '+976', 1),
('MO', '2019-04-10 17:30:39.524577', NULL, NULL, NULL, b'0', NULL, NULL, 'Macao', '+853', 1),
('MP', '2019-04-10 17:30:39.423701', NULL, NULL, NULL, b'0', NULL, NULL, 'Northern Mariana Islands', '+1670', 1),
('MQ', '2019-04-10 17:30:39.392112', NULL, NULL, NULL, b'0', NULL, NULL, 'Martinique', '+596', 1),
('MR', '2019-04-10 17:30:39.394035', NULL, NULL, NULL, b'0', NULL, NULL, 'Mauritania', '+222', 1),
('MS', '2019-04-10 17:30:39.405395', NULL, NULL, NULL, b'0', NULL, NULL, 'Montserrat', '+1664', 1),
('MT', '2019-04-10 17:30:39.389231', NULL, NULL, NULL, b'0', NULL, NULL, 'Malta', '+356', 1),
('MU', '2019-04-10 17:30:39.397383', NULL, NULL, NULL, b'0', NULL, NULL, 'Mauritius', '+230', 1),
('MV', '2019-04-10 17:30:39.385550', NULL, NULL, NULL, b'0', NULL, NULL, 'Maldives', '+960', 1),
('MW', '2019-04-10 17:30:39.382919', NULL, NULL, NULL, b'0', NULL, NULL, 'Malawi', '+265', 1),
('MX', '2019-04-10 17:30:39.400138', NULL, NULL, NULL, b'0', NULL, NULL, 'Mexico', '+52', 1),
('MY', '2019-04-10 17:30:39.384271', NULL, NULL, NULL, b'0', NULL, NULL, 'Malaysia', '+60', 1),
('MZ', '2019-04-10 17:30:39.529698', NULL, NULL, NULL, b'0', NULL, NULL, 'Mozambique', '+258', 1),
('NA', '2019-04-10 17:30:39.410032', NULL, NULL, NULL, b'0', NULL, NULL, 'Namibia', '+264', 1),
('NC', '2019-04-10 17:30:39.416236', NULL, NULL, NULL, b'0', NULL, NULL, 'New Caledonia', '+687', 1),
('NE', '2019-04-10 17:30:39.420137', NULL, NULL, NULL, b'0', NULL, NULL, 'Niger', '+227', 1),
('NG', '2019-04-10 17:30:39.421433', NULL, NULL, NULL, b'0', NULL, NULL, 'Nigeria', '+234', 1),
('NI', '2019-04-10 17:30:39.418878', NULL, NULL, NULL, b'0', NULL, NULL, 'Nicaragua', '+505', 1),
('NL', '2019-04-10 17:30:39.413785', NULL, NULL, NULL, b'0', NULL, NULL, 'Netherlands', '+31', 1),
('NO', '2019-04-10 17:30:39.424868', NULL, NULL, NULL, b'0', NULL, NULL, 'Norway', '+47', 1),
('NP', '2019-04-10 17:30:39.412609', NULL, NULL, NULL, b'0', NULL, NULL, 'Nepal', '+977', 1),
('NR', '2019-04-10 17:30:39.411293', NULL, NULL, NULL, b'0', NULL, NULL, 'Nauru', '+674', 1),
('NU', '2019-04-10 17:30:39.422592', NULL, NULL, NULL, b'0', NULL, NULL, 'Niue', '+683', 1),
('NZ', '2019-04-10 17:30:39.417532', NULL, NULL, NULL, b'0', NULL, NULL, 'New Zealand', '+64', 1),
('OM', '2019-04-10 17:30:39.425989', NULL, NULL, NULL, b'0', NULL, NULL, 'Oman', '+968', 1),
('PA', '2019-04-10 17:30:39.429835', NULL, NULL, NULL, b'0', NULL, NULL, 'Panama', '+507', 1),
('PE', '2019-04-10 17:30:39.434443', NULL, NULL, NULL, b'0', NULL, NULL, 'Peru', '+51', 1),
('PF', '2019-04-10 17:30:39.327620', NULL, NULL, NULL, b'0', NULL, NULL, 'French Polynesia', '+689', 1),
('PG', '2019-04-10 17:30:39.431076', NULL, NULL, NULL, b'0', NULL, NULL, 'Papua New Guinea', '+675', 1),
('PH', '2019-04-10 17:30:39.435721', NULL, NULL, NULL, b'0', NULL, NULL, 'Philippines', '+63', 1),
('PK', '2019-04-10 17:30:39.427338', NULL, NULL, NULL, b'0', NULL, NULL, 'Pakistan', '+92', 1),
('PL', '2019-04-10 17:30:39.436889', NULL, NULL, NULL, b'0', NULL, NULL, 'Poland', '+48', 1),
('PM', '2019-04-10 17:30:39.542004', NULL, NULL, NULL, b'0', NULL, NULL, 'Saint Pierre and Miquelon', '+508', 1),
('PN', '2019-04-10 17:30:39.532018', NULL, NULL, NULL, b'0', NULL, NULL, 'Pitcairn', '+872', 1),
('PR', '2019-04-10 17:30:39.439268', NULL, NULL, NULL, b'0', NULL, NULL, 'Puerto Rico', '+1939', 1),
('PS', '2019-04-10 17:30:39.530867', NULL, NULL, NULL, b'0', NULL, NULL, 'Palestinian Territory, Occupied', '+970', 1),
('PT', '2019-04-10 17:30:39.438158', NULL, NULL, NULL, b'0', NULL, NULL, 'Portugal', '+351', 1),
('PW', '2019-04-10 17:30:39.428644', NULL, NULL, NULL, b'0', NULL, NULL, 'Palau', '+680', 1),
('PY', '2019-04-10 17:30:39.432960', NULL, NULL, NULL, b'0', NULL, NULL, 'Paraguay', '+595', 1),
('QA', '2019-04-10 17:30:39.440461', NULL, NULL, NULL, b'0', NULL, NULL, 'Qatar', '+974', 1),
('RE', '2019-04-10 17:30:39.533212', NULL, NULL, NULL, b'0', NULL, NULL, 'Réunion', '+262', 1),
('RO', '2019-04-10 17:30:39.441732', NULL, NULL, NULL, b'0', NULL, NULL, 'Romania', '+40', 1),
('RS', '2019-04-10 17:30:39.449267', NULL, NULL, NULL, b'0', NULL, NULL, 'Serbia', '+381', 1),
('RU', '2019-04-10 17:30:39.534337', NULL, NULL, NULL, b'0', NULL, NULL, 'Russia', '+7', 1),
('RW', '2019-04-10 17:30:39.442885', NULL, NULL, NULL, b'0', NULL, NULL, 'Rwanda', '+250', 1),
('SA', '2019-04-10 17:30:39.446727', NULL, NULL, NULL, b'0', NULL, NULL, 'Saudi Arabia', '+966', 1),
('SB', '2019-04-10 17:30:39.456781', NULL, NULL, NULL, b'0', NULL, NULL, 'Solomon Islands', '+677', 1),
('SC', '2019-04-10 17:30:39.450669', NULL, NULL, NULL, b'0', NULL, NULL, 'Seychelles', '+248', 1),
('SD', '2019-04-10 17:30:39.462837', NULL, NULL, NULL, b'0', NULL, NULL, 'Sudan', '+249', 1),
('SE', '2019-04-10 17:30:39.466392', NULL, NULL, NULL, b'0', NULL, NULL, 'Sweden', '+46', 1),
('SG', '2019-04-10 17:30:39.452967', NULL, NULL, NULL, b'0', NULL, NULL, 'Singapore', '+65', 1),
('SH', '2019-04-10 17:30:39.536935', NULL, NULL, NULL, b'0', NULL, NULL, 'Saint Helena, Ascension and Tristan Da Cunha', '+290', 1),
('SI', '2019-04-10 17:30:39.455464', NULL, NULL, NULL, b'0', NULL, NULL, 'Slovenia', '+386', 1),
('SJ', '2019-04-10 17:30:39.546601', NULL, NULL, NULL, b'0', NULL, NULL, 'Svalbard and Jan Mayen', '+47', 1),
('SK', '2019-04-10 17:30:39.454079', NULL, NULL, NULL, b'0', NULL, NULL, 'Slovakia', '+421', 1),
('SL', '2019-04-10 17:30:39.451851', NULL, NULL, NULL, b'0', NULL, NULL, 'Sierra Leone', '+232', 1),
('SM', '2019-04-10 17:30:39.445499', NULL, NULL, NULL, b'0', NULL, NULL, 'San Marino', '+378', 1),
('SN', '2019-04-10 17:30:39.448084', NULL, NULL, NULL, b'0', NULL, NULL, 'Senegal', '+221', 1),
('SO', '2019-04-10 17:30:39.545466', NULL, NULL, NULL, b'0', NULL, NULL, 'Somalia', '+252', 1),
('SR', '2019-04-10 17:30:39.463938', NULL, NULL, NULL, b'0', NULL, NULL, 'Suriname', '+597', 1),
('ST', '2019-04-10 17:30:39.544271', NULL, NULL, NULL, b'0', NULL, NULL, 'Sao Tome and Principe', '+239', 1),
('SV', '2019-04-10 17:30:39.313559', NULL, NULL, NULL, b'0', NULL, NULL, 'El Salvador', '+503', 1),
('SY', '2019-04-10 17:30:39.547856', NULL, NULL, NULL, b'0', NULL, NULL, 'Syrian Arab Republic', '+963', 1),
('SZ', '2019-04-10 17:30:39.465135', NULL, NULL, NULL, b'0', NULL, NULL, 'Swaziland', '+268', 1),
('TC', '2019-04-10 17:30:39.480224', NULL, NULL, NULL, b'0', NULL, NULL, 'Turks and Caicos Islands', '+1 649', 1),
('TD', '2019-04-10 17:30:39.287100', NULL, NULL, NULL, b'0', NULL, NULL, 'Chad', '+235', 1),
('TG', '2019-04-10 17:30:39.471626', NULL, NULL, NULL, b'0', NULL, NULL, 'Togo', '+228', 1),
('TH', '2019-04-10 17:30:39.470490', NULL, NULL, NULL, b'0', NULL, NULL, 'Thailand', '+66', 1),
('TJ', '2019-04-10 17:30:39.469250', NULL, NULL, NULL, b'0', NULL, NULL, 'Tajikistan', '+992', 1),
('TK', '2019-04-10 17:30:39.472757', NULL, NULL, NULL, b'0', NULL, NULL, 'Tokelau', '+690', 1),
('TL', '2019-04-10 17:30:39.551698', NULL, NULL, NULL, b'0', NULL, NULL, 'Timor-Leste', '+670', 1),
('TM', '2019-04-10 17:30:39.479000', NULL, NULL, NULL, b'0', NULL, NULL, 'Turkmenistan', '+993', 1),
('TN', '2019-04-10 17:30:39.476268', NULL, NULL, NULL, b'0', NULL, NULL, 'Tunisia', '+216', 1),
('TO', '2019-04-10 17:30:39.473940', NULL, NULL, NULL, b'0', NULL, NULL, 'Tonga', '+676', 1),
('TR', '2019-04-10 17:30:39.477537', NULL, NULL, NULL, b'0', NULL, NULL, 'Turkey', '+90', 1),
('TT', '2019-04-10 17:30:39.475153', NULL, NULL, NULL, b'0', NULL, NULL, 'Trinidad and Tobago', '+1 868', 1),
('TV', '2019-04-10 17:30:39.481426', NULL, NULL, NULL, b'0', NULL, NULL, 'Tuvalu', '+688', 1),
('TW', '2019-04-10 17:30:39.549205', NULL, NULL, NULL, b'0', NULL, NULL, 'Taiwan, Province of China', '+886', 1),
('TZ', '2019-04-10 17:30:39.550504', NULL, NULL, NULL, b'0', NULL, NULL, 'Tanzania, United Republic of', '+255', 1),
('UA', '2019-04-10 17:30:39.483888', NULL, NULL, NULL, b'0', NULL, NULL, 'Ukraine', '+380', 1),
('UG', '2019-04-10 17:30:39.482542', NULL, NULL, NULL, b'0', NULL, NULL, 'Uganda', '+256', 1),
('US', '2019-04-10 17:30:39.487530', NULL, NULL, NULL, b'0', NULL, NULL, 'United States', '+1', 1),
('UY', '2019-04-10 17:30:39.488809', NULL, NULL, NULL, b'0', NULL, NULL, 'Uruguay', '+598', 1),
('UZ', '2019-04-10 17:30:39.490212', NULL, NULL, NULL, b'0', NULL, NULL, 'Uzbekistan', '+998', 1),
('VA', '2019-04-10 17:30:39.513036', NULL, NULL, NULL, b'0', NULL, NULL, 'Holy See (Vatican City State)', '+379', 1),
('VC', '2019-04-10 17:30:39.543123', NULL, NULL, NULL, b'0', NULL, NULL, 'Saint Vincent and the Grenadines', '+1 784', 1),
('VE', '2019-04-10 17:30:39.552848', NULL, NULL, NULL, b'0', NULL, NULL, 'Venezuela, Bolivarian Republic of', '+58', 1),
('VG', '2019-04-10 17:30:39.555155', NULL, NULL, NULL, b'0', NULL, NULL, 'Virgin Islands, British', '+1 284', 1),
('VI', '2019-04-10 17:30:39.556319', NULL, NULL, NULL, b'0', NULL, NULL, 'Virgin Islands, U.S.', '+1 340', 1),
('VN', '2019-04-10 17:30:39.553960', NULL, NULL, NULL, b'0', NULL, NULL, 'Viet Nam', '+84', 1),
('VU', '2019-04-10 17:30:39.491358', NULL, NULL, NULL, b'0', NULL, NULL, 'Vanuatu', '+678', 1),
('WF', '2019-04-10 17:30:39.492498', NULL, NULL, NULL, b'0', NULL, NULL, 'Wallis and Futuna', '+681', 1),
('WS', '2019-04-10 17:30:39.444244', NULL, NULL, NULL, b'0', NULL, NULL, 'Samoa', '+685', 1),
('YE', '2019-04-10 17:30:39.493688', NULL, NULL, NULL, b'0', NULL, NULL, 'Yemen', '+967', 1),
('YT', '2019-04-10 17:30:39.398836', NULL, NULL, NULL, b'0', NULL, NULL, 'Mayotte', '+262', 1),
('ZA', '2019-04-10 17:30:39.458121', NULL, NULL, NULL, b'0', NULL, NULL, 'South Africa', '+27', 1),
('ZM', '2019-04-10 17:30:39.494857', NULL, NULL, NULL, b'0', NULL, NULL, 'Zambia', '+260', 1),
('ZW', '2019-04-10 17:30:39.495963', NULL, NULL, NULL, b'0', NULL, NULL, 'Zimbabwe', '+263', 1);

-- --------------------------------------------------------

--
-- Table structure for table `FeaturedContestEntries`
--

CREATE TABLE IF NOT EXISTS `FeaturedContestEntries` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `ContestEntryId` int(11) NOT NULL,
  `StatusId` int(11) NOT NULL,
  `StartDate` datetime(6) NOT NULL,
  `EndDate` datetime(6) NOT NULL,
  `AdvertPriceId` int(11) NOT NULL,
  `AmountPaid` decimal(65,30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `FeaturedContests`
--

CREATE TABLE IF NOT EXISTS `FeaturedContests` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `ContestId` int(11) NOT NULL,
  `StatusId` int(11) NOT NULL,
  `StartDate` datetime(6) NOT NULL,
  `EndDate` datetime(6) NOT NULL,
  `AdvertPriceId` int(11) NOT NULL,
  `AmountPaid` decimal(65,30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Follows`
--

CREATE TABLE IF NOT EXISTS `Follows` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `FollowerUserId` bigint(20) NOT NULL,
  `FollowingUserId` bigint(20) NOT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `HangfireAggregatedCounter`
--

CREATE TABLE IF NOT EXISTS `HangfireAggregatedCounter` (
  `Id` int(11) NOT NULL,
  `Key` varchar(100) NOT NULL,
  `Value` int(11) NOT NULL,
  `ExpireAt` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `HangfireAggregatedCounter`
--

INSERT INTO `HangfireAggregatedCounter` (`Id`, `Key`, `Value`, `ExpireAt`) VALUES
(1, 'stats:succeeded', 49, NULL),
(2, 'stats:succeeded:2019-04-11', 48, '2019-05-11 18:20:36'),
(3, 'stats:succeeded:2019-04-11-11', 6, '2019-04-12 11:55:19'),
(4, 'stats:succeeded:2019-04-11-12', 10, '2019-04-12 12:02:23'),
(5, 'stats:succeeded:2019-04-11-17', 5, '2019-04-12 17:55:35'),
(6, 'stats:succeeded:2019-04-11-18', 27, '2019-04-12 18:20:36'),
(11, 'stats:succeeded:2019-04-12', 1, '2019-05-12 01:44:06'),
(12, 'stats:succeeded:2019-04-12-01', 1, '2019-04-13 01:44:06');

-- --------------------------------------------------------

--
-- Table structure for table `HangfireCounter`
--

CREATE TABLE IF NOT EXISTS `HangfireCounter` (
  `Id` int(11) NOT NULL,
  `Key` varchar(100) NOT NULL,
  `Value` int(11) NOT NULL,
  `ExpireAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `HangfireDistributedLock`
--

CREATE TABLE IF NOT EXISTS `HangfireDistributedLock` (
  `Resource` varchar(100) NOT NULL,
  `CreatedAt` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `HangfireHash`
--

CREATE TABLE IF NOT EXISTS `HangfireHash` (
  `Id` int(11) NOT NULL,
  `Key` varchar(100) NOT NULL,
  `Field` varchar(40) NOT NULL,
  `Value` longtext,
  `ExpireAt` datetime(6) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `HangfireHash`
--

INSERT INTO `HangfireHash` (`Id`, `Key`, `Field`, `Value`, `ExpireAt`) VALUES
(11, 'recurring-job:e55ca321-fea6-4f25-9130-7157785041e8', 'Job', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', NULL),
(12, 'recurring-job:e55ca321-fea6-4f25-9130-7157785041e8', 'Cron', '0 0 * * *', NULL),
(13, 'recurring-job:e55ca321-fea6-4f25-9130-7157785041e8', 'TimeZoneId', 'UTC', NULL),
(14, 'recurring-job:e55ca321-fea6-4f25-9130-7157785041e8', 'Queue', 'default', NULL),
(15, 'recurring-job:e55ca321-fea6-4f25-9130-7157785041e8', 'CreatedAt', '2019-04-10T16:45:16.6931110Z', NULL),
(16, 'recurring-job:c92b833c-4c2a-4387-b5af-b09063bccaf1', 'Job', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"DisableExpiredContests","ParameterTypes":"[]","Arguments":"[]"}', NULL),
(17, 'recurring-job:c92b833c-4c2a-4387-b5af-b09063bccaf1', 'Cron', '0 0 * * *', NULL),
(18, 'recurring-job:c92b833c-4c2a-4387-b5af-b09063bccaf1', 'TimeZoneId', 'UTC', NULL),
(19, 'recurring-job:c92b833c-4c2a-4387-b5af-b09063bccaf1', 'Queue', 'default', NULL),
(20, 'recurring-job:c92b833c-4c2a-4387-b5af-b09063bccaf1', 'CreatedAt', '2019-04-10T16:45:16.7238700Z', NULL),
(21, 'recurring-job:c96c38e2-6c8e-4acc-8f6d-a28c55a690c2', 'Job', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringHomePageBanner","ParameterTypes":"[]","Arguments":"[]"}', NULL),
(22, 'recurring-job:c96c38e2-6c8e-4acc-8f6d-a28c55a690c2', 'Cron', '0 0 * * *', NULL),
(23, 'recurring-job:c96c38e2-6c8e-4acc-8f6d-a28c55a690c2', 'TimeZoneId', 'UTC', NULL),
(24, 'recurring-job:c96c38e2-6c8e-4acc-8f6d-a28c55a690c2', 'Queue', 'default', NULL),
(25, 'recurring-job:c96c38e2-6c8e-4acc-8f6d-a28c55a690c2', 'CreatedAt', '2019-04-10T16:45:16.7283920Z', NULL),
(26, 'recurring-job:283bf290-17da-423e-86f8-641a6318886e', 'Job', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"DisableExpiredHomePageBanner","ParameterTypes":"[]","Arguments":"[]"}', NULL),
(27, 'recurring-job:283bf290-17da-423e-86f8-641a6318886e', 'Cron', '0 0 * * *', NULL),
(28, 'recurring-job:283bf290-17da-423e-86f8-641a6318886e', 'TimeZoneId', 'UTC', NULL),
(29, 'recurring-job:283bf290-17da-423e-86f8-641a6318886e', 'Queue', 'default', NULL),
(30, 'recurring-job:283bf290-17da-423e-86f8-641a6318886e', 'CreatedAt', '2019-04-10T16:45:16.7321190Z', NULL),
(31, 'recurring-job:e2489ddd-0fef-4990-8b14-b441f622f247', 'Job', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringTopBanner","ParameterTypes":"[]","Arguments":"[]"}', NULL),
(32, 'recurring-job:e2489ddd-0fef-4990-8b14-b441f622f247', 'Cron', '0 0 * * *', NULL),
(33, 'recurring-job:e2489ddd-0fef-4990-8b14-b441f622f247', 'TimeZoneId', 'UTC', NULL),
(34, 'recurring-job:e2489ddd-0fef-4990-8b14-b441f622f247', 'Queue', 'default', NULL),
(35, 'recurring-job:e2489ddd-0fef-4990-8b14-b441f622f247', 'CreatedAt', '2019-04-10T16:45:16.7354650Z', NULL),
(36, 'recurring-job:e755ea24-7385-47fa-a7a4-8004ba21e1f5', 'Job', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"DisableExpiredTopBanner","ParameterTypes":"[]","Arguments":"[]"}', NULL),
(37, 'recurring-job:e755ea24-7385-47fa-a7a4-8004ba21e1f5', 'Cron', '0 0 * * *', NULL),
(38, 'recurring-job:e755ea24-7385-47fa-a7a4-8004ba21e1f5', 'TimeZoneId', 'UTC', NULL),
(39, 'recurring-job:e755ea24-7385-47fa-a7a4-8004ba21e1f5', 'Queue', 'default', NULL),
(40, 'recurring-job:e755ea24-7385-47fa-a7a4-8004ba21e1f5', 'CreatedAt', '2019-04-10T16:45:16.7394250Z', NULL),
(41, 'recurring-job:e0a8a368-da81-4e52-b026-f65f79d4bd69', 'Job', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringFeaturedContests","ParameterTypes":"[]","Arguments":"[]"}', NULL),
(42, 'recurring-job:e0a8a368-da81-4e52-b026-f65f79d4bd69', 'Cron', '0 0 * * *', NULL),
(43, 'recurring-job:e0a8a368-da81-4e52-b026-f65f79d4bd69', 'TimeZoneId', 'UTC', NULL),
(44, 'recurring-job:e0a8a368-da81-4e52-b026-f65f79d4bd69', 'Queue', 'default', NULL),
(45, 'recurring-job:e0a8a368-da81-4e52-b026-f65f79d4bd69', 'CreatedAt', '2019-04-10T16:45:16.7427420Z', NULL),
(46, 'recurring-job:bc62c54b-9a86-4fc1-b103-136806a6368b', 'Job', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"DisableExpiredFeaturedContests","ParameterTypes":"[]","Arguments":"[]"}', NULL),
(47, 'recurring-job:bc62c54b-9a86-4fc1-b103-136806a6368b', 'Cron', '0 0 * * *', NULL),
(48, 'recurring-job:bc62c54b-9a86-4fc1-b103-136806a6368b', 'TimeZoneId', 'UTC', NULL),
(49, 'recurring-job:bc62c54b-9a86-4fc1-b103-136806a6368b', 'Queue', 'default', NULL),
(50, 'recurring-job:bc62c54b-9a86-4fc1-b103-136806a6368b', 'CreatedAt', '2019-04-10T16:45:16.7463080Z', NULL),
(51, 'recurring-job:f56a2a41-4bec-450e-b7a5-1ab5629aa4a5', 'Job', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringFeaturedContestEntries","ParameterTypes":"[]","Arguments":"[]"}', NULL),
(52, 'recurring-job:f56a2a41-4bec-450e-b7a5-1ab5629aa4a5', 'Cron', '0 0 * * *', NULL),
(53, 'recurring-job:f56a2a41-4bec-450e-b7a5-1ab5629aa4a5', 'TimeZoneId', 'UTC', NULL),
(54, 'recurring-job:f56a2a41-4bec-450e-b7a5-1ab5629aa4a5', 'Queue', 'default', NULL),
(55, 'recurring-job:f56a2a41-4bec-450e-b7a5-1ab5629aa4a5', 'CreatedAt', '2019-04-10T16:45:16.7498410Z', NULL),
(56, 'recurring-job:9620e05c-a03f-4a28-9c51-bee2da5545c0', 'Job', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"DisableExpiredFeaturedContestEntries","ParameterTypes":"[]","Arguments":"[]"}', NULL),
(57, 'recurring-job:9620e05c-a03f-4a28-9c51-bee2da5545c0', 'Cron', '0 0 * * *', NULL),
(58, 'recurring-job:9620e05c-a03f-4a28-9c51-bee2da5545c0', 'TimeZoneId', 'UTC', NULL),
(59, 'recurring-job:9620e05c-a03f-4a28-9c51-bee2da5545c0', 'Queue', 'default', NULL),
(60, 'recurring-job:9620e05c-a03f-4a28-9c51-bee2da5545c0', 'CreatedAt', '2019-04-10T16:45:16.7532180Z', NULL),
(61, 'recurring-job:e55ca321-fea6-4f25-9130-7157785041e8', 'NextExecution', '2019-04-12T00:00:00.0000000Z', NULL),
(62, 'recurring-job:c92b833c-4c2a-4387-b5af-b09063bccaf1', 'NextExecution', '2019-04-12T00:00:00.0000000Z', NULL),
(63, 'recurring-job:c96c38e2-6c8e-4acc-8f6d-a28c55a690c2', 'NextExecution', '2019-04-12T00:00:00.0000000Z', NULL),
(64, 'recurring-job:283bf290-17da-423e-86f8-641a6318886e', 'NextExecution', '2019-04-12T00:00:00.0000000Z', NULL),
(65, 'recurring-job:e2489ddd-0fef-4990-8b14-b441f622f247', 'NextExecution', '2019-04-12T00:00:00.0000000Z', NULL),
(66, 'recurring-job:e755ea24-7385-47fa-a7a4-8004ba21e1f5', 'NextExecution', '2019-04-12T00:00:00.0000000Z', NULL),
(67, 'recurring-job:e0a8a368-da81-4e52-b026-f65f79d4bd69', 'NextExecution', '2019-04-12T00:00:00.0000000Z', NULL),
(68, 'recurring-job:bc62c54b-9a86-4fc1-b103-136806a6368b', 'NextExecution', '2019-04-12T00:00:00.0000000Z', NULL),
(69, 'recurring-job:f56a2a41-4bec-450e-b7a5-1ab5629aa4a5', 'NextExecution', '2019-04-12T00:00:00.0000000Z', NULL),
(70, 'recurring-job:9620e05c-a03f-4a28-9c51-bee2da5545c0', 'NextExecution', '2019-04-12T00:00:00.0000000Z', NULL),
(151, 'recurring-job:e55ca321-fea6-4f25-9130-7157785041e8', 'LastExecution', '2019-04-11T12:02:00.5902180Z', NULL),
(152, 'recurring-job:e55ca321-fea6-4f25-9130-7157785041e8', 'LastJobId', '30', NULL),
(154, 'recurring-job:c92b833c-4c2a-4387-b5af-b09063bccaf1', 'LastExecution', '2019-04-11T12:02:00.6865520Z', NULL),
(155, 'recurring-job:c92b833c-4c2a-4387-b5af-b09063bccaf1', 'LastJobId', '31', NULL),
(157, 'recurring-job:c96c38e2-6c8e-4acc-8f6d-a28c55a690c2', 'LastExecution', '2019-04-11T12:02:00.6972160Z', NULL),
(158, 'recurring-job:c96c38e2-6c8e-4acc-8f6d-a28c55a690c2', 'LastJobId', '32', NULL),
(160, 'recurring-job:283bf290-17da-423e-86f8-641a6318886e', 'LastExecution', '2019-04-11T12:02:00.7056190Z', NULL),
(161, 'recurring-job:283bf290-17da-423e-86f8-641a6318886e', 'LastJobId', '33', NULL),
(163, 'recurring-job:e2489ddd-0fef-4990-8b14-b441f622f247', 'LastExecution', '2019-04-11T12:02:00.7135580Z', NULL),
(164, 'recurring-job:e2489ddd-0fef-4990-8b14-b441f622f247', 'LastJobId', '34', NULL),
(166, 'recurring-job:e755ea24-7385-47fa-a7a4-8004ba21e1f5', 'LastExecution', '2019-04-11T12:02:00.7224820Z', NULL),
(167, 'recurring-job:e755ea24-7385-47fa-a7a4-8004ba21e1f5', 'LastJobId', '35', NULL),
(169, 'recurring-job:e0a8a368-da81-4e52-b026-f65f79d4bd69', 'LastExecution', '2019-04-11T12:02:00.7302840Z', NULL),
(170, 'recurring-job:e0a8a368-da81-4e52-b026-f65f79d4bd69', 'LastJobId', '36', NULL),
(172, 'recurring-job:bc62c54b-9a86-4fc1-b103-136806a6368b', 'LastExecution', '2019-04-11T12:02:00.7381550Z', NULL),
(173, 'recurring-job:bc62c54b-9a86-4fc1-b103-136806a6368b', 'LastJobId', '37', NULL),
(175, 'recurring-job:f56a2a41-4bec-450e-b7a5-1ab5629aa4a5', 'LastExecution', '2019-04-11T12:02:00.7460060Z', NULL),
(176, 'recurring-job:f56a2a41-4bec-450e-b7a5-1ab5629aa4a5', 'LastJobId', '38', NULL),
(178, 'recurring-job:9620e05c-a03f-4a28-9c51-bee2da5545c0', 'LastExecution', '2019-04-11T12:02:00.7537210Z', NULL),
(179, 'recurring-job:9620e05c-a03f-4a28-9c51-bee2da5545c0', 'LastJobId', '39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `HangfireJob`
--

CREATE TABLE IF NOT EXISTS `HangfireJob` (
  `Id` int(11) NOT NULL,
  `StateId` int(11) DEFAULT NULL,
  `StateName` varchar(20) DEFAULT NULL,
  `InvocationData` longtext NOT NULL,
  `Arguments` longtext NOT NULL,
  `CreatedAt` datetime(6) NOT NULL,
  `ExpireAt` datetime(6) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `HangfireJob`
--

INSERT INTO `HangfireJob` (`Id`, `StateId`, `StateName`, `InvocationData`, `Arguments`, `CreatedAt`, `ExpireAt`) VALUES
(1, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:08:00.791320', '2019-05-11 08:08:00.791320'),
(2, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:08:00.930441', '2019-05-11 08:08:00.930441'),
(3, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:09:00.008637', '2019-05-11 08:09:00.008637'),
(4, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:10:00.071384', '2019-05-11 08:10:00.071384'),
(5, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:11:00.131723', '2019-05-11 08:11:00.131723'),
(6, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:12:00.189293', '2019-05-11 08:12:00.189293'),
(7, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:13:00.239568', '2019-05-11 08:13:00.239568'),
(8, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:14:00.275618', '2019-05-11 08:14:00.275618'),
(9, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:16:00.373003', '2019-05-11 08:16:00.373003'),
(10, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:18:00.443005', '2019-05-11 08:18:00.443005'),
(11, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:20:00.477824', '2019-05-11 08:20:00.477824'),
(12, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:22:00.503495', '2019-05-11 08:22:00.503495'),
(13, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:25:00.557116', '2019-05-11 08:25:00.557116'),
(14, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:28:00.602420', '2019-05-11 08:28:00.602420'),
(15, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:32:00.675857', '2019-05-11 08:32:00.675857'),
(16, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:36:00.684342', '2019-05-11 08:36:00.684342'),
(17, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:41:00.822571', '2019-05-11 08:41:00.822571'),
(18, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:46:00.852420', '2019-05-11 08:46:00.852420'),
(19, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:51:00.866906', '2019-05-11 08:51:00.866906'),
(20, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 08:56:00.893764', '2019-05-11 08:56:00.893764'),
(21, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 09:01:00.905835', '2019-05-11 09:01:00.905835'),
(22, NULL, NULL, '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 09:06:00.908775', '2019-05-11 09:06:00.908775'),
(23, NULL, NULL, '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":2}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":2}"]', '2019-04-11 10:34:25.228658', '2019-05-11 10:34:25.228658'),
(24, 4, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":2}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":2}"]', '2019-04-11 11:52:33.574085', '2019-04-12 11:53:18.861287'),
(25, 6, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":2}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":2}"]', '2019-04-11 11:52:41.105659', '2019-04-12 11:53:18.896213'),
(26, 10, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":2}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":2}"]', '2019-04-11 11:53:23.800199', '2019-04-12 11:54:18.923229'),
(27, 12, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":2}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":2}"]', '2019-04-11 11:53:47.926631', '2019-04-12 11:54:18.951441'),
(28, 16, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobNotifyUser, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Notify","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobNotifyUserDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3,\\\\\\"Message\\\\\\":\\\\\\"Welcome to iContestPRO. Your account was created successfully.\\\\\\",\\\\\\"Severity\\\\\\":0}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3,\\"Message\\":\\"Welcome to iContestPRO. Your account was created successfully.\\",\\"Severity\\":0}"]', '2019-04-11 11:54:42.676074', '2019-04-12 11:55:19.094771'),
(29, 18, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobEmailSender, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Send","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.SendEmailDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"From\\\\\\":{\\\\\\"name\\\\\\":\\\\\\"iContestPRO\\\\\\",\\\\\\"email\\\\\\":\\\\\\"no-reply@icontestpro.com\\\\\\"},\\\\\\"Tos\\\\\\":[{\\\\\\"name\\\\\\":\\\\\\"Fidelis Kalu\\\\\\",\\\\\\"email\\\\\\":\\\\\\"fidelisekeh@gmail.com\\\\\\"}],\\\\\\"Subject\\\\\\":\\\\\\"Confirm your email\\\\\\",\\\\\\"Body\\\\\\":\\\\\\"Hi Fidelis Kalu! Thank you for registering at iContestPro. One step to complete your account registration. Please confirm your account by <a href=''http://localhost:5000/Account/ConfirmEmail?userId=3&amp;code=CfDJ8ESzHNEnx1JCshHEy56Gu9UbCgvi8%2Fl6nUMaE%2BXzWW749U%2FeypaI19QEEYujb6cdWGSnPgaMRKHevL3QJRwg3wRcmbfgw3kWqHbBzsiyvoDI66UpqWMBSFbV7ALWYRw%2BwrH%2FMu5TPv%2FJuGE8nEEgFKp9xSXDYkGYDeA6pLJl4kKzteBO6utx094QVd0Smt4Bmg%3D%3D''>clicking here</a>.\\\\\\"}\\"]"}', '["{\\"From\\":{\\"name\\":\\"iContestPRO\\",\\"email\\":\\"no-reply@icontestpro.com\\"},\\"Tos\\":[{\\"name\\":\\"Fidelis Kalu\\",\\"email\\":\\"fidelisekeh@gmail.com\\"}],\\"Subject\\":\\"Confirm your email\\",\\"Body\\":\\"Hi Fidelis Kalu! Thank you for registering at iContestPro. One step to complete your account registration. Please confirm your account by <a href=''http://localhost:5000/Account/ConfirmEmail?userId=3&amp;code=CfDJ8ESzHNEnx1JCshHEy56Gu9UbCgvi8%2Fl6nUMaE%2BXzWW749U%2FeypaI19QEEYujb6cdWGSnPgaMRKHevL3QJRwg3wRcmbfgw3kWqHbBzsiyvoDI66UpqWMBSFbV7ALWYRw%2BwrH%2FMu5TPv%2FJuGE8nEEgFKp9xSXDYkGYDeA6pLJl4kKzteBO6utx094QVd0Smt4Bmg%3D%3D''>clicking here</a>.\\"}"]', '2019-04-11 11:54:42.729268', '2019-04-12 11:55:19.309784'),
(30, 30, 'Succeeded', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 12:02:00.630540', '2019-04-12 12:02:16.456883'),
(31, 32, 'Succeeded', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"DisableExpiredContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 12:02:00.687023', '2019-04-12 12:02:17.167861'),
(32, 34, 'Succeeded', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringHomePageBanner","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 12:02:00.697350', '2019-04-12 12:02:17.956074'),
(33, 36, 'Succeeded', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"DisableExpiredHomePageBanner","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 12:02:00.705803', '2019-04-12 12:02:18.733817'),
(34, 38, 'Succeeded', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringTopBanner","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 12:02:00.713706', '2019-04-12 12:02:19.509133'),
(35, 40, 'Succeeded', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"DisableExpiredTopBanner","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 12:02:00.722631', '2019-04-12 12:02:20.167510'),
(36, 42, 'Succeeded', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringFeaturedContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 12:02:00.730426', '2019-04-12 12:02:20.848846'),
(37, 44, 'Succeeded', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"DisableExpiredFeaturedContests","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 12:02:00.738297', '2019-04-12 12:02:21.567982'),
(38, 46, 'Succeeded', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"NotifyExpiringFeaturedContestEntries","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 12:02:00.746149', '2019-04-12 12:02:22.298589'),
(39, 48, 'Succeeded', '{"Type":"IContestPro.AppServiceBgJobs, IContestPro.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"DisableExpiredFeaturedContestEntries","ParameterTypes":"[]","Arguments":"[]"}', '[]', '2019-04-11 12:02:00.753914', '2019-04-12 12:02:23.041006'),
(40, 52, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 17:53:12.143806', '2019-04-12 17:53:35.239838'),
(41, 54, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 17:53:28.703277', '2019-04-12 17:53:35.265843'),
(42, 59, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 17:55:15.169863', '2019-04-12 17:55:35.299979'),
(43, 61, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 17:55:18.012532', '2019-04-12 17:55:35.324612'),
(44, 63, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 17:55:29.020887', '2019-04-12 17:55:35.348403'),
(45, 67, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:01:16.765405', '2019-04-12 18:01:35.390637'),
(46, 69, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:01:27.403593', '2019-04-12 18:01:35.413077'),
(47, 72, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:02:33.122085', '2019-04-12 18:02:35.443985'),
(48, 77, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:02:57.779975', '2019-04-12 18:03:35.473695'),
(49, 79, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:03:12.930413', '2019-04-12 18:03:35.493700'),
(50, 81, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:03:15.333919', '2019-04-12 18:03:35.514011'),
(51, 84, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:04:34.656192', '2019-04-12 18:04:35.545457'),
(52, 87, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:04:57.017835', '2019-04-12 18:05:35.585762'),
(53, 90, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:05:37.209610', '2019-04-12 18:06:35.625874'),
(54, 95, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:07:19.723868', '2019-04-12 18:07:35.657831'),
(55, 97, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:07:23.172999', '2019-04-12 18:07:35.681668'),
(56, 99, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:07:29.925552', '2019-04-12 18:07:35.705131'),
(57, 103, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:07:52.251743', '2019-04-12 18:08:35.732744'),
(58, 105, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:08:23.193104', '2019-04-12 18:08:35.752672'),
(59, 108, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:09:50.645819', '2019-04-12 18:10:35.782497'),
(60, 113, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:12:41.670507', '2019-04-12 18:13:35.813122'),
(61, 115, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:13:04.212678', '2019-04-12 18:13:35.833904'),
(62, 117, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:13:14.897075', '2019-04-12 18:13:35.855894'),
(63, 120, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:13:43.257711', '2019-04-12 18:14:35.886082'),
(64, 123, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:16:26.118053', '2019-04-12 18:16:35.916775'),
(65, 127, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:17:09.129977', '2019-04-12 18:17:35.945450'),
(66, 129, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:17:26.626372', '2019-04-12 18:17:35.971096'),
(67, 132, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:19:03.883784', '2019-04-12 18:19:36.000725'),
(68, 138, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:19:38.055575', '2019-04-12 18:20:36.030016'),
(69, 140, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:19:42.735388', '2019-04-12 18:20:36.054081'),
(70, 142, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:19:58.194930', '2019-04-12 18:20:36.077863'),
(71, 144, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-11 18:20:04.465428', '2019-04-12 18:20:36.100553'),
(72, 147, 'Succeeded', '{"Type":"IContestPro.BackgroundJobs.BgJobSubscribeToAllAvailableNotifications, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null","Method":"Subscribe","ParameterTypes":"[\\"IContestPro.BackgroundJobs.Dto.BgJobSubscribeToAllAvailableNotificationsDto, IContestPro.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\"]","Arguments":"[\\"{\\\\\\"TenantId\\\\\\":1,\\\\\\"UserId\\\\\\":3}\\"]"}', '["{\\"TenantId\\":1,\\"UserId\\":3}"]', '2019-04-12 01:43:22.371398', '2019-04-13 01:44:06.104646');

-- --------------------------------------------------------

--
-- Table structure for table `HangfireJobParameter`
--

CREATE TABLE IF NOT EXISTS `HangfireJobParameter` (
  `Id` int(11) NOT NULL,
  `JobId` int(11) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `Value` longtext
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `HangfireJobParameter`
--

INSERT INTO `HangfireJobParameter` (`Id`, `JobId`, `Name`, `Value`) VALUES
(1, 1, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(2, 1, 'CurrentCulture', '""'),
(3, 1, 'CurrentUICulture', '""'),
(4, 2, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(5, 2, 'CurrentCulture', '""'),
(6, 2, 'CurrentUICulture', '""'),
(7, 3, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(8, 3, 'CurrentCulture', '""'),
(9, 3, 'CurrentUICulture', '""'),
(10, 4, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(11, 4, 'CurrentCulture', '""'),
(12, 4, 'CurrentUICulture', '""'),
(13, 5, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(14, 5, 'CurrentCulture', '""'),
(15, 5, 'CurrentUICulture', '""'),
(16, 6, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(17, 6, 'CurrentCulture', '""'),
(18, 6, 'CurrentUICulture', '""'),
(19, 7, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(20, 7, 'CurrentCulture', '""'),
(21, 7, 'CurrentUICulture', '""'),
(22, 8, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(23, 8, 'CurrentCulture', '""'),
(24, 8, 'CurrentUICulture', '""'),
(25, 9, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(26, 9, 'CurrentCulture', '""'),
(27, 9, 'CurrentUICulture', '""'),
(28, 10, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(29, 10, 'CurrentCulture', '""'),
(30, 10, 'CurrentUICulture', '""'),
(31, 11, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(32, 11, 'CurrentCulture', '""'),
(33, 11, 'CurrentUICulture', '""'),
(34, 12, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(35, 12, 'CurrentCulture', '""'),
(36, 12, 'CurrentUICulture', '""'),
(37, 13, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(38, 13, 'CurrentCulture', '""'),
(39, 13, 'CurrentUICulture', '""'),
(40, 14, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(41, 14, 'CurrentCulture', '""'),
(42, 14, 'CurrentUICulture', '""'),
(43, 15, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(44, 15, 'CurrentCulture', '""'),
(45, 15, 'CurrentUICulture', '""'),
(46, 16, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(47, 16, 'CurrentCulture', '""'),
(48, 16, 'CurrentUICulture', '""'),
(49, 17, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(50, 17, 'CurrentCulture', '""'),
(51, 17, 'CurrentUICulture', '""'),
(52, 18, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(53, 18, 'CurrentCulture', '""'),
(54, 18, 'CurrentUICulture', '""'),
(55, 19, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(56, 19, 'CurrentCulture', '""'),
(57, 19, 'CurrentUICulture', '""'),
(58, 20, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(59, 20, 'CurrentCulture', '""'),
(60, 20, 'CurrentUICulture', '""'),
(61, 21, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(62, 21, 'CurrentCulture', '""'),
(63, 21, 'CurrentUICulture', '""'),
(64, 22, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(65, 22, 'CurrentCulture', '""'),
(66, 22, 'CurrentUICulture', '""'),
(67, 23, 'CurrentCulture', '"en"'),
(68, 23, 'CurrentUICulture', '"en"'),
(69, 24, 'CurrentCulture', '"en"'),
(70, 24, 'CurrentUICulture', '"en"'),
(71, 25, 'CurrentCulture', '"en"'),
(72, 25, 'CurrentUICulture', '"en"'),
(73, 26, 'CurrentCulture', '"en"'),
(74, 26, 'CurrentUICulture', '"en"'),
(75, 27, 'CurrentCulture', '"en"'),
(76, 27, 'CurrentUICulture', '"en"'),
(77, 28, 'CurrentCulture', '"en"'),
(78, 28, 'CurrentUICulture', '"en"'),
(79, 29, 'CurrentCulture', '"en"'),
(80, 29, 'CurrentUICulture', '"en"'),
(81, 30, 'RecurringJobId', '"e55ca321-fea6-4f25-9130-7157785041e8"'),
(82, 30, 'CurrentCulture', '""'),
(83, 30, 'CurrentUICulture', '""'),
(84, 31, 'RecurringJobId', '"c92b833c-4c2a-4387-b5af-b09063bccaf1"'),
(85, 31, 'CurrentCulture', '""'),
(86, 31, 'CurrentUICulture', '""'),
(87, 32, 'RecurringJobId', '"c96c38e2-6c8e-4acc-8f6d-a28c55a690c2"'),
(88, 32, 'CurrentCulture', '""'),
(89, 32, 'CurrentUICulture', '""'),
(90, 33, 'RecurringJobId', '"283bf290-17da-423e-86f8-641a6318886e"'),
(91, 33, 'CurrentCulture', '""'),
(92, 33, 'CurrentUICulture', '""'),
(93, 34, 'RecurringJobId', '"e2489ddd-0fef-4990-8b14-b441f622f247"'),
(94, 34, 'CurrentCulture', '""'),
(95, 34, 'CurrentUICulture', '""'),
(96, 35, 'RecurringJobId', '"e755ea24-7385-47fa-a7a4-8004ba21e1f5"'),
(97, 35, 'CurrentCulture', '""'),
(98, 35, 'CurrentUICulture', '""'),
(99, 36, 'RecurringJobId', '"e0a8a368-da81-4e52-b026-f65f79d4bd69"'),
(100, 36, 'CurrentCulture', '""'),
(101, 36, 'CurrentUICulture', '""'),
(102, 37, 'RecurringJobId', '"bc62c54b-9a86-4fc1-b103-136806a6368b"'),
(103, 37, 'CurrentCulture', '""'),
(104, 37, 'CurrentUICulture', '""'),
(105, 38, 'RecurringJobId', '"f56a2a41-4bec-450e-b7a5-1ab5629aa4a5"'),
(106, 38, 'CurrentCulture', '""'),
(107, 38, 'CurrentUICulture', '""'),
(108, 39, 'RecurringJobId', '"9620e05c-a03f-4a28-9c51-bee2da5545c0"'),
(109, 39, 'CurrentCulture', '""'),
(110, 39, 'CurrentUICulture', '""'),
(111, 40, 'CurrentCulture', '"en"'),
(112, 40, 'CurrentUICulture', '"en"'),
(113, 41, 'CurrentCulture', '"en"'),
(114, 41, 'CurrentUICulture', '"en"'),
(115, 42, 'CurrentCulture', '"en"'),
(116, 42, 'CurrentUICulture', '"en"'),
(117, 43, 'CurrentCulture', '"en"'),
(118, 43, 'CurrentUICulture', '"en"'),
(119, 44, 'CurrentCulture', '"en"'),
(120, 44, 'CurrentUICulture', '"en"'),
(121, 45, 'CurrentCulture', '"en"'),
(122, 45, 'CurrentUICulture', '"en"'),
(123, 46, 'CurrentCulture', '"en"'),
(124, 46, 'CurrentUICulture', '"en"'),
(125, 47, 'CurrentCulture', '"en"'),
(126, 47, 'CurrentUICulture', '"en"'),
(127, 48, 'CurrentCulture', '"en"'),
(128, 48, 'CurrentUICulture', '"en"'),
(129, 49, 'CurrentCulture', '"en"'),
(130, 49, 'CurrentUICulture', '"en"'),
(131, 50, 'CurrentCulture', '"en"'),
(132, 50, 'CurrentUICulture', '"en"'),
(133, 51, 'CurrentCulture', '"en"'),
(134, 51, 'CurrentUICulture', '"en"'),
(135, 52, 'CurrentCulture', '"en"'),
(136, 52, 'CurrentUICulture', '"en"'),
(137, 53, 'CurrentCulture', '"en"'),
(138, 53, 'CurrentUICulture', '"en"'),
(139, 54, 'CurrentCulture', '"en"'),
(140, 54, 'CurrentUICulture', '"en"'),
(141, 55, 'CurrentCulture', '"en"'),
(142, 55, 'CurrentUICulture', '"en"'),
(143, 56, 'CurrentCulture', '"en"'),
(144, 56, 'CurrentUICulture', '"en"'),
(145, 57, 'CurrentCulture', '"en"'),
(146, 57, 'CurrentUICulture', '"en"'),
(147, 58, 'CurrentCulture', '"en"'),
(148, 58, 'CurrentUICulture', '"en"'),
(149, 59, 'CurrentCulture', '"en"'),
(150, 59, 'CurrentUICulture', '"en"'),
(151, 60, 'CurrentCulture', '"en"'),
(152, 60, 'CurrentUICulture', '"en"'),
(153, 61, 'CurrentCulture', '"en"'),
(154, 61, 'CurrentUICulture', '"en"'),
(155, 62, 'CurrentCulture', '"en"'),
(156, 62, 'CurrentUICulture', '"en"'),
(157, 63, 'CurrentCulture', '"en"'),
(158, 63, 'CurrentUICulture', '"en"'),
(159, 64, 'CurrentCulture', '"en"'),
(160, 64, 'CurrentUICulture', '"en"'),
(161, 65, 'CurrentCulture', '"en"'),
(162, 65, 'CurrentUICulture', '"en"'),
(163, 66, 'CurrentCulture', '"en"'),
(164, 66, 'CurrentUICulture', '"en"'),
(165, 67, 'CurrentCulture', '"en"'),
(166, 67, 'CurrentUICulture', '"en"'),
(167, 68, 'CurrentCulture', '"en"'),
(168, 68, 'CurrentUICulture', '"en"'),
(169, 69, 'CurrentCulture', '"en"'),
(170, 69, 'CurrentUICulture', '"en"'),
(171, 70, 'CurrentCulture', '"en"'),
(172, 70, 'CurrentUICulture', '"en"'),
(173, 71, 'CurrentCulture', '"en"'),
(174, 71, 'CurrentUICulture', '"en"'),
(175, 72, 'CurrentCulture', '"en"'),
(176, 72, 'CurrentUICulture', '"en"');

-- --------------------------------------------------------

--
-- Table structure for table `HangfireJobQueue`
--

CREATE TABLE IF NOT EXISTS `HangfireJobQueue` (
  `Id` int(11) NOT NULL,
  `JobId` int(11) NOT NULL,
  `FetchedAt` datetime(6) DEFAULT NULL,
  `Queue` varchar(50) NOT NULL,
  `FetchToken` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `HangfireJobState`
--

CREATE TABLE IF NOT EXISTS `HangfireJobState` (
  `Id` int(11) NOT NULL,
  `JobId` int(11) NOT NULL,
  `CreatedAt` datetime(6) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Reason` varchar(100) DEFAULT NULL,
  `Data` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `HangfireList`
--

CREATE TABLE IF NOT EXISTS `HangfireList` (
  `ExpireAt` datetime NOT NULL,
  `Id` int(11) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `HangfireServer`
--

CREATE TABLE IF NOT EXISTS `HangfireServer` (
  `Id` varchar(100) NOT NULL,
  `Data` longtext NOT NULL,
  `LastHeartbeat` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `HangfireServer`
--

INSERT INTO `HangfireServer` (`Id`, `Data`, `LastHeartbeat`) VALUES
('26bb9345-c84b-4407-b780-b3d37b7ae829', '{"WorkerCount":1,"Queues":["default"],"StartedAt":"2019-04-11T18:23:29.765424Z"}', '2019-04-12 02:53:24.452559');

-- --------------------------------------------------------

--
-- Table structure for table `HangfireSet`
--

CREATE TABLE IF NOT EXISTS `HangfireSet` (
  `ExpireAt` datetime NOT NULL,
  `Id` int(11) NOT NULL,
  `Key` varchar(255) NOT NULL,
  `Score` float NOT NULL,
  `Value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `HangfireState`
--

CREATE TABLE IF NOT EXISTS `HangfireState` (
  `Id` int(11) NOT NULL,
  `JobId` int(11) NOT NULL,
  `CreatedAt` datetime(6) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Reason` varchar(100) DEFAULT NULL,
  `Data` longtext
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `HangfireState`
--

INSERT INTO `HangfireState` (`Id`, `JobId`, `CreatedAt`, `Name`, `Reason`, `Data`) VALUES
(1, 24, '2019-04-11 11:52:33.576915', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T11:52:33.5727260Z","Queue":"default"}'),
(2, 25, '2019-04-11 11:52:41.109100', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T11:52:41.1044680Z","Queue":"default"}'),
(3, 24, '2019-04-11 11:53:18.776755', 'Processing', NULL, '{"StartedAt":"2019-04-11T11:53:18.7487560Z","ServerId":"7c0094d1-732f-4849-b3ac-0ae4f72dc051","WorkerId":"0bf69172-d126-4171-b6fe-1ea0ac11a070"}'),
(4, 24, '2019-04-11 11:53:18.853510', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T11:53:18.8316950Z","PerformanceDuration":"46","Latency":"45210"}'),
(5, 25, '2019-04-11 11:53:18.875724', 'Processing', NULL, '{"StartedAt":"2019-04-11T11:53:18.8725850Z","ServerId":"7c0094d1-732f-4849-b3ac-0ae4f72dc051","WorkerId":"0bf69172-d126-4171-b6fe-1ea0ac11a070"}'),
(6, 25, '2019-04-11 11:53:18.895518', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T11:53:18.8916090Z","PerformanceDuration":"12","Latency":"37773"}'),
(7, 26, '2019-04-11 11:53:23.802913', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T11:53:23.7991290Z","Queue":"default"}'),
(8, 27, '2019-04-11 11:53:47.929704', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T11:53:47.9247500Z","Queue":"default"}'),
(9, 26, '2019-04-11 11:54:18.903506', 'Processing', NULL, '{"StartedAt":"2019-04-11T11:54:18.9013740Z","ServerId":"7c0094d1-732f-4849-b3ac-0ae4f72dc051","WorkerId":"0bf69172-d126-4171-b6fe-1ea0ac11a070"}'),
(10, 26, '2019-04-11 11:54:18.921752', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T11:54:18.9180760Z","PerformanceDuration":"9","Latency":"55108"}'),
(11, 27, '2019-04-11 11:54:18.933179', 'Processing', NULL, '{"StartedAt":"2019-04-11T11:54:18.9300440Z","ServerId":"7c0094d1-732f-4849-b3ac-0ae4f72dc051","WorkerId":"0bf69172-d126-4171-b6fe-1ea0ac11a070"}'),
(12, 27, '2019-04-11 11:54:18.950150', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T11:54:18.9449330Z","PerformanceDuration":"9","Latency":"31009"}'),
(13, 28, '2019-04-11 11:54:42.685077', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T11:54:42.6670700Z","Queue":"default"}'),
(14, 29, '2019-04-11 11:54:42.740593', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T11:54:42.7267760Z","Queue":"default"}'),
(15, 28, '2019-04-11 11:55:18.965770', 'Processing', NULL, '{"StartedAt":"2019-04-11T11:55:18.9596740Z","ServerId":"7c0094d1-732f-4849-b3ac-0ae4f72dc051","WorkerId":"0bf69172-d126-4171-b6fe-1ea0ac11a070"}'),
(16, 28, '2019-04-11 11:55:19.094026', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T11:55:19.0900860Z","PerformanceDuration":"121","Latency":"36292"}'),
(17, 29, '2019-04-11 11:55:19.104991', 'Processing', NULL, '{"StartedAt":"2019-04-11T11:55:19.0995210Z","ServerId":"7c0094d1-732f-4849-b3ac-0ae4f72dc051","WorkerId":"0bf69172-d126-4171-b6fe-1ea0ac11a070"}'),
(18, 29, '2019-04-11 11:55:19.308609', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T11:55:19.3037850Z","PerformanceDuration":"195","Latency":"36378"}'),
(19, 30, '2019-04-11 12:02:00.664772', 'Enqueued', 'Triggered by recurring job scheduler', '{"EnqueuedAt":"2019-04-11T12:02:00.6030020Z","Queue":"default"}'),
(20, 31, '2019-04-11 12:02:00.691160', 'Enqueued', 'Triggered by recurring job scheduler', '{"EnqueuedAt":"2019-04-11T12:02:00.6866920Z","Queue":"default"}'),
(21, 32, '2019-04-11 12:02:00.700799', 'Enqueued', 'Triggered by recurring job scheduler', '{"EnqueuedAt":"2019-04-11T12:02:00.6972440Z","Queue":"default"}'),
(22, 33, '2019-04-11 12:02:00.708591', 'Enqueued', 'Triggered by recurring job scheduler', '{"EnqueuedAt":"2019-04-11T12:02:00.7056600Z","Queue":"default"}'),
(23, 34, '2019-04-11 12:02:00.716616', 'Enqueued', 'Triggered by recurring job scheduler', '{"EnqueuedAt":"2019-04-11T12:02:00.7135860Z","Queue":"default"}'),
(24, 35, '2019-04-11 12:02:00.725411', 'Enqueued', 'Triggered by recurring job scheduler', '{"EnqueuedAt":"2019-04-11T12:02:00.7225110Z","Queue":"default"}'),
(25, 36, '2019-04-11 12:02:00.733371', 'Enqueued', 'Triggered by recurring job scheduler', '{"EnqueuedAt":"2019-04-11T12:02:00.7303130Z","Queue":"default"}'),
(26, 37, '2019-04-11 12:02:00.741105', 'Enqueued', 'Triggered by recurring job scheduler', '{"EnqueuedAt":"2019-04-11T12:02:00.7381840Z","Queue":"default"}'),
(27, 38, '2019-04-11 12:02:00.748943', 'Enqueued', 'Triggered by recurring job scheduler', '{"EnqueuedAt":"2019-04-11T12:02:00.7460350Z","Queue":"default"}'),
(28, 39, '2019-04-11 12:02:00.756713', 'Enqueued', 'Triggered by recurring job scheduler', '{"EnqueuedAt":"2019-04-11T12:02:00.7537500Z","Queue":"default"}'),
(29, 30, '2019-04-11 12:02:06.527376', 'Processing', NULL, '{"StartedAt":"2019-04-11T12:02:06.5132460Z","ServerId":"a346b32d-3a4f-45e1-8c99-a0b915b89118","WorkerId":"a0396fcb-7423-4758-9cca-2258314d8b5f"}'),
(30, 30, '2019-04-11 12:02:16.450954', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T12:02:16.4301760Z","PerformanceDuration":"9895","Latency":"5903"}'),
(31, 31, '2019-04-11 12:02:16.468702', 'Processing', NULL, '{"StartedAt":"2019-04-11T12:02:16.4661330Z","ServerId":"a346b32d-3a4f-45e1-8c99-a0b915b89118","WorkerId":"a0396fcb-7423-4758-9cca-2258314d8b5f"}'),
(32, 31, '2019-04-11 12:02:17.166918', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T12:02:17.1627120Z","PerformanceDuration":"691","Latency":"15784"}'),
(33, 32, '2019-04-11 12:02:17.174387', 'Processing', NULL, '{"StartedAt":"2019-04-11T12:02:17.1719140Z","ServerId":"a346b32d-3a4f-45e1-8c99-a0b915b89118","WorkerId":"a0396fcb-7423-4758-9cca-2258314d8b5f"}'),
(34, 32, '2019-04-11 12:02:17.955387', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T12:02:17.9513680Z","PerformanceDuration":"774","Latency":"16479"}'),
(35, 33, '2019-04-11 12:02:17.962974', 'Processing', NULL, '{"StartedAt":"2019-04-11T12:02:17.9605620Z","ServerId":"a346b32d-3a4f-45e1-8c99-a0b915b89118","WorkerId":"a0396fcb-7423-4758-9cca-2258314d8b5f"}'),
(36, 33, '2019-04-11 12:02:18.732148', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T12:02:18.7286750Z","PerformanceDuration":"762","Latency":"17260"}'),
(37, 34, '2019-04-11 12:02:18.740182', 'Processing', NULL, '{"StartedAt":"2019-04-11T12:02:18.7376120Z","ServerId":"a346b32d-3a4f-45e1-8c99-a0b915b89118","WorkerId":"a0396fcb-7423-4758-9cca-2258314d8b5f"}'),
(38, 34, '2019-04-11 12:02:19.508479', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T12:02:19.5048980Z","PerformanceDuration":"761","Latency":"18029"}'),
(39, 35, '2019-04-11 12:02:19.516057', 'Processing', NULL, '{"StartedAt":"2019-04-11T12:02:19.5135090Z","ServerId":"a346b32d-3a4f-45e1-8c99-a0b915b89118","WorkerId":"a0396fcb-7423-4758-9cca-2258314d8b5f"}'),
(40, 35, '2019-04-11 12:02:20.166869', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T12:02:20.1632770Z","PerformanceDuration":"643","Latency":"18796"}'),
(41, 36, '2019-04-11 12:02:20.174175', 'Processing', NULL, '{"StartedAt":"2019-04-11T12:02:20.1717760Z","ServerId":"a346b32d-3a4f-45e1-8c99-a0b915b89118","WorkerId":"a0396fcb-7423-4758-9cca-2258314d8b5f"}'),
(42, 36, '2019-04-11 12:02:20.848172', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T12:02:20.8447200Z","PerformanceDuration":"666","Latency":"19447"}'),
(43, 37, '2019-04-11 12:02:20.855515', 'Processing', NULL, '{"StartedAt":"2019-04-11T12:02:20.8531170Z","ServerId":"a346b32d-3a4f-45e1-8c99-a0b915b89118","WorkerId":"a0396fcb-7423-4758-9cca-2258314d8b5f"}'),
(44, 37, '2019-04-11 12:02:21.567344', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T12:02:21.5636830Z","PerformanceDuration":"705","Latency":"20120"}'),
(45, 38, '2019-04-11 12:02:21.574616', 'Processing', NULL, '{"StartedAt":"2019-04-11T12:02:21.5722170Z","ServerId":"a346b32d-3a4f-45e1-8c99-a0b915b89118","WorkerId":"a0396fcb-7423-4758-9cca-2258314d8b5f"}'),
(46, 38, '2019-04-11 12:02:22.297944', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T12:02:22.2942210Z","PerformanceDuration":"716","Latency":"20831"}'),
(47, 39, '2019-04-11 12:02:22.305366', 'Processing', NULL, '{"StartedAt":"2019-04-11T12:02:22.3029150Z","ServerId":"a346b32d-3a4f-45e1-8c99-a0b915b89118","WorkerId":"a0396fcb-7423-4758-9cca-2258314d8b5f"}'),
(48, 39, '2019-04-11 12:02:23.040351', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T12:02:23.0368510Z","PerformanceDuration":"728","Latency":"21554"}'),
(49, 40, '2019-04-11 17:53:12.188182', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T17:53:12.1082590Z","Queue":"default"}'),
(50, 41, '2019-04-11 17:53:28.708247', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T17:53:28.7021970Z","Queue":"default"}'),
(51, 40, '2019-04-11 17:53:35.174173', 'Processing', NULL, '{"StartedAt":"2019-04-11T17:53:35.1445990Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(52, 40, '2019-04-11 17:53:35.234501', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T17:53:35.2198130Z","PerformanceDuration":"37","Latency":"23038"}'),
(53, 41, '2019-04-11 17:53:35.250773', 'Processing', NULL, '{"StartedAt":"2019-04-11T17:53:35.2484580Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(54, 41, '2019-04-11 17:53:35.264248', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T17:53:35.2604420Z","PerformanceDuration":"6","Latency":"6550"}'),
(55, 42, '2019-04-11 17:55:15.172657', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T17:55:15.1689240Z","Queue":"default"}'),
(56, 43, '2019-04-11 17:55:18.016281', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T17:55:18.0115580Z","Queue":"default"}'),
(57, 44, '2019-04-11 17:55:29.024221', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T17:55:29.0198780Z","Queue":"default"}'),
(58, 42, '2019-04-11 17:55:35.282320', 'Processing', NULL, '{"StartedAt":"2019-04-11T17:55:35.2791030Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(59, 42, '2019-04-11 17:55:35.299374', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T17:55:35.2953180Z","PerformanceDuration":"9","Latency":"20116"}'),
(60, 43, '2019-04-11 17:55:35.306971', 'Processing', NULL, '{"StartedAt":"2019-04-11T17:55:35.3037870Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(61, 43, '2019-04-11 17:55:35.322861', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T17:55:35.3186480Z","PerformanceDuration":"7","Latency":"17298"}'),
(62, 44, '2019-04-11 17:55:35.332214', 'Processing', NULL, '{"StartedAt":"2019-04-11T17:55:35.3298020Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(63, 44, '2019-04-11 17:55:35.347788', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T17:55:35.3433590Z","PerformanceDuration":"8","Latency":"6313"}'),
(64, 45, '2019-04-11 18:01:16.768599', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:01:16.7644340Z","Queue":"default"}'),
(65, 46, '2019-04-11 18:01:27.406224', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:01:27.4026390Z","Queue":"default"}'),
(66, 45, '2019-04-11 18:01:35.375416', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:01:35.3716520Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(67, 45, '2019-04-11 18:01:35.389939', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:01:35.3861680Z","PerformanceDuration":"7","Latency":"18613"}'),
(68, 46, '2019-04-11 18:01:35.397754', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:01:35.3954210Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(69, 46, '2019-04-11 18:01:35.411615', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:01:35.4076940Z","PerformanceDuration":"7","Latency":"7996"}'),
(70, 47, '2019-04-11 18:02:33.124892', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:02:33.1211260Z","Queue":"default"}'),
(71, 47, '2019-04-11 18:02:35.423463', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:02:35.4201900Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(72, 47, '2019-04-11 18:02:35.440986', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:02:35.4364420Z","PerformanceDuration":"9","Latency":"2304"}'),
(73, 48, '2019-04-11 18:02:57.782998', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:02:57.7790180Z","Queue":"default"}'),
(74, 49, '2019-04-11 18:03:12.933009', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:03:12.9293610Z","Queue":"default"}'),
(75, 50, '2019-04-11 18:03:15.336680', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:03:15.3329670Z","Queue":"default"}'),
(76, 48, '2019-04-11 18:03:35.457627', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:03:35.4548460Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(77, 48, '2019-04-11 18:03:35.472631', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:03:35.4687930Z","PerformanceDuration":"7","Latency":"37681"}'),
(78, 49, '2019-04-11 18:03:35.479589', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:03:35.4774160Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(79, 49, '2019-04-11 18:03:35.493063', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:03:35.4893810Z","PerformanceDuration":"7","Latency":"22551"}'),
(80, 50, '2019-04-11 18:03:35.499800', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:03:35.4976850Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(81, 50, '2019-04-11 18:03:35.513398', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:03:35.5097640Z","PerformanceDuration":"6","Latency":"20169"}'),
(82, 51, '2019-04-11 18:04:34.658680', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:04:34.6552410Z","Queue":"default"}'),
(83, 51, '2019-04-11 18:04:35.525903', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:04:35.5227000Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(84, 51, '2019-04-11 18:04:35.544721', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:04:35.5406660Z","PerformanceDuration":"9","Latency":"874"}'),
(85, 52, '2019-04-11 18:04:57.021311', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:04:57.0164470Z","Queue":"default"}'),
(86, 52, '2019-04-11 18:05:35.559648', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:05:35.5564670Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(87, 52, '2019-04-11 18:05:35.584891', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:05:35.5807480Z","PerformanceDuration":"14","Latency":"38548"}'),
(88, 53, '2019-04-11 18:05:37.212393', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:05:37.2086430Z","Queue":"default"}'),
(89, 53, '2019-04-11 18:06:35.609835', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:06:35.6069360Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(90, 53, '2019-04-11 18:06:35.625194', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:06:35.6204160Z","PerformanceDuration":"7","Latency":"58403"}'),
(91, 54, '2019-04-11 18:07:19.734563', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:07:19.7226040Z","Queue":"default"}'),
(92, 55, '2019-04-11 18:07:23.176970', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:07:23.1714640Z","Queue":"default"}'),
(93, 56, '2019-04-11 18:07:29.929343', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:07:29.9242030Z","Queue":"default"}'),
(94, 54, '2019-04-11 18:07:35.638621', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:07:35.6353080Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(95, 54, '2019-04-11 18:07:35.657124', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:07:35.6533470Z","PerformanceDuration":"9","Latency":"15920"}'),
(96, 55, '2019-04-11 18:07:35.665153', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:07:35.6627630Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(97, 55, '2019-04-11 18:07:35.680968', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:07:35.6768800Z","PerformanceDuration":"8","Latency":"12495"}'),
(98, 56, '2019-04-11 18:07:35.688433', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:07:35.6860670Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(99, 56, '2019-04-11 18:07:35.704422', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:07:35.6999850Z","PerformanceDuration":"7","Latency":"5766"}'),
(100, 57, '2019-04-11 18:07:52.255389', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:07:52.2507050Z","Queue":"default"}'),
(101, 58, '2019-04-11 18:08:23.196504', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:08:23.1918500Z","Queue":"default"}'),
(102, 57, '2019-04-11 18:08:35.717044', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:08:35.7143130Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(103, 57, '2019-04-11 18:08:35.732141', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:08:35.7285300Z","PerformanceDuration":"7","Latency":"43469"}'),
(104, 58, '2019-04-11 18:08:35.738814', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:08:35.7366080Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(105, 58, '2019-04-11 18:08:35.752062', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:08:35.7487910Z","PerformanceDuration":"7","Latency":"12548"}'),
(106, 59, '2019-04-11 18:09:50.652326', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:09:50.6444730Z","Queue":"default"}'),
(107, 59, '2019-04-11 18:10:35.767822', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:10:35.7652570Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(108, 59, '2019-04-11 18:10:35.781851', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:10:35.7782320Z","PerformanceDuration":"7","Latency":"45125"}'),
(109, 60, '2019-04-11 18:12:41.675031', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:12:41.6691150Z","Queue":"default"}'),
(110, 61, '2019-04-11 18:13:04.215553', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:13:04.2117060Z","Queue":"default"}'),
(111, 62, '2019-04-11 18:13:14.900848', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:13:14.8960730Z","Queue":"default"}'),
(112, 60, '2019-04-11 18:13:35.798299', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:13:35.7958920Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(113, 60, '2019-04-11 18:13:35.812457', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:13:35.8089420Z","PerformanceDuration":"7","Latency":"54130"}'),
(114, 61, '2019-04-11 18:13:35.819673', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:13:35.8173090Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(115, 61, '2019-04-11 18:13:35.833164', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:13:35.8294990Z","PerformanceDuration":"7","Latency":"31609"}'),
(116, 62, '2019-04-11 18:13:35.840465', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:13:35.8381650Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(117, 62, '2019-04-11 18:13:35.855001', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:13:35.8504290Z","PerformanceDuration":"7","Latency":"20946"}'),
(118, 63, '2019-04-11 18:13:43.263485', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:13:43.2563180Z","Queue":"default"}'),
(119, 63, '2019-04-11 18:14:35.868260', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:14:35.8655330Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(120, 63, '2019-04-11 18:14:35.885458', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:14:35.8808300Z","PerformanceDuration":"7","Latency":"52615"}'),
(121, 64, '2019-04-11 18:16:26.121133', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:16:26.1167750Z","Queue":"default"}'),
(122, 64, '2019-04-11 18:16:35.897919', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:16:35.8952120Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(123, 64, '2019-04-11 18:16:35.915927', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:16:35.9117110Z","PerformanceDuration":"10","Latency":"9783"}'),
(124, 65, '2019-04-11 18:17:09.132542', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:17:09.1290130Z","Queue":"default"}'),
(125, 66, '2019-04-11 18:17:26.629125', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:17:26.6248360Z","Queue":"default"}'),
(126, 65, '2019-04-11 18:17:35.929406', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:17:35.9265410Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(127, 65, '2019-04-11 18:17:35.944686', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:17:35.9404310Z","PerformanceDuration":"7","Latency":"26802"}'),
(128, 66, '2019-04-11 18:17:35.952904', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:17:35.9502200Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(129, 66, '2019-04-11 18:17:35.970230', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:17:35.9658670Z","PerformanceDuration":"9","Latency":"9329"}'),
(130, 67, '2019-04-11 18:19:03.886563', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:19:03.8828410Z","Queue":"default"}'),
(131, 67, '2019-04-11 18:19:35.984862', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:19:35.9820180Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(132, 67, '2019-04-11 18:19:35.999981', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:19:35.9965580Z","PerformanceDuration":"8","Latency":"32104"}'),
(133, 68, '2019-04-11 18:19:38.058625', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:19:38.0543820Z","Queue":"default"}'),
(134, 69, '2019-04-11 18:19:42.738445', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:19:42.7344540Z","Queue":"default"}'),
(135, 70, '2019-04-11 18:19:58.197610', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:19:58.1940250Z","Queue":"default"}'),
(136, 71, '2019-04-11 18:20:04.468435', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-11T18:20:04.4644730Z","Queue":"default"}'),
(137, 68, '2019-04-11 18:20:36.012629', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:20:36.0097980Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(138, 68, '2019-04-11 18:20:36.029385', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:20:36.0253070Z","PerformanceDuration":"9","Latency":"57960"}'),
(139, 69, '2019-04-11 18:20:36.037857', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:20:36.0339710Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(140, 69, '2019-04-11 18:20:36.052616', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:20:36.0481200Z","PerformanceDuration":"6","Latency":"53305"}'),
(141, 70, '2019-04-11 18:20:36.061364', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:20:36.0588080Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(142, 70, '2019-04-11 18:20:36.077251', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:20:36.0732020Z","PerformanceDuration":"9","Latency":"37869"}'),
(143, 71, '2019-04-11 18:20:36.084248', 'Processing', NULL, '{"StartedAt":"2019-04-11T18:20:36.0817020Z","ServerId":"6b9bdd64-aad0-4217-8bdb-e8c3acdd0ccf","WorkerId":"239a6a82-81f7-40d4-b656-d6ecd7aa9ea2"}'),
(144, 71, '2019-04-11 18:20:36.099698', 'Succeeded', NULL, '{"SucceededAt":"2019-04-11T18:20:36.0953040Z","PerformanceDuration":"6","Latency":"31623"}'),
(145, 72, '2019-04-12 01:43:22.423443', 'Enqueued', NULL, '{"EnqueuedAt":"2019-04-12T01:43:22.3155630Z","Queue":"default"}'),
(146, 72, '2019-04-12 01:44:06.009696', 'Processing', NULL, '{"StartedAt":"2019-04-12T01:44:05.9784240Z","ServerId":"26bb9345-c84b-4407-b780-b3d37b7ae829","WorkerId":"d434e05f-f7e1-44f4-a733-49b1f121b104"}'),
(147, 72, '2019-04-12 01:44:06.098319', 'Succeeded', NULL, '{"SucceededAt":"2019-04-12T01:44:06.0793570Z","PerformanceDuration":"58","Latency":"43649"}');

-- --------------------------------------------------------

--
-- Table structure for table `PaymentHistories`
--

CREATE TABLE IF NOT EXISTS `PaymentHistories` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Title` varchar(500) NOT NULL,
  `Description` varchar(2000) NOT NULL,
  `Reference` varchar(255) DEFAULT NULL,
  `Response` longtext,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `StatusId` int(11) NOT NULL,
  `Amount` decimal(65,30) NOT NULL,
  `WalletOperation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PaymentTypes`
--

CREATE TABLE IF NOT EXISTS `PaymentTypes` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PaymentTypes`
--

INSERT INTO `PaymentTypes` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `Name`, `Description`, `TenantId`) VALUES
(1, '2019-04-10 17:30:41.007168', NULL, NULL, NULL, b'0', NULL, NULL, 'ContestPayment', 'Payment required to complete contest creation.', 1),
(2, '2019-04-10 17:30:41.026048', NULL, NULL, NULL, b'0', NULL, NULL, 'EntryPayment', 'Payment required to complete contest entry.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Periodics`
--

CREATE TABLE IF NOT EXISTS `Periodics` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Periodics`
--

INSERT INTO `Periodics` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `Name`, `Description`, `TenantId`) VALUES
(1, '2019-04-10 17:30:40.855994', NULL, NULL, NULL, b'0', NULL, NULL, 'Daily', NULL, 1),
(2, '2019-04-10 17:30:40.868729', NULL, NULL, NULL, b'0', NULL, NULL, 'Weekly', NULL, 1),
(3, '2019-04-10 17:30:40.869310', NULL, NULL, NULL, b'0', NULL, NULL, 'TwoWeeks', NULL, 1),
(4, '2019-04-10 17:30:40.869785', NULL, NULL, NULL, b'0', NULL, NULL, 'Monthly', NULL, 1),
(5, '2019-04-10 17:30:40.870233', NULL, NULL, NULL, b'0', NULL, NULL, 'TwoMonths', NULL, 1),
(6, '2019-04-10 17:30:40.870648', NULL, NULL, NULL, b'0', NULL, NULL, 'ThreeMonths', NULL, 1),
(7, '2019-04-10 17:30:40.871420', NULL, NULL, NULL, b'0', NULL, NULL, 'SixMonths', NULL, 1),
(8, '2019-04-10 17:30:40.872073', NULL, NULL, NULL, b'0', NULL, NULL, 'Yearly', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Referrals`
--

CREATE TABLE IF NOT EXISTS `Referrals` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `UnderUserId` bigint(20) NOT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `ReferrerAwardedVotePoint` int(11) DEFAULT NULL,
  `ReferrerAwardedDate` datetime(6) DEFAULT NULL,
  `ReferrerSponsorPercentage` double DEFAULT NULL,
  `ReferrerSponsorAmount` decimal(65,30) DEFAULT NULL,
  `ReferrerSponsorDate` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Sponsors`
--

CREATE TABLE IF NOT EXISTS `Sponsors` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Logo` varchar(60) DEFAULT NULL,
  `Banner` varchar(60) DEFAULT NULL,
  `Permalink` varchar(255) DEFAULT NULL,
  `Description` varchar(1500) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `FaceBook` varchar(255) DEFAULT NULL,
  `LinkedIn` varchar(255) DEFAULT NULL,
  `Twitter` varchar(255) DEFAULT NULL,
  `Instagram` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `States`
--

CREATE TABLE IF NOT EXISTS `States` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `CountryId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `States`
--

INSERT INTO `States` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `Name`, `TenantId`, `CountryId`) VALUES
(1, '2019-04-10 17:30:39.714994', NULL, NULL, NULL, b'0', NULL, NULL, 'Abia', 1, 'NG'),
(2, '2019-04-10 17:30:39.740518', NULL, NULL, NULL, b'0', NULL, NULL, 'Kebbi', 1, 'NG'),
(3, '2019-04-10 17:30:39.740965', NULL, NULL, NULL, b'0', NULL, NULL, 'Kogi', 1, 'NG'),
(4, '2019-04-10 17:30:39.741400', NULL, NULL, NULL, b'0', NULL, NULL, 'Kwara', 1, 'NG'),
(5, '2019-04-10 17:30:39.741835', NULL, NULL, NULL, b'0', NULL, NULL, 'Lagos', 1, 'NG'),
(6, '2019-04-10 17:30:39.742252', NULL, NULL, NULL, b'0', NULL, NULL, 'Nasarawa', 1, 'NG'),
(7, '2019-04-10 17:30:39.742680', NULL, NULL, NULL, b'0', NULL, NULL, 'Niger', 1, 'NG'),
(8, '2019-04-10 17:30:39.743104', NULL, NULL, NULL, b'0', NULL, NULL, 'Ogun', 1, 'NG'),
(9, '2019-04-10 17:30:39.743529', NULL, NULL, NULL, b'0', NULL, NULL, 'Ondo', 1, 'NG'),
(10, '2019-04-10 17:30:39.743956', NULL, NULL, NULL, b'0', NULL, NULL, 'Osun', 1, 'NG'),
(11, '2019-04-10 17:30:39.744416', NULL, NULL, NULL, b'0', NULL, NULL, 'Oyo', 1, 'NG'),
(12, '2019-04-10 17:30:39.744840', NULL, NULL, NULL, b'0', NULL, NULL, 'Plateau', 1, 'NG'),
(13, '2019-04-10 17:30:39.745519', NULL, NULL, NULL, b'0', NULL, NULL, 'Rivers', 1, 'NG'),
(14, '2019-04-10 17:30:39.745990', NULL, NULL, NULL, b'0', NULL, NULL, 'Sokoto', 1, 'NG'),
(15, '2019-04-10 17:30:39.746433', NULL, NULL, NULL, b'0', NULL, NULL, 'Taraba', 1, 'NG'),
(16, '2019-04-10 17:30:39.746870', NULL, NULL, NULL, b'0', NULL, NULL, 'Yobe', 1, 'NG'),
(17, '2019-04-10 17:30:39.740029', NULL, NULL, NULL, b'0', NULL, NULL, 'Katsina', 1, 'NG'),
(18, '2019-04-10 17:30:39.747293', NULL, NULL, NULL, b'0', NULL, NULL, 'Zamfara', 1, 'NG'),
(19, '2019-04-10 17:30:39.739549', NULL, NULL, NULL, b'0', NULL, NULL, 'Kano', 1, 'NG'),
(20, '2019-04-10 17:30:39.738621', NULL, NULL, NULL, b'0', NULL, NULL, 'Jigawa', 1, 'NG'),
(21, '2019-04-10 17:30:39.731093', NULL, NULL, NULL, b'0', NULL, NULL, 'Adamawa', 1, 'NG'),
(22, '2019-04-10 17:30:39.731846', NULL, NULL, NULL, b'0', NULL, NULL, 'Akwa Ibom', 1, 'NG'),
(23, '2019-04-10 17:30:39.732494', NULL, NULL, NULL, b'0', NULL, NULL, 'Anambra', 1, 'NG'),
(24, '2019-04-10 17:30:39.732975', NULL, NULL, NULL, b'0', NULL, NULL, 'Bauchi', 1, 'NG'),
(25, '2019-04-10 17:30:39.733510', NULL, NULL, NULL, b'0', NULL, NULL, 'Bayelsa', 1, 'NG'),
(26, '2019-04-10 17:30:39.734058', NULL, NULL, NULL, b'0', NULL, NULL, 'Benue', 1, 'NG'),
(27, '2019-04-10 17:30:39.734514', NULL, NULL, NULL, b'0', NULL, NULL, 'Borno', 1, 'NG'),
(28, '2019-04-10 17:30:39.734964', NULL, NULL, NULL, b'0', NULL, NULL, 'Cross River', 1, 'NG'),
(29, '2019-04-10 17:30:39.735394', NULL, NULL, NULL, b'0', NULL, NULL, 'Delta', 1, 'NG'),
(30, '2019-04-10 17:30:39.735862', NULL, NULL, NULL, b'0', NULL, NULL, 'Ebonyi', 1, 'NG'),
(31, '2019-04-10 17:30:39.736313', NULL, NULL, NULL, b'0', NULL, NULL, 'Edo', 1, 'NG'),
(32, '2019-04-10 17:30:39.736748', NULL, NULL, NULL, b'0', NULL, NULL, 'Ekiti', 1, 'NG'),
(33, '2019-04-10 17:30:39.737181', NULL, NULL, NULL, b'0', NULL, NULL, 'Enugu', 1, 'NG'),
(34, '2019-04-10 17:30:39.737632', NULL, NULL, NULL, b'0', NULL, NULL, 'Gombe', 1, 'NG'),
(35, '2019-04-10 17:30:39.738152', NULL, NULL, NULL, b'0', NULL, NULL, 'Imo', 1, 'NG'),
(36, '2019-04-10 17:30:39.739090', NULL, NULL, NULL, b'0', NULL, NULL, 'Kaduna', 1, 'NG'),
(37, '2019-04-10 17:30:39.747737', NULL, NULL, NULL, b'0', NULL, NULL, 'FCT', 1, 'NG');

-- --------------------------------------------------------

--
-- Table structure for table `Statuses`
--

CREATE TABLE IF NOT EXISTS `Statuses` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Description` varchar(100) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Statuses`
--

INSERT INTO `Statuses` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `Name`, `Description`, `TenantId`) VALUES
(1, '2019-04-10 17:30:40.468415', NULL, NULL, NULL, b'0', NULL, NULL, 'Pending', NULL, 1),
(2, '2019-04-10 17:30:40.492490', NULL, NULL, NULL, b'0', NULL, NULL, 'Closed', NULL, 1),
(3, '2019-04-10 17:30:40.493173', NULL, NULL, NULL, b'0', NULL, NULL, 'Cancelled', NULL, 1),
(4, '2019-04-10 17:30:40.493715', NULL, NULL, NULL, b'0', NULL, NULL, 'Processing', NULL, 1),
(5, '2019-04-10 17:30:40.494246', NULL, NULL, NULL, b'0', NULL, NULL, 'Paid', NULL, 1),
(6, '2019-04-10 17:30:40.494699', NULL, NULL, NULL, b'0', NULL, NULL, 'Suspended', NULL, 1),
(7, '2019-04-10 17:30:40.495160', NULL, NULL, NULL, b'0', NULL, NULL, 'PaymentConfirmed', NULL, 1),
(8, '2019-04-10 17:30:40.495589', NULL, NULL, NULL, b'0', NULL, NULL, 'PendingPaymentConfirmation', NULL, 1),
(9, '2019-04-10 17:30:40.496021', NULL, NULL, NULL, b'0', NULL, NULL, 'PendingPaymentRenewal', NULL, 1),
(10, '2019-04-10 17:30:40.496460', NULL, NULL, NULL, b'0', NULL, NULL, 'Active', NULL, 1),
(11, '2019-04-10 17:30:40.496994', NULL, NULL, NULL, b'0', NULL, NULL, 'PaymentFailed', NULL, 1),
(12, '2019-04-10 17:30:40.497444', NULL, NULL, NULL, b'0', NULL, NULL, 'PaymentPending', NULL, 1),
(13, '2019-04-10 17:30:40.497901', NULL, NULL, NULL, b'0', NULL, NULL, 'PendingClosure', NULL, 1),
(14, '2019-04-10 17:30:40.498425', NULL, NULL, NULL, b'0', NULL, NULL, 'Success', NULL, 1),
(15, '2019-04-10 17:30:40.498918', NULL, NULL, NULL, b'0', NULL, NULL, 'Failed', NULL, 1),
(16, '2019-04-10 17:30:40.499347', NULL, NULL, NULL, b'0', NULL, NULL, 'Disapproved', NULL, 1),
(17, '2019-04-10 17:30:40.499994', NULL, NULL, NULL, b'0', NULL, NULL, 'Paused', NULL, 1),
(18, '2019-04-10 17:30:40.500709', NULL, NULL, NULL, b'0', NULL, NULL, 'Live', NULL, 1),
(19, '2019-04-10 17:30:40.503060', NULL, NULL, NULL, b'0', NULL, NULL, 'ContestEntryClosed', NULL, 1),
(20, '2019-04-10 17:30:40.503939', NULL, NULL, NULL, b'0', NULL, NULL, 'Expired', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `UserVotePoints`
--

CREATE TABLE IF NOT EXISTS `UserVotePoints` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Points` int(11) NOT NULL,
  `UserId` bigint(20) NOT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `UserVotePoints`
--

INSERT INTO `UserVotePoints` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `Points`, `UserId`, `TenantId`) VALUES
(2, '2019-04-11 12:52:33.547678', 2, NULL, NULL, b'0', NULL, NULL, 0, 2, 1),
(3, '2019-04-11 18:53:11.953047', 3, NULL, NULL, b'0', NULL, NULL, 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Votes`
--

CREATE TABLE IF NOT EXISTS `Votes` (
  `Id` bigint(20) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `UserId` bigint(20) DEFAULT NULL,
  `ContestId` int(11) NOT NULL,
  `ContestEntryId` int(11) NOT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `Hash` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `VoteTypes`
--

CREATE TABLE IF NOT EXISTS `VoteTypes` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `VotePoint` int(11) NOT NULL,
  `RewardPoint` int(11) NOT NULL,
  `Amount` decimal(65,30) NOT NULL,
  `MinAmount` decimal(65,30) NOT NULL,
  `MaxAmount` decimal(65,30) NOT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `VoteTypes`
--

INSERT INTO `VoteTypes` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `Name`, `Description`, `VotePoint`, `RewardPoint`, `Amount`, `MinAmount`, `MaxAmount`, `TenantId`) VALUES
(1, '2019-04-10 17:30:40.647943', NULL, NULL, NULL, b'0', NULL, NULL, 'Paid', NULL, 0, 10, 500.000000000000000000000000000000, 100.000000000000000000000000000000, 500.000000000000000000000000000000, 1),
(2, '2019-04-10 17:30:40.674382', NULL, NULL, NULL, b'0', NULL, NULL, 'VotePoint', NULL, 10, 1, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 1),
(3, '2019-04-10 17:30:40.690951', NULL, NULL, NULL, b'0', NULL, NULL, 'Unique', NULL, 0, 5, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 1),
(4, '2019-04-10 17:30:40.702401', NULL, NULL, NULL, b'0', NULL, NULL, 'Daily', NULL, 0, 2, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 1),
(5, '2019-04-10 17:30:40.711889', NULL, NULL, NULL, b'0', NULL, NULL, 'Hourly', NULL, 0, 1, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 1),
(6, '2019-04-10 17:30:40.721763', NULL, NULL, NULL, b'0', NULL, NULL, 'Open', NULL, 0, 0, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Wallets`
--

CREATE TABLE IF NOT EXISTS `Wallets` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `Balance` decimal(65,30) NOT NULL,
  `LastCreditedAmount` decimal(65,30) DEFAULT NULL,
  `LastDebitedAmount` decimal(65,30) DEFAULT NULL,
  `TenantId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Wallets`
--

INSERT INTO `Wallets` (`Id`, `CreationTime`, `CreatorUserId`, `LastModificationTime`, `LastModifierUserId`, `IsDeleted`, `DeleterUserId`, `DeletionTime`, `UserId`, `Balance`, `LastCreditedAmount`, `LastDebitedAmount`, `TenantId`) VALUES
(2, '2019-04-11 12:52:33.531944', 2, NULL, NULL, b'0', NULL, NULL, 2, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 1),
(3, '2019-04-11 18:53:11.787899', 3, NULL, NULL, b'0', NULL, NULL, 3, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 0.000000000000000000000000000000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Withdraws`
--

CREATE TABLE IF NOT EXISTS `Withdraws` (
  `Id` int(11) NOT NULL,
  `CreationTime` datetime(6) NOT NULL,
  `CreatorUserId` bigint(20) DEFAULT NULL,
  `LastModificationTime` datetime(6) DEFAULT NULL,
  `LastModifierUserId` bigint(20) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `DeleterUserId` bigint(20) DEFAULT NULL,
  `DeletionTime` datetime(6) DEFAULT NULL,
  `UserId` bigint(20) NOT NULL,
  `Amount` decimal(65,30) NOT NULL,
  `TenantId` int(11) DEFAULT NULL,
  `PayIn` longtext NOT NULL,
  `PayTo` longtext NOT NULL,
  `BankDetailsId` int(11) NOT NULL,
  `StatusId` int(11) NOT NULL,
  `FeePercentage` int(11) NOT NULL,
  `FeeAmount` decimal(65,30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AbpAuditLogs`
--
ALTER TABLE `AbpAuditLogs`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpAuditLogs_TenantId_ExecutionDuration` (`TenantId`,`ExecutionDuration`),
  ADD KEY `IX_AbpAuditLogs_TenantId_ExecutionTime` (`TenantId`,`ExecutionTime`),
  ADD KEY `IX_AbpAuditLogs_TenantId_UserId` (`TenantId`,`UserId`);

--
-- Indexes for table `AbpBackgroundJobs`
--
ALTER TABLE `AbpBackgroundJobs`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpBackgroundJobs_IsAbandoned_NextTryTime` (`IsAbandoned`,`NextTryTime`);

--
-- Indexes for table `AbpEditions`
--
ALTER TABLE `AbpEditions`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `AbpEntityChanges`
--
ALTER TABLE `AbpEntityChanges`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpEntityChanges_EntityChangeSetId` (`EntityChangeSetId`),
  ADD KEY `IX_AbpEntityChanges_EntityTypeFullName_EntityId` (`EntityTypeFullName`,`EntityId`);

--
-- Indexes for table `AbpEntityChangeSets`
--
ALTER TABLE `AbpEntityChangeSets`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpEntityChangeSets_TenantId_CreationTime` (`TenantId`,`CreationTime`),
  ADD KEY `IX_AbpEntityChangeSets_TenantId_Reason` (`TenantId`,`Reason`),
  ADD KEY `IX_AbpEntityChangeSets_TenantId_UserId` (`TenantId`,`UserId`);

--
-- Indexes for table `AbpEntityPropertyChanges`
--
ALTER TABLE `AbpEntityPropertyChanges`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpEntityPropertyChanges_EntityChangeId` (`EntityChangeId`);

--
-- Indexes for table `AbpFeatures`
--
ALTER TABLE `AbpFeatures`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpFeatures_EditionId_Name` (`EditionId`,`Name`),
  ADD KEY `IX_AbpFeatures_TenantId_Name` (`TenantId`,`Name`);

--
-- Indexes for table `AbpLanguages`
--
ALTER TABLE `AbpLanguages`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpLanguages_TenantId_Name` (`TenantId`,`Name`);

--
-- Indexes for table `AbpLanguageTexts`
--
ALTER TABLE `AbpLanguageTexts`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpLanguageTexts_TenantId_Source_LanguageName_Key` (`TenantId`,`Source`,`LanguageName`,`Key`);

--
-- Indexes for table `AbpNotifications`
--
ALTER TABLE `AbpNotifications`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `AbpNotificationSubscriptions`
--
ALTER TABLE `AbpNotificationSubscriptions`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpNotificationSubscriptions_NotificationName_EntityTypeName~` (`NotificationName`,`EntityTypeName`,`EntityId`,`UserId`),
  ADD KEY `IX_AbpNotificationSubscriptions_TenantId_NotificationName_Entit~` (`TenantId`,`NotificationName`,`EntityTypeName`,`EntityId`,`UserId`);

--
-- Indexes for table `AbpOrganizationUnits`
--
ALTER TABLE `AbpOrganizationUnits`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpOrganizationUnits_ParentId` (`ParentId`),
  ADD KEY `IX_AbpOrganizationUnits_TenantId_Code` (`TenantId`,`Code`);

--
-- Indexes for table `AbpPermissions`
--
ALTER TABLE `AbpPermissions`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpPermissions_TenantId_Name` (`TenantId`,`Name`),
  ADD KEY `IX_AbpPermissions_RoleId` (`RoleId`),
  ADD KEY `IX_AbpPermissions_UserId` (`UserId`);

--
-- Indexes for table `AbpRoleClaims`
--
ALTER TABLE `AbpRoleClaims`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpRoleClaims_RoleId` (`RoleId`),
  ADD KEY `IX_AbpRoleClaims_TenantId_ClaimType` (`TenantId`,`ClaimType`);

--
-- Indexes for table `AbpRoles`
--
ALTER TABLE `AbpRoles`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpRoles_CreatorUserId` (`CreatorUserId`),
  ADD KEY `IX_AbpRoles_DeleterUserId` (`DeleterUserId`),
  ADD KEY `IX_AbpRoles_LastModifierUserId` (`LastModifierUserId`),
  ADD KEY `IX_AbpRoles_TenantId_NormalizedName` (`TenantId`,`NormalizedName`);

--
-- Indexes for table `AbpSettings`
--
ALTER TABLE `AbpSettings`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpSettings_UserId` (`UserId`),
  ADD KEY `IX_AbpSettings_TenantId_Name` (`TenantId`,`Name`);

--
-- Indexes for table `AbpTenantNotifications`
--
ALTER TABLE `AbpTenantNotifications`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpTenantNotifications_TenantId` (`TenantId`);

--
-- Indexes for table `AbpTenants`
--
ALTER TABLE `AbpTenants`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpTenants_CreatorUserId` (`CreatorUserId`),
  ADD KEY `IX_AbpTenants_DeleterUserId` (`DeleterUserId`),
  ADD KEY `IX_AbpTenants_EditionId` (`EditionId`),
  ADD KEY `IX_AbpTenants_LastModifierUserId` (`LastModifierUserId`),
  ADD KEY `IX_AbpTenants_TenancyName` (`TenancyName`);

--
-- Indexes for table `AbpUserAccounts`
--
ALTER TABLE `AbpUserAccounts`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpUserAccounts_EmailAddress` (`EmailAddress`),
  ADD KEY `IX_AbpUserAccounts_UserName` (`UserName`),
  ADD KEY `IX_AbpUserAccounts_TenantId_EmailAddress` (`TenantId`,`EmailAddress`),
  ADD KEY `IX_AbpUserAccounts_TenantId_UserId` (`TenantId`,`UserId`),
  ADD KEY `IX_AbpUserAccounts_TenantId_UserName` (`TenantId`,`UserName`);

--
-- Indexes for table `AbpUserClaims`
--
ALTER TABLE `AbpUserClaims`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpUserClaims_UserId` (`UserId`),
  ADD KEY `IX_AbpUserClaims_TenantId_ClaimType` (`TenantId`,`ClaimType`);

--
-- Indexes for table `AbpUserLoginAttempts`
--
ALTER TABLE `AbpUserLoginAttempts`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpUserLoginAttempts_UserId_TenantId` (`UserId`,`TenantId`),
  ADD KEY `IX_AbpUserLoginAttempts_TenancyName_UserNameOrEmailAddress_Resu~` (`TenancyName`,`UserNameOrEmailAddress`,`Result`);

--
-- Indexes for table `AbpUserLogins`
--
ALTER TABLE `AbpUserLogins`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpUserLogins_UserId` (`UserId`),
  ADD KEY `IX_AbpUserLogins_TenantId_UserId` (`TenantId`,`UserId`),
  ADD KEY `IX_AbpUserLogins_TenantId_LoginProvider_ProviderKey` (`TenantId`,`LoginProvider`,`ProviderKey`);

--
-- Indexes for table `AbpUserNotifications`
--
ALTER TABLE `AbpUserNotifications`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpUserNotifications_UserId_State_CreationTime` (`UserId`,`State`,`CreationTime`);

--
-- Indexes for table `AbpUserOrganizationUnits`
--
ALTER TABLE `AbpUserOrganizationUnits`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpUserOrganizationUnits_TenantId_OrganizationUnitId` (`TenantId`,`OrganizationUnitId`),
  ADD KEY `IX_AbpUserOrganizationUnits_TenantId_UserId` (`TenantId`,`UserId`);

--
-- Indexes for table `AbpUserRoles`
--
ALTER TABLE `AbpUserRoles`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpUserRoles_UserId` (`UserId`),
  ADD KEY `IX_AbpUserRoles_TenantId_RoleId` (`TenantId`,`RoleId`),
  ADD KEY `IX_AbpUserRoles_TenantId_UserId` (`TenantId`,`UserId`);

--
-- Indexes for table `AbpUsers`
--
ALTER TABLE `AbpUsers`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `UserEmailAddress` (`EmailAddress`),
  ADD UNIQUE KEY `UserPermalink` (`Permalink`),
  ADD UNIQUE KEY `UserPhoneNumber` (`PhoneNumber`),
  ADD KEY `IX_AbpUsers_CountryId` (`CountryId`),
  ADD KEY `IX_AbpUsers_CreatorUserId` (`CreatorUserId`),
  ADD KEY `IX_AbpUsers_DeleterUserId` (`DeleterUserId`),
  ADD KEY `IX_AbpUsers_LastModifierUserId` (`LastModifierUserId`),
  ADD KEY `IX_AbpUsers_StateId` (`StateId`),
  ADD KEY `IX_AbpUsers_TenantId_NormalizedEmailAddress` (`TenantId`,`NormalizedEmailAddress`),
  ADD KEY `IX_AbpUsers_TenantId_NormalizedUserName` (`TenantId`,`NormalizedUserName`),
  ADD KEY `IX_AbpUsers_UserName_NickName_Image` (`UserName`,`NickName`,`Image`);

--
-- Indexes for table `AbpUserTokens`
--
ALTER TABLE `AbpUserTokens`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AbpUserTokens_UserId` (`UserId`),
  ADD KEY `IX_AbpUserTokens_TenantId_UserId` (`TenantId`,`UserId`);

--
-- Indexes for table `AdvertHomePageBanners`
--
ALTER TABLE `AdvertHomePageBanners`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AdvertHomePageBanners_AdvertPriceId` (`AdvertPriceId`),
  ADD KEY `IX_AdvertHomePageBanners_StatusId` (`StatusId`),
  ADD KEY `IX_AdvertHomePageBanners_UserId` (`UserId`);

--
-- Indexes for table `AdvertPrices`
--
ALTER TABLE `AdvertPrices`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AdvertPrices_Name` (`Name`);

--
-- Indexes for table `AdvertTopBanners`
--
ALTER TABLE `AdvertTopBanners`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_AdvertTopBanners_AdvertPriceId` (`AdvertPriceId`),
  ADD KEY `IX_AdvertTopBanners_StatusId` (`StatusId`),
  ADD KEY `IX_AdvertTopBanners_UserId` (`UserId`);

--
-- Indexes for table `BankDetails`
--
ALTER TABLE `BankDetails`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `IX_BankDetails_AccountNumber` (`AccountNumber`),
  ADD KEY `IX_BankDetails_AccountName` (`AccountName`),
  ADD KEY `IX_BankDetails_BankId` (`BankId`),
  ADD KEY `IX_BankDetails_UserId` (`UserId`);

--
-- Indexes for table `Banks`
--
ALTER TABLE `Banks`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `IX_Banks_Name_Code` (`Name`,`Code`),
  ADD KEY `IX_Banks_Active` (`Active`);

--
-- Indexes for table `ContestCategories`
--
ALTER TABLE `ContestCategories`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `ContestCategoryNameIndex` (`Name`);

--
-- Indexes for table `ContestComments`
--
ALTER TABLE `ContestComments`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_ContestComments_ContestId` (`ContestId`),
  ADD KEY `IX_ContestComments_UserId` (`UserId`);

--
-- Indexes for table `ContestEntries`
--
ALTER TABLE `ContestEntries`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `ContestEntryPermalinkIndex` (`Permalink`),
  ADD KEY `IX_ContestEntries_ContestId` (`ContestId`),
  ADD KEY `IX_ContestEntries_IsFeatured` (`IsFeatured`),
  ADD KEY `IX_ContestEntries_StatusId` (`StatusId`),
  ADD KEY `IX_ContestEntries_UserId` (`UserId`);

--
-- Indexes for table `ContestEntryComments`
--
ALTER TABLE `ContestEntryComments`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_ContestEntryComments_ContestEntryId` (`ContestEntryId`),
  ADD KEY `IX_ContestEntryComments_UserId` (`UserId`);

--
-- Indexes for table `ContestEntryShares`
--
ALTER TABLE `ContestEntryShares`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `ContestEntryTypes`
--
ALTER TABLE `ContestEntryTypes`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `ContestEntryTypeNameIndex` (`Name`);

--
-- Indexes for table `ContestEntryViews`
--
ALTER TABLE `ContestEntryViews`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `ContestFees`
--
ALTER TABLE `ContestFees`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_ContestFees_ContestTypeId` (`ContestTypeId`);

--
-- Indexes for table `ContestPayments`
--
ALTER TABLE `ContestPayments`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_ContestPayments_ContestId` (`ContestId`),
  ADD KEY `IX_ContestPayments_UserId` (`UserId`);

--
-- Indexes for table `Contests`
--
ALTER TABLE `Contests`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `ContestPermalink` (`Permalink`),
  ADD KEY `IX_Contests_ContestCategoryId` (`ContestCategoryId`),
  ADD KEY `IX_Contests_ContestEntryTypeId` (`ContestEntryTypeId`),
  ADD KEY `IX_Contests_ContestFeeId` (`ContestFeeId`),
  ADD KEY `IX_Contests_ContestTypeId` (`ContestTypeId`),
  ADD KEY `IX_Contests_PeriodicId` (`PeriodicId`),
  ADD KEY `IX_Contests_SponsorId` (`SponsorId`),
  ADD KEY `IX_Contests_StatusId` (`StatusId`),
  ADD KEY `IX_Contests_UserId` (`UserId`),
  ADD KEY `IX_Contests_VoteTypeId` (`VoteTypeId`),
  ADD KEY `ContestNameIndex` (`Title`,`Tags`,`CreationTime`,`StartDate`,`EndDate`,`IsFeatured`);

--
-- Indexes for table `ContestTypes`
--
ALTER TABLE `ContestTypes`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `ContestTypeNameIndex` (`Name`);

--
-- Indexes for table `Countries`
--
ALTER TABLE `Countries`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `FeaturedContestEntries`
--
ALTER TABLE `FeaturedContestEntries`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_FeaturedContestEntries_ContestEntryId` (`ContestEntryId`),
  ADD KEY `IX_FeaturedContestEntries_StatusId` (`StatusId`),
  ADD KEY `IX_FeaturedContestEntries_UserId` (`UserId`);

--
-- Indexes for table `FeaturedContests`
--
ALTER TABLE `FeaturedContests`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_FeaturedContests_ContestId` (`ContestId`),
  ADD KEY `IX_FeaturedContests_StatusId` (`StatusId`),
  ADD KEY `IX_FeaturedContests_UserId` (`UserId`),
  ADD KEY `IX_FeaturedContests_StartDate_EndDate` (`StartDate`,`EndDate`);

--
-- Indexes for table `Follows`
--
ALTER TABLE `Follows`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Follows_FollowerUserId` (`FollowerUserId`),
  ADD KEY `IX_Follows_FollowingUserId` (`FollowingUserId`);

--
-- Indexes for table `HangfireAggregatedCounter`
--
ALTER TABLE `HangfireAggregatedCounter`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `IX_HangfireCounterAggregated_Key` (`Key`);

--
-- Indexes for table `HangfireCounter`
--
ALTER TABLE `HangfireCounter`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_HangfireCounter_Key` (`Key`);

--
-- Indexes for table `HangfireHash`
--
ALTER TABLE `HangfireHash`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `IX_HangfireHash_Key_Field` (`Key`,`Field`);

--
-- Indexes for table `HangfireJob`
--
ALTER TABLE `HangfireJob`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_HangfireJob_StateName` (`StateName`);

--
-- Indexes for table `HangfireJobParameter`
--
ALTER TABLE `HangfireJobParameter`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `IX_HangfireJobParameter_JobId_Name` (`JobId`,`Name`),
  ADD KEY `FK_HangfireJobParameter_Job` (`JobId`);

--
-- Indexes for table `HangfireJobQueue`
--
ALTER TABLE `HangfireJobQueue`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_HangfireJobQueue_QueueAndFetchedAt` (`Queue`,`FetchedAt`);

--
-- Indexes for table `HangfireJobState`
--
ALTER TABLE `HangfireJobState`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_HangfireJobState_Job` (`JobId`);

--
-- Indexes for table `HangfireList`
--
ALTER TABLE `HangfireList`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `HangfireServer`
--
ALTER TABLE `HangfireServer`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `HangfireSet`
--
ALTER TABLE `HangfireSet`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `HangfireState`
--
ALTER TABLE `HangfireState`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_HangfireState_Job` (`JobId`);

--
-- Indexes for table `PaymentHistories`
--
ALTER TABLE `PaymentHistories`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `PaymentHistoryReferenceIndex` (`Reference`),
  ADD KEY `IX_PaymentHistories_StatusId` (`StatusId`),
  ADD KEY `IX_PaymentHistories_UserId` (`UserId`);

--
-- Indexes for table `PaymentTypes`
--
ALTER TABLE `PaymentTypes`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `PaymentTypeNameIndex` (`Name`);

--
-- Indexes for table `Periodics`
--
ALTER TABLE `Periodics`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Referrals`
--
ALTER TABLE `Referrals`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Referrals_UnderUserId` (`UnderUserId`),
  ADD KEY `IX_Referrals_UserId` (`UserId`);

--
-- Indexes for table `Sponsors`
--
ALTER TABLE `Sponsors`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `CompanyPermalink` (`Permalink`),
  ADD KEY `IX_Sponsors_UserId` (`UserId`);

--
-- Indexes for table `States`
--
ALTER TABLE `States`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_States_CountryId` (`CountryId`);

--
-- Indexes for table `Statuses`
--
ALTER TABLE `Statuses`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `StatusNameIndex` (`Name`);

--
-- Indexes for table `UserVotePoints`
--
ALTER TABLE `UserVotePoints`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `UserVotePointUserIdIndex` (`UserId`);

--
-- Indexes for table `Votes`
--
ALTER TABLE `Votes`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Votes_ContestEntryId` (`ContestEntryId`),
  ADD KEY `IX_Votes_ContestId` (`ContestId`),
  ADD KEY `VoteHashIndex` (`Hash`),
  ADD KEY `IX_Votes_UserId` (`UserId`);

--
-- Indexes for table `VoteTypes`
--
ALTER TABLE `VoteTypes`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `VoteTypeNameIndex` (`Name`);

--
-- Indexes for table `Wallets`
--
ALTER TABLE `Wallets`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `WalletUserIndex` (`UserId`);

--
-- Indexes for table `Withdraws`
--
ALTER TABLE `Withdraws`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Withdraws_BankDetailsId` (`BankDetailsId`),
  ADD KEY `IX_Withdraws_StatusId` (`StatusId`),
  ADD KEY `IX_Withdraws_UserId` (`UserId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `AbpAuditLogs`
--
ALTER TABLE `AbpAuditLogs`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `AbpBackgroundJobs`
--
ALTER TABLE `AbpBackgroundJobs`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AbpEditions`
--
ALTER TABLE `AbpEditions`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `AbpEntityChanges`
--
ALTER TABLE `AbpEntityChanges`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AbpEntityChangeSets`
--
ALTER TABLE `AbpEntityChangeSets`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AbpEntityPropertyChanges`
--
ALTER TABLE `AbpEntityPropertyChanges`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AbpFeatures`
--
ALTER TABLE `AbpFeatures`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AbpLanguages`
--
ALTER TABLE `AbpLanguages`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `AbpLanguageTexts`
--
ALTER TABLE `AbpLanguageTexts`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AbpOrganizationUnits`
--
ALTER TABLE `AbpOrganizationUnits`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AbpPermissions`
--
ALTER TABLE `AbpPermissions`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=248;
--
-- AUTO_INCREMENT for table `AbpRoleClaims`
--
ALTER TABLE `AbpRoleClaims`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AbpRoles`
--
ALTER TABLE `AbpRoles`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `AbpSettings`
--
ALTER TABLE `AbpSettings`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `AbpTenants`
--
ALTER TABLE `AbpTenants`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `AbpUserAccounts`
--
ALTER TABLE `AbpUserAccounts`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `AbpUserClaims`
--
ALTER TABLE `AbpUserClaims`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `AbpUserLoginAttempts`
--
ALTER TABLE `AbpUserLoginAttempts`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `AbpUserLogins`
--
ALTER TABLE `AbpUserLogins`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AbpUserOrganizationUnits`
--
ALTER TABLE `AbpUserOrganizationUnits`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AbpUserRoles`
--
ALTER TABLE `AbpUserRoles`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `AbpUsers`
--
ALTER TABLE `AbpUsers`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `AbpUserTokens`
--
ALTER TABLE `AbpUserTokens`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AdvertHomePageBanners`
--
ALTER TABLE `AdvertHomePageBanners`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `AdvertPrices`
--
ALTER TABLE `AdvertPrices`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `AdvertTopBanners`
--
ALTER TABLE `AdvertTopBanners`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `BankDetails`
--
ALTER TABLE `BankDetails`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Banks`
--
ALTER TABLE `Banks`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `ContestCategories`
--
ALTER TABLE `ContestCategories`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ContestComments`
--
ALTER TABLE `ContestComments`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ContestEntries`
--
ALTER TABLE `ContestEntries`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ContestEntryComments`
--
ALTER TABLE `ContestEntryComments`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ContestEntryShares`
--
ALTER TABLE `ContestEntryShares`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ContestEntryTypes`
--
ALTER TABLE `ContestEntryTypes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ContestEntryViews`
--
ALTER TABLE `ContestEntryViews`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ContestFees`
--
ALTER TABLE `ContestFees`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ContestPayments`
--
ALTER TABLE `ContestPayments`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Contests`
--
ALTER TABLE `Contests`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ContestTypes`
--
ALTER TABLE `ContestTypes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `FeaturedContestEntries`
--
ALTER TABLE `FeaturedContestEntries`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `FeaturedContests`
--
ALTER TABLE `FeaturedContests`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Follows`
--
ALTER TABLE `Follows`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `HangfireAggregatedCounter`
--
ALTER TABLE `HangfireAggregatedCounter`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `HangfireCounter`
--
ALTER TABLE `HangfireCounter`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `HangfireHash`
--
ALTER TABLE `HangfireHash`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=180;
--
-- AUTO_INCREMENT for table `HangfireJob`
--
ALTER TABLE `HangfireJob`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `HangfireJobParameter`
--
ALTER TABLE `HangfireJobParameter`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=177;
--
-- AUTO_INCREMENT for table `HangfireJobQueue`
--
ALTER TABLE `HangfireJobQueue`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `HangfireJobState`
--
ALTER TABLE `HangfireJobState`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `HangfireList`
--
ALTER TABLE `HangfireList`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `HangfireSet`
--
ALTER TABLE `HangfireSet`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `HangfireState`
--
ALTER TABLE `HangfireState`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=148;
--
-- AUTO_INCREMENT for table `PaymentHistories`
--
ALTER TABLE `PaymentHistories`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `PaymentTypes`
--
ALTER TABLE `PaymentTypes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Periodics`
--
ALTER TABLE `Periodics`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `Referrals`
--
ALTER TABLE `Referrals`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Sponsors`
--
ALTER TABLE `Sponsors`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `States`
--
ALTER TABLE `States`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `Statuses`
--
ALTER TABLE `Statuses`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `UserVotePoints`
--
ALTER TABLE `UserVotePoints`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `Votes`
--
ALTER TABLE `Votes`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `VoteTypes`
--
ALTER TABLE `VoteTypes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `Wallets`
--
ALTER TABLE `Wallets`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `Withdraws`
--
ALTER TABLE `Withdraws`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `AbpEntityChanges`
--
ALTER TABLE `AbpEntityChanges`
  ADD CONSTRAINT `FK_AbpEntityChanges_AbpEntityChangeSets_EntityChangeSetId` FOREIGN KEY (`EntityChangeSetId`) REFERENCES `AbpEntityChangeSets` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `AbpEntityPropertyChanges`
--
ALTER TABLE `AbpEntityPropertyChanges`
  ADD CONSTRAINT `FK_AbpEntityPropertyChanges_AbpEntityChanges_EntityChangeId` FOREIGN KEY (`EntityChangeId`) REFERENCES `AbpEntityChanges` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `AbpFeatures`
--
ALTER TABLE `AbpFeatures`
  ADD CONSTRAINT `FK_AbpFeatures_AbpEditions_EditionId` FOREIGN KEY (`EditionId`) REFERENCES `AbpEditions` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `AbpOrganizationUnits`
--
ALTER TABLE `AbpOrganizationUnits`
  ADD CONSTRAINT `FK_AbpOrganizationUnits_AbpOrganizationUnits_ParentId` FOREIGN KEY (`ParentId`) REFERENCES `AbpOrganizationUnits` (`Id`) ON DELETE NO ACTION;

--
-- Constraints for table `AbpPermissions`
--
ALTER TABLE `AbpPermissions`
  ADD CONSTRAINT `FK_AbpPermissions_AbpRoles_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `AbpRoles` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_AbpPermissions_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `AbpRoleClaims`
--
ALTER TABLE `AbpRoleClaims`
  ADD CONSTRAINT `FK_AbpRoleClaims_AbpRoles_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `AbpRoles` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `AbpRoles`
--
ALTER TABLE `AbpRoles`
  ADD CONSTRAINT `FK_AbpRoles_AbpUsers_CreatorUserId` FOREIGN KEY (`CreatorUserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_AbpRoles_AbpUsers_DeleterUserId` FOREIGN KEY (`DeleterUserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_AbpRoles_AbpUsers_LastModifierUserId` FOREIGN KEY (`LastModifierUserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE NO ACTION;

--
-- Constraints for table `AbpSettings`
--
ALTER TABLE `AbpSettings`
  ADD CONSTRAINT `FK_AbpSettings_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE NO ACTION;

--
-- Constraints for table `AbpTenants`
--
ALTER TABLE `AbpTenants`
  ADD CONSTRAINT `FK_AbpTenants_AbpEditions_EditionId` FOREIGN KEY (`EditionId`) REFERENCES `AbpEditions` (`Id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_AbpTenants_AbpUsers_CreatorUserId` FOREIGN KEY (`CreatorUserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_AbpTenants_AbpUsers_DeleterUserId` FOREIGN KEY (`DeleterUserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_AbpTenants_AbpUsers_LastModifierUserId` FOREIGN KEY (`LastModifierUserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE NO ACTION;

--
-- Constraints for table `AbpUserClaims`
--
ALTER TABLE `AbpUserClaims`
  ADD CONSTRAINT `FK_AbpUserClaims_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `AbpUserLogins`
--
ALTER TABLE `AbpUserLogins`
  ADD CONSTRAINT `FK_AbpUserLogins_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `AbpUserRoles`
--
ALTER TABLE `AbpUserRoles`
  ADD CONSTRAINT `FK_AbpUserRoles_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `AbpUsers`
--
ALTER TABLE `AbpUsers`
  ADD CONSTRAINT `FK_AbpUsers_AbpUsers_CreatorUserId` FOREIGN KEY (`CreatorUserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_AbpUsers_AbpUsers_DeleterUserId` FOREIGN KEY (`DeleterUserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_AbpUsers_AbpUsers_LastModifierUserId` FOREIGN KEY (`LastModifierUserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_AbpUsers_Countries_CountryId` FOREIGN KEY (`CountryId`) REFERENCES `Countries` (`Id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_AbpUsers_States_StateId` FOREIGN KEY (`StateId`) REFERENCES `States` (`Id`) ON DELETE NO ACTION;

--
-- Constraints for table `AbpUserTokens`
--
ALTER TABLE `AbpUserTokens`
  ADD CONSTRAINT `FK_AbpUserTokens_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `AdvertHomePageBanners`
--
ALTER TABLE `AdvertHomePageBanners`
  ADD CONSTRAINT `FK_AdvertHomePageBanners_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_AdvertHomePageBanners_AdvertPrices_AdvertPriceId` FOREIGN KEY (`AdvertPriceId`) REFERENCES `AdvertPrices` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_AdvertHomePageBanners_Statuses_StatusId` FOREIGN KEY (`StatusId`) REFERENCES `Statuses` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `AdvertTopBanners`
--
ALTER TABLE `AdvertTopBanners`
  ADD CONSTRAINT `FK_AdvertTopBanners_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_AdvertTopBanners_AdvertPrices_AdvertPriceId` FOREIGN KEY (`AdvertPriceId`) REFERENCES `AdvertPrices` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_AdvertTopBanners_Statuses_StatusId` FOREIGN KEY (`StatusId`) REFERENCES `Statuses` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `BankDetails`
--
ALTER TABLE `BankDetails`
  ADD CONSTRAINT `FK_BankDetails_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_BankDetails_Banks_BankId` FOREIGN KEY (`BankId`) REFERENCES `Banks` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `ContestComments`
--
ALTER TABLE `ContestComments`
  ADD CONSTRAINT `FK_ContestComments_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_ContestComments_Contests_ContestId` FOREIGN KEY (`ContestId`) REFERENCES `Contests` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `ContestEntries`
--
ALTER TABLE `ContestEntries`
  ADD CONSTRAINT `FK_ContestEntries_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_ContestEntries_Contests_ContestId` FOREIGN KEY (`ContestId`) REFERENCES `Contests` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_ContestEntries_Statuses_StatusId` FOREIGN KEY (`StatusId`) REFERENCES `Statuses` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `ContestEntryComments`
--
ALTER TABLE `ContestEntryComments`
  ADD CONSTRAINT `FK_ContestEntryComments_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_ContestEntryComments_ContestEntries_ContestEntryId` FOREIGN KEY (`ContestEntryId`) REFERENCES `ContestEntries` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `ContestFees`
--
ALTER TABLE `ContestFees`
  ADD CONSTRAINT `FK_ContestFees_ContestTypes_ContestTypeId` FOREIGN KEY (`ContestTypeId`) REFERENCES `ContestTypes` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `ContestPayments`
--
ALTER TABLE `ContestPayments`
  ADD CONSTRAINT `FK_ContestPayments_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_ContestPayments_Contests_ContestId` FOREIGN KEY (`ContestId`) REFERENCES `Contests` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `Contests`
--
ALTER TABLE `Contests`
  ADD CONSTRAINT `FK_Contests_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Contests_ContestCategories_ContestCategoryId` FOREIGN KEY (`ContestCategoryId`) REFERENCES `ContestCategories` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Contests_ContestEntryTypes_ContestEntryTypeId` FOREIGN KEY (`ContestEntryTypeId`) REFERENCES `ContestEntryTypes` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Contests_ContestFees_ContestFeeId` FOREIGN KEY (`ContestFeeId`) REFERENCES `ContestFees` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Contests_ContestTypes_ContestTypeId` FOREIGN KEY (`ContestTypeId`) REFERENCES `ContestTypes` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Contests_Periodics_PeriodicId` FOREIGN KEY (`PeriodicId`) REFERENCES `Periodics` (`Id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_Contests_Sponsors_SponsorId` FOREIGN KEY (`SponsorId`) REFERENCES `Sponsors` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Contests_Statuses_StatusId` FOREIGN KEY (`StatusId`) REFERENCES `Statuses` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Contests_VoteTypes_VoteTypeId` FOREIGN KEY (`VoteTypeId`) REFERENCES `VoteTypes` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `FeaturedContestEntries`
--
ALTER TABLE `FeaturedContestEntries`
  ADD CONSTRAINT `FK_FeaturedContestEntries_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FeaturedContestEntries_ContestEntries_ContestEntryId` FOREIGN KEY (`ContestEntryId`) REFERENCES `ContestEntries` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FeaturedContestEntries_Statuses_StatusId` FOREIGN KEY (`StatusId`) REFERENCES `Statuses` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `FeaturedContests`
--
ALTER TABLE `FeaturedContests`
  ADD CONSTRAINT `FK_FeaturedContests_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FeaturedContests_Contests_ContestId` FOREIGN KEY (`ContestId`) REFERENCES `Contests` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FeaturedContests_Statuses_StatusId` FOREIGN KEY (`StatusId`) REFERENCES `Statuses` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `Follows`
--
ALTER TABLE `Follows`
  ADD CONSTRAINT `FK_Follows_AbpUsers_FollowerUserId` FOREIGN KEY (`FollowerUserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Follows_AbpUsers_FollowingUserId` FOREIGN KEY (`FollowingUserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `HangfireJobParameter`
--
ALTER TABLE `HangfireJobParameter`
  ADD CONSTRAINT `FK_HangfireJobParameter_Job` FOREIGN KEY (`JobId`) REFERENCES `HangfireJob` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `HangfireJobState`
--
ALTER TABLE `HangfireJobState`
  ADD CONSTRAINT `FK_HangfireJobState_Job` FOREIGN KEY (`JobId`) REFERENCES `HangfireJob` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `HangfireState`
--
ALTER TABLE `HangfireState`
  ADD CONSTRAINT `FK_HangfireState_Job` FOREIGN KEY (`JobId`) REFERENCES `HangfireJob` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `PaymentHistories`
--
ALTER TABLE `PaymentHistories`
  ADD CONSTRAINT `FK_PaymentHistories_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_PaymentHistories_Statuses_StatusId` FOREIGN KEY (`StatusId`) REFERENCES `Statuses` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `Referrals`
--
ALTER TABLE `Referrals`
  ADD CONSTRAINT `FK_Referrals_AbpUsers_UnderUserId` FOREIGN KEY (`UnderUserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Referrals_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `Sponsors`
--
ALTER TABLE `Sponsors`
  ADD CONSTRAINT `FK_Sponsors_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `States`
--
ALTER TABLE `States`
  ADD CONSTRAINT `FK_States_Countries_CountryId` FOREIGN KEY (`CountryId`) REFERENCES `Countries` (`Id`) ON DELETE NO ACTION;

--
-- Constraints for table `UserVotePoints`
--
ALTER TABLE `UserVotePoints`
  ADD CONSTRAINT `FK_UserVotePoints_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `Votes`
--
ALTER TABLE `Votes`
  ADD CONSTRAINT `FK_Votes_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_Votes_ContestEntries_ContestEntryId` FOREIGN KEY (`ContestEntryId`) REFERENCES `ContestEntries` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Votes_Contests_ContestId` FOREIGN KEY (`ContestId`) REFERENCES `Contests` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `Wallets`
--
ALTER TABLE `Wallets`
  ADD CONSTRAINT `FK_Wallets_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE;

--
-- Constraints for table `Withdraws`
--
ALTER TABLE `Withdraws`
  ADD CONSTRAINT `FK_Withdraws_AbpUsers_UserId` FOREIGN KEY (`UserId`) REFERENCES `AbpUsers` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Withdraws_BankDetails_BankDetailsId` FOREIGN KEY (`BankDetailsId`) REFERENCES `BankDetails` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_Withdraws_Statuses_StatusId` FOREIGN KEY (`StatusId`) REFERENCES `Statuses` (`Id`) ON DELETE CASCADE;



-- Procedures and functions


delimiter $$$ 
-- CREATE FUNCTION "spGetContestEntriesToPromote" --------------
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetContestEntriesToPromote`(IN userId INT)
BEGIN
SELECT c.Title, ce.Id from ContestEntries ce join Contests c On ce.ContestId = c.Id
                         WHERE ce.UserId = @userId and ce.StatusId = 10;
END;
-- -------------------------------------------------------------

$$$ delimiter ;



delimiter $$$ 
-- CREATE FUNCTION "spGetRandomFeaturedContestEntries" ---------
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetRandomFeaturedContestEntries`(IN PageSize INT)
BEGIN
SELECT
                    fce.Id, 
                    fce.ContestEntryId,
                    fce.StartDate, 
                    fce.EndDate,
                    ce.IsFeatured  as ContestEntryIsFeatured, 
                    ce.Image as ContestEntryImage, 
                    ce.Video as ContestEntryVideo, 
                    ce.Poster as ContestEntryPoster,
                    et.Name as ContestEntryType,
                    ce.Permalink as ContestEntryPermalink,
                    u.NickName as UserNickName,
                    c.Title as ContestTitle
                   
                   from FeaturedContestEntries fce 
                   Join ContestEntries ce 
                   on fce.ContestEntryId = ce.Id
                   JOIn Contests c ON ce.ContestId = c.Id
                   Join AbpUsers u on fce.UserId = u.Id
                   Join ContestEntryTypes et on c.ContestEntryTypeId = et.Id
                   WHERE fce.StatusId = 18 
                   and fce.StartDate <= now()
                   and fce.EndDate >= now()
                   order by rand()
                   LIMIT PageSize;
END;
-- -------------------------------------------------------------

$$$ delimiter ;



delimiter $$$ 
-- CREATE FUNCTION "spGetRandomFeaturedContests" ---------------
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetRandomFeaturedContests`(IN `PageSize` INT(3))
    NO SQL
SELECT fc.Id, fc.ContestId, c.Title, c.IsFeatured, c.Permalink,
                 c.Picture, c.StartDate, c.EndDate from FeaturedContests fc Join Contests c on fc.ContestId = c.Id
                 WHERE c.IsFeatured = 1 and fc.StatusId = 18 and fc.StartDate <= NOW() and fc.EndDate >= NOW()
                 order by RAND() LIMIT PageSize;
-- -------------------------------------------------------------

$$$ delimiter ;



delimiter $$$ 
-- CREATE FUNCTION "spGetTopContestEntriesWithHighestVote" -----
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetTopContestEntriesWithHighestVote`(IN ContestId INT, IN PageSize INT)
BEGIN
 SELECT 
 u.NickName, u.Image,u.Permalink, 
 count(cv.Id) TotalVotes,
 c.Id ContestId, 
 ce.Id ContestEntryId
                 FROM Contests c 
                 JOIN ContestEntries ce ON ce.ContestId = c.Id 
                 JOIN AbpUsers u on ce.UserId = u.Id
                 JOIN Votes cv ON ce.Id = cv.ContestEntryId 
                 where c.Id = @ContestId GROUP BY u.NickName, 
                 u.Image,u.Permalink,c.Id, ce.Id 
                 ORDER BY TotalVotes DESC 
                 LIMIT PageSize;
          
END;
-- -------------------------------------------------------------

$$$ delimiter ;
