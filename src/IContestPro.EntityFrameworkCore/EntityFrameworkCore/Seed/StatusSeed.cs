﻿using System.Collections.Generic;
using System.Linq;
using Abp.MultiTenancy;
using IContestPro.IContestProEntities;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore.Seed
{
    public class StatusSeed
    {
        private readonly IContestProDbContext _context;
       

        public StatusSeed(IContestProDbContext context)
        {
            _context = context;
        }

        public static Dictionary<int, string> GetItems()
        {
            //TODO: Never change the position and keys of this status as they are used in the code
        var items = new Dictionary<int, string>();
            items.Add(1, "Pending");
            items.Add(2, "Closed");
            items.Add(3, "Cancelled");
            items.Add(4, "Processing");
            items.Add(5, "Paid");
            items.Add(6, "Suspended");
            items.Add(7, "PaymentConfirmed");
            items.Add(8, "PendingPaymentConfirmation");
            items.Add(9, "PendingPaymentRenewal");
            items.Add(10, "Active");
            items.Add(11, "PaymentFailed");
            items.Add(12, "PaymentPending");
            items.Add(13, "PendingClosure");
            items.Add(14, "Success");
            items.Add(15, "Failed");
            items.Add(16, "Disapproved");
            items.Add(17, "Paused");
            items.Add(18, "Live");
            items.Add(19, "ContestEntryClosed");
            items.Add(20, "Expired");
            items.Add(21, "New");
            items.Add(22, "Read");
            items.Add(23, "UnProcessed");
            items.Add(24, "Allocated");
            items.Add(25, "NotAllocated");
            items.Add(26, "Used");
            items.Add(27, "NotUsed");
            return items;
        }
        public void Create()
        {
            CreateItems();
        }
        private void CreateItems()
        {
            // Default tenant
            
            _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Statuses ON");
            foreach (var item in GetItems())
            {
                var defaultItem = _context.Statuses.IgnoreQueryFilters().FirstOrDefault(t => t.Name == item.Value);
                if (defaultItem == null)
                {
                    defaultItem = new Status{Id = item.Key, Name = item.Value, TenantId = MultiTenancyConsts.DefaultTenantId };
                    _context.Statuses.Add(defaultItem);
                }
            }
            _context.SaveChanges();
           _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Statuses OFF");

        }
    }
}