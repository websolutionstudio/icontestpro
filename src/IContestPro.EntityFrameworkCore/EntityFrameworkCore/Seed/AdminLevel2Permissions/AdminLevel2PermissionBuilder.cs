﻿using System.Collections.Generic;
using System.Linq;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.MultiTenancy;
using IContestPro.Authorization;
using IContestPro.Authorization.Roles;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore.Seed.AdminLevel2Permissions
{
    public class AdminLevel2PermissionBuilder
    {
        private readonly IContestProDbContext _context;
        private readonly int _tenantId;

        public AdminLevel2PermissionBuilder(IContestProDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            CreateRolesAndUsers();
        }
          private void CreateRolesAndUsers()
        {
            var itemRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.AdminLevel2);
            if (itemRole == null)
            {
                itemRole = _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.AdminLevel2, StaticRoleNames.Tenants.AdminLevel2) { IsStatic = false }).Entity;
                _context.SaveChanges();
            }

            //Grant permission to user role
            var userGrantedPermissions = _context.Permissions.IgnoreQueryFilters()
                .OfType<RolePermissionSetting>()
                .Where(p => p.TenantId == _tenantId && p.RoleId == itemRole.Id)
                .Select(p => p.Name)
                .ToList();
            var permissions = new List<string>
            {
          PermissionNames.Pages_Users,
        PermissionNames.Pages_Roles,
        PermissionNames.Pages_Admin,
        
        PermissionNames.Pages_Manage,
        PermissionNames.Pages_Manage_Create,
        PermissionNames.Pages_Manage_Update,
        PermissionNames.Pages_Manage_Delete,
        
        PermissionNames.Pages_Wallet,
        PermissionNames.Pages_Wallet_Deposit,
        PermissionNames.Pages_Wallet_Withdraw,
        PermissionNames.Pages_Wallet_Transfer,

        PermissionNames.Pages_WalletManagement,
        PermissionNames.Pages_WalletManagement_ProcessDeposit,
        PermissionNames.Pages_WalletManagement_ProcessWithdraw,
        PermissionNames.Pages_WalletManagement_ProcessTransfer,
       
        PermissionNames.Pages_Referral,
        PermissionNames.Pages_Referral_Update,
        
        PermissionNames.Pages_Contest,
        PermissionNames.Pages_Contest_Start,
        PermissionNames.Pages_Contest_Create,
        PermissionNames.Pages_Contest_Update,
        PermissionNames.Pages_Contest_Delete,
        PermissionNames.Pages_Contest_Choose_Winner,
        PermissionNames.Pages_Contest_Close_Winner,
        
        PermissionNames.Pages_Code_System,
        PermissionNames.Pages_Code_System_Create,
        PermissionNames.Pages_Code_System_Update,
        PermissionNames.Pages_Code_System_Delete,
        PermissionNames.Pages_Code_System_Validate,
        
        PermissionNames.Pages_Advert,
        PermissionNames.Pages_Advert_Create,
        PermissionNames.Pages_Advert_Update,
        PermissionNames.Pages_Advert_Delete,
        PermissionNames.Pages_Advert_Process,
        PermissionNames.Pages_Advert_Report,
        
        PermissionNames.Pages_Vote_Record,
        PermissionNames.Pages_Vote_Record_Create,
        PermissionNames.Pages_Vote_Record_Update,
        PermissionNames.Pages_Vote_Record_Delete,
        
        PermissionNames.Pages_Leader_Board,
        PermissionNames.Pages_Over_Leader_Board,
        PermissionNames.Pages_Contest_Leader_Board,
        PermissionNames.Pages_Voter_Leader_Board,
    
        PermissionNames.Pages_Report_UserList,
        PermissionNames.Pages_Report_Transactions,
        PermissionNames.Pages_Report_Transaction_Fee,
        PermissionNames.Pages_Report_Contest_Statistic,
        PermissionNames.Pages_Report_Vote_Statistic,
        PermissionNames.Pages_Report_Account_Position,
        
        PermissionNames.Pages_SupportTicket,
        PermissionNames.Pages_SupportTicket_Create,
        PermissionNames.Pages_SupportTicket_Update,
        PermissionNames.Pages_SupportTicket_Delete,
        
        PermissionNames.Pages_Dispute,
        PermissionNames.Pages_Dispute_Create,
        PermissionNames.Pages_Dispute_Update,
        PermissionNames.Pages_Dispute_Delete,
        
        PermissionNames.Pages_App_Settings,
        PermissionNames.Pages_App_Settings_Create,
        PermissionNames.Pages_App_Settings_Update,
        PermissionNames.Pages_App_Settings_Delete
            };
            var userPermissions = PermissionFinder
                .GetAllPermissions(new IContestProAuthorizationProvider())
                .Where(p => p.MultiTenancySides.HasFlag(MultiTenancySides.Tenant) &&
                            permissions.Contains(p.Name) && !userGrantedPermissions.Contains(p.Name))
                .ToList();

            if (userPermissions.Any())
            {
                _context.Permissions.AddRange(
                    userPermissions.Select(permission => new RolePermissionSetting
                    {
                        TenantId = _tenantId,
                        Name = permission.Name,
                        IsGranted = true,
                        RoleId = itemRole.Id
                    })
                );
                _context.SaveChanges();
            }
            
           
        }
    }
}