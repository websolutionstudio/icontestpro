﻿using System.Collections.Generic;
using System.Linq;
using Abp.MultiTenancy;
using IContestPro.IContestProEntities;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore.Seed
{
    public class ContestCategoryFeeSeed
    {
        private readonly IContestProDbContext _context;
        private const string ContestCategoryFashion = "Fashion";

        public ContestCategoryFeeSeed(IContestProDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateItems();
        }
        private void CreateItems()
        {
            // Default tenant
            var items = new List<string>
            {
                ContestCategoryFashion
            };
            items.ForEach(item =>
            {
                var defaultItem = _context.ContestCategories.IgnoreQueryFilters().FirstOrDefault(t => t.Name == item);
                if (defaultItem == null)
                {
                    defaultItem = new ContestCategory{Name = item, TenantId = MultiTenancyConsts.DefaultTenantId };
                    _context.ContestCategories.Add(defaultItem);
                }
            });
            _context.SaveChanges();
            
          
        }
    }
}