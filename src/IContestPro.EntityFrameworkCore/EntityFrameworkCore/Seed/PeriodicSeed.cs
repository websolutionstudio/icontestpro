﻿using System.Collections.Generic;
using System.Linq;
using Abp.MultiTenancy;
using IContestPro.IContestProEntities;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore.Seed
{
    public class PeriodicSeed
    {
        private readonly IContestProDbContext _context;
        public const string PeriodicDaily = "Daily";
        public const string PeriodicWeekly = "Weekly";
        public const string PeriodicTwoWeeks = "TwoWeeks";
        public const string PeriodicMonthly = "Monthly";
        public const string PeriodicTwoMonths = "TwoMonths";
        public const string PeriodicThreeMonths = "ThreeMonths";
        public const string PeriodicSixMonths = "SixMonths";
        public const string PeriodicYearly = "Yearly";

        public PeriodicSeed(IContestProDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateItems();
        }
        private void CreateItems()
        {
            // Default tenant
            var items = new List<string>
            {
                PeriodicDaily,
                PeriodicWeekly,
                PeriodicTwoWeeks,
                PeriodicMonthly,
                PeriodicTwoMonths,
                PeriodicThreeMonths,
                PeriodicSixMonths,
                PeriodicYearly
            };
            items.ForEach(item =>
            {
                var defaultItem = _context.Periodics.IgnoreQueryFilters().FirstOrDefault(t => t.Name == item);
                if (defaultItem == null)
                {
                    defaultItem = new Periodic{Name = item, TenantId = MultiTenancyConsts.DefaultTenantId };
                    _context.Periodics.Add(defaultItem);
                }
            });
            _context.SaveChanges();
          
        }
    }
}