﻿using System.Collections.Generic;
using System.Linq;
using Abp.MultiTenancy;
using IContestPro.IContestProEntities;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore.Seed
{
    public class AdvertPricesSeed
    {
        private readonly IContestProDbContext _context;

        public AdvertPricesSeed(IContestProDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateItems();
        }
        private void CreateItems()
        {
            // Default tenant
            var items = new List<AdvertPrice>
            {
                //Todo: Contest prices
                new AdvertPrice
                {
                    Name = "One Day",
                    Days = 1,
                    Amount = 3000,
                    Type = AdvertType.Contest,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Three Days",
                    Days = 3,
                    Amount = 4500,
                    Type = AdvertType.Contest,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Week",
                    Days = 7,
                    Amount = 5000,
                    Type = AdvertType.Contest,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Two Weeks",
                    Days = 14,
                    Amount = 7500,
                    Type = AdvertType.Contest,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Month",
                    Days = 30,
                    Amount = 13000,
                    Type = AdvertType.Contest,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Two Months",
                    Days = 60,
                    Amount = 25000,
                    Type = AdvertType.Contest,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Three Months",
                    Days = 90,
                    Amount = 35000,
                    Type = AdvertType.Contest,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Six Months",
                    Days = 180,
                    Amount = 60000,
                    Type = AdvertType.Contest,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Year",
                    Days = 365,
                    Amount = 110000,
                    Type = AdvertType.Contest,
                    TenantId = 1,
                },
                
                
                //Todo: Contest entry prices
                new AdvertPrice
                {
                    Name = "One Day",
                    Days = 1,
                    Amount = 1500,
                    Type = AdvertType.ContestEntry,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Three Days",
                    Days = 3,
                    Amount = 3000,
                    Type = AdvertType.ContestEntry,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Week",
                    Days = 7,
                    Amount = 5000,
                    Type = AdvertType.ContestEntry,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Two Weeks",
                    Days = 14,
                    Amount = 7000,
                    Type = AdvertType.ContestEntry,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Month",
                    Days = 30,
                    Amount = 10000,
                    Type = AdvertType.ContestEntry,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Two Months",
                    Days = 60,
                    Amount = 17000,
                    Type = AdvertType.ContestEntry,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Three Months",
                    Days = 90,
                    Amount = 30000,
                    Type = AdvertType.ContestEntry,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Six Months",
                    Days = 180,
                    Amount = 50000,
                    Type = AdvertType.ContestEntry,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Year",
                    Days = 365,
                    Amount = 100000,
                    Type = AdvertType.ContestEntry,
                    TenantId = 1,
                },
                
                
                //Todo: Home Page Banner prices
                new AdvertPrice
                {
                    Name = "One Day",
                    Days = 1,
                    Amount = 5000,
                    Type = AdvertType.HomePageBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Three Days",
                    Days = 3,
                    Amount = 8000,
                    Type = AdvertType.HomePageBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Week",
                    Days = 7,
                    Amount = 15000,
                    Type = AdvertType.HomePageBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Two Weeks",
                    Days = 14,
                    Amount = 25000,
                    Type = AdvertType.HomePageBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Month",
                    Days = 30,
                    Amount = 35000,
                    Type = AdvertType.HomePageBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Two Months",
                    Days = 60,
                    Amount = 65000,
                    Type = AdvertType.HomePageBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Three Months",
                    Days = 90,
                    Amount = 90000,
                    Type = AdvertType.HomePageBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Six Months",
                    Days = 180,
                    Amount = 170000,
                    Type = AdvertType.HomePageBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Year",
                    Days = 365,
                    Amount = 300000,
                    Type = AdvertType.HomePageBanner,
                    TenantId = 1,
                },
                
                //Todo: Top Banner prices
                new AdvertPrice
                {
                    Name = "One Day",
                    Days = 1,
                    Amount = 4500,
                    Type = AdvertType.TopBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Three Days",
                    Days = 3,
                    Amount = 7500,
                    Type = AdvertType.TopBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Week",
                    Days = 7,
                    Amount = 14500,
                    Type = AdvertType.TopBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Two Weeks",
                    Days = 14,
                    Amount = 22000,
                    Type = AdvertType.TopBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Month",
                    Days = 30,
                    Amount = 30000,
                    Type = AdvertType.TopBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Two Months",
                    Days = 60,
                    Amount = 60000,
                    Type = AdvertType.TopBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Three Months",
                    Days = 90,
                    Amount = 85000,
                    Type = AdvertType.TopBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Six Months",
                    Days = 180,
                    Amount = 160000,
                    Type = AdvertType.TopBanner,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Year",
                    Days = 365,
                    Amount = 250000,
                    Type = AdvertType.TopBanner,
                    TenantId = 1,
                },
                
                
                //Todo: AdvertBox One
                new AdvertPrice
                {
                    Name = "One Day",
                    Days = 1,
                    Amount = 3000,
                    Type = AdvertType.AdvertBoxOne,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Three Days",
                    Days = 3,
                    Amount = 5000,
                    Type = AdvertType.AdvertBoxOne,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Week",
                    Days = 7,
                    Amount = 10500,
                    Type = AdvertType.AdvertBoxOne,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Two Weeks",
                    Days = 14,
                    Amount = 20000,
                    Type = AdvertType.AdvertBoxOne,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Month",
                    Days = 30,
                    Amount = 35000,
                    Type = AdvertType.AdvertBoxOne,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Two Months",
                    Days = 60,
                    Amount = 45000,
                    Type = AdvertType.AdvertBoxOne,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Three Months",
                    Days = 90,
                    Amount = 60000,
                    Type = AdvertType.AdvertBoxOne,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Six Months",
                    Days = 180,
                    Amount = 120000,
                    Type = AdvertType.AdvertBoxOne,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Year",
                    Days = 365,
                    Amount = 200000,
                    Type = AdvertType.AdvertBoxOne,
                    TenantId = 1,
                },
                
                 //Todo: AdvertBox two
                new AdvertPrice
                {
                    Name = "One Day",
                    Days = 1,
                    Amount = 3000,
                    Type = AdvertType.AdvertBoxTwo,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Three Days",
                    Days = 3,
                    Amount = 5000,
                    Type = AdvertType.AdvertBoxTwo,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Week",
                    Days = 7,
                    Amount = 10500,
                    Type = AdvertType.AdvertBoxTwo,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Two Weeks",
                    Days = 14,
                    Amount = 20000,
                    Type = AdvertType.AdvertBoxTwo,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Month",
                    Days = 30,
                    Amount = 35000,
                    Type = AdvertType.AdvertBoxTwo,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Two Months",
                    Days = 60,
                    Amount = 45000,
                    Type = AdvertType.AdvertBoxTwo,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Three Months",
                    Days = 90,
                    Amount = 60000,
                    Type = AdvertType.AdvertBoxTwo,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "Six Months",
                    Days = 180,
                    Amount = 120000,
                    Type = AdvertType.AdvertBoxTwo,
                    TenantId = 1,
                },
                 new AdvertPrice
                {
                    Name = "One Year",
                    Days = 365,
                    Amount = 200000,
                    Type = AdvertType.AdvertBoxTwo,
                    TenantId = 1,
                },
                
            };
           
            items.ForEach(item =>
            {
                var defaultItem = _context.AdvertPrices.IgnoreQueryFilters().FirstOrDefault(t => t.Name == item.Name && t.Type == item.Type);
                if (defaultItem == null)
                {
                    _context.AdvertPrices.Add(item);
                }
            });
            _context.SaveChanges();
        }
    }

}