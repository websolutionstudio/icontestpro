﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Abp.MultiTenancy;
using IContestPro.Editions;
using IContestPro.MultiTenancy;

namespace IContestPro.EntityFrameworkCore.Seed.Tenants
{
    public class DefaultTenantBuilder
    {
        private readonly IContestProDbContext _context;

        public DefaultTenantBuilder(IContestProDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateDefaultTenant();
        }

        private void CreateDefaultTenant()
        {
            // Default tenant

                    var defaultTenant = _context.Tenants.IgnoreQueryFilters().FirstOrDefault(t => t.TenancyName == AbpTenantBase.DefaultTenantName);
                    if (defaultTenant == null)
                    {
                      _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT AbpTenants ON");
                        defaultTenant = new Tenant( AbpTenantBase.DefaultTenantName, AbpTenantBase.DefaultTenantName);
                        defaultTenant.Id = 1;
                        var defaultEdition = _context.Editions.IgnoreQueryFilters().FirstOrDefault(e => e.Name == EditionManager.DefaultEditionName);
                        if (defaultEdition != null)
                        {
                            defaultTenant.EditionId = defaultEdition.Id;
                        }

                        _context.Tenants.Add(defaultTenant);
                        _context.SaveChanges();
                       _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT AbpTenants OFF");
                    }
                   
            }
           
        }
    }

