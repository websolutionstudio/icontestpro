﻿using System;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.Uow;
using Abp.MultiTenancy;
using IContestPro.EntityFrameworkCore.Seed.AdminLevel2Permissions;
using IContestPro.EntityFrameworkCore.Seed.Host;
using IContestPro.EntityFrameworkCore.Seed.SponsorPermissions;
using IContestPro.EntityFrameworkCore.Seed.Tenants;
using IContestPro.EntityFrameworkCore.Seed.UserPermissions;

namespace IContestPro.EntityFrameworkCore.Seed
{
    public static class SeedHelper
    {
        public static void SeedHostDb(IIocResolver iocResolver)
        {
            WithDbContext<IContestProDbContext>(iocResolver, SeedHostDb);
        }

        public static void SeedHostDb(IContestProDbContext context)
        {
            context.SuppressAutoSetTenantId = true;

            // Host seed
            new InitialHostDbBuilder(context).Create();

            // Default tenant seed (in host database).
            new DefaultTenantBuilder(context).Create();
            new TenantRoleAndUserBuilder(context, 1).Create();

            new AdminLevel2PermissionBuilder(context, 1).Create();
            new SponsorPermissionBuilder(context, 1).Create();
            new UserPermissionBuilder(context, 1).Create();
            
            new BankSeed(context).Create();
            new CountrySeed(context).Create();
            new StateSeed(context).Create();
            
            new ContestEntryTypeSeed(context).Create();
            new ContestTypeAndContestFeeSeed(context).Create();
            new ContestCategoryFeeSeed(context).Create();
            new StatusSeed(context).Create();
            new VoteTypesVoteRewardSeed(context).Create();
            new PeriodicSeed(context).Create();
            new PaymentTypeSeed(context).Create();
            new AdvertPricesSeed(context).Create();
        }

        private static void WithDbContext<TDbContext>(IIocResolver iocResolver, Action<TDbContext> contextAction)
            where TDbContext : DbContext
        {
            using (var uowManager = iocResolver.ResolveAsDisposable<IUnitOfWorkManager>())
            {
                using (var uow = uowManager.Object.Begin(TransactionScopeOption.Suppress))
                {
                    var context = uowManager.Object.Current.GetDbContext<TDbContext>(MultiTenancySides.Host);

                    contextAction(context);

                    uow.Complete();
                }
            }
        }
    }
}
