﻿using System.Collections.Generic;
using System.Linq;
using Abp.MultiTenancy;
using IContestPro.IContestProEntities;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore.Seed
{
    public class VoteTypesVoteRewardSeed
    {
        private static string VoteTypePaid => "Paid";
        private static string VoteTypeVotePoint => "VotePoint";
        private static string VoteTypeUnique => "Unique";
        private static string VoteTypeDaily => "Daily";
        private static string VoteTypeHourly => "Hourly";
        private static string VoteTypeOpen => "Open";
       
        private readonly IContestProDbContext _context;

        public VoteTypesVoteRewardSeed(IContestProDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateItems();
        }
        private void CreateItems()
        {
            //Vote types
            _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT VoteTypes ON");
                var defaultItem = _context.VoteTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == VoteTypePaid);
                if (defaultItem == null)
                {
                    defaultItem = new VoteType{Id = 1, Name = VoteTypePaid, RewardPoint = 10, TenantId = MultiTenancyConsts.DefaultTenantId, Amount = 500, MinAmount = 100, MaxAmount = 500};
                    _context.VoteTypes.Add(defaultItem);
                }
                var defaultItem2 = _context.VoteTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == VoteTypeVotePoint);
                if (defaultItem2 == null)
                {
                    defaultItem2 = new VoteType{Id = 2, RewardPoint = 1, Name = VoteTypeVotePoint, TenantId = MultiTenancyConsts.DefaultTenantId, VotePoint = 10};
                    _context.VoteTypes.Add(defaultItem2);
                } 
                var defaultItem3 = _context.VoteTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == VoteTypeUnique);
                if (defaultItem3 == null)
                {
                    defaultItem3 = new VoteType{Id = 3, RewardPoint = 5, Name = VoteTypeUnique, TenantId = MultiTenancyConsts.DefaultTenantId };
                    _context.VoteTypes.Add(defaultItem3);
                }
                var defaultItem4 = _context.VoteTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == VoteTypeDaily);
                if (defaultItem4 == null)
                {
                    defaultItem4 = new VoteType{Id = 4, RewardPoint = 2, Name = VoteTypeDaily, TenantId = MultiTenancyConsts.DefaultTenantId };
                    _context.VoteTypes.Add(defaultItem4);
                }
                var defaultItem5 = _context.VoteTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == VoteTypeHourly);
                if (defaultItem5 == null)
                {
                    defaultItem5 = new VoteType{Id = 5, RewardPoint = 1, Name = VoteTypeHourly, TenantId = MultiTenancyConsts.DefaultTenantId };
                    _context.VoteTypes.Add(defaultItem5);
                }
                var defaultItem6 = _context.VoteTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == VoteTypeOpen);
                if (defaultItem6 == null)
                {
                    defaultItem6 = new VoteType{Id = 6, RewardPoint = 0, Name = VoteTypeOpen, TenantId = MultiTenancyConsts.DefaultTenantId };
                    _context.VoteTypes.Add(defaultItem6);
                }
            
            _context.SaveChanges();
            _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT VoteTypes OFF");

            
        
            
        }
    }
}