﻿using System.Collections.Generic;
using System.Linq;
using Abp.MultiTenancy;
using IContestPro.IContestProEntities;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore.Seed
{
    public class ContestEntryTypeSeed
    {
        private readonly IContestProDbContext _context;

        public ContestEntryTypeSeed(IContestProDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateItems();
        }
        private void CreateItems()
        {
            // Default tenant
            var items = new List<string>
            {
                "Image",
                "Video",
                "Article"
            };
            items.ForEach(item =>
            {
                var defaultItem = _context.ContestEntryTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == item);
                if (defaultItem == null)
                {
                    defaultItem = new ContestEntryType{Name = item, TenantId = MultiTenancyConsts.DefaultTenantId };
                    _context.ContestEntryTypes.Add(defaultItem);
                }
            });
            _context.SaveChanges();
        }
    }
}