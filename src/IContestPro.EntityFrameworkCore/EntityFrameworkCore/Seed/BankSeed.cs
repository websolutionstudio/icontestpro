﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.MultiTenancy;
using IContestPro.Editions;
using IContestPro.IContestProEntities;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore.Seed
{
    public class BankSeed
    {
        private readonly IContestProDbContext _context;

       
        public BankSeed(IContestProDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateItems();
        }
        private void CreateItems()
        {
            var banks = new Dictionary<int, string>();
            banks.Add(1, "Access Bank Plc");
            banks.Add(2, "Citibank Nigeria Limited");
            banks.Add(3, "Diamond Bank Plc");
            banks.Add(4, "Ecobank Nigeria Plc");
            banks.Add(5, "Fidelity Bank Plc");
            banks.Add(6, "First Bank of Nigeria Limited");
            banks.Add(7, "First City Monument Bank Plc");
            banks.Add(8, "Guaranty Trust Bank Plc");
            banks.Add(9, "Heritage Bank Limited");
            banks.Add(10, "Keystone Bank Limited");
            banks.Add(12, "Providus Bank Limited");
            banks.Add(13, "Stanbic IBTC Bank Plc");
            banks.Add(14, "Standard Chartered");
            banks.Add(15, "Sterling Bank Plc");
            banks.Add(16, "Union Bank of Nigeria Plc");
            banks.Add(17, "United Bank for Africa Plc");
            banks.Add(18, "Unity Bank Plc");
            banks.Add(19, "Wema Bank Plc");
            banks.Add(20, "Zenith Bank Plc");
            
            var bankCodes = new Dictionary<int, string>();
            bankCodes.Add(1, "044");
            bankCodes.Add(2, "023");
            bankCodes.Add(3, "063");
            bankCodes.Add(4, "050");
            bankCodes.Add(5, "070");
            bankCodes.Add(6, "011");
            bankCodes.Add(7, "214");
            bankCodes.Add(8, "058");
            bankCodes.Add(9, "030");
            bankCodes.Add(10, "082");
            bankCodes.Add(12, "101");
            bankCodes.Add(13, "221");
            bankCodes.Add(14, "068");
            bankCodes.Add(15, "232");
            bankCodes.Add(16, "032");
            bankCodes.Add(17, "033");
            bankCodes.Add(18, "215");
            bankCodes.Add(19, "035");
            bankCodes.Add(20, "057");
            /*
            foreach (var bank in banks)
            {
                var defaultBank = _context.Banks.IgnoreQueryFilters().FirstOrDefault(t => t.Name == bank.Value);
                if (defaultBank == null)
                {
                    var bankCode = bankCodes.First(item => item.Key == bank.Key);
                    defaultBank = new Bank{Id = bank.Key, Name = bank.Value, Code = bankCode.Value,Active = true, TenantId = MultiTenancyConsts.DefaultTenantId};
                    _context.Banks.Add(defaultBank);
                }
            }
            _context.SaveChanges();
*/

           
                    _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Banks ON");
                    foreach (var bank in banks)
                    {
                        var defaultBank = _context.Banks.IgnoreQueryFilters().FirstOrDefault(t => t.Name == bank.Value);
                        if (defaultBank == null)
                        {
                            var bankCode = bankCodes.First(item => item.Key == bank.Key);
                            defaultBank = new Bank{Id = bank.Key, Name = bank.Value, Code = bankCode.Value,Active = true, TenantId = MultiTenancyConsts.DefaultTenantId};
                            _context.Banks.Add(defaultBank);
                        }
                    }
                    _context.SaveChanges();
                   _context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT Banks OFF");
                 
            }
            
            
            
        }
    
}