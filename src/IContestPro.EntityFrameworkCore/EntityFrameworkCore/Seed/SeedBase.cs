﻿namespace IContestPro.EntityFrameworkCore.Seed
{
    public class SeedBase
    {
        public const string DefaultTenantAdminEmail = "admin@defaulttenant.com";
        public const string DefaultTenantAdminUserName = "admin";
        public const string HostAdminEmail = "admin@icontestpro.com";
        public const string HostAdminUserName = "admin";
    }
}