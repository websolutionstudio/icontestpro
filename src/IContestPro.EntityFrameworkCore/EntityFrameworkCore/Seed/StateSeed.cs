﻿using System.Collections.Generic;
using System.Linq;
using Abp.MultiTenancy;
using IContestPro.IContestProEntities;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore.Seed
{
    public class StateSeed
    {
        private readonly IContestProDbContext _context;

        public StateSeed(IContestProDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateItems();
        }
        private void CreateItems()
        {
            // Default tenant
            var items = new List<string>
            {
                "Abia",
                "Adamawa",
                "Akwa Ibom",
                "Anambra",
                "Bauchi",
                "Bayelsa",
                "Benue",
                "Borno",
                "Cross River",
                "Delta",
                "Ebonyi",
                "Edo",
                "Ekiti",
                "Enugu",
                "Gombe",
                "Imo",
                "Jigawa",
                "Kaduna",
                "Kano",
                "Katsina",
                "Kebbi",
                "Kogi",
                "Kwara",
                "Lagos",
                "Nasarawa",
                "Niger",
                "Ogun",
                "Ondo",
                "Osun",
                "Oyo",
                "Plateau",
                "Rivers",
                "Sokoto",
                "Taraba",
                "Yobe",
                "Zamfara",
                "FCT"
            };
            items.ForEach(item =>
            {
                var defaultItem = _context.States.IgnoreQueryFilters().FirstOrDefault(t => t.Name == item);
                if (defaultItem == null)
                {

                    defaultItem = new State{Name = item, CountryId = "NG", TenantId = MultiTenancyConsts.DefaultTenantId };
                    _context.States.Add(defaultItem);
                }
            });
            _context.SaveChanges();
        }
    }
}