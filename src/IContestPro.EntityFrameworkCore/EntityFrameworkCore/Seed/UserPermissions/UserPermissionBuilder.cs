﻿using System.Collections.Generic;
using System.Linq;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.MultiTenancy;
using IContestPro.Authorization;
using IContestPro.Authorization.Roles;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore.Seed.UserPermissions
{
    public class UserPermissionBuilder
    {
        private readonly IContestProDbContext _context;
        private readonly int _tenantId;

        public UserPermissionBuilder(IContestProDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            CreateRolesAndUsers();
        }
          private void CreateRolesAndUsers()
        {
            var userRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Users);
            if (userRole == null)
            {
                userRole = _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Users, StaticRoleNames.Tenants.Users) { IsStatic = false }).Entity;
                _context.SaveChanges();
            }

            //Grant permission to user role
            var userGrantedPermissions = _context.Permissions.IgnoreQueryFilters()
                .OfType<RolePermissionSetting>()
                .Where(p => p.TenantId == _tenantId && p.RoleId == userRole.Id)
                .Select(p => p.Name)
                .ToList();
              var permissions = new List<string>
            {
        PermissionNames.Pages_Users,
        PermissionNames.Pages_Manage,
        PermissionNames.Pages_Manage_Create,
        PermissionNames.Pages_Manage_Update,
        PermissionNames.Pages_Manage_Delete,
        
        PermissionNames.Pages_Wallet,
        PermissionNames.Pages_Wallet_Deposit,
        PermissionNames.Pages_Wallet_Withdraw,
        PermissionNames.Pages_Wallet_Transfer,

        PermissionNames.Pages_Referral,
        
        PermissionNames.Pages_Contest,
        PermissionNames.Pages_Contest_Create,
        PermissionNames.Pages_Contest_Update,
        
        PermissionNames.Pages_Advert,
        PermissionNames.Pages_Advert_Create,
        PermissionNames.Pages_Advert_Update,
        
        PermissionNames.Pages_Leader_Board,
        PermissionNames.Pages_Over_Leader_Board,
        PermissionNames.Pages_Contest_Leader_Board,
        PermissionNames.Pages_Voter_Leader_Board,
    
        PermissionNames.Pages_SupportTicket,
        PermissionNames.Pages_SupportTicket_Create,
        PermissionNames.Pages_SupportTicket_Update,
        PermissionNames.Pages_SupportTicket_Delete,
        
        PermissionNames.Pages_Vote_Record,
        PermissionNames.Pages_Vote_Record_Create,
        PermissionNames.Pages_Vote_Record_Update,
        PermissionNames.Pages_Vote_Record_Delete
                
            };
            var userPermissions = PermissionFinder
                .GetAllPermissions(new IContestProAuthorizationProvider())
                .Where(p => p.MultiTenancySides.HasFlag(MultiTenancySides.Tenant) &&
                            permissions.Contains(p.Name) && !userGrantedPermissions.Contains(p.Name))
                .ToList();

            if (userPermissions.Any())
            {
                _context.Permissions.AddRange(
                    userPermissions.Select(permission => new RolePermissionSetting
                    {
                        TenantId = _tenantId,
                        Name = permission.Name,
                        IsGranted = true,
                        RoleId = userRole.Id
                    })
                );
                _context.SaveChanges();
            }
            
           
        }
    }
}