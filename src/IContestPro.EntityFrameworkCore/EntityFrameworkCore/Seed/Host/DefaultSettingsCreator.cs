﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Abp.Configuration;
using Abp.Localization;
using Abp.Net.Mail;

namespace IContestPro.EntityFrameworkCore.Seed.Host
{
    public class DefaultSettingsCreator
    {
        private readonly IContestProDbContext _context;

        public DefaultSettingsCreator(IContestProDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            // Emailing
            AddSettingIfNotExists(EmailSettingNames.DefaultFromAddress, "admin@icontestpro.com");
            AddSettingIfNotExists(EmailSettingNames.DefaultFromDisplayName, "icontestpro.com mailer");
            AddSettingIfNotExists(AppConsts.SettingsWithdrawalRequestFeePercentage, "1", 1);
            AddSettingIfNotExists(AppConsts.SettingsWithdrawalMinimumAmount, "5000", 1);
            AddSettingIfNotExists(AppConsts.SettingsTransferFeePercentage, "0.1", 1);
            AddSettingIfNotExists(AppConsts.SettingsReferralVotePointAward, "1000", 1);
            AddSettingIfNotExists(AppConsts.SettingsReferrerPercentageIfReferredUserBecomesSponsor, "10", 1);
            AddSettingIfNotExists(AppConsts.SettingsShouldPercentageBePaidIfReferredSponsorCreatesFirstContest, "true", 1);
            AddSettingIfNotExists(AppConsts.SettingsCodeSize, "6", 1);
            AddSettingIfNotExists(AppConsts.SettingsBatchNoSize, "6", 1);
            AddSettingIfNotExists(AppConsts.SettingsMaximumBatchSize, "2000", 1);
            AddSettingIfNotExists(AppConsts.SettingsMaxCodesAssignablePerTime, "2000", 1);
            AddSettingIfNotExists(AppConsts.SettingsConfirmEmailAfterAccountCreation, "true", 1);
            AddSettingIfNotExists(AppConsts.SettingsPhoneNumberAfterAccountCreation, "false", 1);

            // Languages
            AddSettingIfNotExists(LocalizationSettingNames.DefaultLanguage, "en");
        }

        private void AddSettingIfNotExists(string name, string value, int? tenantId = null)
        {
            if (_context.Settings.IgnoreQueryFilters().Any(s => s.Name == name && s.TenantId == tenantId && s.UserId == null))
            {
                return;
            }

            _context.Settings.Add(new Setting(tenantId, null, name, value));
            _context.SaveChanges();
        }
    }
}
