﻿using System.Collections.Generic;
using System.Linq;
using Abp.MultiTenancy;
using IContestPro.IContestProEntities;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore.Seed
{
    public class ContestTypeAndContestFeeSeed
    {
        private readonly IContestProDbContext _context;
        private static string ContestTypePaid => "Paid";
        private static string ContestTypeCoded => "Coded";
        private static string ContestTypeFree => "Free";

        public ContestTypeAndContestFeeSeed(IContestProDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateItems();
        }
        private void CreateItems()
        {
            // Default tenant
            var items = new List<string>
            {
                ContestTypePaid,
                ContestTypeCoded,
                ContestTypeFree
            };
            items.ForEach(item =>
            {
                var defaultItem = _context.ContestTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == item);
                if (defaultItem == null)
                {
                    defaultItem = new ContestType{Name = item, TenantId = MultiTenancyConsts.DefaultTenantId };
                    _context.ContestTypes.Add(defaultItem);
                }
            });
            _context.SaveChanges();
            
            
            var itemChild= _context.ContestTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == ContestTypePaid);
            if (itemChild != null)
            {
                var voterRewardItem = _context.ContestFees.IgnoreQueryFilters().FirstOrDefault(t => t.ContestTypeId == itemChild.Id);
                if (voterRewardItem == null)
                {
                    voterRewardItem = new ContestFee{ContestTypeId = itemChild.Id, CodeBatch =  0, Amount = 100000, PercentagePerMonth = 50,TenantId = MultiTenancyConsts.DefaultTenantId};
                    _context.ContestFees.Add(voterRewardItem);
                }
                   
            } 
            
            var itemChild2 = _context.ContestTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == ContestTypeCoded);
            if (itemChild2 != null)
            {
                var voterRewardItem = _context.ContestFees.IgnoreQueryFilters().FirstOrDefault(t => t.ContestTypeId == itemChild2.Id);
                if (voterRewardItem == null)
                {
                    voterRewardItem = new ContestFee{ContestTypeId = itemChild2.Id, CodeBatch =  2500, Amount = 150000, TenantId = MultiTenancyConsts.DefaultTenantId};
                    _context.ContestFees.Add(voterRewardItem);
                }
                   
            }
            var itemChild3 = _context.ContestTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == ContestTypeFree);
            if (itemChild3 != null)
            {
                var voterRewardItem = _context.ContestFees.IgnoreQueryFilters().FirstOrDefault(t => t.ContestTypeId == itemChild3.Id);
                if (voterRewardItem == null)
                {
                    voterRewardItem = new ContestFee{ContestTypeId = itemChild3.Id, CodeBatch =  0, Amount = 50000,PercentagePerMonth = 50, TenantId = MultiTenancyConsts.DefaultTenantId};
                    _context.ContestFees.Add(voterRewardItem);
                }
                   
            }
            _context.SaveChanges();
            /*
Free Contest: N50,000
Paid Contest: N100,000
Coded Contest: N150,000 (per batch of 2500 codes)*/
        }
    }
}