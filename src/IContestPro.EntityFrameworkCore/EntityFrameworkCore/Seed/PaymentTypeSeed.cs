﻿using System.Collections.Generic;
using System.Linq;
using Abp.MultiTenancy;
using IContestPro.IContestProEntities;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore.Seed
{
    public class PaymentTypeSeed
    {
        private readonly IContestProDbContext _context;
        private const string ContestPayment = "ContestPayment";
        private const string EntryPayment = "EntryPayment";

        public PaymentTypeSeed(IContestProDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateItems();
        }
        private void CreateItems()
        {
              var defaultItem = _context.PaymentTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == ContestPayment);
                if (defaultItem == null)
                {
                    defaultItem = new PaymentType{Name = ContestPayment,
                        Description = "Payment required to complete contest creation." ,TenantId = MultiTenancyConsts.DefaultTenantId };
                    _context.PaymentTypes.Add(defaultItem);
                }
             var defaultItem2 = _context.PaymentTypes.IgnoreQueryFilters().FirstOrDefault(t => t.Name == EntryPayment);
                if (defaultItem2 == null)
                {
                    defaultItem = new PaymentType{Name = EntryPayment,
                        Description = "Payment required to complete contest entry." ,TenantId = MultiTenancyConsts.DefaultTenantId };
                    _context.PaymentTypes.Add(defaultItem);
                }
            
            _context.SaveChanges();
          
        }
    }
}