﻿using Abp.Dependency;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using IContestPro.Configuration;
using IContestPro.Web;

namespace IContestPro.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class IContestProDbContextFactory : IDesignTimeDbContextFactory<IContestProDbContext>, ITransientDependency
    {
        public IContestProDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<IContestProDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            IContestProDbContextConfigurer.Configure(builder, configuration.GetConnectionString(IContestProConsts.ConnectionStringName));

            return new IContestProDbContext(builder.Options);
        }
    }
}
