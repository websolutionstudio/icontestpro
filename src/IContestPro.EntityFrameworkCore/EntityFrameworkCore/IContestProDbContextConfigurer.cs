using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace IContestPro.EntityFrameworkCore
{
    public static class IContestProDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<IContestProDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<IContestProDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
        
        /*public static void Configure(DbContextOptionsBuilder<IContestProDbContext> builder, string connectionString)
        {
            builder.UseNpgsql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<IContestProDbContext> builder, DbConnection connection)
        {
            builder.UseNpgsql(connection);
        }*/
        
        /*public static void Configure(DbContextOptionsBuilder<IContestProDbContext> builder, string connectionString)
        {
            builder.UseMySql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<IContestProDbContext> builder, DbConnection connection)
        {
            builder.UseMySql(connection);
        }*/
        
    }
}
