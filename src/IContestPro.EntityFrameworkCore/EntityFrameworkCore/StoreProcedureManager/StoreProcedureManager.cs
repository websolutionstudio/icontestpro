﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SqlDataReaderMapper;

namespace IContestPro.EntityFrameworkCore.StoreProcedureManager
{
    public static class StoreProcedureManager
    {
        public static DbCommand LoadStoredProcedure(this DbContext context, string storedProcName)
        {
            var cmd = context.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = storedProcName;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            return cmd;
        }
        public static DbCommand WithSqlParam(
            this DbCommand cmd, string paramName, object paramValue)
        {
            if (string.IsNullOrEmpty(cmd.CommandText))
                throw new InvalidOperationException(
                    "Call LoadStoredProcedure before using this method");
            var param = cmd.CreateParameter();
            param.ParameterName = paramName;
            param.Value = paramValue;
            cmd.Parameters.Add(param);
            return cmd;
        }
        private static List<T> MapToList<T>(this DbDataReader dr) where T : class , new()
        {
            var objList = new List<T>();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    var obj = new SqlDataReaderMapper<T>(dr).Build();
                    objList.Add(obj);
                }
            }
            return objList;
        }
        public static async Task<List<T>> ExecuteStoredProcedureListAsync<T>(this DbCommand command) where T : class , new()
        {
            using (command)
            {
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                    command.Connection.Open();
                try
                {
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        return reader.MapToList<T>();
                    }
                }
                catch(Exception e)
                {
                    throw (e);
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }
        public static async Task<T> ExecuteStoredProcedureFirstOrDefaultAsync<T>(this DbCommand command) where T : class , new()
        {
            using (command)
            {
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                    command.Connection.Open();
                try
                {
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        var items = reader.MapToList<T>();
                        if (items != null && items.Any())
                        {
                            return items.First();
                        }
                        return  default(T);
                    }
                }
                catch(Exception e)
                {
                    throw (e);
                }
                finally
                {
                    command.Connection.Close();
                }
            }

        }
    }
}