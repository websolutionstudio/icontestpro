﻿using Abp.Localization;
using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using IContestPro.Authorization.Roles;
using IContestPro.Authorization.Users;
using IContestPro.IContestProEntities;
using IContestPro.MultiTenancy;

namespace IContestPro.EntityFrameworkCore
{
    public class IContestProDbContext : AbpZeroDbContext<Tenant, Role, User, IContestProDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public DbSet<Bank> Banks { get; set; }
        public DbSet<BankDetails> BankDetails { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Sponsor> Sponsors { get; set; }
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<Contest> Contests { get; set; }
        public DbSet<ContestCategory> ContestCategories { get; set; }
        public DbSet<ContestComment> ContestComments { get; set; }
        public DbSet<ContestEntry> ContestEntries { get; set; }
        public DbSet<ContestEntryComment> ContestEntryComments { get; set; }
        public DbSet<ContestEntryType> ContestEntryTypes { get; set; }
        public DbSet<ContestPayment> ContestPayments { get; set; }
        public DbSet<ContestType> ContestTypes { get; set; }
        public DbSet<Follow> Follows { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Vote> Votes { get; set; }
        public DbSet<VoteType> VoteTypes { get; set; }
        public DbSet<UserVotePoint> UserVotePoints { get; set; }
        public DbSet<ContestFee> ContestFees { get; set; }
        public DbSet<Periodic> Periodics { get; set; }
        public DbSet<PaymentHistory> PaymentHistories { get; set; }
        public DbSet<PaymentType> PaymentTypes { get; set; }
        public DbSet<ContestEntryView> ContestEntryViews { get; set; }
        public DbSet<ContestEntryShare> ContestEntryShares { get; set; }
        public DbSet<Withdraw> Withdraws { get; set; }
        public DbSet<Referral> Referrals { get; set; }
        public DbSet<AdvertPrice> AdvertPrices { get; set; }
        public DbSet<AdvertHomePageBanner> AdvertHomePageBanners { get; set; }
        public DbSet<AdvertTopBanner> AdvertTopBanners { get; set; }
        public DbSet<FeaturedContest> FeaturedContests { get; set; }
        public DbSet<FeaturedContestEntry> FeaturedContestEntries { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<MessageResponse> MessageResponses { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<CodeSystem> CodeSystems { get; set; }
        public DbSet<ContestView> ContestViews { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<BlogResponse> BlogResponses { get; set; }
        public DbSet<AdvertBox1> AdvertBox1 { get; set; }
        public DbSet<AdvertBox2> AdvertBox2 { get; set; }
        public DbSet<PastWinner> PastWinners { get; set; }
        public DbSet<SponsorFollow> SponsorFollow { get; set; }
        
        public IContestProDbContext(DbContextOptions<IContestProDbContext> options)
            : base(options)
        {
            //DbConfiguration.SetConfiguration(new MySql.Data.Entity.MySqlEFConfiguration());
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            
            builder.Entity<ApplicationLanguageText>()
                .Property(p => p.Value).HasMaxLength(100); // any integer that is smaller than 10485760
            /*builder.Entity<Contest>().Property(item => item.Amount).HasColumnType("decimal(12,10)");
            builder.Entity<ContestPayment>().Property(item => item.Amount).HasColumnType("decimal(12,10)");
            builder.Entity<Wallet>().Property(item => item.Balance).HasColumnType("decimal(12,10)");
            builder.Entity<Wallet>().Property(item => item.LastCreditedAmount).HasColumnType("decimal(12,10)");
            builder.Entity<Wallet>().Property(item => item.LastDebitedAmount).HasColumnType("decimal(12,10)");
            */
            //</ostgres customization>
            
            builder.Entity<User>(entity =>
            {
                entity.HasIndex(a => new
                {
                    a.UserName, a.NickName,a.Image
                  
                });
                entity.HasIndex(a => new
                {
                    a.Permalink
                  
                }).HasName("UserPermalink").IsUnique();
                entity.HasIndex(a => new
                {
                    a.EmailAddress
                  
                }).HasName("UserEmailAddress").IsUnique(); 
                entity.HasIndex(a => new
                {
                    a.PhoneNumber
                  
                }).HasName("UserPhoneNumber").IsUnique();
                
            });
            
            builder.Entity<Wallet>(entity =>
            {
                entity.HasIndex(a => new { a.UserId }).HasName("WalletUserIndex").IsUnique();
            });
           
            builder.Entity<Status>(entity =>
            {
               entity.HasIndex(a => new { a.Name }).HasName("StatusNameIndex").IsUnique();
            });
            builder.Entity<ContestType>(entity =>
            {
                entity.HasIndex(a => new { a.Name }).HasName("ContestTypeNameIndex").IsUnique();
            });
            builder.Entity<ContestCategory>(entity =>
            {
                     entity.HasIndex(a => new { a.Name }).HasName("ContestCategoryNameIndex").IsUnique();
            });
            builder.Entity<ContestEntryType>(entity =>
            {
                entity.HasIndex(a => new { a.Name }).HasName("ContestEntryTypeNameIndex").IsUnique();
            });
            builder.Entity<Contest>(entity =>
            {
                entity.HasIndex(a => new
                {
                    a.Title, a.Tags, a.CreationTime, Start = a.StartDate, a.EndDate,a.IsFeatured
                  
                }).HasName("ContestNameIndex");
            });
            builder.Entity<Contest>(entity =>
            {
                entity.HasIndex(a => new
                {
                    a.Permalink
                  
                }).HasName("ContestPermalink").IsUnique();
            });
            builder.Entity<Sponsor>(entity =>
            {
                entity.HasIndex(a => new
                {
                    a.Permalink
                  
                }).HasName("CompanyPermalink").IsUnique();
            });
            
            builder.Entity<ContestEntry>(entity =>
            {
                entity.HasIndex(a => new { a.Permalink }).HasName("ContestEntryPermalinkIndex").IsUnique();
            }); 
            builder.Entity<ContestEntry>(entity =>
            {
                entity.HasIndex(a => new { a.IsFeatured });
            });
            builder.Entity<VoteType>(entity =>
            {
                entity.HasIndex(a => new { a.Name })
                    .HasName("VoteTypeNameIndex").IsUnique();
            });
            builder.Entity<UserVotePoint>(entity =>
            {
                entity.HasIndex(a => new { a.UserId })
                    .HasName("UserVotePointUserIdIndex").IsUnique();
            });
            builder.Entity<Vote>(entity =>
            {
                entity.HasIndex(a => new { a.Hash })
                    .HasName("VoteHashIndex");
            });
            
            builder.Entity<PaymentType>(entity =>
            {
                entity.HasIndex(a => new { a.Name })
                    .HasName("PaymentTypeNameIndex").IsUnique();
            }); 
            builder.Entity<PaymentHistory>(entity =>
            {
                entity.HasIndex(a => new { a.Reference })
                    .HasName("PaymentHistoryReferenceIndex").IsUnique();
            });
            builder.Entity<Bank>(entity =>
            {
                entity.HasIndex(a => new { a.Name, a.Code }).IsUnique();
            });
            builder.Entity<Bank>(entity =>
            {
                entity.HasIndex(a => new { a.Active });
            });
            builder.Entity<BankDetails>(entity =>
            {
                entity.HasIndex(a => new { a.AccountName });
            });
            builder.Entity<BankDetails>(entity =>
            {
                entity.HasIndex(a => new { a.AccountNumber }).IsUnique();
            }); 
            builder.Entity<AdvertPrice>(entity =>
            {
                entity.HasIndex(a => new { a.Name });
            }); 
             builder.Entity<FeaturedContest>(entity =>
            {
                entity.HasIndex(a => new { a.StartDate, a.EndDate });
            }); 
            
        }
        /*public override int SaveChanges()
        {
            var result = base.SaveChanges();
            try
            {

                var modifiedTransactionHistoryEntries = ChangeTracker.Entries<ITransactionHistoryBase>();
                if (modifiedTransactionHistoryEntries.Any())
                {
                    /var ctx = ((IObjectContextAdapter)this).ObjectContext;
                    var appDbContextChangeTracker = (IContestProDbContext)ctx.InterceptionContext.DbContexts.FirstOrDefault(p => p.GetType() == typeof(IContestProDbContext));
                    if (appDbContextChangeTracker != null)
                        UpdateTransactionHistoryEntries(modifiedTransactionHistoryEntries, appDbContextChangeTracker);

                }
               
                return base.SaveChanges();
            }
            catch
                (Exception dbEx)
            {

                throw new Exception("Entity validation errors: " + dbEx.Message);  // You can also choose to handle the exception here...
            }
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = await base.SaveChangesAsync(cancellationToken);
            return result;
        }
        
        
        private void UpdateTransactionHistoryEntries(IEnumerable<DbEntityEntry<TransactionHistoryBase>> modifiedTransactionHistoryEntries
            , IContestProDbContext appDbContextChangeTracker)
        {
            //if (!HttpContext.Current.User.Identity.IsAuthenticated)
            //    throw new Exception("You must be logged in to carry out this transaction.");
          //Transaction history Inherited classes
            foreach (var entry in modifiedTransactionHistoryEntries)
            {
                if (entry.Entity == null) continue;
                if (entry.Entity.GetType() == typeof(Wallet))
                {
                    var wallet = entry.Entity as Wallet;
                    if (wallet == null) continue;
                    var transactionHistory = new TransactionHistory
                    {
                        FromUserId = Thread.CurrentPrincipal.Identity.GetUserId(),
                        TransactionAmount = wallet.WalletType == WalletType.Credit ? wallet.LastCreditedAmount : wallet.WalletType == WalletType.Debit ? wallet.LastDebitedAmount : 0,
                        Type = wallet.WalletType == WalletType.Credit ? TransactionHistoryType.WalletCredit : TransactionHistoryType.WalletDebit,
                    };
                    appDbContextChangeTracker.TransactionHistoryHistories.Add(transactionHistory);
                }
                else if (entry.Entity.GetType() == typeof(TransferFundAirtimeToWallet))
                {

                }
            }
        }*/

       
        
    }
      
    }

