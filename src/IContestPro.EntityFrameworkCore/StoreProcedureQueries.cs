﻿namespace IContestPro
{
    public class StoreProcedureQueries
    {
               public const string SpGetRandomFeaturedContestsUp = 
               @"
                CREATE  PROCEDURE [dbo].[SpGetRandomFeaturedContests]
                (
                @PageSize INT
                )
                AS
                BEGIN
                SET NOCOUNT ON;
                 SELECT TOP(@PageSize) fc.Id, fc.ContestId, c.Title, c.IsFeatured, c.Permalink,
                 c.Picture, c.StartDate, c.EndDate from FeaturedContests fc Join Contests c on fc.ContestId = c.Id
                 WHERE c.IsFeatured = 1 and fc.StatusId = 18 and fc.StartDate <= getdate() and fc.EndDate >= getdate()
                 order by NEWID()
                END"; 
                public const string SpGetRandomFeaturedContestsDown = @"Drop PROCEDURE [dbo].[SpGetRandomFeaturedContests]";
            
                public const string SpGetContestEntriesToPromoteUp = 
                @"CREATE PROCEDURE [dbo].[SpGetContestEntriesToPromote]
                    (
                    @UserId INT
                    )
                    AS
                    BEGIN
                     SET NOCOUNT ON;
                    SELECT c.Title, ce.Id from ContestEntries ce join Contests c On ce.ContestId = c.Id
                         WHERE ce.UserId = @UserId and ce.StatusId = 10;
                    END";
                public const string SpGetContestEntriesToPromoteDown = "Drop PROCEDURE [dbo].[SpGetContestEntriesToPromote]";
        
        public const string SpGetRandomFeaturedContestEntriesUp = 
                @"CREATE  PROCEDURE [dbo].[SpGetRandomFeaturedContestEntries]
                  (
                  @PageSize INT
                  )
                  AS
                  BEGIN
                  SET NOCOUNT ON;
                   SELECT TOP(@PageSize)
                    fce.Id, 
                    fce.ContestEntryId,
                    fce.StartDate, 
                    fce.EndDate,
                    ce.IsFeatured  as ContestEntryIsFeatured, 
                    ce.Image as ContestEntryImage, 
                    ce.Video as ContestEntryVideo, 
                    ce.Poster as ContestEntryPoster,
                    et.Name as ContestEntryType,
                    ce.Permalink as ContestEntryPermalink,
                    u.NickName as UserNickName,
                    c.Title as ContestTitle
                   
                   from FeaturedContestEntries fce 
                   Join ContestEntries ce 
                   on fce.ContestEntryId = ce.Id
                   JOIn Contests c ON ce.ContestId = c.Id
                   Join AbpUsers u on fce.UserId = u.Id
                   Join ContestEntryTypes et on c.ContestEntryTypeId = et.Id
                   WHERE fce.StatusId = 18 
                   and fce.StartDate <= getdate() 
                   and fce.EndDate >= getdate()
                   order by NEWID()
                  END";
                public const string SpGetRandomFeaturedContestEntriesDown = "Drop PROCEDURE [dbo].[SpGetRandomFeaturedContestEntries]";
        
     public const string SpGetTopContestEntriesWithHighestVoteUp = 
                @"CREATE  PROCEDURE [dbo].[SpGetTopContestEntriesWithHighestVote]
                 (
                 @ContestId INT,
                 @PageSize INT
                 )
                 AS
                 BEGIN
                  SET NOCOUNT ON;
                 SELECT Top(@PageSize) u.NickName, u.Image,u.Permalink, count(cv.Id) TotalVotes, c.Id ContestId, ce.Id ContestEntryId
                 FROM Contests c JOIN ContestEntries ce ON ce.ContestId= c.Id 
                 JOIN AbpUsers u on ce.UserId = u.Id
                 JOIN Votes cv ON ce.Id= cv.ContestEntryId 
                 where c.Id = @ContestId GROUP BY u.NickName, u.Image,u.Permalink,c.Id, ce.Id ORDER BY TotalVotes DESC;
                 END
               ";
                public const string SpGetTopContestEntriesWithHighestVoteDown = "Drop PROCEDURE [dbo].[SpGetTopContestEntriesWithHighestVote]";
    }
}