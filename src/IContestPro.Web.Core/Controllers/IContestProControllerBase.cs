using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Filters;

namespace IContestPro.Controllers
{
    public abstract class IContestProControllerBase: AbpController
    {
        /// <summary>
        /// Check whether request originated from mobile app.
        /// </summary>
        protected bool IsMobile { get; set; }
        protected IContestProControllerBase()
        {
            LocalizationSourceName = IContestProConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
        
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
           var isMobile = Request.Headers["IsMobile"];
            IsMobile = isMobile.Count > 0;
            base.OnActionExecuting(filterContext);
        }
        
    }

    
}

