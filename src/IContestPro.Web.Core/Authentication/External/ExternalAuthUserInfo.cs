﻿namespace IContestPro.Authentication.External
{
    public class ExternalAuthUserInfo
    {
        public string ProviderKey { get; set; }

        //public string Name { get; set; }
        public string NickName { get; set; }

        public string EmailAddress { get; set; }

        // public string Surname { get; set; }
        public string PhoneNumber { get; set; }

        public string Provider { get; set; }
        
        public string CountryId { get; set; }
        
        public int? StateId { get; set; }
    }
}
